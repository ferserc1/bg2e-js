let fs = require("fs");
let path = require("path");

let enginePath = path.join(__dirname,"../");

function requireHeadScript(src) {
    let head = document.getElementsByTagName('head')[0];
    let script = document.createElement('script');
    script.src = src;
    head.appendChild(script);
}

function requireJavaScript(src) {
    let stats = fs.statSync(src);
    if (stats.isDirectory()) {
        fs.readdirSync(src).forEach((file) => {
            requireJavaScript(path.join(src,file));
        })
    }
    else if (/js/i.test(src.split(".").pop())) {
        requireHeadScript(src);
    }
    
}

// This line will include all the bg2 engine sources, but it's more efficient to include
// the precompiled bg2e-es2015.js file, using a <script> tag in index.html.
// Anyway, using this method, it's easier to extend and debug the engine, because you don't
// need to launch gulp, and you can browse the original sources in the electron.js debugger
requireJavaScript(path.join(enginePath,'src'));

// This will require the window-controller.js. You can simply add the file using a <script> tag
requireJavaScript(path.join(__dirname,'window-controller.js'));
