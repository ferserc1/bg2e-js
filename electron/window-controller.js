
class SceneLoadWindowController extends bg.app.WindowController {
	loadScene() {		
		bg.base.Loader.RegisterPlugin(new bg.base.TextureLoaderPlugin());
		bg.base.Loader.RegisterPlugin(new bg.base.VWGLBLoaderPlugin());
		bg.base.Loader.RegisterPlugin(new bg.base.SceneLoaderPlugin());

		bg.base.Loader.Load(this.gl,"../data/sample-scene/scene.vitscnj")
			.then((result) => {
				this._root = result.sceneRoot;
				let cameraNode = result.cameraNode;
				this._camera = cameraNode.camera;

				// Add a camera handler component
				let ctrl = new bg.manipulation.OrbitCameraController();
				cameraNode.addComponent(ctrl);
				cameraNode.addComponent(new bg.scene.Transform());
				ctrl.minPitch = -45;

				// Post reshape (to update the camera viewport) and redisplay
				this.postReshape();
				this.postRedisplay();
			});
	}
    
	init() {
		bg.Engine.Set(new bg.webgl1.Engine(this.gl));
		
        this.loadScene();
		
		this._renderer = bg.render.Renderer.Create(this.gl,bg.render.RenderPath.DEFERRED);
		this._inputVisitor = new bg.scene.InputVisitor();

		this._renderer.settings.ambientOcclusion.kernelSize = 32;
		this._renderer.settings.ambientOcclusion.blur = 2;
		this._renderer.settings.ambientOcclusion.sampleRadius = 0.15;
		this._renderer.settings.raytracer.quality = bg.render.RaytracerQuality.high;
	}
    
    frame(delta) {
		if  (this._root) this._renderer.frame(this._root, delta);
	}
	
	display() {
		if  (this._root) {
			this._renderer.display(this._root, this._camera);
		}
	}
	
	reshape(width,height) {
		if (this._camera) {
			this._camera.viewport = new bg.Viewport(0,0,width,height);
			this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
		}
	}
	
	// Pass the input events to the scene
	mouseDown(evt) {
		if  (this._root) this._inputVisitor.mouseDown(this._root,evt);
	}
	
	mouseDrag(evt) {
		if  (this._root) this._inputVisitor.mouseDrag(this._root,evt);
		this.postRedisplay();
	}
	
	mouseWheel(evt) {
		if  (this._root) this._inputVisitor.mouseWheel(this._root,evt);
		this.postRedisplay();
	}
	
	touchStart(evt) {
		if  (this._root) this._inputVisitor.touchStart(this._root,evt);
	}
	
	touchMove(evt) {
		if  (this._root) this._inputVisitor.touchMove(this._root,evt);
		this.postRedisplay();
	}
	
	// You may pass also the following events, but they aren't used by the camera controller
	mouseUp(evt) { if  (this._root) this._inputVisitor.mouseUp(this._root,evt); }
	mouseMove(evt) { if  (this._root) this._inputVisitor.mouseMove(this._root,evt); }
	mouseOut(evt) { if  (this._root) this._inputVisitor.mouseOut(this._root,evt); }
	touchEnd(evt) { if  (this._root) this._inputVisitor.touchEnd(this._root,evt); }
}

function load() {
	let controller = new SceneLoadWindowController();
	let mainLoop = bg.app.MainLoop.singleton;
	window.windowController = controller;
	
	mainLoop.updateMode = bg.app.FrameUpdate.MANUAL;
	mainLoop.canvas = document.getElementsByTagName('canvas')[0];
	mainLoop.run(controller);
}
