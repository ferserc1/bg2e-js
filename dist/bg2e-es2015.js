
const bg = {};
bg.version = "1.4.3 - build: 83729c0";
bg.utils = {};

Reflect.defineProperty = Reflect.defineProperty || Object.defineProperty;

(function(win) {
	win.requestAnimFrame = (function() {
	return	window.requestAnimationFrame ||
			window.webkitRequestAnimationFrame ||
			window.mozRequestAnimationFrame ||
			window.oRequestAnimationFrame ||
			window.msRequestAnimationFrame ||
			function(callback,element) { window.setTimeout(callback,1000/60); };
	})();

	bg.utils.initWebGLContext = function(canvas) {
			let context = {
				gl:null,
				supported:false,
				experimental:false
			}
			try {
				context.gl = canvas.getContext("webgl",{ preserveDrawingBuffer:true });
				context.supported = context.gl!=null;
			}
			catch (e) {
				context.supported = false;
			}
			
			if (!context.supported) {
				try {
					context.gl = canvas.getContext("experimental-webgl", { preserveDrawingBuffer:true });
					context.supported = context.gl!=null;
					context.experimental = true;
				}
				catch(e) {
				}
			}
			
			if (context) {
				context.gl.uuid = Symbol(context.gl);
			}
			return context;
		};

	bg.utils.isBigEndian = function() {
			let arr32 = new Uint32Array(1);
			var arr8 = new Uint8Array(arr32.buffer);
			arr32[0] = 255;
			return arr32[3]==255;
		};
	
	bg.utils.isLittleEndian = function() {
			let arr32 = new Uint32Array(1);
			var arr8 = new Uint8Array(arr32.buffer);
			arr32[0] = 255;
			return arr32[0]==255;
		};
	
	class UserAgent {
	
		constructor(userAgentString) {
			this.system = {};
			this.browser = {};
			
			if (!userAgentString) {
				userAgentString = navigator.userAgent;
			}
			this.parseOperatingSystem(userAgentString);
			this.parseBrowser(userAgentString);
		}

		parseOperatingSystem(userAgentString) {
			this.system.OSX = /Macintosh/.test(userAgentString);
			this.system.Windows = /Windows/.test(userAgentString);
			this.system.iPhone = /iPhone/.test(userAgentString);
			this.system.iPodTouch = /iPod/.test(userAgentString);
			this.system.iPad = /iPad/.test(userAgentString);
			this.system.iOS = this.system.iPhone || this.system.iPad || this.system.iPodTouch;
			this.system.Android = /Android/.test(userAgentString);
			this.system.Linux = (this.system.Android) ? false:/Linux/.test(userAgentString);

			if (this.system.OSX) {
				this.system.OSName = "OS X";
				this.parseOSXVersion(userAgentString);
			}
			else if (this.system.Windows) {
				this.system.OSName = "Windows";
				this.parseWindowsVersion(userAgentString);
			}
			else if (this.system.Linux) {
				this.system.OSName = "Linux";
				this.parseLinuxVersion(userAgentString);
			}
			else if (this.system.iOS) {
				this.system.OSName = "iOS";
				this.parseIOSVersion(userAgentString);
			}
			else if (this.system.Android) {
				this.system.OSName = "Android";
				this.parseAndroidVersion(userAgentString);
			}
		}

		parseBrowser(userAgentString) {
			// Safari: Version/X.X.X Safari/XXX
			// Chrome: Chrome/XX.X.XX.XX Safari/XXX
			// Opera: Opera/X.XX
			// Firefox: Gecko/XXXXXX Firefox/XX.XX.XX
			// Explorer: MSIE X.X
			this.browser.Version = {};
			this.browser.Safari = /Version\/([\d\.]+) Safari\//.test(userAgentString);
			if (this.browser.Safari) {
				this.browser.Name = "Safari";
				this.browser.Vendor = "Apple";
				this.browser.Version.versionString = RegExp.$1;
			}

			this.browser.Chrome = /Chrome\/([\d\.]+) Safari\//.test(userAgentString);
			if (this.browser.Chrome) {
				this.browser.Name = "Chrome";
				this.browser.Vendor = "Google";
				this.browser.Version.versionString = RegExp.$1;
			}

			this.browser.Opera = /Opera\/[\d\.]+/.test(userAgentString);
			if (this.browser.Opera) {
				this.browser.Name = "Opera";
				this.browser.Vendor = "Opera Software";
				var versionString = /Version\/([\d\.]+)/.test(userAgentString);
				this.browser.Version.versionString = RegExp.$1;
			}

			this.browser.Firefox = /Gecko\/[\d\.]+ Firefox\/([\d\.]+)/.test(userAgentString);
			if (this.browser.Firefox) {
				this.browser.Name = "Firefox";
				this.browser.Vendor = "Mozilla Foundation";
				this.browser.Version.versionString = RegExp.$1;
			}

			this.browser.Edge = /Edge\/(.*)/.test(userAgentString);
			if (this.browser.Edge) {
				var result = /Edge\/(.*)/.exec(userAgentString);
				this.browser.Name = "Edge";
				this.browser.Chrome = false;
				this.browser.Vendor = "Microsoft";
				this.browser.Version.versionString = result[1];
			} 

			this.browser.Explorer = /MSIE ([\d\.]+)/.test(userAgentString);
			if (!this.browser.Explorer) {
				var re = /\Mozilla\/5.0 \(([^)]+)\) like Gecko/
				var matches = re.exec(userAgentString);
				if (matches) {
					re = /rv:(.*)/
					var version = re.exec(matches[1]);
					this.browser.Explorer = true;
					this.browser.Name = "Internet Explorer";
					this.browser.Vendor = "Microsoft";
					if (version) {
						this.browser.Version.versionString = version[1];
					}
					else {
						this.browser.Version.versionString = "unknown";
					}
				}
			}
			else {
				this.browser.Name = "Internet Explorer";
				this.browser.Vendor = "Microsoft";
				this.browser.Version.versionString = RegExp.$1;
			}

			if (this.system.iOS) {
				this.browser.IsMobileVersion = true;
				this.browser.MobileSafari = /Version\/([\d\.]+) Mobile/.test(userAgentString);
				if (this.browser.MobileSafari) {
					this.browser.Name = "Mobile Safari";
					this.browser.Vendor = "Apple";
					this.browser.Version.versionString = RegExp.$1;
				}
				this.browser.Android = false;
			}
			else if (this.system.Android) {
				this.browser.IsMobileVersion = true;
				this.browser.Android = /Version\/([\d\.]+) Mobile/.test(userAgentString);
				if (this.browser.MobileSafari) {
					this.browser.Name = "Android Browser";
					this.browser.Vendor = "Google";
					this.browser.Version.versionString = RegExp.$1;
				}
				else {
					this.browser.Chrome = /Chrome\/([\d\.]+)/.test(userAgentString);
					this.browser.Name = "Chrome";
					this.browser.Vendor = "Google";
					this.browser.Version.versionString = RegExp.$1;
				}

				this.browser.Safari = false;
			}
			else {
				this.browser.IsMobileVersion = false;
			}

			this.parseBrowserVersion(userAgentString);
		}

		parseBrowserVersion(userAgentString) {
			if (/([\d]+)\.([\d]+)\.*([\d]*)/.test(this.browser.Version.versionString)) {
				this.browser.Version.major = Number(RegExp.$1);
				this.browser.Version.minor = Number(RegExp.$2);
				this.browser.Version.revision = (RegExp.$3) ? Number(RegExp.$3):0;
			}
		}

		parseOSXVersion(userAgentString) {
			var versionString = (/Mac OS X (\d+_\d+_*\d*)/.test(userAgentString)) ? RegExp.$1:'';
			this.system.Version = {};
			// Safari/Chrome
			if (versionString!='') {
				if (/(\d+)_(\d+)_*(\d*)/.test(versionString)) {
					this.system.Version.major = Number(RegExp.$1);
					this.system.Version.minor = Number(RegExp.$2);
					this.system.Version.revision = (RegExp.$3) ? Number(RegExp.$3):0;
				}
			}
			// Firefox/Opera
			else {
				versionString = (/Mac OS X (\d+\.\d+\.*\d*)/.test(userAgentString)) ? RegExp.$1:'Unknown';
				if (/(\d+)\.(\d+)\.*(\d*)/.test(versionString)) {
					this.system.Version.major = Number(RegExp.$1);
					this.system.Version.minor = Number(RegExp.$2);
					this.system.Version.revision = (RegExp.$3) ? Number(RegExp.$3):0;
				}
			}
			if (!this.system.Version.major) {
				this.system.Version.major = 0;
				this.system.Version.minor = 0;
				this.system.Version.revision = 0;
			}
			this.system.Version.stringValue = this.system.Version.major + '.' + this.system.Version.minor + '.' + this.system.Version.revision;
			switch (this.system.Version.minor) {
				case 0:
					this.system.Version.name = "Cheetah";
					break;
				case 1:
					this.system.Version.name = "Puma";
					break;
				case 2:
					this.system.Version.name = "Jaguar";
					break;
				case 3:
					this.system.Version.name = "Panther";
					break;
				case 4:
					this.system.Version.name = "Tiger";
					break;
				case 5:
					this.system.Version.name = "Leopard";
					break;
				case 6:
					this.system.Version.name = "Snow Leopard";
					break;
				case 7:
					this.system.Version.name = "Lion";
					break;
				case 8:
					this.system.Version.name = "Mountain Lion";
					break;
			}
		}

		parseWindowsVersion(userAgentString) {
			this.system.Version = {};
			if (/NT (\d+)\.(\d*)/.test(userAgentString)) {
				this.system.Version.major = Number(RegExp.$1);
				this.system.Version.minor = Number(RegExp.$2);
				this.system.Version.revision = 0;	// Solo por compatibilidad
				this.system.Version.stringValue = "NT " + this.system.Version.major + "." + this.system.Version.minor;
				var major = this.system.Version.major;
				var minor = this.system.Version.minor;
				var name = 'undefined';
				if (major==5) {
					if (minor==0) this.system.Version.name = '2000';
					else this.system.Version.name = 'XP';
				}
				else if (major==6) {
					if (minor==0) this.system.Version.name = 'Vista';
					else if (minor==1) this.system.Version.name = '7';
					else if (minor==2) this.system.Version.name = '8';
				}
			}
			else {
				this.system.Version.major = 0;
				this.system.Version.minor = 0;
				this.system.Version.name = "Unknown";
				this.system.Version.stringValue = "Unknown";
			}
		}

		parseLinuxVersion(userAgentString) {
			this.system.Version = {};
			this.system.Version.major = 0;
			this.system.Version.minor = 0;
			this.system.Version.revision = 0;
			this.system.Version.name = "";
			this.system.Version.stringValue = "Unknown distribution";
		}

		parseIOSVersion(userAgentString) {
			this.system.Version = {};
			if (/iPhone OS (\d+)_(\d+)_*(\d*)/i.test(userAgentString)) {
				this.system.Version.major = Number(RegExp.$1);
				this.system.Version.minor = Number(RegExp.$2);
				this.system.Version.revision = (RegExp.$3) ? Number(RegExp.$3):0;
				this.system.Version.stringValue = this.system.Version.major + "." + this.system.Version.minor + '.' + this.system.Version.revision;
				this.system.Version.name = "iOS";
			}
			else {
				this.system.Version.major = 0;
				this.system.Version.minor = 0;
				this.system.Version.name = "Unknown";
				this.system.Version.stringValue = "Unknown";
			}
		}

		parseAndroidVersion(userAgentString) {
			this.system.Version = {};
			if (/Android (\d+)\.(\d+)\.*(\d*)/.test(userAgentString)) {
				this.system.Version.major = Number(RegExp.$1);
				this.system.Version.minor = Number(RegExp.$2);
				this.system.Version.revision = (RegExp.$3) ? Number(RegExp.$3):0;
				this.system.Version.stringValue = this.system.Version.major + "." + this.system.Version.minor + '.' + this.system.Version.revision;
			}
			else {
				this.system.Version.major = 0;
				this.system.Version.minor = 0;
				this.system.Version.revision = 0;
			}
			if (/Build\/([a-zA-Z]+)/.test(userAgentString)) {
				this.system.Version.name = RegExp.$1;
			}
			else {
				this.system.Version.name = "Unknown version";
			}
			this.system.Version.stringValue = this.system.Version.major + "." + this.system.Version.minor + '.' + this.system.Version.revision;
		}

		getInfoString() {
			return navigator.userAgent;
		}
	}
	
	bg.utils.UserAgent = UserAgent;
	bg.utils.userAgent = new UserAgent();

	class Path {
		get sep() { return "/"; }

		join(a,b) {
			if (a.lastIndexOf(this.sep)!=a.length-1) {
				return a + this.sep + b;
			}
			else {
				return a + b;
			}
		}

		extension(path) {
			return path.split(".").pop();
		}

		fileName(path) {
			return path.split(this.sep).pop();
		}

		removeFileName(path) {
			let result = path.split(this.sep);
			result.pop();
			return result.join(this.sep);
		}
	}

	bg.utils.Path = Path;
	bg.utils.path = new Path();
})(window);

(function() {
	let s_preventImageDump = [];
	let s_preventVideoDump = [];

	class ResourceProvider {
		getRequest(url,onSuccess,onFail,onProgress) {}
		loadImage(url,onSucces,onFail) {}
		loadVideo(url,onSuccess,onFail) {}
	}

	let g_videoLoaders = {};
	g_videoLoaders["mp4"] = function(url,onSuccess,onFail) {
		var video = document.createElement('video');
		s_preventVideoDump.push(video);
		video.crossOrigin = "";
		video.autoplay = true;
		video.setAttribute("playsinline",null);
		video.addEventListener('canplay',(evt) => {
			let videoIndex = s_preventVideoDump.indexOf(evt.target);
			if (videoIndex!=-1) {
				s_preventVideoDump.splice(videoIndex,1);
			}
			onSuccess(event.target);
		});
		video.addEventListener("error",(evt) => {
			onFail(new Error(`Error loading video: ${url}`));
		});
		video.addEventListener("abort",(evt) => {
			onFail(new Error(`Error loading video: ${url}`));
		});
		video.src = url;
	}
	g_videoLoaders["m4v"] = g_videoLoaders["mp4"];

	class HTTPResourceProvider extends ResourceProvider {
		static AddVideoLoader(type,callback) {
			g_videoLoaders[type] = callback;
		}

		static GetVideoLoaderForType(type) {
			return g_videoLoaders[type];
		}

		static GetCompatibleVideoFormats() {
			return Object.keys(g_videoLoaders);
		}

		static IsVideoCompatible(videoUrl) {
			let ext = Resource.GetExtension(videoUrl);
			return bg.utils.HTTPResourceProvider.GetCompatibleVideoFormats().indexOf(ext)!=-1;
		}

		getRequest(url,onSuccess,onFail,onProgress) {
			var req = new XMLHttpRequest();
			if (!onProgress) {
				onProgress = function(progress) {
					console.log(`Loading ${ progress.file }: ${ progress.loaded / progress.total * 100 } %`);
				}
			}
			req.open("GET",url,true);
			req.addEventListener("load", function() {
				if (req.status===200) {
					onSuccess(req);
				}
				else {
					onFail(new Error(`Error receiving data: ${req.status}, url: ${url}`));
				}
			}, false);
			req.addEventListener("error", function() {
				onFail(new Error(`Cannot make AJAX request. Url: ${url}`));
			}, false);
			req.addEventListener("progress", function(evt) {
				onProgress({ file:url, loaded:evt.loaded, total:evt.total });
			}, false);
			return req;
		}

		loadImage(url,onSuccess,onFail) {
			var img = new Image();
			s_preventImageDump.push(img);
			img.crossOrigin = "";
			img.addEventListener("load",function(event) {
				let imageIndex = s_preventImageDump.indexOf(event.target);
				if (imageIndex!=-1) {
					s_preventImageDump.splice(imageIndex,1);
				}
				onSuccess(event.target);
			});
			img.addEventListener("error",function(event) {
				onFail(new Error(`Error loading image: ${url}`));
			});
			img.addEventListener("abort",function(event) {
				onFail(new Error(`Image load aborted: ${url}`));
			});
			img.src = url + '?' + bg.utils.generateUUID();
		}

		loadVideo(url,onSuccess,onFail) {
			let ext = Resource.GetExtension(url);
			let loader = bg.utils.HTTPResourceProvider.GetVideoLoaderForType(ext);
			if (loader) {
				loader.apply(this,[url,onSuccess,onFail]);
			}
			else {
				onFail(new Error(`Could not find video loader for resource: ${ url }`));
			}
		}
	}

	bg.utils.ResourceProvider = ResourceProvider;
	bg.utils.HTTPResourceProvider = HTTPResourceProvider;
	
	let g_resourceProvider = new HTTPResourceProvider();

	class Resource {
		static SetResourceProvider(provider) {
			g_resourceProvider = provider;
		}

		static GetResourceProvider() {
			return g_resourceProvider;
		}

		static GetExtension(url) {
			let match = /\.([a-z0-9-_]*)(\?.*)?(\#.*)?$/i.exec(url);
			return (match && match[1].toLowerCase()) || "";
		}
		
		static JoinUrl(url,path) {
			if (url.length==0) return path;
			if (path.length==0) return url;
			return /\/$/.test(url) ? url + path : url + "/" + path;
		}
		
		static IsFormat(url,formats) {
			return formats.find(function(fmt) {
					return fmt==this;
				},Resource.GetExtension(url))!=null;
		}
		
		static IsImage(url) {
			return Resource.IsFormat(url,["jpg","jpeg","gif","png"]);
		}
		
		static IsBinary(url,binaryFormats = ["vwglb","bg2"]) {
			return Resource.IsFormat(url,binaryFormats);
		}

		static IsVideo(url,videoFormats = ["mp4","m4v","ogg","ogv","m3u8","webm"]) {
			return Resource.IsFormat(url,videoFormats);
		}
		
		static LoadMultiple(urlArray,onProgress) {
			let progressFiles = {}

			let progressFunc = function(progress) {
				progressFiles[progress.file] = progress;
				let total = 0;
				let loaded = 0;
				for (let key in progressFiles) {
					let file = progressFiles[key];
					total += file.total;
					loaded += file.loaded;
				}
				if (onProgress) {
					onProgress({ fileList:urlArray, total:total, loaded:loaded });
				}
				else {
					console.log(`Loading ${ Object.keys(progressFiles).length } files: ${ loaded / total * 100}% completed`);
				}
			}

			let resources = [];
			urlArray.forEach(function(url) {
				resources.push(Resource.Load(url,progressFunc));
			});

			let resolvingPromises = resources.map(function(promise) {
				return new Promise(function(resolve) {
					let payload = new Array(2);
					promise.then(function(result) {
						payload[0] = result;
					})
					.catch(function(error) {
						payload[1] = error;
					})
					.then(function() {
						resolve(payload);
					});
				});
			});

			let errors = [];
			let results = [];

			return Promise.all(resolvingPromises)
				.then(function(loadedData) {
					let result = {};
					urlArray.forEach(function(url,index) {
						let pl = loadedData[index];
						result[url] = pl[1] ? null : pl[0];
					});
					return result;
				});
		}
		
		static Load(url,onProgress) {
			let loader = null;
			switch (true) {
				case url.constructor===Array:
					loader = Resource.LoadMultiple;
					break;
				case Resource.IsImage(url):
					loader = Resource.LoadImage;
					break;
				case Resource.IsBinary(url):
					loader = Resource.LoadBinary;
					break;
				case Resource.IsVideo(url):
					loader = Resource.LoadVideo;
					break;
				case Resource.GetExtension(url)=='json':
					loader = Resource.LoadJson;
					break;
				default:
					loader = Resource.LoadText;
			}
			return loader(url,onProgress);
		}
		
		static LoadText(url,onProgress) {
			return new Promise(function(resolve,reject) {
				g_resourceProvider.getRequest(url,
					function(req) {
						resolve(req.responseText);
					},
					function(error) {
						reject(error);
					},onProgress).send();
			});
		}

		static LoadVideo(url,onProgress) {
			return new Promise(function(resolve,reject) {
				g_resourceProvider.loadVideo(
					url,
					(target) => {
						resolve(target);
						bg.emitImageLoadEvent(target);
					},
					(err) => {
						reject(err);
					}
				);
			});
		}
		
		static LoadBinary(url,onProgress) {
			return new Promise(function(resolve,reject) {
				var req = g_resourceProvider.getRequest(url,
						function(req) {
							resolve(req.response);
						},
						function(error) {
							reject(error);
						},onProgress);
				req.responseType = "arraybuffer";
				req.send();
			});
		}
		
		static LoadImage(url) {
			return new Promise(function(resolve,reject) {
				g_resourceProvider.loadImage(
					url,
					(target) => {
						resolve(target);
						bg.emitImageLoadEvent(target);
					},
					(err) => {
						reject(err);
					}
				);
			});
		}
		
		static LoadJson(url) {
			return new Promise(function(resolve,reject) {
				g_resourceProvider.getRequest(url,
					function(req) {
						try {
							resolve(JSON.parse(req.responseText));
						}
						catch(e) {
							reject(new Error("Error parsing JSON data"));
						}
					},
					function(error) {
						reject(error);
					}).send();
			});
		}
	}
	
	bg.utils.Resource = Resource;

	bg.utils.requireGlobal = function(src) {
		let s = document.createElement('script');
		s.src = src;
		s.type = "text/javascript";
		s.async = false;
		document.getElementsByTagName('head')[0].appendChild(s);
	}

})();
(function() {
	let s_Engine = null;
	
	class Engine {
		static Set(engine) {
			s_Engine = engine;
		}
		
		static Get() {
			return s_Engine;
		}
		
		get id() { return this._engineId; }
		
		get texture() { return this._texture; }
		get pipeline() { return this._pipeline; }
		get polyList() { return this._polyList; }
		get shader() { return this._shader; }
		get colorBuffer() { return this._colorBuffer; }
		get textureBuffer() { return this._textureBuffer; }
		get shaderSource() { return this._shaderSource; }
		get cubemapCapture() { return this._cubemapCapture; }
		get textureMerger() { return this._textureMerger; }
	}
	
	bg.Engine = Engine;
})();
(function() {
	
	class LifeCycle {
		init() {}
		frame(delta) {}

		displayGizmo(pipeline,matrixState) {}
		
		////// Direct rendering methods: will be deprecated soon
		willDisplay(pipeline,matrixState) {}
		display(pipeline,matrixState,forceDraw=false) {}
		didDisplay(pipeline,matrixState) {}
		////// End direct rendering methods

		////// Render queue methods
		willUpdate(modelMatrixStack,viewMatrixStack,projectionMatrixStack) {}
		draw(renderQueue,modelMatrixStack,viewMatrixStack,projectionMatrixStack) {}
		didUpdate(modelMatrixStack,viewMatrixStack,projectionMatrixStack) {}
		////// End render queue methods

		reshape(pipeline,matrixState,width,height) {}
		keyDown(evt) {}
		keyUp(evt) {}
		mouseUp(evt) {}
		mouseDown(evt) {}
		mouseMove(evt) {}
		mouseOut(evt) {}
		mouseDrag(evt) {}
		mouseWheel(evt) {}
		touchStart(evt) {}
		touchMove(evt) {}
		touchEnd(evt) {}

		// Utility functions: do not override
		// 4 frames to ensure that the reflections are fully updated
		postRedisplay(frames=4) {
			bg.app.MainLoop.singleton.postRedisplay(frames);
		}

		postReshape() {
			bg.app.MainLoop.singleton.postReshape();
		}
	}
	
	bg.LifeCycle = LifeCycle;
	
})();
// https://github.com/xibre/FastMD5
(function() {
    !function(r){function n(r){for(var n="",t="",o=0,e=0,a=0,i=r.length;i>a;a++){var f=r.charCodeAt(a);128>f?e++:(t=2048>f?String.fromCharCode(f>>6|192,63&f|128):String.fromCharCode(f>>12|224,f>>6&63|128,63&f|128),e>o&&(n+=r.slice(o,e)),n+=t,o=e=a+1)}return e>o&&(n+=r.slice(o,i)),n}function t(r){var n,t;if(r+="",s=!1,v=w=r.length,w>63){for(o(r.substring(0,64)),i(A),s=!0,n=128;w>=n;n+=64)o(r.substring(n-64,n)),f(A);r=r.substring(n-64),w=r.length}for(d[0]=d[1]=d[2]=d[3]=d[4]=d[5]=d[6]=d[7]=d[8]=d[9]=d[10]=d[11]=d[12]=d[13]=d[14]=d[15]=0,n=0;w>n;n++)t=3&n,0===t?d[n>>2]=r.charCodeAt(n):d[n>>2]|=r.charCodeAt(n)<<C[t];return d[n>>2]|=h[3&n],n>55?(s?f(d):(i(d),s=!0),f([0,0,0,0,0,0,0,0,0,0,0,0,0,0,v<<3,0])):(d[14]=v<<3,void(s?f(d):i(d)))}function o(r){for(var n=16;n--;){var t=n<<2;A[n]=r.charCodeAt(t)+(r.charCodeAt(t+1)<<8)+(r.charCodeAt(t+2)<<16)+(r.charCodeAt(t+3)<<24)}}function e(r,o,e){t(o?r:n(r));var a=g[0];return u[1]=l[15&a],u[0]=l[15&(a>>=4)],u[3]=l[15&(a>>=4)],u[2]=l[15&(a>>=4)],u[5]=l[15&(a>>=4)],u[4]=l[15&(a>>=4)],u[7]=l[15&(a>>=4)],u[6]=l[15&(a>>=4)],a=g[1],u[9]=l[15&a],u[8]=l[15&(a>>=4)],u[11]=l[15&(a>>=4)],u[10]=l[15&(a>>=4)],u[13]=l[15&(a>>=4)],u[12]=l[15&(a>>=4)],u[15]=l[15&(a>>=4)],u[14]=l[15&(a>>=4)],a=g[2],u[17]=l[15&a],u[16]=l[15&(a>>=4)],u[19]=l[15&(a>>=4)],u[18]=l[15&(a>>=4)],u[21]=l[15&(a>>=4)],u[20]=l[15&(a>>=4)],u[23]=l[15&(a>>=4)],u[22]=l[15&(a>>=4)],a=g[3],u[25]=l[15&a],u[24]=l[15&(a>>=4)],u[27]=l[15&(a>>=4)],u[26]=l[15&(a>>=4)],u[29]=l[15&(a>>=4)],u[28]=l[15&(a>>=4)],u[31]=l[15&(a>>=4)],u[30]=l[15&(a>>=4)],e?u:u.join("")}function a(r,n,t,o,e,a,i){return n+=r+o+i,(n<<e|n>>>a)+t<<0}function i(r){c(0,0,0,0,r),g[0]=y[0]+1732584193<<0,g[1]=y[1]-271733879<<0,g[2]=y[2]-1732584194<<0,g[3]=y[3]+271733878<<0}function f(r){c(g[0],g[1],g[2],g[3],r),g[0]=y[0]+g[0]<<0,g[1]=y[1]+g[1]<<0,g[2]=y[2]+g[2]<<0,g[3]=y[3]+g[3]<<0}function c(r,n,t,o,e){var i,f;s?(r=a((t^o)&n^o,r,n,e[0],7,25,-680876936),o=a((n^t)&r^t,o,r,e[1],12,20,-389564586),t=a((r^n)&o^n,t,o,e[2],17,15,606105819),n=a((o^r)&t^r,n,t,e[3],22,10,-1044525330)):(r=e[0]-680876937,r=(r<<7|r>>>25)-271733879<<0,o=e[1]-117830708+(2004318071&r^-1732584194),o=(o<<12|o>>>20)+r<<0,t=e[2]-1126478375+((-271733879^r)&o^-271733879),t=(t<<17|t>>>15)+o<<0,n=e[3]-1316259209+((o^r)&t^r),n=(n<<22|n>>>10)+t<<0),r=a((t^o)&n^o,r,n,e[4],7,25,-176418897),o=a((n^t)&r^t,o,r,e[5],12,20,1200080426),t=a((r^n)&o^n,t,o,e[6],17,15,-1473231341),n=a((o^r)&t^r,n,t,e[7],22,10,-45705983),r=a((t^o)&n^o,r,n,e[8],7,25,1770035416),o=a((n^t)&r^t,o,r,e[9],12,20,-1958414417),t=a((r^n)&o^n,t,o,e[10],17,15,-42063),n=a((o^r)&t^r,n,t,e[11],22,10,-1990404162),r=a((t^o)&n^o,r,n,e[12],7,25,1804603682),o=a((n^t)&r^t,o,r,e[13],12,20,-40341101),t=a((r^n)&o^n,t,o,e[14],17,15,-1502002290),n=a((o^r)&t^r,n,t,e[15],22,10,1236535329),r=a((n^t)&o^t,r,n,e[1],5,27,-165796510),o=a((r^n)&t^n,o,r,e[6],9,23,-1069501632),t=a((o^r)&n^r,t,o,e[11],14,18,643717713),n=a((t^o)&r^o,n,t,e[0],20,12,-373897302),r=a((n^t)&o^t,r,n,e[5],5,27,-701558691),o=a((r^n)&t^n,o,r,e[10],9,23,38016083),t=a((o^r)&n^r,t,o,e[15],14,18,-660478335),n=a((t^o)&r^o,n,t,e[4],20,12,-405537848),r=a((n^t)&o^t,r,n,e[9],5,27,568446438),o=a((r^n)&t^n,o,r,e[14],9,23,-1019803690),t=a((o^r)&n^r,t,o,e[3],14,18,-187363961),n=a((t^o)&r^o,n,t,e[8],20,12,1163531501),r=a((n^t)&o^t,r,n,e[13],5,27,-1444681467),o=a((r^n)&t^n,o,r,e[2],9,23,-51403784),t=a((o^r)&n^r,t,o,e[7],14,18,1735328473),n=a((t^o)&r^o,n,t,e[12],20,12,-1926607734),i=n^t,r=a(i^o,r,n,e[5],4,28,-378558),o=a(i^r,o,r,e[8],11,21,-2022574463),f=o^r,t=a(f^n,t,o,e[11],16,16,1839030562),n=a(f^t,n,t,e[14],23,9,-35309556),i=n^t,r=a(i^o,r,n,e[1],4,28,-1530992060),o=a(i^r,o,r,e[4],11,21,1272893353),f=o^r,t=a(f^n,t,o,e[7],16,16,-155497632),n=a(f^t,n,t,e[10],23,9,-1094730640),i=n^t,r=a(i^o,r,n,e[13],4,28,681279174),o=a(i^r,o,r,e[0],11,21,-358537222),f=o^r,t=a(f^n,t,o,e[3],16,16,-722521979),n=a(f^t,n,t,e[6],23,9,76029189),i=n^t,r=a(i^o,r,n,e[9],4,28,-640364487),o=a(i^r,o,r,e[12],11,21,-421815835),f=o^r,t=a(f^n,t,o,e[15],16,16,530742520),n=a(f^t,n,t,e[2],23,9,-995338651),r=a(t^(n|~o),r,n,e[0],6,26,-198630844),o=a(n^(r|~t),o,r,e[7],10,22,1126891415),t=a(r^(o|~n),t,o,e[14],15,17,-1416354905),n=a(o^(t|~r),n,t,e[5],21,11,-57434055),r=a(t^(n|~o),r,n,e[12],6,26,1700485571),o=a(n^(r|~t),o,r,e[3],10,22,-1894986606),t=a(r^(o|~n),t,o,e[10],15,17,-1051523),n=a(o^(t|~r),n,t,e[1],21,11,-2054922799),r=a(t^(n|~o),r,n,e[8],6,26,1873313359),o=a(n^(r|~t),o,r,e[15],10,22,-30611744),t=a(r^(o|~n),t,o,e[6],15,17,-1560198380),n=a(o^(t|~r),n,t,e[13],21,11,1309151649),r=a(t^(n|~o),r,n,e[4],6,26,-145523070),o=a(n^(r|~t),o,r,e[11],10,22,-1120210379),t=a(r^(o|~n),t,o,e[2],15,17,718787259),n=a(o^(t|~r),n,t,e[9],21,11,-343485551),y[0]=r,y[1]=n,y[2]=t,y[3]=o}var u=[],d=[],A=[],h=[],l="0123456789abcdef".split(""),C=[],g=[],s=!1,v=0,w=0,y=[];if(r.Int32Array)d=new Int32Array(16),A=new Int32Array(16),h=new Int32Array(4),C=new Int32Array(4),g=new Int32Array(4),y=new Int32Array(4);else{var I;for(I=0;16>I;I++)d[I]=A[I]=0;for(I=0;4>I;I++)h[I]=C[I]=g[I]=y[I]=0}h[0]=128,h[1]=32768,h[2]=8388608,h[3]=-2147483648,C[0]=0,C[1]=8,C[2]=16,C[3]=24,r.md5=r.md5||e}("undefined"==typeof global?window:global);

    bg.utils.md5 = md5;
})();

(function() {
    function generateUUID () { // Public Domain/MIT
        var d = new Date().getTime();
        if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
            d += performance.now(); //use high-precision timer if available
        }
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
    }

    bg.utils.generateUUID = generateUUID;
})();
bg.app = {};
(function() {
	
	class Canvas {
		constructor(domElem) {		
			let initContext = () => {
				this._context = bg.utils.initWebGLContext(domElem);
				return this._context.supported;
			}
			
						
			// Attributes
			this._domElem = domElem;
			this._domElem.style.MozUserSelect = 'none';
			this._domElem.style.WebkitUserSelect = 'none';
			this._domElem.setAttribute("onselectstart","return false");

			this._multisample = 1.0;
			
			// Initialization
			if (!initContext()) {
				throw new Error("Sorry, your browser does not support WebGL.");
			}
		}

		get multisample() {
			return this._multisample;
		}

		set multisample(ms) {
			this._multisample = ms;
		}
		
		get context() {
			return this._context;
		}
		
		get domElement() {
			return this._domElem;
		}
		
		get width() {
			return this._domElem.clientWidth;
		}
		
		get height() {
			return this._domElem.clientHeight;
		}

		screenshot(format, width, height) {
			let canvasStyle = "";
			let prevSize = {}
			if (width) {
				height = height ? height:width;
				canvasStyle = this.domElement.style.cssText;
				prevSize.width = this.domElement.width;
				prevSize.height = this.domElement.height;

				this.domElement.style.cssText = "top:auto;left:auto;bottom:auto;right:auto;width:" + width + "px;height:" + height + "px;";
				this.domElement.width = width;
				this.domElement.height = height;
				bg.app.MainLoop.singleton.windowController.reshape(width,height);
				bg.app.MainLoop.singleton.windowController.postRedisplay();
				bg.app.MainLoop.singleton.windowController.display();
			}
			var data = this.domElement.toDataURL(format);
			if (width) {
				this.domElement.style.cssText = canvasStyle;
				this.domElement.width = prevSize.width;
				this.domElement.height = prevSize.height;
				bg.app.MainLoop.singleton.windowController.reshape(prevSize.width,prevSize.height);
				bg.app.MainLoop.singleton.windowController.postRedisplay();
				bg.app.MainLoop.singleton.windowController.display();
			}
			return data;
		}

	}
	
	bg.app.Canvas = Canvas;
	
	class ContextObject {
		constructor(context) {
			this._context = context;
		}
		
		get context() { return this._context; }
		set context(c) { this._context = c; }
	}
	
	bg.app.ContextObject = ContextObject;
})();
(function() {
	bg.app.SpecialKey = {
		BACKSPACE: "Backspace",
		TAB: "Tab",
		ENTER: "Enter",
		SHIFT: "Shift",
		SHIFT_LEFT: "ShiftLeft",
		SHIFT_RIGHT: "ShiftRight",
		CTRL: "Control",
		CTRL_LEFT: "ControlLeft",
		CTRL_LEFT: "ControlRight",
		ALT: "Alt",
		ALT_LEFT: "AltLeft",
		ALT_RIGHT: "AltRight",
		PAUSE: "Pause",
		CAPS_LOCK: "CapsLock",
		ESCAPE: "Escape",
		PAGE_UP: "PageUp",
		PAGEDOWN: "PageDown",
		END: "End",
		HOME: "Home",
		LEFT_ARROW: "ArrowLeft",
		UP_ARROW: "ArrowUp",
		RIGHT_ARROW: "ArrowRight",
		DOWN_ARROW: "ArrowDown",
		INSERT: "Insert",
		DELETE: "Delete"
	};
	
	class EventBase {
		constructor() {
			this._executeDefault = false;
		}

		get executeDefault() { return this._executeDefault; }
		set executeDefault(d) { this._executeDefault = d; }
	}
	bg.app.EventBase = EventBase;

	class KeyboardEvent extends EventBase {
		static IsSpecialKey(event) {
			return bg.app.SpecialKey[event.code]!=null;
		}
		
		constructor(key,event) {
			super();
			this.key = key;
			this.event = event;
		}
		
		isSpecialKey() {
			return KeyboardEvent.IsSpecialKey(this.event);
		}
	}
	
	bg.app.KeyboardEvent = KeyboardEvent;
	
	bg.app.MouseButton = {
		LEFT: 0,
		MIDDLE: 1,
		RIGHT: 2,
		NONE: -1
	};
	
	class MouseEvent extends EventBase {
		
		constructor(button = bg.app.MouseButton.NONE, x=-1, y=-1, delta=0,event=null) {
			super();

			this.button = button;
			this.x = x;
			this.y = y;
			this.delta = delta;
			this.event = event;
		}
	}
	
	bg.app.MouseEvent = MouseEvent;
	
	class TouchEvent extends EventBase  {
		constructor(touches,event) {
			super();
			this.touches = touches;
			this.event = event;
		}
	}
	
	bg.app.TouchEvent = TouchEvent;
	
})();
(function() {
	let s_mainLoop = null;
	let s_mouseStatus = {
		leftButton:false,
		middleButton:false,
		rightButton:false,
		pos: {
			x:-1,
			y:-1
		}	
	}
	Object.defineProperty(s_mouseStatus, "anyButton", {
		get: function() {
			return this.leftButton || this.middleButton || this.rightButton;
		}
	});
	let s_delta = -1;
	
	class Mouse {
		static LeftButton() { return s_mouseStatus.leftButton; }
		static MiddleButton() { return s_mouseStatus.middleButton; }
		static RightButton() { return s_mouseStatus.rightButton; }
		static Position() { return s_mouseStatus.pos; }
	}
	
	bg.app.Mouse = Mouse;
	
	bg.app.FrameUpdate = {
		AUTO: 0,
		MANUAL: 1
	};
	
	class MainLoop {
		constructor() {
			this._canvas = null;
			this._windowController = null;
			this._updateMode = bg.app.FrameUpdate.AUTO;
			this._redisplayFrames = 1;
			bg.bindImageLoadEvent(() => {
				this.postRedisplay();
			});
		}
		
		get canvas() { return this._canvas; }
		set canvas(c) {
			this._canvas = new bg.app.Canvas(c);
		}
		get windowController() { return this._windowController; }
		get updateMode() { return this._updateMode; }
		set updateMode(m) {
			this._updateMode = m;
			if (this._updateMode==bg.app.FrameUpdate.AUTO) {
				this._redisplayFrames = 1;
			}
		}
		get redisplay() { return this._redisplayFrames>0; }
		get mouseButtonStatus() { return s_mouseStatus; }
		
		run(windowController) {
			this._windowController = windowController;
			this.postRedisplay();
			this.windowController.init();
			initEvents();
			animationLoop();
		}
		
		// 4 frames to ensure that the reflections are fully updated
		postRedisplay(frames=4) {
			this._redisplayFrames = frames;
		}
		
		postReshape() {
			onResize();
		}
	}
	
	let lastTime = 0;
	function animationLoop(totalTime) {
		totalTime = totalTime || 0;
		requestAnimFrame(animationLoop);
		let elapsed = totalTime - lastTime;
		lastTime = totalTime;
		onUpdate(elapsed);
	}

	function initEvents() {
		onResize();
		
		window.addEventListener("resize", function(evt) { onResize(); });
		
		if (s_mainLoop.canvas) {
			let c = s_mainLoop.canvas.domElement;
			c.addEventListener("mousedown", function(evt) {
				if (!onMouseDown(evt).executeDefault) {
					evt.preventDefault();
					return false;
				}
			});
			c.addEventListener("mousemove", function(evt) {
				if (!onMouseMove(evt).executeDefault) {
					evt.preventDefault();
					return false;
				}
			});
			c.addEventListener("mouseout", function(evt) {
				if (!onMouseOut(evt).executeDefault) {
					evt.preventDefault();
					return false;
				}
			});
			c.addEventListener("mouseover", function(evt) {
				if (!onMouseOver(evt).executeDefault) {
					evt.preventDefault();
					return false;
				}
			});
			c.addEventListener("mouseup", function(evt) {
				if (!onMouseUp(evt).executeDefault) { 
					evt.preventDefault();
					return false;
				}
			});
			
			c.addEventListener("touchstart", function(evt) {
				if (!onTouchStart(evt).executeDefault) {
					evt.preventDefault();
					return false;
				}
			});
			c.addEventListener("touchmove", function(evt) {
				if (!onTouchMove(evt).executeDefault) {
					evt.preventDefault();
					return false;
				}
			});
			c.addEventListener("touchend", function(evt) {
				if (!onTouchEnd(evt).executeDefault) { 
					evt.preventDefault();
					return false;
				}
			});
			
			var mouseWheelEvt = (/Firefox/i.test(navigator.userAgent))? "DOMMouseScroll" : "mousewheel";
			c.addEventListener(mouseWheelEvt, function(evt) {
				if (!onMouseWheel(evt).executeDefault) {
					evt.preventDefault();
					return false;
				}
			});
			
			window.addEventListener("keydown", function(evt) { onKeyDown(evt); });
			window.addEventListener("keyup", function(evt) { onKeyUp(evt); });
			
			c.oncontextmenu = function(e) { return false; };
		}
		else {
			throw new Error("Configuration error in MainLoop: no canvas defined");
		}
	}
	
	function onResize() {
		if (s_mainLoop.canvas && s_mainLoop.windowController) {
			let multisample = s_mainLoop.canvas.multisample;
			s_mainLoop.canvas.domElement.width = s_mainLoop.canvas.width * multisample;
			s_mainLoop.canvas.domElement.height = s_mainLoop.canvas.height * multisample; 
			s_mainLoop.windowController.reshape(s_mainLoop.canvas.width * multisample, s_mainLoop.canvas.height * multisample);
		}
	}
	
	function onUpdate(elapsedTime) {
		if (s_mainLoop.redisplay) {
			//if (s_delta==-1) s_delta = Date.now();
			//s_mainLoop.windowController.frame((Date.now() - s_delta) * 2);
			//s_delta = Date.now();
			s_mainLoop.windowController.frame(elapsedTime);
			if (s_mainLoop.updateMode==bg.app.FrameUpdate.AUTO) {
				s_mainLoop._redisplayFrames = 1;
			}
			else {
				s_mainLoop._redisplayFrames--;
			}
			s_mainLoop.windowController.display();
		}
	}
	
	function onMouseDown(event) {
		let offset = s_mainLoop.canvas.domElement.getBoundingClientRect();
		let multisample = s_mainLoop.canvas.multisample;
		s_mouseStatus.pos.x = (event.clientX - offset.left) * multisample;
		s_mouseStatus.pos.y = (event.clientY - offset.top) * multisample;
		switch (event.button) {
			case bg.app.MouseButton.LEFT:
				s_mouseStatus.leftButton = true;
				break;
			case bg.app.MouseButton.MIDDLE:
				s_mouseStatus.middleButton = true;
				break;
			case bg.app.MouseButton.RIGHT:
				s_mouseStatus.rightButton = true;
				break;
		}

		let bgEvent = new bg.app.MouseEvent(event.button,s_mouseStatus.pos.x,s_mouseStatus.pos.y,0,event);
		s_mainLoop.windowController.mouseDown(bgEvent);
		return bgEvent;
	}
	
	function onMouseMove(event) {
		let offset = s_mainLoop.canvas.domElement.getBoundingClientRect();
		let multisample = s_mainLoop.canvas.multisample;
		s_mouseStatus.pos.x = (event.clientX - offset.left) * multisample;
		s_mouseStatus.pos.y = (event.clientY - offset.top) * multisample;
		let evt = new bg.app.MouseEvent(bg.app.MouseButton.NONE,
										s_mouseStatus.pos.x,
										s_mouseStatus.pos.y,
										0,
										event);
		s_mainLoop.windowController.mouseMove(evt);
		if (s_mouseStatus.anyButton) {
			s_mainLoop.windowController.mouseDrag(evt);
		}
		return evt;
	}
	
	function onMouseOut() {
		let bgEvt = new bg.app.MouseEvent(bg.app.MouseButton.NONE,s_mouseStatus.pos.x,s_mouseStatus.pos.y,0,{});
		s_mainLoop.windowController.mouseOut(bgEvt);
		if (s_mouseStatus.leftButton) {
			s_mouseStatus.leftButton = false;
			bgEvt = new bg.app.MouseEvent(bg.app.MouseButton.LEFT,s_mouseStatus.pos.x,s_mouseStatus.pos.y,0,{});
			s_mainLoop.windowController.mouseUp(bgEvt);
		}
		if (s_mouseStatus.middleButton) {
			s_mouseStatus.middleButton = false;
			bgEvt = new bg.app.MouseEvent(bg.app.MouseButton.MIDDLE,s_mouseStatus.pos.x,s_mouseStatus.pos.y,0,{});
			s_mainLoop.windowController.mouseUp(bgEvt);
		}
		if (s_mouseStatus.rightButton) {
			bgEvt = new bg.app.MouseEvent(bg.app.MouseButton.RIGHT,s_mouseStatus.pos.x,s_mouseStatus.pos.y,0,{});
			s_mainLoop.windowController.mouseUp(bgEvt);
			s_mouseStatus.rightButton = false;
		}
		return bgEvt;
	}
	
	function onMouseOver(event) {
		return onMouseMove(event);
	}
	
	function onMouseUp(event) {
		switch (event.button) {
			case bg.app.MouseButton.LEFT:
				s_mouseStatus.leftButton = false;
				break;
			case bg.app.MouseButton.MIDDLE:
				s_mouseStatus.middleButton = false;
				break;
			case bg.app.MouseButton.RIGHT:
				s_mouseStatus.rightButton = false;
				break;
		}
		let offset = s_mainLoop.canvas.domElement.getBoundingClientRect();
		let multisample = s_mainLoop.canvas.multisample;
		s_mouseStatus.pos.x = (event.clientX - offset.left) * multisample;
		s_mouseStatus.pos.y = (event.clientY - offset.top) * multisample;
		let bgEvt = new bg.app.MouseEvent(event.button,s_mouseStatus.pos.x,s_mouseStatus.pos.y,0,event)
		s_mainLoop.windowController.mouseUp(bgEvt);
		return bgEvt;
	}
	
	function onMouseWheel(event) {
		let offset = s_mainLoop.canvas.domElement.getBoundingClientRect();
		let multisample = s_mainLoop.canvas.multisample;
		s_mouseStatus.pos.x = (event.clientX - offset.left) * multisample;
		s_mouseStatus.pos.y = (event.clientY - offset.top) * multisample;
		let delta = event.wheelDelta ? event.wheelDelta * -1:event.detail * 10;
		let bgEvt = new bg.app.MouseEvent(bg.app.MouseButton.NONE,s_mouseStatus.pos.x,s_mouseStatus.pos.y,delta,event)
		s_mainLoop.windowController.mouseWheel(bgEvt);
		return bgEvt;
	}
	
    function getTouchEvent(event) {
        let offset = s_mainLoop.canvas.domElement.getBoundingClientRect();
        let touches = [];
        for (let i=0; i<event.touches.length; ++i) {
            let touch = event.touches[i];
            touches.push({
                identifier: touch.identifier,
                x: touch.clientX - offset.left,
                y: touch.clientY - offset.top,
                force: touch.force,
                rotationAngle: touch.rotationAngle,
                radiusX: touch.radiusX,
                radiusY: touch.radiusY
            });
        }
        return new bg.app.TouchEvent(touches,event);
    }
    
	function onTouchStart(event) {
		let bgEvt = getTouchEvent(event)
        s_mainLoop.windowController.touchStart(bgEvt);
		return bgEvt;
	}
	
	function onTouchMove(event) {
		let bgEvt = getTouchEvent(event)
		s_mainLoop.windowController.touchMove(bgEvt);
		return bgEvt;
	}
	
	function onTouchEnd(event) {
		let bgEvt = getTouchEvent(event)
		s_mainLoop.windowController.touchEnd(bgEvt);
		return bgEvt;
	}
	
	function onKeyDown(event) {
		let code = bg.app.KeyboardEvent.IsSpecialKey(event) ? 	event.keyCode:
																event.code;
		s_mainLoop.windowController.keyDown(new bg.app.KeyboardEvent(code,event));
	}
	
	function onKeyUp(event) {
		let code = bg.app.KeyboardEvent.IsSpecialKey(event) ? 	event.keyCode:
																event.code;
		s_mainLoop.windowController.keyUp(new bg.app.KeyboardEvent(code,event));
	}
	
	bg.app.MainLoop = {};
	
	Object.defineProperty(bg.app.MainLoop,"singleton",{
		get: function() {
			if (!s_mainLoop) {
				s_mainLoop = new MainLoop();
			}
			return s_mainLoop;
		}
	});

})();

(function() {
	class WindowController extends bg.LifeCycle {
		// Functions from LifeCycle:
		// init()
		// frame(delta)
		// display()
		// reshape(width,height)
		// keyDown(evt)
		// keyUp(evt)
		// mouseUp(evt)
		// mouseDown(evt)
		// mouseMove(evt)
		// mouseOut(evt)
		// mouseDrag(evt)
		// mouseWheel(evt)
		// touchStart(evt)
		// touchMove(evt)
		// touchEnd(evt)
		
		// 4 frames to ensure that the reflections are fully updated
		postRedisplay(frames=4) {
			bg.app.MainLoop.singleton.postRedisplay(frames);
		}
		
		postReshape() {
			bg.app.MainLoop.singleton.postReshape();
		}
		
		get canvas() {
			return bg.app.MainLoop.singleton.canvas;
		}
		
		get context() {
			return bg.app.MainLoop.singleton.canvas.context;
		}
		
		get gl() {
			return bg.app.MainLoop.singleton.canvas.context.gl;
		}
	}
	
	bg.app.WindowController = WindowController;
})();
bg.base = {}

bg.s_log = [];
bg.log = function(l) {
	if (console.log) console.log(l);
	bg.s_log.push(l);
};

bg.flushLog = function() {
	if (console.log) {
		bg.s_log.forEach(function(l) {
			console.log(l);
		});
	}
	bg.s_log = [];
};

bg.emitImageLoadEvent = function(img) {
	let event = new CustomEvent("bg2e:image-load", { image:img });
	document.dispatchEvent(event);
};

bg.bindImageLoadEvent = function(callback) {
	document.addEventListener("bg2e:image-load",callback);
};

bg.Axis = {
	NONE: 0,
	X: 1,
	Y: 2,
	Z: 3
};

Object.defineProperty(bg, "isElectronApp", {
	get: function() {
		return typeof module !== 'undefined' && module.exports && true;
	}
});


(function() {
	class Effect extends bg.app.ContextObject {
		constructor(context) {
			super(context);
			this._shader = null;
			this._inputVars = [];
			this._shaders = [];
		}
		
		/*
		 * Override the following functions to create a custom effect:
		 * 	get inputVars(): returns an object with the input attribute for each
		 * 					 vertex buffer in the shader
		 *  get shader(): 	 create and initialize the shader if not exists
		 *  setupVars():	 bind the input vars in the shader
		 */
		get inputVars() {
			// Return the corresponding names for input vars
			//this._inputVars = {
			//	vertex:null,
			//	normal:null,
			//	tex0:null,
			//	tex1:null,
			//	tex2:null,
			//	color:null,
			//	tangent:null
			//};
			
			return this._inputVars;
		}

		get numberOfShaders() {
			return this._shaders.length;
		}

		setCurrentShader(i) {
			this._shader = this._shaders[i];
		}
		
		get shader() {
			// Create and initialize the shader if not exists.
			return this._shader;
		}
		
		// Automatic method: with ShaderSource objects it isn't necesary to
		// overrdide the previous functions
		setupShaderSource(sourceArray,append=true) {
			let shaderIndex = append ? this._shaders.length : 0;
			let shader = new bg.base.Shader(this.context);
			this._inputVars = [];
			let inputAttribs = [];
			let inputVars = [];
			
			sourceArray.forEach((source) => {
				source.params.forEach((param) => {
					if (param) {
						if (param.role=="buffer") {
							this._inputVars[param.target] = param.name;
							inputAttribs.push(param.name);
						}
						else if (param.role=="value") {
							inputVars.push(param.name);
						}
					}
				});
				
				shader.addShaderSource(source.type,source.toString());
			});
			shader.link();
			if (!shader.status) {
				bg.log(shader.compileError);
				if (shader.compileErrorSource) {
					bg.log("Shader source:");
					bg.log(shader.compileErrorSource);
				}
				bg.log(shader.linkError);
			}
			else {
				shader.initVars(inputAttribs,inputVars);
			}

			if (append) {
				this._shaders.push(shader);
				if (!this._shader) {
					this._shader = shader;
				}
			}
			else {
				this._shaders = [shader];
				this._shader = shader;
			}
			return shaderIndex;
		}
		
		// This function is used to setup shader variables that are the same for all
		// scene object, such as lights or color correction
		beginDraw() {
			
		}
		
		// This function is called before draw each polyList. The material properties will be
		// set in this.material
		setupVars() {
			// pass the input vars values to this.shader
		}
		
		setActive() {
			this.shader.setActive();
			this.beginDraw();
		}
		
		clearActive() {
			this.shader.clearActive();
		}
		
		bindPolyList(plist) {
			var s = this.shader;			
			if (this.inputVars.vertex) {
				s.setInputBuffer(this.inputVars.vertex, plist.vertexBuffer, 3);
			}
			if (this.inputVars.normal) {
				s.setInputBuffer(this.inputVars.normal, plist.normalBuffer, 3);
			}
			if (this.inputVars.tex0) {
				s.setInputBuffer(this.inputVars.tex0, plist.texCoord0Buffer, 2);
			}
			if (this.inputVars.tex1) {
				s.setInputBuffer(this.inputVars.tex1, plist.texCoord1Buffer, 2);
			}
			if (this.inputVars.tex2) {
				s.setInputBuffer(this.inputVars.tex2, plist.texCoord2Buffer, 2);
			}
			if (this.inputVars.color) {
				s.setInputBuffer(this.inputVars.color, plist.colorBuffer, 4);
			}
			if (this.inputVars.tangent) {
				s.setInputBuffer(this.inputVars.tangent, plist.tangentBuffer, 3);
			}
			this.setupVars();
		}
		
		unbind() {
			var s = this.shader;	
			if (this.inputVars.vertex) {
				s.disableInputBuffer(this.inputVars.vertex);
			}
			if (this.inputVars.normal) {
				s.disableInputBuffer(this.inputVars.normal);
			}
			if (this.inputVars.tex0) {
				s.disableInputBuffer(this.inputVars.tex0);
			}
			if (this.inputVars.tex1) {
				s.disableInputBuffer(this.inputVars.tex1);
			}
			if (this.inputVars.tex2) {
				s.disableInputBuffer(this.inputVars.tex2);
			}
			if (this.inputVars.color) {
				s.disableInputBuffer(this.inputVars.color);
			}
			if (this.inputVars.tangent) {
				s.disableInputBuffer(this.inputVars.tangent);
			}
		}		
	}
	
	bg.base.Effect = Effect;
	
	function lib() {
		return bg.base.ShaderLibrary.Get();
	}
	
	class TextureEffect extends Effect {
		constructor(context) {
			super(context);

			this._frame = new bg.base.PolyList(context);
			
			this._frame.vertex = [ 1, 1, 0, -1, 1, 0, -1,-1, 0,1,-1, 0 ];
			this._frame.texCoord0 = [ 1, 1, 0, 1, 0, 0, 1, 0 ];
			this._frame.index = [ 0, 1, 2,  2, 3, 0 ];
			
			this._frame.build();
			
			this.rebuildShaders();
		}

		rebuildShaders() {
			this.setupShaderSource([
				this.vertexShaderSource,
				this.fragmentShaderSource
			]);
		}
		
		get vertexShaderSource() {
			if (!this._vertexShaderSource) {
				this._vertexShaderSource = new bg.base.ShaderSource(bg.base.ShaderType.VERTEX);
				this._vertexShaderSource.addParameter([
					lib().inputs.buffers.vertex,
					lib().inputs.buffers.tex0,
					{ name:"fsTexCoord", dataType:"vec2", role:"out" }
				]);
				
				if (bg.Engine.Get().id=="webgl1") {
					this._vertexShaderSource.setMainBody(`
					gl_Position = vec4(inVertex,1.0);
					fsTexCoord = inTex0;`);
				}
			}
			return this._vertexShaderSource;
		}
		
		get fragmentShaderSource() {
			if (!this._fragmentShaderSource) {
				this._fragmentShaderSource = new bg.base.ShaderSource(bg.base.ShaderType.FRAGMENT);
				this._fragmentShaderSource.addParameter(
					{ name:"fsTexCoord", dataType:"vec2", role:"in" }
				);
				
				if (bg.Engine.Get().id=="webgl1") {
					this._fragmentShaderSource.setMainBody(`
					gl_FragColor = vec4(0.0,0.0,0.0,1.0);`);
				}
			}
			return this._fragmentShaderSource;
		}
		
		//setupVars() {
		// this._surface contains the surface passed to drawSurface
		//}

		drawSurface(surface) {
			this.setActive();
			this._surface = surface;
			this.bindPolyList(this._frame);
			this._frame.draw();
			this.unbind();
			this.clearActive();
		}
		
	}
	
	bg.base.TextureEffect = TextureEffect;
	
})();
(function() {
	
	class LoaderPlugin {
				
		acceptType(url,data) { return false; }
		load(context,url,data) {
			return new Promise((resolve,reject) => {
				reject(new Error("Not implemented"));
			});
		}
		
	}
	
	bg.base.LoaderPlugin = LoaderPlugin;
	
	let s_loaderPlugins = [];
	
	function loadUrl(context,url,onProgress = null,extraData = null) {
		return new Promise((accept,reject) => {
			bg.utils.Resource.Load(url,onProgress)
				.then(function(data) {
					return Loader.LoadData(context,url,data,extraData);
				})
				
				.then((result,extendedData) => {
					accept(result,extendedData);
				})
				
				.catch(function(err) {
					reject(err);
				});
		});
	}

	function loadUrlArray(context,url,onProgress = null,extraData = null) {
		return new Promise((accept,reject) => {
			bg.utils.Resource.LoadMultiple(url,onProgress)
				.then((result) => {
					let promises = [];

					for (let itemUrl in result) {
						let data = result[itemUrl];
						promises.push(loadData(context,itemUrl,data,extraData));
					}

					return Promise.all(promises);
				})
				.then((loadedResults) => {
					let resolvedData = {}
					url.forEach((itemUrl,index) => {
						resolvedData[itemUrl] = loadedResults[index];
					})
					accept(resolvedData);
				})
				.catch((err) => {
					reject(err);
				})
		})
	}

	function loadData(context,url,data,extraData = null) {
		return new Promise((accept,reject) => {
			let selectedPlugin = null;
			s_loaderPlugins.some((plugin) => {
				if (plugin.acceptType(url,data)) {
					selectedPlugin = plugin;
					return true;
				}
			})
			
			if (selectedPlugin) {
				if (!extraData) {
					extraData = {};
				}
				accept(selectedPlugin.load(context,url,data,extraData));
			}
			else {
				return reject(new Error("No suitable plugin found for load " + url));
			}
		});
	}

	class Loader {
		static RegisterPlugin(p) { s_loaderPlugins.push(p); }
		
		static Load(context,url,onProgress = null,extraData = null) {
			if (Array.isArray(url)) {
				return loadUrlArray(context,url,onProgress,extraData);
			}
			else {
				return loadUrl(context,url,onProgress,extraData);
			}
		}
		
		static LoadData(context,url,data,extraData = null) {
			return loadData(context,url,data,extraData);
		}
	}
	
	bg.base.Loader = Loader;
	
})();
(function() {
    // NOTE: All the writer functions and classes are intended to be used
    // only in an Electron.js application
    if (!bg.isElectronApp) {
        return false;
    }

    class WriterPlugin {
        acceptType(url,data) { return false; }
        write(url,data) {}
    }

    bg.base.WriterPlugin = WriterPlugin;

    let s_writerPlugins = [];

    class Writer {
        static RegisterPlugin(p) { s_writerPlugins.push(p); }

        static Write(url,data) {
            return new Promise((resolve,reject) => {
                let selectedPlugin = null;
                s_writerPlugins.some((plugin) => {
                    if (plugin.acceptType(url,data)) {
                        selectedPlugin = plugin;
                        return true;
                    }
                });

                if (selectedPlugin) {
                    resolve(selectedPlugin.write(url,data));
                }
                else {
                    reject(new Error("No suitable plugin found for write " + url));
                }
            })
        }

        static PrepareDirectory(dir) {
            let targetDir = Writer.ToSystemPath(dir);
            const fs = require('fs');
            const path = require('path');
            const sep = path.sep;
            const initDir = path.isAbsolute(targetDir) ? sep : '';
            targetDir.split(sep).reduce((parentDir,childDir) => {
                const curDir = path.resolve(parentDir, childDir);
                if (!fs.existsSync(curDir)) {
                    fs.mkdirSync(curDir);
                }
                return curDir;
            }, initDir);
        }

        static StandarizePath(inPath) {
            return inPath.replace(/\\/g,'/');
        }

        static ToSystemPath(inPath) {
            const path = require('path');
            const sep = path.sep;
            return inPath.replace(/\\/g,sep).replace(/\//g,sep);
        }

        static CopyFile(source,target) {
            return new Promise((resolve,reject) => {
                const fs = require("fs");
                const path = require("path");
                let cbCalled = false;

                source = Writer.StandarizePath(path.resolve(source));
                target = Writer.StandarizePath(path.resolve(target));
                
                if (source==target) {
                    resolve();
                }
                else {
                    let rd = fs.createReadStream(source);
                    rd.on("error", function(err) {
                        done(err);
                    });
                    let wr = fs.createWriteStream(target);
                    wr.on("error", function(err) {
                        done(err);
                    });
                    wr.on("close", function(ex) {
                        done();
                    });
                    rd.pipe(wr);
                
                    function done(err) {
                        if (!cbCalled) {
                            err ? reject(err) : resolve();
                            cbCalled = true;
                        }
                    }
                }
            })
        }
    }

    bg.base.Writer = Writer;
})();
(function() {
    // NOTE: this plugin is intended to be used only in an Electron.js app
    if (!bg.isElectronApp) {
        return false;
    }

    let fs = require('fs');
    let path = require('path');

    function writeTexture(texture,fileData) {
        if (texture) {
            let dstPath = bg.base.Writer.StandarizePath(fileData.path).split("/");
            dstPath.pop();
            let paths = {
                src: bg.base.Writer.StandarizePath(texture.fileName),
                dst: null
            };

            let srcFileName = paths.src.split("/").pop();
            dstPath.push(srcFileName);
            dstPath = dstPath.join("/");
            paths.dst = dstPath;

            if (paths.src!=paths.dst) {
                fileData.copyFiles.push(paths);
            }
            return srcFileName;
        }
        else {
            return "";
        }
    }
    function getMaterialString(fileData) {
        let mat = [];
        fileData.node.drawable.forEach((plist,material) => {
            mat.push({
                "name": plist.name,
                "class": "GenericMaterial",

                "diffuseR": material.diffuse.r,
                "diffuseG": material.diffuse.g,
                "diffuseB": material.diffuse.b,
                "diffuseA": material.diffuse.a,

                "specularR": material.specular.r,
                "specularG": material.specular.g,
                "specularB": material.specular.b,
                "specularA": material.specular.a,

                "shininess": material.shininess,
                "refractionAmount": material.refractionAmount,
                "reflectionAmount": material.reflectionAmount,
                "lightEmission": material.lightEmission,

                "textureOffsetX": material.textureOffset.x,
                "textureOffsetY": material.textureOffset.y,
                "textureScaleX": material.textureScale.x,
                "textureScaleY": material.textureScale.y,

                "lightmapOffsetX": material.lightmapOffset.x,
                "lightmapOffsetY": material.lightmapOffset.y,
                "lightmapScaleX": material.lightmapScale.x,
                "lightmapScaleY": material.lightmapScale.y,

                "normalMapOffsetX": material.normalMapOffset.x,
                "normalMapOffsetY": material.normalMapOffset.y,
                "normalMapScaleX": material.normalMapScale.x,
                "normalMapScaleY": material.normalMapScale.y,

                "castShadows": material.castShadows,
                "receiveShadows": material.receiveShadows,

                "alphaCutoff": material.alphaCutoff,

                "shininessMaskChannel": material.shininessMaskChannel,
                "invertShininessMask": material.shininessMaskInvert,
                "lightEmissionMaskChannel": material.lightEmissionMaskChannel,
                "invertLightEmissionMask": material.lightEmissionMaskInvert,

                "displacementFactor": 0,
                "displacementUV": 0,
                "tessDistanceFarthest": 40.0,
                "tessDistanceFar": 30.0,
                "tessDistanceNear": 15.0,
                "tessDistanceNearest": 8.0,
                "tessFarthestLevel": 1,
                "tessFarLevel": 1,
                "tessNearLevel": 1,
                "tessNearestLevel": 1,

                "reflectionMaskChannel": material.reflectionMaskChannel,
                "invertReflectionMask": material.reflectionMaskInvert,

                "roughness": material.roughness,
                "roughnessMaskChannel": material.roughnessMaskChannel,
                "invertRoughnessMask": material.roughnessMaskInvert,

                "cullFace": material.cullFace,

                "unlit": material.unlit,

                "texture": writeTexture(material.texture,fileData),
                "lightmap": writeTexture(material.lightmap,fileData),
                "normalMap": writeTexture(material.normalMap,fileData),
                "shininessMask": writeTexture(material.shininessMask,fileData),
                "lightEmissionMask": writeTexture(material.lightEmissionMask,fileData),
                "displacementMap": "",
                "reflectionMask": writeTexture(material.reflectionMask,fileData),
                "roughnessMask": writeTexture(material.roughnessMask,fileData),
                "visible": plist.visible,
                "visibleToShadows": plist.visibleToShadows,
                "groupName": plist.groupName
            });
        });
        return JSON.stringify(mat);
    }

    function getJointString(fileData) {
        let joints = {};
        let inJoint = fileData.node.component("bg.scene.InputChainJoint");
        let outJoint = fileData.node.component("bg.scene.OutputChainJoint");
        if (inJoint) {
            joints.input = {
                "type":"LinkJoint",
                "offset":[
                    inJoint.joint.offset.x,
                    inJoint.joint.offset.y,
                    inJoint.joint.offset.z
                ],
                "pitch": inJoint.joint.pitch,
                "roll": inJoint.joint.roll,
                "yaw": inJoint.joint.yaw
            };
        }
        if (outJoint) {
            joints.output = [{
                "type":"LinkJoint",
                "offset":[
                    outJoint.joint.offset.x,
                    outJoint.joint.offset.y,
                    outJoint.joint.offset.z
                ],
                "pitch": outJoint.joint.pitch,
                "roll": outJoint.joint.roll,
                "yaw": outJoint.joint.yaw
            }];
        }
        return JSON.stringify(joints);
    }

    function ensurePolyListName(fileData) {
        let plistNames = [];
        let plIndex = 0;
        fileData.node.drawable.forEach((plist,matName) => {
            let plName = plist.name;
            if (!plName || plistNames.indexOf(plName)!=-1) {
                do {
                    plName = "polyList_" + plIndex;
                    ++plIndex;
                }
                while (plistNames.indexOf(plName)!=-1);
                plist.name = plName;
            }
            plistNames.push(plName);
        });
    }

    class FileData {
        constructor(path,node) {
            this._path = path;
            this._node = node;
            this._copyFiles = [];
            this._stream = fs.createWriteStream(path);
        }

        get path() { return this._path; }
        get node() { return this._node; }
        get copyFiles() { return this._copyFiles; }

        get stream() { return this._stream; }

        writeUInt(number) {
            let buffer = Buffer.alloc(4);
            buffer.writeUInt32BE(number,0);
            this.stream.write(buffer);
        }
        
        writeBlock(blockName) {
            this.stream.write(Buffer.from(blockName,"utf-8"));
        }

        writeString(stringData) {
            this.writeUInt(stringData.length);
            this.stream.write(Buffer.from(stringData,"utf-8"));
        }

        writeBuffer(name,arrayBuffer) {
            this.writeBlock(name);
            this.writeUInt(arrayBuffer.length);
            let buffer = Buffer.alloc(4 * arrayBuffer.length);
            if (name=="indx") {
                arrayBuffer.forEach((d,i) => buffer.writeUInt32BE(d,i * 4));
            }
            else {
                arrayBuffer.forEach((d,i) => buffer.writeFloatBE(d,i * 4));
            }
            this.stream.write(buffer);
        }

        writeTextures() {
            let promises = [];
            this.copyFiles.forEach((copyData) => {
                promises.push(new Promise((resolve,reject) => {
                    let rd = fs.createReadStream(copyData.src);
                    rd.on('error',rejectCleanup);
                    let wr = fs.createWriteStream(copyData.dst);
                    wr.on('error',rejectCleanup);
                    function rejectCleanup(err) {
                        rd.destroy();
                        wr.end();
                        reject(err);
                    }
                    wr.on('finish',resolve);
                    rd.pipe(wr);
                }))
            });
            return Promise.all(promises);
        }
    }

    function writeHeader(fileData) {
        let buffer = Buffer.alloc(4);
        [
            0,  // big endian
            1,  // major version
            2,  // minor version
            0   // review
        ].forEach((d,i) => buffer.writeInt8(d,i));
        fileData.stream.write(buffer);
        
        // Header 
        fileData.writeBlock("hedr");

        // Ensure that all poly list have name and material name
        ensurePolyListName(fileData);

        // Number of polyLists
        let drw = fileData.node.drawable;
        let plistItems = 0;
        drw.forEach(() => plistItems++);
        fileData.writeUInt(plistItems);

        // Material header
        fileData.writeBlock("mtrl");
        fileData.writeString(getMaterialString(fileData));

        // Joints header
        fileData.writeBlock("join");
        fileData.writeString(getJointString(fileData));
    }

    function writePolyList(fileData,plist,material,trx) {
        //let buffer = Buffer.alloc(4);
        //fileData.stream.write(Buffer.from("plst","utf-8")); // poly list
        fileData.writeBlock("plst");

        // Poly list name
        fileData.writeBlock("pnam");
        fileData.writeString(plist.name);

        // Material name, the same as plist name in version 1.2.0
        fileData.writeBlock("mnam");
        fileData.writeString(plist.name);

        fileData.writeBuffer("varr",plist.vertex);
        fileData.writeBuffer("narr",plist.normal);
        fileData.writeBuffer("t0ar",plist.texCoord0);
        fileData.writeBuffer("t1ar",plist.texCoord1);
        fileData.writeBuffer("indx",plist.index);
    }

    function writeComponents(fileData) {
        let path = bg.utils.path.removeFileName(fileData.path);
        fileData.writeBlock("cmps");
        let components = [];
        let promises = [];
        fileData.node._componentsArray.forEach((cmp) => {
            // TODO: Refactorize non serializable components
            if (cmp instanceof bg.scene.Drawable ||
                cmp instanceof bg.manipulation.Gizmo || 
                cmp instanceof bg.manipulation.Selectable) {
                return;
            }
            let compData = {};
            cmp.serialize(compData,promises,path);
            components.push(compData);
        });
        fileData.writeString(JSON.stringify(components));
        return Promise.all(promises);
    }

    function writeNode(fileData) {
        writeHeader(fileData);
        fileData.node.drawable.forEach((plist,mat,trx) => {
            writePolyList(fileData,plist,mat,trx);
        });
        fileData.writeBlock("endf");
       
        return new Promise((resolve) => {
            writeComponents(fileData).then(() => {
                fileData.stream.end();
                resolve();
            })

            .catch((err) => {
                reject(err);
            });
        })
    }

    class Bg2WriterPlugin extends bg.base.WriterPlugin {
        acceptType(url,data) {
            let ext = url.split(".").pop();
            return /bg2/i.test(ext) || /vwglb/i.test(ext);
        }

        write(url,data) {
            return new Promise((resolve,reject) => {
                if (!data || !data instanceof bg.scene.Node || !data.drawable) {
                    reject(new Error("Invalid data format. Expecting scene node."));
                }
                let fileData = new FileData(url,data);

                try {
                    writeNode(fileData)
                        .then(() => {
                            fileData.writeTextures()
                                .then(() => resolve())
                                .catch((err) => reject(err));
                        })
                    }
                catch (err) {
                    reject(err);
                }
            })
        }
    }

    bg.base.Bg2WriterPlugin = Bg2WriterPlugin;
})();
(function() {

    class Bg2matLoaderPlugin extends bg.base.LoaderPlugin {
        acceptType(url,data) {
            return bg.utils.Resource.GetExtension(url)=="bg2mat";
        }

        load(context,url,data) {
            return new Promise((resolve,reject) => {
                if (data) {
                    try {
                        if (typeof(data)=="string") {
                            data = JSON.parse(data);
                        }
                        let promises = [];
                        let basePath = url.substring(0,url.lastIndexOf('/')+1);

                        data.forEach((matData) => {
                            promises.push(bg.base.Material.FromMaterialDefinition(context,matData,basePath));
                        });

                        Promise.all(promises)
                            .then((result) => {
                                resolve(result);
                            });
                    }
                    catch(e) {
                        reject(e);
                    }
                }
                else {
                    reject(new Error("Error loading material. Data is null."));
                }
            });
        }
    }

    bg.base.Bg2matLoaderPlugin = Bg2matLoaderPlugin;

})();
(function() {

    bg.base._brdfLUTData = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAYAAAD0eNT6AAAAAXNSR0IArs4c6QAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsTAAALEwEAmpwYAAABWWlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNS40LjAiPgogICA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPgogICAgICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgICAgICAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyI+CiAgICAgICAgIDx0aWZmOk9yaWVudGF0aW9uPjE8L3RpZmY6T3JpZW50YXRpb24+CiAgICAgIDwvcmRmOkRlc2NyaXB0aW9uPgogICA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgpMwidZAABAAElEQVR4Aey90XIkOZIkWN1ScvsV97Dffn+4L7MzMt19rnBXUF3dDDC4ezCZPQURBszUVNUMiEgyimRm/e3/+9//77/++z/+449//Nd//vHH3//+h65/aXLEF2wD/gXwb19k52iu8Zdi85BEY8DT3AiWNucIk5YhJ9LcxU46JNvHv+TOOMuJR/DYo9oTDLZVPUdZ4Ufc5oMCPuz8GT/D4RXVIizjAuf6KbrfbR7MG95dCPJ0XzteBqCO6G9x2PW7elXmrswEzpte1Z7kYR/dGeor84GPVdVUebvrfFbyVmZQDeLZfTjfc5yJC16eR/7keE1xr7EHdvD++7//+4//+M///OP/bvuf//f//J8//rV98f/nlpwm2IiRkWPtiz+cj+V1wI55PuM4P8wFlLBNNcsbKZgzmusVjAMde9v4DB7DkHKkbYuwlXlWuCu9OGOmufTdiI0bCAKI9pfXEQuZJsMv89Bo20eaT+jYOuub4Z/Sjc446xnWZwegaNsrVHLwx4WxWLSQ+Fsc92fOPsxHO7lPZ0IPej3tV/WKeJVzQDfioY5VOc/OPHNH3iuekTexbI/8R/NEPpFHxCO2yqcOe9f+7W9//Ouf//zjb9vH/7Phf/6BL/7/9V9//Ou//3n5L9IuEifHWi6ghF3lmOcgEsOuFznLVYsYRvRq+Q4xbLvXAb6JwUvPQP8LthEvmGk5V8SDb7So0Vp5JhVZHPmCknmj5nOTj90vPfKPsEDa7Piwqsn42ic6x91+M92sPppXZ6YP97u6qn7Ue1Z7q8d39nlzZnpV5yd/9pyu+FW8VvxWZlzlVmdd9f0O/ursd2eCLuuFz2f4fv+ff/6v//XHv/7xjz/+8Y/tXQHQO+vQ4bsBboEBFItytFTOnRwaLHxb/W9yau+3s86PESfCzqp55meC4oQdic5L1xPvACOMfO6zuSOPCKMf95kveJmP4icfFgD+Bovj/gajvj7i6XlL3Iec3+Q5To72I+HhfR8TVzifPNyv7n/3bPiz/qmX7F3vuzq/A/j88zjfn/sh46N6w4ilHLyB8B8JoI5FrfKBP83hcVli6v0v3A2ocKDjGSIPxaR9h4eYDOA8KXUvBMQRV+ZyX3q4lr4R7hg8sDJv1FwTcXkYvBGK+JkPcKxI4xh4o7NFPiMNalzhmbYi+4G3Ms+Kb+TNvt4zw7Vf5AeM2qxe4dDD54KWa4VDjfvRg3Xsn+LAW/t5H9SxKpydeeYCq3hmPO1L/4pfxoGHe65wszk5W9Xbeau+Mz7qKz0i7qjHKh9eWJFu9f7V58/mejzA3M0UY3PnuAfyEUf5iLXH07zPaKaaaqyzKI6Yi2eJMHCIkzfClEPeXQx6X5wFeNX3KRd69PV+GT6bETr3yvxnPdyH/KxHhKtmVo/6qT6qV+4j60vvka/XRv3ohx3LtcBm+pAjT6DqwZ31yDjAsegX+eyM7+NwFvaNZnIOuBGPHs7PuG/y3Gs04woXPiv8p9zVuUf8aPbsuQAXa2X+jA981CfqAQ3WTPcnxb6rkDViWd4aHsW2bYJI43rogJFbzcGjxj1bfoD8roRyGKsH+z7BOA88sNBHMfYNMSlK2HzoxSTSoxbhEeY+7KceimX4qrf6IGaP5iMJQ+UrBhyL/bVGDHXFle+1kaaqc0/kWN/h7efUvl6bzTPSooZFD/fWWiNuD53DYBNTDw5h8rk7B7lz73Dor7v7aI2xc2bzQId5I557znjga3/kfhfOGXmqV+QDL108A3etaay+wGfe5HPWkYZc7TfyJ1+9M39yWUc+8tYZqGWfik79NVZfj7UPajMd69zVj17t7/1FA0eYGiB+ynH9ah7N4B6NE4EoHCsq38Xu6jBKVcu5o/0Nj8gX2Fveb/l8eia9h2jmrL/qKpxPevsslXmqnMi7qs3OrJ7O8TzqVeFoD8aRjjXuzvEcvCpGT90jbVQHL+M67jn9Mpx138nn7nXmrGNnzNpsn2noRx7zmS/qM417eZ71cN+Kjhp6rmiUqzG9ot15mvcfARDkOwMYARvlU85hoD4aT/XJDNCN5vIe4M9WpLmLQYflM0aYclBvPa0x/VQfYahjmbxjCMJ+jfH1EOlZjWqcJfKmLqpFGPjt30eQIv1bjYayr84k1uKy31vWA3jUh/hIV+HMvOGBFc1e0boOGi6vEc98UZ/ptQ7+qcdhPORAtK1PcE6z7G0uvSocSp3rM4PnHGqdm/EifsZ1z6y/8zK/ld7gum/Wf5W7yo/mgAdWdNaMH3F3l/0x0n2XJjsL8GiufeKvWn8DgIEhwIcOP8thOOSgiKWmO9Ifh/qNFVlUNNpSf0Ex8sMw7vkdGGdBL84bzaF1xLNFX3qSH3lnGDSuBxbxR3hWW5kRHlij3qj7vFEPYhEfWNaDtZEONSyfY0dr3nf0PNOob+ZbOW+mBc7eFU6fL2hKn86Boa0VDqSRFz1ofZdDvfpFXuBVOM5Dnvk94WaeOiP8sd7gwke9M0/wsJS7I/tjpov4GZd+rhnxnUuPn6DBLHfmo679DgAM1ASxHi7KYTDjZHX3g5djnoPjyznTfCPw9wHg5fzvwvwcUa5vWFjHvFh6r8gznLWIH2Hgv4G7B3yxovvOcJ4JdfdjzfHMi/iKF3tEmpEfaljUZzPurOvZiEMfaVGnN2LnjGqqHekiX9Vm9QpH58t83uJU5rnDyeZ+28v9kGP5c7ej59fFCu+7uf78jvrzbNhXdBF31CfiZ/fMmd7SjOZCLerDGUYzjnTUn//tX6JB08jMsVku9uGhZnqvw8+xaW4ES9uIn8Yi/+hu+r/LMBMc4owW4REWzUAs40e4YhrDy/PIv3EyIgUrXosaoU9DHVNjCiOMNeyj+qhGjwqHXN1dt5qrVxa7J3iO9de4mDhHSj2scECu8N7i3O036o/aqI6eXKtc6rBnPeiZ1d3DeZ6zl/oyVq8sdm7kH/XI/BSnNz25K8dj1TB2jubkRLvyNI64wLC479n5saLrPwKgkb6jAOY5Wjg2y5tmM2s9DjI29kQdSzF6kpPl0IHDeinfBP6dAOjc45MYz6U9FAOO5Z8gMaPzZnM3HzxsK+Lulb2m3ogjvmLQUqM4MdazWoa33oeJ/0NJmSbDmxcGOZbOlmlAZU359GANuda9l9dnetaxo4d6s6a9gTmHdcfBZS3S+eyu17rX4Mc16gFO9zlM2mtcRco5Yuh8dZ+tMJqHvLc4mONNr5kf6rpGvcGrnFd59P5VvpyFc6zus7npj5dYhXuHz5mr/uRzv6ODZuVM2gtafPQ3AFrUP4tswjp2x2a5ail2zYkTJM5/nB8Gq2fFaN77DQweOgvy00JTrIBUnQfyiAs8WxE/GyXi0nekIceP1v16sDNnXu4D1UwDjusyjfqNdFENGNbIe2fkzxW15GVzo75SA5/erkMNi3XEM05WV5/OgXFPwFjrBb7JAfXFuSsciDIefbpxwnVe5FfhsM8KF5oq33nQRrMCx6ryq7zddX9UzWiGbA7gI5367x3X+bMeqEd97upG53naC3Ne3gDQVBvzQI6t5n4J8M08vAatL+es5nim/F+fcw/0zDDUsvlRw6pqnQsdV++xBcQ7dpBW+0AWeazimUfkAyyaEzhWVOvnDYoB1H0Q+GzAMs2o1mcAKVij+qgfrEZarSOOzqOcqD7qz96R96gGPtaMo3Xws/lQ44o4qKnXWxz4VrxGPNQ+Nduor/YEDys7C2or/BWue39yBvTCeqsHvKKz3ukx07zda9YvOxd0WPingLH+xA34t5lRoIFeNrDVHF4XjRh5H80Z08Nz4MAu/igcK6qj1DUboXE6kJ/9pJv4Kxf+XNImvWNwQ54Y8dviIY/Njl1koW/ar+Cj3pnPCB/VLmdDswNk7Y3+OoP6sQfqWFrbkT5OWKd+pou8qWWfiANMeejDnD2RMwbfl/IZO4d61j0H3zHPn3Kg9/4RRs5qP/WiNvJCDctrzHnunfXFA17hRDrHkLMPPRWr8lUT+TiGfNSXde4+B7XA6a0Y+dk+04zqPlPWAzi53Mmt+EecCKMnd+9FPNtX+eqj2r/zCcCbAMZKdmw1h1eoMdBSHaHFo7rXZnk4k4uCuSNdhgGPVtDmcj/Qvc3jLJHvSj/6ZPuKPz2WNBn5MMvKGT6aYaaBtsJhD91nulmdXs6b5dRFO7XYGSvPMc/BdczzIUfIEvYRKhg53If9uvMeUOO70U4puSfQkjucSFPFrP3lOWFd/TSO6sBmHNa5Zz6ZF/nZ7r4RL+IAi/BIr5hqqh7O81z9GVc45L6xsx/2848ANsS/JY6GIPJdTDUHb6pxY4gGS+kaQ1LJwZvOBJIs90Upw1Bzf2BYjmuOeuaZ8kQgIazaAoblemARP8MzH+LQeY+Rf8TPeqf41oDfAYn8Zv0jTdZLz7miox92LL8jYDNvrUd6eGCRRw7zvXqtE1ctYuhVq/GKt+q0Bz0UQ4zVNVvAX9Dt2E458Upem4I89/Ic5uA67nnVL+JVvTALlvIjv5115hGLzuKeys1qOoPyIzzyuDM3+0R71jc7Lz0iHWcjh3vERS3Dn9SyntlsGZ8494oe5zm/AYB6Q994E3BY9T+AaY4pjmkl7JetB9GYftiJq17rMw71o7OrR+YNvLp8VuiWMQiwtgNIuGPH4wjv5xbFKh9SaNwr85FWlzDThLg0lbB7hppejWdG+ZM6+Ps9AeOKzqE1xpkHZwcv4rAe1aAZ1VmbeaOOFfVQj4wDXP9WQOTTOHiQVeFlHNpwvhGPHGqwR3znVTiZF3D3G3EjftQfPKyn3vDI/N/wHvmjhhX1AZ7N9Z2aUa9sxuw84GONzoX6TP/1OwAReVNHbwJgrAtNdBDPwXUszAWUsLXSXONWfOnh5LsllbNnrU9eQorwFQxWfteOSbtLmPVa9cj4kT+4EQ6MS89ELNKgRl3XCPFSO8yII+06qzmOslgf7K+NnpmOzFE9qkE38qbvUw718IvmYD2qQcN6pv9ujvbLZlYOYqyMq+cb8ZrJ9qD8imfGgV/FK+pLLPNW3xkX9U/yV72zeYBn50UN661ed/rsE3zfjNV+ei/9dwAo7sXt5vzyPO9cEd/hNI0IJWzOyIlxl5Y9dF7GjfATtiWn/OhADDvj3lwwrWkMbpRXsEh7wg4T9dKYXMeIY9elPMSak+cYc+4RL6tlODxQ8zpxLyhP484PvMjDzg/le4wcK9Pt1a86c92pBaY9ydE6OaxxX+Gwh2voxR7kMdc6MfXQmPVIQ4x87RPqNgK5FS253OmJ3D/ox93r6kEOd+UCc67mymUcaare5HFXT8U0Jkd31n1XjsfgAvNFjHyva+5cargr12NydAcHebaU63GkcY7nkQaY8zRnPdIqL4ojDbGIrxh50a68/iMAgJe1gfxZK98BkcccGsc8J8c1lxzCA5QQ8raI0R8g6JprjLpr2JM85spFDKGfnZxW3ymX/lrTWOfkTKxjf4Qdh+FZjrTZr/hCQA/E6sNcz6EYYixqvC9xcqI+I43qLl4mvNQhPhZrJulzK292VnDp5zrNGWOPzs16dSbysc/6zziupzdnyerkzfzJUx+NWY98cDj/bpxqNR76aHGLofPn1iidA3zWh370yPisjzzJoWfVa/S6oif3FW9quEfzKEZv8BUf6TMuNdnOXlGfTPOk18gzq33njJxhdh+o86O9ARgKUNxeXcfGHpcchRmnVBeShL2vB87RPwjkKkdj1If5UcSGNfPeWbVH7wtVhqGmvbN5+Nc5+UtU0GFVfcnFrv2Qj1bkDz7nROx+M43z4YEV6Vqf7cHPvStiDb2wL/c6jFd0hyScnzXs7SzbPvIm/7s4UR/OiVlm9YwDfOYDAt+Ig48V9QOuXiOec6t+I89q7yrPZ0SO9cas8PE5Rt6r/Mh75r/aA3ys37UXZs+eS9SwsrOh9kSr+st3AGiM5ozbJD2BfF8nDkHbnVPKhSRhc/YcoGKIsYJx90LwqHqUT/kpCcQGZf0jvIpdZjp6hqNtB4/wqFfma0c6pSMfEFfvPdNkfcDPangDNHoTMOqVzZ314hwzXdSTnqhhRR7KierQvcnJemifjPPmHOiHdeqFBgJovwsXwLHe5sFWPWUktuz7HR7EI0/U1XfGd+7vxo/OCwxr9Z521VgX3ddP1GGm0ayoV+4HHu0NgJIBMtcYHf3bcWh04gR5xAGma+Yxq8OrwmFP5xLX/cQ5khMmZOBYvDfEEUZceRkGvLIuMx3ABT/MIjzD2N/nBQ4NltciL/AyPmvuAxxrVUc+tJHnbL4nulUt+FjZTHt1fAdvcfTe4JndHftFddTUp8KBJuKpT+MMLkm5kRf0WHd40GWe6gceVsR1XsTZ1ecZia3wR1z4+SzARhrnj7hv+MMD606f/wm6djnHw+iO/HlTncbtlwCdrLnGePWc8sPJMc+1IeKo7hi/nU2t14nrPuJ4bZbD98Q5khOmzZ1/1CJ+Batw2P7CvQBk7ntUjrCzKs4iXYRRndUyfFk3M9oMCxS2Pe0VXcbJcDaY1cH7aRzO/nQvn6tALFDauFXe6tlmvqjPOKs9lX/He0Vzh7ui4VnuaKC9o4Pmd9DxbnjOuzNTjz39HQCY8x0GG7V8S6KfyZ04m3Y1xzDQsGfLN6DlB6h1+oN30kjuHObkZ7l6ktMwJJtYMeUiPigI+8owEDgLYvoSY668CFNt527Exj3M6Ik6Fn0UJ7YzvmZTXPnksa41YuRktQyHTmvI1VNrijfdAUSvU9SxXAOMnndr8MBSPT0db8TtYVYH7ydxOIuekWfhvsKhJvOjF3gZB7W3eeo56qs8xFgz/qy+u3yd6VN89OG9rfSocnkO7Cua1ZnY565udT72+xW6t3rj+cBH/xEAnyBeIg+neW8OcljojB64r0tLuZDcD42k3Pp63odJgsgzoV6bBcSof9Yj4/r1Zjy0H3KPxvxiGHF5hKgWYVFPYNGM9M5qx3iNttqL3qnOmrKX8+kzqpsVJW2f6UiO+lILTlQHTk5W/04OZ0HPbJ5VzpIXzIPG2rPqN+Khdtdz5uvewXFAKfdv5IAPPPO+q/E7oU/WJ+JnXHpFGtTu6ipacL67b9avOu9o5pkHeuPjTwb4l4EY60UDY44Yq+VSkLB5kL+z54+qBzvMDbQ01khr5WsslFMYcYBh/cuKHd/L7THCUDBp51bubNUT5u4b9W9DJA8ZfzQLraLeqDlO/mqvkm4z9V8M5OzZLLM5VnU6Z6YFXpmLXpmPenwnZ/SccuaMg7rOnfE6B8FGynhVP+chr3quckf8fi6QjhXNUeXRA/tbGnhFMwHHWukTcXeX9R4VXTbfr9SO7nI2L+fGPvKp3PPlbwGwuRrTSLHG2wr6yRU8cHwHFyvDtdaIwj3lNDhAS7u/ahDr3NRg5/I6cD0Head9M2heIqa38lYx8nWHn7RpfSsYOPDBam9aWvCFtQJqR0DuQWO5eziOXOekD4X0i3BgUZ0YPCIdvb0+1G1FvE7JUV9ikZ/OGNV1FtbVj3XtB6zKGfWnT4XDObhH81R8lMNYvaJzoecdDv05M/futTXjLyXP+qKe+V18D2DmCRp9GR/S09bnFfRNb51hNAfb8x6qcynPe7EfPbUHY9+zs4Onvaq6kQYeo36oc/bMZ6anB3ZfM+2oJ2szD/QkN+rPmvsgx0fpdwBoDAEM1Yx/9Uox8JlHGq1zQGLTnIYQbMvSS64cztSE8kCPqE7M56Kc5+/5FigXOP3JIYZduexFnufEuWe+6jnijmqRN/gRfnfOUf+sFzWjejijgBKq3eVsfq5MdzIJkoou4lT6Vzg+0hu92JdezL0X8hUO9ZkfvRpvS/ijLeqyPfMjn75VnuoYRzt9UfuU98w3myvCHdP5UYt6OUaN4+7t+Sqf8+Dz3R2t9r+r/5Su6jvjZXXg+Lj3OwB6c4jhFH3VOXha1jiSVuroF33n4Wg3G4e00+59T8UtmdWVH3GBYfk1ZdyI90QPbesVNWQNJFkJNb0L8LF8dmCjGusf18mBJET7vipzgrw6K33ZaFVPHX0ivXOQZzz6POVAT6+s1ypnNJN6IUZz/VzQsOOBcxEbzQeO8kdc5VW8nf9Jb8yz6j87g88/46N+R3NXl/WC3+guRv1Qw5rpwcn6z7SZDp5cMw/wnvj0HwGwIcy0aZSDe+EcRA6jdXpne9RD9V6Hz+y/vF2jOWIs7aExahEnwhp3K/gnoZRrfdmr0n/EfaKHLxZmXvFpokTjPuRGPbSGONK+phMjhFhZv726Xh/50nPEYS2bDXiFU+WpV9ZTOdF9vdnLvbKZgGNVZnNeE24PlbPMuJF35htxga3wR9zIf8aHBqt6jzv7zCc266U9qMF+VzfTZv3Y+0nfmcfT3vCfeYAzO0PkAw28L78ESLKagqh5yhGihKCf1qh2IkoSaZ68CYC1eiLm0rMqh/UQO0DVgh9x6aN7xovwDINfpb/f2905qFuZBxrwuXxe4JEf8e/UsRfnjWblXFFtpqMWe6TX+lOOemW9wMGazc06uJlXhVPpBQ6W+iGv9AUPK+OiVvV9yoX+U3OseoPPlc3k90I+9+/SZX04B/Zs1ida+Fb0T/pnc6/0HvVHDWt0jr/vlOsl+nCeQ+dYyx08Gji8mkf9GmZGlsYzHjNlm3tkvAu+CSOtY57TJ8NZr+yRR4hF4NEgK2X4aK47GvjNdFk9wzljqwekAKKk7xVOJ0tQ0f00Dsb/STNVZpErP4Uj7ah2MgmSkRY1fgTSKfS2N2cZ+fpQqlnR0Uf1xEY7e6iO2Ej3iRpnqHorvzIz+eRqrvGd/tSoD/ugRhxx/yVAEvTdArBRDoOQc4D0BA8+zOm5msPnsEbYl/8XrXNGOWeAGedCrJqIE2HQQcjfTm758UA+ezBHmRjiCK9i0GOBr57EsCuOe2vrAE+1o4Qtw7VGK8UQY1Vr4I56jerLumMo/dGNzoleWOqLXDlaUxw8rJU6+DOPJxxoZ/OAg1XhfSdnn+r8uj7dFRMZSsImJ4Ve2V7hqXeFDw40FS7mon+Fv+oNf2oYY6+suzp4U4t9tLzu+Uj7du1Ob2q4V2aKuBH2lhe8+dF/CZDmKPAFCCzKgc84/nflockWemDRM+rJGnheb9gG+ifzkSbygI+uiFPCNhJ/O1lngHdJfwxR5WY82FT6t6Ew20Z2Pjwif+BYUQ0Y1qoXNCMt65kv6lFtqJMDRNrRPKxlfVfqFY8KJzoDdFicZ8RRHuKMS683OSMv1LCGfVE8BlYedNk5UHPu23zvMZrFucixRprV+eEXaWZ9Rron2tHZRj3v6uCJNdODk90TalyZzxMtvSse4GYz0Ae7eiHGx5/4pI+A/xDQFrZcDVHXvMJpGhFK2AdxT/hmS/XgeN6wDXzyJiD03Ix9zhLvIEVczOoLPKxbvTbdqj7qBWzFB3ys7IwjL+j8rMC4Mk/UM9/btaNZxRc9orlH2tlcqGPNPCocejTD7WE0KznYIx5w9fs0p9oPPCydbUdq55hxV71X+Stzc9ZIg1rlOaHHiE9O1ueJNptx1vOurjIrOKOzVj1mPk/OAG+ut33o92e7he0m/DKQk4QhPI8w5zTP7YFflFsuvqPca5V+jYN+CLC2AD493+IoP6jY+j24Rjll3nEI/y/rA4ZNW6/0OrzUm74RBvoFJ7AJJTycvzAC9A+9SNp2egGqasBVnWtn9ZHWa80b4DFcWAfpWFrX86CsNfKVM6tXPDIOcO2V8ZyT8YBjkR/NvjPOHPAzLmvcqddd+4140JBLffuxloMsBnyURrOKtPFW5oHvjA9/cnTsbCbOQy57EI905ILjfGLURzv11EY9Ip1i6qH4LFYd4kpvauDt/KoH53IvzSN/6nx3Heu806oX5+dOH+zs4WdWDmNy/s4bAkDQSVkOfKZpHCfR8NhHZa/N8tZv4u8eFY1Z9rTihUuKeDSJak8w+q7sUT/oM3xUG2mymSqaGWdUH9VGZ+G8mT7DqXviveJB7hvz0Ovu/uYMM6+wHoLX04A2oka1CFNnr3uuXMbkYOcHa5WdGvq4xnHle821nq/yodd+7jfKXVft7TrtUfWARn2oizD199j5nmsf12quOmq0Tgy80VIfxLf+HQC+02AjGCnmeeOFIB32y6aHU1dzuDaNCCWsjBNy3KOR2IvJsUdcYFg8557tjxnfuVUeXDMuau574psQKZfrjEpa640k4kc4MPZxDWpcM042D/TUIo56+C+Tgqcr81Zf8EPvwyiqoUSPrF7lVHnsBz5X1lu5FQ78KrwKZ+TFuXW+Gd+5b/Pht9oj0mR3Ay5W1GOvxHe/yqfXrBfqs1lHHjPt3blHuurMo7lR45qdAbzZPOB8tw9m+vp3ALaM3z5DQYeZ5Rh+xmn1g4QNS3tovFfPjyX/s2SfSZrd9hBfsRP0en4UT1xvflKf70JLkSzDoPN7jLjgAceK+BGeYcAzr1FtpvG54KWLemDOZc1x10d1/hmIatA/8aY+82Yde4UDHlbG5aw7K+exTn7mBx451GCP+M6bcaI6e1S8yMXe+Hg4TEfenY9A1kjj80A24qO+qlnlowfXqjbi02t2LvAy/a/SYqZZ72xmaHXNfMCteL3lg34zr+o84PXvAHQRgqCDw55jMMfCXEAJ+yWytdbgHS3neA6NY6t55LGCnbhHc5+BHOxYvIM9u54BODywnLuj58cRN5qlqYMCfVCP+gaSPkhWo6f7Ec96deMtWPWmlj28Nwz5tziy/qn2MGc90mstqgOrcMDDUu7lLDvlxBtx3A95lT/izWbU+qync1M+iNtQzh/NCS8s1wAb6Vb5b/WAD9ZoNtR9vhkfGizX7ej+OPP4lJYzjPqPekM/0tIf+3f5oNdbM8GLa+bZ/iGgyyE34IJtjo55jqaOhbmDnNZ2p3le6VfhuK/nkccKBm5fh3nUg5yoFmHgO+45PUd7pGkYC9zFJIBaNcNFepkZtYpOPVZi9daYHo61XEAJKen7qEaScjSO6sAqnBUe+3CP/FnLfLXO2H08z7wqvIijfUd18tq+EZ3r+Yk/SEY61Eb1yJaaFZ1q7uowyxMtzzLz8FmZV/uTz5065thHS3lRPNKyFukijPzKHukjrOIFjmszjH7kI+//DgBArvauYQP4X0H6LgK8UQ6PGafVDxI2LvgyZ49ZDu1hRZtL3jgbqXkexjON16M+Iww1noG8jh3m2HQ5X3PwyI9wxciDpoJn3s1HzRI/7aFeiLFYVyvExBtpe9A6MNYd1xpiLOdQu1fP9UrvxhHiyN9r6PlKfw5/7FEfGbGzIx6KOlPGoYn6jrjKgzbiOqfKi7w4Hz14piF3K87+Jo76MqY3c/bU3GM963AmE1K3ouE8mHNFd7eX9uP4K31Vszoze9MjyrU2i+/MHXm+5fP0PDqbzqQxOcCI998B0Bc7ii0/gp4fDrMctBmn1Z10+EebU1fz7ilCCVt5loPkHGLY9Q6Jp5gZOY/6Fd+M+wbeXzHBoDhK1OOAwztD7a5OtYiDkUremRY4VnuK7HnaK1+zI3/SP9MD5/2MOMqL5kCdi34z3iqfvtRF/sqJ6tQqD9iIi7ryV7jQYo006j3jNrPjwXVV7Zu6Wc+o10xzHO9058S4j+6THOxZf3Le8Jl5zGZYmaXqBc/ZXOxb9az4RV7A+u8AsOllB2vrcGy9PMs7UYJQI6CEl35i00PlA1zN39JwIO8f+Z8wESDk8idUaKT0P0DO7QQLIg9QMtzke3q8DpBEfUdeqGG9qdsdx2dg31HvrAa86bcH/lsWwHyxR3Y28qM6atBnNWrZA3nGVc4Kb8RFreoLLpbyo1m1vitqZ4q8qPe+xLsGTXvC6nlWogGtlaK5ZxrWM23Wa6ZD/a52pMvmrPQDZ6RHHWvUf2fMfZ56VPTVWcirelbuCJ5v+7kn/NsbgGggYH2AgxB9G61zNv5Jc+Tb9uWTcTYhP8EerZqGMT08Bw5sNsOlbkYVD86AHYsW6k08wppoe2CNehrx/DMP6ukXzc4aduWzp2LgRDgx1LnaP+98JPzxEFL1c53WwGU9w8HBelJ37e741fuWPwY3Y0v72djP+/DsUT2qkVfp472QZ57uN+Ku+M647Btc5WlW5akndXqujAsdlnJbsglcAx6xiwbAtlhXP2KoK44cK6tn+K7aH8lxX+JgeQ1YpQ4deKqnTjH4cZHPHbjG5M12arhHfNSwsln26tdZlUdfemQ+0JBDDX11JyfzUS7jkZ9yZp46I7hv++IXAP/Oy8POGM2wPAfg2CwPfZr7+aH99aszdMq8jxa9tprDa6aJOCsYuMNlA1japREeYRREtQgDP8NHXjNd5pnhs16sj/aZ92zmtG7GloYjVTih8AZY7VXl3Rih/BqazZDVHZ/llzNsAteAE2GudY7nzmfuPM/J0z3jZDi0qI3qmf8TnXquxLM5K/VobupYYx7Nxhr3jEOvqK4YeSM/8sllnu3kcc94xGc81rlffgSAgr/r0RyNZhyvc7jR3npAiOUNd/T06D1u5yKUsPXyHOAKBr4fJdKv+I64Ub8R32cjN/NhvT9XYpCd66RBIgsaLLHZgeNx5klypKf3E456RD3gTU5WJyerUw/eEw70WPTLvHbWF4859kxDT3IzHuvOp7fjntPXcc/pl/Wb8aGLOIpxlqzHCKdW/cjnntWoBS/jZPhIM+s78qz4gqOzs5/vsz7OH+UVr9lMFQ/MMPPROaueP8E3/BEADqAHPuU8nRI2/okzyRML3Me+3Iy47e7jsnIuRjMNRnBOhhHHbtcF6LKif4lORjvxoxlOBEsifuZt0kvavcyg4xfF1yez6B7M5qRmDWCkBU5OVidnVJ9x+tl6AMXXms3AOhTZHKscdo/81GvUkx7YqYn8Ih6xGb/qzf7gzzyVe4c/07g/+Fyrs1V14GV9Zz3Z44nH096ZnrNVzzDzgd93e1Vm4jnfno++1TODP5sXdXxcvgMAcbRA1gGiL1auu2jcQ3JwsdjDtXt1/dF9Zjk63OFEumxa+HPxvMyze/WZwK9i3XsLvN/Ih7pMg7rXRmfLerEPte65Us+07I39Lofz4eL1dzY4n/YY9ek+g1kqHPYlt3Ku0Vzux3ymYf8ZD3XljvjOG3Ej3xn/ruZtXWXOrGdVCx5WdKfAR6+bkQ41rpkHeFn/FQ9yZ17kYZ/NVvWa+WjPqic1Fe9Vz4p3+2uAJOqOZj6UY/7Fyuvwc2yW9xk24uyXDjtXAvdHybFhfhSHnKOfc6JeB7VvrvG8eWxg9AUm5G6C0fPkGuRYb2jg0/17AHRfs16UcFcd5nOcde5RXTHEWLOz3uG0174Io16chTXQsXQecoBrjJyL+MgHXOXx/oBrP+UhxnLdjp4flcMKe6g/eeAgjpbytT7jq3emG3n32makv8QKr14T46f96BudS/uN+pCXeWRaHoP1TA8eOdRwJx5pyeF8zLN95KGat/3oPfP9xHzsWfXGrNRw7uq+0gOe5PfvABBAkUMQY07hKd9I+sUKGq2HGuNoH40xZcs3Q3qe6jDfFjDWmWN3bJrTHOJtRb7qEXGIYVeuWaPcF2sn/tFcMQhC7uGkXPKo0doIO6zC7RjpUsMXQyzvsaPXewTO+XRXfYarlrHqiGHngtcnOG1GM7e0n5OzYB9xvEYdcF8Rlzzu0Ix49CQ/4jrHc9fQizzflT/jRlrHNB95n3opcTNgeuIcuPp7nOnIcz/F9TU54lHj+6w3+SNvzjDi0CfaqzNEWseyGZy3mr/lu+qzwl/hvnV+9Fz/HYBNBCFfOBjmje8EwCddW8PZdwN8Jng5Vs6FKGEbz/OoTyMG/Yln+8V7A/zc1F64Rz/U9bm5y6fOd/TFCntsxYYHxZFud7w+X4ozDqzb84x6VKPuSX96YH/TJ5uXPdg34+k8iN/guefMF3WslZkjPrBs/o95w1iaah+BMdpwqY7Eij7SrejBfeqR6Svzc9bMg3XsVb+K14ofZ6j4Vmas+LCn7hVv8D/tz5nQBx/9OwAsZDvIeohLvgFvfCeAPdy/TcviMaRzPI/O4pxZDo87HPZ2Lf2w23Ga5MLfAH670vkXbnO4znvAl3OMcHhzeV/gWZ2zgpPpIhx8rOxMe3Vcz2aituoP3mhG+nReMHR1FnhgjfrRa8SBB3lVP/CwVnx3xZpm5k/Pu/M/OgOaBgPqLJyPe0Bnqe+/Wo9BRjOgPjrHTDvTo65r5jeaRX0Yz/zIw171/oQn51jxXpn5rj/mwUd7AxBdEIoRzobYnfPGdwLUU+OoX4YB53IP4ro7p+UGWno5O/ycwx6P8cxg0hP9/TnMrDIcHqMa6qclZIRY0Qx75VoDLhaknfbMV0kjDmvk+3zAlRPVqe28LdA3wKxXvbrPJsj6KQe+Ga/aEzysmS/q3mtF41z0dD9gulY1yp95s49qgN3V0a/q4X2p/67+7JfN8fQc9K/6gDeaRf0YV+9qxfsTnpyXe7VHdB/QRji9uVd7gP93/BcbTPlBE+zezPOQYyRL1b7HztFcYwx0yrvDVxDVHfMcasdabqClF03k8zXZWuS9Rme/cKVVVIuw2eyZJtQZ2VKZLg8rmu/k5JN+Vfi7EF/IZ6PK+TFBlTeadsWjwq1wfJ6ZhnXuro/yxj0EKzp4RfwIi/pG2BMt/d7yeMMHM73lw/N9cses1XnJrfI5d5Wv/hrTZ7QrX2NqiCH/EycGwMWY7yJmOXTgkN/yDdD/GvI6OL5GHNSwWo8tafmWsOepvvE8hxYY+VEeYU1jZlUf+Hk/YFgVHDxr3YCGHQYrPsoNvQFu69Jzh081JJlfr5mRpc0PD8QJqK/XwNE68k9w7vTALFjtTYAYSLjX22P8QG50JirIYZ5xqzz4VLnaq6JRPudVXVQnj7vygc00qEMz48HrtA7hqo791GvVg9pbc1O87byru/3Fav3+VHzEb86j9k/vSb3eiFfvO3rNVOZY7ZN50gd7ewNQebZ96Gm+EbI3AdBi8QWyZ4uPmwl/3kyf6UzW0/mYwLGeI8DamnVsRy75Aac46767L+sX/ACwYfH8e3adD/iIi3rkARzLa8DoF9VZa7pTEs8GDy6jE+77rA4iONHM3WTCYQ/wM58hRwYY8nSgY6ZRT9Sqfspb8ZxxUcdyf2DZXaHG5bqZxvnweVtzpwfPgz3Ssz6blbyRT9VjNAf7VL1G86BW8anMw7mqnuCt+q54f4f/3R7QcVXun1zuem+I8dF/B4DFO8Zs4PvsdwLQU/tprrH79rxAcsosh3fKkWEjDrRCQXrxyrBGTvihxgegwbFn5Qw3+Sm9o4EBdFjRneyVa434rGfmrXrG3p84PZC/zgkOwH5ZL5+ryhvN756rXOpns4DH81GDfab7FZp0JnnOorlWzkXuyCedg+Jjf8ODliMvcipzveXDntg/4Un/ije52Ct3oPxVf9Wu9oL2ST/Vt38ICGbt//RG120iCcFvq/GYbPssB7XyJgA8XoL2pb/v4PfF4gFY2lDHZnn3lqBp8CAL34Hg3ITdG/gM8zrbTL1NaGn4HM7mGfWG1mcChpXpWMPrq81nBtSBZ6V+b+R4vXtvwYwDLtcTH2jZC37uhVrnHET9Thg0Jw4AW/Qc8cih9GjF9LSvcCF0PrCRP+pYmQ74TK/a0blbo+NBNYqPenEW13YNAydsDRTijOxLGfPRrj4Rr+oFH5/D/ape0HGup5708VlG+cqc9Kn00bN8qgfm0T7MOWd1r5yHXtlZ1CPjcD7UL38NMDqIm67krdlm6p8EeRDu3ldxxtjBwzrNYP6Rl2OrOXq6BsDKm4DL3ACOdfHe8AyDpJ//mOGwCbeSz6YEjyvSoBbhVR29s/0N78jD+2WcO+eIvNQHvf1NcMN8qCR3r4Q2hKMZh4KXi5Uz+Iyrmgpfj7XKh/bOjNqTMX3uzEAP7G/50JN+9Ca+utPn6fmivvTE5z/GEU+xKu8Nzcpc7HdnPmp9X/ECt70BcJF/wkK9f8HZ4tW8DemiBtYfOCPncLvZzFGni8dGoj/4Xg+xjVR9ExDqAQ5WNAPoJ/yUxGYZBTiXnh3YTON8+tAzqvvzRA33kRacWf27OW/2gxcWz4g4ukPnjHioYVU8d+aZSyybg3Xs2oN4RUeu6qs61dBnpI34FR05mX7Uk1rdMx9wVrxGPqte4M/8qp4VH3jpevPc6st4xZ+a1XOQ/x29OGO2V2a4fAcgM8PB1PBWfogqlxRxtD/mvMywAfqdhkvdzhB6GMc9Is0KBm62ol7gjnDU270cJGxc0X2h5niGAadfpMnmgg4r0xIHJ/IdaVHDWvEA/24f7ZV5gIM1nAnFzUA50DzyhMGx6DvzA51caj+liXoBu9Ovqot6VvpFuic9odVVnQEaf37UB/GbXvR+23PFjzPMzk0e9k/7ay/GKz1XzkL/0b7Smz6jGVDDR/8lQIq4z/5LDTwY6GClXEgSsu3yPvPwuudRQ+d4Ds0JO5ITdhhnGPvq/dEXe4Q7Ro/eowd7xVLSw33GzerAuabzkShm1I+0kGV11MQOabiqfSDOetGDDTIe6srtvGBQ8jqH5sFOLkoZXzm0yLisu2bGh841wCq6J9q7PW/pINoOFGlxBq4nZ6YH9qoPuLOZwMF62/Ntv33K/XHFG4rqHWgPxqu9qLvb824/9sV+t7d6aAw/zPV3BPpxIllXSxs1wk4emmxx4yeiBO4OszqIeOPiy6FZDv0dTqTLMOCj5f3BjbCLh5EsbfQIq/hnOs4wq5MXDTHTzuon70FS8alw0OIWLxEl8OAk9dKqd4UfcSIsmjLiRVhFu6JTrsZRn4YdJGz8cG7Jx/SRJsK8F/Nolggjv7rTY7RXvcBzH2qBY2X1vTp+pJY+Y/ZXlTruX5V5RA33ueJ6Rmq5VzzAId/3it41ml9+BwBFLL5r4RdUfmsdddYa0R6ieoS1E21a//n5pf/hz55eR9n923cvDl3bNvGFsxXoCY7XI2zIQfFYfibALGtPxRFHtQjLuMCx2t/oEGHUm1jjN9X+oLjXkGtdWuxiqXtNdSC3OsGDzJRmI49RLdMTn/UBzznU3u178kOyGVW80Nd5wE5+AGR9ms9WxzGYtn00lxLf1FZ7an/EJZ0NammzLPkczcl96pPNr77sdbR+vD31U73G2VkqA9OH+4qmwh1xqj2rvFGvrPbE+++bafl3APqflu2zC5rqJ5lZng3f8E2ML5hY6rkjX4+zHsP6URxytlZe/+r+FUWcE3Yc4oR9yUs9hJ7y4c/l99Z6BwMEULPIcBRnNXC8P3VZ7VJHEywzqvQOZM0KD7QdcVZ4yrVRUeqr1Hcj8XVPYeapfuBmPPo4f6Zx/syffbC/qaVvpb/3rWozHfRpX4ikeMuDA8o+8iFN2hKa7hVfmPyu3n4Bd85Bj+pdkZ/td2d4q//ducpvADBoO+QR9PzovJKDi9UvbQP4HYa9Mv7Ck3FmM1Cn+0zjdWiHGIpY2+Ei3l6MH0V6ImT4iWRJ6x0MEEBNOeoxqkGcec5qrGPvrwUksma9QX2LU/W6w4MmOyNqWJVzKA/xzBMcLHojnmmUCz7WTLOzzn2IVbXk/6r+UV/O5N9VI+77yIPclfuo+MF3xZNzVL3JX+nxSW/Ow321F3XRvnJG1b85A3zvzqEzIc7mIt7+ISCKCKK5xlpvgx1F/1Y3YB3cPUZ5+1HDIaZH5MfaZaYDSDVHc58ZslQjngi192F3wTpn0u+wblvXHKDPQ26Go57Ng8LKm6vIR/szjmau1MBxLbDWV5o7hyVwuWYcr0MX+QB3bsRzzsiv5Hk0iZ4f6H097U+/6GyscfdeFQ20rgMGLfCqBzTu88SDWvhiVeYI+5tRxQf9Qi8UjlX1IV9399YafcFhrPWVmB7codXe9Nf6ij+59KQfcexaYwyc3AhD/e6CH73hof7uWeVRp3xid3fOBU/G9Mr6AMdH/w7AiKim4PV8S6IvqGzO/aQhGO0B0SHPKzYnzZZEM584m6nnUR9gzvMchKif+l00gS/5GZf1aG+/E9GftJ0BHyyDd3B7jPr04iSYaWd12L/FmYzay9/drzfGWbfm/iZA64wrM5L7dL/bK9MBX1mRz6qH9lvVvtn/TS89E2J6j843qrlflkd9It8IyzwVr+iUozF9Ioy16j7yGNXUv8pTjcd3PSo6cPhx+SVADIKifmEY5l6EwZMV+Dk0y6P236VBb+8FAG8CsPRed2R/vGg2OMLABo4VeaWarRB9kaFX5Jd5gTvSaT2acVo/Go/6wwNrxJnNuDt8PY68yFrxXOJK8+zOMIN6cqaM79yMRx/fVb+iVZ16rnhA99Qn08O7MkuoB3iIKx7ohRV67aX+uOLXRUVv8O/6s1flDOTe7bfaQ/tp/PSs8HprFp2L8Rvz0evunND17wDQLNtBzobWGofJuJG/6lGP/ovowrF5ZvVSX/OsaMDx3hk2wlGLFryxovvMatE88OCPWSKvCGsaPGwrq+/V/DGbhYrsDNU6eDOPKmeFp1zEs/vhjG9zV+bQGaDDms29s77umPmKlpqo/6/yeWOWzIPnrd4t+TM/8lZ9qav6k4/9bi9o7/R72hN6rLu9d3X8+OQu3PET83kPzbPZ/8QzzCKGYqziKD5xt8S/zX2qbwaaaxx5VzH3KedClLC1neUgOaeERaLW8ethRBnVvhy+IvCxLs9nYkR+qNnARAZ6/8N26dWq8zo9TnpraOnhfN6qHKhOvc42LYOXrip/xoOnep/4wQFSrg4n8RM+bE7ziG8Uai/WV/TURD6orXq94ZN5nOYBqTDcyAt+XAUrUtte9aVo1Z867Ku9VIv4Tu+nPd+YwT2Qvz1X1APYnTvLvIj77MjR50/8VyGSjEAD7BQRu+QboN9m9jp1d7wiTYaV+nC4bZ+9eVE/xpQzx17Cgn7qMYoj/xEftUgTfYdl5pN5qS7qpXXGGe+CEzh2pvSJ9goHuiqPPar8Ko++0Z49P6veq/xollXsTs9Mk+HZTOBz6SfRFR/1gFfqUzB1r2g2YAUrStue+SopnVtJhTjrBX+dO+Oxhc5DrLLPfCseEefuPJFXBYvOwTus6GeclfNwFux/4lls3xo+OgCkGYnMQdH6KSfZCSAdy0vTfCO03jKAa+jNPao71nMEWNvubwL2wv5ImozRChmuWsTgNS0FB4Za5hnVgHUvJMcS24sfKKFGwJUZKr0418hX2pPedvUH0P76lTC83jhSR1jhZDzgPjcwrMgXuPMzXol7iLG5L/Qr3jM+6t5j5A8+l+uIY696uCbyvOPFGdTvIz4w1SZ6oEEcye7Ol7XxHp/wR++Z76yezf8m/hNmiM6TzZXhkccdDP78CH8HAEV9Xc/y0xAqPBqhTnjmFdYNtLQdhv7o5XVgvi6cC+CKZ3lmn+HoltWAY+mZdyR/DDVHg7B2WB2U0Hikg4B1xNGsrEc1aLA6pwc7ro+DUqeRA2DUD/UV7ip/xVu56IM1mt35I+7udj4rsbu62Xz0z3afP+JVZqMu81vxgFfm0/psRf2uJ3vP9qGniVfnpXylBzXc7/ak3ve7s7w9B+e6Ow/1q/unzsE5Vs4DLj/CNwA01R2C7BBa43cTVv5QqB49w9xAS2ONHMD5Uuqhfus14gPD8ntQbsaBrvNIOsw6jtzWao3WPiNtL34CSEh625c9T+p5kvVVZeNkhzqIFR9QZ+fxvswn7RuN3khm/BUuZ1jRKLcyT9SD2Ows5HnPlb70GO2RP/lPZqQH96oX+KOZ6Id9xVN1Vf8nPbQf4pWervX87rnfnsPnYv5kPnrM9jfvc9arUuc8538I6EDxxRvh6GJoQM4l3wDWvoJ9tJl3dICmYRMQghndN8oPaWtBuz7nhrY3AQeJddUgdt8Io1a9Ix7/pPGvCYITaSI89AO4LfZHnPn1mhyIOteAy1rXITjWqAbKSp2ePgPfXLb6UbxwKJbdOSzpTMSw/wr+ZRYFtoGimZSi8zN2zYyf6YBHWvprjRi9Mq3WEVOnXsphXTGPMy14rnduVAfmPPZUfntdKkCS9c28hN5DtavqOC/3brYYsHe178wePvSccbO66t+aK+tFnD2jfqjpuSIOfao7+1X5M95oJtTwgf8fQFunT66H0g08hzDCdkd53EjOW83F7RS6z6m4JbM6+J/kRN4RVhki1BXmz3Q4e19GsrTTKsFMO6uzR4X3Foc97+yVGdR3lQ/tHY32/FTsc3le7TvTzeqzPqrXmLoqRj73SMca9llduRrf0VHDXf1W4zc8tOdTP+j5ob5vxPTN9qgHuFjU7NmzR3q9tc+mQZ/wHwJqQlS3tyTH1r0874UgcO6ruZsd/SM4wnxc5WisPMU1Vk4UR1xgWNG7voi/s6/Ph+KZHznRfup1SvJe8OH8iLMzZDXgMz04WORdesisKWe3OPkQuvixsO30IzTikqOaVT49Ut1x1o/24BDbrn0IR7NFPPAzPPKg/0hHTubLOvesz0w/q8Of3hUu56ly6b2qIx97tZdqNMYMTz3UT+O3fP2etMeb8VvzvjnTild2T3qu8HcAQGjiI+j5SveDy2b8n2m416P8MPff4HfPytiXyzq8+5943omYBRSp1sLTrKck1xdpJ4PRrCe/U/L1yeByPyf3PDG7kHibAyHWMRxTgVo5eiC3cq4VLnqRr30f94GpmDztwdnEklC4R/1IfMNj1Yv8aB/NCn513iXv4/n5iHc0iGBPeopNC2d35/zV/I1ZPz3j6pmU/8b51O9JPLon1PARvgE4NQXLTuWQ5hqffJAMixd2CtyxUY3GWZPO6UHGPOMZPcOhPtWOBBuXXX+DTxoStz3DScnq7Nd6BaQAomXriSSaE/jJG0CwyEFp5jPi0Jp+mdcqD3x6Uot95k8utRU+udRWNODe0bkGPtV+4GK5x6p+d9kf3UtrjJ/4w+OTPSrePAf37zgPe2X70xkyX8fv3I97RPl3zR/1VuxT59Meb8Sc8/RLgGoMQr/ULWl5B45cBRKftIK3EF6bj1jt3sJzveegOsZfxKn6XvSbp2pPPUDels+9o9dH9ybjsLn0Qf2kOSVUX/eMprjGdIgw1rC3ekBSSGNqI4w13We8WR1e5GBvawuiv3ly4W1kf56hJ695LTys6jJ+hp9mI4n7ZG7SsGPh3IwbcDz4fahONVHsWlhSzx7aUz1Q95yayJc1392fdcWJVfv5GajHHtVar6iw8WdnSWS9ZfM+fDQmgRjz1Z13Mptz1Tfj67zsnXEr+Mzju87FWXG+aKYIoyarKc7Yd3pgR41L7xkYdVrv3wEg2Q1O+UbST7I8KAw1nuYb2b+YVvTwPc3DHGJbhNxXaeTQ03NwT/otwdxY1CDOdKhhKXdH4kfvRWHkT4esRhy8k+8hZN1nI940TIREaOaLusha15m2kY4H5boPeZ3TA1bOu5eRR57Oo0vEZQ37qi7jZ3OdegTikm4wJ/x9sQ131KM4661c9Xbcc3IzX5+DfO6Z36we9Rt5pbXAKIA4Tt9Tv86I71/Kj8PKDI+bmMEbPd/wsLEep9FMEcZGWU1xxr7TAztrijFmDTs/Lr8EiIJ+wvOcZty1rjHqw3wrrr4J0Lmi/peGB0nn0JgevjvHc/CrWMb1nsxPvqeEjHhfoJ4MRjrUsPj7G3t2fpzpo+eMDvRnnnHJy+rU47tAjTMjboKqJ7zJZZ+CfaOq7o4GJnd136VFHz0ncl3V+VXDeORLDvYnPdSn0u9ur4q3zsL4bj/qK/vd2SreFc6nz/irz1e5g+/m4E7w0b8DoAOgkD0p7ZNsVlSTSjxqVNG/xKmM0Tk9+PrEt3odsMDKdNJib3IQl3R7i/440p76dcVX0OoD0qDUXmTZOb867NHIBwzUuTJPfodmxmOdnpkfebpTA6yqu6OBf6pDYdJctfDCmkh20vb4RNtNEh/Wq7OQn+3R3HYM+wAAQABJREFUrBkX+JO+pV4bSb9TOpplViv1m5g8Oe/E+pXyG2d8ZZDN5Kff1VvnpE/4BoBF7niC9GJW/0vrpI28hOC9OAP2UY28V9+g0NT3yiCuCXLYYMnxd2B7PLU4JV+fnKe67vYVmFUvZDgJlTq42Uz0ieqsYUcfrNs8G5R+M0/lzbioc93RuQZes/OCc9FtwOoXmotHsXfYH+CxKvOTyz2ahTXdZ97us8rXXhrPfMjV/tTg81DpSaWJ7PQQaCnUeSiEZ4Sz/on96Tk+MdPM87vvaDbPp+o8598ZeCPHw9zBzSSAQkz7tT8sAqiHxqB4LrIeul8vHEHk4ZjnkEZYxXumzXxP+CnZuwZQK2Q4Z83qGd51M8JGLFBoN9yrPiEvBIftLsW7Ft+uu9tQTvyChbidQ3h/2v/ccb1fNl+Gaz/nnPJToqrPxVFLYBH+uSl2Z/b9Fb3vnk1nZnzX66fq+Hxg798BIKjv2oCN8nbAjcRvu5IbeelluC9q7b/cEdAE8bGcP8sh63702HzVOpox8oXcdS2nAXoVvOHj/sC4shrb9J46zCYu6dhE9pNvASelv7myM7OOPfPWGvl2HMJtp4+CEd95jUNQBIRmfqxHfNTEktTTnulm2pGODaLe7TmJChTJntEqvWkTecz0o3rkx17YR1rlabyqAT+aY9UHM6xqvO+qXs+dxZ/wzHr9u+H/bneH8/CjvwHAi5CgvyD5hDrOixn9khi14Krec/LaEEo8Cs6f5ZB9G2dr5G8Cov4ZBhzL593R/bHXevBVDaBeRA0ruNKGZ1rqUu1G8Dd+zVAeph4bl5xsPrFrIfgzLj0h6P874UREblJuPf2BGuLfpUU/7w2s9UehMEiqh1FxRR6QFtqHHTI/J9/1d58sr87h+qdz3e3rcyB/Okvk+Qb25hnfmOd/ugeeD3zsbwC2KHuCgPNFRQ7z0yXCYytoTbXgen7Sa1IkOs3zU8+jGHG09UnjBckvPhvg5696iW3tji7Nv54/vf+qb2Cn0nymQzjTV+4BHlzZGVgnd8YDvzIbefTHXvEmn/Os6sBXLf1Wep88NrPV3wk46TnAtq/OkPmI5S1P1Ud3pfVZfOdMM0/Uh3OhaI0trbQoc4azlF2+j/jJu/i+U/yenXD37Q1Ae9HIK8dfs56nxw2IDlXz9i18e3W4FnNEmM/XOQiwCr478euxe3xB194RaeM77LlYtjCrE8cO03aMhbM03SY1yalnxslwHo51mEX+wCsc8jIP1LngB97Mt/G2h8YLjAOIx2qtZv6cB3vrdeyKaxz107p6+PmUF/moVrmz2L0iH51lFM96wfut5XM/8X1zLp3Dv0PKu1XO0/hTsz+d6zv0b74GvmPeSo9PPp98/eH/BNh/BNCGYmUyodM0r37hRgvodF3yw1ifYO2lWsazOngRxzHPy7pNGP1XmPshx9Kz7cj+6HzWqEPeOAExgChve1YHPluh1kBLQ8sZB3Wu7I5QVx7ziO88eq9qZnPTT/09XvFYnnsT8Ecz3neURzN5b81HcXT/6K2a0SwrNXhm/dznE/29R5RHM/6qWaL5fiX21z3Et//Je4E3P9obAPwBYkN/t8rxep3AYG9vAlCv/skceLXBJj6YTSmew94xz6MRIk6EuTZ6ExTNkGH0K/XayNFzBi2W3suO7I8Vb+VrHHqbYchRky2ucCCp8mg/5KMYXMpQQ+NjJ5dwYMdSursHiK/5JGdMhzkKn5xJe985p+o9juZ2TiV/ey7tic8Jt55gNRnEn5x90PZHlN56/n/EYb55iPN3ANh8u1H+V4S+sDT217PnzSoE2eRrd9ol3wD9r2qvfzkVIxhsy39mH/lG2K7+eow42ZuAL9VXFOlZRQ1L735H7DExSeAmHnmzBmLW++J9AfYv3pmeJwhkLJ32ykwqIH/W/y2N+qz0pI7zMsd+20cu9Y4HZ4hmYu2u78jzqTf1d/bKXFXfu3dT9Xfem7O791/5v98N8PXSfweAL1gUWnwEPQ/uoFQLSAEUuBtkIktL/4Xvmotoa3nh2BhII06IbaC+ccm0Ixw1rMh/r0gtISUw5UPvcu/udg3QH4uvsT07P5Iz41FF/sjTucyj75iwxp3+zCt9yMXuemCrHpHPkgeG2ATRLPDmWvKkaNs/5YsWM28Z4xLePc/F6AEQzg/wwXAPpA9O8pf0d7yB8PUnB0EdH/g9gMvq4h7sFEsbqJjGJ9Ot4LVbuYksnfbATK45zXkkFU5V1771Z+Q3/c26dsCLaC4bzXyqnZKgUQFasVjhsnXTLAoX6Wz1+r48R0FQoLx+jlXDlRlXuKtzPOYXhssoGf54pr8MvvUG8Dx+x0d2KL6OsMc/AlDlxgKR/yXbYq0P4hMXPttbWH0Xe6pvPrO8tQIJ6zByzV6sP+oXaJ7R1daylYkh0TO5Fnn044BobnqO/CJd6yGNo//CpTdomT85d+rUtjG2pHmY0YkzmAMeT7hthoo/m8icEtKm76R3QIKRjrSRnhzsM6+qT/fcBNlrm5xlTwoH8z7xFPsewm92NyS/3Zu+37VnZ/3dz/Vd9/eT+3zXc5j1Ac6P+RsA3iTdtj+BCFf/IDb+ipB9s73oJWNnTl940fNLsBAFg2TtMpzdSvUZiWbBPpOijjV6DTTO9jD6wkOf3a3gdxBHfemFXf2HGhI3koRqNY2pI3HYj6Rkdy/SHnnCNDFIYLad7tm8Knzag16VXuRW97dmq/ar8j5x1mrvv3j/XjfgryXk+PiTBe7ZHwbUs0/mrXbc19RnI6iPamExyi/eTj5muLP1cx+ed60zXcd7sE9paR89w0mo1HGZeteqZdzPTeDYZ/6glTjJDNaupRU/9qU+m5917vDmqmiUv6IjN9KjVulND98jzyU/GASCyFd7BxItl+JZD5q80Yte1b06W9XvDu9XnPvOnP+TND/hdfHp+05/CRCHv7wo5UZaXQgh/5heaDsiPmji2qXcyJaG9wcOF2c76Y7khG2CSEcf3V3HGvX+LfoZH3rOSS/s3U9Bi6MfPyhl5MEa+aMZhhw7YORDvfYc8SI+sKpGeX08NtciG207ywKdwkR24sw8lPy2X/M+Dlvx5iwrM1MT7Ss9oX+rbzTLd2Onsx/Pgc4QQFr+Kx7cwKdeJ3d8T8/zYOZKia+JaA7voxzUNEcv8oHjg/mfPQLLlhKt1NLRFxfVagyh5xnWmhQefI67/ifdkZywZJaIE2EnuREsPVFnyUzr9xP5zTygecQRsYTRKB2r8rqgOCP48NZ1p5fqPxF/cqZPej+5C39ennj9JO3lvi/A9TX5k+b/6bP8lNfN23NkfhmO5ymqEcOuH39GbBD4DiGL0Qir8sWl8bYHep5ybYDCzeVz3LU96Y7khCXzRZwIO8mnhC82qFh6hzuyP86scD8QZ3q4zDwec6SBhLBNF3i6RvOTp5oK/6LbDKIfnZCX7doXnJXeVU/l3fbHoJvY51VvjW/3UZMtrvZT2Vu91fNXxZfzH8/Dr5rnr77v38DlOX6/xauO4RuAUYfRa9Zrnqe+vLXBn3b38rx5h+BX16gcYV+KLUoIEZxh8EuPJiIJTyMwST0OwkyPszTOwAh1rAElu5JdeDymPm2AnUQOslG/w7JtIlc4jbUHSCt93LSqpc57E1/1oc73zJ+8YZ+Fi5z1KfUjaXGv9p7ZDu9iJv6r/qNu4K3XxI86VHGYt85On/1vATBLhkB5+AdoStiNnab50L84V/MTUwkThxi+6DagYTbkhRfbXdCLTgAJ57oLI32/EjCfQZgTy65kB+VxdB6hld5UkM/eyGf9qeFO7aoOemrpdccj8nnqR73vPq/W2+wg3D2Emh3xqJ/SX2yptsO4OtvQ5BPFl5+DT4z4l+fXDfzY19HXiNOIZ8De/xaAq/x16bnzWx6QHPKcPo57Tp7vIY/gts/+7QH4ke7elzwgOuT5xeMALjwBJLzIRzWSZxz/cQl1us88yK3wLpwLsLslMFuF+x0NjKa6KaHgEU6cg4WWuXix0nv1YNEgoFetqrygxW0IPe8svFnJtKPaUq/BhYx6jGpL/QfkUY9RbWB5KcHnO1f2fFZmUC3nJsa84vMGh31XvaDjR/9bAO1nxBvaDnGcBCQ9FHPubNxyPGwr+uLi/J25P2qtaQFP+qs+jY95cFJ/ExBpdA7UPe9YVDDDiJJhkPY7FpKE5r7PRrBrCRw79FwRpz3fIGzFqI7SzAMcLPIyH+U0wfbQ/iYEEhPRi7yAoqUWR5onOjbIZmQde9abHDse4XSf+aVCKVR79l4IqiLpE4VVq947MvlB2GjOUW35CMnFjXqMasv9E8Gox6iW2IXwWz6heQC+1c99PA9avwrd7QcdPy7/EBAK/tfUOLV+jmg8FmyP3gQYJU29v/fxPDXSwi3Rfkl6Zlj6fB3TfuQVMOqxt15ogLUllbEfczYDvEE6Wu5B8Pi4j3nCDyt7re3V/bFzFSzE1IHqz2NB/vXFfTO680uB6KEzsOedWait7FFP1YX9IQoLqqzFs/6Zy0vtM/u/8B96A3dfLz/tOL/TOTArPi5vAPxSQar8wbzwLoA7f+UhFSBWobnqI5lbRBy0Uh/kWBE2LrRq+pD6JYoK/zHnMJj5oI7l97mj+yM5Mx41jb89VL7ArnqzB/Yn2qaHgR3cUtBKS2fJBHe9Mz/FK/3B/+QMOg/j6lzkV/ZPneETs1bO8xenfgO/63P0HXPjzwU+wjcAGKB/+3OL+S30hm851yg/edifQtfBTzGNUWi5eJzqxzCOad5iPGCJzw6cH1XHimPdSgodo2jbI0zK45Bi+Bwzj0Yn/TYHBpt4xQcHqPSb8VBvP5IQMwlRvizO6YWZDvxMS6/U47gj8mY+5KV+JAR71TuQhtCvmEF73jmP6sNDFUA+ZewPT8YqJ85da4xHNXJW9miOFf1P4+J+fFXPyLvlTh/NNdY6Y+/lOXnRDu/Zcj9qiDN3H9Ydz3LnZ76R3rXOgRc4+L8A4iN8A7Dh6YJYB/I8FG4kvolg3XXqSY7uT36sAJ/erweCaaPVWPwyqVM8py7DWX9rT/tIQcJh21d5YibhsL8X7+rcJ81vNLghSdvfLZRmKJHWJ4DtnfXWONpfY52JOHetMR7VyKnuoRfA2SfCaoNfwAvPVJyDWu6Uaa5xtU7ebI+8VzWZR4bP/Fl/qqcPdnjpR38DwCbh649FOIQEFK4LsgX61cAQfRNwx7tremANjnRSvoqWBfmbj9DqAMPadZoSknpJASHW7Dlc5Q09xUzCNkf1gbphn4GZ6kk73QEIJ4CsfJ965tLXKtEMF/PjbIvHu9gAKPULlV/gGx5fbr9BdNz/bzDpZcTf/bn6qfO/PZf6pX8N8PLsCgCD5U8Qm2j2XQBpEYaznj6X5yfTozjkiCDiAcNqc52SHZ89Rp7QhPgBhjVphDrX6n1R5wPMelJX5YG/yoVmdh5wfOl9oHbHA7qLz8oBYBAs9wwoHbo7dzdYCbbB+GOnFdmI+63zjwb5DWsrr5Pf8Hi3Rv5pd/KT5qnMQk76/wIAQf/Q9rwHX89bAPXiqYYES413pH+CZemkOzhtk4KEyhjGJ80pGcrKxcxyFR81zLxcU+GBg8V737PrY8ULqiqPXOxh78CIs0KDFer2Uvr4hgfMm08w452Z0mGl4HNLKQ0/NUvacFC4M//A7nZp5U5+2czB6+r2gT8s/GV39PBcP2HuXzUD+2Jv/xQwgugPhuM934IWB6LOkSdIsRYLIKEo9tBrPUdwLP+uAnHuXUPA943QOHaWSBdhbgez2UyuiXwjbB90V6OOZWPvoDyGPlJneOERkEaEqMl25Wk847MVeO1cFHM3A4c9N3qY3tGcjMyAKfZo4VxRbfY8Rl4zjLPMeGH9kfjs+F3nPXd9lkUzVx2faKMen3htRH1WMJ6R+4o24o7+XLCHclbuhHrt69hdb/XMYu9FnuIr56E+2tUzqgMDhx+X/xcACtkwFzwhJzB6f62E5PAs1y+KX+bnyD3O1T3T3y9gPdKVsI3UeHZhkXa1VzOG6PAeeY68WdMdXlh9bAIAj0YKnbhIZCnvkEr1Giof1a5hoQNnLctEExrL4e4eTur34QXm1nTkl9XMgs6P96xfyfhTQ23NP2g9PNqj+xg678VP+H/Cs3CUIeXtmTI/xbN4OOhWVF3GVY7GGX8Fr/hVOJWeFR9w+LH/EqCpkPKTnsbhAAdhyovEt0SR0c/Eqm8qRtOnV4TCsSrfcSCdzyu10T7saQYp14xX+lN60RSbUUcf7Da2lqZxya8426hZ1GfEf3Kmke+p9sK5Tn6SrJ5XpH+FyQ38znf6U2b/VXN8Z1/0wkf/WwDJ62kIw6B9EurBFz2AvooabcTKFzCVeKxfaNEXSz85aoxaygmGDqCmjzwdQ6+wWStcH6JetAi9xSLTCqWFKzwITn1PyZczPLGS8l48Hsmt8sGjpvKPBR1tLhs9UKjMeTEwQP209Ia3+o3ibIaRJqsN50ajISFz/QvnDSw/VxT8wHvnaDzbT9u/e77fsR9eVvj4+lsAOAVPYi86wIC4b2FfHetBL9WDTatvAtxqlrdGTpLug1JndQ4CLLuDHRw/dg+hRZiUL2HGz/BmcBSHHOlU5UECLla7jolwUt6N5PHkLXgW4o2ePi83nqJmzb7a566XeuCy8DoerUl5JP1oLbqTU0O7+1Ptr+TWDeidv/m6UN/qYOgf6TK86jviRf1GfK29Pddslu/o91aP2VlQR6/8HwIiQ2/8iAel8H8ERAvVacx65d+EJzfdI+ODPCh1uxPnlHTKrcCtPHfTrJ7hqq9wwK/y6N35PWDlvE/KZ/KRLWmELGHouwK+5vWa0cr038T9dz7bN10h2+Aqdb11te6rPUZxpsvwkdeo9pYffDIvfHHLVqbJ+MTv9FvtRT539vb9jTcKPE/7EQATNOqXB7An5xEGpdObgCEvsocAK+m7F/dH9+75EfRcRI55DuoJO5ITdvgB4yqMe361bgLqS1o22vZollamITiH6cybkhmP7TufAQqBWMvUBjSW2u6aIV/IPOvJbEuGeicfudgmjB2eesMoIVV7DAd4oZiM94Lzv6fFdzxvYY/Ba2l006HXSPBNte+Ya9RjVLt7BZlnht/tQx18n3ir9vI7ACjik0MjbQ/8uStxDpHt7RNLEx9GQqx6RF/lylr0O8gVzZSz4CVHvYTTPhdFDgy95DP7kCf24GGJdAeSxxO/2KRI6x3ZA8BwrsRY9VOP3rUWuLeq+qzJXMr9lfHoDMO5fvi5hrP/8OLt5+Qbz/WrZvzuvt/R75M9Zt6o4+PyBuDyWgKrf1a7VBuQUtLC7jMpx80G6MkPCdZk9p10fjz5oHQBznzNSlSZLeNnuPYKY/FGfcVnhbvqTT721afEjgSL5UUPCFf7rzTTPtnlf7L/yqy3uasvlNuNfh/h6Xl/e+yb9/3RmRbP+Ctm+Y6en+zxKW/40jt8A4Di6ZMU2QJeOMcLwnH9DX1QvH7ILnjToSg9yeWeebGOvdK/4nMZUJtYnPkBx+pHOgD9BcidsT9mPqhevFSoBHC3hug58qOcvsj7nCwGe/PcHpru6BPQTlDTbEjTHJVyr0NHfvOiEcFTt2sS9b+yrkjR/ixkM0EJcWwpLYeYyX0ibNl4JjgOEfWKsJnd3XrUK8Iyf3CfLr9/+mU469W9zXjcd1XD3tyruoh39460t8bVHtDwuRzNkHlneOblfOd5HedQzPl6TvLAQaxc1pRfjelFD/pX9ek/BUwDNmj5KSHjujvNvwhfFTty0W3w7JcDXRN5V/o/8Ym0ERbN1rCNzC/Szpn5zOrq796zvOS9mYDX1hZk5yCFe9ccwGov1XdtD9gl31Wfs86VBfup8E7/s+meRT4RFmkfY1sjPN++vq3/1jjqFWE+I/MVLjXfvfcZEQT3nc3TdRmhiN/1WdHxaLpjPHpwz0ae1VVX5TrPc/VEPKsrR7kau2eW6xd61TPmHulR++f2gf3yLwFGghMG1cKLsGsTXQJ3WQtA2lbj3undxc3m0UPlOxNsUDpbgbzkQ79kX/Va5eNJ4heF1acKvbBWddBQu79I7nnAZ7Z6n4SYzg5hWkzMfgf4Nz/X7Pn8CU/Br57xu/rjr6Vhfbrfp/w/4Zt5Zvh+g/kjdPjAXbe/BkgglwQViLZPZtiw8HntgFqePhRITjnlR8/bn0dPZumUrQAqVtrLvCzdxYuPo+9UjPxHtT5CidTZpwBSrPQu9vL+KKQlnXhQB0jshDEID4F6kL3sReHCHvXtchRlCAk75X9iMLyzf6MLeXzOxRfM434P7v6TvT/prUd+u8/bfpj1jidfRtjbdwDat9kPMxZhzoUmwLk3nJ0jAYXRfjKJCAPsiZa2icfKMbpFD2h+3SNKhFE5ehNATrTDE2t4jqMxuVN+c/x6GM3dWQEpgDp9FizPOmimXuw7vC+S3txlvmieN1t9m5ec6dt6flOjn/Qc4XNDW9/0omW7T171797jzfm/ywt98BLCfvl3AGZPNsWdJ4CEvRwGRrQ0lHRwQp6Um030Rbai6zNsQef3QDAlKldwkQm6h9F8qIw0NJlypgQ6xXtJHpACKG4wQMseZeKg2SdLP32+D5z9dzwyZv4pi7Nwx1x8HxBhT+eGJz7Yo+JXnUN5kW/UVzXZTMpRX/XT2DmaI/Y+mb/qRhz4jerRbCO+9q3G8FPPr78FoBU/+cwdWtME0NmFUxy6KV/V1ALb9K71XKWM2xfZQ9+xLbBjXLzJPe3SUMITJUqG3KSYwCf7KUcICLn87MR9Vw1qoU5JB0EheoZaFoM98gDt4qPES/HLWGlf6LNo0O7LGI1LxC/Jj44K5/nEXf+qO/kVZ/GenuMuIuzOHdGH+6rHSDeqsc+Ik9UyHJ5a05j9ot15nkeaEQb9zGNWV/8VruoQc5avNwAHAwX8SMB/kavhyjnivimhg19BWg4KDg1zL361PEUpLS2c5PPk8Hn1c3oyWwKfZpxyAkIAnTyzBDqs9OwDY2qH+uY+flCfi9eg/9j1XtVnSV1srvT+UoPnhfKslVZ2norkV3BePfMLB1ieZ+Gel71vnuftPm/76bHe9n7T7y2vkY/WLm8A9KKWYzgXPotxgE4t6tJ5An0AXeSd04P9nVGf61BI+eRxwS/AFz0rTfGEkMBfDbdoygkIgLD8DnZ0/BjYfQmGxZ3G3sju9P9qtp9dc8ZPfenz2i73oud/zf/fwOh3uZfvmvPTfT7p/7b32376x+Wp91O9zoL4DT96YMdH+AYABf5i4Bbuq/qZkx02VfXvhLcGorvz2T/6lv4xed/auXpmwbBoXEld1ucAx+7MubSZ4iBgFf128v6YSL8oCSGBv3RJBB3GpB60PjbBDqAaL1K9WpC6ZM8PQ35nKyY9Qx/Ndlv8bGZXZ/fuvDRXg4UzZVS1S3tKYZUv0jDEXPTUGORsZhpRx7yylz1hvpHv9Ijm0LNVPDknucwjb3JQ01i5rgfPsUjvfhUNfJTnHqiPlvJHPuQph76sMZ/t4Fd8wBl5ey18AxAOk00Qkn9TcHDGQely2BUuxZnmhJ+SXRlAtDztU15CSOCTtyfQ6LrjoXqNH3s9NtBpXox/6lwvHnFkheM/XW94+AzqqTF4nrv2Tr7iif/YCL8q3GjMvtxnFs7zXPWjGnkRp4rRA3uk0Tpj5WnMenVXrcaqz3DlaJzxM9y1K7zpGwCY9XcekkjYLr1zfBopqEZplzggOuR590gLnXEKLvQNaJjMfRIUk+ZxMT+8ix6knWxOCRm1/a4UOq6716IeuGD+T6bou7KfvDbh3ZlWela4PldF8xfn6wZ+4v1990zL/SAo/AFY9v16WsLobT80ecPzDQ898Ft+b/i84cF7/vqHgLYXT9m48ELTy2sxze9oL2ZFAD2Pc31n23C6hfMfY4c2J9CIlp6ongy5KGINLm2o39WlR/7Xy6BVyQckjq2CN3zV71vity73W4bNm/Tn4wedp8+Uj/3Ryif6v+35tp9e6FPvp3qdBfEbfk89nur1TKte+PyIj/5PAdOAnziRM+6NnNQLeRD6HHSthb0uYN7nUlHzS7EALOgzaoYXul8oFy8DLL3oFZhyJwSUuZ48RfhT6D+Tf+THobZdZxR4f01Pzqf8b49/0GzZHS7dyQfO88pcS4c4k7+r/5t9fqoXb/bpfE/1b80Bn7dmecsrmgcYPvZ/CRCdggXC8BNyQAig1qj7RIStTwgrqPExq0Oe92eiN4/7XHS8i7RAwny/WGzA0i9HSovIS5+gS120Hk65U8LuWKR5+68cBljyHO3AZx77vAy4f6bdPdcfMBNGuLPC0UPwjvv+53emjGZ/6+Xl3jwacfZhzlmJM5/t1HMHnx7EmFe8qCFXtahpTo7v9Ij4rFV84Es+e3he8VGNxvRc9YjmqniwH2fgThz7zCfSQAd8pgUPK/PYq1+P4PGj/Q7A6TfXv3i9MYfgfup0gL12mF+GBmGy1KNTVRcQHPK8+YRg7zCmQIt1OdAO83HUotXocwgyfoanfUxgKWXhDq6uyxGLZlMfbZLFRy/3cvplRicU8t6DwdG7IP0+yk+caXJ6vc4J9WNlzuANMtx5Kzk9uUOrsXpluHJmsXt4nukjnmOeR17K0Vi5Ga4cxiPuqFbRg/NdHpV5ZrOM6qMae1fPSx488fH1S4BEwNg+y2afaEFjDTEW8z0bP6p+xLzjDT+fpftUG2dDdaOM8B6+PKoJLC0PFupCcGwJCZY/Fzv67JHe6vJKnxvn1Bk+Ev+QmaI7Xz7vzbO80nth2O/o93qPzdB/lLZw5E59Y643PDDQGz5vePyEWZ6cI9Iq9vUG4HgZoNj+DYCVz6pNdBhUNk6w0oO+q72ow/5Ee8jh8eQ32Ns4x/lf+1FAcLa7Rw11IdhOMnw4jtk506f7pT5oOO3Vp/rhwc07qZ7Kn6Oq7hZPzvKtfbdhv6Pfp3q84fuGB57zn+Lzxhy/2uNJ/ydaPo/wuLwBQBGr/Vhg5bMoJ1rUQNYki7o25IqmCXAwNiQQ70NactahJmqzCZomOMfMC3Wsk9RAS3dB4ZE6UlsPgKdmrNZ391VltyapA8qqx7SpKzYmRQ97L/WskH/QXBylMnbE0X9mPKoTi/pET0vGIw4NY3pjj7CKv/oxdl3krb3JVx69nKccjZWHmP/RBo56aTzSux9z1QPLPMDjGs2gHMajXfuzB2fgrnrnsBbhqmedfNaAI+bOOnflEcNOnBj9FWfMWqQDFvWGVnXgYdFzz+JH5aRvAJrZxuR/7WYNLy3KRFPe0d3RtINtD9HtrY50t7/16a8wwyv2IcdAS63LQvqa0bXnB62vzWbIjxpGhv3Fc6H901XxyDh+/BGPc0acCAO/4q9axtzpwd7Z7n0y3R1f91KPbJ4RTj33jOt1zRFrnnlEuOqyWHXKifBZXTWIycfO2DnKi2pajzwiTH2y3jOd9lU/4tAP3wA0EbtsXzAR+tdNz6m5++3tpl95iIaq6OVcFbpyTi1PibLO8ZQ2JZz9ptmLfrDi4n9pMH9zP/V50/gvr+kN6N1PyTcJvQeC8BPH3Lh7JNRZPZF1eKQf1bpBMVAvjYvyCw0eT3yeaDHMU70e6InX/yTt3bNC1/8hIL34Ybyp/LsBwz/D6DIkBN02za03Dnd6sf0TLT0e7hgBK/ri+mi8R+J9Jn9ss24P7aldfX7dbJC3fygIdekh4UD5V6k9R7/gGpb6gnzjCV3qUbyDT3ii9Sd8h54oDu50qJ3c1ROtWj/1uav/bh3P/Cv6VnriZYKPP/EF1wXIB68jnq3plHfRXYAuzYM7Grjd1R1avrnxwUq2IGHpZezI6bHsZT4z3bB+FIec05S1BH648+zeai4FVmu08bY7YZip7NoyWg1Hs1cN622HzF80F2eaPQfklXY7y6veNsDb3m/7Ydynnr9a//QMT+a/o72jsZfV7efsbu87ukgDDB/9XwL0g4U5FPikSMfjk/Lw86RwQ88ITDRsH0kaNiXsyoi2+kuPkQfu5dZ3MLaxTncYmAdQeg2XwiPxxe0EjO7t1bYFM6UgXln6su66zQRvcNp3I7b99BwdJPZkP3KYd69JAF1ZQyKbTbxBL1KHTmw7JK0Wj+Eyb8XvnkE9MJ7nwN7yhpevijdmiuZSr4oP+Hd8qOGufRmP+lPHHVzG0K9o2a+qA097VXUjDWo+s/PZ13nAseixqqO2mSw+RL3cAhx+7G8AKiq6KBdxdnryua9wReNfUKc2U8JuHtFGX8w40nTfjH3mqWYjXOa5AGOXKf2Yiy7Vp4380c4vkBFnOlckyrCCGSh3VqZrZ4PhRsDzGi3VahxxM+yWDqJkJvahL3fin9g/3eMN/8wjw0f3VNVUeK9xYFR8TYzOFtVGM3ptlqu/c39VzeeY5ZzTecSxo5bVM5w67Ktr5EkvzoS9/RIgXi/tE92G+Ld0QRq+nqYEtkWTmZlwGd7VQD8cnA1sRz+sO9pdOTzn6DiX2gVgg3if0kHA2s425e7M+uPAkG1p9uRq3x+cUxX2wRkL6tcop/v8BTOd+r9wqp/o9xNnwlWX5gJp8Ies5JE8r3e1v4Pu33HG6EyKXf8WAKuDF1Dy2qjB8F/1vqPBNHd1dpJbNrdE1phnwL56Z4GVQ2+N2H2LhqD5WjpesY/3eJwvDbneLbqXdZfnik/MMfRcfD6HXovHf8vrDZ83PHj8u153dHc0v8ucd8/2XbonfaC9vgHgM7Oyw+nDnxyrX8wvo1yA4sHu6g77JscD1sLdpG23QuVHC6l+n2R/FJKEyvj2GHPM1ukaf8XgD3tWzji7g7BenOtj/W2olT599B58ma36QHl6jRxWVR/yIg9YsX7YhptyIh+thwYLYOSFnhHuts6p6KjhuZi7t+fKq/Sh/jt1d3pRUz0T+TxfZf+ERj3bGwAAfFI51Cs/D6eZ71FD50R5UXehXYDIPMAmukn5y7BM3CUpfSv8+DcB6fBf13E3ulhfgLvOC7pf0XNhvF9NxfWsruhK7/igr3tVfWa8WZ29V89+hz+aZVRjL3DwoZ/zZzrVkMtdfdgDO+vYyUFMHBziiLm0rpjirtMaNdiBs/ZUQx/1Zw/dEbNXpgEHa1bfWftjxEWfCB/pwOfH8DsA7U0AnHgadfVYpwj4KJ/gCd/te07dyaxX8+AyQE49VSb9yrZClPDUqpRs4uqbAPgNrwmDYG0khjsw0ZGU7Wo2HCAzyHG1biwAL/fIux8VHeKbemvLcL4X72HaKxxgDM48W31yhpkHJxjxRrWZvqKlB/YZf1ZXL41LOpCS1yb13NU7i8nlrrwIQ11xjWfajDvSfYem0oMzksud+Gxf5eMf9Lmz1v4hIJ3qeFEBSl5f+zOfFoNx6b+qWeGj7XDofa6UcmdGP6qYS+iseb6JK28CYFQaOyARgsfqNUPTF40emXS3OECPT/rHXXf05d68rlHLtLYwy6M+6QBfVzKh5OVtMLy2n6ynZ3uq5+xPfe7quw7Bwl12HQ8w2Vf5areqXeWj16c1n/bnfa32mfFRx8sCH8PvAHCAvgcvpgDa6ezSxR8I7vS4o9HRF/QhNQS1QTF+y6fQDq10pc+5kr47/sb7eONofqdveKrH7+CfzrjwXKYeehlJ/ERLyyced7V3dZwZ+x2PFc0Kl3N9l2b1/KtzrfI/PU/kjxnxMfyHgC6f6PVkRxHQhYeOWOSnhJ3WH8kHsKKpctnoRp/TOalf7ev9B/pTP+psX/oRzaadevJc6DOYTcdQicu01v6ZYyeo0Rvx9IBrTWjHc/BKNGdMZ3KQZzXHqR3t0MCbWsbs1/EtYLzyHNLPZyDufZynee+voMT0FKiHrlUuZyDZucAjjHzu6kkMe6RV7qh/pKW367RXpos01GWasB/IYkYtd2qwC63DytN4xh9xUdNe5HLPvPtQW0Aud9UAi/wjvfOYqy9jr3mu/h6rB2PnaE5O1AM14q4BTq3WNEadnP07AES2nd9ajhqoicaQD/lTgrrdiD/tf4x0aXMB1maf/aJlxb5xCkRQ3lojr6xWGPGd8V5qxHNwx3Aan3Lp6Rw91KimPI+p4669FVPcPbKceu7OI87d655XeBVOu+ztk4pyNfa+yGd1aiJehLlnxqFvtt/R3dGw/0UL4PgEfalRtO1ZbQWvcpWnMccBhpGjGjgRrlgWQ6sLPO1DHXfnZnnEVy5j8DKuzqF8xK7xfMZnXb3gcf0RANDjxUJRAJ0nMn6oC03IfGGHP1cyD8unfWGu0HZB//+T9zfKjeQ6sC4asWK//yOfe5ksflQSBf5USZ6ZfQ4jLACJzASqZFtyt6dnmEvxrf6hz6/GMfZJ1Oz6mZzeyCdOG+6/cZE/mlnv0ebyjtu20099ywK/8Nt62P5+zVudk5P8G/1b7VPdUz6X+VYn/VPtX/Ll/Zf+fr9O55zy3txLaZ78Qt+TXeI+0vJxfwMA+y++SWvqqS9XeMrX3jpP+dI82Uv8eBb6RevjsrjWE33n9ORj/Tpb7PTa858U/vJenO79YCa399T6hJd6Ar74ukB6MnvHeev1Vsc+3+ifap/yv9nxvzzryW5PuLpfT/lP7/ET/yfcN7s/8X/C9V3yNwDctdOo6ck3mAR6/mI78Z6u9pSPEXcwXRrSIi7mLlpfv/dgo9UMOMQn3J8tyHDFRwu48GH+T82ZrKXxf3V+7f1Lv7deqU7gwddkqj24+f+k7s2sf0qjW/Vk1hNu9NbTudPv+vLkPOG+0Tz1P+Wf8thZ8VSz4nmv/u+AfUDPxTr4wuv8J8lfemvtP/afXuq/NbctVMcf7nBIm17q141/aoF/ag435J+ex9wvolb+1XnrtdT9h+/p6WpcH9Hv9+zbrLin/vjNND43zqMXZ4FHPrNOo3zcm1r66M1M54sH7nwwYvTLcDDFzAsMnjz9RFz8iM34kTvbAX3kgxNXc+EoRp5qPup/BRAJXewN7kxvhuQJV1L4O1/nKt/wexv/A40o/aDrRr1zlkj/jZYp5vHY8vAaDmmf50q72V6s+joeL/B6wiV8fAPHS348/YvrQvp4ZhDcfF7cA1nefMKck/Ibj6fap3z2f6NDQ8RrFme8HT7rZ3PgElcces71POuvMHox4kn0fsS89hyNY55nfTBFuETHdjzvx/zp39v7/NkOzHjqje4kag99LP8zwMGIzU9eAMQ94WnAE+4b/luNdG/P02t6O2en4zkTb/F8QFtQPpP+4toeLfBZ5Umm/+JidQ+eeB1zJzO53GOfA+KR52Qf7I88IC/iNz5TLQ37JAVarJK23uieap7ytehTzSN+Ib/5B5aezDjlnvLe3JOnmie7nHr/hadm65x6r3je+/xngJf/+tG++NbEtqnxLb1LtdGSECRP+ZI/1fhderIbq6I/0KarBX3KYdaP4vGMY+KPFlvYcJsWlE/r39i7zXy052fjNPuF17ce3+p1YW88/gnN0xlP+E+4PPlPNU/5T5+Lp/6n/F/zuH9/eX1/sfOp55Prip6q9fH5JUAQuc5etKLLjCcPHfg7nnOVG99SdT6tJ95VWR7eaKR9q0MbL0J4OBqR0mz2lGNeRje0pNPGK9qx3+ieV6zWdxQtvRkfvdpd94H3mUQb75XJX8985R8WXnpsrn+pDXNm5c5j9tztdNnzNtPwFHvf82x3NPRmfOfBIaKN0TXqzfgzXsaPXHwzrnozPjrFeFyDLxFu5Kh2judwhXkuL+ep1oGjnD4x64HBkU6HOvZjDRe8ipteGD7gMaqf8cDhu0/kR640YK7Dy+OMB46Xan183gC4y2neXOPNusl9+q15DqRz3ni/0WjNf1rnt6bM/v+lN8BJn3y66rTx0So7pD0gjv5UmvPmvNXVWccXN272b8wcN9hXRzu+vP799OvTYcfLdsywm0/Ye6VZ9W6+DXCN55G/6kUutWs8p0/MehkmfoZn2BvvTDPzjjg1ES9Fx8iJziP3nufR64SfafAkZj6ORR49j3CI9GLteOzFWlxhGY4PEQ4xw/HK3wCoe/hic/wb94eedewhlwurd+Vw353mqU332yVPrynx2/3LgVEyHTltjA6i6fzinuB1Of4HHjf34E/2DTN/NeOVT9vllbY9fW+1b3Uae/q7HG9mnGpOeXXfdq9Ow195V189HH4xP9njyXWe+p7wTjjxvp9qTnl/ce1PPJ9wuSaP+RuAeNcWtcwOP6euty/H5MXQrPVokWbwViP5m+t4My9c65s3Aem6D3YRNfVoeHwDBp/2q4jJm/t8OPD0xeTQbkurl6SHL6+JW7MdmBC+0crurf6NbqpRI9zDKTe5B0+u46nvX3k/2eOvuH9xbae7nvKe7PiE+xfz/8LzyTWJ+3/iF5LAeny78MUGpUe4O54E4v6S15d44P1Cc1v59Dp8lvK3Ovf5hceLXTSW4/ej4r/aiQHEH/n67ljX+CN/95zOctJh/o3XVru59q1+cg3/hO7pjFP+KY9LP+Wf8v7K98n8J1zte8o/5f2F5+nsU95fPE//5Gxm/e/o2RMbBVeexROOdIe8/n+Qe6A59vb9D/dxSc3/aZ0v8Ha2eyj/0meQD0Uc9EUt3y+8t9It4Xz3I6uElEDfXPK5Nhv8xe2e2G1v4GOdCSxN59BXJE+JBp5wneO52dTUZ6546OB4pOeRvjDPnUOPqD/1WnHROkd5duB4XPG8d+Lp/Jj7zJmXNE95aOI88Oj3y9krr2z+2x39Gq5/B2A3mUni+Y9+4B5POOK3mbNfbOtjfLfEu/PiDqrT5gROvN1ymn+jW+w4nVcafaQSncl1Xs3rsWscJH/gEyXUPS4Hdda7JHiz9juzoAreoVvLv5qHLzGbnWFP+dGj65Ucfg5Fj5O6zzkhN86RppD4b9uP+Ife7uV5XJ8eUX3PZ/wnPDwyXzDizhcvouvAogcc4owHfsqLczJ95pXxZl4ZPvOMeKwzr9kuMzzzjL4zTuQxI8aZ3nHl+tA/NPT17wDEBWot94NvKI+5Ydh2xJM95C2+ztb4ovXHp3O6sCTfaN3nIP/HRr29j4fXcEB7R7EbxCW8M5qrBl+bN1eMnUE/trbVUjvZZamZTHyq+Yo/2Vurnfqe8v5tz5P5T67lxE8cnVPfU96p56nfv8U7vY5T3ul1nPqJp+O+5HqZ+7wBEKqP3Yufqwt9ek55Mmhzd6PrrM2Ot/YNmG78ufQHmu4mjc7RRVzU/vhi3iB5MHvQ9QVasmxG8kH9Az8urU/7gWf3Ckmd9UP/2+5hXi038448Ml/Dnng84TLirzVP/U/4v+LoHpx4vblXJ74nnKezTzxPOMxVPOGfcP4NL67j/y37cQ91PZ83APEqNy9k/e/nA0+mgjoM0Pw7zjyi390pqZEn3Jms7yr5hHSDw96suY3f6DC/LUNjjLdRN2DkU4nGuY2ieWugeBgPd5Iro7cTID7YEcnWm0U23o/8dkOLGX+UvaNm/Te7PNE84XL72JPbiIdqcudGHvoY0Q58gQVw75R30fr8E47mn/LgDrsJTA6eseXajENfOvVnHMe5L45lc+kTM44w+sS4U6xdEz1VZ/vh4TPIid9yfK+VV8YTpqNdojbuB09RXPVXHPE4kUdN36N67AKe8R27vwFwZXSj5zFMTSWB4/I0f8LfcG/7TPgpnILpxiP4VofLA/2NegMwfRh/5aOxB16iPD4HvvL8S+/HOyeCut/htUT5m2ubal7u4Dtl3o55HnW3r1UnlDxqd3WQ1xINEY7XntNXFO47Rh515LkHPhGjdg8wj/SFeT7jrHhRM/OD5/1ZHuc5D58YxYm8rM4w93rrI4/MO2IZz7HIjzVcxz1XX+evMfnzMX8DwCb+GS9scZZUTQyEUC6cb9JotdR+1Sx71z2fLKuByfV+tccT8YPZS+qy+WSh9f3QmNdnseNXvlrIvL/2ahe49LF5q/ux9JgIjzWF+PRPI1besRdrX9d7njuHPPZjnfFOONLNeN5bcZ7wxOWcesLP4s6jakRq389O+L/isO+v/H7lo71OvJ7sf+p5OvcveNcvAe6cS3/2m/rckH73di+SzNrxuvFhIt/EM4EODRPaZEbC/EBvNB/1d9mD2UvqsvlwxeCl8i/OL3y7R9j57b7d76XBG/1TTecrWXzxdN7Btfxj3MnOJ/N/xeF27Px2ffmccE7npTwNSJ7jk7knHM085aX7AYZ44vkLzokHq51yT3i/4qx2Y4ae/utPAEBQxUg/+YQZqOLtOINgUTz06n/Pv5lf2w+9F1vuW29nvdX5RvLQ2dwTUZbUX+yiITrNi3kX+N3jL/9Fv3Svh9efepxeos166vNf4Z/u8Wset/jE95/kaK//4rx/Y6/Tmae81X1d9eTv55R7wvsnObqGk3mRJ40+Pn8FcOIiTngxCWX/Bq+BHDhETd7+qYLEbV58ce8+DPCIxrEs51oUy5ntM8wKGt2LoX9Z3R9ddyRoFq4T9ETrW+Dj2NP8Sw+Xf/uCPXhxHQJ1Du8RdJc4Vr38wYc63nLaS49El0LF5Okfw6c+BrIfULbnwBmK60vRtfE2Q5/5olWUdsaLvq5Tji7llSb3DZ40kUuPeMKJHtKgJwrTiVz1f8WRv3vFWfSdI0zHufSJF+NvOHgzixh3WvEiFw9idm3SeF85PHBxdLyGc3XGnnvQJ+JBFC6vqFEdOeL6iRz1ZnvhFfv40fca/88bAHWFzlxc3Tgzan3BXnBqi60SkwGCd7rfUx6XfHLteCuW49d5IYtHv44F7dZy3WTHCTxaHZHGT8zRoFTFY/ZG6cY1QKN1iL0YnuiKHj8MXq5SY+MbtbF2uyE370yTYYM+FEu+zQqy8T7GZlIzh5hQuufAebjDoG1DnmIZn3295zn9GtUIz/+Ua8KM45jnJuv3bYVl2ojFWn4Z5nNOOWjcz3P60e9XHPwzv1Nst5v7eB5n0yPS9xh7uxpt5AkHIzp2ooNzovMZJzrxxzcAPiV8EblhvapJv8Ns04HB4VM0XqXtuB/VkKUy+YZGKAeP10UyZ+l1el+WJvfm0RpHpLu3I7uf3uM95nLdo+cv9ln6deM8+UaL4+764c3iox3s/pzqTnnst+W3Hba8ZnjCO+Ec7/fjuSe7/d/I0W267S0gfMHeOO3+evgVR56/8tr57Ppc3wnvhPNPXtvT3bU/13B/A4DbLuIgXvgkGqTwFpxFa7CqhfxOBcyW8ERj3if0us/bB5v1yOKtjiHf6uUjD53kJtG6CAePB/v8wvOxR1s91R3szJWnepqbWLWHs57OOeF3zmaHzju5ng1H7RO/Jcf2XfLaLr/g/MLjJ9f+w2v65T6/9PrFvT7x+OXOv/Q62f2Eo52u/xtg8o28fhUGXKaCgKllBKb8mxN90pqri802OIMzTPQbvvHm2gad3wgIFgeu4dk99vY0n+y4WWNq97rxq4ELHy718Y5FyN8HP9W+ntkGvdW/0Z1q/g3e6UzdthNuxgHzrzEwf96F/TXnmxnajb1nPk840UP3An/ls/OE4/tEP3x+xYn+Xj+Z5bqY4xNxr/+C88098n1OfXQ90unj+r8Buou6nBlOv8QDSmVrue0JZlET62z4jbMYuuSGXRY2y9ZyxlJ50Hy741tdttKvvBKfBMo2+E9hb3d+ozvVnPKWN/InJvmEzFqY457j8hY70Z1yIi/W7Oox40Qs1tJHLNZwMtznz/ITnXM8x1OY49QZFjUZJ8OiLtauUU91xNAQIyfW4kUs1hknw6Tzg4/jYPCo4VDTVwSD4xg8ONT7/xugu0kVXtFqCSfrMQlO8AiS/kxV/NbE7BOPfxGP+Qee3V2ahJ9Afe+qTQnddUzYS+gTHS6THWlP47dz3fjtDu6hvO30s5/cD67RKXGdZV2EXfvgeeuapfnYrL93ALSZdeJ/wmHcwFWRzB84CEPccWb9DD/Bst/ViLpYa+WIxdo59Ihcstee03ePmP+SE2fHus4SaM9pyvGlSr7inPSeciKfmhjWq6X3yIkrfuR4TU7Ex2tyIhxFMKL3yOkRwT2qt+qLS584039+BwBX+2RwUZbfqPK4gZnyjqWyjV/XbHjDtANu95XwgD/4m2bwuZF+CLzZ0cdLr/PNwt/uUMazxtN73nX1IvYPT/nu+Fb7RPeEq91O+T/hyaR8npx4/euctivP3z+1zy/m/BMev5hx+vn3X5r1i11+4XH6eXky68nzID8+Pm8AfJvVC4GUu768vuUk+yxHq6mzmnsxrv9875BbJeZ9YH9NWS7bFonB5sTWtm7z3ozdep8S/oXh3LLtinZ/ttwJYTorue4pd+It+EjTZh1x26xT7gnv/ybOP7XrPzHnZMbuc+jE4xecX3jsrqV9ah99zXy7z7f6X+/6y3tz/RIgG5ZYX+B0xe2Vzl/weu53pIAddx84WbPx+j/wo3rB41k++m/QbXfWmVonO065WjHxZkaM1Sfxj7y0fjBn0Ld5+iP01XUMGi/e7hs9XgxndLcCSLzUEgylaxZJ5SJIPDPp6Rz31ucoYzJPYfh6f6ZJ8cQg8rjEiDPfZ2f3Eh09YtVZUyk9YjaDeTuO+jqMIAqjp1xHPe8Lg9NxJQXstXHE97PjeF86zYqYanYQR0dY5Al3Xsbxvvg67hP7K4+sJ7/qoWZJMo7PUF+HeFX76xAv+uw8pBEn6qjRE8XXoX9V+TU5D32cJY73XKOcM+O434yDh+IJJ/K4VrSqycXVoabnGvWu3wGo1PCgLmxLDaqC7YvixCdMuzaN5kaqLfPKqB3b8Mz2Shu/628EA8zb0CG9+fyRZhjqRZn3+k2A+7zND64Xa1GXZ+K11ZnpEy4yNETwGGM/+7tn18Anei/mJ5yooZ5pMzxiXpMTM396RDhZ3HG877m8dnXGqZiE9kUZfTJd5MR6pYncWEur47jnV3fsR/5JnXHwJva5Suwe3foF6FyaLTruudMc9xyOY7NcXHpE9MSIxzryvO+5eLt6x0FPjLOpZz56OlbarBcxZoAr+kd9A5A875cOVSNkPFHAiQzt0UkdPE8G34nXwJF14R39iUFbY/tGxted7CDKbQ90P9ZgO41l3us3AYtdp/NiQx46yQ2hdREOHm2fJ9onXLZ4ovlXuWX4k1+WPNl1x/m2r3v81x47/5MdfsXZ7bLqr3raT+cXnJ3Hrv+LPXYzdv16M/4j9+Nk11POjrfrc1/ic+S663cAHHEVufrhG/lQJn2knVc4vCB3DFKJ1aL5ZH2jnv9RvPw4zXTm7fOReLzpEm/np3m7vrQ3A5lzW2AmMLxov3oTIKs3c20Fvks9ebFyec8f3jtuW9d7QjNcG7BTY37CuT6Zz75Bu/+RdxF0Hkm4DnnScv8s3/GW/dY8eW5XPqseO+84u758dpxd/8Rjxdn57/orb/U4K59VT/pv+yceO85uh51efZ2dz7f9kxl1kYNdTr1WO696vod4fNx/CRBmjFKEbzRDaf0Bdx/jOKy8a8TRaUDHL/TzaLyMc4p9DFuW7Jh53XSnQPA/9g6603F6pl+/CdCQl3Ml+/U59TzlcW/+ZM+D+3a8Z1vwhH/Ckd0Jb8VZ9U78d/oTjx1nmKEifLENfZklZ8fZ9WW54qx6O+1Jf8f5r8/f7a++Dtehp5i8NuxhhkNRf6UXb+cBh0+1GX+GS8/5BWfn8T+GHV9ZFyQ3o0zjwo1W044nG/VeYfY84cmo96treUh4N06mQ9/ioDHPAQ+a12Xzf+xtez2a/Vb3aMiH/BfjqueB8QHls+jD7MR74AzFOGzRGomtOuGfcGR3wjvhpIsm/tEr1pkPHEXyyFv1IjfWM0/n7Tj0Y3zi4dyYy3fnTV9az/HCgwjuMdOp75qMs+tHj6wGI8Y5WZ1h0uuw0wnnUnw06ME9znwzDj5RE+vIy+oMW/mIrwPnqub1+CcAUnHKq1P6AgWnNVUq7U1REsgAAEAASURBVNzQl13vqdAxzq13Mepj/Hv5jFsxliiqJQdvI1lK94rFs/amBKM/4TZZvTblJ/42qt67p5qi7/+YTNM+suD5Cvs67CumOeRHgz+fKt1z4gPceYtk4Hphuzm8sKqtJbc0a9+8V35LLxMOPIowA9hkt/QXnNUvPuJPjAvM8BMeWmLUeF05egj3SJydftd3j4ybYdJwZn3wGKOOvnDPn9RR51p6RPV0qGO8utfjrBdx6szX/cgzPj0iHCI4EZwInkXneO7cGS6Oeqv+rzjMIMrXD3sQ/0/y9eD8mqccOaSNJi99/s5/SrVGtOq1ceJinaPGgrdoRcted+8D8ROuBnS+igN/0YYjjc5gdEHbxzYPiy3/14TD693uZz5brl3DCfeEI8sT3gmH9U64/xRnN+fWF2Cfj7c+F3lw31Za2fx/ub+79t392em/6e+0u91+0T/x+AVnd627/i92kIfO01nOH/8E4PKrX8fZT6gS6mu8f50DNN0tMKkLboyzF8DiUy1WPrLe7cP4thdvUICn8dRXBs37c5Omrp/GE/+P6vx6XfNt/nbXb+dm+rLLyS+gIeWpob7FQ7+tTzH+FUc7HnuJuPgaOfbR0MlZeaz+JEB2K+2u/4128JbR5B6tZqx6g7+K5Kz0q56sVv1VjzVWnFVvN3vX33nv9L/on3jsOLvr2PV3/urr/NM+mjf8OwDp14VvlRD0RqHDJRFddcdK3kHldjqHGR0wUkk7jLljjTpwwDrYAAu1xVzhC26VwQ28UH4mTPgfQsjgC56aBo1K6Z7w0ShyvtE/1Wpmcq0OsdYuVs3B9Z94dw5Jcl20sr1WvcovhMpJfN1v57Pra8jJm6Loo7UiFmvfU3naF1jM0p4ZzPoZ7rcs9nd77/paCU70pmdrD9fFXq7zHL2wDHdf5RlnpYVPxI+98Jz1V9477awvnPsJR5ETdxOuPRyH++1+ePv1xzn0sh28Jy+0zlUOTxwdeFd19eHQix7innDwJGY+9Ij4ZvXnPwMUy91gewx9XQwXVGmlP/2pumnhE7Gv7eBPb4gnnEFwL+LsyjDftI/NKS/hC1p6o3kabaen0sr/Rv+Nti0ri6dn0Cx2GHiTIb/iTOxHeLHrSLxXx3tuZmQ+EYt13GbaP/gEn2rjkFbDJzotYk9reUkTdcxw3HN08LIaLOrAT7TOIXc/z9/00dRYzPzN4xtv+aAj+oyIURNXXPeOObro86R+w42abC/nZLljUU+PyHV6pEf0nuf0Pd7/CkDdyRdxBju99h3w6bJtvcynY4UzfRPR/PAJ9vdyscudfCF9jxlB+MGOg/wFf/YcDL5evLhWl9ev2qOLH1RfFVr5zdyqi5MB7RqAItXrHWfXl9cvODuPXf+2hwR2L259AclZzVn1ZFX7yVzGHOkhW3yrk0WqtR3T/uHst9qdbrp322ulX/V2vrv+v+ndLj1/Pmm2uNpz1fv2+lljNWPVO9Hvdjz10H/+p13ubwCYEL55qKyQVKEnST+hF8pragC9rLnN8J5m9NruZPaG4cZrQMf7wp/k9I1F9bAdPw6L7Cl/YTVt/RMzsuEv5krSzwP9oOsG9+SEt+SUZu0vPmGW+rLSrq+td5xVf9WLd2TFXfVe7SjDdt9W3m97u51WvtyX2e8r7LS7/mq3nXbWn+H9WkiS+I12dS2MWvmvet96/xP63Yxvr2/nf9J/w/G9r/8ZUPlinX2fS/HiUPFMR0+bpeKCa4NZTzqdE87FvD2m1torbQT5Zu5gseHi3DWH/KoTl9MNABbxyYxo8w9o/bKG8TSeXOtg0Iri43+MmVGEMW7W77iIyU47/bJfmt/uuPTn+kRKdte17fQ7zk4/e4GVr85MP8Mv1e91O9/Vrt/0dtpV/+09kqfOSr/quVafVhk3w6TTWfVO+iec1YxV78T7hLOaseqdeP+Sg9f/6rNim/XvFwXrudjZSXSDJul3G3rE0hi0qq0nXezfsMLPOAMveNIbdAkHnuJwEq57eV51CX/wywppnujgEjPPFfZP6+Iuk/kTeFBXDkTiwDi7lYNUxQAEw1DuqLUPifjQI9CHcrAciouWQINexQnnJmpA1/ZkZE7gkZRU6IgJJYV2fPrE1CQB4SvykdDSHnxi1IEr+nE89sSjT+5aMOfQByOCe6RH9J7yp7jrZ9qMI248K733Mq284BCjf+TEvuvijFUPH+eAeaQfvZ9w3IP8+q8AqNyt5HoX31/AejKSKmfSq7C8dTIO/oqh38umj/3BkhkClXexACvhGSdQL4Ee4RbClAM74S41xsfiKNreWz4znmhk+lbHQmEedrS3MQhCuZQP3KG4ZAk0+G37Iiye2JU+7Rlo6bCTilXvqC+Dl3sf+Ys0Oavd3/Y0aqad4alG5HZf0BHF9zPDnaM88rz2/K91zCIyz+s0F1juifd22tiP2ljD9wiH6D1y73mufqzRZL2MG7FYR5/YX9WrHnvCIYITHfecvqLjnsMBU+Tj+h0AvjkIJUdFRF36N8pKh75F13q+eiNRpTZftWub9YWFRiih1juQvan4EK7s9PcCKvvBfXjFj8ud1k/3wvcL3ckfcTMmizzdWe8N9q3fSr/q7XadaWe4++043/Tfat/qdF0r7X+pt9p1tedK939TT7vqrK511bvU3+l3/qv+qney2+7ad/1/ez7XqHj9DoAj2i68alLqmzq5JOQ1tquCQ69bc9W3RmdcnxGlHylDXXyyF+7OOZgzcHthe9i1VVSeEx6qTRvaPR54D6KD6xv4FE/nfKGrK77cE1n/7nJ4Y7uOvYlqbDym2iIdeonP0Gdmi8c9Ec17pZP1q36b8Urbrmc3O/U+mJvq2sxZb4Y/3rHt91jX9vsr3V/5vr1vXO43+pV2d727/l967659N3u3+67/F/541r8CiH/XzneY2wstKu6IfdPqkDgBp6yx9cG6joQZjRB5lXbI6dqeMMRi8LLOmC54gz081EMTMEQ0J1yk/5RG8zazaLPaENU8vK7UB3DiQXuYGYrZL6TttLd+A2qY7KPRN13bZ4bXdmnO/tRkpXvUE9n2Xmm106rvVpFHzah4/+lrBhzlfpzjuTiZJnKo4VL7DOUV10MjRp5gxzyXnpPhzBYn62c4GvEzje8T+/QyXLM4WV9YxMXHk+gcYfGoH3E0RDR4Eum7Hkwaz+GAEcXz3sxbfOe5Xh46aJVH/kwLLo0OvkT61Bdr9HfMPVyr3D08z3jed//hXwKk0aNUuLXUyrqBv0mg539s3rFuWiwTX2tfaeG4N338ah184Cie8rpm4dU5SgJvmDMQWxH4GaVjT7iI3mjQPo3JLEHbk+iiZuuTeGw1ZUjnBH3H4yKtnvVnODa7PrwYq04P20+oj/LVrDZjp931tUWz+izUMAeiz6wG1+WTu4/n9InxloGjmdWDTqQBuNSu9RzvVYRPXHHpwSWCE8GJ4IrCZji8WX/AVbR7Aa5IPvOiryi512g8ep9cfc+z2rHI9R65c7LcMWk44DGqn2GOR4+TGs9TbuRnOnEiD0zx8y8Boi5RT17/WhCrFAPmnNYvUD1RJ7BjF+V6NN+MUzUzb/M5ejPR+OkeCy9rjWnZK3tzMpKsCtdhnXv61PvucHvCb5Qn+9zEH0A2x2cx89jHPE40N07T3/BwEbt+pTcvl650s94Nb7433AeVfNaf4ci/6WdaxzzXPK/jnwLM9kFDhOcx9rz2XBrVEcMLnAju8b/S+6/s8eTezHae4Xh/0/9Gu5u/85Z+x/mmv9OezI/XeP3fAPXKWNz9xVTDBGcvxNGk82i0WP1KXr2qUSBwRaXHvMDojUzesan44+bX9kGTbOHV50nG7sqHhoDknPpKuuAmzp9d2h4Kvt6JJuVk4NPd3CNotzu6tuWnmimvNPTmbdaf4ckq3WT2R/fSrPxmvdmL5Vu/b3Qr7Wx/aXRW/Vlvhl+OuedKM+vN8Ld7v9XN9pjhq/vwl72/9F5d69veyb7irPx3/Z32v6r3vY9+CVAXUu/U7IVOjq1naZXFB7fo+U6U9LuWAYWz/alcPjpBHMr1tV4O7x7tOm4zo6NxY2tam0b+XO6Ur4ZpljxvFk313l6Ei1re5lV90t5CTb/irbzf9jQv02YYu73p/VqT7q0h5blbzUp17cJe69rcZnMLM98ZPttxxp/hLDLrz/DZ/BX+zayV7z+5I9fwdp+/0J3sBOfNvTrR7jirudLu+iecEw/fs/9DQHwvr7G4UIvc8+be69ZDoyugR3Q9fyLgmHIdeopRG/29X7XSK9F+bUeVOsIjnz3TXlWVh7YHnJsHPGKYC+z6mjdf+suIJ3FJbk3jbnfGzzRAsyhqp/dkxs7x+hNu3rqhPsLzzxKjZOCMrU+VkBKo8sGJHxO7DwKNYGmnO+Z5J4wW1c95JzleM6769HbPQedh2iJ4gHtJX5FczZ63pNdNGesGDyFyVINl0fuDkenA4eLjuHL64I7RQ0t0zkw3w90DTpzjuPKZhp73PcfHI31FPryvHBwufcdj71e6J74ne8Wdd/4nfeeQK/LBXvGeZP3IcS05OiI4Edzj/JcACyv+RC0hL9R6VeXFjQH1BSfo+ouQxO10LNbGUSvl1SWacBYaJ+qhV3zj0zkSzYwwDHsDT2Wb2ehrxPupZjp8cP8UB/6s8hGV7EAHf9BTLPaEQsSnzlShRtPfOJ38SQYORdGTfphXBk70/oBZQUqcaqwx5ZbG7K8XMo0swYk25t4T6eX1+yyfEfOjPUyU8X2W98ljNLua0genJnZ/Acn9uPGakePRu3smXNd5jgcx9rz23GdFHC9i1s8w+B4jz2vPXaM89rz2/InujW+c5bXn7BGxp3XccaWPvaiN9VO+66XlI/0lQJHrYUr7JtvCpzcAF1wh6axn6TXZAEsvAz0WfXzz8Wleffcfeq2ob1RSc2OH67POmIbrGZtWaW+Vu7ninM4Wl3O6h/hPuIf+uizWRlLjwaxUJ/FEO+UPgy/97EXSqTO/2d+3z/jynPX6T9Unz//KJy7e/GZzVzthlWkrpodk34y/8vqm92bWTPPXuK5zNuNt75/0e7ujdDqzXWf4pXqu2/mtdtnNPOn/1+f/Yj/dh/z/BsgdImpa+ybB9wpF/9MAqD0WDS/iJr/abI9ZE4WyW6WJ/Gn05AJ6yRzBDew9tIq2YNoPHJeSDzrzoz+NT7gyecIvXPY6eZGsO8pfB+FV7R8ne2G3NJhol5rWPPLfGT2YP5vX8eS+9Z7tkWFqp3gBs+cv5R7MmM6ZzV/gK6+3vdV1zXo/w2V0+Bzq+nSezl5p3vZmO8hPZ9V/01tpdvNW/be+O91qpno6K49Vb6et5hv/ncev5+NXfwmwvlALaSgv3Cxeo3rhC6NDJZlqJDadpcN/dVBn+EM3v0DXCYn1DLvUOZ9ejbNrcFLYSa1sD/zmTTcteeIbGGP5lD+qX1cauzxhry3fzYLWW7O8+0+0vT8xGPoTj4l0gDOfARvY/cssoM/xm0EDZrNnuD7/sjcYsosa1fqcj7i4OiucrxX/Uxf8dtpq3h5mmtlstLH/1ifu6j6xx+wVDifu5xrueTaL+4pPjNLMdHFmrKOX6swP3kwvnGuAS5z1wJ0X85WnuLv+KYe5HmfXKg69k/nu6TkejsV8xgHP5tOTV/8lQBUi12MMMMX+E3+jEeCoRupYpvO+dLEW1s1q8f4h9Q52J5wq4QJLcawJs9LSfNN+BJ/yo35XB/9QztXHxO8svh2z0s96GZ5iGdgud9aa4f0ubQkX85DWbZVUTSKMUKwHE3wiGPDu0ZNrvpXVQXXE1HBsxqkGm4foM9C9OTTuRaTGnWJ9d7gQ98k0M0xq17o/mthf4a6POTpw96VHhKMIjx517EWN810TeV67Jzk+8LKa3kyT9R0jz7xnGJoYT/iR4x7qceDNsPpLgN5EqBft2U/14HoB7C+CzaRiHRz7rmOOPjs6vSQ9x9uWqz0nNJPbGwzj9BSfDoyzqhUcFcZrYz6Q8ybcqnFe4odvj/BPuBI94T/hslDTzH46hOaxSl7MOtVh7TNr3ho1bO7fzGP2d/gZP8O0R8dJ2i6Uce8nuP/k7D5PPIYd3aTlJ15wiIlNhWKfmlhvVniuei/sk+FgxLhHhkeMmnjiAcc15DFGLn1wYoaDEWfc2N/x4BPhK2YY/awHRsy4sedzst5J3znM9Jj5gsXouugL1zkZRv+kB4eIlniCzzgrD9eQ138I6PYC2lz0DUcv2nyNSlRz1KXoWOvxIs8iiuj7Z1cHrCeimRlFnfNjQks/+jIj2/FDGLPUY6ScVXZtW8ETrsye8J9wWfSBRvdL9HoOdZ3/UAedWH0WT9htDsIYbe9Mk2GymOGZfcSoZx4dV9KusWOILc56M1zS7A3GjD/Dq4/t4elKM9PNNDP8Vz4z/6c415/pMmzFP+nNrv8NvtKsem+va+Wpns7Me4Zfqrlu5XmiPdF/y9ld24m/eyjn2+TnfwcstHzw4thJSnRQXNX1aPwOw5fEvlFl8q5pSeX0wWO36/FvQMehayflt8ZFqHDwQDpE+Uw8Bp4K+Z1wT+bezA+B0x1k92aPJ/6+8kLHGk7P8hNe55CE5wM48xeW9U+xmb7OKibxT1C02rfemb7O+9Zbxu3eZTMyTHNn+KpXNXp48FzN5szw2fyMn2EzvXCdp5oZf+VVB72YtdKt9ljtMtPN8JXXN73Vte18d/3Vtey0J/0TzmqHVe/EG4588n8JsDT04jd8XRb2DSu8+KcEMtepWjZtRt1PXp1U6bdZFY26i5p6t9bHh9lqFJ8+G6LijuN98VMTNcpp3KM3DeKuvGT3ZLbm6xz4XsT2yIzFLlCqwouJxil9lsDAT3ldUJJCqJygc4ryqQ+NoifNtLLP+v3+v5gvP2TVuwHgFQvLPMIgtyFekrt9xGINF1x/Isiblo4VErnzuU4wRXjqkRPVv+FqFjB6Nbh63DTNR37ujUa4Dj3izIf+pbp0s32cwzz07u/zT7ycr9w1+BPpqQaLGtXen2miLtNEL9cwX5EZ6ut4T7X3Y8/76tEXruNa1c7xnuPi6az63rvYl3fE2YcY+9LG2StO1tt5RH/x3Sfrzzji9v8dsJso9xqD+lcFrUG/cuXUAEslq8d1AtDWu1WKXl/0T52YRS4eg2/zIcT54B5POD7LteR1t2Rn+kPc8OQlSj8b/o538+uCkpx6uybJh31j32YseU3XOabLLCPmdfVY6MXtc0w4YKYf8IkWm8jlHkc81lN9Nq+IeaGWLnrFOuMI03EuOTH2vXaO456vOOLVU0jZtaAlQleMGDURrtfkxKlPIaz2cd3Ma4d7n109xr7X5ER0q5oeEY2iYzXXQ/mmccNNlPUcg+qY5+rP6oivuFlPmB/381ycWT3D8Y39lRca50iffV+OvrF2L/eLufOiB/X1LwFSmUKQltPHcEqDn3KHfvOgh+amp0H02YUc+f7CHHtY6BmMc+lNNY2w6+OTxalW1zRtmhPXPuECQ6ufqYBmc0sRnHARn+484TOSdhqfzsDkhW7YJ+iHHjNaTHsF9BeDIDkqq68e7DlJZxVKhk+x4KllMu4M/yvubN4KX/WyPTNMHjqz3gxPNYU8e95nPk/xdK7Adn7pN/OaziqC2fVLM/Ob4SvNdAcaLc68ZzjyVf8veidzxfkfxEVc7SfZrh85zp/+Q0D1hVdK+4alsh45NNzSCmX0ijE1ELyc5f0KndBWUaiwLwJmnOrR9BOb605Om5fZ8r7Eeao3flUyuTfYuUX2i1rwbjHck1s/Am/4xWP1TeI24mAGt2PQmi7tG3nVf9OrGj3YE5H5ZJitNaQZN8MkyvAMGwaEIvJjDf0J/oQr/6f8X2p+NTvzybDV7n/R++UOM6/V3ivNW91bz9U89XRW3m97l/N77xP9bveTvs8ZfgnQf9qGpDtVv++VB92Y/j2Qu9SBpjASLYOGf/yHvs/6DOhoTW7c1h7wNmjA3Kb0a29KKGR56DTOlOoXdSnyx1Ners7RJ57G1bWoXB7jL3kvmn32YkbnTPx3/YmsXvjqjcrMN8NPMXYZ+Cqmn1QorjjorJXhp5jZ3NLBQ0XZc8CaIsMwm/Uy/IYJmMxc+d98Xu75S5/sKZ75v7m2X2vkN9tvhr/V/IVOnjqzXWf4pfq9Dt/VTrseHqvd1dPn2ooT5zi3/gmEA/6J67kmqB6w6Kxap3GvItdEH2piqvVFCyFypek/oWPQ4sANPlAjZ6gheZz4OKXmp7ybMAe01+w6U8XT+Y1/IjvhaKcb7wYknHAx/RfzAu5lYlvbM1zNWe+GF+CG+fAkz/jZdURerLHO8AyDn8WMn2IJmEB9xKwnPPZi3U0WCT6uneULm7qL68SN9UpPL9OAEeHuovhRE+vo8VYTfVRnXhnPsZnGceXxeN974JlGPPqu8XzWn+FoV/PgZHHnK80pZ8b13aIXtXN2e8JFe/93AGDIyV4B+4tOw2jVWDT+d/Bgg0UBo6Yua3it5VWTjyd17KNXjJysd+MYQEqset2LAajo+MD9+gUPL02Y+A0w/AEc1+vVE24RnfxVA5a371wn+2gxDAqftO8bkt4nsRlAQVLLoUfRtJRRt8RpbuZDc+8BUzG57oHXDJZY8xJ1yWtehCNu8z7ibuYfeRzOi17UxJNrjFxpbpiAB8/Tam7q3wTMJeLzVxr39xm/mr/yZAYx7uJa78Enes81sU9NnOki7p6r3szX9TMOONE1MWcHuDHSjzHyVPPRfwfAvp99XnsKy1/Yq7Fh0nRdw3ttW2QYbf9rB+fVXFsaaCnyeiW3HVvXvT8Cy7SzytS48bSDzoqjfthVUHpOeal4Aj7xfMrVyN21+1pPuE3XfyKeaLXy7RRw9Uf64qe6Bf5U88Q/42ZvslKeFgvnxitAdj9uvOKTYbLP8IrpwZ6bjLfUq2lnphdl1svwDGPMrDfDZ7MzfoYx94nPjItXNifDVvxd76nfU/5uvvp/4flXvqtdv73W3c4n/R1H+/s1XP8zIKlmR+z2xd/TngRRwPmeUeHWA5Oy50E3uFrP0oGSFTvv3pf4xDjhDB4HPrI4PpBvQz4OtCo12e/D/GTSYP1BN9mhd3XBnOU286DPNlj2F3vNdBX35sGeTu97CjRtxw+TzPNrLOz0jZ8u4xt9pn3iOeO+wWea4x1FbM91psmw2UzhOpkmw2bcajLx2Wlm/V/On80QrjOb9U1vpV3N+wudPDmr2aue9N/2dx7XfwbYWP73yvqpmu9v4BmmnzrEg6ufanrdQHr4iACm0Tr0Km7NmnIXGm7tS0xfPrdmAdtOlZz11dh5BM7tAqr56BM5fbTNqrLSiBB2Q6Mb9C7yi4bJhNdV8AB2fPGkOeHh2WZkP5UGCuXnJrQ5cU2IA+7FQuc0fGosjdmOM03HlSxmyr9z67DrwbHZn344B+kvscdeEoRrVYkPkV090mvy2lph/rkGz/2UO+6+9BwTN/KpidK5xn3EUQ8uMcOky3wclx4PcGoiuCKHHv6qweD4TsLoowGL+MwLX3TUPgcv75G7jh2ezpIH83wWfsyKvvTREPF7o3MtfkT8mKtavVkfnCj+Sus9cVfe6utkHMev/xmQb1BlkwfxwhZ64fYXXbWdQk6Uc32xN4CUKI5OrHU1PutifR5v/NYa8OQaPg7/UtauS6stz+nupzyGnfJPec1X9PoZODwBV7P2Gi8Lu36m0azZi3nKL+Bsx+P5IibXp3mZR4aJ6/cp4xxjIoZ9ojbWjFf0c+MF71vfxS13DjlRFHKiW2SY98mdR04Ux/OsPvGJHk99nB+9Yr3aZ+Xjvad5toNjnrt3xL03y59o5KEjTdQ9ratRe3Ct52rH2nWxn3Ed8xwfxzzfea+4mXf0c4689NF/B4CrDt870NTvKbGnumJyKkmvURkO1GPoVR+apZe90PssqIpR68DQQ9RmU+48nNfzxKP3SE44xuWaJXt6dJ2D7slsDTvln/L8AoJG5epkfzcOf6bteJiFLotdo6bpBtyEU9y00DNuxGLNGngQT3gDR0X5hBiwZvZLLPN6srO4M48Mz7CnHm/4M81sn6f8zCfD5Kuz6s36M81TvC6w2GHmN9tr57fSrWb9he6bXU+0q513vVP/yLv+KwCh9gpSXzTb3eVFCaE++2q/PnR0+AYKio+s8HGZ/0lA5SDcxbabv9APktmOThLHl/Heab7bQz4nnDBPayELrU8j2R2oa5UA3oy+AIpvnbHw7jsw5nCXmw79Ig6asNPQM49f4IOHijZ7wNvMiMVatBPshFO9RDy4F8d+MtVJfK/G5/HEM+Ng/3G6shn3KX/mc4SLtHh+Z7tcV3D23K64q97R/hi0+EYj6VPdjL/yais+nvVf031zjbtrOfGecfSc8K2h/xJgfTH2jm3AC6W3efEGq4aliL8TgI2eTXyEsUDFB6D1ZNyO6zqmhOGALbr3Z9DV7D2VE31l2vzocTnZ48oH2ilH/LKk9vQVsOnxxE/kU95T7hf82bUN15vsPfQ1v5wbZrpb75LcNeCmbdCcC+GXMcyf7e8jM84JdsLRnH+Cx/UMs0ox++ucgdfEGZb6HvBFSf0mO6XciceMO5s548/wmc8KX/VWc2a6leZN741Gu+nMtDP8Ul2PM84MR/tN/xvt6XzxNEcf/yPTN2V91FM6PRfQ2AOn4RHTG4NBW3gDJ+nLSjMGnuvafNEiJ+ribO/fejI0b/dnTo3azfYberbTiiPvejRvdxonzrnJTrwkOuUx4Ak/4SYQznWXZf/DHPbuvyxn/akPDaJpZulAbcWAzYSOF0GmiVisZTFgpRjqNiNisc58ZBR5sb7pJvOe8JrFcdBOvlfNHTh2GomZRYa5atYXHnuxdp9VHnV4Oz7L3dc5wvFxDrhH7+NBpOe1596POLUiH/A9wgNzbuzBidE16kUd/QyHD2fnTR9+9Mz6Gcf1sb/q7faN86mz6HOG/wqgktmqRP/JmxdCf3WPGC9Y/ZkoQMdkbt74qN+P+g0Y8EbgTx06H3rz9X3hVB/mZqYQbTbQLa44zJhwNLq/iE04w7yEUz0GkkzLR7iuKU/a0kz76unITyfxvRofCvWK2zktqfaJN2Od37mApsv4onWcpGkosfI49FoxYI18hInUno/Ij7VsI9Y/R9rMjJNh0WfgqFl2WnIkaGfL+9Jv5R97s98HiTytnmGra1ppMi8w4kq/moueGH0c994JDofIHiuftz1mEH2W57FPTczme8/7ma9zT3L3IHedz4s4fOessKjf1ade0QcdOBE8Rvr1lwBV6EPft9r3rotfwNuL6gQbhZecF2y8hXb/zEcEkXU6MZRmFiifi7jLLxPTXkAYk/Th9fgFx/edfWPrc5Rolo4LL2R8POVJ1faXJbLRzKrGNWSeHnCHecYf8DYhwxg+681wrhk9ccbP8EeYyOE5O9H/iqPri16xfsvpOhnaNZ74d60SO6daSb7lzjz+LTy7nn9il9WMVW+270qz6s38ZvjKSz2dmXaGX6q5buW56/3b3tl83Qe+dP+PXuAp+EbZa66uAV1YEt4YgNUX+8K//Q4ABBva/Vuv15pXzq7Wnsy/FONj1A/djbZ/9qxM7JoGby82nGrfOArLY17STfmnvOWw0DTP0LmXT7hSP+VLMtEInp2sl2HSd5yk3HDSmb/j/fmRqD7J3v3kJ55vOW91n+2uLPOJWKyl/Ccwdj2dNdvrkc/icyHb45F3I2c+GfbGW5qnXm81b3Wz/Wb4bs6qv/Jc6b7pSauzmr3q7bQnfTiac/0VQMn696o23d8Y8OLuWL+CLpRtOdIHTPoO9aSy+z8ABKG32UO0Dlra+vQ6Bfyy74LeF+6coYGocWY99/gBRxazX3jqG7FzIVd+b4REvNVOouOF9JS/40Vv48eRjO67GLf3ZglmRUMaqTe8ABWbzLnxi2HF9GCaKS8uIH3QQnEPz+krVm0D4udGpolYrKtn8yN8zZFBuzffeHF73YO89mwO1yFcMDzhflwChwgvm0tPXPcQzvOJD3o0ldMKenDda4e5Hz6OkcsHL3jURHA0ivTAxBE2w8VTDy/ymQa+R7Rgs1kZLs3sZDuIyzz61PgwhwgODx24Ij3HyMVXHz8ifSKcrE9PXPzIFb3vuHId78/8xVOPj+uXABsqg3gGLLiqN7y4q24YPrWmUAwevVXwYVZp7OqqTXTdU8muP5BDMdvVaT/i8CbLrdO8zYv3ZuCecFxwcg3iF96Ouuv72GZ5QSa0dKBH3F8knRh53ssuIONn2ODTii0vEEKZWWYrdt5rfRCGsvt7knFu2A1wh/PcbbJcz3XEvfZJjpMTI084H+qRw79FgAXXfcgVdZATZ1glBz4aRT7gKdJ3DBx+xqGX6RxznucrDj34RHCi8Ow4P8t3OvoeyVfzMo4wPqT1nFpxdaLGuerpxHihH9w59GZY7DOfGZ9/CKgwVy8q6tEnyrzmxY0/ku89w8TT8Z6KXtPTVgGXTnekc3tiWNOJam2V17E+UI/yTkWmVbrjJH2N7We1QyPVNwHNZ9B2k5YcePXPomSvaFfrE08RF7zbzgsuVnX2wcPNG02YMeM9xbGvsc2IHrFGc8MLEH+KF/fGS7CMwxziMUfE8vmQ8SMWa82KWKwzjjAd53ruvYh7T7lMuI8ZN/JnnOp12ZHWKH6mASMiijW4YtYTFvFYu0fmk3nAy7xmfOZkGvzgxDjTrHRvNDO/1TXt5qz6s3lc/0wLToTvcdWDl3HAiHBj3PXFdw4vC8MbAL3L7n/Mb2xeIAXxk+qA4d6+uci8DpCfejo9+fTArHX5G2Dp5TNadUxXx04f0LK6/FXfPNXTuTUuuD6a3tBPmni4XW0nnI9By044oornA5r8Fk79bsIFcDpbFk/mF26lh+vCYrFRb0l6ys94GSZz/vi3D/oyyeY45vnsuR44k30iJ9aSRSzWGScbd6qLz9GpbrXHqUfG41qyXoat9pj1Mp8Mm+n/rR1Xc3e92fWtrnGl+S/pVrvs7suJ9oTz9l7hLX3/lwD791yhvRC1nIYJ7q0E4ydYOERZeK66nuLhL9qdY95QibwBccOog+uxcpqv40N+0pegDxzUy8IlGrM78Fdc3Qt+Ipr5rXzUG/wpEM1MhYt7wpt4MMrbHTvw7lwZNP6AmXGGn2LYm11NH+slWNyvzG+YWQj+XGf8iKW1QNsj5djg2FfrhhXAd0s5me4B9heet+vQkHJm+Kw348/wJz4zjxn+xHvGFa4zmzHDL9U73cxzhn8z6y+ujX3+LW/mr+5X1qu/A8CLqr4v1O8NhUnev1eYGgydhkeMOuuBVY582zzhnOgtrntWTaLDCz4R3/pZbdcCDk9z60eb13GvjYO+R7wVyXvzSrp/8wntq2xa9kk5BVS/HmIr0yDOKS81COCJl49s/CNZIYkXuRlWt5o0ol7c7HcHUl6cX0iRF+u6iz3Qr7EVYNCoieCKYDVSOCHJRYvUrM4wt4t99TKsaqxhqdv1fNfvREvQENVS7h9gin5c4zj5rB/xOEt653i+8sYHDj7g+Hgkdw06x6KHc7Ie2ugP1/Xk3kNPpJf5oYdLhIvWcTA49BTpOQYOlnHAMs9MB88jHvCJ4HDBdxE++ozvPfjOA3Oe98npK9Z/CliN/kLiLL36tVNfCIuCn9h7y7DOlbNOITmvQR+skhpHmkaOGvBGr2Hg9MK8xDLctTXP5jnJ+g4PeeHofog6HAdmPsbRvY8/QVW/E04hVv2wQFKYV9K9Q+Kv7h+KxpvZd7wnl62VOA2x9vVgO8w0A940A9acB4wie/4Kn7YvVbHyUKPpIjfWeEQ81uI5Rk6c9fGPfXDXV46AzX29aTCz+A3HtZ5jv8LoETNN7ImzwmKPmjjoCxi/XuERne/YDJ9xxOfMOI7PcjyIGS/FBNrnOnpF54M75rnzI+49zyNvVkd8t4vzPV/pMp7vutLSy/je8zybBxbjTAfP+8qF07v+d8ANqG8CypPdvzeI5XUpq7ITGtd41urcDJOVDr0am4/jldS2rRwEtdEeSp83Jg73qxTYdIPc5g06il1fvGS2ZMM58Jm+CTCjFUfX1X+yHS7SDJ6mB3tXSy44zAUexhYwfvMc+rGwHeo1xv6XdfZ3+9neGfZq9OH1T+epEe4ze0RNrMXrWPPpdTOJ9aCZcE40Jz4Zp4387A2QxGwPaN7znL7iX+J/6f1099kuT33E13nqt+K/8atLbPbY+e763+z8b3prtk7cn28h9ZcAVQBUZi8+Sn+B5Y2CjPthgmlrWnC10INJB6a8coZExedUnXl/Om33buAdu64R/lTS6Uy8+52b9aUNs52KfeRI9vi48Uoc9rlRWerEb+fl5gfcOjrMp3SrIV/4ZtrHL+rNP/UaFvkUJzNO/SIv1po6YKWIb6KGfuQn9c0zcKKf+DqOe96b5XPqhgdd9KnahJPyinm89pR36jfhPfGccYXrnNyPGW+Fz3rZvBlXuE6mybCLnfN3vZXfbIcV/k1vp931316LfHVW+lVvp63mG388NGf4HQDE2k6vDcPrQ8MGjmFwZapDfVXXo2M1hxz49Q2GCbsu8IX3nvitD37rmd7sr3TVM++bDkD6xGPYJekjV9R1x2v3fn1eJnOq3smbWZW68HKrel0nfoPooFh4Ds+drAo30mM9TFw2714382vkYKlCtt26JQPWFJ3TaoWOvdVNvLvvpN/gIVRNFA6Me3FCd45y/8DROSeY88ndF2zm5Vw4iuARo+f4DbOh0cdro3U7x+A6BpEeNREuEVwRjEiPmuh4xNwHnkfxXUNNdK5ycNc4Tq7oB51jyvEhZv2s536eo3eN57E/64ET0SkKAyenhpfVjqGDn0XneC5urIX9r/8UXrrDN12xy4nYUIsQdPVFTLgdaVznuWixrtJsvhoNr5z24PrlC+hE717LPJl94+84u34x3F7DbegH8HvxQedZ5R/sVB1OeCec+Tq9E21qHcHO/iQDpRUDVqixljpisc44FUuICSTqcPpf1wzovojeu1qObzhxk+gR+9mcU07mfYrFGehijLxVLS0f8PCjVoTjPTD6Gd+xWe4+zmEWfWpxPM80EXvCx981yvlwb8/hE+l57bn3Z7g4WQ981lt5o1H0D9fgD5ZFfLznGN7eVw4Olwgv1uAeZ5yIq+aj/w5ANSqo3hD0F5Gm7G8SRBLWOOJVbsAqR71u1HTwG961AS/lx7cXDVPd9uqtYuSjet/niMxp+1L2aL6jYWd8Zg8DP3BlTvy7pPTrqA6Yf0v1JiD7o061JRteQGY+dchlqOfCygv0RzVnPhvezbcAFQt+N17xza4j8oZaRXItA6fte4INnFZk933gae82owYVyU7qDbxKNuyB7uZTgI61+9zrOGdVSzTRS+aenseeap3sr0auzscr+sR+9QFsUZquU9J2VrvjKtr5FYYPEX/FiKkGI6749FbcVS/qI9frLHcML8UnOFyi+2RezvN8pss4GYaeHhGcGPEndeTiSXzTRxMjnoqrnvOcu8Px/PzvgFGoU7649PXVv8YK5m8M6k+ovXkJwZr80jJFlObZ0kvUHruVzXZC/4m4E203J1peqSxjeE/ZzTx77yQJ3m5TrTf+7Je92DC+vglQ4eatKYgRNUk4jXqFsO/QozjhiPsjXt+f+c3ayjRdvcgg6N4k5f6Q3jgAJUaOWhGLdeUIDM9B5MU606UcEe284bjG82oLYPsDMTbW4B47R0lyv8XtHBNmmLVrmnEyTOQMj1isT3Uz3gw/nSO9TsZf4avezGumyfgZJr3OrDfDV5p/o7ebueuvrnOnVV9n57HinGjrkDBHOr7U638G+P8UBKAKGiNierECiy/40oFVD3uQBl2F2dz8hFfOpFd1cCiITTP8iYP3huE0WmzXGtDxmZnpJ1qn716sVi/y2kleO4+6u+1SNRUMD8YJnU8pjo5fxIWMjydeUjQetqPJWGXXOdXZ/CnH7CMn1qJ2TEm5/l4vfAZd453oTjgn3qc+dgnX55QDJXcfz092OOFEz0yTYVOdGvY5OuXJ1E7GUzvDt1ghzN7AZ1pbI00zTYbN9sU002TYP+WzmjPba6X5pietzl/MvZzfe5/od7uf9OHoHnz+BKBUejGqX1N6KLkI8Sf/WMvMMYkk7z4i6Lh3KWtfA9qJL97+ZqJyxXO+6t5QUY76hvXUdJXXG7X6+Ea8tfvc0K+2G+8qKZxOCx4a0TlJb+j3Qsml674CKIqPrCjV0qkY4GTWxSyP4h1wql3gMWLwomjcG4c+jcIjpaU4YKXIvgEPHDSAJ77SiL+7LrxLrOfJPmgUEx3rQot1k9HuTxc8oi5B+azuBsYBQ0OtGLFYd44a7f5NOSJPTqaJVBvRW28waaLuBBNHQv8cjPdbFP80cl/l9MDF19n5iCONDh7KZz70FJ2vWid64cMexIxbDdqDdNEfL3ixLzyb7/hMi079bMdM5/ugiTvh6zgY0XvMcW8wxRl31hPOHOU68nDM89iPPfqK3rt+B8CRQvBleSEWVnFxjVBxw6xViNepHIoSI6fWmYdhJv+kWT/DPop1ttOGvvYWdHJOuPVPA+LNMfPqcbJD4JjFJ/0Bp1+7eXXsM+mW/ZRjszUoesf6tsxKY97RZ1ovNMv9Fro46+bTtFteu9ZTnub4ibqntbyiJsOOOYXoL77oiO69wugRT3XOU64jD/cBU9ShN4sX69zHPWOe1RnGLt4Di1EcDr1Yz/DIy2rXer7jqp/xdzo0xOjjOF7ErJdh0RM9MdPssF0fb4+uIf/8z4BApCi5fiL/f1Cr5y9MrU+7xsgR2Hgu7c9SA72X5vLVKU3vX2B5DHMrxzSdd5oEv5ss9H2nOjb0Xe+7+TeugVP01ceNnaB8MaNT/0mO7aS16/59kZCUpj63lpwg2ZbtWk8840/2URNrLRqfq8iJ9YlG1xR1cbeUk9yM6qOH9jkTfRPJAHV+8+h1Y+1q0X7BiR5HvkUUn59Ml2En8zKdMJ1T/Yz7b+HZ3tpFZ9bL8Ay7XK7HWX+Go131V72dfqed9Wf4bh594rc+v9K7z+d/BlS+efS/AmDjFuv3FanEoVfq7I/tHUNXpU240vfPvk5i2LXbZ/gHV5bQL0LbeWSXSrjOTDjTXaqpvtsVffZNaZAvZsiHFdHcoumnfOPc9AC/4sjvxIu5uygvnXZTKS8weUxmR83TWlOqxrx3Hpkm2Xb//DI7Ezcs7pJRI+eXdfRifseVTJ6/zkGUxLecTBexWGt8xGKdcVj7CXfmk3nMuG/mPvWf8d/stNOs+m/3WN2j1bxvdTs9/d0Oq+veaU/64vT/F0D97NfE8gVbv2ZLrhex9vUrbj8dKxxe8MGaxaCrPePKCAwi+oh3bp9+JfBrpaE6A3hBui52bMgnbHTRD3o3EJDNFNzI2RuBLimcrF/9J97SNusr6WZVNTxUbvAZ9I29+6uHSgs+w6CnReLVr6l4eV6L5BoHTtTs6iKe3ne7lmGGirDH0G86xzw32+H6bpwCxN1unGIWsVrroe0Y+5ofsbTeeMiHk30uRU9xI+a15/hGTcaJWKyjxxPvmfZ0RsZ74smumU+Gzbzf+Kw0szmznd54oZnNWuFotc/p5yYa98203ndNzHf3Yuez0+/6O3/68tFH/ZcABeqi9cGpdWGAeQ1WucZRHf8UAS6xcsRT0g45UTAvoHBq1MbJ6bqwi6i1N9F1q1k/4H1OE554p9fRB1/7RV+1T7yrTdsx8+hjNtdR5wVO13rykhNltTbQUp/2PG9GO7/aD6RQ3l60tEz9I/rFVtEjM7lxMr8jUia8sEwesWVdmrEv54jt6qoJpFBeC4fHE06QHJcn3hlnhkWcmshisQafRfGjJtYzreNoiOp57lzyWV947MUaDyJ9YsRVxx5YhqOfRWn4mHEy3Gd5DheMCO6RHtF75OrxAXYaV7547Dj02aH+EmB90faO3MorCi9CNbZXGF7Q9FN1g/oz2LHmhc55yvmJvOKB20YrfN4EFKJ71GZ7GLyEFb/K7YJG5Poi3tr9szD20TUe85BVeuFUWtSiaR7xJ7vabj1JY79irc88XVzFOyBhK9QIp3MXnC75NacYV0vbixF1ZimW/bbY8MJbvAYPZjjX5gmO/EbtjXjfI/9JHbkaXjHb6cZJdnzDqRo9LGZF32/qqOW+Rjz+bsOtj9DiX3E04o33Shf9uIyIx3rG81mZJsNcg2+MURdr52c9MOKOTx8+EVwRjEiPmghO3OGzvvTf9k70M47jnnNdu/3gzbQnfdfW3wFgaH0jUL556PuHPkTke0ntCaPvTXiGua60qxFYrdvDgBX97AW2LxJ1NtN9h+VLo8+Z8RG3vkJ6Jvrqv9HqHsYXm2FG4i1fwfFkeH1zVhqRP3CTGdE73rtbX8CJD8Idl4W1aDi0gL32nH69VjWaV+R8XS+8tcNwr0v9dJ48vtYUg+zz7Ilv5GqvNyf6PK01c6f5S07mPcOE68R9v8We6MXVyXZY4aveL700R+eN51/oVp7q6cx2vbr7/s5j57/Tn+6Rzfn8VwDu0r6LKQwvyAUQ1k9xVB9sFsUfem2TitGAo17DrHXd5QIMmIx1TKOycwKuXj1t/odI49MfrvsDX9nMV93S0/zsG7DaqzcBVZd4V1xinYPdK6/fhFqNDzuPkT2vwq7YItjV8B7FMNO1w7xSxOdg6Bdhr5WU+9XrZhrrPqvxez3hR/2jupGPr8GWiXPUitiuHj7nkuvb6Z/2T3Z8y3mri9eQ+QjTOeVmvMvh7jHjPsVn+63wVW82f3YdK6+V5t/QfTOTa/mnPE7m7DifvwIoTH3R19cNPcM1KbHkpKWqBX8aUBvts6FyykP9KfSiVbo46oFXsGHKO96wasdMx5yrXKZ+2h7jsoXAfpEvLRqlsW89UbmOmuuh9W+6RvDrulkZEPV1jdKvFNtJqck+hXHa6Cs4ecfJ+q6XY8YR7rwZx3nGcWmlADQOpXo6vSZZebmg8JBUI/dqwJM/ppZXfX4TX9mlswCfaNitRQJWs1o416NbJH7UVI4e7Igjvo70+tzs9QVXH7DKazghzvEaf8eiLnLgaiY5MWrheD9i1NLCA6PG1zkrTHqOPLwWPsPUgysO89lHfTDlK1x9DvM8uo94zHWN4+JnGjAierSZDk7UsAM40b2UO04PfNajr+jH76HwJ/qonenxJDLfr3fWg6t+5NBzH2HOY8cVB410/4MoF3JFfYOrUWyOMKk4kdN6+FSa85tu6IMZj77PAmP0NJoPnKpNcPqKPsvxnk/0J7rj3fuw694Pz6x2tL7Sk+uqksnu3W7XF7FwtrQt4fJpdgrTE63SOoLN7QYHIJSf62qNad+2Fae+yBqmdKeN/a0mCEJ5mxf9xI8aceKBA7/W5YFa/IolES/6ad2aGSfD5OG45/h3TmsqRF7E6BO7h5u23Dm0Vxg9Rc+lzTDH4YN5VK7jnAu5HjNvuN7badSHjz5qvI452ojPaufHebF2D3QZh57zPc80WT/jRSzW8skw/NXjA4wI7pFejHCEK/dDfcr5/FPAxUXf0Pwn6/ri1l51FLIXoBtHk02j5SJHmE7009Uwv/cyTGLN4YR5tdcNIJWIJuuVtu9pqi6b+aLD3rU1b9eQ9aWNfwogTeeSzHYWV5xJX15HZ+NR19hw6pwDTvVaLXXg0eWFm92/3t8kcZd4L2/96Ge77rhP+gO3zRiwskestdqAlSK7NwMnag7q25wDjc/0XF4cxz3P+sIyDlxixskw+MSMk2Hix8+ZimFkMdNn2BP9jMvYzD/DVvxve7Md/yt7cH2zPemv9t1pf9E/3eOEx7VcvwNAxZb+YlJ6elEWVOFWM4SIhD82pCaKR15fME0IXjnaxQGV7BdwsyD9xI3mQwxZNh9KmY8tUI+lUdebcdSf9LZvIDQk2avPDn3tcdtzpw8egzdFvUCKSWycOD/WE3UKR22sU1EDO1fJ5P6jd272wglPsXMbeFwf7OFzyHf+4qUcwMm1057OEcGed+d7jl5xhtdG8ZKdczzP9LGfckSyPVOOwHBOvCU55Z1yv/XjMn7hk3ms/OnNrvUNvtJ805NW55+8xmvi9biau9sLn53HE5683K/+Q0D6uhk+xChACz3xmp/UNZwXsPr1V0g1CieBE7FSf4Z8dGAmF7OfG94X65RP0nqDRlg76Tf6mV/Bu09JzAa7GuHc+k0/nSl1Eyvc9K0d8c5Vg+ElvZ3Wj/qBt/JAmMygxXM3eMYimdH1hVvzhBNtVK+47lm5wTP2M3/H4BPpxZ8AYx/eKrrGc2lqrYd239O+mcd+9/gBxyzS1Gd7/qsddp7ZnAyLPhnnW+xb/emOsznCdTKfN/hKo57OPzVrNufaYr7HaX91Ld/0Tubvrg2P3R4nvP/VF29NDFOF6/tN/15vdcVazZCIUauvnA/m1H7DhdHveMF0wGvhM0O/8oJP9Koe4cGvk1k3rzZrkNou6NgRHt7UxHrPKbJo16Y218G9q5JsJzVmeBVd/e4HFuPOA/6MB06E75FeiaTerjk9I1g66tSAfzOyGc3AfUSPdQToExnR65b0GkLzdlxvGIbauMs06MTFZxadg3fkrjhV0wQKaPHyuOp1nnl1bJJEv1nd8ZL03DyFOU5OFJWc6Jjyk+Na8WO9w5yv3OuojT31swOPCCfW4B4rx75Z7DSz/gz3WTFHo0geOaqzXsRWdewxA5wIvosnfDjEleeKQ0+RPPPKemj67wAML1blSdfzXl+oyK3uQ4pL/fzQA1OEVfBidd+AfV7VbJYkhQe1v1Aa1ueIqpmQbX7HrN118EuvH7SxB96JISn9Kmk6hSpxnTil4ZBctHv8k4BqI1wEPRSgYqo5BRfmHFo9wjFx14iU9LuWRANMD1xj6alNnvIgbHyiB7LmboM6Mk2qts27+Zhq9RP7oCtFfI7M5nMPGuhaz9GA1Vge3Jte5J7W4j31cE3UZnPhxAjX/cDgzuo3mug5eKhZPm+XHJYp8YT3hONcz23kciaaGDM9HHqxjnjWzzDpwIl4ec8x8ox/0nvjyywiczzSI572Mp5jyjNP5+z6Jx6nnFOedvK96u8A+Pd55bcXVidIXerKu9LyeB2wOqFx1GmSShKnngLWvAMfHP+GdINIxecy6uzPFRbBTdOWUbgdXzQ0/c3ITWs6zcv60zcBmnNbsg3HKOn3OW12r5u0BtvL4Z5/2+9Gm2QxR616Aqfj3lfe7kXsUxORxdj7SopXr0uZ5o039WmNqm1c94m6oZ7wXe+5tPHNS8UG0/E6aLnu5gnJ4o0D0O6/UXsKBeDXtXzd0/M+c7Ff55CE6H6eO81xz2ecHZ55ZBg+3vOcvsdZf4ZLu+rN+ivNX/Rme6xw9XRW+1yMPWfnseqveqfzT6/jhNf/JcA6XNuFLyCV/uLXa+cVXS3LAxa8ieg042gWODxhOhXH5IJSrLVqiB69F3wcr3NYojdKUjSS9QW91/jIKo8+RWmqT0lbQPomQFyRMW3poJ9o3dv1HVcSvIfeaV8822/nMeweyF/1Jju4Z7334Zq97+vMcOfUvBCf/LTu+jjD65rroV2X9+QRa/cld47nM/2OE/uZT+Ts6hMPrmcWfzXjjU+2v7D6ucbCxdg/R4AV48xvsZleuM7pvIud83e9bMZOo/5b3c575bubu/Pe6Xezd/pv56Mnnu4j3vVXAKboL/alqU/w+r2pfYOqz57nmlhqQRWWT6wF1WZJdAqHGji+gHevS9E96wtlwdB9ksu30lsTDhrv1VwPdt3uxfzbF7Tzizx7QcdTHpk+wyvm3nUBLWhH962UN08oSb/y1T/wrjbZXPP3ewTcIzMSj9pK+kA7j85rSQ1tTu81k17DXfEgFw4pkZ16rcR4sX/Ki7paFzHPa/dpxDbS9VC3AABAAElEQVQW2rWngxiG6D6d3hJ67dZ076wWt+tbLgEeynWcE+uZRxW2h4zDDN8L76iNtWtOfOC4D7Pcy/toFCOHHnxq8chdt8IyDzAiXop4sRO1c8nhqMaDHjoiuGvAIoc640pDH71H9dDBU63ca2molXMyzHvRm56itN4nh4M3ODV9xQxb9eWFhuj8mM847iMNPHbFB1z1/wD1IhyJ9BTVqx+N17nuJk7wqTzjUHe9zHWC7gLHnTINvB6Dz03TiSGxHen4myGwIYZZQ29RZL7DnivfZE8flXl7v39WDGArNt5LbeZXsJvlDUiEC05vlaTnk/Vjn0kRH94kQipxx4t9pNEv8tI6gmG+2pHimPeyHK7vBm8X+3UNn6SgY2QOKN5DbSB8oFhLRw8Pj7FHHePKB677eu595XzMOI4rj/onGF4zj4h7Heesatcp95odiPR2POc7Fz39LEaO6+FHjvAMg098wvG5URfrk/lvNOy9i5l33AmOoj7+j79g1xeP8kWur3M+ZFA5hks5/BQvp9Cnll7HvStgmlrrQb6K9aGHrrWW0nrwpa6xeYM1u89nRwdgfKKkfqq/ANMMHO1sPVHp+25g6uuox099F3KN6LySpH/CILJIZabGdr5wTutTEjt/pZ9o8WB2r0sy7LDTu/AP82GnxZzOK4k/Hx1v2lUde1UisNzw2FvV3svy/vxpgAgCylHqfDBFPztO7EsLpljfPLSZmW/lWAMtEHWNerD94SjCA4v1CedEc+LzlpPpMmy2Z4ZLrxN71MSLNfJiL+O494zvnMwj6lRHDF2MkUdNdL5jnjvH88jx2nNpvPZ85jfjzPiOK/9Wj9/KZ9b7/ENAjaEXJr3w3F741Pcv/sbT8Aon/YEvXuN0G9U6HbhK3ZH4oppijV7vYOJR2xFHk0XNFV4eWK3TBDQvLDvHek3+0TdS9mJe3wQgaIPk7b5R1/ttZq+bvoewU8dJVv1VT/qXfclOjvM8v2lplptAKo7nvdCNCr2Bd7XTx4FXCt4oDHiijC+Y8ImJJIUiv9ctYR/EvQ9QYsRibdSaxn6vlST3UqLOqQ7P66cecV4be9vjxPcN55v5T3aFm+04w1b4rDe7nhlfuM5b3Uq78lzp1NP5Rr/Tfut/oj/hnOwpHx3nev75h4D8lUSM9kXOG4Lq0h5a63KFp17R6ZuRoAo3n14Hjkrn9brx+g6qddi8ii6oP5be7U0DmsD3PwZlRvdpmuhVR4f5sgXqSZs19Myz89vAyhPYdA3+hIe9wb8UXMeA4/7QG1mNK60Iq37pbV+0VvpmX/cIOZhHWekQryp5bDNXvNprvOjgOs/Fi3WqLaTdfYk+qzr2mOm459mesf8rzltfrmEWd76xH+vs+pjlXM/pz7QZ9xSbeTLz3/B5sxP7ErO9d767/sxzN/O0v5t/6rPac9U79X/C+1/9zlSm1hf6oqw/pbeo16T6uqSt2kfFqCNvVssfr8AB9/lgmqmcCF6jevTF0Sl1/aiFPQjXabF6ljL6uD/cqoNrkVmuqb7Mci4mpcf9BVIcdOJ4U315Bt9OAW8xatkT/q4Pr8bgPfRU0L81GqB+4zi15gAlDm/IzKvjjWuSzgLL5ohEv0YKwzHqrZ5cHS8978YrL1kU0aC7bOsj+BApLqmx7z5GHfwGUSiqJgoDh9Jp5Ip8iBdxMPCTesfBy+dKo0Ov5wW4YZW5fnANTMc8p6/oOJ+vA9bIp5h7x1we0cdr8amJ0SOr4RLdJ+M75pqZLuNELNPCiZH5wumBZT7eI4+6WMPzCIfoPfJVT5xVP+tFLNZ4Cvee53AUOfA/vwNQEF4cehSrFQr+4kWtnzArrtjo8GoPi+YFp28roB10vVYiXTyOlZyfcjutzeq1EteU0sZetKBRny/oG9m9DnTV65pSH3Wd8Se9YZ5xSXtfSTjVz7A4r7Zsz13frG73beipMN/YU6ueGacTCqtxgIg8b/W5CNd+47RxhN4XYIWlDlcZPWLqhV0h8TzCJw46gWV375HHOOhaASfrCfO+5/BTjojtfkbNrBZee3oI16MZOq4ln8bm47op17xnnO5TCDwvHVPSDvpYO05OFNdzrx3n83TA2iDHGnTzdF84EcMnRudHDT001ERwIrhihjk+68848InixUMvRnjgqrPcMTREekRw4gynr/gLzonHblb0iLXr6RHVu34HQFk5atTvCS2poTwML+SVefHErS/aSsRTXQ0uEr1md3G8L1prGnzjVTfxdAbirbw4euxDP1DNzIe0M4KGUf2NgIgNVLjpu1FLgp+3szcBvT/R1Zmtl85nobZj9yOZ+NJexpX3F77Yarbn6S67ORiU6yeNPnyDzuZ1TZjT8WZGXaNxwZnp9SyPe1SeHtpz6LqUC5hc80qLTJHjfM/pxzjjOO45el2a456LE+sMe8N5o8lmL7H2vImjczLzYn7P/aVPtvfKn57iW+1Kh/+MM8N3OvqKK49V79QD3s5r19/tejpHvOEfAtKLUvZiX+9M+8TmRd2HqMXnvZb3uvJFLmDltBmCONETXu3XAmbxYMAH+jxzgTs8o0nP36zITtZdY3xS/6lC1I5X4QfouEhtX2HQBOvoWirWBM7hvsWZVYgRg+RVPoCZWbnxQSTTDW0MZv2GQ3ukRTTzltlkN6Tau+fDcMMDZ8YP8qF0jecixRphxGPNtUXca8/jrCe9qJ3WMg33S1wdPU3LmU1byRvukvMrH4Yku6i1uhakbziZ9xNsxp3hcUfxdDI8wy52zp/5rPCd366/2vHbuX/pvbsu+sTdLifX+muOdrr+HYCS6Qu+HjZtmPDaA1fteaybTl7dM+R8tnZvkW2eSo5mwet+mq+PeBoGP7ZvdfBB1+cEQd0laERBF3fqPk3T6+AbdUO7XdOAUfxFT94z3wRPILbLYxFM78Nq9q7HtMVC3vLcrTveE4znMVJn9fAnScUu8mYTIs/rnvfk7quWtYc8zoQL3yM918wwcdDG3OvIibVzyTOOen5WHHpE6Tz32nFy4hueNBz5uJdwsIjT80juXPTqxQMvRvFcRz/i1IqcnW7Xx8cjmrgHnKxPT5G+Y56v+vQUZ+eEI+0Jb8fxvvLsfMOpvwRYvyEXl/6NueW8+GqoetQ1TzYBF88PuDDlOu5Va8NU63Ruy6lrUw9hjqA4W9j2/Min7he84s6x7rsFXcd3iemm3pmH6bJ2dm8rz3SWdov4Itcblmw5mTH6Ve+QE+djSew2AfCSnNg1JC3SJ3Jfex14yOOO4B6jh7yFOe65tPTBie4bczTgaLId6cFVnOl/wYnzdrPgE9kv7hJx+MTYR+99OBGD+yTiQcTbPVY9eM75Fjv1Yg4x09FbxRPdjrPqr3qrvf6qd7LPW450+qi/BNgvoCD8FYC69Y/IFUVory71BbbkvNDS6y/oxus96ZtPxLAGhyecmXXTChSo+bfyugrnKi+zdAZuw66Omj27ktKvFMO5xkooOBaV0oqbppD7N8bmpVB5TVNr610LXLoKl4dGrWt2v0LUNQ09iQ2QXmWN4OIIiId+66HtNIw60BJ0Ks23wyWpufWwGDhJX7zK0UPpdz54I3R8xTH/zpdepwHpX7F82pUHJ3r0OvHqvTrs8xBxr2+5gHYN3pPbrK6fK+GeMD3VFJDrW/JoxtlBn80AixFL4fSExTrDnBO14svEryvlXLRKb5JlHj28Rh+xbliSrHeK4Y+f6zzP+mDRw+udh/c9dw/meN/zyI099JE3w1f6lQd+K/2qh554wt1xdv2T63nDuf5vgGV6f7HTJnzTEd7y+gLf8vqZXHLKodfuivcq5PzGIcClVqyY7dJ7hg06wwcuRSGL0k/CZ6Z/43C++rFXseBVMQltoL9wq6V7Vtt1qBA7Mz9RHvTM8aaj1/cXcLALuh7DPh1XsupZXzQOeY2mBxePfMsxffTvPg84/Xm1HQbf5lX3ao1Z3udjYNE1ulj/nPOe58jj55lzPM/m7/pR85Qf9ezc3+C2z7/om+kiJ9aZJsPe6tg9Rv8c8Z7P8XzG+TX+dKbmzzR/1eOa3879J/QnM55wxF1d7y/6q30+vwRYPnPrC3mbqG8k+mSevrhr68apA1qtq5FOhzcPNXe+8gp+uLWWFvHFuO6OzxFuemg1thkDZkW0lo9/c4Wqa+YiGBV7jldfeYnUhig4B/0tinRbrIkzXPQw6+Y5A2azZnzwnW7Vt55STs+tT2+Iu34jd79Se+7PseOeP+UM+8V51vQZnosyq2e42fY0+xxDT4S8rEvTvw6W3MXuzFLcefyKE+dkvhn2Vpd5fYv9lV6+OqfXerFz/q6XzUCj+G1/5bHzXmnV4/x/0af/3wDri57uRLkL9cWf2DBuEj19w/A3B/W1yjVF0D3FdR+rK65eS1yjJyTVia8jgk43KTlYbViv4TUYv88Tv+GV0/idWpIGXfeo0G9ewjqp2AU/vPhm22s0AG2VwUve1i/ltUCG1eZ1OalG/agTxh5Zn57plAL3pIAdk085/LH0Vd37VZDo4HfDCaffp6RfdykPfh9u+9Uln3OarK4pzzK+XqvPipyBXIvP/Uj3wmBybcyFpns1m9/G9ds51E2na3hy2JlY70ExiLU8IxbrJxzmZJqOlQH6GtSck1ldV5Ij/8aTjqM50vphdsTg0XetY9L5PvjAhxtx1fTgOhb5zqk8FiyFz8cTfYzedx08+thTq+85fcfp06N2b+8pzzjwnQumiAY9MXJcP+O4ZsYBdz/XKddO9KkVddj3qj68iNMXro/6XwHItL6YNzZD/MURDpPgqCbvHKa06D6C6izF1q/BfNJa/Lafy+DeMICgqTMDBpVr67Uncb/Sy7yGa2r6iM2uI+JHumQvX5tril7gA/ekCPcO3wB3p473pLdq0uGejH1VvdUTwwKdfQbdrfiIzPIDhmzGibjqilnD0tG1NNSjTxTJ8V4bgT4QNVxMHaeHJtbgHl1PHvvy8QMPDD614i84+BLx9TkZBp+YcZ5ieHn0fLaTOJGXYdk+UccMxz3HAyzjw6nRv5AKEHXoY4y8Va1e7LsffThEcehFfuy5hl7UrDj0iGipFT2nrwi+wuB4JM906vHh/ZivOO5f/wrAn+uaiwFY8vpTv0EaVl/EC6fzK6iHcprmKq6FxcOyawCshybj1B7bm7bjEcPMr6dg7o0dVN991nMcL/+pS1jl6OFwJ+brvroXeIx9hhos9HBW1c00NhD7DrXruuHsknhWbtN1n5gk/XRG1CX1TOe459VCQNndcc+POW0f16a5wM0812mx2efGwCu2q/ptT5fl2p4rac95x0S247jnRsm9nVBy13oOLcPoEWccxz1H9//n7gyYHEdyHR0xsf//H9+7Iyh+aYjOlFK2a/bFZaxFEgRAylVtVff0zijO8Bn2hDvTz7CV5wp/6iEfnU90K83huPa8mnfX2/H+JefuHn+5787eO/vc+eR/DCi/4rjZh7egLEcSdeT80fb4TgmSeL03eNXLZcxfeudkLVLjuG60bKfs6zLDqtn/GDphzVdSppLnKR9mDVxNaaJxwgLuD25pk1PErsm+epEo1xmehQsbPip0rHcAdv2kt9II12G5o3pdVzoxqofFS/TqnTAvQrR6yLELvkTk068xTUUJ6n5ce8ofcFyHvaLO1S7+NXXepd9hO67O9Txnc7m71+F2JL4LFo1yWU73MEXvz2bscD7Vfeptt/CWfuM5087ubYWt8JXvii9cZ6V7il955aCLWTvaHc5qZ+bvePzbnN15u7yd9+D0rwKWsR5k+pDkYa7PkPwcKVwc/yBVWZ8zSscZGiFFGFgkBZ28drExxHcCnGGsUD2F0ymc+dkvTLwTLiB6/YGevMD94SVdel1oBoEhwU2dzZf8dDA1DX1a1Ke48lzhErfeyb/1tmYFiR+WTl4uLt/e7w8obl8853ouW+qMuiBU0w48QTu5SZf87uW1z3Av55zy2Ltrdus73tb8It159f7pHhYef8n51LtWfXvPr/Cde5/t8wR7On/lfYVf9Wb3yE5/odvxfsL5dP9fzsDrbhd4xB3+Lke8f/JBH4k+E8crOplX1HCv3/Lqi6dfLcMngagNE9Rr15TkzYeZ9McM23H0AvMj7jgTfvZMM/iGiTNwFQsf3ZufkyYavR7cpku8sB0NnLv5jz7NxnIXyWxv6NXrlKwb2MrTX6aUHf38IQD/wuklzwvXuaZxrHWk1rf0TLOGpWPPJEfDe8KoiTPeqZeE4zK7d2sPbzD43a/X4jvmOV6r2Lm9xttx5V6vOH1m13UPfDz2fFavMOGrczXbNb/m4b3rK/6Mi88qoiHC6zU48a4Pr8cd3S84Ox59t6f1pzPudHd97fmUc/yLgELFQ1km/LF8PlTUKyyjJkRCrqiT0XDA5KnvOjVVxxmzWp29wg7zpB8+2TzqvAaPPbLWHmiLdvodO/yTKFZCIz32gQELYt/E4pK8uJw4UZxq+gVKoz8pyMgceUsn0I8bBX66D/HUb5r0Ua9O1+R9Nk1SmdV74CJZT6laSz96EdG5Fbs75vmsL6vECbaPWrlTmby9l2hEDI73fW7mujTvO85bv+ZkMK8Vb7WP2bztfepF8eYtoGZ7b6kr/hW3a7O2Ob2P1yoOfhD8PRi4kjhdf6AvnJoIf1UL75x/C3sy5yl3xf8E/1TzFzp5cmZfN3rEO85dXz5XnKseOxDvuPSJ6K7iL7j5fwO0z6bXPNxpWs3D/EU+MuE8IIW88cwD7YyTI5krIjrl4DPM+8r9tN1Sjkd5DmvhFJVCzV2ip7ZjWd9oWCd1Je4P52FqXuiWkUVWGvWjpzZU7mPqWfxHPYxrh1aefkigl/5Xs0RY9IfHVX/R6/c1vGocY332Dqf7zuqZz4znX6uVxvGxc5mdelH4g/XUC/5V/aTHfbjG877jjL/LWfFWuO/hOTvs6ODO9DNs5bnCZx67GLs98V5xr/C73lV/di/ic+764n3L+Va/u+vOnN37YeYT/u784y8BlrM+dPRA8ge556eHYkzgYe8c/W4QH9n6A144x3M+aE8YxIgDH4k1e+p3XvwBKSnMrfrvYNVLTN7Gn/mIwtG9+gdt+lSz99Do3nkfWY8ojnKd4SV+1JqjqAOH9/FAN64IMdqQJCV0fp/bMs2rvRmNn3DHtJLqxHRZ7Ogaz9O3LuBEwZnrYr7eL2kGxz3f5aAhSue51yvcZ0ns7/+dxvur3HdglnPBLmMIPt1rNv8nmG7i4deYe5zdf8d6/US7ur+nHv+Wz9Wc1ftwdS/f9nb0/79zuD+Pd18LccXRa/yLgATmKbV+zeQPA4qOqRZRl8B5eMERntrSZG36fHBJJw+dSNSW3rEEk3Bcck75DBiB44UlVDg7SseDXbRsR5IS81A6NIWfsPKpUWmAPB/00efIp/eGd5CyNwiHSr6rPaG+TJl03Ac/zKRHtU4zAvOdkiLCuBkXVa6w6jecvVNZvbGzwNmsCX7SVJ+HCz0ickWdE64i9nAsSfBotPsYHOuTEsXx3OsV7hzlOnCJjpFrPfX1ylUrQUMUX2dW+y3SJx6q0gksMv23+SUw6rFfAP79RV9RLx28juq40lt+/zYdfKln93XCRA7ghEkYRxheRUtcObgAeGDUSa4LejhEnysquGvB4Yqz4qFb9Wc42GpvPD2icczzVV/4bA7aKx2clf7OW/qVP947nL/yuLov32+Vs5d8/IA7Rt57XudfAhTRDTMP1nioV5+HtPq89G6ftJPa/Z0rXGd4HeX86lvD+BCb7YCl4qyfmM07cQx/00fPud3H5/p3rmuSs5oxww2z9DTqzv9EVrEw8gf+jDLD3PvUrwKMOMY7sDR5NaD7jsPrRcvMOUNnnCuM9waOyQ5vXavpHOXUGasAS3FdHCMnOm+Ww+txxnUMvjDl1OTTOr6xHEeryKGvGi96ROdcYerBJX6DsY974eeY5/Q9KufgqbrrnEPf+fR7vPNxvnM9Z55zyTsPnLjqgxPhE3fxGQ+MiKfHqx68O85dXz47HOZ5nOlmmGvInafcX3B6dM2sd/yLgMpJD4Xxu1OeEOqRW5o/HASemuJkrilWq4Q78kx0qd6RjjnOp5Wx7XKFjfvAILT5pw/UEfO2Cjc4U/U0zk9isx2CpJ35HaprMm+aKx/euz576aNG8x9cJXmTibwuM766Kxxl66vs54QZf4V3PTuc+Eaa4YnpMrnXwa/+qBee9IlGy9W89pzfuQpz7eNcgrgP1/kccu+vcnF3e99wu/au9p3E5Tju+awPprjL/W/xVjuu8NmeK65wnSeaFfdwWntdzfmr3p3vTv9XnLv37Vdz5ONnZ67zle9qxl8CHJ+dodTDMx/CYUSeEeciw9EHFg9tDVabuqi5EQ/lxCDKs87gquYOChy9iS65RUDm2PCvZn9Qs+vQ1nxZJjaGlxPEhrvP0JZEvT5X5skrn6GRf2ElfwXrKdVJneEHWlcneeMpX9qNnXxE5hfz396PECS97YbF8Is9ThhDTUeflVWDie65y8k9Jjcu+v5FRxTP8zddAcnRhYUcn+QFjeAzyMefWpQnOCKv/QcU9U89BBU/7WGzqxffuTP9CpvpVlzhnd/rJ9qZ3wpb4bP5M+zpXlf81S53mpXuat+Vhlnf9u9m7/j/25zdeeLp7NzjEx5c+eL9+kuA6sbJzxJ1+aAiJ6oVuT/Mocp1/KBQPHzwRRft3KLXgt+wMUDdOLbLARQW4U2rVtPzoJZWVnm0u5K4DOwoE8ADq1whLuDpoUs2jkpc96J3wprmUJawhuEzdDZj8Bc+aE88ipVP9NGNmfgr1l5Kx4FoPaDBqWTgSoo/sE6e1Re6mc/ATIft6AmovmOeoyG+9QLg++GtZ6Idzup9Wfk67jljPXp/lYvvPdfPep17V+94fMph177DzO8pd9dzl3c1/+m+V16zfeB/MufK76p3Neuut9P/38j55U67XuL5WX1N/slf5dHNh7qiVNSRUqdZ9cFSA6eiY4NnnPROs5e3eMktHA64Ryj5EKew6POB4eKTeNyLTmLc9wENrMoMeIBJpwNOnWB5ez9zXdosx0ceSfqZz5tu4iO9eP2sdkue+KU5SU9Fdzw0J18oE51D5ETNJiemVRWOeT5EzD1WSjPnKfd6Ne/EKc+O6XfRjvX7p0dkNa/JiU85XYeeOOs75jkaxY6PP10wkjidZ+0jnRAm0FLmXM8l6HXH7vo+1LnkxO6Ljj5xxYO/G93PNR3vtXPJZ5wZBl9x1V/hru35neaqf9W72tN3uPP4xqd799r38HyHt8PZ3d1nrzSap1f+i4D0QcZLaOYVVdADP2FyQV/c8R2FVhxykeUtszgZ8JhgSah+8ouTueGHkdA44DNuYIKTAq802sk/+PRw53dromStRMd8+CGAHdLfvN2HXkZd6oBnWfgJUyM8c8fSZC3YfLLFbMfB0LbY/4h46RON8R7ZPtiPnWs2uH9NBsYOAJsa6H7/A5NnFXpfwHtktNET4n2A7/3E4uLvd95vkAZfSb8P73+Zh3zMGjMFxvF6mtdu3rvSydDvFV2POdzni2Dv/VsfwDUXGPOg9Fo4GHGFrXB0RPF0en2g7/iMN8OuPK96eBGvuJ/03Ff6fq76n/Y0Y6Vd4b7XLzg7Hr7nHf+u715+Lz3f8flU496nvwSYhvELNz+/iMHWQ0cPMfD8qmVxfAGVZolzFNSplfGkx7yMcckHaQpBIkrnmPkY650nmWmR4YflwMtMuGP+8BZl9Mt71OqVUB+ajkvHXKX00htAkVPelIrS6PhuB1IghAFe4M6pPHeazM32Cldz1SuctVTqpYv/EJOY8KsjUhjNuMJWuCzf5i/44g4fJQjVqCMIDlEtz5MagD84E6uLcz1/ynG+cvda5Ve8J72n3Du++jpj70h+9f4dzuYNUHHMNHyG0fae5/QVn+JoXec5fY+r/gqXdtV7w+17/61nS3zau9rlrvdv9HdmiMO5eh/gEHe4Oxz57fKYPdOc/mNA+rr7Q1MCMH1o86EIprrnqYnNkq8ich6I9Nwn70ImHGmVNyzbM8y5ekeKozRPYScpzQDBEyrcH1Dy6A9qaQYfgxxWFzXLuyxzDr+7dGrmxQdPy8BS2/3dMASjFD+41HglcOMxuEpk0PkX+Ns8ceMkvvI6KO9X4698Ec36b1gAsweJfz3fNOxeg6Z9gfUe9b7qGSa7gUeSub3P9IgiOGfg7iPTOKvvK9ckL9nHxXuev/HUnOyJVdeCe3SO5+L0ehfb1e36PeGtuMJXZ7avuDN8huG76q3wK91faFb3dIVf7bjT2+Xc3e/Ojruz4BH/W7Pv5o5/EdD4NV6KrAcYtxG4Sj0cgfVByoP+lOuui59kcuEST+psYawiOOOAzzCRCs8HNznimkep9njAR0F7YCKCM1caweUtyuDDFRhnPHCMm+Kjnd7ZKm/mu7fzx8NKeteEifukPTOdpwa48uopvcLfvEVvPpQDd+8ccMwY74lhSi9nmFfy4sJ7ztzhEUBipfF+NgJ3jDz3bhp6ErD3wNi5Bs/6auWRyOa6RzHeHtzOIfcdpRv4Yf+qozF7f940Mmj3LI6OtUZ9hSepdMplKw+9dKiVgynnOOY5fUXhte7Uo3NU68z88FL0PnuC9Xng8qWn3I843cf7yt1HtXv13owvTGfGPTrna+f12tmr3gpHu+qv8DsdfcVvPO60O/67HPE4O3PF/SVv14sdidK9/muAUeVDPMCMViPQ1oMjnhrCyCv6N7b3Z3xZoJf3V6d2kcfYoXn6DsyaYekx0yLyGRcY3kYZ7xuYOMkDiLlt9NFp4Mw7iY2H7WXPNCMdyXyf084yD75JXuM6mJ3zpVPevEVvpFYu++MHFLNwredslVhreEnfsb5i77l373lNTnQdeZ9FvdI4rtxrtLMojHOlE6f3mUHEp8dVH1yRfKVd9Wf8zp35dw4+M+5OD45HZhBnPceUz7jOudrPeZ6vPFc42lV/hd/p6Ct+43Gn3fHf5YjH2Zkr7i95u17sSER3/CMAVfHSh65+F5EfvlVL4DgP6eQUNzHlrqle6uWfgvISGCchaaqXmHGTpMsuVlz8ZCtp1yfe54rWsBybBtGrHVmVh0p6xQXcZ5U0e3h3TOuJgJ6++yRncZEud8EAnow6Ru9JbD5X+2Wv8Rk1dAAWZz0wouiZ63J1X63vekaesOKfMIjMtNrT1JReuHv0HjrhzpvqPuQwg9jnMKvjXnsOn7jqTecFmV8z036Bd57MxmNW/xrTTn2v1QzhnJnmqrfiP8U1o2vu6pmGXf+qd+d71+/35PuS33Hu+rs+d7vi4/GXs7+Z73sc/whAH6h6VSfL+pDNh3rkxLwh8YRlcVzyh4BI9YteeNadY3ylQ89G8Mv/RF9hZoRN3keZM6M/JBMPQWqMy4Od+8OT+6H23SQ/4RQMhxx4QnERZeiMPzBpijTDsBwRwwGUnlomd2fmIc0Vrr559w/9sXt5KJyOedMjOg8soy5hjDe95NcuJ0yNAPpuBWcvvWbamuV+nufMw5403y4K55Kztzj+fUlf+A5HPD+u93zFEe48z0+9aPT3bsmtYU/7p3ltr7LM4L6erzjgM+4uJo8n3KuZV73ZjNXsK/yud9Vf7SCNzqq/wg/VWnfliXaH88185tx5POXt7I3nE+4v98w/AciHe23Cg84Xy696fTgKV5o8ckCv4ZRu8P2TrelOc3SXNlPU8d03wwM7waHPN6rADPXO+YeZcP8QzppZUYwannoN971qhFj5HrGD+2RzdhFZRD+GDe9I0m/C5U8/Bte9yKM5+hMPdhgc6VQY99TDF55icOEQ3QMsoy7mLbm4/nUqKFuZlyb1Az1mOiZb1YnpEsAJM61S15L79weYuFc+w6tmuk49nRnW8UecxSw8iH2Gaj/OE35V994d3+d47j6ef8NBO/PbwWYcea7wp/PuvFZzVvgn89GsdvlkFp7faFf74E28m/FLn1977fo9udennv/oQyxf8U4S9R3ODwWnPpzayDlD0zgDt7sYnprdZuWvrsDyeCSvlvuyNxGK6uRFYKag097Vy7lq2j5gfUfV8CTRYXb2Dmjg7iNw/ClDzTL68M3daTCPumKflbC4q1M+J0phJ0lgY0dvnIRHY0AjMYF8rMx0hqlRRPgZT8XxHp/s6Beosu+dmIsCeMOkqxdU1TpEJSM/Wm81/DdeA1SeoCjesIcc/Gb3z16KOnB8pucH6/7KTGd2HzhEcVcc94EH5nqwHU6fhab7dR59cK99fs+dv9Lc4ezYva9wuHhTzzQ7nJnOPVf9mTdc9egT3dMxz53j+YzTsV67nnyHI+4Ob4fD3LvoXp67ruO9dq7n4umV/yIgZfmQKwY5D03+XgC4aI7p6Te48iofkqyFFy8nF2lwtQ196avOiN8Vhiai9uR3j+kvnc4YFnnD4AFD58Mya/NVreOzstal8eR98mn1uFdp68hX+7IPHnoPwZJaPOXJUQJBAAeMukfzGa0bbFhGQt538PdncMT33RgoQsfFpV/xVJcGLOcbBo6Fasc8z0YYgBGlzVwX6wvPE3i/H7RE8ciJjt3Ohmzz3Yc275/3Znl+P+rNijPtH61xdU7XeA2PeNWbcRhIj+g+v+LMvB1jDrH3en21Y/foWmoi/JknvRn3roeGCJ+4wtW/6nm/8+7qK616nO4DTqRPBPd41YO3wxF3l/fU172vZtAjMucuOn/8JUB9IOTnQVzqcyEfbllIAUjqmLSlU+TwEBtQabJWrjOakZdP4roUf9QNw2LGY/bglDbHxQU8eTVAH+Lq0xPca2Y57g+5sjp+MMAAUFHmkznCrx7ukubM0qseh4W774w7RJtJePSHG/fw5mB7eG/2/sjjzVei2pn3F8vE1VejncExnAccdHFmPJOc0hm3Y6daBcPKyfuenwZNCud67lTHV7n4s55j7kne+1kX6F+zzkM/YhCcL7xrer3D+USjwX2X2SxhOrMZn+Bp9mO/p7td7f3Wa9/Db30BdVZ70Fe843zb35nxS86ul3g6d/d3sI7rX3Gv9jj+DkAw+Lrz4GQx1flw13YiVVSaXMUo0PGDBD8IgAftmIGPgDjeT6D1xzvIgiKJE4cZWRR2+kUeWMpKW5RDG1evE6yL6PkAoQ4A7uhFolwne0Xw+c49mIdmeGNAUx67mDS73Bmv9NyXyryRzl3hs13FLT2+RLXeTnnAIc72oEc8eQmMuW89w72nFVU7hh8/OFArwkPnWM9V66A5qgLajnCIXXeHe/9K23ur++i87q/+1en8X9eafec548yw7iOOzhN8xb3yySEP56BZ+V7tsdJc4cy78r3qfev9i/k7O+zMgUO8u+8nc594/hU3/1XAMteHgl46iv9TUXX+qggQXDUPX2F65Sk8a+MkX4RBPKXDf1D0LhtXODtkXpfZh7X/QCGbPJVo50yp1QRTGni2JrMF+cP9IL6w7B92tKI6bqM/8OEmMYpRS6AFAqgVj54X4tTh/k96mh4XemY5dYqJUHuRKvJeYy9sddhV/RO/fE9Ycd4w3agd9TtHbbCMunTdDDOdDPheSQ+Z6pSPY55Ld/oeOVTnfRpWZYaTlzUGHvNHbn2lA4+EHYS1W08V3OXXJFnvl6FrLXDg2dwZB74ifSI991Lea7Tcp+vhfqJj/iz6jLv5Xa892Ue92d5d0+d5f9abYXca+t9o7zz+0pvZindzdjlPeOLq7Mx+wnvKfcrP/xqgPsjHKxzyg73uRN+g+U0Kx+vK4WQUD05Enbe6OEf3uOZ8uJqtVxxpOblXFPhlr3iDo6Rh9FbRZ4jDLp3P/OTQtFns0/1OfHQtnjTmeaI1nHlXnFNvt2hzhmyC6yHicK+HthK4xISj8PtXLx9OTezvf3JaX+XJl76BpO4PBh2fjvd7e+uX0HHP8Z9h2bOGpcim78loVpK6JvaSnCiZ5/h1jJr4hCeN6zyXDzUxvaPwWnmvXUs+4+3ocuYHl5n3lU3fj72vNKuez4Yzw+gp3vWd2/Md7Q6n+6re0f2bnN2d/F529vvE12fc5bs74HP81wBDpQ/gfMlBiWN8Opu7foeUD0rjSqcyP6gLjzKx7JVeLR1i9gS4v9XJK7I/BAZuOrz0YT385RXFCasmDwKV+p1Twcc9SBeH31EdRXGMq71zhcKYI8y12j0xGRk3fR1TnoYRWUiYjnDDlHIPA0crvnGnno07ShLXy094YUqTVtipFnemlcbw1ItrHirzLLDUmIe4vAeC5T98VYsQp0cBvsuUYzr1MXEdvtm2vYR7r8/zHnmP8tQBd4+BHZQXx/ji7PDKYnC7Rv2Oee35jOtY53pPuU5ydLGvZX5t6VmMNPnuS06E49Fz5wnn7OLwiOh77H1qYudTX/X/7Z52upq5s/Odxy/82eNu1k7fvf6av3Pvvs8nfDTH/wsg3Hho6RccD3Z/kOcDvz7cFHpPC6FTzneIdHli4sgFeM02cA/F+brgjD0Oy6HBCpnPS6wa2gmOvDI3TIaJl6FCckqUdfWyQV6b5H7SNBxu6ot7GEfh3JrjGHuuPLHLONGf+neF9L6P+IUB54gJLx/KkEom+c6RTJZ45AwXBsDXjl5Gmyf66Cmp3vAuwtv7KHzj4A3Va89P/drD++QZq8+O0tLvOb4ddx/XXvF6z71778qzc8fy9nXxe3vjB/DIXwZx7jQzTgon2qfcK/7VjCtdvx98PF5xrnrfzL3zvfK+63FvdzPu+rtzdufBe+K7s6P7PvF+ymXObKf/6ENRvyDz4RyMfGD5L1Zh9St28NyRXmk0JHmNk228Wo/yNFtGnPLOkgFRDEphzBAvewEI6zz1x4lmclJcubCq0eZD1zlhQE9e6SEMMICBidDwhAxTPY7wmiUsfRqW3BmWjcnFuco5Nsd3pJ3RtTS+wPoPMGOdmafmGQ6XyDoe6RGf9sTn6+0e5Bnj4t8j9LRr5vW+5tcu/Ebfl1ngzvV8SAP0H1qmnIm38zyXr9ervPN67bre+0W94zHjfIut9J/g0uj09+pA1/hu/ytv/yxgYMTVrka55fzC4+renuyy6/OE95T7Cf9TzZUu/1XA+rrrA+8UhUmpi331HNMHYOqMoxRccvR8WIqfJ40isxoosVGcOYf4hfFBiG/alRZMGvESrnkK7JR5XKqVOA9y1ki9c2QaTdcmtzDMTpgkeGRDJnHG4KPM1hVPNPRowbwGU9Shh/YKKw4SafUejFp2hcmGk/d3w5OH+6BNrBp9VgomO4334WRS/jO+Zs/wwtyGP30Qxr7E5EXR9xx9JeGpMLAUXWO5hmlLkgEvX3XHW2Lnee494TNvcN43aXTwoX+gBz7zoe9asO7ltTjyA5tpZhz2ct0Mk9Y53Z8aHvfWNTve3AdaajyZNeuDEeG61nvkzIDPfVDf9eER5buaKQ59j2iJ7KZ6Nl+467/h4KW48nHOHU99He7B34uj837lXuigpZ5F51zt3bW7OvHyvwaYd6KqlPoQ5+GpweNVePYNl65zop1ncIsDPmYFkNro5+mx4FkPnSIn8zYLnnPgCUNPdGxoaq8Tp81J3Qbms30W7zlY57HLo1h732oaz3eRVm1+KBo1SfVV5jGv1AksbNSGpcYuJ85LOjyMOnYqe8YMCrgAzwehEu6N2YMbychNBM+bzqM/xRxse9EiaqTyXoMr6sBxnuNJKp7n8FfRuSzBLPVWuo67Dz30XoONWE3nKKcmDr6SOODEFea4co7rOqZe71MTu8Zr55Areu585fTAPaJ1ziqfeTl31vdZ5LOZrsWTiM45YFccekQ07kOP6Bzn9dx5Xdtr53Yfca/49OBRd0+vO6fXzvW883rtXOXHfw44WPmgnrB5gOf/LTCeSEkRP8Snl2G8G/kA0xR8FY2XDxl68K7qCSd3cE1wVtjYoziyS0w7lUfu7HmSjgscg4bOMe6xY6c6Cvd7e6+c3O4vW7uYyDOu+6/ypvP35iRpvOwZdnVvPHjxMxnQMsLtcSa44tz2IJRxKxN1zHN2ecMKeMND4JjneCn6+7bDSY0ZuMbzK172jGxpOj+pxb3i914OaJoZZ+Yr7S73E/2u92oP4Tqr2d6bzer9FQeeYj/MvtJKs+qj775e/4pztcfTebte8HbugR2ecD/xZw5R83aO73X8mwBDpQ/pfKkbCXUaFqYcfDy8ipu94PHH6vmdYj20ihx55TH/Za3GEMQ3YtOwj/8xJZikwlMeOjiqZdNP4tXQ/Zw4UWTf8QUmX2Yp1z7ycuxkLmMdkcgTuMDU71w0qyh/na57MvfX2raP7PO9Eh6Hlf3rDqe/XzPu4XJc6btvdtQIU/rEN16SX7wq34J2PX2tgzHznGEy+xXevVRzcoYu9fVkJvHEo4h41e89yWYYdr13V6/8um7l/0T/refVDqs97jS7/Tv/HZ/V/e9ofzGfOcS7fXZn/gXvyY6fcNEQd94LuMSuyX8REA9KfQbwkkC4HoCJKRdW9ehncvSUakDyvBDGZOkhicOpfv8BQm2kUBU1YwyrVJC8+4eucOe6oT/gUytumiuJE34q5el7cA+OJbe0wjONZLqPvK/OMHiR/OH3QiNzrnJO7ZKlc+gvsLd9Gy9HGJa1PCPx92nF01rqDd2kPt1T9HWcTw22897AZX6atgscInPYB5w4+uUDTuRLoBqsa9jH+2WXwXHPdzldQ01kiH/d2Um9E6/VvZde7QKHSLvXwjt2V880TzBxdfqcb7GVXrjObN4VniLT8X0F7nHl/WvO1ZyrHnvscMTd5T3h7njucJ7ey6f8v9Id/wgg3POBFnesbyzl+SInOi4sXvrqpLbqoVM94Y9+9Dg5U/z+Kg47+XfCDJPfwFXEUe0n+w5E/sbR3p2zgaXG7ksWOc+wWe2jmJuSrhPRMNdN8wWXGa4RddArGbWIp+JQ8nU/qoMyob1rG0llg7LOhzrmEX1v5488kpGbLn8n3morM5VupnVe30c915ATsx+F6hOmhp3eV+tu1qcc9iCyRt+h9+F5hEPMneLSa/CZ1rGeu8/M4wnWvb3uc574zrTuPctXmhWOh/c9p6/ouOcrjuOer7RwrvpXvR09nKdxZ648d3g7nKf7wf/U+y90+f8C0DvCg1lL5u/yhbGxML0My15cwEXNXIkdf0j0393TSy9plPhdRg0nLb2XwNEXPDyUN5/sGTbq4PJBS5sdfS6/MwLLNdLktR9Y+vieASQ1MOeoGLTyegGvG9LMwQv47T2aYfiph1gYeWmyjIti5qbL96Xq7EmjxDiC3rAJZ+hTULMaL9crTGFoIsk8CGB8zdLO9sm+Ls51r8oHTwbGVakh2S/fzA8420XJnF5qGh9ecnSZ9QsfPkEjz1h9vBR1nnAQ8D3s+p57zYzUx+VUR+F+M92Jj0nF3nta+zy37j7f8p7oV1z2m+12pVnx7/x6/1ufqx3ven0X6qt4t69rf8l94qUddvm7PL+vJ/7f6sZ/C0CL6jMqP6coIvLDwKsZpOoPvnRoIlefB6nKUZtuvIP1wQiPD8pZ7bvJiuMWwvSAYD68fJAWMbFqiDc4pdUOYDlDhQ1Rqhn+IdixrCXGCH3zEuWNk+D7xS3eu4bMZrR2zsQwemNf43VOtmbegfFepI/VouvwA4VqMCXokmQ89oF70hU5e7rYfbgf2u7FLEU4jq1w577lAaxm4/emoVH7e9/3cdzzHU7ne+35yosV6bvG8877Rb3jMeM8wf5trubp9PfuQI/rX/Twv/KGo7jD+xWHuf8Nv917fbLjU8+n3vB73Hn/uuY/EunhOD5DEzgwQPWyXxP4oSCxuGSUM9rKeQir7BxhOqcfHATUjCGwmlQcvBOjUUMU+F3i6QEfPFG7NvldW7VWws8/4LMtP+OJ+7Z/goXDbfsOWS5XAjgq0Q3iBHMOPMdsV8Fp7/NmmguM+/Y1RR+nvMcsNfq8LzBZ+Wzy/EFPvn7vbQ47pUYX4w6fgMmJqVvwNYID3+uOeY+c6FzP6Ss67vkup2uoiX2G+856rpv2RZi8z93X6+7pPfIZ59/Ans4Xf7YXPlf9T3U73k85V3v+ldfuTObfvV9PeX81nz2e+v9Kp/fpP/8TV/265IeAzFXzUlLv6P8ojzMe2mjhREyf4qHTAzcp5UM7vbJRI6qRobjHwLhWrQdP76uVD/VIXHYMLX4aiXh4dd54cBQl6cbNmQINs3LQ1U/vuBR1LMAMHp7U6W26l1lmr5tK4oFJq4PXuHHjgO1wcln58caUD/fCLNryzpx51dCsFSfxuPBD2YnnOu1RL83l5PtlPI3uvFErMa7KJBeWtTCdKE57Cyos+3WRRq987zW8jrA82TynaIoxaEMjJIo+v+DBn92rc5IYF+bZeonBXfnQdx/H3nQaFODpPgLqtXuUZHDgdm9q1yrnoKNW7FivZ5wr/YqP792On3jfafpOvsOOFg73QN3jqr8zb8XRjJVvn/+Eu+u5y/NddjQ7HPck/0T3iWb2XrrPP/lwDkQfajxUMg+lvpgjl0ocOcbJniLajrUa7dBVP0IenwXWIxzH8QNTreORfInr3uI4L4HCM1dfdcO4L+fc+bx5SDzxxXPEzvlUN/EZD/4alvc6Bh/rdU62zUupc0ZrJGZo6Uw380me6UZa/r1f8KApka/jQ2PgJSaPchy8qhXc/9SPglrRT+INHGUlozahY8p5iUKPiMw5YINfZDTE0XdB5N6fcdRfcRz33H06rt5fnNmcO4w+cbXXrD/DXL/qd7zX8phhO97OWeUz7471euV1he96/JrnO+1473Dck/wT3ScazbvTHX8JMIh6aPlLYh4C+btrAeGWD7e45INfsXAliakWKG42BcTp9YGODZNauly6aaGnZxCck1SwII65SSolfoHlmLgAJUP6wiRLTkkfBwzcx7D0a3XuEph2GKdxZrrB9WSm877yDzir90RWT04+3Os+0eJ9qqPg/RCePV2CnPmToegP+VKPL/toxAoDF0dHNfsmYJfOpQU+mycOffhExz1/6tNn4EVkXo+9/6Tu3O7dd6Lfdb1e6Vb4X+hXu97hqx2/1e3odzmz9wvtXdzR7nA0Z5f3lPuE/2QH+er8G5qnM15/CTCU+vDIV1wUdTKqJ6xAHv4Ho3EEagtxy1PQqAtPDJyidDlGuU7NJD/BVfChO6zNZ/AjgZdYNXRPcNg7fQLED07X5U5x0QMtueal9yj5gamn4w++A8jri5CkKEvg84ZJcd5q4QxacbxfHN4Ttd72W/igUXt6tLjfQ5R45z1ZfdKbbuAXGF7i4p/3oTrBiP2ehddB3yN9RXxPWDbiYt54MB+K6zqGxjlgxOxF4ffhvcx1icFveDe+4bi+S39V9xm91pxdzHd6qtudMeOtZrHPTDPDrvjf9u523Onv7ADH49W9Ok/5LneXh/8T/hPuk53Z5a81T/f3vf7RB9z4nXt18kNPeNSj13mtFldfzfhfnqGjjginoJN/zhJHvhAUMaw8+44F7tjQitO8xAM78eQdJzHjHGhc5dO8wAZHibT9NAyfMV/8xnmrP+Tk19H36XM2fVc+4x7MF+xNY3sM+kiOtyDLwhRo4zW8a+9TPbsXw/AC8hof9Tjed6zjqjsm/hQz0NK077Xv5D3PU1gXxz1Xm5roWMkHR8kWr4TO7b7qed/zzqWecXYxefjpOvWErXDXwp1hK/0Tb/xnXsxc9VZzXHenfev7N1wZ3c3p83b50u1yd3nuKc3dwXeH615P+dI+0Tzd64m33wd7/TP+EmAg+h7Qq2Mi85AdD7CYrFx3h65rs9f6M2x4ao6/5G+n89QC81wevOunvvjWU5rzfA45UZzK3Uv+CTvPuHgrskvmccFPNTkPOefkrgCKNivhXi84J5/QSHaSdqzX5XvSbXJyT7+Eru/j7RxV3pnTnMzTexb/O50Z5iTx+3udmLngSZR+5MabgbP50g79xIseUSOUe63iVE/66BQ5aIjCPZ/VXftpjW4Wd3boHPnsYrOZK2zXc8ZbeYKvNDPcMc/xUlzhcGb9GQY/4+kX5NG51ZTBLs/n7Wp2ee69k3/q+4nuieYJV/f5lO/vDdr/5EMtKh5u+l7Il7DKJeR7JGP1tMGJEwV9JaMXPE5iVh+C6IKJUL6pAcdA9YwjTMe1BzIesuyUNHihS2nUZp1KHs5lc6xYOnERiJd18xJF3vnHt0koQA1q5RywXChAavU7tqrFlY7+qha+4EieD0nvN5/BKZ/xULXZSsc9hBec1KpXB1z7pCZwjSYXLTW61E5Z0zDd7I/K8SFKJkO4AxemnvuprgOPKDi/9sZPqggdy0b5X+R4T2PzhVN2b+/Xp7h07u35qdf2OfVUxFlqj/aU86muz5r5PMFyubjMfH/p416zWTPMNcpXBy1xxrvqOX+XJ80T7v8PfN6nv7zvp96rndzn+I8BBcIPACmKD688YkaevYrC+WfUoqmnk1jxs7b8rcZfjdIrzQ936uJkMK9sx0U4H+ApRVd49g1zb/bHK/V1EZb3FAYuV9FnSpIkNXQQUAOlaekrTz6X7t04OVeYDt6Nc9pDvEVfMFYrjuR5zGNo6E1ierPfpC8oOdbb8YWe7wPFIsLBdxVTrqbt27k+Qj+kZH+X7+LK8Vc5y/MHIfPHYsalR1xxTriK8HdMeq89771eJ1eX2vlWa9zupVqneyT2qS4dz5ep/5mS1YwHbdVb4dKtek/xux12+2OnyfcbHoPjwEW+upeV5Cn/f+M+3NuTe3nCfXrP7LOryx8A/Hsg89gwoy7KIyrlwemYBmVPdwW/QH44UKkeD9bTrwZpOHioVq4T/UyrBz3LwkRLDtG44Jo9fmAoUPdDXx4qwPhdqeZJlzFyfFRzxEWXmEydINAx5TpPOVd8+YWvdsX+NLP6Cqe5pUlcF4nDQ6OUvj2Uqh+tcU4c9XVs1/Q50OM64by1g5M0v5/ydLly6oxxyeg6mQtsOwErzk76RIMoOTnRdSdMRdvXueQnTYGOeY5GEZzoWM97vboP9+qaq17nflLvaD7lzHTCdPp9rbAkP+TPvK98vu39Sr/j84TzCReN4tX76Dzyv+YzR/EvZz31Zq8nuvxHALoLfSj8T7z0gFOuB1rG5pp4cURIjk1EN94ZcUTSabyB0S/OeFBXTTvlKjQ/Yta6RD5+uDjaRy09YuXB7Q/ItMOrOAp50OagNjMIYweRi8Pu+YOC8Jl3YLLmhwzR2NN1iZfvIUjkuPhu5OrAV55DDijh4uWMyAdVSffoNX4R2dvvnx8CJJMddebSRqJcczqn2qmBk1EyJbWLcnBhyrMWDkdmcU461QnGpXiU2XAvGs0P2CO5zxoYifnkDsLjeE7dd1xxuAX6RL8XPBV14EirnFo9nVMdRX8/D1bjdd1NfZqB4USjlnM97z3VOp1zoM/wmccM+5U3PoqrOSsc7V3/yhsP4q+8dnyY6fET3b+l0Z5PZ/01n/fu6Rx0iv/orvShoIdCvpSrQ119Qc4ZfeG80FB3DXXFCGPWyAvLHQTGcX++ConFvKwV68AdtfWEcY/0Z9jwaFpmo80PfoqIYyfHwkP46QTWrd+ATui1G171nOd50/R76XVKJ5oTFIXqE+YzK++cGb+/ZzPNJzqt8OYVQPd640jYluqamfcMm+nE89M5uU/Nz7zIb7wOuGnk+rp2yqpe4Vg+7aPz2D3Um2FornpwiCvuDN/Fbr1nRogWcSVZ4djc9cXb4ezydrx2OOzv8RPdv6XZfX/8fv6t/JP3wHfLfw8AD3Ya+p1dftaEe+aK0eSlRLn+3wJ5qlYurzyG8V0ojV4cuJrhHPojliiD/Itf8KGNgodWtccwzWGt3H0UgZcJu2QrsILTM/0GUOMXdcLMK87wrjpnxEUl8/Ne1SjObZ0mySojyyvVTc/8h3Qy75KPr8fuYb38DezygwAAQABJREFUesT95H0a7ikcx5SPHSn8felk48x0Pl/9E6e88nukZqw4vuvMY6ab8XIBm8X6tcppP/QZdQndCUMUMfHma+1pipeaq7z3flHfefgu4l6dGXcXk+8ud8Zjr9Gr9x/8Kg7NhHTVE/3bfnps7no3i/V/zcO3x905rnuqecpn1qc69E/ip7Ok4zX+EqAG6/shX9HNqAtTaIqjvmqd4kqQFMPVdl4SBPbjM9RTXX5JjTopYL2WJDCN5gGWeYrSYfjxIZ79aOUHv3Silb84J5/isX9y4yIPneQmGAVgNnSpUwOzHRfoSvQeef3mkaLyEdFr/FcRYzQP5qXU5inl/cu8zZxhurHEa76/t0Mugs6C4+30EjcS3jf6qyi6TvZNN7DsHhc8tAo5MbEo+N4w2eDOdDOeBPjgL17muoQRODH71VPu58SxhuOCvfbcJG9p5z2t3wzbHrP+Dtb3QDPDv8Hku6XXF9/OTEN71Vvhdzr6inceO5xfePhO5Du+cHf2dG7Pn876dN4nc9j1E+0nmtW95T8CyGXCNY2JAer7mZea+eAHS3JgQUhO1Xz3uU7+cEbsfuIIKy4+0uoIT4y51Ipxsq9YvmCOg828c655y8fnSes1vonHZdToDCtbqO+xE76pbX4f1G3VTywuY/8CT9xTgeilyR8KpIvD+9YlR/e4Mss5yr12fuatCR84I4V7GdY9veW5z3Pcc7yEJd6aoxzJ+f6AifLz3P3JPTrX8yuO8zyXxutV3nmzWlg/3W9Wd8w91OMF7nwwxRn+Dbby9Jk977vSB5/tszNnpev+1D3ezWeHuznwuv9VzewrDj24O3ug8YjesZ38yTxmPNFoB9c91aLfuZfOWc36hwemYv4LgCxmL5z0gU0PTHcytOJ4rbx0A3cOuUf4xPKLMs+YG5V75x5gNfdQHDvRF9Z1YJ2TuHlJJ078L0/WygAO+MRJSH3nUINRi+yY18p1nLuo83fXSS47PAtjhvNGK7jQFfV+64C9kgO/rRe0hIfpQWplgm/zA2Vv+PzgQS2hcq8TC8CxzqFHHD4GkBLhKI4TTfU7h73h0ScK95x6F8P3Stc5VzW92Xx6ir0P5rjnroXr2BXXeat8pgcjzuausNkc93nS/0TnGs997gp3jvId3g6n++7Uv/S987rr816Ixwts517+bc4vdrx6T17/IqC4M33o6j/5m1F1f0V/PPyKDyfKfEfzj/xtYvKthjeiCPQtn+nyoXDByZZ2bJx8UDRszMxFYgXtYJy+n1rSnP64v7CTrHy0q1Idz6kV6WdyMlE3DoQcfkCDW2Wvc5Z7aQ+vXad801ur8B6xVj7YSj8w8eQbF/6IW6XX6nevK07JD98kHpe8LZnZDviiMfrQb3EQ2n1olM5MTy+H+D6HZFwHL43iElzHyInQTlHNGx16ovQ6Xq/yK17v9RpPYu/PasfQEdXjdKzXK94VPvOYYfIAJ+LrPceu8N3ebBYz6BHBPS579j3q/FW+9GmCXZ5kT7jOf6rzFWfaGXan8f4sv/OcaTrmHp533qze5f+j7wN/yay+N0b0rxTc/L8MRpHcmoYuPQKjVhyvwqe1euUlDx3V+IzaONlTXRi1++Qs48gnj2Ho8Cm7Yx8KiUxzmBQ2ijUnZ0B3T2m99rz3qj6tscGXLM+Ee4KqGNhIzl8H7H61t8YwynPNAR9foxoOXuUrLBsHBR9oipkDRH36WtE/5OMKnZiNEoKpnHnRJ0rreXo1DJ/OdZ1/3zvuGuHe89x5Pe9191Hfd+z8WT3D+j4zjrBvzmx3+c1mr3A8Zhp6sx2veqtZ7nOnx2O2V/bii7TjcefzdCf4u7M/5aNTZBbRe1c5fMUnB90TjXPRP53bPby+yv9RM3/RLiaPB3D0+cUNRp0etjE4vFzA+vqq0Buzk3RcTj1BxTfK68GsXjTw8THyye+AEnZO1tK6x6RO+Yzj/uoXp8Yde48iEvH9WJ33YHVye9207J8054rX69IKfmsZkGlc8H6zKq5CpeX8XmcjSPod8+BGkveKfjTKZlEL5iUmedLjkjFw9qYe3AIUvKc+BxwONX1FMO6JGg5aajTOO+3oDRdF7i337TiywYnEOewAj/kd73X3QL+KY34Run5Wz7DuP+OsMMeV8+qes9q19Ff6Gdc15D3e6a768rrr33F29Oy8wxXnCW+H6/Of8LvuGy1eu1GzPpknf7Sf6r/xOP2LgGSUD01FveKi3+lrQ/6yX9aFKeQftyvGSx+Ko59NgS/OeIf4BKo79j+yl0xm+cesSuEKF79qS8+20Sjb5IouL/mg0T0qz7rwl+iYASeqoR0GAv1gLMzzjTrnsMMd/6H3aRfTvs3U2tZnDUF5qqeQWNX53gqrunNVD03lHet1+gc4vCFcvUfi1BnzaqdRQzCeljv9Y4rone4FTXl5qVw76qxmnHrmMeMLg+/xKk9BXdB3/lXtmhmvrEeAT6TR65lX5/R6RzPjfItJr7Ozz8Gcc+96M380u/HK46on/7s+O+zy/sLzyWz2fbKHa3r+35j96Uzf/VuP978EGO56QOjDTVEna2GqG6aaD8LxFwWl8Vdx8MQ3KHnSnzzi0ErnWnFsHjx2UntgKuoIc07C4cPJPsUiDg/Tidq1qk/3J75rIj/1ZeJ91Venc5/WeHcdOLH3ew2vRdHyIdrwU9m9qh5wJMpHLXEUp/famqSK5CmJwmthq3PSNl16FJY5Jo0n2O9d3BO/6jesAVk2LL114UR/QnnDOoeaaHakGVf9FX4St2JH8wlHY7puhbWVTuUTjxkXs1VvhaNTvOPc9d2r599ouxf1ruevecwn7vrDn8VPPT7VaYdvtNzDtx7Sv/4SYFT58AqQvwh4+sDV1OIozZ5pHMtcBE1YnPEQZAjepRty84Aqy1kONXvD4LWGQbmVdjhpytg/xPNPDiCpHzk+RAHuNQg55eiVtJDDJwuZ6DBjmB5w9zrVMy1e0cNSif9FwPG7XuHiBzfHluDUrxnZ0gXfyFUmLqx6ssujfmkV6CdfhOL3mtbAi5q4aZTi6b2cZRpq5+CdMS7sKY5Oeh/pqEdS9zQ4Si4w6USZnRMehe+RPV3MGw90xI6r3u2teB2/8/ykv6OZcf4CW3l+gkujM3sPr/AU1WWlhfNtf9fnr3jyvbsHZhOf8tEpfqP9hf5bj2/313yOvPD7D0l9zsA5YjSF6yFIVEMPu/FH82VATWTCqZ4NYYE0jkvUCdVMweOBXJhqWfGBmXlhakg/HsilwUfcFEdwnvLD9OWNr1rqpTZSf/BkL8UHx2vdu1q0s9cv3syBQWCQuOqvarT0net582GM4DzBFZ338zTzwqfUI+h9GR5CrZbNybdKYMXZSV00FMlPPIGz+z+RXtq+Y/fsdbPJEo6PdYxckRyh3p8T1ji9J51jnqdnXRz33DnKved57/XvkStu1+7Ud5w+T3ydGT7DnnLTfHJ54o18pVnthO4X/R2PHc7VPfi+O17Of+KL7t/SMM/jJ7NdT/7U5ymfOVdx5jn+XwD8ytKDUzkPeX0QnDDVmiJOhPzTAmFVg6eueAr4jMiM6okzdrBc6cmLOcIj54ijkw/nIx17pq/pmFO0VzDOm7fNSkHUzGK2fE80L5RTRxwapjsAj95dfbYeY17JYcS++RDsnsyaRHSjFVqXe85tDMy4A8PIgEyN6+8/dH6YRPY2K4jq0c+cwvA3XXCMluPeOM07SRMMHyI8RTC8Hes5HDTq+3Hcc+eQ0ycK9xweUT3v99xrae5qfDMWuWtOnCi4/473euYjbIXP9E/40s+8dxae6myhX/R3PHY4ttZleueFWLxd7jcaaZ/O6fPY9b/l8+lc7mMWV57vfwcgmPnP8sOFh7ri+Of75ZS9yOHku161cv0Cdk4uJa1eOuTGFZy/8IXVS5jOmKOcl3F8HvPRKXLkM2YX6N6CkqNEXJ3SjAchc/EqTvqU5uRhvMSjdi/8fdaYneCxQz64W005PGo++pNGZPpKba/0sb1mOuhpkZdUpWd6qXS82ics+qIM2kgKqzpDXKyd+55q92k5oxX7vcij+whw3PP00CW/OZUcZ8qJlnAfcOJFcaoPK6cnsuII97OquWf6RGn5WgkD9zz9A6CXtXGpFeHMYsfQOT7Lu69z8HDODoYHXNUzzPvkinCJ9Kh79L569MEVHV/14c/6eMBZxZXW+XCI3vPc+547R7l69Imd0+tPNfJhBrF79xpej513V3c99Z1u1pf2G708XU9OnM18/R2A6OYDLD7k9Dk3fggQziuc+AzMWM5gOT0KHn4nPDzyzLbBd+aHLmL6yrT4ojNLtOk8ONHkg09c91CJ9+Ck+YFXmhrlnFzFATV6HVCtjOyInddrsQrrf6zOje7OTx+RsQxf/vFEYlGfZiSzyKbL96bqlS5X1iV4meNVUZjjmevivsYVmT82d67yrJ0rTKDNdo6oqSsOtUdyfKgzhs7fp+Ft+MBCoNxrvMB6pO9RBszs/NmO0vaDTvjIldR7PjDrJxaXPlseOq45kHtspuleztnJr2Z37zvuHd/3cS44cWcOHPeZYd3zv8XxPa928v1c0/Feu6fnnec1vB6dM8s7n3rGvcLQ9XilWfXwWPV3cDyI0ng+88g/AVBDnwX2GZzKfCg2h+QFBh8O9fjBwXT0prHzzDv3Uu0c5bxEiMMOmatWUrrMrVbKHnCE5THf5FgNJTWjqFlWn1L0ijrEo7qvnYeX+VzZZc8JnmuVVvtu3vOc95W1MpqPUl74Ub/5FFceaZGXV16l2gW+3mu8+NpSo+n1YTJsxvfH4EPoMQjOyRzAuEBEtTyHCsbenUcffu9PdSHqOq9nuTDHfV6f+Yu6e9zN73zVnNneK2yF47UTZx7f6uR55XvV0+w7/S4HnuLV2ZmH/glXmqf8TzXf6KTlsK/ip8c9yD/1ku4bj+NfBBQO+tCUkR6mpwduYWryAPCo30mqTl1EHfKTl8z10vE8ysHP5qsvnHPKAc2HnfLDP/Csxavc6xmHUcOHGeUxdv+k7l47td3boI+bCKQWzsDyEL1WTk2c8cBmsevK8g0OgBWzV/UJk7+aSRhB6NspCtS3vgNwHTvlt4TYpe2EhMh9yBeMyCzVYPg5H94KcxwfMGqP5O5L7j3P/T5W3jMPuN5LXzenWVEtb3sOtXPAPc44M0yaJzPwWGl8B3I01D3OvMTZ0a20zLjrMwf+Kt7tgs/OvKfcT/jcx+4+zt+5V/g9oiX2/tP66f53/t/6jb8EOP4Zf03k4Z3/yt/A9KHBa/wKi+k8vFOm2vVoCk99z9FElO/JT/rCZKucX0VjFzXAxamXYyOXF/2I4GMv9yEvjejJz+S45D5Vy6P3TzV+xkuN1UrziMuZ6EZ7JEH2XNqoHRr5SI4Bp4eU9ZRSZozLwKrB/lUehu06NMKLODATDqz01jqQAE6cSV3SDKf7CsT93MdxCXv9DgSnSM4F4z3JJeriPGbMMHoer3L1+sGX2PuqvbfKO2/mM8Oe+InrfGbOsNmsGda1Mw7YFXfVu8LVu+ozdxZXOuf+hBPfoD/xqcXkteP39D46/+mcp/xv57l+ln+zT/fDS/Hbc/2XAOUeU/Sh5j8gjJp+cXhYq6+XDvmI4na+19JUnXr4EamJeOaOxTtYcW06au7H9wNT1BkP9qoPtO4FjkDlzoncd09f72OElrr7yLbrqImmHdBIaM6jvE/UU6Hh9epy53luvJU39Nl90cMm6wLpKbq216kNMHEzUu2n1/ROugBnvM5B67Hr7mpp4RDd70mOnojxqG1W93WOer1eYe4z09DvvV7v+OO14s48n3JX/Ctcvauz2gvNXV+8/22c3Z24R+LOfcD9NP4bM57s9st9fu31n/+rfwgQrvkw9fh/DozvPB6248FZSYbS6U0ZD0/lAman30XpE9YlhO4jC+ak3Q2n2sfqUbCH8KGPRLj+klNGmidx9KJu0OHBFd1OnYMgtug+fSfpdCaL5F/SAhcFn5pFifz0ZpSvOKmLeuiHIBLhquuQizty9cqPPdXLly70xKuDVmXmceEvnSVWOniyUE7tnMzVKw31KVYPPVxqcWWuHRzzPDkH7cQp6Qlz3Sx/wwTMZq/wWubN5wHO3iU57d97vfa5u/ruMat3sdn81R6/xlc73s250+3o/xuc3b3ZjXj1NYLT41PNU/6387pe9bc7zDz/yle7su/4RwCnYXzSBqj09DAOZWLVkw6OYhYN8/7Qmo9k2sh7gqiVZ7+2Fq46P8BLJ8g9VCZPcOkSKz49acYLTfFXnOTLrM7p/RFm85JCzazSvfFWeOiweCUHecwehOJajW1Gxz03ksOZvwGnt2wo/X32HAI2ip6r7++19wexNK4jl568/7sC1NOhr8grcRoq4rRyCs44YOM+DmV+7/l7IR6vopxm4vMrDjOI+Gcdhdc99xr9Vez8XkvbsV7POCtM+OysPH+Ba558Zl7sctUTp3+PoCPe6e/my+dXnN2d4BF35sMlPtU85TNHEa3ip+cXHqvZeK/6n+L9fv8Zv+OqTj5QIlccr5qmb9x8iVsvMFEG33vyMV1yqu4e6iUWgZ5i4sLiwMEzQePkhy21/DhgqoXXSz4cvFWP3D2KnAEPxLOaXkWfNVru7x6Oi+x15F7i5e9TYkXK4HkJHHf+8COJeNq9vMCqNPaR8uAb71fA4r7xA+j40tvEM477ZG78TONyp9P2t5yJj+43Z8zu0TC8NQd+z+84ruvaVW+FS9+Pz1dPWtd7Tl+RM+vPMPiK6s84O9hKL3x1ui+8Fa7+tFdvlnrTfhnTX3HoF30aVlon/4qD544fXMUnfHF5ucdV/pTvXt9o8fmFB149/rV3n/ef8c/2YzJ/4S9jfFPrgaJ/0x9fUaX5O6zAMy83ftclnvDpp6f5pCzq0wNr0k9eeI4THH3Iiro84qjpvNIJlpY/Ns/cavV1nKNCe6bnaCbt/RJcHnonqgx1hknkOSTRccl24fnH0I2f3t3LfLgveQ+pEurKX03bozjsP/TaTr0Acqcos2e1KBznJSaye0fO+5k+rTfz5r7Voz88GUyv6uSRR5HvDXXnihx76GRafOUcdlANXdE5yQ3AZ8HPXl3wci25olZRBEtZFLz/WdflxDHNCkfr/Z5P59/Mm/muMJ+34gjf4c04U219fZ/Om3phEnE13yhLzp32rn+329sO7T3wfs93Zj+Z7/673t9q0H8yD63it3r3Iu+e/Jqj/8vYZ7n3+BcB6QeB8eAPhj6k8//iJ3VspwUTK3XWysUrLAO1g/LwM6mBUkYRGlIeGuonFpfkxgWO7wImHSdTq8Hxpn4ZHvc8PnhDO+YHOecHxgd+zvJ9ipMf+MM8krZDzk8zI4ljGA+N1BaeNsVTECd3KUxuog6tgDjJPdLXKqap1ujB7z5jlyDwHok7cGa4t3Ghpqa4YFYeqXsEonLoIuFrMN6DMhgcqx0j93sTBi4ZuXsnRxf/WojrXzPTRprHvbrWOfBOmM0TPuPA9+g8z2ceYJ0HvvKd9WfYju9MN8NmXjOeMM4nGrQ9rryct+KscLR3ffF2OE94T2Z/4vtvap7eC/x/I86+bjPsF7vc+b7+XwAxjc8uxfxhINT60NMDKv+kQBupbtzEOl46uPIYufRR+5nWpYEHRz46adE4Cc6w4KNjF0yyTrMXJ3tc3E+84tI+Re+tcgmiRzvjqShHsChzd2pi0Txwj/g7NXMHbMwEzq5rfIfBj+SNYwupN7iVew01eZMG0KqPXjE5JFWr5ODFe0SNjhq+Ihgc72UeDefQB6NWBCM6NnJvCowDxN6OJcE4vYe24zu1OAx3H4OTsqp3NDPODjYGt6RraQu/6sEjwl9p4K0i+ln/zvOuL88dzhMe3L/wfert/N19pPEj3ada+aD/xmO2z6/83Ntz33tn1j/5O59y4GE/7j5wPnQwU80Dkx8K4PBgVeTARedfFe+JTz20SsILv7TVpTDmsm/qo5148WQxmz008EU0DTMSo6fIgevRe56LowP3qI6a3CN8YZ7HjYyykgwDdJOmfcBZUZv7e+nCyFs5+Hzdsl+kEyam8OpVOH8vBnjSFAlsDDtbue2gSOM65vXYOerDGWYNQ+O8GbbSuw5Ox/zXMBzF5OkSrzeNE1t+x33al/2dZsa5wrrfiit8deQx81nxwe90V55XPfnv9O84+Ozw/pL71Fv8X5zd+17N+lbvvvL6pZ97e/7pnH/yj/nlVFvysBU0HsjlfnrASgIurgR1lA8tYO+JQy98UgOnfFXCmfqJVweefxgmZhxR5cOhjwYPvRcjF7nq1EpPjRFRPU7xKJfRNRNStuPCrieKa8mJRdS9DYi8AMdFzxqQaLgg54wavOKQRjI4AzTMdJEexzQmyR7/mMGo497euG8Aqog1Q4hoTvVc/dmBo68J3ydgnS/ce72GD4co3HPqGYYHHI/eI3cP5bxmOufu9JnhsXt47yqf6WaYPFb4qrfiC1/1Vl7gV9qrnvQ6V3MPxt5112dnJ5+464vmKV+6pzsxi/jJTLSK3+rx+vY+8LmL3875R/8eAB6u/hcCwfQBl38/ICY5Bt6xrEMzcOmq1s2QD55x1deBw1cja+G1w+AUJp5z0MHLujjCqDOvWt6czFc1eERS6TT/5KsmL3pEF25y+CHlNLT8hl0kaQdgkXToByCTOF5HrvL0w0OS7GJ8S4fPwEbyrh2tSJR7DXtgAqxIvtXJjzpxE3cKLfcSdtKpLqHr3zilk57T+TNc2Iw3sEhGXga97h5ed65qMGLZnkLvPa19B4y7x4wzw3Z1My2zV72Z94p75+X9Vb6a5/w/5+QH1Gvi03k7/Jf7Z9m3M/7beu762z3wuYu/mHP6S4D5EI1LPvBjumr9XQAl2YuJijpZK6ktwAVxEltt2XT6wHUP12LhmGY4fzYTHT0iOvqq+cCHw32NWkkJ9ANC/mUvxRPBOJW++YrfRVaPdCRF77V8/EZaPuZKlzf44mevMLXHX1yLYuSB56m5+bvv8kqNmlUTO65ah/dLc4c/2uinrmqF0zE8e3EZHkEc96J8UssrdRtRXJ30IYn3CX026zI4BiamC18L68mEvYfWuEo55MRdXDzXeO69jntPuY5zPO+9WT3DuseMs8JW+K6n9Doz/tFZ9z7RfDqLXe70n/DQXN0PHJ+/y3eN+9zlT/y71zdavH7h8RdeeHr85a745n8MSL8y9JmVf/EvIp9fHpXzf/fTB3q+ips8MGL0xglMp+vyVyQ9EdAqim/YiStNvPLDv7jelwwvcLyyToJxrmrrdS0tZpzqUUSifXWIPfd6k+O0lDtg+bhvnxF5vnfCdMQ3jaXZ9ovrklfkN9xEorjnyEdi5EpdM/P2viRY8T1KfYpRqOalRPk4vR6Nc4JmfI9G2zEKYfUv2hwGwuACek1OdI4wx2e188kvY5jsesrnissc54D1OOOssBW+4wln5qGe8FlvhV9p1NOZ+R2dveuufpfHTrv8XR53I/5TDTvh8SR+Oq/P+GTn7qH6V/u4N54evf+rPH8AGA+JmKaHtO4oo6ZUPvCA+OAbEY56ASbumHDVpeW7hTp7xQ9KnvzQN0xcHfZCm2DwdJKj3HQn3DlqcFwvrOpslx9U3gd6sz3ooTnF5u0lPH/gnXbhTRCx7ZWQm0XeKdk2jqWvMYCKlWcAF+y5BtdxHC09RffBgjj68ATE8f6BrMCDKz4aIjpq3kZqRXK4K87pHkuH1n3A8FMEw9uxns84jonvNd7dp9fOe9IT9+7Iu/tLM8PuvFa6T2bM5q98VnPZd+blvW/6d7N358AjXu0ER/HqPXGe57verlH+b+t+Nf+vfPD95GuA9knk/R//CMA/TMY/AhBLjWIrzVdhgv1fFBRlnvxdGBMAK2KZnuFzokXNA150ejmuZgonZc7wVDPOqQ7yP2WEDpL02To1snt8uKpf2gpHs1+7flZLszIRf3KGTegkPcmjqd0SgwimWqf6/NHzAdoVw+KrTEg6eanWBT+TZtN0o1V8hZyPzyC8ks4pSRLy3tzf9khd2eDRdxycSLiXw/i9RiuN3y/e/f0TrtWSL1MA5XFSd6TLenBKS0288inJa/5klvtceSG94l/1Zt4zrHvMOCtMuM63HjP94XxcZ/0Zdqehf6cVb4fzxO+J55PZT3eAT/xk1pN7Yc4qfjp/5ve/1Wu2K5jvTK74+vcARMXvxBFR1+dwfugNrPj6Dlaff29APsDlzGl56qOXnqUd/lYLG7jy6qWt8nidfvVYjW5wMym+eHGSU5r07nqrk1+6FOtC3XiJew8u2DCoxPXOidzLZL8BgQoD77FGOCcp8JBSE9FZVGv8wHGh84ctmrQp7wyVD3t6CxyezwcjIu2cxGkGWamVI3dMnr0GA1ckp+dR+ex0nTjug6Zjs3qGrfzwnfVXPq5Z5V07859pZ7oZD2zGn2Hwn8aZ1wxz37u+c7/Nd2f9mud773q7Rvm/rfvV/O7zzb3MvP4t7Or9/0e/29dXSA9B5eNhWNvx9wLGA164eBHAipoY+ozREO+UVy0NPeafHsr0RbS9UiMszpilPJHXrCybriipI4enGSOvNLHCx670Ko6dHcerYQMeyUHIUhd75e9EqaPF/SVXspFEavnACzvpCstgGqXDIwpradKrl0VCeUlekYf+1X5ljaMSfrVeQwMYmHjlokguSHqvE4NTjVlfvNN7gkaNycFDkbzTwInqs1/H1GN+8nSp41wwRcc9P3FWDSc1L7W67EnduTM/Ybtn5bfCZ74zrnhX+Kw3w3zet/2rnfqcu1l47fDg+oydfNfbvaT5RCePT3U+/5c+v/TiffnVPfZ79vpuxn9E5gNJceSh5I/YwVXroSsSD/UU1JTkYyDj4irlK8ofp6aPYHklIXLNLIwHRNbqwwsOtu4l7GWkos4wP+rxx/4pCEn1s4xL9lWgk8zrJFZ/lbtmxbnyZCc4EblX7UKu91AU7ZqxcsaPvSf3knxdvCchB0P6VpOK+pYXwI4zjmtmfWHjiFw7KKhEf6qNN7QPE3x9hizAmSdA9wfuYxxb5fC9D6bouOdPOXde3fuu9vmzvOv7/Jlmhc284M56M+xq/op/pbnrXe1Hj3g1/5dzmEe8mwuP+JQv3Sca5v1C/ysPdvr2fvD59V7u+6n3P/pq5cNcd0keqT7sdBR5WCdQ9eibJr/y1IoX2q7HW3HsU2Byw89/KDjxokfNd1/OFkgvUnwcU46v6NmTxl5Dl4S6lG9WngeQfDUc9zxF1S/cH5iuY7fT12DlxUw8NUJ5vbosa8BYmlSJ8qwHKPM61bfySJ17weH94d4kdulpvvcQHtPy3tARoVDjvaoTtyY1PjnK+vgpOod7cSy1cWnyrGeY+I57PnoBOu754CiJ4z3Pey/JGxf38Hzl1zmMmOEzDL7HGW+FzXD36rn4TzXucaenfzfjru8zd3Nm7/LF+4s9ruZ/suPM71d7/2ofdvzVXvgpsuOn3sffAQgjHrr6AOMfBWSsnnKvNTkfSorimEekx6mt3Bse313UcCR0LOcYxh3nbJGZoVxHNa+q2VOljs9KoPFP+iTExTnCou6+SQ1cVPJXMcEgKqKr/KRTD7xski/MjjgnWtXDi6aiH3BhrZdlXKCMdmFuk3LjSjT4RhQ28JEY1riQuT9rp2hYRKJ81C2XbtXr3vCIaGf1DBNfw7w380isSM71HJ3Hns9qYTorr6N7XMVx3qx2vueuc3yWwyWKQ050nWOew/kE+0TjezL7SZzNfKKHu+sjHtzTby4wuolob2hjBrxdHXzipzr0xG98XOs53t/EX/tpl194nv5zwOPBG+b88/3Eos6HpmK8xokN6AtTzgdp5nHJh+QQHIkWV99P52Xf7nDwA0tYQPWlVfrGKUw7cYbMsOxFPXoCqj/D8IIz6lnS5mTZsJzRsOEdTbXyBWeB5XhxTkuPWxntK29mJUc+cRg7isLVyA+Xiqmll+Tjknrn9B61iKFPPljFgZWPYGEDpxagHRrvVFsvmCcP6u4r3I/6zlHPMe953j2o83u03jv4xMEhieg9z0Xx2vPeu6rREcXV8drzo3vurzB0RHiKv8bcc9f7TqO+zszv6Lyuv+L4vB1PNth9+D/xHN6VfKLtHtRP4zezZ7N+6fdLL9/1l76v/xcAE8w9H8pW6ztemD6nFPkLgpImVzFe/ElBxtKIk79iSq8yvaIGB1MtOGtdOscw5nZOaruOWk2dqvN+rFbKPSqHd4rZsAveHmkL06mYoWH0gXMB02SfyyABRHQsckrdh47/EKQPhexDOiina++nvvgnWRTOZV7ONEfXwAFT9Bxtx8zuLYV7+jsdV74lQIchNTuCe0wOxGpQuk4YOHr/OgyMxPhvugWn84z2PtubNqvBH5ezXWbYzgDpZtorfOY784B31YOzijvaX3G0w44XvF0u9/aUj+7b+O3cb/Xf7n+l/6vdfu17/B0AuxN9gI0Hs+cxeXy4VQ7v9BCWBl1ETnKjSA/p66U+vunTvPWdP/zFjRpMOpXUSnWGdzZf2EGOOnAeaOxzsOpqutT0OminGd7nZmTlOHnEQTGsJh+7oZ30xwOEnsZEnqVheY/VY4+hrWFocp+4qAYTxfNRV5I9CetmspYmktMcNao5cMfkV6do0IEzpm9kyYmLWZx5cBQxLAZlrXyac+WX8iB0TvfxvvJ+HPMcnmN4q+e45+iI6tEneo9c0fuu6z3qHY64fnyG8O7hmHM9d7+e4zfj0+sa6plmpyfOlZb+Lzi7XuLp3M08WK+r+E81Un+qe03+bC76X8zvXp+8D3j0+EsvvH95z3gq5n8NUF9RPdD+T0V9+PhLRK/zw0kbOU5dMZvdTz1h2Twu44Ee5ZhROjHgjgduw/SnDOOYDsx1iRVn/OkFGnxUi0iNkdeee1+4v9RzbuU8zFJKn1galQmBE4Vbnh6lGTlJ8ER9+2PAps9/XW3DzII03xinne5DLGuyoyCD06tjJx/IFSmH0Pz43jhxkri4bBPrfatZkqW06QfOuOo32vE1gNNi59L29w/sKrqP51ca9Tr3rl5qmrCVb3NmPleY/Lqn+LNzx1v1d2a8afkmrEXe+pMFdziSPeHtclnnKf9bHfejuZ/OxoNdvonf7rGa/c29/ZuezBr/JkD+2F5/hJoPTY9ix53pez2/36snmD9yzVx3L4JHNeLwz4pTf0AvbtUpK73ycfAcwDGXD0naRGjj/9I3gNCFMbscNxP1aViQc5GK0rqx8jonmRfGSSl+0pFbTCkziOK6T5AGD5/G4UF6euCn6OWV98qMiNnWZYVpBgee15EzT+3cgb2Nr1Tn1D+g42pcLSXPsWvxRMFncMzj1AfHi7o8hs+iNvpI3zQCuFdYM4xeRfeh5Rj57P5nfDDek1FXgh94r1f4igdf8VPOTDfDfNZOfuex6q9wn/nGaV/7t76LK9/hiPprnq+y6+2aJzt13bda/D7dGz3xVz74Ef/KF/+/iP/J/xxwOOt7md8t5/d13I2ifqesk/0jfeV2x6nRxbCiv2PGUZrawmg5NnyUBAEOuPbW4UEkwtsqLvL8kKZpzsRnwRHdvd9oBbzhzEEcMR+GwiHTMyzvKfpJMZ7SfBkm2fBSHn5pGZykqYgzHsICnZPkA1OLBxD89IjLeDgHh3njva8ZtNJHRZzUH+lRlxcQfddINLwhRpxxEtOF+4RfmILOiIYnVnVyLEeTHBV2eI+A2NW5vK1gxKGpxPGnOV55c/3+m797o+vYXS3dHaf3Z5on2Ip7haunM9vlCk/RhW5Hi8cOd7Wfe3i+xW+/Fl2/m2/NWZj9t7SLdZbfAyv+Hf7N/f03vX328R8DijvJh390etS7NjD1iyMTch7AJ57eHXuHsmdYfm+Wt3j5QYp36fj+Tf/iMiO90WsZHeNQD14A+GRPF83RKw77ZFFY5rp4Ta4Yr+EJztKum2HwsS8/yfKgCby3TvsEOa3yEkXp0ADLk/dYuUTJqeg69x/6SjKQa1bbM70Pe9IjDqP3srWSYLZZO4e8c0RMDEIq6z7JW6/gcRuqRclXcZEwzznKOUMHoBhg/gBlWPexVqbMU+H54E3B4pZ5p3jt82cznJv9Bqh0qNczz28x6VfHd+mc2W5wPtWh34lX89Ff7QGHuOMHV/EpH+2num9mMvsv4pP3eGf+r/185l96+xzl/9GHAQ+x/ON/obEBOFjW6sXJDxBtGUnmAidb5z8eMFy/OxKfB1HmhclCHmmbjcoP+GgL1ylP9G+1cZJffm+5gOjp/vmdG17J5YJe3MDYPwsBnNprlOgAFGf8wDQ/5bqgg1vY2FE+wuKVGFpwRffARxL8xalzekC5l/rMgHxARzXpHTcRbfNhFYk0npO5LuKKFMf7B2JYcQYe5KErn9EjWUTmEKFR+870iM4RptoxciI6uNSzPr272LVee959ei9rXRbv/5Rvpr2v1g424+xqbfx01spnR/etlhmr+6NP/DUPX8Vd72816D+Zh5b4Cw+8iL/0/KUX+3n8a3+fpfw//xN/BqAHYP4QoDz+zD//glx9AuYPAOoHuT4jxr8iWAb5kFeiw/alTSxyHpjohxF8EcU7QibZCmxoxAlQH/hgycnLgeXDoGqR4KGTxWvIkYuDRO230wmYdlHnNSMeVGMYfKL4kfNe5fJ9RlDk4w/xIS9u9n224dy7oHxVj1nj/asZw0u8GpQc+S+woRFHJ3hg7E08CHUtHpjsdTLq0nbSOnkmOrTqK+eFhn5GXeRdvEyqDihLRZ3BOcrRSx/DyrKQl475ariGeqYbJqZxref4uAbszlsLja9tGdx59z6zZvMd+1SHh/R6L2c+4qxw9XRW/RV+qNY6+lfeztnlaZ+r++ye+Pr32YzTsU/m4PGNFg/Fu/feuXc5O/2F793sT/u/vP/dHTQz/xIgfwFQXwVy/UCg/9Rv/i37iPlDQgj4huSbDFxDZZh9JZxFPuBK0i8uPADxx4a+hkiiOj+woiDPH2IQqA/PsO4rTh4SCC4uTP7QkL0B6MyWe8pFhb+ZnDHxRfEP5LxH01k6/BLTJclHVDnmR85s9xY8PXhFM3dqvulV2PBDU3HMF89O7hSY+u6j3cGGZ1FS7r6OV55aESNxfUGHd3GF6QzNUWbtmOdFeePgA7dH+h577jV6x5T7cY7wp/XK685nZ9aM8y222ncHX81G2+8ZnHjXv/PHZ5fHPKLrVzlc4ornOFyi9+5yNMQ7/qr/rd598SJ679P8l16zHf7a32dqFi/h+Y8AlOjh9j/1N/70WZ2v2kz5eKjQS1CNOPCERZ6hcrWTGpeiHX/aIGpgg6amaVMTNRr5ZJGNrPKHk/y/sA2TA3+ZHjMSdW98VzpmaHjn1IjjpigiwkWrFljReLBX+fJG43zlcXiIVfnyDEBY4uhTcYCpU9N6yRUnEsHiDKzwxC50EqJR5EFe8vd91bCDVhC5++CXPV1sntm8pfD7fSXe2I55LlqvE5CpHTjAqh0jJ5p08ITN+h3vnF7j3fFez+6jz/qknmm+xaRfnbf7KuIKx+eqf9WT/tv+zg5wduY5d8nnm7OTrb67L6Oe0k91J5MofuUj3196/YWfPPv59c7dv9ezef/RB37+Lj66GaUq7HhKvGz0jwfy+0rcF3xkAjShGvLKU7U+2KEoSRhOEHMPxXhJSyt5ccm/rVi9COPhlby4iCePPC6unvDsRz3lJSEuaVQRTGXgQ2e40jzoqBU7pjqOfHJFXSLP+63eeA+tZu8Ug2chs8NrcKw/ZsCNXmJwav74wQQcvmJxHMqZ4hZ/zIlaUNaKaAMEO/kFyHt60tWwoTGvxErnfUmytlnTfvCYRZ8oD86KM+6piNLO9DMM797zepWjJV7xvCe+16u882a1MD/udYfPuLsY32c+w/OZD/2/6OGteOX/lLfr9dTX+eSfzJL2Ux1zib/y+eVO7KbIr3/Hfpn/8v539lrNy38T4GhGohvvLw3Ivxdg/fxO4O8PSFMmisktn+RVTz7prTpeqaE3wyTQiV7+owm4B5p6/pEFvJxnffY69WtW0Y7varzpxb3lh33U2jl9848bKgdT1HH9gbywNCgO/pRRI0WW0fHKeVgOAV8oCbpJ1QqjNcNy2HHJ9oIjeMwvzfjBQT0wywWlLi9rzuApaQffhKsA87e1yd5LRO+d8b3rFOXUxC51Dr0Vpj4+7I2GSN+59By746HxXTx3L7g9+gx6M4weccb5BpOv9LyYQ5x5q7fi3+no38b4Iq5mo2WHOx78p/Gp77f7PJ23up9f+cj/l1748T6t9v8W//XOd/tczTv+PQDB0INU/8xfD8x8hWtGYeSKmpYXJad0fJAOXLzJ9AFFP60KUDBr2aR8ZqPdhk8yD+0OVvRlyHlxyRlXC7EYUY7kRKao5rBk44wHrPrRO80vjXOGvLx5IDuHN5SR400rDSudIuTgkGaMy5t3x/5fe2+75DiSKwv21Om7x+za2n3/x9xfuzZmZ2YW7oRDTiiCHxKlVFZlWIsAHA4HSCkZUmZ2FoRcG4Wms+qDQPWtBn1Q6gtxYVlXcRJXMYLUcZzXyfohxzd7ifV8St96gy/QLHUshivM2q16Nfo0VD9ZafcCzyvXsR7PtFR/JD/jHO014nn/kT+rmeHS2MuLt2Wv0HD9s3qv5vts8M/26/WKr9K5cqZXzCbNbq88/649ivf68V8D1KdrbDhY+D8D8PsAiHXjwqd6LMVLdB8D96bFTxCGGA4RVIzCIoebfMC16YCSeKVTBzzUU0KcCOiKzGTwEMMf4PxWP9LGES3gW53XM5GHFXnRWUE+g0qCIA4szxeOeoSlm6QVJzWWE0fxAvg1IxJ4barG0WZHyDjcEFEYjbU5Okea1TcA5FVXvUBEAicQi5zFXfniKA/LFY60CouEeHucRWQ5ej0EdI0cP+tD2Wt0HsKV69b7L9PdjuJSA4FdO7FWHIFhHQf86njU4wx2ljvjA8fq57ugy/HRnDS26q/mSE/2SG9xZR+puaJWGm6fmUU6V2hIy+2rdNXj1frqA3u2198o4kYflbjP8F6TNxypEReWfN2UxKnY86GpgWpjjTxX5iCLGzHljZ+sJRG42ktPgN4QINbmw1oUFDl8j8/4ztVQWxhyWOgdPkfQHKprHMKNw00vMG5Ssqpjg0UbP5XgQj3y4mRNXRPhQaE2ilQDi9A4q9mBJ0f6CoVXbXCZw0E+xGPpjQR9IrcD6J5Xy+oT+Y71+Kb2mKdenMUlAPRz8Xz6qscF0PXAjFhds7iZE0l1nlc9OeJbD+GwuibwtzT28r0W/L5mnBE+wqA3wkeYes9yM3yvbjaD6o7kj3Jc84y/d24rrXzBnapZCYyfk0Y5FD4zQ29wpZZrv0pXPV6trz6wj/Sq/wuAQrGb8Bf9QgmvI2zatEwuB30yliUBKXXPFyBiQNr4ESC1ogVAThxWOHiRo1QmaAg0PLF7gRDBUo03CL/razjvw/qs43m0XtDgI4jYvOCv5kAtMK+THzDXHmeki8LA+fyEHiTw4JKDPsn5lRySrH+jLuUABxwmF8mlWWpKA/nDfhJ5vSSc1jWUqjcGNhdy4IovC4rjK14k1FN8WfCweuyYtEnK868im61rIO4Y6xrunOq1wZGGrNe7j/xeLI2Z7fUz3lF8pncWR79ZjWaZ5Wf4Xp3ysns6Z3mP8r+qTn1lj14P8bfslVre51W66vFqffWRfbQf/xAQf/4fCvwdgFDkdwRk405UMbrpRhd8bECrr76MtRHe5VGPFTwODI0F4RF1FUeOHGBwkBBmNXKrbqEtsyFJkTAipBY0M7W8SVEgwWaR7g9QWGba5ERMPA6MwQNGMDHFshALnyt5HqteeuCJthSZLnJINm3yVWS96g2CC8E3Dv2MSwdxPBB7v36tIaW2cCqPRCzU8vwYxCE1FcJiOY+jQFRc+U1fc1GgHTyHcjx8jeKOge+YfNmuh7mR63nFsEc4XfeqWHNIr8fAj2Kv5M60t3DksEbzL5ntnDiyWzriwB7lneWqxxl91cg+U3ulxiu0pCl7xblKa8u+qw9meLbX8q8B4pN/KOHGw98HCAe+YjjK85NwduXGnDxw6oZq+RoQeXELXJ8A+mnBNxr7q5429UC641ptz0m/LHQkXGATAA6OFkRz+RsJYbIs8bpIsJcIiPUIh1TTJqAYSfhpYfiIw2pDzXykFz13or5qgGcsTVlwqlg1gKwXOeD5Athm7GnGIx3VBoHaxkENMTltJvZMjmrr3IAD1Lk6L3wspEt/EI84wLB63YKuj+TgYHM7wzWAIx5hXiN/j3c2L13YXuu5I/6ofoTNes24M/4Wrnm3NMU5Yo/qHOUd6TnivFp/1NOxr+7vs3yC/87rcUWvX7Wxx9Xjhh6qtbGjg8W8I9ibhbsc+MqrFrrxKG7ijJVLq/7sM8JUGznpaVZAXOBgidut59yPudW3apWHxXLtBVmO6tGxiHXuVQuO88MnJ2u5yaZPo54I3HdOT4kXlq4aCG+xZlSarQJE7BhbGsD8JBZMDgVZvZpHb4bENdrCy4TnF5X7ozjt1Gp+5WHlez/V3Svf+M7pOqrruHrJiifruPvKwx7FO++ZuNdqnhHeMcQdm53HGe7WDDN91VyRl8bo3LyP/KO8d/HVR/bsfKqTfbZeOrJX60kX9pXa6vOOHup1lf1buw82Uj7yLiefVt0sJ6hs5OoGiSsRAS8IdJOUcAHaAJAmdylbagVkLQx/pAAncoM0Mou2J3tz5BxTDWyvyxiGLurgyIZbS1iz/HQODEu1Pab4QpE2ID6CyzcFqNmpq+spXkpKK8MyI/wOC4DngAQW5lm8OrJv9oRRnjaD1XcpqvLmqAYIfI/JCgAaxOVnT+bzgDw5ZpVXLb+LJbBZ1eo8FDttC1Md+Fu8Wb7j1MAhz3Wk2WseiVFzds1mGemMuCMMtTN8K7dVs1WH3NVrbxbvd4arukdqVCv7rMaz9ZoD9kqtd+h6D/mvOgfpd3tVv+VHAKa2eiMQOG6Wee/hDPDxvwTWTV9JaURMF7XgsWrRgYtYGxV6cYGoOgDCw2rTX21CLLIa8b0hONBEDjiWeEu0HFXjnBmmOulIu/PBU05c10feY/g9BgfLdOD6JsiaVqe8zhvXWCOsrmGrm/bnEO0ATdRLOHy56q+5gVcOTnILM2lhKNAmXRh4CKwXz81i5SmZXPqTg2vDVyy7Kguwzs0SQy60kIjZfDmXvnGUk0Wd+67Tc53X4147ir3Gfec67v6MI3zEHWHgz/BZbotf/dvzIHym6fkhZ0Ov127FR2bv9Y/USOOZ2is1XqElTdgrztP1tvx39tIcV/Zc/yGgUMaNt/4uQHTUBqzNmq/9OCjWUHWz03T6IskYRhBqVrFqOo44c6oVFbE0ZAO6PflB8BlZpyIQuyAwLOdEyI0OuBrDF0cao3xioiKsOgapHbolvSInKTFtiHXSkca1Qe3djJoLeWkmRn6rG+lIFxZNuPmFBmNguYgrMKs6XedVHYI+o9WOXJTooTxj04JLks1JTAVpWWfYiIO04/RxsLnLFzn7ep1SHQOupZwscPedB1wjjHizOmkcrXH+qKbnZ5wj80hrxN3KbfGrzi+WwBfaIzOh/VGej/pIjeqfqb1S4xVa0oS94jxdb8t/Zy/NcXXP278GiJ/dxxcLNk18zcDqLwOiOTFNkXG/2hpOX3OI5YO7iqFVyaUfICzpeF4gS+JALREtVh4EclIPuOg3B93Ga8UXRYIey4etBgnmXHd48ESlzUAYqyOojR3SSuZgyglXXMLZG2WrHOp9LsRYqb/iAneux7cSoKxHLedJTY28EJZjYeFUr6W8NMAEj9zGQw6r8kuo8e80qZGczbpMaiadh9fLl0UJThWxY8CxRljH7zgA8vqBiyXOzC6sG++KWL2kJdvxHs94wK/gbukfyW3NoforOa55VPeKmkd7XdW76yievQaUf8S+QnNvjt+l59/6quTmH2fNNwBxA8I9SJ+g4ePNQC2cfXJYHz5umKQkr+Kg6mIhhRtsUm6bWgnfHH7nQYWmwWLDbxXZh01uKG/one+cGiZqhIdFHctUixweWMDcBzaIcQ2kg5KKnRu+cpApfnKqhkkcYiGXM8hFzHPNOvJEzRxKtFQHzHHkiUkHQfjkiIg4cSaUR3GsysEnshy6BtC7/FEsCvVdEWnAyoc2lp+GcsCKGw6vW2KoEY9OChQfhFziyQqHHWEdP8vR3N5Hftc6G0vnjO09VDvCRxj4M1xaI/tIjXSO1B7hSO+oPat5lt/n+Or6q+fpeoifPceR5hHsK/q+qmf9DgA3/ujCjTeuAjf/uOPgppP3QF4bvSlYAh75TJATB978rYaDB448fNTTZqlMYRS66ShPKxGBEaOuXgmDvDfT7KzJPqta6ZKgIOyWbupo06sNXCc0spBWf2tTrvqLkxquzU1L2lkIuv5nBs1T5wdOEPT8VP+m7ddLY6T8SkoYrTRsHq/VBguucNmqh4P5CKwPhel6rNMsUg9QwK+andh5Luu4fGmDB0x4r+u44npOWq3y0Jn53qPzzuQ6V/FeX8+r5hm7pfeKHGbd0tW5XMWR3qPWX2uHNfLr48g5zDSfqe2aV2q9U7v3QvzKcxn1c+yVvf+ub/OjSzywSfIRLya8nviaygkY45AxTcTkYOLE4WLhpsyc4eYupDiSEwflEONmqRhEfUcAGPngpO+8SkZuJYAYi+KLW8cBRs3eqArSycbajFdzJKWkyzGRxPhJ1nsZRW6VGw990VObevVvHJxz8cwnBq4KMzeMwbNa+lGnGTQnnzdpyqIFfK30tSFyjshxDBwiP8QCdJ0VJ7WFMWx8YMhrdZ8xDtEfI8IdclJgxMlUma6BhDRlRfb4iO9aexrkQhRD5/IegHo8wo5wRnXAtEYayJ3Ft2rU690c9Z2di/Ij+0jN0fMb9XPs0d6ucdUsXdPjq+Z0zZn/zl6jGd7R//47ANEV9wg+5OedrvDI4+aNeLgigeH90/6Kn3nWhk+dKECN7gLA8CBWjmGeDx9k9mNjAIMlHdkBBRB7em7CB2/FVQA+FmKvFZ4pULC4EcJanjgwOLFoFRhPEGw94ASnYgjsLZCxVBcxoYyX5O0o7RuS3mC2RSjyllPdSIcYDsYvXuIVSyhsxxBj0cZB13fGW9jLccShTifZjEg5x32VjTDlztqu1eMtvSPcRzlbdbPcDP+qc+h9z8x3hqs+j9Sg9tE69b1K40odn839K87V9bb8d/YazfGu/vWngPmpPybB/Yz/GiD8/CcAgekTeLgk1X0vJ6UJEPhqs0e8lNSLFb3q5p71gdxB7EHB9kJnIgqitnOSDrll3QFRE3XaDJaBRQ4bfJbYXJUFhmQspWWFV0I8q2EucNXgOsHXQ7qYTW8KoKs8LBec1F+dS6ZXHBUln1pWDy4xr4WffMGjOudwXqsBH8u1R9jCuh3FIYLANOG63q3q5nWO9NzKR5X7UgHmuHxpr+qQzBnFq3zmvA65vXWns1cQea8BfSvuuT0+8li9rscjDgvzMOI/WuO6M3/WT/y9/Fke+Ec1H9FWjezZXqqTfbb+ah3pjexVs460HXtXH+/5lT5/CRA3KD7i7PlGIG9oGIxx5nWjw6vc3xDgoimmj8JYKz80JUsch1zmClqThbqAsG7BkeCer/xI13McOHQDg+ubHTdgzYAa10q/NnrUazavgS8cNeon3DD2T8pKSxzUoI84iAfanClr9CMI1uAAPBd7mDalRnHw1aZq5SgXhHrjJSw5vRawMNmk0gCr8495ak4nHfXVADoHakYcYjg0Dede5WPE6gcfz4ct72NwuaA7x32QejzDgPfVtZUfaSJ3Ft/T29JU7VHOkNeu9SOaqpmdu/Iz+2id6/dbbjMAAEAASURBVH2Khs808q+Yc6Q7wt7Za9Rf2Dvn4D8HjK9CvK71wCD0gYcDXwsbXq1M0BgulxuLyAIjBt9CMW59kgBOtigObna+6fLmJ7Gs45sR9Gg4RVxQeSSE96YZV8/O85gNBlrOkS9u6APqN3GlacEJEkaplXPBCKdOEZYE6uof+8ka56nWy7rfOYgLa3OtcgjQLFfVCAhbG7fpFIa8uOHgGlUcOKQRO4ZaxuIiADGWdPVc+jUHjXVkmrbXZ86N1zgO33PuO89x92eco7qz+t5jL+79pNvrZjzxR3akcURnVjfq8Qx2tM9Rns/ySM2Ra+M9uv9oz67z7BwjvRF25bwjfcfe2cv7frVfvwOgfxEQN0f9ISD+BCBunsC0uLlaLJw2b7SFNR5CUBz2krohJ6Fzoaubt+pGHPCwtnILw44aLoq4USiVIj7znbA1Ao+P1Kk640ia5xsEnXdxgZkO+JqJOLSwEJhfNUglrrrOqxjcKHRdj4EjCT1yEOcaaTOVXPjSpU4C9BuHxJyZGu1AnThgDtGIKcherYwhz6fxOEOS3V8KbucrPXJCw7nw8TDp5XwbJg3ZVV0KwPjqsXKOu195gDbQiFNcOWm3uKI2ecIjTHzYke4I85qZv1c3ywPXZXmG0+eaaY14e/1nNZr9kV7SPFor/lfbq+fVNRyd19W9Rj06tjVP574yXt4ARAdsrP6ofxVQkyIPXjwAcZUTWCb0IhdlZFXmXGHgr3qMBBIDr7jl3BeUtjgA4GMJCxdwcZ0DXqzKLSGBFbYKFpLesKhEVpsnSvSAU34RA4sZieOgpfkTE0dpaokDMHjiUCuxug7GgcuGsK4hHwLp13lETF0csDLuOkzHgVZ6QS9cugMMslja0PmaW6BqQx1wGp4hceWAyZ/ZFQekPl/T6DqregSxOkfxKMeCPIx4ynsODUbXZsgdaANa6TWO5+TLJnVYP9Pdwp/J+XN1VKefB+r6OsLxGvFlPTfzxe12xhd+lq+6mZXeLP8s/mp9zKcess/O/Gy95pB9Vu+Zev4IAPe0/oAoNi/aPFQMsk2Pm41yLFinCaEEC2XuA8N3FYilpnOQV4E2G2GkxwG1WcrU6qCkrCeFwWqpuTDFyPcmyakNqXMiz5Ks8w2Y7VIbMjw3OhGkbl2s5BWefL/JUw+H7KWLctfTOMxJuwRSQjOkJGiSdt/kSkG8yiUwmpeplr+rz96Os1mvi5hQzt75inWZFXdbJ5J9EYsDZ3oewZM2arBUJ7ugt6Pj7t8Yi+c595Hdinuu6+7Viz/SGWEjvS2NLf4zub2eyu/1cN4rueozu6bKz+yjdSO9K7W+Ql89X30e6nPUftI8mAWP+hEANnA9cELc0ONulvfSdZxnohuhNn+Jsj4OSUNYiz9CiAg53SxXG3sx0xHJ+CtKDlgzGB8NeE5hqwfyeGD5EMA8Rj552uAVi7fSVK20UR8YN9nAkF4OcHKJq1pZ5WF3MOqDFjw+H9JMDYSQwJpxkBdH/QqD0zShxWV41+a1yXxpZ5lmVq+EaToXoDCdS49XnCAh3/urplvU9rXFUQ41fZ6ZjuNe775zcAL4OlGeFgfDwFd+VWtBz/d4pHGEM6qztkN3pLunM6vZqzuSP8o5wwMXa2vuhbE+nuV79TO1rgP/Sq13an9lr957Fr/y2s56HsH/rp/pY8J44KamTZN2gZnQJkvhIFacNybWJh9yiLUU68aMHLBaqyBRFwAUHEJxoM4C3RpJo4sD5z9hCJFc0k5NUOqmK53A6EoPQf6vkevhUSzy4us8i6d+kSam2HrVBQOGBzjJw7UmVXWRGmmrVP11naSjMmrhYHqqBUeLPATp+OZNvtWrZmRLx5LCqGM43F0sCDVLq90Ms44c97MIfbFku89kHsBxnnM7XnVIHLhmqpdl/YFa8WWr78DpnB6j5Cgm+TP8EfdOZ3CttupmM0tXdk/jLO8R/tEZpN3ts/Wud6WWdF+hKe2Z/Yqes1mEf+JMmo3fAdAvAGJvw9cbN/6Ymr4wVADA2cgCe2DVphS1kMLyiySMeM4x4tQoXixinxF/I9cxFQescloF0OlL9WGLJgc5rIgFYYNiiQAGRatvJSstmzLUEUYbB1rppF1hKAYeC7g/CoSTnCFGwcYBcYQDMy08tzjv2QJdMp0zw4tnujOdjve4tJpzhAeOlvsdW+UiGF2PFScEPJYv2/UVdyu+bM973Dk9BvcoJt0z/BF3S0e5Pbulq9ojHHCP8s7qPqKtHrJnZ1PdyF6pNdJ/F/aJ5/FpM/V5+IeAasOPLHy8EdAvAerJs3vv7SsjQdz0lVcDxPCFQ8dziLHE6TyvVx0LlIiArgSQtBy5OjScb0CA2Rr1IKbatMDuuNBJsHLGtzacsTjhwK9PsopRoPmERQwuljZZ2Y6RlP39uxrehzWpyRkkLiz7Yg718Y2sasRn0zwgaXU6l9IxLnU8Vq1JoY4aLacy6SqGBRVrZntOPMfLj2RdO4Btea2nhNf8mRSO8Ii/xes5xH15D+T24hFnhHUdcLRmuRmOukdze7V7MykvuzWHOLBHeao5y1fdoV7x9XZ0PTPHVo9X6c56vrvfbI6Of6e5Nv8QEF9TOhsE4dfrLBze2AIW1qh3uC6U+IpRB0z1wLnZOACwkyRkPN/wUFJDwFcjr3Nf/OCt+mdfa0NdwgnCsGagx1y2X20GSIiP3liOdX9hrGvAwZJOWECCmdMh9VabWWC6Zqu61Os6jHFQXgT0dT97gqZ/oEhjZClxnS+vnQiwoUU5kRdo6YFeGYN6dmlM1MlHG/mOu+8c54IzW53nsfte33GPZ77Xu+984D12rvwRZ4SJ3+2MO8N7fY/36vby0LuKc1Tr7Dl0vsdHZnf+ln+llvq8QlPaM/sVPWezfAd8dr2W3wGIM9B9Ft8BwFeLLBLM5bfQsVlwNUWEymEjAA2Y0yallMOh8iquzOKM4MLKsaIRZmm5nBFcLATyFcMCE54nxY00MIaJgVo8+K7nPnJYgXEDznpthLiG2qj1pqHirFvNFBieM8pgTtU7hjoskHJu8oHZuXGG5CBVHASxEDvmPgl56HjF4dDXtWv84rU+rg0/T/NuFtXLqk6x6oS7PcIZ8YGp1n27rMO8tLxWmOs4NvO7xl480u81I84Mm8214uuCNPKo76qu8RXO6pQ/onGUc4Z3tr/z5R85N3H37JVa3utVut7D/Xf3895H/E+fr5/D3/+Jjb3+8E9Mrx8B4OuUbwLCoe2VyOcXM04arjZ+UIXBX935WpgSN1rrt3VB2ROV5aQ44i1Mw4kDiyVcTR1fGCtd0bwXMD6ilpv4pE4cpLnBw+Y81FXvrBeHvQwrnXBWbxDAQTJ1qJmYeAixNCf7L9ByDsgphj5izYVYPjhIRixthFh32lZXnOSxnkVN2/KZhlm0bQbpFQcAZgIXYK4eA+6YYpMvjr4uXFPa0uqxc913PvDer+t4LH+mpzzsqzjeY9RnD8P59tmkuYXv1ek67mnM5uszbPUTVxY99/qL261qZ3N3/ih2jTNzj7Q6Jm3gz8zYdY/E7+53dKZHn+sj+ldwtq7b39z8400Abmo4ET30C4F6lvkpFdNITa8s2ZwUab4xCIe+1Xhp0osjrjjKN3nBHLQ2RaESUXzAqsT7rvwYAHHvpdi5bOdA1Nb8jpvPDTPi1Wbqc4MLEaxW5xBSlfYakLAC0+a8AAtW2slZxR1TA82TEqIpjZjDiKdExHTjsDpfgODKoj4WuW4tD5f5xOgnJj/C0pA2csO8yDmjOG7dB92XcnuY5+H3OsWy4u/FM95WD9Uc5Ti/+30+5Ue4Y+5v1SgnO6pDTris+G6Vk/Wc+5533zndF0+254/EV9RKQ/ZI3z2OtGT3+Ffl393v6NyaS/Zo3Tt5e7P9jR/E9s2fn3CsUp/0iWN63azhJw8GMBY2R/mKmZgcrFXV3WG4MZtuz1dcTjZbDRIYYmHgQhfUOMDSDwwUbvIAEBBIDvxcuCaqEVYWuawDdrcBE4yD9NRLOKzqYcVTPmKmvQ65WJi9+invFpwo5jmaLijEIIIFzuLVkZyK7vNK9brCWz/iQea1isDrNF/vKS23neM64ClGe/l0BvM4R9wR5rrwtVSjWNZx95Uf2c7rsdf4jMC3uKo7whF3ZGf1M3ykIWyrZiuH+r38Uc4ZHrhaR/qLO7JfXT+aCdizc810t/Cv6Lk1z++a+xs3DD7iiuPHAavNPhJ6c8ALACJWPjsweqKQgt8oN0zE4PgawcJknX/nqzESvbnIzhHWbXBI86Zdz3XETw43XGg2DuRWG/GAA4g82QhUQ131aHnUrZZmTz5zrrUi3wKV3ZDwJhorDgKbDSG0hnoNJweHqJ/xI7taaAWu3hTou1KutSrYCdRXdkQf5YTJqq7HwB2b+aqX3eJt5VS/Zb0evB4/i6n3SHemvVezV3ckf5Rzhgeu1ux8lZ/ZR+tcT2+cHbvCv2K2R+b4qr5nZv1dZuTfAVjuqsvp4yZb/wtgnKW/IeCnXd3ww8LFgxcD3PDjPQQXfDm/QCApMTPiMZ1iwvBjCOmxJPPwzV0HKu79HI8cN9gUGWmhvGZCQ9TAGpkxUtJGPhdzOIjvnMB9g19xvAZaqqOgxeHqTQe1VIcaW2rP4aUlmzUIcZ3ZIjEYPUouiCtOBIxBkGaShcPKp2O8wo0jzOuEpXSZEQfyjoM8ikvEHO/jvigjbJTb4om/slGg19Dp2pWQXevAu1aPWynDI5xRnWMzjRnutSN/q24r51pX86R9VFd82UfrVC97lY70vtp+h/P5DjMefR7v/xBQnB0/9edZ8g0AMCjGQW8CYPsCJJx+xNikeFNOvm7QvbY4cGINealxlxS+lK6OTOEAwdReEQLjJ8rg6CaMPEpYi0DDeD34Iw6w5PHcwfN6FLlOhBRqHNSUvjTA1RroiM83Bqn3D7yLioUcFnOwqS8M9m4ugnmQAHR9Be7nyxnEQZC+ymVdQv5WDhzkVxwE6gVCrDr3JVzzkZ/gCdOMONBFL+VGfMe673Xug8c4DrqOqh3ylBzYznfKVk68IxxxZ3amMcOls5cXz+3Rmqt5muGorviyj9apXvYqHem5faW293H/K3p6/yP+d5jxyHmI87d+2Q/30dr4cZbx+DfAfsaBaZOHiKdBdwA5YsCxAujYXf1CcxmWAtC3fBcgjy4oX01hAwNcSzkAlqibL/hZl4Y0UjvfYul3iBtHtkKOD82AIHxxeLECq1kgmjHrcMBSfbiEcDAMlKqL3Eovc+Sg90KtGcClJglLUpjwqgEn+ypXxa7dOKpHufuIAfi80pVNCqk8GB8c8nDo56Gc2XBrXPhYrDfrGJPtXJB3nHFC8t1KX1iPZ7jz3AffY/el1e0RjmpG3BEG/gyXVrdb/K3c0V57GprnKO9R/tF5pT+zZ+ec6YzwV2qP+gn7qr7qP7OfOtds3o4fnX/5Q0DB1uaP+5s/7m5upgw374frL37jIF9hObdxlfeUMLDKL+dWu/KQ1+pimeOnQ3FgpSmbGK5FbULSco30vYzaibMF6iKG4Y9Aei57gYBe/FFH+qzHITUYpw+jXvBVL5xc6wWXbzDCqTrLVw/kWZyaMI4hFwRyUA+n5as+02GWlVwEU45R0y3jNQUedVSMmXNpfKW6Ba1zVOtWdcJ6LHxmZ/yOezzz1UNzK37Ueh/XGOEjDDVncfWZ1R3Nb/WWhuxeL/Fkz/JV96x9Vd9X6R4536/svTXfp861NbPnzsy//BJgVOj+qE/3jA2vO2J2QhPdbFRbja2ueIkVxyZGvbQAjzhFdyLAu+YTrATu8+xnutz8cwj3KWE89dYGqxYo5SM16jsXiHNe5amfcNIp420gNtJgk9STT131hxIAcRBjBeabu/cCva+ODWOAELJexQus/CZe125QVzWp7TMDqrz5kBnlxO0W46gGPpY4S3Q7CpfVyIiFge2+x4677xz4vjrPc0d913Bf9SNMuT07qz2Lq8+s7mgevD2NM1qPcJ+pUa3s0XMR/4x9pfbeHF/ZezbbJ840m/UqnH8ISJ/++eOAuAqKcYPj7wCgW14d37A4ROBI6WYoHnM6QDP8Fc9i4bAgqSfjgKStfAHgYyHhJPgZ040850YAP0ytxBiHzw1JSeSwUp91VgyXoWlUOmv53QRomPaKEwE3d4Cdgxi1sTQXN8EEtSGKszBBLq+uAyDCOORsYrk2STkHa8ANh700j2u4nz1Ylzi04VZPi4kjlwtxYXDUTwTjgag3RV6ja6IS5JTneeb5SJthEFCH5RxhxJm9aWVIbekTQzCY2znuo0axLHUMn8Vb+CNavQb6R7GtWVY6EMxrvVezqhN5YEczdtoRjte8mu+95J/tqbot+wrNrX7IbfXk19uewMX5rXkubvUSuVfOz18CxCZVfw0wnyHGcTpoDogwDlg2EW6Y3OQixzRy8dDNk3XgrMugUphyskz2A3RBsHXH75w7ghWny5LgwdZKHW4GAp2QuoAKHvUyXWmRX0UhAA7iZgGJpk1NcaRuuQx4vZOgay8eYRxsHuQgsqVNzuQAOcytXownXA2rXuwLbsyjhfq7BTA5w3wr4AytZlRHLHkwHq8kjbPCVWNW+alWEpgXeVCv1B6v51V3xB6tPcpTzxl/hqPu0dxeT+X3ejhP/tZM4lxtX9HzFZp7573Xcy+/p382/+5+Z+f7an79Y0D4SsRGjmX35cUHHiDzyUlz4wZAmvIiQDB96XpKaWHkRMBYBSDBFwkx1ghbMuSC3ktWNVkvTm3EgWvDppz3gR+LNXGoT6HhczOU2ELjjOLWxYJ+apSFk9oqLascLJZ4EcMdbcKgik5HNcAjwbxhItcmrRy45qNh6YbW1nLeypdeFiOna08oAGLJq1o4hhWOehbm9diIxUv6ynhOvqwThckq12PhMzvjd3wv7vqd3/NH4pnG07g99zMtzLeV0/xXcc7oiQt7pL/zR/4VGq57tZ5rz/yv6PkdZpnNeAR/1zXl7wDglYyvS2zwqwdATCKbbpjV4rCp4T5IVc7EUmZyBNg7uQBYEyBmsTJy7w4oxmLR4t4VOQeUiEGvTUd9wAufJn3QyUUM37j61j3yLEpLPriIeQib9YgJ5f+eN8oXV/VRy3GkAbwviCrf/NrUhYsXJTwfxIbdzeS88LF4DunA55uEcMpHLlbxJv4qjyDmIJbzKF82OdAeLp3HBk9aqHdfesA63mMS1KvxnXvW1wyyXg/MY/fF77Zzetw1e/2ReKS5pzur2as7ktfMWz3E+Sr7itleobl1fd7d77vMsjXnkdw7r+vtLwHmZLynxQGWn/h94rzhYdOAy0HT4WYt0M/A8pAapNhBpQx0GIE7mOtLGBhxOdp8vY9850avqlPe+lMOMRaJi3u7OBErD0pwtFHy1/6RiweMyt3C1wMO/SDDMk6/dIH7AhHiWKg3v/BIAa4/BARuLPZKSyAP7C0/+3te/oonUBZJzRKuz+91oncrDiTgo156jLNAvAzJha864W57jefge146jjnfcfed033nud95s9hr3Ae/x2ewK7jQ2FujGb1mLw/uEY40z3CfqVHtK+wj5/DMHO/utzXrJ82yNefR3LvPp34HABt4/U2A8HFzw6Pulnm3w4DENanb8BkmxhLP38uVvLeC7zqIayHBAewLXT2ShLA2WhVmHakDvmg1UPYgLn7DBFdNiYTDk7/NuJoncqhlfRykQ4ucAOhJB9ZWUeAolyB6KX9nA1C+50x+7YKYczAhP/u6DjdzkDY4mld11IyA14jBMv8o7xj8rTilyOk85WS7lsZXfmR7DTjeZ1SzxfFa96UzwpTb0t3LucbMH/UeYUd6XV3nM5d2vjY9d4Vf+k+IXaGh9ldqSXPLvrvfd5lla86jua+4tvzXAO9++z8+Dv4nwPoaCmf47XibGDd+8P27BkhTI3miC1OMC+Q3f8S4k64wkFnI7HKjFSa8xwu1jkhTg076wBSvXYeXeZyHnojDatNDyM0751HM87DaoNUiJ6JKw7F69VAB9RFkf79G0iI3AuQ0mzSls+JEoLnJR51j8PGwuRD32QD5IgeA6gCkT17E0mScB9CqNn1iOEhrwAmIaa93HeQ9li+LvJYwWeDd93iUl5Zbr3HfOd3vPI/dV13H9uJZHfBeu8V9hL+ndzS/1VsasrNzUn5kH6mRzjO10uj2FZq9h+J39lLPkf2UOUazPYt91bnxlwDxVY77KjZvPiLQfZo2pxNWw5az1CHsbxR6jAs1wninUQOSFh5ctkHO+gGvuz2DQSy8W9MaaXPjRE328xiQNi39DgDlNXtY33RHHMqmdl1o9UsdcdSbfXGAPmaIA636Zk4zSxdppLC0uS9BHHstExsH9UgKdRODNvWkaZzKOZY+DEqpZZjm9lphRlu5XWOVtMB57ouyhymvU0UsDBpnffWVndVvaau2W9fqua14q26Wm+GP9lHdEd0jHOgd5an3qkZPuCd3/Ef6bUlerTfr9a4+s/6Of9IsPtdV/leeH78DgNd1f+ArBZtXfcXkix+bN1YNDR7iAiI2f4T3nzezdxz4F/OovnFgs1teGyQR9EUeK/3qb3Ucz2K6Xrco5IlloLy0s4X+1j6vV2L8WUryqpf4BCLw30MIDLDOpWY2Dt80aQaQ4VusNyXUgVY8sGSXaDmK4xiIxFNT/ToXsS/1BeY592vODQ7a9l7QHC1pc8Yg+AzgKy8rDcXqJdytOMDcF6djPRbvjHUN989ogHukdsaZ4X2GGW+Gb821VbNVd3SmR3mqW82nrzUlD9hV/QH+FmWo9cBMWz2UG/ZS8o32U+Z45Sl/9Tny3wL4V0yhTZuvKcThaBPABQCujUlc3XFwEoUlb8W32jphEiKhpQT6AouYEAORFtw3FGZAxGYpDYLLYdXGAlC5cYSjMsckoRzi2mgSVD3m0bVhCoecGzjCFRcxcM2T3IAXsmLpJJc6SRGXEsnnDF4LbsSjOs4MEfVQXWKcDX4uavQ+AGOpB33EcsCfcEDxRX0AcGzmISfBqsmyzlXsPOjr3IBrrTgB9hx4jnnsuHxZ8LA8li+7MNacXjOKVTfKde1HOVs9Rprij/ort1Unzl79EQ1pneU+wn+ml9e6f+QaOP9R/119jsz3SbMcmfdRzqec5/KPAcU0+MM/uEthI+d9Pm3e81d3J2w04tQFCAwLhm8cxAEGPx648boFHws17B352sQAqjl8+zQMulZR+Mf0haZVHRu0nMLsCcqQVg3WBHKzQHVlhatWvSAhUmLajBgiZzUI63okf7nA61mbpJjr85G2bLLYA33yAVg+rPqVWM7HHPIbqzhyUJu+zlsplwHmuOKOeY185wBT7SofoPqPOOJ2a+Ov5hNPvWU7jrjnxOm28/Zir+9cz8k/whEXdsQfYTPuntaZ/JEeZ/We4V9V+wod1xz5s+dwxP3BrrkCn3TNb38KOM6Nm39Mx0//Olebtm6AgQEGX3cG0ni4YZQQJn5ag7nncV8JkFYSTsIGr2S49JHHw3HksAJjOvOrjVd85LQCA+wbrqdJCwCYNg++mbF6ubDqx98ByFh1znMu8tS3+tV5gGyziy8Nj4Fx6SSyTvqVCxwpP++lcDl6+aoWNaAYQa7DezWo53XReaE4FmcK65rCCBpfHFkK2KHqEmM/+Wmd4zoj3zG10fXrOY/dV93Idt6ZuHOl77j7ysOexb1W/kzjqjx09noc7SWe7FFd8d0+U+s68K/U6trv0B/1PIK9+ryPzPBqzqed4+0vAcaZ4yZYD580QNzgBHHjD67icgLgjbQSTgrflzgoyCVIVvhhy+bGzph6yrkNqm7cOIfiAZdM4r5pKHm34UoDtegTCzrU4iGCxJFDgnBgSpcDnrhIys+6rZjPVWqWLupsAV/lUn+FI0gdlmaBX4uVhunD9Zz7oj2KjepGmp2nGKdKH4c8b9XLikuiXYPCpaGCtKXd8B7u6YjvPGAeuy/+kf5HONIb9ehziAu7pT3TUv1eHrwjnDM89X7GHp3pSI8rtXq/V2r3XkfjT5zp6OxneZ94rvV3APgjgDgjfRdgdWOMyfGFjcUN088EOYuRVywYsfxFhMf9w+hu0rEaLOWUd5spzMAHDviOQv5YwancPJMPw80uh6dRP+QSr7ue5SgReVHqAjKRc6RfBmTXUHFggBl2ThUnZ5AH5LWl5Th0lIBtqzQSp173BQ7qm1yFKinANGe54nofkD0GaYRVcV4Ti1XS60ZzOOa+yznuvnO6f5TX67biRzVndTMcM8xyM3xr7kdzj/R6pGbrfB+Z/dEZjvR6pfaR/p3zafP0+a6OP/V8/8aJ4r6pjZ/30JhWm/jdhYgcN3kk8qy4EfoZwo8HtATrTYBilHM5oBs4sFWxcYGLFy6okig/8v/oPzJICRrkg6wNXHW00Iaj1WLWCYONJZ0lWo7+Kbnw1C351FFc31EIABgf6gER4BEDx2I+McaWJycO+hEEC/JQ86p/aqKGdbDhVK/0Ua48HdTjYQv54jRftM4B7lj3Z3XOEydPSSFtnUeio7qOSQf41ur5Ho9qZ5yO78VHtLsGao5gI86sVnPMapTfssPawWtrSwO5oc5O0SM1j/bSKOjpp/foDNJz+0pt73PG95muPNczM3wV95PP9/YdAFydmJQbf7wy+YZggW57Mc7Ez0ZxWMJx4I2TQeNGiMW8WYI6oE5fFdJQLI6s5+OTPEMc5IOXsUpohdkg2hCpYWRsHKvzRS5JNJotAt9kmEvM+c5hGxBTgzUEq0VGS6wNWeCqR4CIXWPFQxBJ9Z9xKaBzksBSujxvLec65cNBjXHlF2eh1HGEozylyCPHNQd5EFWzsgiyljog5hJPsWznARe3W6951FedrHo8GqNuT2PEUb+ztvfy+ody7bl2vZm/1afXnOFeWQst9Zbt+s/E0pR9RuuqWs0ie5Xud9D59HOuNwB4VeJrTl93tMDg5FkohxgQP9Xnb9+Lj41KPPlZXrgkZR96ItUExRwmVXoz+1Z/DSBOlqw+JUdOG6XOu+LklwEXgc1S0nIiJ7fXSbfycqQnC1y+RBIDzLLOibj0ey41AOPR1wgjJ3VW+cS6BmK+scq5VzVJdkzn4VhpZo/O6TFoXu8+ErgeWKpbovtj1wFjpXVfsso7d+YPJKaQa4C0Fz/K0QBdX/jMkp/XtnO2tLZy0jnCAfco7yxXc8ie6aMat8/Wu1b3X6ndex2NP3Gmo7M/y/sO5768AYgz5ddvTMzvAOSZC8OJ8KYJxzi8wYMLPFfxIpYWMeSNl/RsXNG2U0JJQ4w10gXuedUKyzov9TcCotc5qo0SYb12FWSPVT770WQePhbfSNFheDtIQHrZEzNpMyMZvMiBxpKMb0ILvpuzAr15oGD2t/TqdKkbSdDwfkux82f+jAtcOdk7DSUG891xB4DKPTXCzuSLC6GNubyP+6jvcWkedJ6pn9WO8BHmI27lt3LSOMIB9yjvLFdzPFMnjTMzquaMfbX+mVnA/bR5zs7/LP+7nD9/BwDP1upe5XEkkMOmww09LDfFsPUsKxdE5vK7An4RpeEYfeggiUXS4lYMBxyt5NamGTn+NT7piJta2ii5aYaG0pRLLWB8eDJzalsWnNQuLGIvRaC+lUhO8aQvvRRTvuZ17QEXkB6QYJ20I+Z1shicWgM95OraWm9Q63nKut6X+daLHPFlTReyWK7l12zJLscVp9Ugh6XzVQys1wHbWzgNacje1SAxOJc7nmmNco71Xmdj19ryuy64I2wLP6u/xe+52SyP8lB3VNN7PFLj9a/2P3G+T5zp1c+D63+n868fAeDGiYUbn3wCfjbw44FNhjfatOAB41eYcgC1kJst5Ng0rHiIc1HXYsDa5ESvf1oXSXCRiDch+hSLkFzlIq4bdySYA4aVHPZQDAuSzcGaxDQjbVLFBYWP5JZOxHqTgBRWWXEXeMkpicjPQ3FYzQFIS2Vu4eshXs2VucLlRAFr7Bow1WPxm0XtdCmZWgwHmCDo8PkZ9AbHrwPj1vgI5r1U7ph87wWe8O5L4wzuWl4vf5Q/go040rzC7unv5Y/OcJXOrN9V+lfp9Dlfpdv7nIk/caYz8z/L/W7nX/8aYP2hvTiDuq/CiRgnpQ0fAeGw/45Nlh/2k4OLJ540eINEYmuhAR5VZP6kjroYOmrYM1zHqIda6AaPM2fINwbAtZKDEK7yohDLZPkQjMfq/BSrmemhvM4xREobNZkqEPVYsiDIZ+J2kM4NWTxqxkFvgoi6TvowvhDrIXzEYS4T/kZG3K4x0uK1y/PXuZdWnq/rANqKPYd+PR5h4AhPV6OscPGc65jj3S8hnSuB5dA1lBrhI0x82FG+Yz32eve3eI/mZjMe7fsITzVbM4sje4armm6v0OiaiF+lO+q1h33SLHuzvjr/Xa/F8oeAbHpspvzFP1yx8JGqTV082MyRED44WDQ4ZEywEhXdnLzR1w59y2x7Uce58qZam0n0ZfvU5c/1bZaKkQcOHWkMOtYGL75xKJvvnNgTOfByaSbOCSxzNg6ZPZ/ly6fcCLSJqwd0hUGTOA7Wu84N9RD0GsS2pEfIdDQ/cGp4jfly0R4857ovnttRvms4f8/XDOLpkrhm54DbsdFc0hxZ58/8Xue8nutx5+7FqO+crqn4KE/8md3TeTavvns6Z3niwx7V9pruX6HRNe9ivbDvEu8B3nKO7zmVh7v8Dtfg9q8B5tngdcXXVsSA5OMqcUPQWcPGg5uX/8w/cfCxqBcY4M0FApstpvjAsp6YOBGgLf9/f337oopap6wpFDwUB+4l9eZgSdUbA9WRa1pwuXkmgTF86IIctrAIAXk/BdrMPc/64FMAVit1FXYrjVWfRhKnwRpnNbNz9jSdSzGcfKz6VL+ETHWt2UxZQlPXGtfXtOAqTngV99yIcwizvuKfsT6H+3sandvjUf2IcxQb6QkbaSi3ZR+t65pHdY7yXP+RGq+Hf4XGOzR7j6PxK87vaO8f3vVXgL8DgFctNnI86u4fNzvG6Ak8H9xwQYuYm1TiuNcjpk76YWoVDgRkLdRrAY8HJXHI2PnDP/CTmzl50kNt+Jwx3iAIhl1tSOAsVHKLhzkQpE545XPDtjrN5zqFiWc6vuFT1w/JV71SLEcDOL4Mg9uXMFnms4ZY+qor3oQDWBz6cdD1lAasOHI2OVZYdYkplnVtYbLopWsrST2HxYkE/IrhxDVdYQt0hxHo19+1wp+t6hcEzeRcz2/hnddj1D6Dee/uj3Rn/bx2VvdVHO8r/8iM4rpFnV4Sj2q4nvtX67n2I/6nzfPIOTxb48/3s1qfUL/8a4AxCV7E3PD9WQ6fN6vMwScnYvqx8dIqTos7EHjxXy33Vwl99QQTrvPky5YYeCJHkv8XgJKOJ4YZocEHDv9afALBB4QFXm0G4SPkA5wMYLSUX90B0N9W8eEoJ9B6s8Q5Afim6bNJHnRKeR2BSKS2zr16qzitNFxHEqJWLEfnAQJ84OgXFq7mFn0Z8sYJ73Ytwi0eE7fDCkfgfTN0DnxdJ6hUzmrJQdIWsaZdtcajm1rKy4rmsfvKw/KSIZk9nee+14z8M9xeP6u9Cle/md7RPHh7Gme0xJU9qi2+W9XKeu5Z/xWaj870SbM8eg5X1v1O1+P2p4DjZsT7Ec6Od6jbJeNNNXBt/viKxCdxxMLE4VcrNK5ebSbKc+Dw1A8WPw4QFzY5BYXDWSPVc4CwxF2iOELX65BIkn5sAIrelNBHDBrqwm4t5kleWHCFQRPz0qaI5wEpV3jyYHJMIszjABCPEwtlXsNraBrMD/RGuGPuD8oJFQfOTk+ki98ER7gwrxPWyitUXlYJj91XXtZz7iv/antVzz2dZ/O4DnsaulZHeeKf0faaV/uPnMcrZvqUOV5xbj+atyvwN258egjWps4Yr4R48AWRPn5+jppfimVR4D5iLGC+UKyFnAZQrWJwvFZ1idHEhg+4NskAtWmifLpY1OQDqzcHKEwObRNCb23uOU5j3IfgFbecBfNNfFUJHuboa4QnBoPHbDEvgmkLQh05TcDzLTUNdQlB8PrRGwjlV70R5IyOu9+1ET+7oO+rx54742/peM79M/qde0Znxp3hvZfHezV7eWgd4ZzhnZnPuXr9CTs6l/hH7at0j/YX71Pm0DxfbX/n61F/BwBfbdz4/Y6NKw8cFj9nz2/58ysz+fqdAGG04PvqV1Bx/+W93hsaHUMtNv3UQH/8CIChdGV9hpFOYr/CooRvImCBz1ZykWZN47G11YtDnIelQBt+K7+F4qYWTM2FHIG0WSVIN6y+yWJgvWlBibeQT4tD9uU1Sd9r5KsOsS/hsPIrH0DNgaTpkxwxoGFtiWw73tN9VA21k1TXeCLftURz3H3kezyqEbZlZzpec4Sj8/e6mT/Tm+HQ2crN+nT8qMaQhxN80Rr2u6DXq3TPjPYJM5yZ99XcP+F61J8C1l7sFxU3f260uBL5BgCxcG4wyCWPcYTkwC6pOKbTvzChqRU53XydRk0Hgs9N3zHoKIbFTGnhYklHFhzl5DBWnduN+qphkzhEnfcAXAuabaE+W90yAeoNAvUjo2ujmOTUEyZb5486e6CmOAhycV74TU/5TQtBq4MWe+ikYOOhvrKuCUzn57j7ozrPy6eWArNe774oHdP4ym/ZXuvcWW6Ge637R/hHONAc8Y5iPtOj/qjXI1pDHTxxO2tYt1OD9KN1e9Kv0t3r6/lPmMHn+Wr/T7ke/CVAbP76RL16leMq4BM2no28IuD9R1hY1gWGNHPwGaAoV8S7N1TU5Sf5qkeRvqChyUHDwseCjfzqlwCVQzqbcjMNnCnw5UMjYp5PaJOeeZDJh07yOBf8WNLGJ1n8KAQcYcx7DGHE9iCQB9cFRB04Xpc9lF9pZY7nCQIWCFEPCbg4VJ7AkoerB2haonis54XXL2dDvnO9Rn63o57gSEvWMWkop3OruazeueKPtGaY6pmHgJ3vkZoRR3PIgqPlmPvI78UjzgjrOuBgjfARtrDH/GdzR+rFgd2az3nyz/JVJ/tsvXRkr9aT7hn7CTOcmfcd3D/tmvAPAdVXU5z96j4XMS8IPmHDgc1NHzddbPjTHwGwsD1lwtQEsXy4oc1NSpj4kSMeMaHI66bPDuKhTj4TcUitUQoU0KGNxVIcIuabmbDsw+yCy4Wtb2M72HxpN3gJc1ZwcGmxEloCHR3MWZViQcMw86ov6o2DsEsWZty7a2wa6O8axd3ioKgtaaBMPqz8Rt8Nj+jMtEf4CNsdYkBwHZ8RVM8NSnehUf0IGwkd5al2i/9o7oi2OLBbfZx3lf/uflfNvaXzO57T1vkeyf2J12T4S4C8WLga8cCmzAWLDRq4Ydgo+cuAC2v56kRdxKAOF5J4YIGET/ZaqgUungul8GjzBY3U4HgJg6xTmz6gNkzVUQsH1GllzGsQGDnKpQXm2uJQn8mFiFi5VrIQcBQ/uH18kpD3+VSTmPQlwxo7KG9QtRTWa+9i6++5mQ9d5Dw/w4D78jrVd+t8+BqPtTgISKLqM6QZYbO8c2e+1+75rgHu2bhqUGjn2nWKB6etEbdR7sJHau5EDgCP9HmkBqM8Wrd1Gq/Q3Ornua/s7XN8kv+nX5P6JcB/x5XQxlZPUGLY5PHVgDzfEMBmDLzyKIx4+pWDG5Ly8PMGVT/TRy4wUsTFm43khbe8CUkOie3NA95M8LsSWQMtatK5tUdtbcrOBR852FjiIOYDh84H0RauE+sMq36BQYKH1IEhBhwL9YspHeWJK+hzAJdY5tSHdamt+RGyV3AlSWxwkGyWLIwsujvXXh+8EWfUU5j6Ke6SnANJnWcj8LWcua6xpe1c99kvDo657+1nuDjKy3Z8FguX7fXCj9hZ7QyH5iw3wzXHXn5LWxpn7ZGeW5rP1nftq/W6/lb8lb235vrK3M81Wa7+7XcA8tngxq5nBlfJP/VjM06M91bEeGAB11WVZcIO/WYtHjbx8GuTAI6NPO/UpCVWauKjP7hI6M6eJGHckAODHjH4clQTcblwwMlDzUV0OSiHIvh6GIWYNIWzTkFaYVu26yv2msJyfsgf6V/jQCDPp4YPSD2KN8GQ1wzyYX1Ry+ZDzmuc6/6Ug4Tp9eeK/Vxo4nfeKO6YSx3NbfH29I7Ujjgd67H3nfmzmhkunb08eEc4Z3jq/ag9Os9R/av1jvYF7yt7n5nzndyfa3K72vxDQLcwPVyhePRP+Yz1JgD58Fe/A4DyraurnN2wefOG5gxzXPXokzghHJyHvJbXBAaabxJIS8Op+BEDudKRNRLziOFMlj6JWtmQqTck5KXe3Y85JOL95PscgZEaB9rGIYYp4EQOabiF9zgSfPMkQuopDPqqFrEWOa2/clt2pN3n7LHXdO1Hc9BRrfp17a1Yta6zxZ/lXOcMZ1Q3O48Rd2vuGV/z7eW3tKUhe0TrEa5qZM/0Uc2WvVqPvfAE7qyX9N3p+enpn2ty/wwt/xpg4Po2Pih6fWFzJ46NHldPD2zYWHlF642CsH6lJcii+UE3JtIViK44bW3iEbOd9wTWYpVTruWACSobDnsAQHEuhMUZ9Fau+H0W6SReetZDtW55jUccNfQcMOiH4cbtQulX35aTnOBVnJrKoeUqr8TEjrjCXAuY8InUJqxaWSePMM8f9V2nfDg4kQOrag5wR5Qj9Uc4I23HrtBwPfePah/lufYj/pV9rtQ6ey5f2fvsrO/i/1yT8ZXmLwEi1e9bqw0fVw+bvh6KZSGgKywLTAvYXYMoyW/9Fy04q41b39oPAiRW0gowU+oUB473yxjatSGiV9BctN5UrOH1TKbLemicXNU3tWCGWgnS5Lzwh9zAVzogCcjaQO7WSKtjsxh4PV/oZctr3AdFz4PReU6d5/mVD2Lrp/xRjRHPMV06x9TjqPXamS+trbznxO/2CEc1Z7iq6XZP49m8+u3piCd7lq+6K+1XzvCVva+8hs9q/VyHY1fw9m8BgB9XDZtgfTcAcTz0rX5Y+ri62Hhh/REhFj+tLm4da3PVnRUWGljwsRCHD0mucgQs81QU+f4mAjluStlHEtqolJOtZsHPknU9QGgu5nY0gOeGOLk6B0FGvdcx7eKFA5+PSX/MP+MA17r7MUImdK4IqYOD5nc/+VtG/WRH3FFuhKlW8zkH/laMWtVJZ2RdY5QH1nsJ6/yu1ePOR9xn3KrZykn7CGePO9IYYdLZso/Wdc2zOmf53u+Z2lfouOZR/6pzONrvU3k/1+H4M4O/gssNGxZ3PW7+udFjQ8aGTxzWfVxl5TOHWvLFTX7dTbOGcea4ETcef6/AuCtOcuuNCHjxgFktYWmpEQR/c9OLyJEQrB4QTh1hpPFwy6mHLMuSAyM6cC3icVAOtfLFmVrjooaPdGCwZOFkasErwfDuQC5fFEvKa4FUuXFWeBCKs0jwSEw1SYARV9ZKqs5zXgOC50axY3wNAIjV64R13GP5shRqWj0njvQ9dn+rTjznuK887AifYTPc9dwf8ZXfyl3NkR7skb7Ol4+6R2ulIXuVjvT2rGaX3eP/znldg3c/B9/9mtafAsZXAT4twvJiIoYTG25tyBnzjUDgyNdmh5weg6vCjU03fuThYzOHzb7hLSviu43QOGyjOsNZPIojsYIhACCWRq7zAGg5hCSlAD7to4bL/YRWOoGBv/o7CeBlnbj8lC7RyNVyv8BwNH/kQVGpU+ADZ078JAhnLjBpKE7aTXg0R5DruzrqY/rSuNNUolsQR32MpzkN2nWn/dv8EpryRXjSdv0e78k7332vG+EjzGuO+ns6e8/RXv3ROZz3qOajdd4b/lU6XXcr/oqeW/N8Re7nGjx/1fm/AeKLlvdeXFE9bOPXp3ps+Nz0sXFjiQurJYyCASJWA+cB8xj1HXMN5LGEwUc95sTvAGhlP26u4iZWszhXfnI5UhxoDWMMrpzIyZVEt8pDRj44q9l6kccoyhlgEW7WOt90fKMmbDy4FM4+jL3WfFEF9fMS7tY5Xbv0IjH7UUVxTLTrKO4WJeqvnMmUu5UDyfPul8BBjvNHftfu8ajmGWymP8P3eu3V7eWlf5QH/hmu9J+pc40rdbruLH70fGd63xX/uQ7XPHOrvwPAixoHfvJHEA9++seG3x68sQJLHseBrwUfJCzz6xP0fwWeeWxq3MShFxj7/2up04YHCW5kcODrwUEILZjitEmv7yiwl3KwWiA2XD00v7RYYkHNJb2wSqdklQiXJhLA/FGYdJDMa4MZpaFzYS0OaNZXkuu6K7/Fbzopocpb/0BWOQV9RlSaJmiiIoW1wiJgrBoE8BWjIBZCpHztxeBS24vS77UDyhDyOvdFFibbccVuO9dzV/gz/Rm+13Ovbi8P/SMcn+MsX7WP1qle9iod6e3Zd/fbm+dd+T/1vN9xfevvAPD+GlcaF1uf+GvzB9jeANRdFLiWnindqC3mdw7As9/Yl887eeisPsnbhkf5vNtrs2WLxJBXKzjyywZPbyQ0t3JV2zkR18zqA5sL9cxnjhsswXUvzhtc7yeNmS2u9FCfvmrIid7FBcce4ml0xaXTzqXriH/GusasbspRIs+Jc0Kkzem6KFGZ4/KVkwW+V6Paft2Ey7qmMNheN+PNcNfq/pGaEadjPe59ZvGjdTO9Z/BHZ3m0rs96lU7XncXv7jeb4wf/va5AvQHAafFei1caHrnh1y/bIcan9sj9BxYLmFbgKBsuJCCOhzZ2EIWnxU0fLjdN8PBmIWqAsQ5x4sRw4NAg5Bphyg0sdQKXFaU2dwGwQeJGL6z3BsWFMu+QSoH5Q3hZL4LOoBe4gGtTB5BL5bLCYYlJD4F8I/U6xI4946uNNGSFw+pHAsrBykd+FAPn9YCTy2uEuZ3lhct6zVF/VjvDt3SP1Iw4I6xfo62+yo10juTE+QS7dQ5n5rtK50jPd/Y6Ms+7OH/qeb/r+qrP7R8DiiuOi84NPzZZbIDcBGEjrl8ExAYMLsh6SA143F3uNsHRHSc28+JlfkVDgIUeWEqGrTrgysPHSh5h09CGQg6SyhGIQ2C+uYOiPo57P+JdR3qLZGkYvLzB2ahjj628i234mB8zjpbnQOH5NiLPf1JfVBQGB0bLfWIC9rSCLKq03GpOx/Z8tYSutPd0xNvTPpO/WvNqPZzLTHOGnzn/K7mPzPNIzWjmq3RG2h17Z6/e+6viP/Gcv+pao+/t/wKIQBs+7gT69j8+7ZePNwb+ZgDPVn/GEOuuGy7zEa82It2BZcHDylgbbkkPNEsvcvJBW21aJUB15qQNRGlZnj8SmDctTBED9zcS7IU3RHgzQ+JygK+4nyIZTgigOHJgY4kmrR4vrNtR5TdkEdH1WeHWg408NqJ664SoBTD5Rt11S2vA3MoN6LvQ1XpoKE1Zx7qP+MyaaULDczPNEWeEzeofxY/0uIqDGY9oPXoun1L3J5zj6Fr/qec9uhbvwpbvAMSV980fmzy+0rTx49vutfEjl/n6avRnLnehgiKGT33k8EB9+r4h69v7risdzoIfAVAsTFjmoJOY6upNADjIg5KclV1St6NzWXSrF4l9k1eYuLCjXMCcFXktzRyxcq4tzOnyYcmFxUNzIzFZ1MNB87kvbFI7g9m7J9no1mfEGWEjGUn13CqO2cnDQf6KkNeoYQhLP2s7pfLGHWG9bi/G5d7VyZmcN9M9wvHaEX+EoWaG7+XUb6v+DEfcR+yRGY7oXqVzpNfvzsG19NvOz7X9mmd89aeAsUljE8Zmiw2/Nn38zB+btj/0jMHKD9ef1drcI69Nilxs5L6Q16fo8DkHOOgHm3dMbnrZi1AcgGHRtleUeoKiPOuyhnU4YEmraazOJ+pUSgsuBdOGUZ9wa3mN/Eqm47h82c6tOAirc4wEaliHQ8y2+o6FcmG5kuPPE68DkpmTS34eqO+A4crJDmib2uCj1uvdVx7Wz81x+FpdC3jXIxdgXC/lZJl74LBV7zn30WYVI8Drq60Vp+UUzjgjfIRJ5xl7RPcIRzOc4T5To1rZR/qq9qh9R4+js7yap3OVfXW/H/35FVj9KWBuvHhW4vErNl99B6DeCPQ3AKNnEJjftDKG4QM5OL6Sz3wcuKkhSC7ppskY9eLIhzUeQnEopcLOSRrSvoGT7j2g15YkV7DA6CN3lY8AuD96HjFnHiVQiGQ87jjZ0N8YUEI1W3qWS5lCtmLP+TzCV1iAmk3i4Ikr7Igd1YywVf+B8KjGaXt5cJ3jvut0Xs8dibe09+rP1O5xn82fvRZ7/Ubn/khN17lCo2v2+B09es+viP+U8/yKa/toz/oOwGo3imdq9R0APHPY/PP/AqCPjsD7s4q7rRZyiu0uzDKLSQ+Qn+i8BgmPWUj2cpD2gAOISxzFsEqOcs7LPN8UAG984t7ba8Of1jXeKEQrjbnKW78hx2Yc1qfuKNexvXg4l/VXHjqupblHmGq6FbfbzvNYXMfkb+XEucKe6XOGuzXbVTpbPd6Ze+R8Hqnp53SFRtfs8Tt69J5fEf8p5/kV1/aZnsu/BRAKuCnjOwDY+Otb/f6tf/edg2fWH1s58aIENQhrZ1AOeGjU5ioOyQhigeN8wwonceFJDzmVZUmywkSi9i7wnLikbxv6rWoRzPwAXjSh1Vc1s77GYUlwaoxw7uYf6Tas6lPb2lIbsGM9bnKpYiaLxev9jLlye0/Vr0gROO6+eMJkuy54yvUaxbLOG+mId5X1fiPNvTxqjnC2eEfrfb69mr381jze5wzP6470d/7Iv0JjpOvYO3p4v3f6ODd/vLP3T6/jV2D1vwHiDYC+7c9P+7ER+/8FUG8M9MqV7f1w9+x3UOfiTQJ+tg8MPOUUBwRMGx5D6RmHmzSSWI4vyBrLPGtCC6H60gcmjtX3b1dbSu5dHfUyK7+0s8doXnElrBrEPUcsz6Ny0paAWXFk7/pbbXGsXq7nMB9jPTciySKJ3CDvOqK7RUnndMyvj9de4ffeXXMv3/ln4yP6Rzhn++7x93ru5aF/hHOGtzfzJ+aPXoNPnH1vpt/53PbO/bvl/8a33Xl/xrOWn97rZ/741K9P/rLi6W6MmAJ56sJ9gw+MsOfQK2JssPyEHny++QgL2lKw1qwNB2mSoiZfbdyofZbwa/NObskaL8tvN6XMoeROExh0IRSLP7KQdoLVc6HwKH5tWAIoEoeIVYcUH84RDzaX0lUXAHzh4vE8AA5y4HQ+4jss6yuhc4aArWFtyzNUA5uJc0ZSKSvbdjVbY7lO11aMEudJwjH3le92xul4j7vOXvxsfdef6c3wXj+K/dqO8kexR2d4tE5zPVsvnS37jh5b/V+R+x3P6RXX6dM0sQ0vC89gRPoRgH4HQG8KVja5xOD3BxSh3HHEWB1f0GXj75zOt/Isu7+LS99qcWNa3e2dkzmH3K95wbO14ggHaGvI8XwQWCKirHG2XJ5XI1CvYQqV8zph4kxtFm3xpbvFmepPEl2rx5Oyj4bPnsNZ/qee/CvP45XaV13P7zDj0XPFuehxtOaH91lX4PYdAMyFTRtvAuLTfv0oIDE+0/L7OeBVgDs/LD7529ILRJ/ULXWryVp9gi1u33AkFiL8PxbUc2BBXXbWtKbFXMS04PUVCeZwvvjORM9nzDmRTO0Jbfi/q6FspjvS0Sne5STSZih9OC2HxvrOgfRW8ahGxAcs5Hwh7ljPI97iIK9rsseb5Wc4tLF6XrFs5zheAnnt73IkXHs402PGneFHJn2mtuvrue34K+Mr5x/N+Wr9Uc9XYb/TubzqGn0HXb4BwJ1U/wugvgPAb/37jwC0+eOZb88+w9BgCjz4GRPEmwLbTCVRG33mVpyoXwTDuoviyP0Ky/rsRQ4wkVEfizFsOYSrtgjBRwl43AzTepn6LQp5FN9A1PARh/5/NgDvi30bWBqJq054o09D1Tk5es55AAAUjUlEQVSBGjjkNdI18DcB5CgfVMYuYj5ye2vKQSL61Oslhab8nUaoq9pwdE6FeX6gpefY+aD1eIZJcsRXbmTP8kcaM6xr93hW1/FH67rOkfhML3DtpXpE/o5zpt9d8QHg1foHRniaouv8O5zL0xfjNxGoXwLEHa5+9o+/DqDNXz/71xsAnPjgFaAb7eoTs14xUcs/9ON1+Ir1WL5qwlJTOPpio4B1Dvx4VP9MEwM3a5K2qtVdAzks3fwZ6I6CpHwmloNg1sYB/ekjLQd18uEOdJwu6sgCE64avllBMNFFCsvrFiSOVtPGLMqWM9SMAsfdl9YMG+Gqkd3jIN85PDc7154/qi3eK2yfaS/GDJ0zm+sob1YvfE9nLw+dI5wzPM32aI3qj84l/ln7av2z8zzC1znIPqLxU/N5V+BvPqFx4L/w55u+b/zy/dmXbzdXbirCca7aWcLy/zBQTnhQVptiaqXZvFrFgaa+g4CKSmQ58t43YZrA1X+1+UcSMvz07vzw7zZd9Zv0ECzb5CpUXhYJSHtcZHM285HUORQPDk9urF287KG4nybSym35KTM0Xr93ruA6v/ccNlANCvsJKBZnJtDyfYaNsremrpjrCo3ZSR/VPsqb9TmLv7rfq/XPnu9Z/nef/+z5/mn8X7oxYoPmjwH0SV9vBhDjVaBH5vUjA26cuGrIq1ZcWKyZzRw3Va8xn/oWQ2vFh4bdzBGuFnLxKIocWSOrDSD310EWYI7kJUIDrK/CygmG9R/pdI27OLUkc0TD23P4EN2sa8kW3o00BVaN71k6B88I81Jh4PE1AOtFzX8012Tuw4HwALqvGyC9rseDks1zdv5My6+j8x/1Z30e1Ttah75f1fvIjJ88258w/5Fz/NM5y48A9ErNTf8f+sQ/exOgq4Y7iWrh6wEMD/zsH1Z3HMUBFa58WN3UkcMnV3FA6UuS4PBNAgiqAVaEwCGA3lgSw5sVn6fzwU2OSgD5GuIATeuOk0D9r48pqNFd/86XbuvhPKTYYoPj/CM+9dTbCo7MzFqrGbngiCc74j2CjfRGGLRn+Nm+ezp7ee93hut1W/5ZzbP8rd5buXf10Qyv7PdKbc3/Svvd53/ltfmdtG+/A4DNLjd8/DiAPxIQhleDPxRqU9BOgA0VNYiFhWVp1tcmDx6WePDBAT9sberAYtWGbhwmVJ911GMBs6VDPXAWmEf3DV7lxJFd8QKU7gqPYMjvJMRB5CngsLH0rfwNyjDV5+jxqGjIAdhmBDTkmijPzeKpO9Cfck8kRv1HWJc8wuk1s9ivkfsz/hb+dP3F1/nIPEc4W+f8itwrZ3ql9iuuRdf87vP38/mJ51eAfwqYd/Hc9GvzV6w3AbB64BXivl4xwODng59yMye4ckGj320Q9QYgS8Egl7j8BaIEbtZa1ScA1kMPPqweiL0oi+vHGhmjrmaES2BJKoca+Hogy1za3qZ4tsOIj9rVsmL2ruIVaxgMqaNGga24yelUxqMcsAGekFKccQuzUx2ezwj0Gmk7phph4giHHWGO7+Vda+TP6kfcR7FRjxG2pT/jz3BobeXU6whH3DP2Gd1navdmfKX2Xu9n8phbj2d0fmq/1xX4hY0Qmxj/DwD89r/+D4C09fcA8N2B3OBrY9crRht/ULjJwsaDSxxZ4WEJFTGAvFPrhl31ViO3OFE/kBatLH+kEJHXobBq01cBeMUV2Ow0X6LrAsC1VkGhK0cy9cYlgANlK9Ih/qrrRuAnLGHHJqWienqEed79zvV41t45rnXEP1t7lq8Zep3H8D1WzTN2pjfDn+n1ybWz18yzM3/H64iZv+Pczz5XP/XLFfiln7Vzo8cmrzcA2NTxXQBt7nqlIO4PaAV29ylVryy30hEWpYQcN580HkQ0G27t0iMO8lrSRCyu/Igdwg2CdN0pGIAcOLgiK7+kapQM16IF3juUSU1pM8RBvaxsAhtj7XaJWf2It1a6Rc51/8Y46elahhj09NhTuaT3qMnRAUa1E+wVs75CczL+S+Ez53GG+9Khv7n4z3X85k/gBeMv/xpgvBL8W/96E8BP+vrkj00//PqOAF49wEZ2hImLHBZse6w2V5LsoDorU702TcZZQrpqZFEbvkJZ67JIKiHrBPmRqzScjAtLHlOxuXVcMrDK6TxoWeise18UWTIk1uh3sM2kvRclneex+03+6RDaru9+iTdQoWzxmrOXb/RxGCKX6IzVH0Jn84zwEbbX9JGaPc1R/kyfM9xRL2BXaLg29K7WdP2r/e8279Xn/6N3uwLL/wYYmzN/6c9/BICNX5u/+9jIwc8NXT8OWG1amas3CPGK08/X+ZWiV6Db20y3L6bMw9RS0G0QBN2cG6jNVR/Ti3ujrMo8ANf5EdZa/S7BgFTXpSrWjrQHpWviJBrWGeib+0rCOOauKM8G0sUM8mnbUMCUr553QGYMbzIkWHoYl/4Bx7XoO5D1A6iUH82VwAPOVs+R3Iw/w6WxlwfvCEd677JXz3S13quvw3eb99XX40/Xv/0IQJu/fgTgm39u+r6h86s7X021ySHWK0w2r3BtlM7pVz9rSg958WUTS2q8s1hzUMtcEmD4CF5CUBguvUkhX4VD5gLuUZCXZm++V7vRdpXyTbA0HTQ28qsVwB3mhJbssp52nxIGlFvO7WlTu64t/Iw1+buyUW6EqfBIbosjnZF9tG6k1bErrmPXfGX8ymvR576619V6fd6r4+8279Xn/6N3fwWwtd8+/fum75/6heMV1B+hUJscxDyPMGJAWPJFuSWQJKVscQzuWPVKTjeSBH53YzQx5ox8x01ho1QrcU2ucnREkF1nr41ywNGcur7DXJtixMH4wEc5Lz/Ccf7U1/VSw8uEpx2ZULtt1ji7NaLr6tTGKr8H6uf7CWf0afO8+5r86ef/7uv9XfotPwLABv+/8qHvAPin/vD5rX5h8WriZo5XlT+QRww+XBywsn4J4pg1TItTyVuePOATTs0Qd1RSNu6snAUkPaA7Wa5FX7xVEOBIK7ARLAnYLuO5mb9xasOSvR5H9MSZnY/ywwF2wKPzOW82R2/lNT2HeC8/qvkE7LvO/QnX7k+e4ed18yc/+9vn/ouf3vEGwH4EwL8EqE/9tunzztljvLqAhdUmyxccDnqEW6th3MSVZOH603pCYix9FGkHkqbIM4s65aQRMeduKUrj0PlZ57D7kr2zSTrEtWLxZS11yJ3VOe6+i85wcZA/whFfdq9GPLejXiOdEdZ1PJa/Vyfeq+xe/7381XPpS+sR3VfN+irdR87xu9T8XLPv8kx9zZzLHwLSdwD0JiDi4ZuA3Oh519cbAcfiHPiCw2HrgXNVPn2+EXAMOBYwLM8JA+53KucghyWurPMHadWQrhqCywGQ4NWPPowzc/UGaZaf4eopu+LpfDSUJQeQZc0NDXKtwFwjLu4sB3yWQ6VyGnlRW47KyXpOvtdt8cQ/wgH3KE+6R+wZzTPcI72Pcrb6Ppo72hu8rR5d5wy3174i/rR5+jlivk+fsc/8E7//CvziqwQbv34EoO8G5Aav3/InL19Vd5u1Xm2yOI/Rq69jirMuzVKqYCKFFjUTg/VBn+qF+uYhbNNGf4zAZbMIgsUvNhbHE+bv5Y26cictyZHmkXMSdyXegwGpaw8oXYWx6sRXPCQ/CaKH+jwptVnuPdzfLHoi+Y4eW+N9df+t2T4h9+nX59Pn+4Tn8GeG5Qr84otFbwD0bX9YPJDEY/JpPzLMi6a47sqWWG0EhrMmD/prfVWvJPh9jbDOeSA+JTsgD6C6jDVOI/m1Qaqlq8yd4pXj2Zvv2jf0AW+nzwOKLNG5vmrO2djq++jcX103mn92rlfOOur7iP5VOkd6v7PXkXl+OD9X4FOuwN/8Nra+9S+rb+trStyd8VUU/9gP/xBQWNYlXDucvtJgm8/vGgRcy/O6+yc2+mRNSR5CwfmBZdn9J3Lkgssy2Sgvfg6juBLJrVknTv2vjZM84NLuHCTyPODq0WlbsWpgZ4scHLIXeKrrNa4z4/Qa6RGXwE6vM9qjfiPMW8sf8YDN8jN8puP4M7WuM/Kf1R7Vj7BR70/Anpn1mdrRuV+tN+rxDPbp8z1zbj+111+BX/iDPv/Bt///r7gx6g2AvhPQP/nrjQFeZfLD5bfbLe47DPP5yrzzcU7I6YE4F0uyTndtYYJJVZBWoXRg2bdbI5qrVlWOXOVXQeKVzJIRp9RcbAF7uVP3/IdrHyj0Evf3ZvwO+d/tfHTNR+c1wsT/sd/3Cvw8r9/3ufuqyX9x04/Nn78DgDcAffPHqwoPbfAZ8zsB6dcHPnGDjuUhfOk4DvhuicCiu+wNQGM8xL9l6LE8c/qFPdlG9Q/IS2qi2euOxHV9RDYAbSwU47BdfSdmUCVtXouWFyZOS69CcVfgC4N39ztyDV54um+V/lPO9d2vobc+ia3Zn3Su7dR/wieuwC9u+PoFQG3ysP2BV5g/ECoeDFC5zvG7T+T0ybwkAnMKcWgMFmpZHwWk4KBH50tU1vKEKNBBi8NdURRIb9C3zk0ckyM9NSRl6Yfcq3RGzQenMKJdiul8ZB8Rx9yaXTqKoSfsEe1ZzVRzmpgpvR//BiO+/6J8eMef5+zDn6APHu8Xvv3PHwHo079/ByBeWbxZ6hUGKx8npVhvHBKrjQ8xlt9xEasO/mh5j1H+EeysZvD3xsQYOrUZV/lHRj5U0xoMT9M45g7lh/XBnOFDkQQfqdnSU066OJe981HNp1qdy6fO99Vz/Vyfr34Gfvr/zlfg13+w8duPAPit/fz0z/8FUJt7fCXyixEHw+riIO8cJMD1hzBYrawrHnDd1ZUTFxZ6sfSt7yXaP05/YU/zdYmYQWNUKnsjNrfS7mzlVzkEK8BVdvxB7XBm0ze3xEeYksrd6YpwwHot9O7eIALLB+TcR/wVSzPr/K+c4RWaV873jNannNsr5niF5jPXGrWfONOz5/RT/74r8Ouv/45megPgn/59kw+fN2282vRAvi/lgPsrU75sr0v6Kr0K7gmr9CpI8RGGVMMRCpIFTRtAJQFOlteB0uN7YCJ0AYzed/1Td4SPMNIHieE1GfAuOI2nJT50rOlzoxM+MveIM8Kk+S77CTO86lw/8dw+caZXXf8f3ddcgV9//e8QxhuA/iMAvLrw0EavOOH6RG14pM4t1B5YncYYu5F61840F6vvGEy4vcdc6ZaZSN0I8B4RXiuMI537OPsW9FWn9pbhn2zyknN/ieiTJ/pT/nMFfq7Ab3sF/q5P/9ro/VRxQ/KNFvHgQQiHo0safQeNuLR6zrVn9c7p/pZe5x6NH5njqPYR3ivO6UjfAQeXYrZ0mbbys9xX4FvncnaeZ7WeqX+m9ux5HuV/4kxHZ//h/VyB3+0K8DsA/8jvAOBvAuATP3/2v/eVivyI4/jIH9XkVa0fM/SrvFHTqYofKFEprY++SkTwrHbXq4bDxJvAAye1dU3eNOVdmwNjv+b5upvkB7j6Chx5bq/u+V30fq7Nd3mmPnvOX3/93zEg/jdAbP7+OwB4heHNQNjamEevOmDCZQMqrPuIJ6u+Te86E+4KPstfFZ8L3tjq3GAfwsb1edc1OtLnCOdDLt3PGHYFnnnenqm1EX7cnyvw21+B5XcA8PN/fNX4A28IIuZ3BQyvNwNXXhrpp2b9fgFi5HYWKQd4OzIvT7fTfLzfgXM9QJn2f6Z2KvobJ36X6/U7nMcrzuEVmr/xl8PPqX2jK/CrfgEwP+2vNtzRKx+YHjpRxSO+OF9hP22er7gGrefokoywVvbR4Xef/6Mv7jcb7oN+LeZlV+7n9f6yS/vHCf/iz///K15S+Yn/bnPHJRm94mZfaeCO+LNLe4Y707gQ5zgHz+Eg7cLpvkbq6aeovVYUPq37NZfj67rqwn3BBH/Ka/0LLu1Py58r8CVXALeTv//5v/7517/+8S/+S3//jq9yfKEjUX4A+Gd6gfmDeZCBD/LOVT7ppcNa1PdHAjBIwuLHArB84AC/gBuHBCYTAy8eOi/OEACxbr1ukKvz6DnE8ViGW3zOG5jm5o81rM65aIu4zyYOLPSpkVzG6YsXlIUDRw+4qg2ruhFWNahtdQsAkB4tXJ0X8FUMmrjuJ1b9QQuszi25mhPhnY5reH7Q705bfOcKg8VSTnZBb/hRjvO6Rtfu+a1acUecI7lR7xF2REucvfpX8aQre3QO8Y/YV2ge6fvD+bkCL7oC+LyPPwH093///d9//cIL/D//s/wOYOxCv/LBbwwMfH6zIHDkixMSqsPvCfwXfqQQusDEwe8Y8v8wAFeP+KeFVVcWtcijPi21wA0AGGNphMXi7zBG4T9SEzdyciNXNcjH41dwaIOjGP3hQ1tYxdAKEWkHbVWPGP9cMjXAjQcWtRCjX1jOnTqsSQzXhRwMCv6IAxyiqgkXNR5zvsgDIx5GfWkj5oIGZkIgPdgIweNyTuKowRrWAU8tklwnfIaJdd7dbMFjDwipZqDv8+pNg2M1R2pAlOeHeISxYDkULwdZxZpJGllHjmlUD2HiywqHbdidlrjOc1952bO5s3z1CTud1Th0t3o49yjPa+CjDo98zgA9taT3lMjFxY9em4vH+JH7hlcgNrT//Pvff/0Lj7hh/v1/fv2fv/757//vr3/99c/YQH/dNuM4N27cYbHpaBPHhqjN/b/ihVg4eNi0xA/rPjd3bYKRQIxabrQZF6bNL/jsFZYbKyx6pg43zMTwRU88Y26MmCW+uaFNURueYtaDA72s6xzE0MY1mHHYKzi4CRYnfKw4NeLqwTh7QVd51FIHs6BGHIjEQkwOAqvjGw3VAFdd+Filn/UCNavy1ER9arM29cDxmH4cwK06gLGE0SeSmugfQswnD7NiQPZMrbt4oSxKmAePJRrWIVV6ImrOjDlDw3Qe6s8W2a8aWkwNkAxDWNq9l8dZx1rNmJiHXRuUO6yfB0k7h6yp80pdaK8wlxnVKI/c0XWUu9Vvq9ejdTPNo/PO6q/GP22eq8/vR+/lV+Df//M/f/2///znX/9P2P8fByb4M3D7iBwAAAAASUVORK5CYII=";

})();
(function() {

    class CubemapCaptureImpl {
		constructor() {
		}

		createCaptureBuffers(context,size) {
			console.log("CubemapCaptureImpl.createCaptureBuffers() not implemented");
		}

		beginRender(context,captureBuffers) {
			console.log("CubemapCaptureImpl.beginRender() not implemented");
        }
        
        beginRenderFace(context,face,captureBuffers,viewMatrix) {
            console.log("CubemapCaptureImpl.renderFace() not implemented");
            return bg.Matrix4.Identity();   // This function must return the projection matrix for the face
        }

        endRenderFace(context,face,captureBuffers) {
            console.log("CubemapCaptureImpl.endRenderFace() not implemented");
        }

		endRender(context,captureBuffers) {
			console.log("CubemapCaptureImpl.endRender() not implemented");
        }

		getTexture(context,captureBuffers) {
			console.log("CubemapCaptureImpl.getTexture() not implemented");
        }
        
        destroy(context,captureBuffers) {
            console.log("CubemapCaptureImpl.destroy() not implemented");
        }
	};

    bg.base.CubemapCaptureImpl = CubemapCaptureImpl;
    
    class CubemapCapture extends bg.app.ContextObject {
        constructor(context) {
            super(context);
            this._captureBuffers = null;
            this._texture = null;
        }

        create(size) {
            this._captureBuffers = bg.Engine.Get().cubemapCapture.createCaptureBuffers(this.context,size);
        }

        // This function does not destroy the textures created with this instance
        // of CubemapCapture
        destroy() {
            if (this._captureBuffers) {
                bg.Engine.Get().cubemapCapture.destroy(this.context,this._captureBuffers);
            }
        }

        get texture() {
            return this._texture;
        }

        updateTexture(renderCB,viewMatrix) {
            let cap = bg.Engine.Get().cubemapCapture;
            cap.beginRender(this.context,this._captureBuffers);

            for (let i=0; i<6; ++i) {
                let matrix = cap.beginRenderFace(this.context,i,this._captureBuffers,viewMatrix);
                renderCB(matrix.projection,matrix.view);
                cap.endRenderFace(this.context,i,this._captureBuffers);
            }

            cap.endRender(this.context,this._captureBuffers);

            if (!this._texture) {
                this._texture = cap.getTexture(this.context,this._captureBuffers);
            }
        }
    }

    bg.base.CubemapCapture = CubemapCapture;
})();
(function() {
	
	function lib() {
		return bg.base.ShaderLibrary.Get();
	}

	class DrawTextureEffect extends bg.base.TextureEffect {
		constructor(context) {
			super(context);
			
			let vertex = new bg.base.ShaderSource(bg.base.ShaderType.VERTEX);
			let fragment = new bg.base.ShaderSource(bg.base.ShaderType.FRAGMENT);
			vertex.addParameter([
				lib().inputs.buffers.vertex,
				lib().inputs.buffers.tex0,
				{ name:"fsTexCoord", dataType:"vec2", role:"out" }
			]);
			
			fragment.addParameter([
				lib().inputs.material.texture,
				{ name:"fsTexCoord", dataType:"vec2", role:"in" }
			]);
			
			if (bg.Engine.Get().id=="webgl1") {
				vertex.setMainBody(`
				gl_Position = vec4(inVertex,1.0);
				fsTexCoord = inTex0;`);
				fragment.setMainBody("gl_FragColor = texture2D(inTexture,fsTexCoord);");
			}
			
			this.setupShaderSource([
				vertex,
				fragment
			], false);
		}
		
		setupVars() {
			let texture = null;
			if (this._surface instanceof bg.base.Texture) {
				texture = this._surface;
			}
			else if (this._surface instanceof bg.base.RenderSurface) {
				texture = this._surface.getTexture(0);
			}
			
			if (texture) {
				this.shader.setTexture("inTexture",texture,bg.base.TextureUnit.TEXTURE_0);
			}
		}
	}
	
	bg.base.DrawTextureEffect = DrawTextureEffect;
})();
(function() {

    function destoryResource(paramName) {
        if (this[paramName]) {
            this[paramName].destroy();
            this[paramName] = null;
        }
    }

    class Environment extends bg.app.ContextObject {
        constructor(context) {
            super(context);

            this._irradianceIntensity = 1;

            // Cubemap generators:
            this._cubemapRenderer = null;       // Convert the equirectangular 360º image into a cubemap texture
            this._irradianceRenderer = null;    // Generate the irradiance map
            this._specularRenderer = null;      // Generate the specular reflections
    

            // Renderer: generate the cubemap textures
            this._cubemapCapture = null;        // Main cubemap and specular reflection L0 (from 0% to 33% roughness)
            this._irradianceCapture = null;     // Irradiance map
            this._specularCaptureL1 = null;     // Specular reflections L1 (from 33% to 66% roughness)
            this._specularCaptureL2 = null;     // Specular reflections L2 (from 66% yo 100% roughness)

            this._blackTexture = bg.base.TextureCache.BlackTexture(context);
            this._texture = this._blackTexture;
        }

        set equirectangularTexture(t) {
            this._texture = t;
            if (this._cubemapRenderer) {
                this._cubemapRenderer.texture = t;
            }
        }

        set irradianceIntensity(i) { this._irradianceIntensity = i; }

        get equirectangularTexture() { return this._texture; }

        get cubemapTexture() { return this._cubemapCapture ? this._cubemapCapture.texture : this._blackTexture; }
        get irradianceMapTexture() { return this._irradianceCapture ? this._irradianceCapture.texture : this._blackTexture; }
        get specularMapTextureL0() { return this._cubemapCapture ? this._cubemapCapture.texture : this._blackTexture; }
        get specularMapTextureL1() { return this._specularCaptureL1 ? this._specularCaptureL1.texture : this._blackTexture; }
        get specularMapTextureL2() { return this._specularCaptureL2 ? this._specularCaptureL2.texture : this._blackTexture; }
        get irradianceIntensity() { return this._irradianceIntensity; }

        create(params) {
            if (!params) {
                params = {};
            }
            params.cubemapSize = params.cubemapSize || 512;
            params.irradianceMapSize = params.irradianceMapSize || 32;
            params.specularMapSize = params.specularMapSize || 32;
            params.specularMapL2Size = params.specularMapL2Size || params.specularMapSize || 32;
            this.destroy();
            this._cubemapRenderer = new bg.render.EquirectangularCubeRenderer(this.context,90);
            this._cubemapRenderer.create();
            this._cubemapRenderer.texture = this.texture;

            this._irradianceRenderer = new bg.render.CubeMapRenderer(this.context,90);
            this._irradianceRenderer.create(bg.render.CubeMapShader.IRRADIANCE_MAP);

            this._specularRenderer = new bg.render.CubeMapRenderer(this.context,90);
            this._specularRenderer.create(bg.render.CubeMapShader.SPECULAR_MAP);
            
            this._cubemapCapture = new bg.base.CubemapCapture(this.context);
            this._cubemapCapture.create(params.cubemapSize);

            this._irradianceCapture = new bg.base.CubemapCapture(this.context);
            this._irradianceCapture.create(params.irradianceMapSize);

            this._specularCaptureL1 = new bg.base.CubemapCapture(this.context);
            this._specularCaptureL1.create(params.specularMapSize);

            this._specularCaptureL2 = new bg.base.CubemapCapture(this.context);
            this._specularCaptureL2.create(params.specularMapL2Size);

            this._frame = 0;

            this._maxTextureUnits = this.context.getParameter(this.context.MAX_TEXTURE_IMAGE_UNITS);
        }

        update(camera) {
            let view = new bg.Matrix4(camera.viewMatrix);
            view.setPosition(0,0,0);
            //this._cubemapRendere.pipeline.viewport = this._camera.viewport;

            // Update main cubemap
            this._cubemapCapture.updateTexture((projectionMatrix,viewMatrix,usePipeline) => {
                this._cubemapRenderer.viewMatrix = viewMatrix;
                this._cubemapRenderer.projectionMatrix = projectionMatrix;
                this._cubemapRenderer.render(!usePipeline);
            }, view);


            // Update cubemap renderers textures
            this._irradianceRenderer.texture = this._cubemapCapture.texture;
            this._specularRenderer.texture = this._cubemapCapture.texture;

            // Update irradiance and specular maps
            if (this._frame == 0) {
                this._irradianceCapture.updateTexture((projectionMatrix,viewMatrix) => {
                    this._irradianceRenderer.viewMatrix = viewMatrix;
                    this._irradianceRenderer.projectionMatrix = projectionMatrix;
                    this._irradianceRenderer.render(true);
                }, bg.Matrix4.Identity(), view);
                this._frame = 1;
            }
            else if (this._frame == 1) {
                this._specularCaptureL1.updateTexture((projectionMatrix,viewMatrix) => {
                    this._specularRenderer.viewMatrix = viewMatrix;
                    this._specularRenderer.projectionMatrix = projectionMatrix;
                    this._specularRenderer.roughness = this._maxTextureUnits>8 ? 0.2 : 0.3;
                    this._specularRenderer.render(true);
                }, bg.Matrix4.Identity(), view);
                this._frame = this._maxTextureUnits>8 ? 2 : 0;
            }
            else if (this._frame == 2) {
                this._specularCaptureL2.updateTexture((projectionMatrix,viewMatrix) => {
                    this._specularRenderer.viewMatrix = viewMatrix;
                    this._specularRenderer.projectionMatrix = projectionMatrix;
                    this._specularRenderer.roughness = 0.8;
                    this._specularRenderer.render(true);
                }, bg.Matrix4.Identity(), view);
                this._frame = 0;
            }
        }

        renderSkybox(camera) {
            let view = new bg.Matrix4(camera.viewMatrix);
            view.setPosition(0,0,0);
            this._cubemapRenderer.pipeline.viewport = camera.viewport;
            this._cubemapRenderer.viewMatrix = view;
            this._cubemapRenderer.projectionMatrix = camera.projection;
            this._cubemapRenderer.render(false);
        }
    
        destroy() {
            destoryResource.apply(this,["_cubemapRenderer"]);
            destoryResource.apply(this,["_irradianceRenderer"]);
            destoryResource.apply(this,["_specularRenderer"]);
            destoryResource.apply(this,["_cubemapCapture"]);
            destoryResource.apply(this,["_irradianceCapture"]);
            destoryResource.apply(this,["_specularCaptureL1"]);
            destoryResource.apply(this,["_specularCaptureL2"]);
        }

        clone() {
            console.warn("bg.base.Environment.clone(): not implemented");
        }

        serialize(data,promises,url) {
            console.warn("bg.base.Environment.serialize(): not implemented");
        }

        deserialize(data,url) {
            console.warn("bg.base.Environment.deserialize(): not implemented");
        }
    }

    bg.base.Environment = Environment;
})();
(function() {
	let shaders = {};
	
	function lib() {
		return bg.base.ShaderLibrary.Get();
	}
	
	let s_vertexSource = null;
	let s_fragmentSource = null;
	
	function vertexShaderSource() {
		if (!s_vertexSource) {
			s_vertexSource = new bg.base.ShaderSource(bg.base.ShaderType.VERTEX);
			
			s_vertexSource.addParameter([
				lib().inputs.buffers.vertex,
				lib().inputs.buffers.normal,
				lib().inputs.buffers.tangent,
				lib().inputs.buffers.tex0,
				lib().inputs.buffers.tex1
			]);
			
			s_vertexSource.addParameter(lib().inputs.matrix.all);
			
			s_vertexSource.addParameter([
				{ name:"inLightProjectionMatrix", dataType:"mat4", role:"value" },
				{ name:"inLightViewMatrix", dataType:"mat4", role:"value" },
			]);
			
			s_vertexSource.addParameter([
				{ name:"fsPosition", dataType:"vec3", role:"out" },
				{ name:"fsTex0Coord", dataType:"vec2", role:"out" },
				{ name:"fsTex1Coord", dataType:"vec2", role:"out" },
				{ name:"fsNormal", dataType:"vec3", role:"out" },
				{ name:"fsTangent", dataType:"vec3", role:"out" },
				{ name:"fsBitangent", dataType:"vec3", role:"out" },
				{ name:"fsSurfaceToView", dataType:"vec3", role:"out" },
				
				{ name:"fsVertexPosFromLight", dataType:"vec4", role:"out" }
			]);
			
			if (bg.Engine.Get().id=="webgl1") {
				s_vertexSource.setMainBody(`
					mat4 ScaleMatrix = mat4(0.5, 0.0, 0.0, 0.0,
											0.0, 0.5, 0.0, 0.0,
											0.0, 0.0, 0.5, 0.0,
											0.5, 0.5, 0.5, 1.0);
					
					vec4 viewPos = inViewMatrix * inModelMatrix * vec4(inVertex,1.0);
					gl_Position = inProjectionMatrix * viewPos;
					
					fsNormal = normalize((inNormalMatrix * vec4(inNormal,1.0)).xyz);
					fsTangent = normalize((inNormalMatrix * vec4(inTangent,1.0)).xyz);
					fsBitangent = cross(fsNormal,fsTangent);
					
					fsVertexPosFromLight = ScaleMatrix * inLightProjectionMatrix * inLightViewMatrix * inModelMatrix * vec4(inVertex,1.0);
					
					fsTex0Coord = inTex0;
					fsTex1Coord = inTex1;
					fsPosition = viewPos.xyz;`);
			}
		}
		return s_vertexSource;
	}
	
	function fragmentShaderSource() {
		if (!s_fragmentSource) {
			s_fragmentSource = new bg.base.ShaderSource(bg.base.ShaderType.FRAGMENT);

			s_fragmentSource.addParameter(lib().inputs.material.all);
			s_fragmentSource.addParameter(lib().inputs.lightingForward.all);
			s_fragmentSource.addParameter(lib().inputs.shadows.all);
			s_fragmentSource.addParameter(lib().inputs.colorCorrection.all);
			s_fragmentSource.addParameter([
				{ name:"fsPosition", dataType:"vec3", role:"in" },
				{ name:"fsTex0Coord", dataType:"vec2", role:"in" },
				{ name:"fsTex1Coord", dataType:"vec2", role:"in" },
				{ name:"fsNormal", dataType:"vec3", role:"in" },
				{ name:"fsTangent", dataType:"vec3", role:"in" },
				{ name:"fsBitangent", dataType:"vec3", role:"in" },
				{ name:"fsSurfaceToView", dataType:"vec3", role:"in" },
				
				{ name:"fsVertexPosFromLight", dataType:"vec4", role:"in" },

				{ name:"inCubeMap", dataType:"samplerCube", role:"value" },
				{ name:"inLightEmissionFactor", dataType:"float", role:"value" }
			]);
			
			s_fragmentSource.addFunction(lib().functions.materials.all);
			s_fragmentSource.addFunction(lib().functions.colorCorrection.all);
			s_fragmentSource.addFunction(lib().functions.utils.unpack);
			s_fragmentSource.addFunction(lib().functions.utils.random);
			s_fragmentSource.addFunction(lib().functions.lighting.all);
			s_fragmentSource.addFunction(lib().functions.blur.blurCube);
			
			if (bg.Engine.Get().id=="webgl1") {	
				s_fragmentSource.setMainBody(`
					vec4 diffuseColor = samplerColor(inTexture,fsTex0Coord,inTextureOffset,inTextureScale);
					vec4 lightmapColor = samplerColor(inLightMap,fsTex1Coord,inLightMapOffset,inLightMapScale);

					if (inUnlit && diffuseColor.a>=inAlphaCutoff) {
						gl_FragColor = diffuseColor * lightmapColor;
					}
					else if (diffuseColor.a>=inAlphaCutoff) {
						vec3 normalMap = samplerNormal(inNormalMap,fsTex0Coord,inNormalMapOffset,inNormalMapScale);
						// This doesn't work on many Mac Intel GPUs
						// vec3 frontFacingNormal = fsNormal;
						// if (!gl_FrontFacing) {
						// 	frontFacingNormal *= -1.0;
						// }
						normalMap = combineNormalWithMap(fsNormal,fsTangent,fsBitangent,normalMap);
						vec4 shadowColor = vec4(1.0);
						if (inReceiveShadows) {
							shadowColor = getShadowColor(fsVertexPosFromLight,inShadowMap,inShadowMapSize,inShadowType,inShadowStrength,inShadowBias,inShadowColor);
						}

						vec4 specular = specularColor(inSpecularColor,inShininessMask,fsTex0Coord,inTextureOffset,inTextureScale,
															inShininessMaskChannel,inShininessMaskInvert);
						float lightEmission = applyTextureMask(inLightEmission,
															inLightEmissionMask,fsTex0Coord,inTextureOffset,inTextureScale,
															inLightEmissionMaskChannel,inLightEmissionMaskInvert);
						diffuseColor = diffuseColor * inDiffuseColor * lightmapColor;
						
						vec4 light = vec4(0.0,0.0,0.0,1.0);
						vec4 specularColor = vec4(0.0,0.0,0.0,1.0);
						// This doesn't work on A11 and A12 chips on Apple devices.
						// for (int i=0; i<${ bg.base.MAX_FORWARD_LIGHTS}; ++i) {
						// 	if (i>=inNumLights) break;
						// 	light.rgb += getLight(
						// 		inLightType[i],
						// 		inLightAmbient[i], inLightDiffuse[i], inLightSpecular[i],inShininess,
						// 		inLightPosition[i],inLightDirection[i],
						// 		inLightAttenuation[i].x,inLightAttenuation[i].y,inLightAttenuation[i].z,
						// 		inSpotCutoff[i],inSpotExponent[i],inLightCutoffDistance[i],
						// 		fsPosition,normalMap,
						// 		diffuseColor,specular,shadowColor,
						// 		specularColor
						// 	).rgb;
						// 	light.rgb += specularColor.rgb;
						// }

						// Workaround for A11 and A12 chips
						if (inNumLights>0) {
							light.rgb += getLight(
								inLightType[0],
								inLightAmbient[0], inLightDiffuse[0], inLightSpecular[0],inShininess,
								inLightPosition[0],inLightDirection[0],
								inLightAttenuation[0].x,inLightAttenuation[0].y,inLightAttenuation[0].z,
								inSpotCutoff[0],inSpotExponent[0],inLightCutoffDistance[0],
								fsPosition,normalMap,
								diffuseColor,specular,shadowColor,
								specularColor
							).rgb;
							light.rgb += specularColor.rgb;
						}
						if (inNumLights>1) {
							light.rgb += getLight(
								inLightType[1],
								inLightAmbient[1], inLightDiffuse[1], inLightSpecular[1],inShininess,
								inLightPosition[1],inLightDirection[1],
								inLightAttenuation[1].x,inLightAttenuation[1].y,inLightAttenuation[1].z,
								inSpotCutoff[0],inSpotExponent[1],inLightCutoffDistance[1],
								fsPosition,normalMap,
								diffuseColor,specular,shadowColor,
								specularColor
							).rgb;
							light.rgb += specularColor.rgb;
						}

						vec3 cameraPos = vec3(0.0);
						vec3 cameraVector = fsPosition - cameraPos;
						vec3 lookup = reflect(cameraVector,normalMap);

						// Roughness using gaussian blur has been deactivated because it is very inefficient
						//float dist = distance(fsPosition,cameraPos);
						//float maxRough = 50.0;
						//float rough = max(inRoughness * 10.0,1.0);
						//rough = max(rough*dist,rough);
						//float blur = min(rough,maxRough);
						//vec3 cubemapColor = blurCube(inCubeMap,lookup,int(blur),vec2(10),dist).rgb;

						vec3 cubemapColor = textureCube(inCubeMap,lookup).rgb;

						float reflectionAmount = applyTextureMask(inReflection,
														inReflectionMask,fsTex0Coord,inTextureOffset,inTextureScale,
														inReflectionMaskChannel,inReflectionMaskInvert);

						light.rgb = clamp(light.rgb + (lightEmission * diffuseColor.rgb * 10.0), vec3(0.0), vec3(1.0));
						

						gl_FragColor = vec4(light.rgb * (1.0 - reflectionAmount) + cubemapColor * reflectionAmount * diffuseColor.rgb, diffuseColor.a);
					}
					else {
						discard;
					}`
				);
			}
		}
		return s_fragmentSource;
	}
	
	class ColorCorrectionSettings {
		constructor() {
			this._hue = 1;
			this._saturation = 1;
			this._lightness = 1;
			this._brightness = 0.5;
			this._contrast = 0.5;
		}

		set hue(h) { this._hue = h; }
		get hue() { return this._hue; }
		set saturation(s) { this._saturation = s; }
		get saturation() { return this._saturation; }
		set lightness(l) { this._lightness = l; }
		get lightness() { return this._lightness; }
		set brightness(b) { this._brightness = b; }
		get brightness() { return this._brightness; }
		set contrast(c) { this._contrast = c; }
		get contrast() { return this._contrast; }

		apply(shader,varNames={ hue:'inHue',
								saturation:'inSaturation',
								lightness:'inLightness',
								brightness:'inBrightness',
								contrast:'inContrast' })
		{
			shader.setValueFloat(varNames['hue'], this._hue);
			shader.setValueFloat(varNames['saturation'], this._saturation);
			shader.setValueFloat(varNames['lightness'], this._lightness);
			shader.setValueFloat(varNames['brightness'], this._brightness);
			shader.setValueFloat(varNames['contrast'], this._contrast);
		}
	}

	bg.base.ColorCorrectionSettings = ColorCorrectionSettings;

	class ForwardEffect extends bg.base.Effect {
		constructor(context) { 
			super(context);
			this._material = null;
			this._light = null;
			this._lightTransform = bg.Matrix4.Identity();

			this._lightArray = new bg.base.LightArray();

			this._shadowMap = null;
			
			let sources = [
				vertexShaderSource(),
				fragmentShaderSource()
			];
			this.setupShaderSource(sources);
			this._colorCorrection = new bg.base.ColorCorrectionSettings();
		}
		
		get material() { return this._material; }
		set material(m) { this._material = m; }
				
		// Individual light mode
		get light() { return this._light; }
		set light(l) { this._light = l; this._lightArray.reset(); }
		get lightTransform() { return this._lightTransform; }
		set lightTransform(trx) { this._lightTransform = trx; this._lightArray.reset();}

		// Multiple light mode: use light arrays
		get lightArray() { return this._lightArray; }
		
		set shadowMap(sm) { this._shadowMap = sm; }
		get shadowMap() { return this._shadowMap; }

		get colorCorrection() { return this._colorCorrection; }
		set colorCorrection(cc) { this._colorCorrection = cc; }
		
		beginDraw() {
			if (this._light) {
				// Individual mode: initialize light array
				this.lightArray.reset();
				this.lightArray.push(this.light,this.lightTransform);
			}

			if (this.lightArray.numLights) {
				let matrixState = bg.base.MatrixState.Current();
				let viewMatrix = new bg.Matrix4(matrixState.viewMatrixStack.matrixConst);

				// Update lights positions and directions using the current view matrix
				this.lightArray.updatePositionAndDirection(viewMatrix);
				
				// Forward render only supports one shadow map
				let lightTransform = this.shadowMap ? this.shadowMap.viewMatrix : this.lightArray.shadowLightTransform;
				this.shader.setMatrix4("inLightProjectionMatrix", this.shadowMap ? this.shadowMap.projection : this.lightArray.shadowLight.projection);
				let shadowColor = this.shadowMap ? this.shadowMap.shadowColor : bg.Color.Transparent();
				
				let blackTex = bg.base.TextureCache.BlackTexture(this.context);
				this.shader.setMatrix4("inLightViewMatrix",lightTransform);
				this.shader.setValueInt("inShadowType",this._shadowMap ? this._shadowMap.shadowType : 0);
				this.shader.setTexture("inShadowMap",this._shadowMap ? this._shadowMap.texture : blackTex,bg.base.TextureUnit.TEXTURE_5);
				this.shader.setVector2("inShadowMapSize",this._shadowMap ? this._shadowMap.size : new bg.Vector2(32,32));
				this.shader.setValueFloat("inShadowStrength",this.lightArray.shadowLight.shadowStrength);
				this.shader.setVector4("inShadowColor",shadowColor);
				this.shader.setValueFloat("inShadowBias",this.lightArray.shadowLight.shadowBias);
				this.shader.setValueInt("inCastShadows",this.lightArray.shadowLight.castShadows);
			
				this.shader.setVector4Ptr('inLightAmbient',this.lightArray.ambient);
				this.shader.setVector4Ptr('inLightDiffuse',this.lightArray.diffuse);
				this.shader.setVector4Ptr('inLightSpecular',this.lightArray.specular);
				this.shader.setValueIntPtr('inLightType',this.lightArray.type);
				this.shader.setVector3Ptr('inLightAttenuation',this.lightArray.attenuation);
				this.shader.setValueFloatPtr('inLightCutoffDistance',this.lightArray.cutoffDistance);

				// TODO: promote this value to a variable
				let lightEmissionFactor = 10;
				this.shader.setValueFloat('inLightEmissionFactor',lightEmissionFactor);

				this.shader.setTexture('inCubeMap',bg.scene.Cubemap.Current(this.context),bg.base.TextureUnit.TEXTURE_6);
				
				this.shader.setVector3Ptr('inLightDirection',this.lightArray.direction);
				this.shader.setVector3Ptr('inLightPosition',this.lightArray.position);
				this.shader.setValueFloatPtr('inSpotCutoff',this.lightArray.spotCutoff);
				this.shader.setValueFloatPtr('inSpotExponent',this.lightArray.spotExponent);

				this.shader.setValueInt('inNumLights',this.lightArray.numLights);
			}
			else {
				let BLACK = bg.Color.Black();
				this.shader.setVector4Ptr('inLightAmbient',BLACK.toArray());
				this.shader.setVector4Ptr('inLightDiffuse',BLACK.toArray());
				this.shader.setVector4Ptr('inLightSpecular',BLACK.toArray());
				this.shader.setVector3Ptr('inLightDirection',(new bg.Vector3(0,0,0)).toArray());
				this.shader.setValueInt('inNumLights',0);
			}
			
			this.colorCorrection.apply(this.shader);
		}
		
		setupVars() {
			if (this.material) {
				// Matrix state
				let matrixState = bg.base.MatrixState.Current();
				let viewMatrix = new bg.Matrix4(matrixState.viewMatrixStack.matrixConst);
				this.shader.setMatrix4('inModelMatrix',matrixState.modelMatrixStack.matrixConst);
				this.shader.setMatrix4('inViewMatrix',viewMatrix);
				this.shader.setMatrix4('inProjectionMatrix',matrixState.projectionMatrixStack.matrixConst);
				this.shader.setMatrix4('inNormalMatrix',matrixState.normalMatrix);
				this.shader.setMatrix4('inViewMatrixInv',matrixState.viewMatrixInvert);
				
				// Material				
				//	Prepare textures
				let whiteTex = bg.base.TextureCache.WhiteTexture(this.context);
				let blackTex = bg.base.TextureCache.BlackTexture(this.context);
				let normalTex = bg.base.TextureCache.NormalTexture(this.context);
				let texture = this.material.texture || whiteTex;
				let lightMap = this.material.lightmap || whiteTex;
				let normalMap = this.material.normalMap || normalTex;
				let shininessMask = this.material.shininessMask || whiteTex;
				let lightEmissionMask = this.material.lightEmissionMask || whiteTex;

				this.shader.setVector4('inDiffuseColor',this.material.diffuse);
				this.shader.setVector4('inSpecularColor',this.material.specular);
				this.shader.setValueFloat('inShininess',this.material.shininess);
				this.shader.setTexture('inShininessMask',shininessMask,bg.base.TextureUnit.TEXTURE_3);
				this.shader.setVector4('inShininessMaskChannel',this.material.shininessMaskChannelVector);
				this.shader.setValueInt('inShininessMaskInvert',this.material.shininessMaskInvert);
				this.shader.setValueFloat('inLightEmission',this.material.lightEmission);
				this.shader.setTexture('inLightEmissionMask',lightEmissionMask,bg.base.TextureUnit.TEXTURE_4);
				this.shader.setVector4('inLightEmissionMaskChannel',this.material.lightEmissionMaskChannelVector);
				this.shader.setValueInt('inLightEmissionMaskInvert',this.material.lightEmissionMaskInvert);
				this.shader.setValueFloat('inAlphaCutoff',this.material.alphaCutoff);
				
				this.shader.setTexture('inTexture',texture,bg.base.TextureUnit.TEXTURE_0);
				this.shader.setVector2('inTextureOffset',this.material.textureOffset);
				this.shader.setVector2('inTextureScale',this.material.textureScale);
				
				this.shader.setTexture('inLightMap',lightMap,bg.base.TextureUnit.TEXTURE_1);
				this.shader.setVector2('inLightMapOffset',this.material.lightmapOffset);
				this.shader.setVector2('inLightMapScale',this.material.lightmapScale);
				
				this.shader.setTexture('inNormalMap',normalMap,bg.base.TextureUnit.TEXTURE_2);
				this.shader.setVector2('inNormalMapScale',this.material.normalMapScale);
				this.shader.setVector2('inNormalMapOffset',this.material.normalMapOffset);
				
				this.shader.setValueInt('inReceiveShadows',this.material.receiveShadows);

				let reflectionMask = this.material.reflectionMask || whiteTex;
				this.shader.setValueFloat('inReflection',this.material.reflectionAmount);
				this.shader.setTexture('inReflectionMask',reflectionMask,bg.base.TextureUnit.TEXTURE_7);
				this.shader.setVector4('inReflectionMaskChannel',this.material.reflectionMaskChannelVector);
				this.shader.setValueInt('inReflectionMaskInvert',this.material.reflectionMaskInvert);

				let roughnessMask = this.material.roughnessMask || whiteTex;
				this.shader.setValueFloat('inRoughness',this.material.roughness);
				if (this.context.getParameter(this.context.MAX_TEXTURE_IMAGE_UNITS)<9) {
					this.shader.setTexture('inRoughnessMask',roughnessMask,bg.base.TextureUnit.TEXTURE_7);
				}
				else {
					this.shader.setTexture('inRoughnessMask',roughnessMask,bg.base.TextureUnit.TEXTURE_8);
				}
				this.shader.setVector4('inRoughnessMaskChannel',this.material.roughnessMaskChannelVector);
				this.shader.setValueInt('inRoughnessMaskInvert',this.material.roughnessMaskInvert);

				// Other settings
				this.shader.setValueInt('inSelectMode',false);

				this.shader.setValueInt('inUnlit',this.material.unlit);
			}
		}
		
	}
	
	bg.base.ForwardEffect = ForwardEffect;

	// Define the maximum number of lights that can be used in forward render
	bg.base.MAX_FORWARD_LIGHTS = 4;
	
})();

(function() {
	
	bg.base.LightType = {
		DIRECTIONAL: 4,
		SPOT: 1,
		POINT: 5,
		DISABLED: 10
	};

	bg.base.SpecularType = {
		PHONG: 0,
		BLINN: 1
	};

	class Light extends bg.app.ContextObject {
		constructor(context) {
			super(context);
			
			this._enabled = true;
			
			this._type = bg.base.LightType.DIRECTIONAL;
			
			this._direction = new bg.Vector3(0,0,-1);
			
			this._ambient = new bg.Color(0.2,0.2,0.2,1);
			this._diffuse = new bg.Color(0.9,0.9,0.9,1);
			this._specular = bg.Color.White();
			this._attenuation = new bg.Vector3(1,0.5,0.1);
			this._spotCutoff = 20;
			this._spotExponent = 30;
			this._shadowStrength = 0.7;
			this._cutoffDistance = -1;
			this._castShadows = true;
			this._shadowBias = 0.00002;
			this._specularType = bg.base.SpecularType.PHONG;
			this._intensity = 1;
			
			this._projection = bg.Matrix4.Ortho(-10,10,-10,10,0.5,300.0);
		}
		
		// If context is null, it will be used the same context as this light
		clone(context) {
			let newLight = new bg.base.Light(context || this.context);
			newLight.assign(this);
			return newLight;
		}
		
		assign(other) {
			this.enabled = other.enabled;
			this.type = other.type;
			this.direction.assign(other.direction);
			this.ambient.assign(other.ambient);
			this.diffuse.assign(other.diffuse);
			this.specular.assign(other.specular);
			this._attenuation.assign(other._attenuation);
			this.spotCutoff = other.spotCutoff;
			this.spotExponent = other.spotExponent;
			this.shadowStrength = other.shadowStrength;
			this.cutoffDistance = other.cutoffDistance;
			this.castShadows = other.castShadows;
			this.shadowBias = other.shadowBias;
			this.specularType = other.specularType;
			this.intensity = other.intensity;
		}
		
		get enabled() { return this._enabled; }
		set enabled(v) { this._enabled = v; }
		
		get type() { return this._type; }
		set type(t) { this._type = t; }
		
		get direction() { return this._direction; }
		set direction(d) { this._direction = d; }
		
		get ambient() { return this._ambient; }
		set ambient(a) { this._ambient = a; }
		get diffuse() { return this._diffuse; }
		set diffuse(d) { this._diffuse = d; }
		get specular() { return this._specular; }
		set specular(s) { this._specular = s; }
		get specularType() { return this._specularType; }
		set specularType(s) { this._specularType = s; }
		get intensity() { return this._intensity; }
		set intensity(i) { this._intensity = i; }
		
		// Attenuation is deprecated in PBR lighting model
		get attenuationVector() { return this._attenuation; }
		get constantAttenuation() { return this._attenuation.x; }
		get linearAttenuation() { return this._attenuation.y; }
		get quadraticAttenuation() { return this._attenuation.z; }
		set attenuationVector(a) { this._attenuation = a; }
		set constantAttenuation(a) { this._attenuation.x = a; }
		set linearAttenuation(a) { this._attenuation.y = a; }
		set quadraticAttenuation(a) { this._attenuation.z = a; }
		
		get cutoffDistance() { return this._cutoffDistance; }
		set cutoffDistance(c) { this._cutoffDistance = c; }
		
		get spotCutoff() { return this._spotCutoff; }
		set spotCutoff(c) { this._spotCutoff = c; }
		get spotExponent() { return this._spotExponent; }
		set spotExponent(e) { this._spotExponent = e; }
		
		get shadowStrength() { return this._shadowStrength; }
		set shadowStrength(s) { this._shadowStrength = s; }
		get castShadows() { return this._castShadows; }
		set castShadows(s) { this._castShadows = s; }
		get shadowBias() { return this._shadowBias; }
		set shadowBias(s) { this._shadowBias = s; }
		
		get projection() { return this._projection; }
		set projection(p) { this._projection = p; }

		deserialize(sceneData) {
			switch (sceneData.lightType) {
			case 'kTypeDirectional':
				this._type = bg.base.LightType.DIRECTIONAL;
				// Use the predefined shadow bias for directional lights
				//this._shadowBias = sceneData.shadowBias;
				break;
			case 'kTypeSpot':
				this._type = bg.base.LightType.SPOT;
				this._shadowBias = sceneData.shadowBias;
				break;
			case 'kTypePoint':
				this._type = bg.base.LightType.POINT;
				break;
			}
			
			this._ambient = new bg.Color(sceneData.ambient);
			this._diffuse = new bg.Color(sceneData.diffuse);
			this._specular = new bg.Color(sceneData.specular);
			this._spotCutoff = sceneData.spotCutoff || 20;
			this._spotExponent = sceneData.spotExponent || 30;
			this._shadowStrength = sceneData.shadowStrength;
			this._cutoffDistance = sceneData.cutoffDistance;
			this._projection = new bg.Matrix4(sceneData.projection);
			this._castShadows = sceneData.castShadows;
			this._specularType = sceneData.specularType=="BLINN" ? bg.base.SpecularType.BLINN : bg.base.SpecularType.PHONG;

			this._intensity = sceneData.intensity || 1;

			// Deprecated: not used in PBR lighting model
			this._attenuation = new bg.Vector3(
				sceneData.constantAtt,
				sceneData.linearAtt,
				sceneData.expAtt
				);
		}

		serialize(sceneData) {
			let lightTypes = [];
			lightTypes[bg.base.LightType.DIRECTIONAL] = "kTypeDirectional";
			lightTypes[bg.base.LightType.SPOT] = "kTypeSpot";
			lightTypes[bg.base.LightType.POINT] = "kTypePoint";
			sceneData.lightType = lightTypes[this._type];
			sceneData.ambient = this._ambient.toArray();
			sceneData.diffuse = this._diffuse.toArray();
			sceneData.specular = this._specular.toArray();
			sceneData.intensity = 1;
			sceneData.spotCutoff = this._spotCutoff || 20;
			sceneData.spotExponent = this._spotExponent || 30;
			sceneData.shadowStrength = this._shadowStrength;
			sceneData.cutoffDistance = this._cutoffDistance;
			sceneData.projection = this._projection.toArray();
			sceneData.castShadows = this._castShadows;
			sceneData.shadowBias = this._shadowBias || 0.0029;
			sceneData.specularType = this.specularType==bg.base.SpecularType.BLINN ? "BLINN" : "PHONG";
			sceneData.intensity = this.intensity || 1;

			// Deprecated: not used in PBR lighting model
			sceneData.constantAtt = this._attenuation.x;
			sceneData.linearAtt = this._attenuation.y;
			sceneData.expAtt = this._attenuation.z;
		}
	}
	
	bg.base.Light = Light;

	// Store a light array, optimized to be used as shader input
	class LightArray {
		constructor() {
			this.reset();
		}
		
		get type() { return this._type; }
		get ambient() { return this._ambient; }
		get diffuse() { return this._diffuse; }
		get specular() { return this._specular; }
		get position() { return this._position; }
		get direction() { return this._direction; }
		get rawDirection() { return this._rawDirection; }
		get spotCutoff() { return this._spotCutoff; }
		get cosSpotCutoff() { return this._cosSpotCutoff; }
		get spotExponent() { return this._spotExponent; }
		get cosSpotExponent() { return this._cosSpotExponent; }
		get shadowStrength() { return this._shadowStrength; }
		get cutoffDistance() { return this._cutoffDistance; }
		get specularType() { return this._specularType; }
		get intensity() { return this._intensity; }
		get numLights() { return this._numLights; }
		
		// Deprecated: not used in PBR lighting model
		get attenuation() { return this._attenuation; }
		
		get lightTransform() { return this._lightTransform; }

		get shadowLight() { return this._shadowLight || {
			shadowStrength: 0,
			shadowColor: bg.Color.Black(),
			shadowBias: 0,
			castShadows: false,
			projection: bg.Matrix4.Identity()
		}}
		get shadowLightTransform() { return this._shadowLightTransform || bg.Matrix4.Identity(); }
		get shadowLightIndex() { return this._shadowLightIndex; }
		get shadowLightDirection() { return this._shadowLightDirection; }
		
		reset() {
			this._type = [];
			this._ambient = [];
			this._diffuse = [];
			this._specular = [];
			this._position = [];
			this._direction = [];
			this._rawDirection = [];
			this._attenuation = [];
			this._spotCutoff = [];
			this._cosSpotCutoff = [];
			this._spotExponent = [];
			this._cosSpotExponent = [];
			this._shadowStrength = [];
			this._cutoffDistance = [];
			this._numLights = 0;
			this._lightTransform = [];
			this._specularType = [];
			this._intensity = [];
			
			// Forward render only supports one shadow map, so will only store
			// one projection
			this._shadowLightTransform = null;
			this._shadowLightIndex = -1;
			this._shadowLight = null;
			this._shadowLightDirection = new bg.Vector3(0,0,1);
		}

		push(light,lightTransform) {
			if (this._numLights==bg.base.MAX_FORWARD_LIGHTS) {
				return false;
			}
			else {
				if (this._shadowLightIndex==-1 && light.type!=bg.base.LightType.POINT && light.castShadows) {
					this._shadowLightTransform = lightTransform;
					this._shadowLight = light;
					this._shadowLightIndex = this._numLights;
				}
				this._type.push(light.type);
				this._ambient.push(...(light.ambient.toArray()));
				this._diffuse.push(...(light.diffuse.toArray()));
				this._specular.push(...(light.specular.toArray()));
				this._rawDirection.push(light.direction);
				this._attenuation.push(light.constantAttenuation);
				this._attenuation.push(light.linearAttenuation);
				this._attenuation.push(light.quadraticAttenuation);
				this._spotCutoff.push(light.spotCutoff);
				this._cosSpotCutoff.push(Math.cos(light.spotCutoff * Math.PI / 180));
				this._spotExponent.push(light.spotExponent);
				let exp = light.spotCutoff>light.spotExponent ? light.spotExponent : light.spotCutoff * 0.98;
				this._cosSpotExponent.push(Math.cos(exp * Math.PI / 180));
				this._shadowStrength.push(light.shadowStrength);
				this._cutoffDistance.push(light.cutoffDistance);
				this._specularType.push(light.specularType);
				this._intensity.push(light.intensity);

				this._numLights++;
				this._lightTransform.push(lightTransform);
				return true;
			}
		}

		updatePositionAndDirection(viewMatrix) {
			this._direction = [];
			this._position = [];
			for (let i=0; i<this._numLights; ++i) {
				let vm = new bg.Matrix4(viewMatrix);
				let dir = vm.mult(this._lightTransform[i])
							.rotation
							.multVector(this._rawDirection[i])
							.xyz;
				vm = new bg.Matrix4(viewMatrix);
				let pos = vm.mult(this._lightTransform[i]).position;
				this._direction.push(...(dir.toArray()));
				this._position.push(...(pos.toArray()));
				if (this._shadowLightIndex==i) {
					this._shadowLightDirection = dir;
				}
			}
		}
	}

	bg.base.LightArray = LightArray;
	
})();
(function() {
	bg.base.MaterialFlag = {
		DIFFUSE							: 1 << 0,
		SPECULAR						: 1 << 1,
		SHININESS						: 1 << 2,
		LIGHT_EMISSION					: 1 << 3,
		REFRACTION_AMOUNT				: 1 << 4,
		REFLECTION_AMOUNT				: 1 << 5,
		TEXTURE							: 1 << 6,
		LIGHT_MAP						: 1 << 7,
		NORMAL_MAP						: 1 << 8,
		TEXTURE_OFFSET					: 1 << 9,
		TEXTURE_SCALE					: 1 << 10,
		LIGHT_MAP_OFFSET				: 1 << 11,
		LIGHT_MAP_SCALE					: 1 << 12,
		NORMAL_MAP_OFFSET				: 1 << 13,
		NORMAL_MAP_SCALE				: 1 << 14,
		CAST_SHADOWS					: 1 << 15,
		RECEIVE_SHADOWS					: 1 << 16,
		ALPHA_CUTOFF					: 1 << 17,
		SHININESS_MASK					: 1 << 18,
		SHININESS_MASK_CHANNEL			: 1 << 19,
		SHININESS_MASK_INVERT			: 1 << 20,
		LIGHT_EMISSION_MASK				: 1 << 21,
		LIGHT_EMISSION_MASK_CHANNEL		: 1 << 22,
		LIGHT_EMISSION_MASK_INVERT		: 1 << 23,
		REFLECTION_MASK					: 1 << 24,
		REFLECTION_MASK_CHANNEL			: 1 << 25,
		REFLECTION_MASK_INVERT			: 1 << 26,
		CULL_FACE						: 1 << 27,
		ROUGHNESS						: 1 << 28,	// All the roughness attributes are controlled by this flag
		UNLIT							: 1 << 29
	};

	function loadTexture(context,image,url) {
		return bg.base.Texture.FromImage(context,image,url);
	}
	
	class MaterialModifier {
		constructor(jsonData) {
			this._modifierFlags = 0;

			this._diffuse = bg.Color.White();
			this._specular = bg.Color.White();
			this._shininess = 0;
			this._lightEmission = 0;
			this._refractionAmount = 0;
			this._reflectionAmount = 0;
			this._texture = null;
			this._lightmap = null;
			this._normalMap = null;
			this._textureOffset = new bg.Vector2();
			this._textureScale = new bg.Vector2(1);
			this._lightmapOffset = new bg.Vector2();
			this._lightmapScale = new bg.Vector2(1);
			this._normalMapOffset = new bg.Vector2();
			this._normalMapScale = new bg.Vector2(1);
			this._castShadows = true;
			this._receiveShadows = true;
			this._alphaCutoff = 0.5;
			this._shininessMask = null;
			this._shininessMaskChannel = 0;
			this._shininessMaskInvert = false;
			this._lightEmissionMask = null;
			this._lightEmissionMaskChannel = 0;
			this._lightEmissionMaskInvert = false;
			this._reflectionMask = null;
			this._reflectionMaskChannel = 0;
			this._reflectionMaskInvert = false;
			this._cullFace = true;
			this._roughness = true;
			this._roughnessMask = null;
			this._roughnessMaskChannel = 0;
			this._roughnessMaskInvert = false;
			this._unlit = false;

			if (jsonData) {
				if (jsonData.diffuseR!==undefined && jsonData.diffuseG!==undefined && jsonData.diffuseB!==undefined) {
					this.diffuse = new bg.Color(jsonData.diffuseR,
									  			jsonData.diffuseG,
									  			jsonData.diffuseB,
									  			jsonData.diffuseA ? jsonData.diffuseA : 1.0);
				}
				if (jsonData.specularR!==undefined && jsonData.specularG!==undefined && jsonData.specularB!==undefined) {
					this.specular = new bg.Color(jsonData.specularR,
									  			 jsonData.specularG,
									  			 jsonData.specularB,
									  			 jsonData.specularA ? jsonData.specularA : 1.0);
				}

				if (jsonData.shininess!==undefined) {
					this.shininess = jsonData.shininess;
				}

				if (jsonData.lightEmission!==undefined) {
					this.lightEmission = jsonData.lightEmission;
				}

				if (jsonData.refractionAmount!==undefined) {
					this.refractionAmount = jsonData.refractionAmount;
				}

				if (jsonData.reflectionAmount!==undefined) {
					this.reflectionAmount = jsonData.reflectionAmount;
				}

				if (jsonData.texture!==undefined) {
					this.texture = jsonData.texture;
				}

				if (jsonData.lightmap!==undefined) {
					this.lightmap = jsonData.lightmap;
				}


				if (jsonData.normalMap!==undefined) {
					this.normalMap = jsonData.normalMap;
				}
			
				if (jsonData.textureOffsetX!==undefined && jsonData.textureOffsetY!==undefined) {
					this.textureOffset = new bg.Vector2(jsonData.textureOffsetX,
														jsonData.textureOffsetY);
				}

				if (jsonData.textureScaleX!==undefined && jsonData.textureScaleY!==undefined) {
					this.textureScale = new bg.Vector2(jsonData.textureScaleX,
														jsonData.textureScaleY);
				}

				if (jsonData.lightmapOffsetX!==undefined && jsonData.lightmapOffsetY!==undefined) {
					this.lightmapOffset = new bg.Vector2(jsonData.lightmapOffsetX,
														 jsonData.lightmapOffsetY);
				}

				if (jsonData.lightmapScaleX!==undefined && jsonData.lightmapScaleY!==undefined) {
					this.lightmapScale = new bg.Vector2(jsonData.lightmapScaleX,
														 jsonData.lightmapScaleY);
				}

				if (jsonData.normalMapScaleX!==undefined && jsonData.normalMapScaleY!==undefined) {
					this.normalMapScale = new bg.Vector2(jsonData.normalMapScaleX,
														 jsonData.normalMapScaleY);
				}

				if (jsonData.normalMapOffsetX!==undefined && jsonData.normalMapOffsetY!==undefined) {
					this.normalMapOffset = new bg.Vector2(jsonData.normalMapOffsetX,
														 jsonData.normalMapOffsetY);
				}

				if (jsonData.castShadows!==undefined) {
					this.castShadows = jsonData.castShadows;
				}
				if (jsonData.receiveShadows!==undefined) {
					this.receiveShadows = jsonData.receiveShadows;
				}
				
				if (jsonData.alphaCutoff!==undefined) {
					this.alphaCutoff = jsonData.alphaCutoff;
				}

				if (jsonData.shininessMask!==undefined) {
					this.shininessMask = jsonData.shininessMask;
				}
				if (jsonData.shininessMaskChannel!==undefined) {
					this.shininessMaskChannel = jsonData.shininessMaskChannel;
				}
				if (jsonData.invertShininessMask!==undefined) {
					this.shininessMaskInvert = jsonData.invertShininessMask;
				}

				if (jsonData.lightEmissionMask!==undefined) {
					this.lightEmissionMask = jsonData.lightEmissionMask;
				}
				if (jsonData.lightEmissionMaskChannel!==undefined) {
					this.lightEmissionMaskChannel = jsonData.lightEmissionMaskChannel;
				}
				if (jsonData.invertLightEmissionMask!==undefined) {
					this.lightEmissionMaskInvert = jsonData.invertLightEmissionMask;
				}
				
				if (jsonData.reflectionMask!==undefined) {
					this.reflectionMask = jsonData.reflectionMask;
				}
				if (jsonData.reflectionMaskChannel!==undefined) {
					this.reflectionMaskChannel = jsonData.reflectionMaskChannel;
				}
				if (jsonData.invertReflectionMask!==undefined) {
					this.reflectionMaskInvert = jsonData.reflectionMaskInvert;
				}

				if (jsonData.roughness!==undefined) {
					this.roughness = jsonData.roughness;
				}
				if (jsonData.roughnessMask!==undefined) {
					this.roughnessMask = jsonData.roughnessMask;
				}
				if (jsonData.roughnessMaskChannel!==undefined) {
					this.roughnessMaskChannel = jsonData.roughnessMaskChannel;
				}
				if (jsonData.invertRoughnessMask!==undefined) {
					this.roughnessMaskInvert = jsonData.roughnessMaskInvert;
				}

				if (jsonData.unlit!==undefined) {
					this.unlit = jsonData.unlit;
				}
			}
		}
		
		get modifierFlags() { return this._modifierFlags; }
		set modifierFlags(f) { this._modifierFlags = f; }
		setEnabled(flag) { this._modifierFlags = this._modifierFlags | flag; }
		isEnabled(flag) { return (this._modifierFlags & flag)!=0; }
		
		get diffuse() { return this._diffuse; } 
		get specular() { return this._specular; } 
		get shininess() { return this._shininess; } 
		get lightEmission() { return this._lightEmission; } 
		get refractionAmount() { return this._refractionAmount; } 
		get reflectionAmount() { return this._reflectionAmount; } 
		get texture() { return this._texture; } 
		get lightmap() { return this._lightmap; } 
		get normalMap() { return this._normalMap; } 
		get textureOffset() { return this._textureOffset; } 
		get textureScale() { return this._textureScale; } 
		get lightmapOffset() { return this._lightmapOffset; } 
		get lightmapScale() { return this._lightmapScale; } 
		get normalMapOffset() { return this._normalMapOffset; } 
		get normalMapScale() { return this._normalMapScale; } 
		get castShadows() { return this._castShadows; } 
		get receiveShadows() { return this._receiveShadows; } 
		get alphaCutoff() { return this._alphaCutoff; } 
		get shininessMask() { return this._shininessMask; } 
		get shininessMaskChannel() { return this._shininessMaskChannel; } 
		get shininessMaskInvert() { return this._shininessMaskInvert; } 
		get lightEmissionMask() { return this._lightEmissionMask; } 
		get lightEmissionMaskChannel() { return this._lightEmissionMaskChannel; } 
		get lightEmissionMaskInvert() { return this._lightEmissionMaskInvert; } 
		get reflectionMask() { return this._reflectionMask; } 
		get reflectionMaskChannel() { return this._reflectionMaskChannel; } 
		get reflectionMaskInvert() { return this._reflectionMaskInvert; } 
		get cullFace() { return this._cullFace; }
		get roughness() { return this._roughness; }
		get roughnessMask() { return this._roughnessMask; }
		get roughnessMaskChannel() { return this._roughnessMaskChannel; }
		get roughnessMaskInvert() { return this._roughnessMaskInvert; }
		get unlit() { return this._unlit; }

		set diffuse(newVal) { this._diffuse = newVal; this.setEnabled(bg.base.MaterialFlag.DIFFUSE); }
		set specular(newVal) { this._specular = newVal; this.setEnabled(bg.base.MaterialFlag.SPECULAR); }
		set shininess(newVal) { if (!isNaN(newVal)) { this._shininess = newVal; this.setEnabled(bg.base.MaterialFlag.SHININESS); } }
		set lightEmission(newVal) { if (!isNaN(newVal)) { this._lightEmission = newVal; this.setEnabled(bg.base.MaterialFlag.LIGHT_EMISSION); } }
		set refractionAmount(newVal) { if (!isNaN(newVal)) { this._refractionAmount = newVal; this.setEnabled(bg.base.MaterialFlag.REFRACTION_AMOUNT); } }
		set reflectionAmount(newVal) { if (!isNaN(newVal)) { this._reflectionAmount = newVal; this.setEnabled(bg.base.MaterialFlag.REFLECTION_AMOUNT); } }
		set texture(newVal) { this._texture = newVal; this.setEnabled(bg.base.MaterialFlag.TEXTURE); }
		set lightmap(newVal) { this._lightmap = newVal; this.setEnabled(bg.base.MaterialFlag.LIGHT_MAP); }
		set normalMap(newVal) { this._normalMap = newVal; this.setEnabled(bg.base.MaterialFlag.NORMAL_MAP); }
		set textureOffset(newVal) { this._textureOffset = newVal; this.setEnabled(bg.base.MaterialFlag.TEXTURE_OFFSET); }
		set textureScale(newVal) { this._textureScale = newVal; this.setEnabled(bg.base.MaterialFlag.TEXTURE_SCALE); }
		set lightmapOffset(newVal) { this._lightmapOffset = newVal; this.setEnabled(bg.base.MaterialFlag.LIGHT_MAP_OFFSET); }
		set lightmapScale(newVal) { this._lightmapScale = newVal; this.setEnabled(bg.base.MaterialFlag.LIGHT_MAP_SCALE); }
		set normalMapOffset(newVal) { this._normalMapOffset = newVal; this.setEnabled(bg.base.MaterialFlag.NORMAL_MAP_OFFSET); }
		set normalMapScale(newVal) { this._normalMapScale = newVal; this.setEnabled(bg.base.MaterialFlag.NORMAL_MAP_SCALE); }
		set castShadows(newVal) { this._castShadows = newVal; this.setEnabled(bg.base.MaterialFlag.CAST_SHADOWS); }
		set receiveShadows(newVal) { this._receiveShadows = newVal; this.setEnabled(bg.base.MaterialFlag.RECEIVE_SHADOWS); }
		set alphaCutoff(newVal) { if (!isNaN(newVal)) { this._alphaCutoff = newVal; this.setEnabled(bg.base.MaterialFlag.ALPHA_CUTOFF); } }
		set shininessMask(newVal) { this._shininessMask = newVal; this.setEnabled(bg.base.MaterialFlag.SHININESS_MASK); }
		set shininessMaskChannel(newVal) { this._shininessMaskChannel = newVal; this.setEnabled(bg.base.MaterialFlag.SHININESS_MASK_CHANNEL); }
		set shininessMaskInvert(newVal) { this._shininessMaskInvert = newVal; this.setEnabled(bg.base.MaterialFlag.SHININESS_MASK_INVERT); }
		set lightEmissionMask(newVal) { this._lightEmissionMask = newVal; this.setEnabled(bg.base.MaterialFlag.LIGHT_EMISSION_MASK); }
		set lightEmissionMaskChannel(newVal) { this._lightEmissionMaskChannel = newVal; this.setEnabled(bg.base.MaterialFlag.LIGHT_EMISSION_MASK_CHANNEL); }
		set lightEmissionMaskInvert(newVal) { this._lightEmissionMaskInvert = newVal; this.setEnabled(bg.base.MaterialFlag.LIGHT_EMISSION_MASK_INVERT); }
		set reflectionMask(newVal) { this._reflectionMask = newVal; this.setEnabled(bg.base.MaterialFlag.REFLECTION_MASK); }
		set reflectionMaskChannel(newVal) { this._reflectionMaskChannel = newVal; this.setEnabled(bg.base.MaterialFlag.REFLECTION_MASK_CHANNEL); }
		set reflectionMaskInvert(newVal) { this._reflectionMaskInvert = newVal; this.setEnabled(bg.base.MaterialFlag.REFLECTION_MASK_INVERT); }
		set cullFace(newVal) { this._cullFace = newVal; this.setEnabled(bg.base.MaterialFlag.CULL_FACE); }
		set roughness(newVal) { this._roughness = newVal; this.setEnabled(bg.base.MaterialFlag.ROUGHNESS); }
		set roughnessMask(newVal) { this._roughnessMask = newVal; this.setEnabled(bg.base.MaterialFlag.ROUGHNESS); }
		set roughnessMaskChannel(newVal) { this._roughnessMaskChannel = newVal; this.setEnabled(bg.base.MaterialFlag.ROUGHNESS); }
		set roughnessMaskInvert(newVal) { this._roughnessMaskInvert = newVal; this.setEnabled(bg.base.MaterialFlag.ROUGHNESS); }
		set unlit(newVal) { this._unlit = newVal; this.setEnabled(bg.base.MaterialFlag.UNLIT); }

		clone() {
			let copy = new MaterialModifier();
			copy.assign(this);
			return copy;
		}
		
		assign(mod) {
			this._modifierFlags = mod._modifierFlags;

			this._diffuse = mod._diffuse;
			this._specular = mod._specular;
			this._shininess = mod._shininess;
			this._lightEmission = mod._lightEmission;
			this._refractionAmount = mod._refractionAmount;
			this._reflectionAmount = mod._reflectionAmount;
			this._texture = mod._texture;
			this._lightmap = mod._lightmap;
			this._normalMap = mod._normalMap;
			this._textureOffset = mod._textureOffset;
			this._textureScale = mod._textureScale;
			this._lightmapOffset = mod._lightmapOffset;
			this._lightmapScale = mod._lightmapScale;
			this._normalMapOffset = mod._normalMapOffset;
			this._normalMapScale = mod._normalMapScale;
			this._castShadows = mod._castShadows;
			this._receiveShadows = mod._receiveShadows;
			this._alphaCutoff = mod._alphaCutoff;
			this._shininessMask = mod._shininessMask;
			this._shininessMaskChannel = mod._shininessMaskChannel;
			this._shininessMaskInvert = mod._shininessMaskInvert;
			this._lightEmissionMask = mod._lightEmissionMask;
			this._lightEmissionMaskChannel = mod._lightEmissionMaskChannel;
			this._lightEmissionMaskInvert = mod._lightEmissionMaskInvert;
			this._reflectionMask = mod._reflectionMask;
			this._reflectionMaskChannel = mod._reflectionMaskChannel;
			this._reflectionMaskInvert = mod._reflectionMaskInvert;
			this._cullFace = mod._cullFace;
			this._roughness = mod._roughness;
			this._roughnessMask = mod._roughnessMask;
			this._roughnessMaskChannel = mod._roughnessMaskChannel;
			this._roughnessMaskInvert = mod._roughnessMaskInvert;
			this._unlit = mod._unlit;
		}

		serialize() {
			let result = {};
			let mask = this._modifierFlags;

			if ( mask & bg.base.MaterialFlag.DIFFUSE) {
				result.diffuseR = this._diffuse.r;
				result.diffuseG = this._diffuse.g;
				result.diffuseB = this._diffuse.b;
				result.diffuseA = this._diffuse.a;
			}
			if ( mask & bg.base.MaterialFlag.SPECULAR) {
				result.specularR = this._specular.r;
				result.specularG = this._specular.g;
				result.specularB = this._specular.b;
				result.specularA = this._specular.a;
			}
			if ( mask & bg.base.MaterialFlag.SHININESS) {
				result.shininess = this._shininess;
			}
			if ( mask & bg.base.MaterialFlag.SHININESS_MASK) {
				result.shininessMask = this._shininessMask;
			}
			if ( mask & bg.base.MaterialFlag.SHININESS_MASK_CHANNEL) {
				result.shininessMaskChannel = this._shininessMaskChannel;
			}
			if ( mask & bg.base.MaterialFlag.SHININESS_MASK_INVERT) {
				result.invertShininessMask = this._shininessMaskInvert;
			}
			if ( mask & bg.base.MaterialFlag.LIGHT_EMISSION) {
				result.lightEmission = this._lightEmission;
			}
			if ( mask & bg.base.MaterialFlag.LIGHT_EMISSION_MASK) {
				result.lightEmissionMask = this._lightEmissionMask;
			}
			if ( mask & bg.base.MaterialFlag.LIGHT_EMISSION_MASK_CHANNEL) {
				result.lightEmissionMaskChannel = this._lightEmissionMaskChannel;
			}
			if ( mask & bg.base.MaterialFlag.LIGHT_EMISSION_MASK_INVERT) {
				result.invertLightEmissionMask = this._lightEmissionMaskInvert;
			}
			if ( mask & bg.base.MaterialFlag.REFRACTION_AMOUNT) {
				result.reflectionAmount = this._refractionAmount;
			}
			if ( mask & bg.base.MaterialFlag.REFLECTION_AMOUNT) {
				result.refractionAmount = this._reflectionAmount;
			}
			if ( mask & bg.base.MaterialFlag.TEXTURE) {
				result.texture = this._texture;
			}
			if ( mask & bg.base.MaterialFlag.LIGHT_MAP) {
				result.lightmap = this._lightmap;
			}
			if ( mask & bg.base.MaterialFlag.NORMAL_MAP) {
				result.normalMap = this._normalMap;
			}
			if ( mask & bg.base.MaterialFlag.TEXTURE_OFFSET) {
				result.textureScaleX = this._textureScale.x;
				result.textureScaleY = this._textureScale.y;
			}
			if ( mask & bg.base.MaterialFlag.TEXTURE_SCALE) {
				result.textureScaleX = this._textureScale.x;
				result.textureScaleY = this._textureScale.y;
			}
			if ( mask & bg.base.MaterialFlag.LIGHT_MAP_OFFSET) {
				result.lightmapOffsetX = this._lightmapOffset.x;
				result.lightmapOffsetY = this._lightmapOffset.y;
			}
			if ( mask & bg.base.MaterialFlag.LIGHT_MAP_SCALE) {
				result.lightmapScaleX = this._lightmapScale.x;
				result.lightmapScaleY = this._lightmapScale.y;
			}
			if ( mask & bg.base.MaterialFlag.NORMAL_MAP_OFFSET) {
				result.normalMapOffsetX = this._normalMapOffset.x;
				result.normalMapOffsetY = this._normalMapOffset.y;
			}
			if ( mask & bg.base.MaterialFlag.NORMAL_MAP_SCALE) {
				result.normalMapScaleX = this._normalMapScale.x;
				result.normalMapScaleY = this._normalMapScale.y;
			}
			if ( mask & bg.base.MaterialFlag.CAST_SHADOWS) {
				result.castShadows = this._castShadows;
			}
			if ( mask & bg.base.MaterialFlag.RECEIVE_SHADOWS) {
				result.receiveShadows = this._receiveShadows;
			}
			if ( mask & bg.base.MaterialFlag.ALPHA_CUTOFF) {
				result.alphaCutoff = this._alphaCutoff;
			}
			if ( mask & bg.base.MaterialFlag.REFLECTION_MASK) {
				result.reflectionMask = this._reflectionMask;
			}
			if ( mask & bg.base.MaterialFlag.REFLECTION_MASK_CHANNEL) {
				result.reflectionMaskChannel = this._reflectionMaskChannel;
			}
			if ( mask & bg.base.MaterialFlag.REFLECTION_MASK_INVERT) {
				result.invertReflectionMask = this._reflectionMaskInvert;
			}
			if ( mask & bg.base.MaterialFlag.CULL_FACE) {
				result.cullFace = this._cullFace;
			}
			if ( mask & bg.base.MaterialFlag.ROUGHNESS) {
				result.roughness = this._roughness;
				result.roughnessMask = this._roughnessMask;
				result.roughnessMaskChannel = this._roughnessMaskChannel;
				result.invertRoughnessMask = this._roughnessMaskInvert;
			}
			if (mask & bg.base.MaterialFlag.UNLIT) {
				result.unlit = this._unlit;
			}
			return result;
		}
	}
	
	bg.base.MaterialModifier = MaterialModifier;

	bg.base.imageTools = {};

	function isAbsolutePath(path) {
		return /^(f|ht)tps?:\/\//i.test(path);
	}
	bg.base.imageTools.isAbsolutePath = isAbsolutePath;

	function mergePaths(path,component) {
		return path.slice(-1)!='/' ? path + '/' + component :  path + component;
	}
	bg.base.imageTools.mergePath = mergePaths;

	function getTexture(context,texturePath,resourcePath) {
		let texture = null;
		if (texturePath) {
			if (!isAbsolutePath(texturePath)) {
				if (resourcePath.slice(-1)!='/') {
					resourcePath += '/';
				}
				texturePath = `${resourcePath}${texturePath}`;
			}

			texture = bg.base.TextureCache.Get(context).find(texturePath);
			if (!texture) {
				texture = new bg.base.Texture(context);
				texture.create();
				texture.fileName = texturePath;
				bg.base.TextureCache.Get(context).register(texturePath,texture);

				(function(path,tex) {
					bg.utils.Resource.Load(path)
						.then(function(imgData) {
							tex.bind();
							texture.minFilter = bg.base.TextureLoaderPlugin.GetMinFilter();
							texture.magFilter = bg.base.TextureLoaderPlugin.GetMagFilter();
							tex.fileName = path;
							tex.setImage(imgData);
						});
				})(texturePath,texture);
			}
		}
		return texture;
	}
	bg.base.imageTools.getTexture = getTexture;
	
	function getPath(texture) {
		return texture ? texture.fileName:"";
	}
	bg.base.imageTools.getPath = getPath;

	function channelVector(channel) {
		return new bg.Vector4(
			channel==0 ? 1:0,
			channel==1 ? 1:0,
			channel==2 ? 1:0,
			channel==3 ? 1:0
		);
	}
	
	function readVector(data) {
		if (!data) return null;
		switch (data.length) {
		case 2:
			return new bg.Vector2(data[0],data[1]);
		case 3:
			return new bg.Vector3(data[0],data[1],data[2]);
		case 4:
			return new bg.Vector4(data[0],data[1],data[2],data[3]);
		}
		return null;
	}

	let g_base64Images = {};

	function readTexture(context,basePath,texData,mat,property) {
		return new Promise((resolve) => {
			if (!texData) {
				resolve();
			}
			else if (/data\:image\/[a-z]+\;base64\,/.test(texData)) {
				let hash = bg.utils.md5(texData);
				if (g_base64Images[hash]) {
					mat[property] = g_base64Images[hash];
				}
				else {
					mat[property] = bg.base.Texture.FromBase64Image(context,texData);
					g_base64Images[hash] = mat[property];
				}
				resolve(mat[property]);
			}
//			else if (/data\:md5/.test(texData)) {

//			}
			else {
				let fullPath = basePath + texData;	// TODO: add full path
				bg.base.Loader.Load(context,fullPath)
					.then(function(tex) {
						mat[property] = tex;
						resolve(tex);
					});
			}
		});
	}
	bg.base.imageTools.readTexture = readTexture;

	class Material {
		// Create and initialize a material from the json material definition
		static FromMaterialDefinition(context,def,basePath="") {
			return new Promise((resolve,reject) => {
				let mat = new Material();

				mat.diffuse = readVector(def.diffuse) || bg.Color.White();
				mat.specular = readVector(def.specular) || bg.Color.White();
				mat.shininess = def.shininess || 0;
				mat.shininessMaskChannel = def.shininessMaskChannel || 0;
				mat.shininessMaskInvert = def.shininessMaskInvert || false;
				mat.lightEmission = def.lightEmission || 0;
				mat.lightEmissionMaskChannel = def.lightEmissionMaskChannel || 0;
				mat.lightEmissionMaskInvert = def.lightEmissionMaskInvert || false;
				mat.refractionAmount = def.refractionAmount || 0;
				mat.reflectionAmount = def.reflectionAmount || 0;
				mat.reflectionMaskChannel = def.reflectionMaskChannel || 0;
				mat.reflectionMaskInvert = def.reflectionMaskInvert || false;
				mat.textureOffset = readVector(def.textureOffset) || new bg.Vector2(0,0);
				mat.textureScale = readVector(def.textureScale) || new bg.Vector2(1,1);
				mat.normalMapOffset = readVector(def.normalMapOffset) || new bg.Vector2(0,0);
				mat.normalMapScale = readVector(def.normalMapScale) || new bg.Vector2(1,1);
				mat.cullFace = def.cullFace===undefined ? true : def.cullFace;
				mat.castShadows = def.castShadows===undefined ? true : def.castShadows;
				mat.receiveShadows = def.receiveShadows===undefined ? true : def.receiveShadows;
				mat.alphaCutoff = def.alphaCutoff===undefined ? 0.5 : def.alphaCutoff;
				mat.name = def.name;
				mat.description = def.description;
				mat.roughness = def.roughness || 0;
				mat.roughnessMaskChannel = def.roughnessMaskChannel || 0;
				mat.roughnessMaskInvert = def.roughnessMaskInvert || false;
				mat.unlit = def.unlit || false;

				let texPromises = [];
				texPromises.push(readTexture(context,basePath,def.shininessMask,mat,"shininessMask"));
				texPromises.push(readTexture(context,basePath,def.lightEmissionMask,mat,"lightEmissionMask"));
				texPromises.push(readTexture(context,basePath,def.reflectionMask,mat,"reflectionMask"));
				texPromises.push(readTexture(context,basePath,def.texture,mat,"texture"));
				texPromises.push(readTexture(context,basePath,def.normalMap,mat,"normalMap"));
				texPromises.push(readTexture(context,basePath,def.roughnessMask,mat,"roughnessMask"));

				Promise.all(texPromises)
					.then(() => {
						resolve(mat);
					});
			});
		}

		constructor() {
			this._diffuse = bg.Color.White();
			this._specular = bg.Color.White();
			this._shininess = 0;
			this._lightEmission = 0;
			this._refractionAmount = 0;
			this._reflectionAmount = 0;
			this._texture = null;
			this._lightmap = null;
			this._normalMap = null;
			this._textureOffset = new bg.Vector2();
			this._textureScale = new bg.Vector2(1);
			this._lightmapOffset = new bg.Vector2();
			this._lightmapScale = new bg.Vector2(1);
			this._normalMapOffset = new bg.Vector2();
			this._normalMapScale = new bg.Vector2(1);
			this._castShadows = true;
			this._receiveShadows = true;
			this._alphaCutoff = 0.5;
			this._shininessMask = null;
			this._shininessMaskChannel = 0;
			this._shininessMaskInvert = false;
			this._lightEmissionMask = null;
			this._lightEmissionMaskChannel = 0;
			this._lightEmissionMaskInvert = false;
			this._reflectionMask = null;
			this._reflectionMaskChannel = 0;
			this._reflectionMaskInvert = false;
			this._cullFace = true;
			this._roughness = 0;
			this._roughnessMask = null;
			this._roughnessMaskChannel = 0;
			this._roughnessMaskInvert = false;
			this._unlit = false;
			
			this._selectMode = false;
		}
		
		clone() {
			let copy = new Material();
			copy.assign(this);
			return copy;
		}
		
		assign(other) {
			this._diffuse = new bg.Color(other.diffuse);
			this._specular = new bg.Color(other.specular);
			this._shininess = other.shininess;
			this._lightEmission = other.lightEmission;
			this._refractionAmount = other.refractionAmount;
			this._reflectionAmount = other.reflectionAmount;
			this._texture = other.texture;
			this._lightmap = other.lightmap;
			this._normalMap = other.normalMap;
			this._textureOffset = new bg.Vector2(other.textureOffset);
			this._textureScale = new bg.Vector2(other.textureScale);
			this._lightmapOffset = new bg.Vector2(other.ligthmapOffset);
			this._lightmapScale = new bg.Vector2(other.lightmapScale);
			this._normalMapOffset = new bg.Vector2(other.normalMapOffset);
			this._normalMapScale = new bg.Vector2(other.normalMapScale);
			this._castShadows = other.castShadows;
			this._receiveShadows = other.receiveShadows;
			this._alphaCutoff = other.alphaCutoff;
			this._shininessMask = other.shininessMask;
			this._shininessMaskChannel = other.shininessMaskChannel;
			this._shininessMaskInvert = other.shininessMaskInvert;
			this._lightEmissionMask = other.lightEmissionMask;
			this._lightEmissionMaskChannel = other.lightEmissionMaskChannel;
			this._lightEmissionMaskInvert = other.lightEmissionMaskInvert;
			this._reflectionMask = other.reflectionMask;
			this._reflectionMaskChannel = other.reflectionMaskChannel;
			this._reflectionMaskInvert = other.reflectionMaskInvert;
			this._cullFace = other.cullFace;
			this._roughness = other.roughness;
			this._roughnessMask = other.roughnessMask;
			this._roughnessMaskChannel = other.roughnessMaskChannel;
			this._roughnessMaskInvert = other.roughnessMaskInvert;
			this._unlit = other.unlit;
		}
		
		get isTransparent() {
			return this._diffuse.a<1;
		}
		
		get diffuse() { return this._diffuse; }
		get specular() { return this._specular; }
		get shininess() { return this._shininess; }
		get lightEmission() { return this._lightEmission; }
		get refractionAmount() { return this._refractionAmount; }
		get reflectionAmount() { return this._reflectionAmount; }
		get texture() { return this._texture; }
		get lightmap() { return this._lightmap; }
		get normalMap() { return this._normalMap; }
		get textureOffset() { return this._textureOffset; }
		get textureScale() { return this._textureScale; }
		get lightmapOffset() { return this._lightmapOffset; }
		get lightmapScale() { return this._lightmapScale; }
		get normalMapOffset() { return this._normalMapOffset; }
		get normalMapScale() { return this._normalMapScale; }
		get castShadows() { return this._castShadows; }
		get receiveShadows() { return this._receiveShadows; }
		get alphaCutoff() { return this._alphaCutoff; }
		get shininessMask() { return this._shininessMask; }
		get shininessMaskChannel() { return this._shininessMaskChannel; }
		get shininessMaskInvert() { return this._shininessMaskInvert; }
		get lightEmissionMask() { return this._lightEmissionMask; }
		get lightEmissionMaskChannel() { return this._lightEmissionMaskChannel; }
		get lightEmissionMaskInvert() { return this._lightEmissionMaskInvert; }
		get reflectionMask() { return this._reflectionMask; }
		get reflectionMaskChannel() { return this._reflectionMaskChannel; }
		get reflectionMaskInvert() { return this._reflectionMaskInvert; }
		get cullFace() { return this._cullFace; }
		get roughness() { return this._roughness; }
		get roughnessMask() { return this._roughnessMask; }
		get roughnessMaskChannel() { return this._roughnessMaskChannel; }
		get roughnessMaskInvert() { return this._roughnessMaskInvert; }
		get unlit() { return this._unlit; }

		
		set diffuse(newVal) { this._diffuse = newVal; }
		set specular(newVal) { this._specular = newVal; }
		set shininess(newVal) { if (!isNaN(newVal)) this._shininess = newVal; }
		set lightEmission(newVal) { if (!isNaN(newVal)) this._lightEmission = newVal; }
		set refractionAmount(newVal) { this._refractionAmount = newVal; }
		set reflectionAmount(newVal) { this._reflectionAmount = newVal; }
		set texture(newVal) { this._texture = newVal; }
		set lightmap(newVal) { this._lightmap = newVal; }
		set normalMap(newVal) { this._normalMap = newVal; }
		set textureOffset(newVal) { this._textureOffset = newVal; }
		set textureScale(newVal) { this._textureScale = newVal; }
		set lightmapOffset(newVal) { this._lightmapOffset = newVal; }
		set lightmapScale(newVal) { this._lightmapScale = newVal; }
		set normalMapOffset(newVal) { this._normalMapOffset = newVal; }
		set normalMapScale(newVal) { this._normalMapScale = newVal; }
		set castShadows(newVal) { this._castShadows = newVal; }
		set receiveShadows(newVal) { this._receiveShadows = newVal; }
		set alphaCutoff(newVal) { if (!isNaN(newVal)) this._alphaCutoff = newVal; }
		set shininessMask(newVal) { this._shininessMask = newVal; }
		set shininessMaskChannel(newVal) { this._shininessMaskChannel = newVal; }
		set shininessMaskInvert(newVal) { this._shininessMaskInvert = newVal; }
		set lightEmissionMask(newVal) { this._lightEmissionMask = newVal; }
		set lightEmissionMaskChannel(newVal) { this._lightEmissionMaskChannel = newVal; }
		set lightEmissionMaskInvert(newVal) { this._lightEmissionMaskInvert = newVal; }
		set reflectionMask(newVal) { this._reflectionMask = newVal; }
		set reflectionMaskChannel(newVal) { this._reflectionMaskChannel = newVal; }
		set reflectionMaskInvert(newVal) { this._reflectionMaskInvert = newVal; }
		set cullFace(newVal) { this._cullFace = newVal; }
		set roughness(newVal) { this._roughness = newVal; }
		set roughnessMask(newVal) { this._roughnessMask = newVal; }
		set roughnessMaskChannel(newVal) { this._roughnessMaskChannel = newVal; }
		set roughnessMaskInvert(newVal) { this._roughnessMaskInvert = newVal; }
		
		get unlit() { return this._unlit; }
		set unlit(u) { this._unlit = u; }

		get selectMode() { return this._selectMode; }
		set selectMode(s) { this._selectMode = s; }

		// Mask channel vectors: used to pass the mask channel to a shader
		get lightEmissionMaskChannelVector() {
			return channelVector(this.lightEmissionMaskChannel)
		}
		
		get shininessMaskChannelVector() {
			return channelVector(this.shininessMaskChannel);
		}
		
		get reflectionMaskChannelVector() {
			return channelVector(this.reflectionMaskChannel);
		}

		get roughnessMaskChannelVector() {
			return channelVector(this.roughnessMaskChannel);
		}
		
		// Returns an array of the external resources used by this material, for example,
		// the paths to the textures. If the "resources" parameter (array) is passed, the resources
		// will be added to this array, and the parameter will be modified to include the new
		// resources. If a resource exists in the "resources" parameter, it will not be added
		getExternalResources(resources=[]) {
			function tryadd(texture) {
				if (texture && texture.fileName && texture.fileName!="" && resources.indexOf(texture.fileName)==-1) {
					resources.push(texture.fileName);
				}
			}
			tryadd(this.texture);
			tryadd(this.lightmap);
			tryadd(this.normalMap);
			tryadd(this.shininessMask);
			tryadd(this.lightEmissionMask);
			tryadd(this.reflectionMask);
			tryadd(this.roughnessMask);
			return resources;
		}
		
		copyMaterialSettings(mat,mask) {
			if ( mask & bg.base.MaterialFlag.DIFFUSE) {
				mat.diffuse = this.diffuse;
			}
			if ( mask & bg.base.MaterialFlag.SPECULAR) {
				mat.specular = this.specular;
			}
			if ( mask & bg.base.MaterialFlag.SHININESS) {
				mat.shininess = this.shininess;
			}
			if ( mask & bg.base.MaterialFlag.LIGHT_EMISSION) {
				mat.lightEmission = this.lightEmission;
			}
			if ( mask & bg.base.MaterialFlag.REFRACTION_AMOUNT) {
				mat.refractionAmount = this.refractionAmount;
			}
			if ( mask & bg.base.MaterialFlag.REFLECTION_AMOUNT) {
				mat.reflectionAmount = this.reflectionAmount;
			}
			if ( mask & bg.base.MaterialFlag.TEXTURE) {
				mat.texture = this.texture;
			}
			if ( mask & bg.base.MaterialFlag.LIGHT_MAP) {
				mat.lightmap = this.lightmap;
			}
			if ( mask & bg.base.MaterialFlag.NORMAL_MAP) {
				mat.normalMap = this.normalMap;
			}
			if ( mask & bg.base.MaterialFlag.TEXTURE_OFFSET) {
				mat.textureOffset = this.textureOffset;
			}
			if ( mask & bg.base.MaterialFlag.TEXTURE_SCALE) {
				mat.textureScale = this.textureScale;
			}
			if ( mask & bg.base.MaterialFlag.LIGHT_MAP_OFFSET) {
				mat.lightmapOffset = this.lightmapOffset;
			}
			if ( mask & bg.base.MaterialFlag.LIGHT_MAP_SCALE) {
				mat.lightmapScale = this.lightmapScale;
			}
			if ( mask & bg.base.MaterialFlag.NORMAL_MAP_OFFSET) {
				mat.normalMapOffset = this.normalMapOffset;
			}
			if ( mask & bg.base.MaterialFlag.NORMAL_MAP_SCALE) {
				mat.normalMapScale = this.normalMapScale;
			}
			if ( mask & bg.base.MaterialFlag.CAST_SHADOWS) {
				mat.castShadows = this.castShadows;
			}
			if ( mask & bg.base.MaterialFlag.RECEIVE_SHADOWS) {
				mat.receiveShadows = this.receiveShadows;
			}
			if ( mask & bg.base.MaterialFlag.ALPHA_CUTOFF) {
				mat.alphaCutoff = this.alphaCutoff;
			}
			if ( mask & bg.base.MaterialFlag.SHININESS_MASK) {
				mat.shininessMask = this.shininessMask;
			}
			if ( mask & bg.base.MaterialFlag.SHININESS_MASK_CHANNEL) {
				mat.shininessMaskChannel = this.shininessMaskChannel;
			}
			if ( mask & bg.base.MaterialFlag.SHININESS_MASK_INVERT) {
				mat.shininessMaskInvert = this.shininessMaskInvert;
			}
			if ( mask & bg.base.MaterialFlag.LIGHT_EMISSION_MASK) {
				mat.lightEmissionMask = this.lightEmissionMask;
			}
			if ( mask & bg.base.MaterialFlag.LIGHT_EMISSION_MASK_CHANNEL) {
				mat.lightEmissionMaskChannel = this.lightEmissionMaskChannel;
			}
			if ( mask & bg.base.MaterialFlag.LIGHT_EMISSION_MASK_INVERT) {
				mat.lightEmissionMaskInvert = this.lightEmissionMaskInvert;
			}
			if ( mask & bg.base.MaterialFlag.REFLECTION_MASK) {
				mat.reflectionMask = this.reflectionMask;
			}
			if ( mask & bg.base.MaterialFlag.REFLECTION_MASK_CHANNEL) {
				mat.reflectionMaskChannel = this.reflectionMaskChannel;
			}
			if ( mask & bg.base.MaterialFlag.REFLECTION_MASK_INVERT) {
				mat.reflectionMaskInvert = this.reflectionMaskInvert;
			}
			if ( mask & bg.base.MaterialFlag.CULL_FACE) {
				mat.cullFace = this.cullFace;
			}

			// All the roughness attributes are copied together using this flag. In
			// the future, the *_MASK, *_MASK_CHANNEL and *_MASK_INVERT for shininess,
			// light emission and reflection, will be deprecated and will work in the
			// same way as ROUGHNESS here
			if ( mask & bg.base.MaterialFlag.ROUGHNESS) {
				mat.reflectionAmount = this.reflectionAmount;
				mat.reflectionMask = this.reflectionMask;
				mat.reflectionMaskChannel = this.reflectionMaskChannel;
				mat.reflectionMaskInvert = this.reflectionMaskInvert;
			}

			if (mask & bg.base.MaterialFlag.UNLIT) {
				mat.unlit = this.unlit;
			}
		}

		applyModifier(context, mod, resourcePath) {
			if (mod.isEnabled(bg.base.MaterialFlag.DIFFUSE)) {
				this.diffuse = mod.diffuse;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.SPECULAR)) {
				this.specular = mod.specular;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.SHININESS)) {
				this.shininess = mod.shininess;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.LIGHT_EMISSION)) {
				this.lightEmission = mod.lightEmission;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.REFRACTION_AMOUNT)) {
				this.refractionAmount = mod.refractionAmount;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.REFLECTION_AMOUNT)) {
				this.reflectionAmount = mod.reflectionAmount;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.TEXTURE)) {
				this.texture = getTexture(context,mod.texture,resourcePath);
			}
			if (mod.isEnabled(bg.base.MaterialFlag.LIGHT_MAP)) {
				this.lightmap = getTexture(context,mod.lightmap,resourcePath);
			}
			if (mod.isEnabled(bg.base.MaterialFlag.NORMAL_MAP)) {
				this.normalMap = getTexture(context,mod.normalMap,resourcePath);
			}
			if (mod.isEnabled(bg.base.MaterialFlag.TEXTURE_OFFSET)) {
				this.textureOffset = mod.textureOffset;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.TEXTURE_SCALE)) {
				this.textureScale = mod.textureScale;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.LIGHT_MAP_OFFSET)) {
				this.lightmapOffset = mod.lightmapOffset;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.LIGHT_MAP_SCALE)) {
				this.lightmapScale = mod.lightmapScale;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.NORMAL_MAP_OFFSET)) {
				this.normalMapOffset = mod.normalMapOffset;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.NORMAL_MAP_SCALE)) {
				this.normalMapScale = mod.normalMapScale;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.CAST_SHADOWS)) {
				this.castShadows = mod.castShadows;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.RECEIVE_SHADOWS)) {
				this.receiveShadows = mod.receiveShadows;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.ALPHA_CUTOFF)) {
				this.alphaCutoff = mod.alphaCutoff;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.SHININESS_MASK)) {
				this.shininessMask = getTexture(context,mod.shininessMask,resourcePath);
			}
			if (mod.isEnabled(bg.base.MaterialFlag.SHININESS_MASK_CHANNEL)) {
				this.shininessMaskChannel = mod.shininessMaskChannel;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.SHININESS_MASK_INVERT)) {
				this.shininessMaskInvert = mod.shininessMaskInvert;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.LIGHT_EMISSION_MASK)) {
				this.lightEmissionMask = getTexture(context,mod.lightEmissionMask,resourcePath);
			}
			if (mod.isEnabled(bg.base.MaterialFlag.LIGHT_EMISSION_MASK_CHANNEL)) {
				this.lightEmissionMaskChannel = mod.lightEmissionMaskChannel;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.LIGHT_EMISSION_MASK_INVERT)) {
				this.lightEmissionMaskInvert = mod.lightEmissionMaskInvert;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.REFLECTION_MASK)) {
				this.reflectionMask = getTexture(context,mod.reflectionMask,resourcePath);
			}
			if (mod.isEnabled(bg.base.MaterialFlag.REFLECTION_MASK_CHANNEL)) {
				this.reflectionMaskChannel = mod.reflectionMaskChannel;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.REFLECTION_MASK_INVERT)) {
				this.reflectionMaskInvert = mod.reflectionMaskInvert;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.CULL_FACE)) {
				this.cullFace = mod.cullFace;
			}

			// See above note for ROUGHNESS flags
			if (mod.isEnabled(bg.base.MaterialFlag.ROUGHNESS)) {
				this.roughness = mod.roughness;
				this.roughnessMask = getTexture(context,mod.roughnessMask,resourcePath);
				this.roughnessMaskChannel = mod.roughnessMaskChannel;
				this.roughnessMaskInvert = mod.roughnessMaskInvert;
			}

			if (mod.isEnabled(bg.base.MaterialFlag.UNLIT)) {
				this.unlit = mod.unlit;
			}
		}
		
		getModifierWithMask(modifierMask) {
			var mod = new MaterialModifier();

			mod.modifierFlags = modifierMask;
			
			if (mod.isEnabled(bg.base.MaterialFlag.DIFFUSE)) {
				mod.diffuse = this.diffuse;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.SPECULAR)) {
				mod.specular = this.specular;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.SHININESS)) {
				mod.shininess = this.shininess;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.LIGHT_EMISSION)) {
				mod.lightEmission = this.lightEmission;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.REFRACTION_AMOUNT)) {
				mod.refractionAmount = this.refractionAmount;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.REFLECTION_AMOUNT)) {
				mod.reflectionAmount = this.reflectionAmount;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.TEXTURE)) {
				mod.texture = getPath(this.texture);
			}
			if (mod.isEnabled(bg.base.MaterialFlag.LIGHT_MAP)) {
				mod.lightmap = getPath(this.lightmap);
			}
			if (mod.isEnabled(bg.base.MaterialFlag.NORMAL_MAP)) {
				mod.normalMap = getPath(this.normalMap);
			}
			if (mod.isEnabled(bg.base.MaterialFlag.TEXTURE_OFFSET)) {
				mod.textureOffset = this.textureOffset;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.TEXTURE_SCALE)) {
				mod.textureScale = this.textureScale;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.LIGHT_MAP_OFFSET)) {
				mod.lightmapOffset = this.lightmapOffset;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.LIGHT_MAP_SCALE)) {
				mod.lightmapScale = this.lightmapScale;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.NORMAL_MAP_OFFSET)) {
				mod.normalMapOffset = this.normalMapOffset;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.NORMAL_MAP_SCALE)) {
				mod.normalMapScale = this.normalMapScale;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.CAST_SHADOWS)) {
				mod.castShadows = this.castShadows;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.RECEIVE_SHADOWS)) {
				mod.receiveShadows = this.receiveShadows;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.ALPHA_CUTOFF)) {
				mod.alphaCutoff = this.alphaCutoff;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.SHININESS_MASK)) {
				mod.shininessMask = getPath(this.shininessMask);
			}
			if (mod.isEnabled(bg.base.MaterialFlag.SHININESS_MASK_CHANNEL)) {
				mod.shininessMaskChannel = this.shininessMaskChannel;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.SHININESS_MASK_INVERT)) {
				mod.shininessMaskInvert = this.shininessMaskInvert;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.LIGHT_EMISSION_MASK)) {
				mod.lightEmissionMask = getPath(this.lightEmissionMask);
			}
			if (mod.isEnabled(bg.base.MaterialFlag.LIGHT_EMISSION_MASK_CHANNEL)) {
				mod.lightEmissionMaskChannel = this.lightEmissionMaskChannel;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.LIGHT_EMISSION_MASK_INVERT)) {
				mod.lightEmissionMaskInver = this.lightEmissionMaskInver;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.REFLECTION_MASK)) {
				mod.reflectionMask = getPath(this.reflectionMask);
			}
			if (mod.isEnabled(bg.base.MaterialFlag.REFLECTION_MASK_CHANNEL)) {
				mod.reflectionMaskChannel = this.reflectionMaskChannel;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.REFLECTION_MASK_INVERT)) {
				mod.reflectionMaskInvert = this.reflectionMaskInvert;
			}
			if (mod.isEnabled(bg.base.MaterialFlag.CULL_FACE)) {
				mod.cullFace = this.cullFace;
			}

			// See above note about ROUGHNESS flag
			if (mod.isEnabled(bg.base.MaterialFlag.ROUGHNESS)) {
				mod.roughness = this.roughness;
				mod.roughnessMask = getPath(this.roughnessMask);
				mod.roughnessMaskChannel = this.roughnessMaskChannel;
				mod.roughnessMaskInvert = this.roughnessMaskInvert;
			}

			if (mod.isEnabled(bg.base.MaterialFlag.UNLIT)) {
				mod.unlit = this.unlit;
			}

			return mod;
		}
		
		static GetMaterialWithJson(context,data,path) {
			let material = new Material();
			if (data.cullFace===undefined) {
				data.cullFace = true;
			}
			
			material.diffuse.set(data.diffuseR,data.diffuseG,data.diffuseB,data.diffuseA);
			material.specular.set(data.specularR,data.specularG,data.specularB,data.specularA);
			material.shininess = data.shininess;
			material.lightEmission = data.lightEmission;
			
			material.refractionAmount = data.refractionAmount;
			material.reflectionAmount = data.reflectionAmount;
			
			material.textureOffset.set(data.textureOffsetX,data.textureOffsetY);
			material.textureScale.set(data.textureScaleX,data.textureScaleY);
			
			material.lightmapOffset.set(data.lightmapOffsetX,data.lightmapOffsetY);
			material.lightmapScale.set(data.lightmapScaleX,data.lightmapScaleY);
			
			material.normalMapOffset.set(data.normalMapOffsetX,data.normalMapOffsetY);
			material.normalMapScale.set(data.normalMapScaleX,data.normalMapScaleY);
			
			material.alphaCutoff = data.alphaCutoff;
			material.castShadows = data.castShadows;
			material.receiveShadows = data.receiveShadows;
			
			material.shininessMaskChannel = data.shininessMaskChannel;
			material.shininessMaskInvert = data.invertShininessMask;
			
			material.lightEmissionMaskChannel = data.lightEmissionMaskChannel;
			material.lightEmissionMaskInvert = data.invertLightEmissionMask;
			
			material.reflectionMaskChannel = data.reflectionMaskChannel;
			material.reflectionMaskInvert = data.invertReflectionMask;

			material.roughness = data.roughness;
			material.roughnessMaskChannel = data.roughnessMaskChannel;
			material.roughnessMaskInvert = data.invertRoughnessMask;
			
			material.cullFace = data.cullFace;
			
			material.unlit = data.unlit;
			
			if (path && path[path.length-1]!='/') {
				path += '/';
			}
			
			function mergePath(path,file) {
				if (!file) return null;
				return path ? path + file:file;
			}

			data.texture = mergePath(path,data.texture);
			data.lightmap = mergePath(path,data.lightmap);
			data.normalMap = mergePath(path,data.normalMap);
			data.shininessMask = mergePath(path,data.shininessMask);
			data.lightEmissionMask = mergePath(path,data.lightEmissionMask);
			data.reflectionMask = mergePath(path,data.reflectionMask);
			data.roughnessMask = mergePath(path,data.roughnessMask);
			
			return new Promise((accept,reject) => {
				let textures = [];
				
				if (data.texture) {
					textures.push(data.texture);
				}
				if (data.lightmap && textures.indexOf(data.lightmap)==-1) {
					textures.push(data.lightmap);
				}
				if (data.normalMap && textures.indexOf(data.normalMap)==-1) {
					textures.push(data.normalMap);
				}
				if (data.shininessMask && textures.indexOf(data.shininessMask)==-1) {
					textures.push(data.shininessMask);
				}
				if (data.lightEmissionMask && textures.indexOf(data.lightEmissionMask)==-1) {
					textures.push(data.lightEmissionMask);
				}
				if (data.reflectionMask && textures.indexOf(data.reflectionMask)==-1) {
					textures.push(data.reflectionMask);
				}
				if (data.roughnessMask && textures.indexOf(data.roughnessMask)==-1) {
					textures.push(data.roughnessMask);
				}
				
				bg.utils.Resource.Load(textures)
					.then(function(images) {
						material.texture = loadTexture(context,images[data.texture],data.texture);
						material.lightmap = loadTexture(context,images[data.lightmap],data.lightmap);
						material.normalMap = loadTexture(context,images[data.normalMap],data.normalMap);
						material.shininessMask = loadTexture(context,images[data.shininessMask],data.shininessMask);
						material.lightEmissionMask = loadTexture(context,images[data.lightEmissionMask],data.lightEmissionMask);
						material.reflectionMask = loadTexture(context,images[data.reflectionMask],data.reflectionMask);
						material.roughnessMask = loadTexture(context,images[data.roughnessMask],data.roughnessMask);
						accept(material);
					});
			});
		}
	}
	
	bg.base.Material = Material;
})();

(function() {
	class MatrixStack {
		constructor() {
			this._matrix = bg.Matrix4.Identity();
			this._stack = [];
			this._changed = true;
		}
		
		get changed() { return this._changed; }
		set changed(c) { this._changed = c; }
		
		push() {
			this._stack.push(new bg.Matrix4(this._matrix));
		}
		
		set(m) {
			this._matrix.assign(m);
			this._changed = true;
			return this;
		}
		
		mult(m) {
			this._matrix.mult(m);
			this._changed = true;
			return this;
		}
		
		identity() {
			this._matrix.identity();
			this._changed = true;
			return this;
		}
		
		translate(x, y, z) {
			this._matrix.translate(x, y, z);
			this._changed = true;
			return this;
		}
		
		rotate(alpha, x, y, z) {
			this._matrix.rotate(alpha, x, y, z);
			this._changed = true;
			return this;
		}
		
		scale(x, y, z) {
			this._matrix.scale(x, y, z);
			this._changed = true;
			return this;
		}

		setScale(x, y, z) {
			this._matrix.setScale(x,y,z);
			this._changed = true;
			return this;
		}
		
		perspective(fov,aspect,near,far) {
			this._matrix
				.identity()
				.perspective(fov,aspect,near,far);
			this._changed = true;
			return this;
		}
		
		frustum(left, right, bottom, top, nearPlane, farPlane) {
			this._matrix
				.identity()
				.frustum(left,right,bottom,top,nearPlane,farPlane);
			this._changed = true;
			return this;
		}

		ortho(left, right, bottom, top, nearPlane, farPlane) {
			this._matrix
				.identity()
				.ortho(left,right,bottom,top,nearPlane,farPlane);
			this._changed = true;
			return this;
		}
		
		invert() {
			this._matrix.invert();
			this._changed = true;
			return this;
		}
		
		get matrix() {
			this._changed = true;
			return this._matrix;
		}
		
		// This accessor will return the current matrix without mark the internal state
		// to changed. There isn't any way in JavaScript to ensure that the returned matrix
		// will not be changed, therefore use this accessor carefully. It's recommended to use this
		// accessor ONLY to retrieve the matrix and pass it to the shaders. 
		get matrixConst() {
			return this._matrix;
		}
		
		pop() {
			if (this._stack.length) {
				this._matrix.assign(this._stack.pop());
				this._changed = true;
			}
			return this._matrix;
		}
	}
	
	bg.base.MatrixStack = MatrixStack;
	
	let s_MatrixState = null;
	
	class MatrixState {
		static Current() {
			if (!s_MatrixState) {
				s_MatrixState = new MatrixState();
			}
			return s_MatrixState;
		}
		
		static SetCurrent(s) {
			s_MatrixState = s;
			return s_MatrixState;
		}
		
		constructor() {
			// Matrixes
			this._modelMatrixStack = new MatrixStack();
			this._viewMatrixStack = new MatrixStack();
			this._projectionMatrixStack = new MatrixStack();
			this._modelViewMatrix = bg.Matrix4.Identity();
			this._normalMatrix = bg.Matrix4.Identity();
			this._cameraDistanceScale = null;
		}
		
		get modelMatrixStack() {
			return this._modelMatrixStack;
		}
		
		get viewMatrixStack() {
			return this._viewMatrixStack;
		}
		
		get projectionMatrixStack() {
			return this._projectionMatrixStack;
		}
		
		get modelViewMatrix() {
			if (!this._modelViewMatrix || this._modelMatrixStack.changed || this._viewMatrixStack.changed) {
				this._modelViewMatrix = new bg.Matrix4(this._viewMatrixStack._matrix);
				this._modelViewMatrix.mult(this._modelMatrixStack._matrix);
				this._modelMatrixStack.changed = false;
				this._viewMatrixStack.changed = false;
			}
			return this._modelViewMatrix;
		}
		
		get normalMatrix() {
			if (!this._normalMatrix || this._modelMatrixStack.changed || this._viewMatrixStack.changed) {
				this._normalMatrix = new bg.Matrix4(this.modelViewMatrix);
				this._normalMatrix.invert();
				this._normalMatrix.traspose();
				this._modelMatrixStack.changed = false;
			}
			return this._normalMatrix;
		}
		
		get viewMatrixInvert() {
			if (!this._viewMatrixInvert || this._viewMatrixStack.changed) {
				this._viewMatrixInvert = new bg.Matrix4(this.viewMatrixStack.matrixConst);
				this._viewMatrixInvert.invert();
			}
			return this._viewMatrixInvert;
		}

		// This function returns a number that represents the
		// distance from the camera to the model.
		get cameraDistanceScale() {
			return this._cameraDistanceScale = this._viewMatrixStack.matrix.position.magnitude();
		}
	}
	
	bg.base.MatrixState = MatrixState;
})();
(function() {
    bg.base.PBRMaterialFlag = {
        DIFFUSE                     : 1 << 0,   // diffuse, isTransparent, alphaCutoff
        DIFFUSE_SCALE               : 1 << 1,   // diffuseScale
        DIFFUSE_OFFSET              : 1 << 2,   // diffuseOffset
        METALLIC                    : 1 << 3,   // metallic, metallicChannel
        ROUGHNESS                   : 1 << 4,   // roughness, roughnessChannel
        LIGHT_EMISSION              : 1 << 5,   // lightEmission, lightEmissionChannel
        NORMAL                      : 1 << 6,   // normal
        NORMAL_SCALE                : 1 << 7,   // normalScale
        NORMAL_OFFSET               : 1 << 8,   // normalOffset
        LIGHT_MAP                   : 1 << 9,   // not used
        HEIGHT                      : 1 << 10,  // height, heightChannel
        HEIGHT_SCALE                : 1 << 11,  // heightScale
        SHADOWS                     : 1 << 12,  // castShadows/receiveShadows
        CULL_FACE                   : 1 << 13,  // cullFace
        UNLIT                       : 1 << 14   // unlit
    };

    function getColorOrTexture(data,defaultValue = bg.Color.Black()) {
        if (Array.isArray(diffuse) && diffuse.length==3) {
            return new bg.Color(data[0],data[1],data[2],1);
        }
        else if (Array.isArray(diffuse) && diffuse.length>=4) {
            return new bg.Color(data[0],data[1],data[2],data[3]);
        }
        else if (typeof(diffuse) == "string" && diffuse != "") {
            return diffuse;
        }
        else {
            return defaultValue;
        }
    }

    function getVector(data,defaultValue = bg.Vector2()) {
        if (Array.isArray(data) && data.length==2) {
            return new bg.Vector2(data);
        }
        else if (Array.isArray(data) && data.length==3) {
            return new bg.Vector3(data);
        }
        else if (Array.isArray(data) && data.length==4) {
            return new bg.Vector4(data);
        }
        else {
            return defaultValue;
        }
    }

    function getScalarOrTexture(data,defaultValue = 0) {
        if (data!==undefined && !isNaN(Number(data)) && data!=="") {
            return Number(data);
        }
        else if (data!==undefined && typeof(data)=="string" && data!="") {
            return data;
        }
        else {
            return defaultValue;
        }
    }

    function getScalar(data,defaultValue = 0) {
        if (data!==undefined && !isNaN(Number(data)) && data!=="") {
            return Number(data);
        }
        else {
            return defaultValue;
        }
    }

    function getBoolean(value, defaultValue=true) {
        if (typeof(value)=="string" && value!=="") {
            return /true/i.test(value) || /yes/i.test(value) || /1/.test(value);
        }
        else if (value!==undefined) {
            return value;
        }
        else {
            return defaultValue;
        }
    }

    function getVector(value) {
        if (value instanceof bg.Vector2 || value instanceof bg.Vector3 || value instanceof bg.Vector4) {
            return value.toArray();            
        }
        else {
            return [0,0];
        }
    }

    function getColorOrTexture(value) {
        if (value instanceof bg.Color) {
            return value.toArray();
        }
        else if (typeof(value) == "string" && value != "") {
            return value;
        }
        else {
            return [0,0,0,1];
        }
    }

    function getScalarOrTexture(value) {
        if (value!==undefined && !isNaN(Number(value))) {
            return Number(value);
        }
        else if (typeof(value) == "string" && value!="") {
            return "";
        }
        else {
            return 0;
        }
    }

    class PBRMaterialModifier {
        constructor(jsonData) {
            this._modifierFlags = 0;

            this._diffuse = bg.Color.White();
            this._isTransparent = false;
            this._alphaCutoff = 0.5;
            this._diffuseScale = new bg.Vector2(1);
            this._diffuseOffset = new bg.Vector2();
            this._metallic = 0;
            this._metallicChannel = 0;
            this._roughness = 1;
            this._roughnessChannel = 0;
            this._lightEmission = 0;
            this._lightEmissionChannel = 0;
            this._height = 0;
            this._heightChannel = 0;
            this._heightScale = 1;
            this._normal = new bg.Color(0.5,0.5,1,1);
            this._normalScale = new bg.Vector2(1);
            this._normalOffset = new bg.Vector2();
            this._castShadows = true;
            this._cullFace = true;
            this._unlit = false;

            if (jsonData && jsonData.type != "pbr") {
                console.warn("non-pbr data used in pbr material modifier.");
                // TODO: Import from legacy material modifier
                if (jsonData.texture) {
                    this._diffuse = getColorOrTexture(jsonData.texture, this._diffuse);
                }
                else {
                    this._diffuse = new bg.Color(
                        jsonData.diffuseR!==undefined ? jsonData.diffuseR : 1,
                        jsonData.diffuseG!==undefined ? jsonData.diffuseG : 1,
                        jsonData.diffuseB!==undefined ? jsonData.diffuseB : 1,
                        jsonData.diffuseA!==undefined ? jsonData.diffuseA : 1
                    );
                }

                this._diffuseScale = new bg.Vector2(
                    jsonData.diffuseScaleX!==undefined ? jsonData.diffuseScaleX : this._diffuseScale.x,
                    jsonData.diffuseScaleY!==undefined ? jsonData.diffuseScaleY : this._diffuseScale.y
                );
                this._diffuseOffset = new bg.Vector2(
                    jsonData.diffuseOffsetX!==undefined ? jsonData.diffuseOffsetX : this._diffuseOffset.x,
                    jsonData.diffuseOffsetY!==undefined ? jsonData.diffuseOffsetY : this._diffuseOffset.y
                );

                if (jsonData.normalMap) {
                    this._normal = getColorOrTexture(jsonData.normalMap, this._normal);
                }
                this._normalScale = new bg.Vector2(
                    jsonData.normalMapScaleX!==undefined ? jsonData.normalMapScaleX : this._normalScale.x,
                    jsonData.normalMapScaleY!==undefined ? jsonData.normalMapScaleY : this._normalScale.y
                );
                this._normalOffset = new bg.Vector2(
                    jsonData.normalMapOffsetX!==undefined ? jsonData.normalMapOffsetX : this._normalOffset.x,
                    jsonData.normalMapOffsetY!==undefined ? jsonData.normalMapOffsetY : this._normalOffset.y
                );

                if (jsonData.diffuseR || jsonData.diffuseG || jsonData.diffuseB || jsonData.diffuseA || jsonData.texture) {
                    this.setEnabled(bg.base.PBRMaterialFlag.DIFFUSE);
                }
                if (jsonData.diffuseScaleX || jsonData.diffuseScaleY) {
                    this.setEnabled(bg.base.PBRMaterialFlag.DIFFUSE_SCALE);
                }
                if (jsonData.diffuseOffsetX || jsonData.diffuseOffsetY) {
                    this.setEnabled(bg.base.PBRMaterialFlag.DIFFUSE_OFFSET);
                }
                if (jsonData.normalMap) {
                    this.setEnabled(bg.base.PBRMaterialFlag.NORMAL);
                }
                if  (jsonData.normalMapScaleX || jsonData.normalMapScaleY) {
                    this.setEnabled(bg.base.PBRMaterialFlag.NORMAL_SCALE);
                }
                if  (jsonData.normalMapOffsetX || jsonData.normalMapOffsetY) {
                    this.setEnabled(bg.base.PBRMaterialFlag.NORMAL_OFFSET);
                }
            }
            else if (jsonData) {
                this._diffuse = getColorOrTexture(jsonData.diffuse, this._diffuse);
                this._isTransparent = getBoolean(jsonData.isTransparent, this._isTransparent);
                this._alphaCutoff = getScalar(jsonData.alphaCutoff, this._alphaCutoff);
                this._diffuseScale = getVector(jsonData.diffuseScale, this._diffuseScale);
                this._diffuseOffset = getVector(jsonData.diffuseOffset, this._diffuseOffset);
                this._metallic = getScalarOrTexture(jsonData.metallic, this._metallic);
                this._metallicChannel = getScalar(jsonData.metallicChannel, this._metallicChannel);
                this._roughness = getScalarOrTexture(jsonData.roughness, this._roughness);
                this._roughnessChannel = getScalar(jsonData.roughnessChannel, this._roughnessChannel);
                this._lightEmission = getScalarOrTexture(jsonData.lightEmission, this._lightEmission);
                this._lightEmissionChannel = getScalar(jsonData.lightEmissionChannel, this._lightEmissionChannel);
                this._height = getColorOrTexture(jsonData.height, this._height);
                this._heightChannel = getScalar(jsonData.heightChannel, this._heightChannel);
                this._heightScale = getScalar(jsonData.heightScale, this._heightScale);
                this._normal = getColorOrTexture(jsonData.normal, this._normal);
                this._normalScale = getVector(jsonData.normalScale, this._normalScale);
                this._normalOffset = getVector(jsonData.normalOffset, this._normalOffset);
                this._castShadows = getBoolean(jsonData.castShadows, this._castShadows);
                this._cullFace = getBoolean(jsonData.cullFace, this._cullFace);
                this._unlit = getBoolean(jsonData.unlit, this._unlit);

                if (jsonData.diffuse || jsonData.isTransparent || jsonData.alphaCutoff) {
                    this.setEnabled(bg.base.PBRMaterialFlag.DIFFUSE);
                }
                if (jsonData.diffuseScale) {
                    this.setEnabled(bg.base.PBRMaterialFlag.DIFFUSE_SCALE);
                }
                if (jsonData.diffuseOffset) {
                    this.setEnabled(bg.base.PBRMaterialFlag.DIFFUSE_OFFSET);
                }
                if (jsonData.metallic!==undefined) {
                    this.setEnabled(bg.base.PBRMaterialFlag.METALLIC);
                }
                if (jsonData.roughness!==undefined) {
                    this.setEnabled(bg.base.PBRMaterialFlag.ROUGHNESS);
                }
                if (jsonData.lightEmission!==undefined) {
                    this.setEnabled(bg.base.PBRMaterialFlag.LIGHT_EMISSION);
                }
                if (jsonData.heigh!==undefined) {
                    this.setEnabled(bg.base.PBRMaterialFlag.HEIGHT);
                }
                if (jsonData.heightScale!==undefined) {
                    this.setEnabled(bg.base.PBRMaterialFlag.HEIGHT_SCALE);
                }
                if (jsonData.normal) {
                    this.setEnabled(bg.base.PBRMaterialFlag.NORMAL);
                }
                if (jsonData.normalScale) {
                    this.setEnabled(bg.base.PBRMaterialFlag.NORMAL_SCALE);
                }
                if (jsonData.normalOffset) {
                    this.setEnabled(bg.base.PBRMaterialFlag.NORMAL_OFFSET);
                }
                if (jsonData.castShadows!==undefined) {
                    this.setEnabled(bg.base.PBRMaterialFlag.SHADOWS);
                }
                if (jsonData.cullFace!==undefined) {
                    this.setEnabled(bg.base.PBRMaterialFlag.CULL_FACE);
                }
                if (jsonData.unlit!==undefined) {
                    this.setEnabled(bg.base.PBRMaterialFlag.UNLIT);
                }

            }
        }

        get modifierFlags() { return this._modifierFlags; }
		set modifierFlags(f) { this._modifierFlags = f; }
		setEnabled(flag) { this._modifierFlags = this._modifierFlags | flag; }
        isEnabled(flag) { return (this._modifierFlags & flag)!=0; }
        
        get diffuse() { return this._diffuse; }
        get isTransparent() { return this._isTransparent; }
        get alphaCutoff() { return this._alphaCutoff; }
        set diffuse(v) { this._diffuse = v; this.setEnabled(bg.base.PBRMaterialFlag.DIFFUSE); }
        set isTransparent(v) { this._isTransparent = v; this.setEnabled(bg.base.PBRMaterialFlag.DIFFUSE); }
        set alphaCutoff(v) { this._alphaCutoff = v; this.setEnabled(bg.base.PBRMaterialFlag.DIFFUSE); }
        get diffuseScale() { return this._diffuseScale; }
        set diffuseScale(v) { this._diffuseScale = v; this.setEnabled(bg.base.PBRMaterialFlag.DIFFUSE_SCALE); }
        get diffuseOffset() { return this._diffuseOffset; }
        set diffuseOffset(v) { this._diffuseOffset = v; this.setEnabled(bg.base.PBRMaterialFlag.DIFFUSE_OFFSET); }
        get metallic() { return this._metallic; }
        set metallic(v) { this._metallic = v; this.setEnabled(bg.base.PBRMaterialFlag.METALLIC); }
        get metallicChannel() { return this._metallicChannel; }
        set metallicChannel(v) { this._metallicChannel = v; this.setEnabled(bg.base.PBRMaterialFlag.METALLIC); }
        get roughness() { return this._roughness; }
        set roughness(v) { this._roughness = v; this.setEnabled(bg.base.PBRMaterialFlag.ROUGHNESS); }
        get roughnessChannel() { return this._roughnessChannel; }
        set roughnessChannel(v) { this._roughnessChannel = v; this.setEnabled(bg.base.PBRMaterialFlag.ROUGHNESS); }
        get lightEmission() { return this._lightEmission; }
        set lightEmission(v) { this._lightEmission = v; this.setEnabled(bg.base.PBRMaterialFlag.LIGHT_EMISSION); }
        get lightEmissionChannel() { return this._lightEmissionChannel; }
        set lightEmissionChannel(v) { this._lightEmissionChannel = v; this.setEnabled(bg.base.PBRMaterialFlag.LIGHT_EMISSION); }
        get height() { return this._height; }
        set height(v) { this._height =v; this.setEnabled(bg.base.PBRMaterialFlag.HEIGHT); }
        get heightChannel() { return this._heightChannel; }
        set heightChannel(v) { this._heightChannel = v; this.setEnabled(bg.base.PBRMaterialFlag.HEIGHT); }
        get heighScale() { return this._heighScale; }
        set heighScale(v) { this._heighScale = v; this.setEnabled(bg.base.PBRMaterialFlag.HEIGHT_SCALE); }
        get normal() { return this._normal; }
        set normal(v) { this._normal =v; this.setEnabled(bg.base.PBRMaterialFlag.NORMAL); }
        get normalScale() { return this._normalScale; }
        set normalScale(v) { this._normalScale = v; this.setEnabled(bg.base.PBRMaterialFlag.NORMAL_SCALE); }
        get normalOffset() { return this._normalOffset; }
        set normalOffset(v) { this._normalOffset = v; this.setEnabled(bg.base.PBRMaterialFlag.NORMAL_OFFSET); }
        get castShadows() { return this._castShadows; }
        set castShadows(v) { this._castShadows = v; this.setEnabled(bg.base.PBRMaterialFlag.SHADOWS); }
        get cullFace() { return this._cullFace; }
        set cullFace(v) { this._cullFace = v; this.setEnabled(bg.base.PBRMaterialFlag.CULL_FACE); }
        get unlit() { return this._unlit; }
        set unlit(v) { this._unlit = v; this.setEnabled(bg.base.PBRMaterialFlag.UNLIT); }

        clone() {
            let copy = new PBRMaterialModifier();
            copy.assign(this);
            return copy;
        }

        assign(mod) {
            this._modifierFlags = mod._modifierFlags;
            
            this._diffuse = mod._diffuse;
            this._isTransparent = mod._isTransparent;
            this._alphaCutoff = mod._alphaCutoff;
            this._diffuseScale = mod._diffuseScale;
            this._diffuseOffset = mod._diffuseOffset;
            this._metallic = mod._metallic;
            this._metallicChannel = mod._metallicChannel;
            this._roughness = mod._roughness;
            this._roughnessChannel = mod._roughnessChannel;
            this._lightEmission = mod._lightEmission;
            this._lightEmissionChannel = mod._lightEmissionChannel;
            this._height = mod._height;
            this._heightChannel = mod._heightChannel;
            this._heightScale = mod._heightScale;
            this._normal = mod._normal;
            this._normalScale = mod._normalScale;
            this._normalOffset = mod._normalOffset;
            this._castShadows = mod._castShadows;
            this._cullFace = mod._cullFace;
            this._unlit = mod._unlit;        
        }

        serialize() {
            let result = {};
            let mask = this._modifierMask;

            if (mask & bg.base.PBRMaterialFlag.DIFFUSE) {
                result.diffuse = getColorOrTexture(this.diffuse);
                result.isTransparent = getBoolean(this.isTransparent);
                result.alphaCutoff = getBoolean(this.alphaCutoff);
            }
            if (mask & bg.base.PBRMaterialFlag.DIFFUSE_SCALE) {
                result.diffuseScale = getVector(this.diffuseScale);
            }
            if (mask & bg.base.PBRMaterialFlag.DIFFUSE_OFFSET) {
                result.diffuseOffset = getVector(this.diffuseOffset);
            }
            if (mask & bg.base.PBRMaterialFlag.METALLIC) {
                result.metallic = getScalarOrTexture(this.metallic);
                result.metallicChannel = getScalar(this.metallicChannel);
            }
            if (mask & bg.base.PBRMaterialFlag.ROUGHNESS) {
                result.roughness = getScalarOrTexture(this.roughness);
                result.roughnessChannel = getScalar(this.roughnessChannel);
            }
            if (mask & bg.base.PBRMaterialFlag.LIGHT_EMISSION) {
                result.lightEmission = getScalarOrTexture(this.lightEmission);
                result.lightEmissionChannel = getScalar(this.lightEmissionChannel);
            }
            if (mask & bg.base.PBRMaterialFlag.NORMAL) {
                result.normal = getColorOrTexture(this.normal);
            }
            if (mask & bg.base.PBRMaterialFlag.NORMAL_SCALE) {
                result.normalScale = getVector(this.normalScale);
            }
            if (mask & bg.base.PBRMaterialFlag.NORMAL_OFFSET) {
                result.normalOffset = getVector(this.normalOffset);
            }
            if (mask & bg.base.PBRMaterialFlag.HEIGHT) {
                result.height = getScalarOrTexture(this.height);
                result.heightChannel = getScalar(this.heightChannel);
            }
            if (mask & bg.base.PBRMaterialFlag.HEIGHT_SCALE) {
                result.heightScale = getScalar(this.heightScale);
            }
            if (mask & bg.base.PBRMaterialFlag.SHADOWS) {
                result.castShadows = getBoolean(this.castShadows);
            }
            if (mask & bg.base.PBRMaterialFlag.CULL_FACE) {
                result.cullFace = getBoolean(this.cullFace);
            }
            if (mask & bg.base.PBRMaterialFlag.UNLIT) {
                result.unlit = getBoolean(this.unlit);
            }

            return result;
        }
    }

    bg.base.PBRMaterialModifier = PBRMaterialModifier;

    // Image load functions defined in bg.base.imageTools:
    //      isAbsolutePath(path)
    //      getTexture(context,texturePath,resourcePath)
    //      getPath(texture)        texture ? texture.fileName : ""
    //      readTexture(context,basePath,texData,mat,property)  

    // Returns a texture from a scalar or a vector parameter
    function getMap(context,matParam) {

        let vecValue = null;
        let num = Number(matParam);
        if (isNaN(num)) {
            if (matParam instanceof bg.Vector3) {
                vecValue = new bg.Vector4(matParam.x,matParam.y,matParam.z,0);
            }
            else if (matParam instanceof bg.Vector2) {
                vecValue = new bg.Vector4(matParam.x,matParam.y,0,0);
            }
            else if (matParam instanceof bg.Vector4) {
                vecValue = matParam;
            }
            else if (matParam===undefined) {
                vecValue = new bg.Vector4(0,0,0,0);
            }
        }
        else {
            vecValue = new bg.Vector4(num,num,num,num);
        }

        if (vecValue) {
            return bg.base.Texture.ColorTexture(context,vecValue,{ width: 1, height: 1 });
        }
        else {
            throw new Error("PBRMaterial invalid material parameter specified.");
        }
    }

    // Release the specified map, if is marked to release
    function release(mapName) {
        let map = this._shaderParameters[mapName];
        if (map && map.map && map.release) {
            map.map.destroy();
        }
    }

    // Combine height, metallic, roughness and ambient occlus into one map
    // TODO: Combine the light emission channel in the normal map alpha channel
    function combineMaps(gl) {
        // Combine height, metallic, roughness and ambient occlussion
        // TODO: ambient occlussion not implemented yet
        

        let height = {
            map: this._shaderParameters.height.map,
            channel: this._heightChannel
        };
        let metallic = {
            map: this._shaderParameters.metallic.map,
            channel: this._metallicChannel
        };
        let roughness = {
            map: this._shaderParameters.roughness.map,
            channel: this._roughnessChannel
        };
        // TODO: Implement ambien occlussion
        let ao = {
            map: bg.base.TextureCache.WhiteTexture(gl),
            channel: 3
        };


        if (!this._merger) {
            this._merger = new bg.tools.TextureMerger(gl);
        }
        let hmrao = this._merger.mergeMaps(height, metallic, roughness, ao);

        //if (!this._shaderParameters.heightMetallicRoughnessAO.map) {
            this._shaderParameters.heightMetallicRoughnessAO.map = hmrao;
        //}

        this._updateMergedTexture = false;
    }

    // colorOrPath: MUST be a valid file name or a bg.Color instance
    function getMaterialMap(context,paramName,colorOrPath,basePath) {
        return new Promise((resolve,reject) => {
            if (typeof(colorOrPath) == "string") {
                if (!bg.base.imageTools.isAbsolutePath(colorOrPath)) {
                    colorOrPath = bg.base.imageTools.mergePath(basePath,colorOrPath);
                }
                bg.base.Loader.Load(context,colorOrPath)
                    .then((texture) => {
                        this[paramName] = texture;
                        resolve(texture);
                    })
                    .catch((err) => {
                        reject(err);
                    });
            }
            else if (colorOrPath instanceof bg.Color) {
                this[paramName] = colorOrPath;
                resolve(colorOrPath);
            }
            else {
                reject(new Error("Invalid PBR color parameter"));
            }
        })
    }

    class PBRMaterial {
        static ImportFromLegacyMaterial(context,mat) {
            let result = new bg.base.PBRMaterial();
            result.diffuse  = mat.texture || mat.diffuse;
            result.diffuseScale = mat.textureScale;
            result.diffuseOffset = mat.textureOffset;
            result.normalScale = mat.normalMapScale;
            result.normalOffset = mat.normalMapOffset;
            if (mat.normalMap) {
                result.normal = mat.normalMap;
            }
            if (mat.roughnessMask) {
                result.roughness = mat.roughnessMask;
            }
            if (!result.roughness && mat.shininessMask) {
                result.roughness = mat.shininessMask;
            }
            if (mat.reflectionMask) {
                result.metallic = mat.reflectionMask;
            }
            result.castShadows = mat.castShadows;
            return result;
        }

        static FromMaterialDefinition(context,def,basePath="") {
            let mat = new PBRMaterial();
            
            mat.diffuseScale = getVector(def.diffuseScale, new bg.Vector2(1));
            mat.diffuseOffset = getVector(def.diffuseOffset, new bg.Vector2());
            
            mat.metallicChannel = getScalar(def.metallicChannel, 0);
            mat.roughnessChannel = getScalar(def.roughnessChannel, 0);
            mat.lightEmissionChannel = getScalar(def.lightEmissionChannel, 0);
            mat.heightChannel = getScalar(def.heightChannel, 0);
            mat.heightScale = getScalar(def.heightScale, 1);
            
            mat.normalScale = getVector(def.normalScale, new bg.Vector2(1));
            mat.normalOffset = getVector(def.normalOffset, new bg.Vector2());
            mat.alphaCutoff = getScalar(def.alphaCutoff, 0.5);
            mat.castShadows = getBoolean(def.castShadows, true);
            mat.cullFace = getBoolean(def.cullFace, true);
            
            let promises = [
                getMaterialMap.apply(this, [context, 'diffuse', getColorOrTexture(def.diffuse, bg.Color.White()), basePath]),
                getMaterialMap.apply(this, [context, 'metallic', getScalarOrTexture(def.metallic, 0), basePath]),
                getMaterialMap.apply(this, [context, 'roughness', getScalarOrTexture(def.roughness, 1), basePath]),
                getMaterialMap.apply(this, [context, 'lightEmission', getScalarOrTexture(def.lightEmission, 0), basePath]),
                getMaterialMap.apply(this, [context, 'height', getScalarOrTexture(def.height, 0), basePath]),
                getMaterialMap.apply(this, [context, 'normal', getColorOrTexture(def.normal, new bg.Color(0.5,0.5,1,1)), basePath])
            ];

            return new Promise((resolve,reject) => {
                Promise.all(promises)
                    .then((result) => {
                        mat.getShaderParameters(context);
                        resolve(mat);
                    })
                    .catch((err) => {
                        console.warn(err.message);
                        mat.getShaderParameters(context);
                        resolve(mat);
                    });
            });
        }

        updateFromLegacyMaterial(context,mat) {
            if (mat.texture && this.diffuse!=mat.texture) {
                this.diffuse = mat.texture;
            }
            this.diffuseScale = mat.textureScale;
            this.diffuseOffset = mat.textureOffset;
            this.normalScale = mat.normalMapScale;
            this.normalOffset = mat.normalMapOffset;
            if (mat.normalMap != this.normal) {
                this.normal = mat.normalMap;
            }
            if (mat.roughnessMask && mat.roughnessMask != this.roughness) {
                this.roughness = mat.roughnessMask;
            }
            if (!mat.roughnessMask && this.roughness != mat.shininessMask) {
                this.roughness = mat.shininessMask;
            }
            if (mat.reflectionMask != this.metallic) {
                this.metallic = mat.reflectionMask;
            }
        }

        constructor() {
            this._diffuse = bg.Color.White();
            this._alphaCutoff = 0.5;
            this._isTransparent = false;
            this._metallic = 0;
            this._metallicChannel = 0;
            this._roughness = 1;
            this._roughnessChannel = 0;
            this._lightEmission = 0;
            this._lightEmissionChannel = 0;
            this._normal = new bg.Color(0.5,0.5,1,1);
            this._normalChannel = 0;
            this._height = bg.Color.Black();
            this._heightChannel = 0;
            this._heightScale = 1.0;
            this._castShadows = true;
            this._cullFace = true;
            this._unlit = false;

            // This variable is a flat that indicate that the
            // combined heighMetallicRoughnessAO texture must be
            // updated, because at least one of the base textures
            // have been chanted
            this._updateMergedTexture = true;

            this._shaderParameters = {
                diffuse: { map: null, release: false },
                metallic: { map: null, release: false },
                roughness: { map: null, release: false },
                lightEmission: { map: null, release: false },
                normal: { map: null, release: false },
                height: { map: null, release: false },

                heightMetallicRoughnessAO: { map: null, release: false },

                // Scalar and vector parameters
                diffuseOffset: new bg.Vector2(0,0),
                diffuseScale: new bg.Vector2(1,1),
                normalOffset: new bg.Vector2(0,0),
                normalScale: new bg.Vector2(1,1),
                alphaCutoff: 0.5,
                castShadows: true
            };
        }

        static Defaults(context) {
            let TexCache = bg.base.TextureCache;
            let whiteTexture = TexCache.WhiteTexture(context);
            let blackTexture = TexCache.BlackTexture(context);
            let normalTexture = TexCache.NormalTexture(context);

            return {
                diffuse: {
                    map: whiteTexture
                    // channel not used
                },
                metallic: {
                    map: blackTexture,
                    channel: 0
                },
                roughness: {
                    map: whiteTexture,
                    channel: 0
                },
                lightEmission: {
                    map: blackTexture,
                    channel: 0
                },
                normal: {
                    map: normalTexture
                    // channel not used
                },
                height: {
                    map: blackTexture,
                    channel: 0
                }
            };
        }

        clone() {
            let copy = new PBRMaterial();
            copy.assign(this);
            return copy;
        }

        assign(other) {
            this.diffuse = other.diffuse;
            this.isTransparent = other.isTransparent;
            this.alphaCutoff = other.alphaCutoff;
            this.diffuseScale = other.diffuseScale;
            this.diffuseOffset = other.diffuseOffset;
            this.metallic = other.metallic;
            this.metallicChannel = other.metallicChannel;
            this.roughness = other.roughness;
            this.roughnessChannel = other.roughnessChannel;
            this.lightEmission = other.lightEmission;
            this.lightEmissionChannel = other.lightEmissionChannel;
            this.height = other.height;
            this.heightChannel = other.heightChannel;
            this.heightScale = other.heightScale;
            this.normal = other.normal;
            this.normalScale = other.normalScale;
            this.normalOffset = other.normalOffset;
            this.castShadows = other.castShadows;
            this.cullFace = other.cullFace;
            this.unlit = other.unlit;
        }

        /* Release the PBR material resources:
         *  The PBR material parameters are always textures. You can set scalar or vector parameters,
         *  but in that case will be converted into a minimum size texture, that is stored in the material
         *  object, and for that reason it's important to delete the material objects to release that 
         *  resources.
         * 
         *  If the specified parameters are textures, then the PBRMaterial will not release that objects
         *  when the destroy() function is called
         */
        destroy() {
            release.apply(this,["diffuse"]);
            release.apply(this,["metallic"]);
            release.apply(this,["roughness"]);
            release.apply(this,["lightEmission"]);
            release.apply(this,["normal"]);
            release.apply(this,["height"]);
        }

        /* Returns the PBR shader parameters
         *  This function returns the material _shaderParameters object, and if it's necesary, will
         *  create the texture resources (for example, if a scalar or vector parameter is set).
         * 
         */
        getShaderParameters(context) {
            let prepareResource = (paramName) => {
                if (!this._shaderParameters[paramName].map) {
                    this._shaderParameters[paramName].release = true;
                    this._shaderParameters[paramName].map = getMap.apply(this,[context,this[paramName]]);
                }
            }

            prepareResource("diffuse");
            prepareResource("metallic");
            prepareResource("roughness");
            prepareResource("lightEmission");
            prepareResource("normal");
            prepareResource("height");

            if (this._updateMergedTexture) {
                combineMaps.apply(this,[context]);
            }

            return this._shaderParameters;
        }

        /* PBR material parameters
         *  The following functions returns the shader parameters. Each parameter may be an scalar,
         *  a vector or a texture value. If the parameter is intrinsically a one-dimesional value, for
         *  example, the roughness map, it will be accompanied by a channel index
         */
        get diffuse() { return this._diffuse; }
        get metallic() { return this._metallic; }
        get metallicChannel() { return this._metallicChannel; }
        get roughness() { return this._roughness; }
        get roughnessChannel() { return this._roughnessChannel; }
        get lightEmission() { return this._lightEmission; }
        get lightEmissionChannel() { return this._lightEmissionChannel; }
        get normal() { return this._normal; }
        get height() { return this._height; }

        set diffuse(v) {
            release.apply(this,["diffuse"]);
            this._shaderParameters.diffuse.map = v instanceof bg.base.Texture ? v : null;
            this._shaderParameters.diffuse.release = this._shaderParameters.diffuse.map == null;
            this._diffuse = v;
        }

        set metallic(v) {
            release.apply(this,["metallic"]);
            this._shaderParameters.metallic.map = v instanceof bg.base.Texture ? v : null;
            this._shaderParameters.metallic.release = this._shaderParameters.metallic.map == null;
            this._metallic = v;
            this._updateMergedTexture = true;
        }

        set metallicChannel(c) { this._metallicChannel = c; }

        set roughness(v) {
            release.apply(this,["roughness"]);
            this._shaderParameters.roughness.map = v instanceof bg.base.Texture ? v : null;
            this._shaderParameters.roughness.release = this._shaderParameters.roughness.map == null;
            this._roughness = v;
            this._updateMergedTexture = true;
        }

        set roughnessChannel(c) { this._roughnessChannel = c; }

        set lightEmission(v) {
            release.apply(this,["lightEmission"]);
            this._shaderParameters.lightEmission.map = v instanceof bg.base.Texture ? v : null;
            this._shaderParameters.lightEmission.release = this._shaderParameters.lightEmission.map == null;
            this._lightEmission = v;
            this._updateMergedTexture = true;
        }

        set lightEmissionChannel(c) { this._lightEmissionChannel = c; }

        set normal(v) {
            release.apply(this,["normal"]);
            this._shaderParameters.normal.map = v instanceof bg.base.Texture ? v : null;
            this._shaderParameters.normal.release = this._shaderParameters.normal.map == null;
            this._normal = v;
            this._updateMergedTexture = true;
        }

        set height(v) {
            release.apply(this,["height"]);
            this._shaderParameters.height.map = v instanceof bg.base.Texture ? v : null;
            this._shaderParameters.height.release = this._shaderParameters.height.map == null;
            this._height = v;
            this._updateMergedTexture = true;
        }

        set heightChannel(c) { this._heightChannel = c; }

        // Vector and scalar parameters
        
        get alphaCutoff() { return this._alphaCutoff; }
        set alphaCutoff(v) { this._alphaCutoff = v; }
        get isTransparent() { return this._isTransparent; }
        set isTransparent(v) { this._isTransparent = v; }
        get diffuseOffset() { return this._shaderParameters.diffuseOffset; }
        set diffuseOffset(v) { this._shaderParameters.diffuseOffset = v; }
        get diffuseScale() { return this._shaderParameters.diffuseScale; }
        set diffuseScale(v) { this._shaderParameters.diffuseScale = v; }
        get normalOffset() { return this._shaderParameters.normalOffset; }
        set normalOffset(v) { this._shaderParameters.normalOffset = v; }
        get normalScale() { return this._shaderParameters.normalScale; }
        set normalScale(v) { this._shaderParameters.normalScale = v; }
        get castShadows() { return this._castShadows; }
        set castShadows(c) { this._castShadows = c; }
        get heightScale() { return this._heightScale; }
        set heightScale(h) { this._heightScale = h; }
        get cullFace() { return this._cullFace; }
        set cullFace(c) { this._cullFace = c; }
        get unlit() { return this._unlit; }
        set unlit(v) { this._unlit = v; }

        getExternalResources(resources=[]) {
            function tryadd(texture) {
                if (texture instanceof bg.base.Texture &&
                    texture.fileName && 
                    texture.fileName!="" &&
                    resources.indexOf(texture.fileName)==-1)
                {
                    resources.push(texture.fileName);
                }
            }
            tryadd(this.diffuse);
            tryadd(this.metallic);
            tryadd(this.roughness);
            tryadd(this.lightEmission);
            tryadd(this.height);
            tryadd(this.normal);
            return resources;
        }

        copyMaterialSettings(mat,mask) {
            if (mask & bg.base.PBRMaterialFlag.DIFFUSE) {                  // diffuse, isTransparent, alphaCutoff
                mat.diffuse = this.diffuse;
                mat.alphaCutoff = this.alphaCutoff;
                mat.isTransparent = this.isTransparent;
            }   
            if (mask & bg.base.PBRMaterialFlag.DIFFUSE_SCALE) {            // diffuseScale
                mat.diffuseScale = this.diffuseScale;
            }   
            if (mask & bg.base.PBRMaterialFlag.DIFFUSE_OFFSET) {           // diffuseOffset
                mat.diffuseOffset = this.diffuseOffset;
            }   
            if (mask & bg.base.PBRMaterialFlag.METALLIC) {                 // metallic, metallicChannel
                mat.metallic = this.metallic;
                mat.metallicChannel = this.metallicChannel;
            }   
            if (mask & bg.base.PBRMaterialFlag.ROUGHNESS) {                // roughness, roughnessChannel
                mat.roughness = this.roughness;
                mat.roughnessChannel = this.roughnessChannel;
            }   
            if (mask & bg.base.PBRMaterialFlag.LIGHT_EMISSION) {           // lightEmission, lightEmissionChannel
                mat.lightEmission = this.lightEmission;
                mat.lightEmissionChannel = this.lightEmissionChannel;
            }   
            if (mask & bg.base.PBRMaterialFlag.HEIGHT) {                   // height, heightChannel
                mat.height = this.height;
                mat.heightChannel = this.heightChannel;
            }   
            if (mask & bg.base.PBRMaterialFlag.HEIGHT_SCALE) {             // heightScale
                mat.heightScale = this.heightScale;
            }   
            if (mask & bg.base.PBRMaterialFlag.NORMAL) {                   // normal
                mat.normal = this.normal;
            }   
            if (mask & bg.base.PBRMaterialFlag.NORMAL_SCALE) {             // normalScale
                mat.normalScale = this.normalScale;
            }   
            if (mask & bg.base.PBRMaterialFlag.NORMAL_OFFSET) {            // normalOffset
                mat.normalOffset = this.normalOffset;
            }   
            if (mask & bg.base.PBRMaterialFlag.LIGHT_MAP) {                // not used

            }   
            if (mask & bg.base.PBRMaterialFlag.SHADOWS) {                  // castShadows/receiveShadows
                mat.castShadows = this.castShadows;
            }   
            if (mask & bg.base.PBRMaterialFlag.CULL_FACE) {                // cullFace
                mat.cullFace = this.cullFace;
            }   
            if (mask & bg.base.PBRMaterialFlag.UNLIT) {                    // unlit
                mat.unlit = this.unlit;
            }   
        }

        applyModifier(context,mod,resourcePath) {
            if (mod.isEnabled(bg.base.PBRMaterialFlag.DIFFUSE_SCALE)) {    // diffuseScale
                this.diffuseScale = mod.diffuseScale;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.DIFFUSE_OFFSET)) {   // diffuseOffset
                this.diffuseOffset = mod.diffuseOffset;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.HEIGHT_SCALE)) {     // heightScale
                this.heightScale = mod.heightScale;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.NORMAL_SCALE)) {     // normalScale
                this.normalScale = mod.normalScale;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.NORMAL_OFFSET)) {    // normalOffset
                this.normalOffset = mod.normalOffset;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.SHADOWS)) {          // castShadows/receiveShadows
                this.castShadows = mod.castShadows;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.CULL_FACE)) {        // cullFace
                this.cullFace = mod.cullFace;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.UNLIT)) {            // unlit
                this.unlit = mod.unlit;
            }
            
            // Texture or scalar/vector
            function getTexture(texturePath) {
                if (!bg.base.imageTools.isAbsolutePath(texturePath)) {
                    texturePath = bg.base.imageTools.mergePath(resourcePath, texturePath);
                }

                let result = bg.base.TextureCache.Get(context).find(texturePath);
                if (!result) {
                    result = new bg.base.Texture(context);
                    result.create();
                    result.fileName = texturePath;
                    bg.base.TextureCache.Get(context).register(texturePath,result);

                    (function(p,t) {
                        bg.utils.Resource.Load(p)
                            .then((imgData) => {
                                t.bind();
                                t.minFilter = bg.base.TextureLoaderPlugin.GetMinFilter();
                                t.magFilter = bg.base.TextureLoaderPlugin.GetMagFilter();
                                t.fileName = p;
                                t.setImage(imgData);
                            });
                    })(texturePath,result);
                }
            }

            function textureOrScalar(value) {
                if (typeof(value)=="string" && value!=="") {
                    // texture
                    return getTexture(value);
                }
                else if (!isNaN(value)) {
                    // scalar
                    return value;
                }
                else {
                    throw new Error("Invalid parameter: expecting texture path or scalar");
                }
            }
            function textureOrColor(value) {
                if (typeof(value)=="string" && value!=="") {
                    // texture
                    return getTexture(value);
                }
                else if (value instanceof bg.Color) {
                    // color
                    return value;
                }
                else {
                    throw new Error("Invalid parameter: expecting texture path or color");
                }
            }

            if (mod.isEnabled(bg.base.PBRMaterialFlag.DIFFUSE)) {          // diffuse, isTransparent, alphaCutoff
                this.diffuse = textureOrColor(mod.diffuse);
                this.isTransparent = mod.isTransparent;
                this.alphaCutoff = mod.alphaCutoff;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.METALLIC)) {         // metallic, metallicChannel
                this.metallic = textureOrScalar(mod.metallic);
                this.metallicChannel = mod.metallicChannel;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.ROUGHNESS)) {        // roughness, roughnessChannel
                this.roughness = textureOrScalar(mod.roughness);
                this.roughnessChannel = mod.roughnessChannel;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.LIGHT_EMISSION)) {   // lightEmission, lightEmissionChannel
                this.lightEmission = textureOrScalar(mod.lightEmission);
                this.lightEmissionChannel = mod.lightEmissionChannel;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.HEIGHT)) {           // height, heightChannel
                this.height = textureOrScalar(mod.height);
                this.heightChannel = mod.heightChannel;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.NORMAL)) {           // normal
                this.normal = textureOrColor(mod.normal);
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.LIGHT_MAP)) {        // not used
            }
        }

        getModifierWithMask(modifierMask) {
            let mod = new PBRMaterialModifier();
            mod.modifierMask = modifierMask;

            function colorOrTexturePath(paramName) {
                let data = this[paramName];
                if (data instanceof bg.base.Texture) {
                    return data.fileName;
                }
                else {
                    return data;
                }
            }

            function scalarOrTexturePath(paramName) {
                let data = this[paramName];
                if (data instanceof bg.base.Texture) {
                    return data.fileName;
                }
                else {
                    return data;
                }
            }

            if (mod.isEnabled(bg.base.PBRMaterialFlag.DIFFUSE)) {
                mod.diffuse = colorOrTexturePath.apply(this,["diffuse"]);
                mod.isTransparent = this.isTransparent;
                mod.alphaCutoff = this.alphaCutoff;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.DIFFUSE_SCALE)) {
                mod.diffuseScale = this.diffuseScale;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.DIFFUSE_OFFSET)) {
                mod.diffuseOffset = this.diffuseOffset;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.METALLIC)) {
                mod.metallic = scalarOrTexturePath.apply(this,["metallic"]);
                mod.metallicChannel = this.metallicChannel;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.ROUGHNESS)) {
                mod.roghness = scalarOrTexturePath.apply(this,["roughness"]);
                mod.roughnessChannel = this.roughnessChannel;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.LIGHT_EMISSION)) {
                mod.lightEmission = scalarOrTexturePath.apply(this,["lightEmission"]);
                mod.lightEmissionChannel = this.lightEmissionChannel;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.HEIGHT)) {
                mod.height = scalarOrTexturePath.apply(this,["height"]);
                mod.heightChannel = this.heightChannel;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.HEIGHT_SCALE)) {
                mod.heightScale = this.heightScale;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.NORMAL)) {
                mod.normal = colorOrTexturePath.apply(this,["normal"]);
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.NORMAL_SCALE)) {
                mod.normalScale = this.normalScale;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.NORMAL_OFFSET)) {
                mod.normalOffset = this.normalOffset;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.LIGHT_MAP)) {
                // not used
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.SHADOWS)) {
                mod.castShadows = this.castShadows;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.CULL_FACE)) {
                mod.cullFace = this.cullFace;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.UNLIT)) {
                mod.unlit = this.unlit;
            }

            return mod;
        }

        static GetMaterialWithJson(context,data,path) {
            return PBRMaterial.FromMaterialDefinition(context,data,path);            
        }
    }

    bg.base.PBRMaterial = PBRMaterial;

})();
(function() {
	
	bg.base.ClearBuffers = {
		COLOR:null,
		DEPTH:null,
		COLOR_DEPTH:null
	};
	
	bg.base.BlendMode = {
		NORMAL: 1,			// It works as the Photoshop layers does	GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA, GL_FUNC_ADD
		MULTIPLY: 2,		// GL_ZERO GL_SRC_COLOR, GL_FUNC_ADD
		ADD: 3,				// GL_ONE GL_ONE, GL_FUNC_ADD
		SUBTRACT: 4,		// GL_ONE GL_ONE, GL_FUNC_SUBTRACT
		ALPHA_ADD: 5,		// GL_SRC_ALPHA GL_DST_ALPHA, GL_FUNC_ADD
		ALPHA_SUBTRACT: 6	// GL_SRC_ALPHA GL_DST_ALPHA, GL_FUNC_SUBTRACT
	};

	bg.base.OpacityLayer = {
		TRANSPARENT: 0b1,
		OPAQUE: 0b10,
		GIZMOS: 0b100,
		SELECTION: 0b1000,
		GIZMOS_SELECTION: 0b10000,
		ALL: 0b1111,
		NONE: 0
	};

	class PipelineImpl {
		constructor(context) {
			this.initFlags(context);
			bg.base.ClearBuffers.COLOR_DEPTH = bg.base.ClearBuffers.COLOR | bg.base.ClearBuffers.DEPTH;
		}
		
		initFlags(context) {}
		
		setViewport(context,vp) {}
		clearBuffers(context,color,buffers) {}
		setDepthTestEnabled(context,e) {}
		setBlendEnabled(context,e) {}
		setBlendMode(context,m) {}
		setCullFace(context,e) {}
	}
	
	bg.base.PipelineImpl = PipelineImpl;

	let s_currentPipeline = null;
	
	function enablePipeline(pipeline) {
		if (pipeline._effect) {
			pipeline._effect.setActive();
		}
		pipeline.renderSurface.setActive();
		
		bg.Engine.Get().pipeline.setViewport(pipeline.context,pipeline._viewport);
		bg.Engine.Get().pipeline.setDepthTestEnabled(pipeline.context,pipeline._depthTest);
		bg.Engine.Get().pipeline.setCullFace(pipeline.context,pipeline._cullFace);
		bg.Engine.Get().pipeline.setBlendEnabled(pipeline.context,pipeline._blend);
		bg.Engine.Get().pipeline.setBlendMode(pipeline.context,pipeline._blendMode);
	}
	
	class Pipeline extends bg.app.ContextObject {
		static SetCurrent(p) {
			s_currentPipeline = p;
			if (s_currentPipeline) {
				enablePipeline(s_currentPipeline);
			}
		}
		
		static Current() { return s_currentPipeline; }
		
		constructor(context) {
			super(context);

			// Opacity layer
			this._opacityLayer = bg.base.OpacityLayer.ALL;
	
			// Viewport
			this._viewport = new bg.Viewport(0,0,200,200);
			
			// Clear buffer
			this._clearColor = bg.Color.Black();
			
			// Effect (used with draw method)
			this._effect = null;
			
			// Texture effect (used with drawTexture method)
			this._textureEffect = null;
			
			
			// Other flags
			this._depthTest = true;
			this._cullFace = true;
			
			// Render surface
			this._renderSurface = null;
			
			// Blending
			this._blend = false;
			this._blendMode = bg.base.BlendMode.NORMAL;

			this._buffersToClear = bg.base.ClearBuffers.COLOR_DEPTH;
		}
		
		get isCurrent() { return s_currentPipeline==this; }
		
		set opacityLayer(l) { this._opacityLayer = l; }
		get opacityLayer() { return this._opacityLayer; }
		shouldDraw(material) {
			return material &&
				   ((material.isTransparent && (this._opacityLayer & bg.base.OpacityLayer.TRANSPARENT)!=0) ||
				   (!material.isTransparent && (this._opacityLayer & bg.base.OpacityLayer.OPAQUE)!=0));
		}

		get effect() { return this._effect; }
		set effect(m) {
			this._effect = m;
			if (this._effect && this.isCurrent) {
				this._effect.setActive();
			}
		}
		
		get textureEffect() {
			if (!this._textureEffect) {
				this._textureEffect = new bg.base.DrawTextureEffect(this.context);
			}
			return this._textureEffect;
		}
		
		set textureEffect(t) {
			this._textureEffect = t;
		}

		set buffersToClear(b) { this._buffersToClear = b; }
		get buffersToClear() { return this._buffersToClear; }

		get renderSurface() {
			if (!this._renderSurface) {
				this._renderSurface = new bg.base.ColorSurface(this.context);
				this._renderSurface.setActive();
			}
			return this._renderSurface;
		}

		set renderSurface(r) {
			this._renderSurface = r;
			if (this.isCurrent) {
				this._renderSurface.setActive();
			}
		}
		 
		draw(polyList) {
			if (this._effect && polyList && this.isCurrent) {
				let cf = this.cullFace;
				this._effect.bindPolyList(polyList);
				if (this._effect.material) {
					
					this.cullFace = this._effect.material.cullFace;
				}
				polyList.draw();
				this._effect.unbind();
				this.cullFace = cf;
			}
		}
		
		drawTexture(texture) {
			let depthTest = this.depthTest;
			this.depthTest = false;
			this.textureEffect.drawSurface(texture);
			this.depthTest = depthTest;
		}
		
		get blend() { return this._blend; }
		set blend(b) {
			this._blend = b;
			if (this.isCurrent) {
				bg.Engine.Get().pipeline.setBlendEnabled(this.context,this._blend);
			}
		}
		
		get blendMode() { return this._blendMode; }
		set blendMode(b) {
			this._blendMode = b;
			if (this.isCurrent) {
				bg.Engine.Get().pipeline.setBlendMode(this.context,this._blendMode);
			}
		}
		
		get viewport() { return this._viewport; }
		set viewport(vp) {
			this._viewport = vp;
			if (this.renderSurface.resizeOnViewportChanged) {
				this.renderSurface.size = new bg.Vector2(vp.width,vp.height);
			}
			if (this.isCurrent) {
				bg.Engine.Get().pipeline.setViewport(this.context,this._viewport);
			}
		}
		
		clearBuffers(buffers) {
			if (this.isCurrent) {
				buffers = buffers!==undefined ? buffers : this._buffersToClear;
				bg.Engine.Get().pipeline.clearBuffers(this.context,this._clearColor,buffers);
			}
		}
		
		get clearColor() { return this._clearColor; }
		set clearColor(c) { this._clearColor = c; }
		
		get depthTest() { return this._depthTest; }
		set depthTest(e) {
			this._depthTest = e;
			if (this.isCurrent) {
				bg.Engine.Get().pipeline.setDepthTestEnabled(this.context,this._depthTest);
			}
		}
		
		get cullFace() { return this._cullFace; }
		set cullFace(c) {
			this._cullFace = c;
			if (this.isCurrent) {
				bg.Engine.Get().pipeline.setCullFace(this.context,this._cullFace);
			}
		}
	}
	
	bg.base.Pipeline = Pipeline;
})();
(function() {
	
	bg.base.BufferType = {
		VERTEX:		1 << 0,
		NORMAL:		1 << 1,
		TEX_COORD_0:		1 << 2,
		TEX_COORD_1:		1 << 3,
		TEX_COORD_2:		1 << 4,
		COLOR:		1 << 5,
		TANGENT:	1 << 6,
		INDEX:		1 << 7
	};
	
	class PolyListImpl {
		constructor(context) {
			this.initFlags(context);
		}
		
		initFlags(context) {}
		create(context) {}
		build(context,plist,vert,norm,t0,t1,t2,col,tan,index) { return false; }
		draw(context,plist,drawMode,numberOfIndex) {}
		destroy(context,plist) {}

		// NOTE: the new buffer data must have the same size as the old one
		update(context,plist,bufferType,newData) {}
	}

	function createTangents(plist) {
		if (!plist.texCoord0 || !plist.vertex) return;
		plist._tangent = [];
		
		let result = [];
		let generatedIndexes = {};
		let invalidUV = false;
		if (plist.index.length%3==0) {
			for (let i=0; i<plist.index.length - 2; i+=3) {
				let v0i = plist.index[i] * 3;
				let v1i = plist.index[i + 1] * 3;
				let v2i = plist.index[i + 2] * 3;
				
				let t0i = plist.index[i] * 2;
				let t1i = plist.index[i + 1] * 2;
				let t2i = plist.index[i + 2] * 2;
				
				let v0 = new bg.Vector3(plist.vertex[v0i], plist.vertex[v0i + 1], plist.vertex[v0i + 2]);
				let v1 = new bg.Vector3(plist.vertex[v1i], plist.vertex[v1i + 1], plist.vertex[v1i + 2]);
				let v2 = new bg.Vector3(plist.vertex[v2i], plist.vertex[v2i + 1], plist.vertex[v2i + 2]);
				
				let t0 = new bg.Vector2(plist.texCoord0[t0i], plist.texCoord0[t0i + 1]);
				let t1 = new bg.Vector2(plist.texCoord0[t1i], plist.texCoord0[t1i + 1]);
				let t2 = new bg.Vector2(plist.texCoord0[t2i], plist.texCoord0[t2i + 1]);
				
				let edge1 = (new bg.Vector3(v1)).sub(v0);
				let edge2 = (new bg.Vector3(v2)).sub(v0);
				
				let deltaU1 = t1.x - t0.x;
				let deltaV1 = t1.y - t0.y;
				let deltaU2 = t2.x - t0.x;
				let deltaV2 = t2.y - t0.y;
				
				let den = (deltaU1 * deltaV2 - deltaU2 * deltaV1);
				let tangent = null;
				if (den==0) {
					let n = new bg.Vector3(plist.normal[v0i], plist.normal[v0i + 1], plist.normal[v0i + 2]);

					invalidUV = true;
					tangent = new bg.Vector3(n.y, n.z, n.x);
				}
				else {
					let f = 1 / den;
				
					tangent = new bg.Vector3(f * (deltaV2 * edge1.x - deltaV1 * edge2.x),
												   f * (deltaV2 * edge1.y - deltaV1 * edge2.y),
												   f * (deltaV2 * edge1.z - deltaV1 * edge2.z));
					tangent.normalize();
				}
				
				if (generatedIndexes[v0i]===undefined) {
					result.push(tangent.x);
					result.push(tangent.y);
					result.push(tangent.z);
					generatedIndexes[v0i] = tangent;
				}
				
				if (generatedIndexes[v1i]===undefined) {
					result.push(tangent.x);
					result.push(tangent.y);
					result.push(tangent.z);
					generatedIndexes[v1i] = tangent;
				}
				
				if (generatedIndexes[v2i]===undefined) {
					result.push(tangent.x);
					result.push(tangent.y);
					result.push(tangent.z);
					generatedIndexes[v2i] = tangent;
				}
			}
		}
		else {	// other draw modes: lines, line_strip
			for (let i=0; i<plist.vertex.length; i+=3) {
				plist._tangent.push(0,0,1);
			}
		}
		if (invalidUV) {
			console.warn("Invalid UV texture coords found. Some objects may present artifacts in the lighting, and not display textures properly.")
		}
		return result;
	}
	
	bg.base.PolyListImpl = PolyListImpl;
	
	bg.base.DrawMode = {
		TRIANGLES: null,
		TRIANGLE_FAN: null,
		TRIANGLE_STRIP: null,
		LINES: null,
		LINE_STRIP: null
	};
	
	class PolyList extends bg.app.ContextObject {
		constructor(context) {
			super(context);
			
			this._plist = null;
			
			this._drawMode = bg.base.DrawMode.TRIANGLES;
			
			this._name = "";
			this._groupName = "";
			this._visible = true;
			this._visibleToShadows = true;
			
			this._vertex = [];
			this._normal = [];
			this._texCoord0 = [];
			this._texCoord1 = [];
			this._texCoord2 = [];
			this._color = [];
			this._tangent = [];
			this._index = [];
		}
		
		clone() {
			let pl2 = new PolyList(this.context);
			
			let copy = function(src,dst) {
				src.forEach(function(item) {
					dst.push(item);
				});
			};
			
			pl2.name = this.name + " clone";
			pl2.groupName = this.groupName;
			pl2.visible = this.visible;
			pl2.visibleToShadows = this.visibleToShadows;
			pl2.drawMode = this.drawMode;
			
			copy(this.vertex,pl2.vertex);
			copy(this.normal,pl2.normal);
			copy(this.texCoord0,pl2.texCoord0);
			copy(this.texCoord1,pl2.texCoord1);
			copy(this.texCoord2,pl2.texCoord02);
			copy(this.color,pl2.color);
			copy(this.index,pl2.index);
			pl2.build();
			
			return pl2;
		}
		
		get name() { return this._name; }
		set name(n) { this._name = n; }
		
		get groupName() { return this._groupName; }
		set groupName(n) { this._groupName = n; }
		
		get visible() { return this._visible; }
		set visible(v) { this._visible = v; }

		get visibleToShadows() { return this._visibleToShadows; }
		set visibleToShadows(v) { this._visibleToShadows = v; }
		
		get drawMode() { return this._drawMode; }
		set drawMode(m) { this._drawMode = m; }
		
		set vertex(v) { this._vertex = v; }
		set normal(n) { this._normal = n; }
		set texCoord0(t) { this._texCoord0 = t; }
		set texCoord1(t) { this._texCoord1 = t; }
		set texCoord2(t) { this._texCoord2 = t; }
		set color(c) { this._color = c; }
		//  Tangent buffer is calculated from normal buffer
		set index(i) { this._index = i; }
		
		get vertex() { return this._vertex; }
		get normal() { return this._normal; }
		get texCoord0() { return this._texCoord0; }
		get texCoord1() { return this._texCoord1; }
		get texCoord2() { return this._texCoord2; }
		get color() { return this._color; }
		get tangent() { return this._tangent; }
		get index() { return this._index; }
		
		get vertexBuffer() { return this._plist.vertexBuffer; }
		get normalBuffer() { return this._plist.normalBuffer; }
		get texCoord0Buffer() { return this._plist.tex0Buffer; }
		get texCoord1Buffer() { return this._plist.tex1Buffer; }
		get texCoord2Buffer() { return this._plist.tex2Buffer; }
		get colorBuffer() { return this._plist.colorBuffer; }
		get tangentBuffer() { return this._plist.tangentBuffer; }
		get indexBuffer() { return this._plist.indexBuffer; }

		// Note that the new buffer must have the same size as the old one, this
		// function is intended to be used only to replace the buffer values
		updateBuffer(bufferType,newData) {
			let status = false;
			switch (bufferType) {
			case bg.base.BufferType.VERTEX:
				status = this.vertex.length==newData.length;
				break;
			case bg.base.BufferType.NORMAL:
				status = this.normal.length==newData.length;
				break;
			case bg.base.BufferType.TEX_COORD_0:
				status = this.texCoord0.length==newData.length;
				break;
			case bg.base.BufferType.TEX_COORD_1:
				status = this.texCoord1.length==newData.length;
				break;
			case bg.base.BufferType.TEX_COORD_2:
				status = this.texCoord2.length==newData.length;
				break;
			case bg.base.BufferType.COLOR:
				status = this.color.length==newData.length;
				break;
			case bg.base.BufferType.TANGENT:
				status = this.tangent.length==newData.length;
				break;
			case bg.base.BufferType.INDEX:
				status = this.index.length==newData.length;
				break;
			}
			if (!status) {
				throw new Error("Error updating buffer: The new buffer have different size as the old one.");
			}
			else {
				bg.Engine.Get().polyList.update(this.context,this._plist,bufferType,newData);
			}
		}
		
		build() {
			if (this.color.length==0) {
				// Ensure that the poly list have a color buffer
				for (let i = 0; i<this.vertex.length; i+=3) {
					this.color.push(1);
					this.color.push(1);
					this.color.push(1);
					this.color.push(1);
				}
			}

			let plistImpl = bg.Engine.Get().polyList;
			if (this._plist) {
				plistImpl.destroy(this.context, this._plist);
				this._tangent = [];
			}
			this._tangent = createTangents(this);
			this._plist = plistImpl.create(this.context);
			return plistImpl.build(this.context, this._plist,
							this._vertex,
							this._normal,
							this._texCoord0,
							this._texCoord1,
							this._texCoord2,
							this._color,
							this._tangent,
							this._index);
		}
		
		draw() {
			bg.Engine.Get().polyList
				.draw(this.context,this._plist,this.drawMode,this.index.length);
		}
		
		destroy() {
			if (this._plist) {
				bg.Engine.Get().polyList
					.destroy(this.context, this._plist);
			}
			
			this._plist = null;
			
			this._name = "";
			this._vertex = [];
			this._normal = [];
			this._texCoord0 = [];
			this._texCoord1 = [];
			this._texCoord2 = [];
			this._color = [];
			this._tangent = [];
			this._index = [];
		}
		
		applyTransform(trx) {
			var transform = new bg.Matrix4(trx);
			var rotation = new bg.Matrix4(trx.getMatrix3());

			if (this.normal.length>0 && this.normal.length!=this.vertex.length)
				throw new Error("Unexpected number of normal coordinates found in polyList");
	
			for (let i=0;i<this.vertex.length-2;i+=3) {
				let vertex = new bg.Vector4(this.vertex[i],this.vertex[i+1], this.vertex[i+2], 1.0);
				vertex = transform.multVector(vertex);
				this.vertex[i] = vertex.x;
				this.vertex[i+1] = vertex.y;
				this.vertex[i+2] = vertex.z;
		
				if (this.normal.length) {
					var normal = new bg.Vector4(this.normal[i],this.normal[i+1], this.normal[i+2], 1.0);
					normal = rotation.multVector(normal);
					this.normal[i] = normal.x;
					this.normal[i+1] = normal.y;
					this.normal[i+2] = normal.z;
				}
			}
			this.build();
		}
		
		
	};
	
	bg.base.PolyList = PolyList;
	
})();
(function() {
	let shaders = {};
	
	function lib() {
		return bg.base.ShaderLibrary.Get();
	}
	
	let s_vertexSource = null;
	let s_fragmentSource = null;
	
	function vertexShaderSource() {
		if (!s_vertexSource) {
			s_vertexSource = new bg.base.ShaderSource(bg.base.ShaderType.VERTEX);
			
			s_vertexSource.addParameter([
				lib().inputs.buffers.vertex
			]);
			
			s_vertexSource.addParameter(lib().inputs.matrix.all);
			
			if (bg.Engine.Get().id=="webgl1") {
				s_vertexSource.setMainBody(`
					gl_Position = inProjectionMatrix * inViewMatrix * inModelMatrix * vec4(inVertex,1.0);
				`);
			}
		}
		return s_vertexSource;
	}
	
	function fragmentShaderSource() {
		if (!s_fragmentSource) {
			s_fragmentSource = new bg.base.ShaderSource(bg.base.ShaderType.FRAGMENT);
		
			if (bg.Engine.Get().id=="webgl1") {	
				s_fragmentSource.setMainBody(`
					gl_FragColor = vec4(1.0,0.0,0.0,1.0);
				`);
			}
		}
		return s_fragmentSource;
	}
	
	class RedEffect extends bg.base.Effect {
		constructor(context) { 
			super(context);			
			let sources = [
				vertexShaderSource(),
				fragmentShaderSource()
			];
			this.setupShaderSource(sources);
		}
		
		beginDraw() {
		}
		
		setupVars() {
			let matrixState = bg.base.MatrixState.Current();
			let viewMatrix = new bg.Matrix4(matrixState.viewMatrixStack.matrixConst);
			this.shader.setMatrix4('inModelMatrix',matrixState.modelMatrixStack.matrixConst);
			this.shader.setMatrix4('inViewMatrix',viewMatrix);
			this.shader.setMatrix4('inProjectionMatrix',matrixState.projectionMatrixStack.matrixConst);
		}
		
	}
	
	bg.base.RedEffect = RedEffect;
})();
(function() {

    class RenderQueue {
        constructor() {
            this._opaqueQueue = [];
            this._transparentQueue = [];
            this._worldCameraPosition = new bg.Vector3(0);
        }

        beginFrame(worldCameraPosition) {
            this._opaqueQueue = [];
            this._transparentQueue = [];
            this._worldCameraPosition.assign(worldCameraPosition);
        }

        renderOpaque(plist, mat, trx, viewMatrix) {
            this._opaqueQueue.push({
                plist:plist,
                material:mat,
                modelMatrix:new bg.Matrix4(trx),
                viewMatrix: new bg.Matrix4(viewMatrix)
            });
        }

        renderTransparent(plist, mat, trx, viewMatrix) {
            let pos = trx.position;
            pos.sub(this._worldCameraPosition);
            this._opaqueQueue.push({
                plist:plist,
                material:mat,
                modelMatrix:new bg.Matrix4(trx),
                viewMatrix: new bg.Matrix4(viewMatrix),
                cameraDistance: pos.magnitude()
            });
        }

        sortTransparentObjects() {
            this._transparentQueue.sort((a,b) => {
                return a.cameraDistance > b.cameraDistance;
            });
        }

        get opaqueQueue() {
            return this._opaqueQueue;
        }

        get transparentQueue() {
            return this._transparentQueue;
        }


    }

    bg.base.RenderQueue = RenderQueue;
})();
(function() {
	
	class RenderSurfaceBufferImpl {
		constructor(context) {
			this.initFlags(context);
		}
		
		initFlags(context) {}
		
		create(context,attachments) {}
		setActive(context,renderSurface) {}
		readBuffer(context,renderSurface,rectangle) {}
		resize(context,renderSurface,size) {}
		destroy(context,renderSurface) {}
		supportType(type) {}
		supportFormat(format) {}
		maxColorAttachments() {}
	}

	bg.base.RenderSurfaceBufferImpl = RenderSurfaceBufferImpl;
	
	class RenderSurface extends bg.app.ContextObject {
		static DefaultAttachments() {
			return [
				{ type:bg.base.RenderSurfaceType.RGBA, format:bg.base.RenderSurfaceFormat.UNSIGNED_BYTE },
				{ type:bg.base.RenderSurfaceType.DEPTH, format:bg.base.RenderSurfaceFormat.RENDERBUFFER }
			];
		}
		static SupportFormat(format) {
			return bg.Engine.Get().colorBuffer.supportFormat(format);
		}
		
		static SupportType(type) {
			return bg.Engine.Get().colorBuffer.supportType(type);
		}
		
		static MaxColorAttachments() {
			return bg.Engine.Get().textureBuffer.maxColorAttachments;
		}
		
		constructor(context) {
			super(context);
			
			this._size = new bg.Vector2(256);
			this._renderSurface = null;
			this._resizeOnViewportChanged = true;
		}
		
		get size() { return this._size; }
		set size(s) {
			if (this._size.x!=s.x || this._size.y!=s.y) {
				this._size = s;
				this.surfaceImpl.resize(this.context,this._renderSurface,s);
			}
		}
		
		get surfaceImpl() { return null; }

		get resizeOnViewportChanged() { return this._resizeOnViewportChanged; }
		set resizeOnViewportChanged(r) { this._resizeOnViewportChanged = r; }
		
		create(attachments) {
			if (!attachments) {
				attachments = RenderSurface.DefaultAttachments();
			}
			this._renderSurface = this.surfaceImpl.create(this.context,attachments);
		}
		
		setActive() {
			this.surfaceImpl.setActive(this.context,this._renderSurface);
		}
		
		readBuffer(rectangle) {
			return this.surfaceImpl.readBuffer(this.context,this._renderSurface,rectangle,this.size);
		}
		
		destroy() {
			this.surfaceImpl.destroy(this.context,this._renderSurface);
			this._renderSurface = null;
		}
	}
	
	bg.base.RenderSurface = RenderSurface;
	
	bg.base.RenderSurfaceType = {
		RGBA:null,
		DEPTH:null
	};
	
	bg.base.RenderSurfaceFormat = {
		UNSIGNED_BYTE:null,
		UNSIGNED_SHORT:null,
		FLOAT:null,
		RENDERBUFFER:null
	};
	
	class ColorSurface extends RenderSurface {
		static MaxColorAttachments() {
			return bg.Engine.Get().colorBuffer.maxColorAttachments;
		}
		
		get surfaceImpl() { return bg.Engine.Get().colorBuffer; }
	}
	
	bg.base.ColorSurface = ColorSurface;
	
	class TextureSurface extends RenderSurface {
		static MaxColorAttachments() {
			return bg.Engine.Get().textureBuffer.maxColorAttachments;
		}
		
		get surfaceImpl() { return bg.Engine.Get().textureBuffer; }
		
		getTexture(attachment = 0) {
			return  this._renderSurface.attachments[attachment] &&
					this._renderSurface.attachments[attachment].texture;
		}
	}
	
	bg.base.TextureSurface = TextureSurface;
	
})();

(function() {
    if (!bg.isElectronApp) {
        return false;
    }

    const fs = require('fs');
    const path = require('path');

    class SaveSceneHelper {
        save(filePath,sceneRoot) {
            filePath = bg.base.Writer.StandarizePath(filePath);
            return new Promise((resolve,reject) => {
                this._url = {};
                this._url.path = filePath.split('/');
                this._url.fileName = this._url.path.pop();
                this._url.path = this._url.path.join('/');
                this._sceneData = {
                    fileType:"vwgl::scene",
                    version:{
                        major:2,
                        minor:0,
                        rev:0
                    },
                    scene:[]
                }
                this._promises = [];
                bg.base.Writer.PrepareDirectory(this._url.path);

                let rootNode = {};
                this._sceneData.scene.push(rootNode);
                this.buildSceneNode(sceneRoot,rootNode);

                fs.writeFileSync(path.join(this._url.path,this._url.fileName),JSON.stringify(this._sceneData,"","\t"),"utf-8");

                Promise.all(this._promises)
                    .then(() => resolve())
                    .catch((err) => reject(err));
            });
        }

        buildSceneNode(node,sceneData) {
            sceneData.type = "Node";
            sceneData.name = node.name;
            sceneData.enabled = node.enabled;
            sceneData.steady = node.steady;
            sceneData.children = [];
            sceneData.components = [];
            node.forEachComponent((component) => {
                if (component.shouldSerialize) {
                    let componentData = {};
                    component.serialize(componentData,this._promises,this._url);
                    sceneData.components.push(componentData)
                }
            });
            node.children.forEach((child) => {
                let childData = {}
                this.buildSceneNode(child,childData);
                sceneData.children.push(childData);
            })
        }
    };

    class SceneWriterPlugin extends bg.base.WriterPlugin {
        acceptType(url,data) {
            let ext = url.split(".").pop(".");
            return /vitscnj/i.test(ext) && data instanceof bg.scene.Node;
        }

        write(url,data) {
            let saveSceneHelper = new SaveSceneHelper();
            return saveSceneHelper.save(url,data);
        }
    }

    bg.base.SceneWriterPlugin = SceneWriterPlugin;
})();
(function() {
	
	let s_shaderLibrary = null;
	
	function defineAll(obj) {
		Reflect.defineProperty(obj,"all", {
			get() {
				if (!this._all) {
					this._all = [];
					for (let key in obj) {
						if (typeof(obj[key])=="object" && obj[key].name) {
							this._all.push(obj[key]);
						}
					}
				}
				return this._all;
			}
		});
	}
	
	class ShaderLibrary {
		static Get() {
			if (!s_shaderLibrary) {
				s_shaderLibrary = new ShaderLibrary();
			}
			return s_shaderLibrary;
		}
		
		constructor() {
			let library = bg[bg.Engine.Get().id].shaderLibrary;
			
			for (let key in library) {
				this[key] = library[key];
			}
			
			defineAll(this.inputs.matrix);
			Object.defineProperty(this.inputs.matrix,"modelViewProjection",{
				get() {
					return [
						this.model,
						this.view,
						this.projection
					]
				}
			});
			defineAll(this.inputs.material);
            defineAll(this.inputs.lighting);
			defineAll(this.inputs.lightingForward);
			defineAll(this.inputs.shadows);
			defineAll(this.inputs.colorCorrection);
			defineAll(this.functions.materials);
			defineAll(this.functions.colorCorrection);
			defineAll(this.functions.lighting);
			defineAll(this.functions.utils);

			// PBR materials
			for (let key in this.inputs.pbr) {
				defineAll(this.inputs.pbr[key]);
			}
			for (let key in this.functions.pbr) {
				defineAll(this.functions.pbr[key]);
			}
		}
	}
	
	bg.base.ShaderLibrary = ShaderLibrary;
	
	class ShaderSourceImpl {
		header(shaderType) { return ""; }
		parameter(shaderType,paramData) { return paramData.name; }
		func(shaderType,funcData) { return funcData.name; }
	}
	
	bg.base.ShaderSourceImpl = ShaderSourceImpl;
	
	class ShaderSource {
		static FormatSource(src) {
			let result = "";
			let lines = src.replace(/^\n*/,"").replace(/\n*$/,"").split("\n");
			let minTabs = 100;
			lines.forEach((line) => {
				let tabsInLine = /(\t*)/.exec(line)[0].length;
				if (minTabs>tabsInLine) {
					minTabs = tabsInLine;
				}
			});
			
			lines.forEach((line) => {
				let tabsInLine = /(\t*)/.exec(line)[0].length;
				let diff = tabsInLine - minTabs;
				result += line.slice(tabsInLine - diff,line.length) + "\n";
			});
			
			return result.replace(/^\n*/,"").replace(/\n*$/,"");
		}
		
		constructor(type) {
			this._type = type;
			this._params = [];
			this._functions = [];
			this._requiredExtensions = [];
			this._header = "";
			this._preprocessor = "";
		}
		
		get type() { return this._type; }
		get params() { return this._params; }
		get header() { return this._header; }
		get functions() { return this._functions; }
		
		addParameter(param) {
			if (param instanceof Array) {
				this._params = [...this._params, ...param];
			}
			else {
				this._params.push(param);
			}
			this._params.push(null);	// This will be translated into a new line character
		}
		
		addFunction(func) {
			if (func instanceof Array) {
				this._functions = [...this._functions, ...func];
			}
			else {
				this._functions.push(func);
			}
		}
		
		setMainBody(body) {
			this.addFunction({
				returnType:"void", name:"main", params:{}, body:body
			});
		}
				
		appendHeader(src) {
			this._header += src + "\n";
		}

		appendPreprocessor(src) {
			this._preprocessor += src + "\n";
		}
				
		toString() {
			let impl = bg.Engine.Get().shaderSource;
			// Build header
			let src = this._preprocessor;
			src += impl.header(this.type) + "\n" + this._header + "\n\n";
			
			this.params.forEach((p) => {
				src += impl.parameter(this.type,p) + "\n";
			});
			
			this.functions.forEach((f) => {
				src += "\n" + impl.func(this.type,f) + "\n";
			})
			return src;
		}
	}
	
	bg.base.ShaderSource = ShaderSource;
})();
(function() {
	
	class ShaderImpl {
		constructor(context) {
			this.initFlags(context);
		}
		
		initFlags(context) {}
		setActive(context,shaderProgram) {}
		create(context) {}
		addShaderSource(context,shaderProgram,shaderType,source) {}
		link(context,shaderProgram) {}
		initVars(context,shader,inputBufferVars,valueVars) {}
		setInputBuffer(context,shader,varName,vertexBuffer,itemSize) {}
		setValueInt(context,shader,name,v) {}
		setValueIntPtr(context,shader,name,v) {}
		setValueFloat(context,shader,name,v) {}
		setValueFloatPtr(context,shader,name,v) {}
		setValueVector2(context,shader,name,v) {}
		setValueVector3(context,shader,name,v) {}
		setValueVector4(context,shader,name,v) {}
		setValueVector2v(context,shader,name,v) {}
		setValueVector3v(context,shader,name,v) {}
		setValueVector4v(context,shader,name,v) {}
		setValueMatrix3(context,shader,name,traspose,v) {}
		setValueMatrix4(context,shader,name,traspose,v) {}
		setTexture(context,shader,name,texture,textureUnit) {}
	}
	
	bg.base.ShaderImpl = ShaderImpl;
	
	bg.base.ShaderType = {
		VERTEX: null,
		FRAGMENT: null
	};

	function addLineNumbers(source) {
		let result = "";
		source.split("\n").forEach((line,index) => {
			++index;
			let prefix = index<10 ? "00":index<100 ? "0":"";
			result += prefix + index + " | " + line + "\n";
		});
		return result;
	}

	let g_activeShader = null;
	
	class Shader extends bg.app.ContextObject {
		static ClearActive(context) { bg.Engine.Get().shader.setActive(context,null); }
		static GetActiveShader() {
			return g_activeShader;
		}

		constructor(context) {
			super(context);
			
			this._shader = bg.Engine.Get().shader.create(context);
			this._linked = false;
			
			this._compileError = null;
			this._linkError = null;
		}
		
		get shader() { return this._shader; }
		get compileError() { return this._compileError; }
		get compileErrorSource() { return this._compileErrorSource; }
		get linkError() { return this._linkError; }
		get status() { return this._compileError==null && this._linkError==null; }
		
		addShaderSource(shaderType, shaderSource) {
			if (this._linked) {
				this._compileError = "Tying to attach a shader to a linked program";
			}
			else if (!this._compileError) {
				this._compileError = bg.Engine.Get().shader.addShaderSource(
																this.context,
																this._shader,
																shaderType,shaderSource);
				if (this._compileError) {
					this._compileErrorSource = addLineNumbers(shaderSource);
				}
			}
			return this._compileError==null;
		}
		
		link() {
			this._linkError = null;
			if (this._linked) {
				this._linkError = "Shader already linked";
			}
			else  {
				this._linkError = bg.Engine.Get().shader.link(this.context,this._shader);
				this._linked = this._linkError==null;
			}
			return this._linked;
		}
		
		setActive() {
			bg.Engine.Get().shader.setActive(this.context,this._shader);
			g_activeShader = this;
		}
		
		clearActive() {
			Shader.ClearActive(this.context);
			g_activeShader = null;
		}
		
		initVars(inputBufferVars,valueVars) {
			bg.Engine.Get().shader.initVars(this.context,this._shader,inputBufferVars,valueVars);
		}
		
		setInputBuffer(name,vbo,itemSize) {
			bg.Engine.Get().shader
				.setInputBuffer(this.context,this._shader,name,vbo,itemSize);
		}
		
		disableInputBuffer(name) {
			bg.Engine.Get().shader
				.disableInputBuffer(this.context,this._shader,name);
		}
		
		setValueInt(name,v) {
			bg.Engine.Get().shader
				.setValueInt(this.context,this._shader,name,v);
		}

		setValueIntPtr(name,v) {
			bg.Engine.Get().shader
				.setValueIntPtr(this.context,this._shader,name,v);
		}
		
		setValueFloat(name,v) {
			bg.Engine.Get().shader
				.setValueFloat(this.context,this._shader,name,v);
		}

		setValueFloatPtr(name,v) {
			bg.Engine.Get().shader
				.setValueFloatPtr(this.context,this._shader,name,v);
		}
		
		setVector2(name,v) {
			bg.Engine.Get().shader
				.setValueVector2(this.context,this._shader,name,v);
		}
		
		setVector3(name,v) {
			bg.Engine.Get().shader
				.setValueVector3(this.context,this._shader,name,v);			
		}
		
		setVector4(name,v) {
			bg.Engine.Get().shader
				.setValueVector4(this.context,this._shader,name,v);
		}

		setVector2Ptr(name,v) {
			bg.Engine.Get().shader
				.setValueVector2v(this.context,this._shader,name,v);
		}
		
		setVector3Ptr(name,v) {
			bg.Engine.Get().shader
				.setValueVector3v(this.context,this._shader,name,v);			
		}
		
		setVector4Ptr(name,v) {
			bg.Engine.Get().shader
				.setValueVector4v(this.context,this._shader,name,v);
		}
		
		setMatrix3(name,v,traspose=false) {
			bg.Engine.Get().shader
				.setValueMatrix3(this.context,this._shader,name,traspose,v);
		}
		
		setMatrix4(name,v,traspose=false) {
			bg.Engine.Get().shader
				.setValueMatrix4(this.context,this._shader,name,traspose,v);
		}
		
		setTexture(name,texture,textureUnit) {
			bg.Engine.Get().shader
				.setTexture(this.context,this._shader,name,texture,textureUnit);
		}

		
		destroy() {
			console.warn("TODO: Shader.destroy(): not implemented.");
		}
	}
	
	bg.base.Shader = Shader;
})();
(function() {
	
	function lib() {
		return bg.base.ShaderLibrary.Get();
	}
	
	let s_vertexSource = null;
	let s_fragmentSource = null;

	
	function vertexShaderSource() {
		if (!s_vertexSource) {
			s_vertexSource = new bg.base.ShaderSource(bg.base.ShaderType.VERTEX);
			
			s_vertexSource.addParameter([
				lib().inputs.buffers.vertex,
				lib().inputs.buffers.tex0,
				null,
				lib().inputs.matrix.model,
				lib().inputs.matrix.view,
				lib().inputs.matrix.projection,
				null,
				{ name:"fsTexCoord", dataType:"vec2", role:"out" }
			]);
			
			if (bg.Engine.Get().id=="webgl1") {
				s_vertexSource.setMainBody(`
				gl_Position = inProjectionMatrix * inViewMatrix * inModelMatrix * vec4(inVertex,1.0);
				fsTexCoord = inTex0;
				`);
			}
		}
		return s_vertexSource;
	}
	
	function fragmentShaderSource() {
		if (!s_fragmentSource) {
			s_fragmentSource = new bg.base.ShaderSource(bg.base.ShaderType.FRAGMENT);
			
			s_fragmentSource.addParameter([
				lib().inputs.material.castShadows,
				lib().inputs.material.texture,
				lib().inputs.material.textureOffset,
				lib().inputs.material.textureScale,
				lib().inputs.material.alphaCutoff,
				null,
				{ name:"fsTexCoord", dataType:"vec2", role:"in" }
			]);
			
			s_fragmentSource.addFunction(lib().functions.utils.pack);
			s_fragmentSource.addFunction(lib().functions.materials.samplerColor);
			
			if (bg.Engine.Get().id=="webgl1") {
				s_fragmentSource.setMainBody(`
				
				float alpha = samplerColor(inTexture,fsTexCoord,inTextureOffset,inTextureScale).a;
				if (inCastShadows && alpha>inAlphaCutoff) {
					gl_FragColor = pack(gl_FragCoord.z);
				}
				else {
					discard;
				}`);
			}
		}
		return s_fragmentSource;
	}
	
	class ShadowMapEffect extends bg.base.Effect {
		constructor(context) {
			super(context);
			
			this._material = null;
			this._light = null;
			this._lightTransform = null;
			
			this.setupShaderSource([
				vertexShaderSource(),
				fragmentShaderSource()
			]);
		}
		
		get material() { return this._material; }
		set material(m) { this._material = m; }
		
		get light() { return this._light; }
		set light(l) { this._light = l; }
		
		get lightTransform() { return this._lightTransform; }
		set lightTransform(t) { this._lightTransform = t; }
		
		setupVars() {
			if (this.material && this.light && this.lightTransform) {
				let matrixState = bg.base.MatrixState.Current();
				this.shader.setMatrix4("inModelMatrix",matrixState.modelMatrixStack.matrixConst);
				this.shader.setMatrix4("inViewMatrix",this.lightTransform);
				this.shader.setMatrix4("inProjectionMatrix",this.light.projection);
				
				if (this.material instanceof bg.base.Material) {
					this.shader.setValueInt("inCastShadows",this.material.castShadows);
					let texture = this.material.texture || bg.base.TextureCache.WhiteTexture(this.context);
					this.shader.setTexture("inTexture",texture,bg.base.TextureUnit.TEXTURE_0);
					this.shader.setVector2("inTextureOffset",this.material.diffuseOffset || this.material.textureOffset);
					this.shader.setVector2("inTextureScale",this.material.diffuseScale || this.material.textureScale);
					this.shader.setValueFloat("inAlphaCutoff",this.material.alphaCutoff);
				}
				else if (this.material instanceof bg.base.PBRMaterial) {
					this.shader.setValueInt("inCastShadows",this.material.castShadows);
					this.shader.setTexture("inTexture",this.material.getShaderParameters(this.context).diffuse.map,bg.base.TextureUnit.TEXTURE_0);
					this.shader.setVector2("inTextureOffset",this.material.diffuseOffset || this.material.textureOffset);
					this.shader.setVector2("inTextureScale",this.material.diffuseScale || this.material.textureScale);
					this.shader.setValueFloat("inAlphaCutoff",this.material.alphaCutoff);
				}
			}
		}
	}
	
	bg.base.ShadowMapEffect = ShadowMapEffect;
	
	bg.base.ShadowType = {
		HARD: 0,
		SOFT: 1,
		STRATIFIED: 2
	};

	bg.base.ShadowCascade = {
		NEAR: 0,
		FAR: 1,
		MID: 2
	}

	function updateDirectional(scene,camera,light,lightTransform,cascade) {
		let ms = bg.base.MatrixState.Current();
		bg.base.MatrixState.SetCurrent(this._matrixState);
		this._pipeline.effect.light = light;
		this._viewMatrix = new bg.Matrix4(lightTransform);

		let rotation = this._viewMatrix.rotation;
		let cameraTransform = camera.transform ? new bg.Matrix4(camera.transform.matrix) : bg.Matrix4.Identity();
		let cameraPos = cameraTransform.position;
		let target = cameraPos.add(cameraTransform.forwardVector.scale(-camera.focus))
		
		this._viewMatrix
			.identity()
			.translate(target)
			.mult(rotation)
			.translate(0,0,10)
			.invert();

		this._pipeline.effect.lightTransform = this._viewMatrix;
					
		bg.base.Pipeline.SetCurrent(this._pipeline);
		this._pipeline.clearBuffers(bg.base.ClearBuffers.COLOR_DEPTH);

		let mult = 1;
		// Far cascade
		if (cascade==bg.base.ShadowCascade.FAR) {
			mult = 20;
			light.shadowBias = 0.0001;
		}
		// Near cascade
		else if (cascade==bg.base.ShadowCascade.NEAR) {
			mult = 2;
			light.shadowBias = 0.00002;
		}
		else if (cascade==bg.base.ShadowCascade.MID) {
			mult = 4;
			light.shadowBias = 0.0001;
		}
		light.projection = bg.Matrix4.Ortho(-camera.focus * mult ,camera.focus * mult,-camera.focus * mult,camera.focus * mult,1,300*camera.focus);
		this._projection = light.projection;
		scene.accept(this._drawVisitor);

		bg.base.MatrixState.SetCurrent(ms);
	}

	function updateSpot(scene,camera,light,lightTransform) {
		let ms = bg.base.MatrixState.Current();
		bg.base.MatrixState.SetCurrent(this._matrixState);
		this._pipeline.effect.light = light;
		this._viewMatrix = new bg.Matrix4(lightTransform);

		let cutoff = light.spotCutoff;
		light.projection = bg.Matrix4.Perspective(cutoff * 2,1,0.1,200.0);
		light.shadowBias = 0.0005;
		this._viewMatrix.invert();

		this._projection = light.projection;
		this._pipeline.effect.lightTransform = this._viewMatrix;
					
		bg.base.Pipeline.SetCurrent(this._pipeline);
		this._pipeline.clearBuffers(bg.base.ClearBuffers.COLOR_DEPTH);
		
		scene.accept(this._drawVisitor);
		bg.base.MatrixState.SetCurrent(ms);
	}

	class ShadowMap extends bg.app.ContextObject {
		constructor(context) {
			super(context);
			
			this._pipeline = new bg.base.Pipeline(context);
			this._pipeline.renderSurface = new bg.base.TextureSurface(context);
			this._pipeline.renderSurface.create();
			this._pipeline.effect = new bg.base.ShadowMapEffect(context);
		
			this._matrixState = new bg.base.MatrixState();
			this._drawVisitor = new bg.scene.DrawVisitor(this._pipeline,this._matrixState);
			
			this._shadowMapSize = new bg.Vector2(2048);
			this._pipeline.viewport = new bg.Viewport(0,0,this._shadowMapSize.width,this._shadowMapSize.height);
			
			this._shadowType = bg.base.ShadowType.SOFT;
			
			this._projection = bg.Matrix4.Ortho(-15,15,-15,15,1,50);
			this._viewMatrix = bg.Matrix4.Identity();
			
			this._shadowColor = bg.Color.Black();
		}
		
		get size() { return this._shadowMapSize; }
		set size(s) {
			this._shadowMapSize = s;
			this._pipeline.viewport = new bg.Viewport(0,0,s.width,s.height);
		}
		
		get shadowType() { return this._shadowType; }
		set shadowType(t) { this._shadowType = t; }
		
		get shadowColor() { return this._shadowColor; }
		set shadowColor(c) { this._shadowColor = c; }
		
		get viewMatrix() { return this._viewMatrix; }
		get projection() { return this._projection; }
		
		get texture() { return this._pipeline.renderSurface.getTexture(0); }
		
		// it's important that the camera has set the focus to calculate the projection of the directional lights
		update(scene,camera,light,lightTransform,cascade=bg.base.ShadowCascade.NEAR) {
			if (light.type==bg.base.LightType.DIRECTIONAL) {
				updateDirectional.apply(this,[scene,camera,light,lightTransform,cascade]);
			}
			else if (light.type==bg.base.LightType.SPOT) {
				updateSpot.apply(this,[scene,camera,light,lightTransform]);
			}
		}
	}
	
	bg.base.ShadowMap = ShadowMap;
	
})();
(function() {

    class TextProperties {

        constructor() {
            this._font = "Verdana";
            this._size = 30;
            this._color = "#FFFFFF";
            this._background = "transparent";
            this._align = "start";
            this._bold = false;
            this._italic = false;

            this._dirty = true;
        }

        clone() {
            let newInstance = new TextProperties();

            newInstance._font = this._font;
            newInstance._size = this._size;
            newInstance._color = this._color;
            newInstance._background = this._background;
            newInstance._align = this._align;
            newInstance._bold = this._bold;
            newInstance._italic = this._italic;

            return newInstance;
        }

        get font() { return this._font; }
        set font(v) { this._dirty = true; this._font = v; }
        get size() { return this._size; }
        set size(v) { this._dirty = true; this._size = v; }
        get color() { return this._color; }
        set color(v) { this._dirty = true; this._color = v; }
        get background() { return this._background; }
        set background(v) { this._dirty = true; this._background = v; }
        get align() { return this._align; }
        set align(v) { this._dirty = true; this._align = v; }
        get bold() { return this._bold; }
        set bold(v) { this._dirty = true; this._bold = v; }
        get italic() { return this._italic; }
        set italic(v) { this._dirty = true; this._italic = v; }
        

        // this property is set to true every time some property is changed
        set dirty(d) { this._dirty = d; }
        get dirty() { return this._dirty; }

        serialize(jsonData) {
            jsonData.font = this.font;
            jsonData.size = this.size;
            jsonData.color = this.color;
            jsonData.background = this.background;
            jsonData.align = this.align;
            jsonData.bold = this.bold;
            jsonData.italic = this.italic;
        }

        deserialize(jsonData) {
            this.font = jsonData.font;
            this.size = jsonData.size;
            this.color = jsonData.color;
            this.background = jsonData.background;
            this.align = jsonData.align;
            this.bold = jsonData.bold;
            this.italic = jsonData.italic;
            this._dirty = true;
        }
    }

    bg.base.TextProperties = TextProperties;
})();
(function() {
	let s_textureCache = {};
	
	let COLOR_TEXTURE_SIZE = 8;
	
	let s_whiteTexture = "static-white-color-texture";
	let s_blackTexture = "static-black-color-texture";
	let s_normalTexture = "static-normal-color-texture";
	let s_randomTexture = "static-random-color-texture";
	let s_whiteCubemap = "static-white-cubemap-texture";
	let s_blackCubemap = "static-white-cubemap-texture";

	class TextureCache {
		static SetColorTextureSize(size) { COLOR_TEXTURE_SIZE = size; }
		static GetColorTextureSize() { return COLOR_TEXTURE_SIZE; }

		static WhiteCubemap(context) {
			let cache = TextureCache.Get(context);
			let tex = cache.find(s_whiteCubemap);

			if (!tex) {
				tex = bg.base.Texture.WhiteCubemap(context);
				cache.register(s_whiteCubemap,tex);
			}
			return tex;
		}

		static BlackCubemap(context) {
			let cache = TextureCache.Get(context);
			let tex = cache.find(s_blackCubemap);

			if (!tex) {
				tex = bg.base.Texture.BlackCubemap(context);
				cache.register(s_blackCubemap,tex);
			}
			return tex;
		}
		
		static WhiteTexture(context) {
			let cache = TextureCache.Get(context);
			let tex = cache.find(s_whiteTexture);
			
			if (!tex) {
				tex = bg.base.Texture.WhiteTexture(context,new bg.Vector2(COLOR_TEXTURE_SIZE));
				cache.register(s_whiteTexture,tex);
			}
			
			return tex;
		}
		
		static BlackTexture(context) {
			let cache = TextureCache.Get(context);
			let tex = cache.find(s_blackTexture);
			
			if (!tex) {
				tex = bg.base.Texture.BlackTexture(context,new bg.Vector2(COLOR_TEXTURE_SIZE));
				cache.register(s_blackTexture,tex);
			}
			
			return tex;
		}
		
		static NormalTexture(context) {
			let cache = TextureCache.Get(context);
			let tex = cache.find(s_normalTexture);
			
			if (!tex) {
				tex = bg.base.Texture.NormalTexture(context,new bg.Vector2(COLOR_TEXTURE_SIZE));
				cache.register(s_normalTexture,tex);
			}
			
			return tex;
		}

		static RandomTexture(context) {
			let cache = TextureCache.Get(context);
			let tex = cache.find(s_randomTexture);

			if (!tex) {
				tex = bg.base.Texture.RandomTexture(context,new bg.Vector2(64));
				cache.register(s_randomTexture,tex);
			}

			return tex;
		}
		
		static Get(context) {
			if (!s_textureCache[context.uuid]) {
				s_textureCache[context.uuid] = new TextureCache(context);
			}
			return s_textureCache[context.uuid];
		}

		static PrecomputedBRDFLookupTexture(context) {
			if (!s_textureCache["_bg_base_brdfLutData_"]) {
				s_textureCache["_bg_base_brdfLutData_"] = bg.base.Texture.PrecomputedBRDFLookupTexture(context);
			}
			return s_textureCache["_bg_base_brdfLutData_"];
		}
		
		constructor(context) {
			this._context = context;
			this._textures = {};
		}
		
		find(url) {
			return this._textures[url];
		}
		
		register(url,texture) {
			if (texture instanceof bg.base.Texture) {
				this._textures[url] = texture;
			}
		}
		
		unregister(url) {
			if (this._textures[url]) {
				delete this._textures[url];
			}
		}
		
		clear() {
			this._textures = {};
		}
	}
	
	bg.base.TextureCache = TextureCache;

	let g_wrapX = null;
	let g_wrapY = null;
	let g_minFilter = null;
	let g_magFilter = null;
	
	/* Extra data:
	 * 	wrapX
	 *  wrapY
	 *  minFilter
	 *  magFilter
	 */
	class TextureLoaderPlugin extends bg.base.LoaderPlugin {
		static GetWrapX() {
			return g_wrapX || bg.base.TextureWrap.REPEAT;
		}

		static GetWrapY() {
			return g_wrapY || bg.base.TextureWrap.REPEAT;
		}

		static GetMinFilter() {
			return g_minFilter || bg.base.TextureFilter.LINEAR_MIPMAP_NEAREST;
		}

		static GetMagFilter() {
			return g_magFilter || bg.base.TextureFilter.LINEAR;
		}

		static SetMinFilter(f) {
			g_minFilter = f;
		}

		static SetMagFilter(f) {
			g_magFilter = f;
		}

		static SetWrapX(w) {
			g_wrapX = w;
		}

		static SetWrapY(w) {
			g_wrapY = w;
		}
		
		acceptType(url,data) {
			return bg.utils.Resource.IsImage(url);
		}
		
		load(context,url,data,extraData) {
			return new Promise((accept,reject) => {
				if (data) {
					let texture = bg.base.TextureCache.Get(context).find(url);
					if (!texture) {
						bg.log(`Texture ${url} not found. Loading texture`);
						texture = new bg.base.Texture(context);
						texture.create();
						texture.bind();
						texture.wrapX = extraData.wrapX || TextureLoaderPlugin.GetWrapX();
						texture.wrapY = extraData.wrapY || TextureLoaderPlugin.GetWrapY();
						texture.minFilter = extraData.minFilter || TextureLoaderPlugin.GetMinFilter();
						texture.magFilter = extraData.magFilter || TextureLoaderPlugin.GetMagFilter();
						texture.setImage(data);
						texture.fileName = url;
						bg.base.TextureCache.Get(context).register(url,texture);
					}
					accept(texture);
				}
				else {
					reject(new Error("Error loading texture image data"));
				}
			});
		}
	}
	
	bg.base.TextureLoaderPlugin = TextureLoaderPlugin;

	class VideoTextureLoaderPlugin extends bg.base.LoaderPlugin {
		acceptType(url,data) {
			return bg.utils.Resource.IsVideo(url);
		}

		load(context,url,video) {
			return new Promise((accept,reject) => {
				if (video) {
					let texture = new bg.base.Texture(context);
					texture.create();
					texture.bind();
					texture.setVideo(video);
					texture.fileName = url;
					accept(texture);
				}
				else {
					reject(new Error("Error loading video texture data"));
				}
			});
		}
	}

	bg.base.VideoTextureLoaderPlugin = VideoTextureLoaderPlugin;
})();
(function() {

	bg.base.TextureUnit = {
		TEXTURE_0: 0,
		TEXTURE_1: 1,
		TEXTURE_2: 2,
		TEXTURE_3: 3,
		TEXTURE_4: 4,
		TEXTURE_5: 5,
		TEXTURE_6: 6,
		TEXTURE_7: 7,
		TEXTURE_8: 8,
		TEXTURE_9: 9,
		TEXTURE_10: 10,
		TEXTURE_11: 11,
		TEXTURE_12: 12,
		TEXTURE_13: 13,
		TEXTURE_14: 14,
		TEXTURE_15: 15,
		TEXTURE_16: 16,
		TEXTURE_17: 17,
		TEXTURE_18: 18,
		TEXTURE_19: 19,
		TEXTURE_20: 20,
		TEXTURE_21: 21,
		TEXTURE_22: 22,
		TEXTURE_23: 23,
		TEXTURE_24: 24,
		TEXTURE_25: 25,
		TEXTURE_26: 26,
		TEXTURE_27: 27,
		TEXTURE_28: 28,
		TEXTURE_29: 29,
		TEXTURE_30: 30
	};
	
	bg.base.TextureWrap = {
		REPEAT: null,
		CLAMP: null,
		MIRRORED_REPEAT: null
	};
	
	bg.base.TextureFilter = {
		NEAREST_MIPMAP_NEAREST: null,
		LINEAR_MIPMAP_NEAREST: null,
		NEAREST_MIPMAP_LINEAR: null,
		LINEAR_MIPMAP_LINEAR: null,
		NEAREST: null,
		LINEAR: null
	};
	
	bg.base.TextureTarget = {
		TEXTURE_2D: null,
		CUBE_MAP: null,
		POSITIVE_X_FACE: null,
		NEGATIVE_X_FACE: null,
		POSITIVE_Y_FACE: null,
		NEGATIVE_Y_FACE: null,
		POSITIVE_Z_FACE: null,
		NEGATIVE_Z_FACE: null
	};
		
	class TextureImpl {
		constructor(context) {
			this.initFlags(context);
		}
	
		initFlags(context) {
			console.log("TextureImpl: initFlags() method not implemented");
		}
		
		create(context) {
			console.log("TextureImpl: create() method not implemented");
			return null;
		}
		
		setActive(context,texUnit) {
			console.log("TextureImpl: setActive() method not implemented");
		}
		
		bind(context,target,texture) {
			console.log("TextureImpl: bind() method not implemented");
		}
		
		unbind(context,target) {
			console.log("TextureImpl: unbind() method not implemented");
		}
		
		setTextureWrapX(context,target,texture,wrap) {
			console.log("TextureImpl: setTextureWrapX() method not implemented");
		}
		
		setTextureWrapY(context,target,texture,wrap) {
			console.log("TextureImpl: setTextureWrapY() method not implemented");
		}
		
		setImage(context,target,minFilter,magFilter,texture,img,flipY) {
			console.log("TextureImpl: setImage() method not implemented");
		}
		
		setImageRaw(context,target,minFilter,magFilter,texture,width,height,data) {
			console.log("TextureImpl: setImageRaw() method not implemented");
		}

		setTextureFilter(context,target,minFilter,magFilter) {
			console.log("TextureImpl: setTextureFilter() method not implemented");
		}

		setCubemapImage(context,face,image) {
			console.log("TextureImpl: setCubemapImage() method not implemented");
		}

		setCubemapRaw(context,face,rawImage,w,h) {
			console.log("TextureImpl: setCubemapRaw() method not implemented");
		}

		setVideo(context,target,texture,video,flipY) {
			console.log("TextureImpl: setVideo() method not implemented");
		}

		updateVideoData(context,target,texture,video) {
			console.log("TextureImpl: updateVideoData() method not implemented");
		}

		destroy(context,texture) {
			console.log("TextureImpl: destroy() method not implemented");
		}
	}
	
	bg.base.TextureImpl = TextureImpl;

	bg.base.TextureDataType = {
		NONE: 0,
		IMAGE: 1,
		IMAGE_DATA: 2,
		CUBEMAP: 3,
		CUBEMAP_DATA: 4,
		VIDEO: 5
	};

	let g_base64TexturePreventRemove = [];

	class Texture extends bg.app.ContextObject {
		static IsPowerOfTwoImage(image) {
			return bg.Math.checkPowerOfTwo(image.width) && bg.Math.checkPowerOfTwo(image.height);
		}

		static FromCanvas(context,canvas2d) {
			return Texture.FromBase64Image(context,canvas2d.toDataURL("image/png"));
		}

		static UpdateCanvasImage(texture,canvas2d) {
			if (!texture.valid) {
				return false;
			}
			let imageData = canvas2d.toDataURL("image/png");
			let recreate = false;
			if (texture.img.width!=imageData.width || texture.img.height!=imageData.height) {
				recreate = true;
			}
			texture.img = new Image();
			g_base64TexturePreventRemove.push(texture);
			//tex.onload = function(evt,img) {
			// Check this: use onload or setTimeout?
			// onload seems to not work in all situations
			setTimeout(() => {
				texture.bind();
				if (Texture.IsPowerOfTwoImage(texture.img)) {
					texture.minFilter = bg.base.TextureLoaderPlugin.GetMinFilter();
					texture.magFilter = bg.base.TextureLoaderPlugin.GetMagFilter();
				}
				else {
					texture.minFilter = bg.base.TextureFilter.NEAREST;
					texture.magFilter = bg.base.TextureFilter.NEAREST;
					texture.wrapX = bg.base.TextureWrap.CLAMP;
					texture.wrapY = bg.base.TextureWrap.CLAMP;
				}
				texture.setImage(texture.img,true);
				texture.unbind();
				let index = g_base64TexturePreventRemove.indexOf(texture);
				if (index!=-1) {
					g_base64TexturePreventRemove.splice(index,1);
				}
				bg.emitImageLoadEvent();
			//}
			},10);
			texture.img.src = imageData;
			
			return texture;
		}

		static FromBase64Image(context,imgData) {
			let tex = new bg.base.Texture(context);
			tex.img = new Image();
			g_base64TexturePreventRemove.push(tex);
			//tex.onload = function(evt,img) {
			// Check this: use onload or setTimeout?
			// onload seems to not work in all situations
			setTimeout(() => {
				tex.create();
				tex.bind();
				if (Texture.IsPowerOfTwoImage(tex.img)) {
					tex.minFilter = bg.base.TextureLoaderPlugin.GetMinFilter();
					tex.magFilter = bg.base.TextureLoaderPlugin.GetMagFilter();
				}
				else {
					tex.minFilter = bg.base.TextureFilter.NEAREST;
					tex.magFilter = bg.base.TextureFilter.NEAREST;
					tex.wrapX = bg.base.TextureWrap.CLAMP;
					tex.wrapY = bg.base.TextureWrap.CLAMP;
				}
				tex.setImage(tex.img,false);	// Check this: flip base64 image?
				tex.unbind();
				let index = g_base64TexturePreventRemove.indexOf(tex);
				if (index!=-1) {
					g_base64TexturePreventRemove.splice(index,1);
				}
				bg.emitImageLoadEvent();
			//}
			},10);
			tex.img.src = imgData;
			
			return tex;
		}
		
		static ColorTexture(context,color,size) {
			let colorTexture = new bg.base.Texture(context);
			colorTexture.create();
			colorTexture.bind();

			var dataSize = size.width * size.height * 4;
			var textureData = [];
			for (var i = 0; i < dataSize; i+=4) {
				textureData[i]   = color.r * 255;
				textureData[i+1] = color.g * 255;
				textureData[i+2] = color.b * 255;
				textureData[i+3] = color.a * 255;
			}
			
			textureData = new Uint8Array(textureData);

			colorTexture.minFilter = bg.base.TextureFilter.NEAREST;
			colorTexture.magFilter = bg.base.TextureFilter.NEAREST;
			colorTexture.setImageRaw(size.width,size.height,textureData);
			colorTexture.unbind();

			return colorTexture;
		}
		
		static WhiteTexture(context,size) {
			return Texture.ColorTexture(context,bg.Color.White(),size);
		}

		static WhiteCubemap(context) {
			return Texture.ColorCubemap(context, bg.Color.White());
		}

		static BlackCubemap(context) {
			return Texture.ColorCubemap(context, bg.Color.Black());
		}

		static ColorCubemap(context,color) {
			let cm = new bg.base.Texture(context);
			cm.target = bg.base.TextureTarget.CUBE_MAP;
			cm.create();
			cm.bind();

			let dataSize = 32 * 32 * 4;
			let textureData = [];
			for (let i = 0; i<dataSize; i+=4) {
				textureData[i] 		= color.r * 255;
				textureData[i + 1] 	= color.g * 255;
				textureData[i + 2] 	= color.b * 255;
				textureData[i + 3] 	= color.a * 255;
			}

			textureData = new Uint8Array(textureData);

			cm.setCubemapRaw(
				32,
				32,
				textureData,
				textureData,
				textureData,
				textureData,
				textureData,
				textureData
			);

			cm.unbind();
			return cm;
		}
		
		static NormalTexture(context,size) {
			return Texture.ColorTexture(context,new bg.Color(0.5,0.5,1,1),size);
		}
		
		static BlackTexture(context,size) {
			return Texture.ColorTexture(context,bg.Color.Black(),size);
		}

		static RandomTexture(context,size) {
			let colorTexture = new bg.base.Texture(context);
			colorTexture.create();
			colorTexture.bind();

			var dataSize = size.width * size.height * 4;
			var textureData = [];
			for (var i = 0; i < dataSize; i+=4) {
				let randVector = new bg.Vector3(bg.Math.random() * 2.0 - 1.0,
												bg.Math.random() * 2.0 - 1.0,
												0);
				randVector.normalize();

				textureData[i]   = randVector.x * 255;
				textureData[i+1] = randVector.y * 255;
				textureData[i+2] = randVector.z * 255;
				textureData[i+3] = 1;
			}
			
			textureData = new Uint8Array(textureData);

			colorTexture.minFilter = bg.base.TextureFilter.NEAREST;
			colorTexture.magFilter = bg.base.TextureFilter.NEAREST;
			colorTexture.setImageRaw(size.width,size.height,textureData);
			colorTexture.unbind();

			return colorTexture;
		}

		/*
		 *	Precomputed BRDF, for using with PBR shaders
		 */
		static PrecomputedBRDFLookupTexture(context) {
			return Texture.FromBase64Image(context,bg.base._brdfLUTData);
		}

		/*
		 *	Create a texture using an image.
		 *		context: the rendering context
		 *		image: a valid image object (for example, an <image> tag)
		 *		url: unique URL for this image, used as index for the texture cache
		 */
		static FromImage(context,image,url) {
			let texture = null;
			if (image) {
				texture = bg.base.TextureCache.Get(context).find(url);
				if (!texture) {
					bg.log(`Texture ${url} not found. Loading texture`);
					texture = new bg.base.Texture(context);
					texture.create();
					texture.bind();
					texture.minFilter = bg.base.TextureLoaderPlugin.GetMinFilter();
					texture.magFilter = bg.base.TextureLoaderPlugin.GetMagFilter();
					texture.setImage(image);
					texture.fileName = url;
					bg.base.TextureCache.Get(context).register(url,texture);
				}
			}
			return texture;
		}
		
		static SetActive(context,textureUnit) {
			bg.Engine.Get().texture.setActive(context,textureUnit);
		}
		
		static Unbind(context, target) {
			if (!target) {
				target = bg.base.TextureTarget.TEXTURE_2D;
			}
			bg.Engine.Get().texture.unbind(context,target);
		}
		
		constructor(context) {
			super(context);
			
			this._texture = null;
			this._fileName = "";
			this._size = new bg.Vector2(0);
			this._target = bg.base.TextureTarget.TEXTURE_2D;
			
			this._minFilter = bg.base.TextureFilter.LINEAR;
			this._magFilter = bg.base.TextureFilter.LINEAR;
			
			this._wrapX = bg.base.TextureWrap.REPEAT;
			this._wrapY = bg.base.TextureWrap.REPEAT;

			this._video = null;
		}
		
		get texture() { return this._texture; }
		get target() { return this._target; }
		set target(t) { this._target = t; }
		get fileName() { return this._fileName; }
		set fileName(fileName) { this._fileName = fileName; }
		set minFilter(f) { this._minFilter = f; }
		set magFilter(f) { this._magFilter = f; }
		get minFilter() { return this._minFilter; }
		get magFilter() { return this._magFilter; }
		set wrapX(w) { this._wrapX = w; }
		set wrapY(w) { this._wrapY = w; }
		get wrapX() { return this._wrapX; }
		get wrapY() { return this._wrapY; }
		get size() { return this._size; }
		

		// Access to image data structures
		get image() { return this._image; }
		get imageData() { return this._imageData; }
		get cubeMapImages() { return this._cubeMapImages; }
		get cubeMapData() { return this._cubeMapData; }
		get video() { return this._video; }

		get dataType() {
			if (this._image) {
				return bg.base.TextureDataType.IMAGE;
			}
			else if (this._imageData) {
				return bg.base.TextureDataType.IMAGE_DATA;
			}
			else if (this._cubeMapImages) {
				return bg.base.TextureDataType.CUBEMAP;
			}
			else if (this._cubeMapData) {
				return bg.base.TextureDataType.CUBEMAP_DATA;
			}
			else if (this._video) {
				return bg.base.TextureDataType.VIDEO;
			}
			else {
				return bg.base.TextureDataType.NONE;
			}
		}

		create() {
			if (this._texture!==null) {
				this.destroy()
			}
			this._texture = bg.Engine.Get().texture.create(this.context);
		}
		
		setActive(textureUnit) {
			bg.Engine.Get().texture.setActive(this.context,textureUnit);
		}
		
		
		bind() {
			bg.Engine.Get().texture.bind(this.context,this._target,this._texture);
		}
		
		unbind() {
			Texture.Unbind(this.context,this._target);
		}
		
		setImage(img, flipY) {
			if (flipY===undefined) flipY = true;
			this._size.width = img.width;
			this._size.height = img.height;
			bg.Engine.Get().texture.setTextureWrapX(this.context,this._target,this._texture,this._wrapX);
			bg.Engine.Get().texture.setTextureWrapY(this.context,this._target,this._texture,this._wrapY);
			bg.Engine.Get().texture.setImage(this.context,this._target,this._minFilter,this._magFilter,this._texture,img,flipY);

			this._image = img;
			this._imageData = null;
			this._cubeMapImages = null;
			this._cubeMapData = null;
			this._video = null;
		}

		updateImage(img, flipY) {
			if (flipY===undefined) flipY = true;
			this._size.width = img.width;
			this._size.height = img.height;
			bg.Engine.Get().texture.setTextureWrapX(this.context,this._target,this._texture,this._wrapX);
			bg.Engine.Get().texture.setTextureWrapY(this.context,this._target,this._texture,this._wrapY);
			bg.Engine.Get().texture.setImage(this.context,this._target,this._minFilter,this._magFilter,this._texture,img,flipY);

			this._image = img;
			this._imageData = null;
			this._cubeMapImages = null;
			this._cubeMapData = null;
			this._video = null;
		}
		
		setImageRaw(width,height,data,type,format) {
			if (!type) {
				type = this.context.RGBA;
			}
			if (!format) {
				format = this.context.UNSIGNED_BYTE;
			}
			this._size.width = width;
			this._size.height = height;
			bg.Engine.Get().texture.setTextureWrapX(this.context,this._target,this._texture,this._wrapX);
			bg.Engine.Get().texture.setTextureWrapY(this.context,this._target,this._texture,this._wrapY);
			bg.Engine.Get().texture.setImageRaw(this.context,this._target,this._minFilter,this._magFilter,this._texture,width,height,data,type,format);

			this._image = null;
			this._imageData = data;
			this._cubeMapImages = null;
			this._cubeMapData = null;
			this._video = null;
		}

		setCubemap(posX,negX,posY,negY,posZ,negZ) {
			bg.Engine.Get().texture.bind(this.context,this._target,this._texture);
			bg.Engine.Get().texture.setTextureWrapX(this.context,this._target,this._texture,this._wrapX);
			bg.Engine.Get().texture.setTextureWrapX(this.context,this._target,this._texture,this._wrapY);
			bg.Engine.Get().texture.setTextureFilter(this.context,this._target,this._minFilter,this._magFilter);
			bg.Engine.Get().texture.setCubemapImage(this.context,bg.base.TextureTarget.POSITIVE_X_FACE,posX);
			bg.Engine.Get().texture.setCubemapImage(this.context,bg.base.TextureTarget.NEGATIVE_X_FACE,negX);
			bg.Engine.Get().texture.setCubemapImage(this.context,bg.base.TextureTarget.POSITIVE_Y_FACE,posY);
			bg.Engine.Get().texture.setCubemapImage(this.context,bg.base.TextureTarget.NEGATIVE_Y_FACE,negY);
			bg.Engine.Get().texture.setCubemapImage(this.context,bg.base.TextureTarget.POSITIVE_Z_FACE,posZ);
			bg.Engine.Get().texture.setCubemapImage(this.context,bg.base.TextureTarget.NEGATIVE_Z_FACE,negZ);

			this._image = null;
			this._imageData = null;
			this._cubeMapImages = {
				posX: posX,
				negX: negX,
				posY: posY,
				negY: negY,
				posZ: posZ,
				negZ: negZ
			};
			this._cubeMapData = null;
			this._video = null;
		}

		setCubemapRaw(w,h,posX,negX,posY,negY,posZ,negZ) {
			bg.Engine.Get().texture.bind(this.context,this._target,this._texture);
			bg.Engine.Get().texture.setTextureWrapX(this.context,this._target,this._texture,this._wrapX);
			bg.Engine.Get().texture.setTextureWrapX(this.context,this._target,this._texture,this._wrapY);
			bg.Engine.Get().texture.setTextureFilter(this.context,this._target,this._minFilter,this._magFilter);
			bg.Engine.Get().texture.setCubemapRaw(this.context,bg.base.TextureTarget.POSITIVE_X_FACE,posX,w,h);
			bg.Engine.Get().texture.setCubemapRaw(this.context,bg.base.TextureTarget.NEGATIVE_X_FACE,negX,w,h);
			bg.Engine.Get().texture.setCubemapRaw(this.context,bg.base.TextureTarget.POSITIVE_Y_FACE,posY,w,h);
			bg.Engine.Get().texture.setCubemapRaw(this.context,bg.base.TextureTarget.NEGATIVE_Y_FACE,negY,w,h);
			bg.Engine.Get().texture.setCubemapRaw(this.context,bg.base.TextureTarget.POSITIVE_Z_FACE,posZ,w,h);
			bg.Engine.Get().texture.setCubemapRaw(this.context,bg.base.TextureTarget.NEGATIVE_Z_FACE,negZ,w,h);

			this._image = null;
			this._imageData = null;
			this._cubeMapImages = null;
			this._cubeMapData = {
				width: w,
				height: h,
				posX:posX,
				negX:negX,
				posY:posY,
				negY:negY,
				posZ:posZ,
				negZ:negZ
			};
			this._video = null;
		}

		setVideo(video, flipY) {
			if (flipY===undefined) flipY = true;
			this._size.width = video.videoWidth;
			this._size.height = video.videoHeight;
			bg.Engine.Get().texture.setVideo(this.context,this._target,this._texture,video,flipY);
			this._video = video;

			this._image = null;
			this._imageData = null;
			this._cubeMapImages = null;
			this._cubeMapData = null;
		}

		destroy() {
			bg.Engine.Get().texture.destroy(this.context,this._texture);
			this._texture = null;
			this._minFilter = null;
			this._magFilter = null;
			this._fileName = "";
		}

		valid() {
			return this._texture!==null;
		}

		update() {
			bg.Engine.Get().texture.updateVideoData(this.context,this._target,this._texture,this._video);
		}
	}
	
	bg.base.Texture = Texture;
		
})();


(function() {
	bg.Math = {

		seed:1,

		PI:3.141592653589793,
		DEG_TO_RAD:0.01745329251994,
		RAD_TO_DEG:57.29577951308233,
		PI_2:1.5707963267948966,
		PI_4:0.785398163397448,
		PI_8:0.392699081698724,
		TWO_PI:6.283185307179586,
		
		EPSILON:0.0000001,
		Array:Float32Array,
		ArrayHighP:Array,
		FLOAT_MAX:3.402823e38,
		
		checkPowerOfTwo:function(n) {
			if (typeof n !== 'number') {
				return false;
			}
			else {
				return n && (n & (n - 1)) === 0;
			}  
		},

		checkZero:function(v) {
			return v>-this.EPSILON && v<this.EPSILON ? 0:v;
		},
		
		equals:function(a,b) {
			return Math.abs(a - b) < this.EPSILON;
		},
		
		degreesToRadians:function(d) {
			return Math.fround(this.checkZero(d * this.DEG_TO_RAD));
		},
		
		radiansToDegrees:function(r) {
			return Math.fround(this.checkZero(r * this.RAD_TO_DEG));
		},

		sin:function(val) {
			return Math.fround(this.checkZero(Math.sin(val)));
		},

		cos:function(val) {
			return Math.fround(this.checkZero(Math.cos(val)));
		},

		tan:function(val) {
			return Math.fround(this.checkZero(Math.tan(val)));
		},

		cotan:function(val) {
			return Math.fround(this.checkZero(1.0 / this.tan(val)));
		},

		atan:function(val) {
			return Math.fround(this.checkZero(Math.atan(val)));
		},
		
		atan2:function(i, j) {
			return Math.fround(this.checkZero(Math.atan2f(i, j)));
		},

		random:function() {
			return Math.random();
		},

		seededRandom:function() {
			let max = 1;
			let min = 0;
		 
			this.seed = (this.seed * 9301 + 49297) % 233280;
			var rnd = this.seed / 233280;
		 
			return min + rnd * (max - min);
		},
		
		max:function(a,b) {
			return Math.fround(Math.max(a,b));
		},
		
		min:function(a,b) {
			return Math.fround(Math.min(a,b));
		},
		
		abs:function(val) {
			return Math.fround(Math.abs(val));
		},
		
		sqrt:function(val) {
			return Math.fround(Math.sqrt(val));
		},
		
		lerp:function(from, to, t) {
			return Math.fround((1.0 - t) * from + t * to);
		},
		
		square:function(n) {
			return Math.fround(n * n);
		}
	};

	class MatrixStrategy {
		constructor(target) {
			this._target = target;
		}

		get target() { return this._target; }
		set target(t) { this._target = t; }

		apply() {
			console.log("WARNING: MatrixStrategy::apply() not overloaded by the child class.");
		}
	}

	bg.MatrixStrategy = MatrixStrategy;

})();

(function() {
	
class Matrix3 {
	static Identity() {
		return new bg.Matrix3(1,0,0, 0,1,0, 0,0,1);
	}
	
	constructor(v00=1,v01=0,v02=0,v10=0,v11=1,v12=0,v20=0,v21=0,v22=1) {
		this._m = new bg.Math.Array(9);
		if (Array.isArray(typeof(v00))) {
			this._m[0] = v00[0]; this._m[1] = v00[1]; this._m[2] = v00[0];
			this._m[3] = v00[3]; this._m[4] = v00[4]; this._m[5] = v00[5];
			this._m[6] = v00[6]; this._m[7] = v00[7]; this._m[8] = v00[8];
		}
		else if (typeof(v00)=="number") {
			this._m[0] = v00; this._m[1] = v01; this._m[2] = v02;
			this._m[3] = v10; this._m[4] = v11; this._m[5] = v12;
			this._m[6] = v20; this._m[7] = v21; this._m[8] = v22;
		}
		else {
			this.assign(v00);
		}
	}
	
	get m() { return this._m; }

	toArray() {
		return [
			this._m[0], this._m[1], this._m[2],
			this._m[3], this._m[4], this._m[5],
			this._m[6], this._m[7], this._m[8]
		]
	}
	
	get m00() { return this._m[0]; }
	get m01() { return this._m[1]; }
	get m02() { return this._m[2]; }
	get m10() { return this._m[3]; }
	get m11() { return this._m[4]; }
	get m12() { return this._m[5]; }
	get m20() { return this._m[6]; }
	get m21() { return this._m[7]; }
	get m22() { return this._m[8]; }
	
	set m00(v) { this._m[0] = v; }
	set m01(v) { this._m[1] = v; }
	set m02(v) { this._m[2] = v; }
	set m10(v) { this._m[3] = v; }
	set m11(v) { this._m[4] = v; }
	set m12(v) { this._m[5] = v; }
	set m20(v) { this._m[6] = v; }
	set m21(v) { this._m[7] = v; }
	set m22(v) { this._m[8] = v; }
	
	zero() {
		this._m[0] = this._m[1] = this._m[2] =
		this._m[3] = this._m[4] = this._m[5] =
		this._m[6] = this._m[7] = this._m[8] = 0;
		return this;
	}

	identity() {
		this._m[0] = 1; this._m[1] = 0; this._m[2] = 0;
		this._m[3] = 0; this._m[4] = 1; this._m[5] = 0;
		this._m[6] = 0; this._m[7] = 0; this._m[8] = 1;
		return this;
	}
	
	isZero() {
		return	this._m[0]==0.0 && this._m[1]==0.0 && this._m[2]==0.0 &&
				this._m[3]==0.0 && this._m[4]==0.0 && this._m[5]==0.0 &&
				this._m[6]==0.0 && this._m[7]==0.0 && this._m[8]==0.0;
	}
	
	isIdentity() {
		return	this._m[0]==1.0 && this._m[1]==0.0 && this._m[2]==0.0 &&
				this._m[3]==0.0 && this._m[4]==1.0 && this._m[5]==0.0 &&
				this._m[6]==0.0 && this._m[7]==0.0 && this._m[8]==1.0;
	}

	row(i) { return new bg.Vector3(this._m[i*3], this._m[i*3 + 1], this._m[i*3 + 2]); }
	setRow(i, row) { this._m[i*3]=row._v[0]; this._m[i*3 + 1]=row._v[1]; this._m[i*3 + 2]=row._v[2]; return this; }

	setScale(x,y,z) { 
		let rx = new bg.Vector3(this._m[0], this._m[3], this._m[6]).normalize().scale(x);
		let ry = new bg.Vector3(this._m[1], this._m[4], this._m[7]).normalize().scale(y);
		let rz = new bg.Vector3(this._m[2], this._m[5], this._m[8]).normalize().scale(z);
		this._m[0] = rx.x; this._m[3] = rx.y; this._m[6] = rx.z;
		this._m[1] = ry.x; this._m[4] = ry.y; this._m[7] = ry.z;
		this._m[2] = rz.x; this._m[5] = rz.y; this._m[8] = rz.z;
		return this;
	}
	getScale() {
		return new bg.Vector3(
			new bg.Vector3(this._m[0], this._m[3], this._m[6]).module,
			new bg.Vector3(this._m[1], this._m[4], this._m[7]).module,
			new bg.Vector3(this._m[2], this._m[5], this._m[8]).module
		);
	}
	
	get length() { return this._m.length; }
	
	traspose() {
		let r0 = new bg.Vector3(this._m[0], this._m[3], this._m[6]);
		let r1 = new bg.Vector3(this._m[1], this._m[4], this._m[7]);
		let r2 = new bg.Vector3(this._m[2], this._m[5], this._m[8]);
		
		this.setRow(0, r0);
		this.setRow(1, r1);
		this.setRow(2, r2);

		return this;
	}
	
	elemAtIndex(i) { return this._m[i]; }
	assign(a) {
		if (a.length==9) {
			this._m[0] = a._m[0]; this._m[1] = a._m[1]; this._m[2] = a._m[2];
			this._m[3] = a._m[3]; this._m[4] = a._m[4]; this._m[5] = a._m[5];
			this._m[6] = a._m[6]; this._m[7] = a._m[7]; this._m[8] = a._m[8];
		}
		else if (a.length==16) {
			this._m[0] = a._m[0]; this._m[1] = a._m[1]; this._m[2] = a._m[2];
			this._m[3] = a._m[4]; this._m[4] = a._m[5]; this._m[5] = a._m[6];
			this._m[6] = a._m[8]; this._m[7] = a._m[9]; this._m[8] = a._m[10];
		}
		return this;
	}
	
	equals(m) {
		return	this._m[0] == m._m[0] && this._m[1] == m._m[1]  && this._m[2] == m._m[2] &&
				this._m[3] == m._m[3] && this._m[4] == m._m[4]  && this._m[5] == m._m[5] &&
				this._m[6] == m._m[6] && this._m[7] == m._m[7]  && this._m[8] == m._m[8];
	}

	notEquals(m) {
		return	this._m[0] != m._m[0] || this._m[1] != m._m[1]  || this._m[2] != m._m[2] &&
				this._m[3] != m._m[3] || this._m[4] != m._m[4]  || this._m[5] != m._m[5] &&
				this._m[6] != m._m[6] || this._m[7] != m._m[7]  || this._m[8] != m._m[8];
	}
		
	mult(a) {
		if (typeof(a)=="number") {
			this._m[0] *= a; this._m[1] *= a; this._m[2] *= a;
			this._m[3] *= a; this._m[4] *= a; this._m[5] *= a;
			this._m[6] *= a; this._m[7] *= a; this._m[8] *= a;
			
		}
		else {
			let rm = this._m;
			let lm = a._m;
			
			let res = new bg.Math.Array(9);
			res[0] = lm[0] * rm[0] + lm[1] * rm[1] + lm[2] * rm[2];
			res[1] = lm[0] * rm[1] + lm[1] * rm[4] + lm[2] * rm[7];
			res[2] = lm[0] * rm[2] + lm[1] * rm[5] + lm[2] * rm[8];
			
			res[3] = lm[3] * rm[0] + lm[4] * rm[3] + lm[5] * rm[6];
			res[4] = lm[3] * rm[1] + lm[4] * rm[4] + lm[5] * rm[7];
			res[5] = lm[3] * rm[2] + lm[4] * rm[5] + lm[5] * rm[8];
			
			res[6] = lm[6] * rm[0] + lm[7] * rm[3] + lm[8] * rm[6];
			res[7] = lm[6] * rm[1] + lm[7] * rm[4] + lm[8] * rm[7];
			res[8] = lm[6] * rm[2] + lm[7] * rm[5] + lm[8] * rm[8];
			this._m = res;
		}
		return this;
	}

	multVector(vec) {
		if (typeof(vec)=='object' && vec._v && vec._v.length>=2) {
			vec = vec._v;
		}
		let x=vec[0];
		let y=vec[1];
		let z=1.0;
	
		return new bg.Vector3(	this._m[0]*x + this._m[3]*y + this._m[6]*z,
								this._m[1]*x + this._m[4]*y + this._m[7]*z,
								this._m[2]*x + this._m[5]*y + this._m[8]*z);
	}
		
	isNan() {
		return	!Math.isNaN(_m[0]) && !Math.isNaN(_m[1]) && !Math.isNaN(_m[2]) &&
				!Math.isNaN(_m[3]) && !Math.isNaN(_m[4]) && !Math.isNaN(_m[5]) &&
				!Math.isNaN(_m[6]) && !Math.isNaN(_m[7]) && !Math.isNaN(_m[8]);
	}

	toString() {
		return "[" + this._m[0] + ", " + this._m[1] + ", " + this._m[2] + "]\n" +
			   " [" + this._m[3] + ", " + this._m[4] + ", " + this._m[5] + "]\n" +
			   " [" + this._m[6] + ", " + this._m[7] + ", " + this._m[8] + "]";
	}
}

bg.Matrix3 = Matrix3;

class Matrix4 {
	static Unproject(x, y, depth, mvMat, pMat, viewport) {
		let mvp = new bg.Matrix4(pMat);
		mvp.mult(mvMat);
		mvp.invert();

		let vin = new bg.Vector4(((x - viewport.y) / viewport.width) * 2.0 - 1.0,
								((y - viewport.x) / viewport.height) * 2.0 - 1.0,
								depth * 2.0 - 1.0,
								1.0);
		
		let result = new bg.Vector4(mvp.multVector(vin));
		if (result.z==0) {
			result.set(0);
		}
		else {
			result.set(	result.x/result.w,
						result.y/result.w,
						result.z/result.w,
						result.w/result.w);
		}

		return result;
	}

	static Identity() {
		return new bg.Matrix4(1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1);
	}
		
	static Perspective(fovy, aspect, nearPlane, farPlane) {
		let fovy2 = bg.Math.tan(fovy * bg.Math.PI / 360.0) * nearPlane;
		let fovy2aspect = fovy2 * aspect;

		return bg.Matrix4.Frustum(-fovy2aspect,fovy2aspect,-fovy2,fovy2,nearPlane,farPlane);
	}

	static Frustum(left, right, bottom, top, nearPlane, farPlane) {
		let res = new bg.Matrix4();
		let A = right-left;
		let B = top-bottom;
		let C = farPlane-nearPlane;
		
		res.setRow(0, new bg.Vector4(nearPlane*2.0/A,	0.0,	0.0,	0.0));
		res.setRow(1, new bg.Vector4(0.0,	nearPlane*2.0/B,	0.0,	0.0));
		res.setRow(2, new bg.Vector4((right+left)/A,	(top+bottom)/B,	-(farPlane+nearPlane)/C,	-1.0));
		res.setRow(3, new bg.Vector4(0.0,	0.0,	-(farPlane*nearPlane*2.0)/C,	0.0));
		
		return res;
	}

	static Ortho(left, right, bottom, top, nearPlane, farPlane) {
		let p = new bg.Matrix4();
		
		let m = right-left;
		let l = top-bottom;
		let k = farPlane-nearPlane;;
		
		p._m[0] = 2/m; p._m[1] = 0;   p._m[2] = 0;     p._m[3] = 0;
		p._m[4] = 0;   p._m[5] = 2/l; p._m[6] = 0;     p._m[7] = 0;
		p._m[8] = 0;   p._m[9] = 0;   p._m[10] = -2/k; p._m[11]= 0;
		p._m[12]=-(left+right)/m; p._m[13] = -(top+bottom)/l; p._m[14] = -(farPlane+nearPlane)/k; p._m[15]=1;

		return p;
	}
		
	static LookAt(p_eye, p_center, p_up) {
		let result = bg.Matrix4.Identity();
		let y = new bg.Vector3(p_up);
		let z = bg.Vector3.Sub(p_eye,p_center);
		z.normalize();
		let x = bg.Vector3.Cross(y,z);
		x.normalize();
		y.normalize();

		result.m00 = x.x;
		result.m10 = x.y;
		result.m20 = x.z;
		result.m30 = -x.dot( p_eye );
		result.m01 = y.x;
		result.m11 = y.y;
		result.m21 = y.z;
		result.m31 = -y.dot( p_eye );
		result.m02 = z.x;
		result.m12 = z.y;
		result.m22 = z.z;
		result.m32 = -z.dot( p_eye );
		result.m03 = 0;
		result.m13 = 0;
		result.m23 = 0;
		result.m33 = 1;
	
		return result;
	}

	static Translation(x, y, z) {
		if (typeof(x)=='object' && x._v && x._v.length>=3) {
			y = x._v[1];
			z = x._v[2];
			x = x._v[0];
		}
		return new bg.Matrix4(
			1.0, 0.0, 0.0, 0.0,
			0.0, 1.0, 0.0, 0.0,
			0.0, 0.0, 1.0, 0.0,
			  x,   y,   z, 1.0
		);
	}
		
	static Rotation(alpha, x, y, z) {
		let axis = new bg.Vector3(x,y,z);
		axis.normalize();
		let rot = new bg.Matrix4(1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1);
				
		var cosAlpha = bg.Math.cos(alpha);
		var acosAlpha = 1.0 - cosAlpha;
		var sinAlpha = bg.Math.sin(alpha);
		
		return new bg.Matrix4(
			axis.x * axis.x * acosAlpha + cosAlpha, axis.x * axis.y * acosAlpha + axis.z * sinAlpha, axis.x * axis.z * acosAlpha - axis.y * sinAlpha, 0,
			axis.y * axis.x * acosAlpha - axis.z * sinAlpha, axis.y * axis.y * acosAlpha + cosAlpha, axis.y * axis.z * acosAlpha + axis.x * sinAlpha, 0,
			axis.z * axis.x * acosAlpha + axis.y * sinAlpha, axis.z * axis.y * acosAlpha - axis.x * sinAlpha, axis.z * axis.z * acosAlpha + cosAlpha, 0,
			0,0,0,1
		);
	}

	static Scale(x, y, z) {
		if (typeof(x)=='object' && x._v && x._v.length>=3) {
			x = x._v[0];
			y = x._v[1];
			z = x._v[2];
		}
		return new bg.Matrix4(
			x, 0, 0, 0,
			0, y, 0, 0,
			0, 0, z, 0,
			0, 0, 0, 1
		)
	}
	
	constructor(m00=0,m01=0,m02=0,m03=0, m10=0,m11=0,m12=0,m13=0, m20=0,m21=0,m22=0,m23=0, m30=0,m31=0,m32=0,m33=0) {
		this._m = new bg.Math.Array(16);
		if (Array.isArray(m00)) {
			this._m[ 0] =  m00[0]; this._m[ 1] =  m00[1]; this._m[ 2] =  m00[2]; this._m[ 3] =  m00[3];
			this._m[ 4] =  m00[4]; this._m[ 5] =  m00[5]; this._m[ 6] =  m00[6]; this._m[ 7] =  m00[7];
			this._m[ 8] =  m00[8]; this._m[ 9] =  m00[9]; this._m[10] = m00[10]; this._m[11] = m00[11];
			this._m[12] = m00[12]; this._m[13] = m00[13]; this._m[14] = m00[14]; this._m[15] = m00[15];
		}
		else if (typeof(m00)=="number") {
			this._m[ 0] = m00; this._m[ 1] = m01; this._m[ 2] = m02; this._m[ 3] = m03;
			this._m[ 4] = m10; this._m[ 5] = m11; this._m[ 6] = m12; this._m[ 7] = m13;
			this._m[ 8] = m20; this._m[ 9] = m21; this._m[10] = m22; this._m[11] = m23;
			this._m[12] = m30; this._m[13] = m31; this._m[14] = m32; this._m[15] = m33;
		}
		else {
			this.assign(m00);
		}
	}
	
	get m() { return this._m; }

	toArray() {
		return [
			this._m[ 0], this._m[ 1], this._m[ 2], this._m[ 3],
			this._m[ 4], this._m[ 5], this._m[ 6], this._m[ 7],
			this._m[ 8], this._m[ 9], this._m[10], this._m[11],
			this._m[12], this._m[13], this._m[14], this._m[15]
		]
	}
	
	get m00() { return this._m[0]; }
	get m01() { return this._m[1]; }
	get m02() { return this._m[2]; }
	get m03() { return this._m[3]; }
	get m10() { return this._m[4]; }
	get m11() { return this._m[5]; }
	get m12() { return this._m[6]; }
	get m13() { return this._m[7]; }
	get m20() { return this._m[8]; }
	get m21() { return this._m[9]; }
	get m22() { return this._m[10]; }
	get m23() { return this._m[11]; }
	get m30() { return this._m[12]; }
	get m31() { return this._m[13]; }
	get m32() { return this._m[14]; }
	get m33() { return this._m[15]; }
	
	set m00(v) { this._m[0] = v; }
	set m01(v) { this._m[1] = v; }
	set m02(v) { this._m[2] = v; }
	set m03(v) { this._m[3] = v; }
	set m10(v) { this._m[4] = v; }
	set m11(v) { this._m[5] = v; }
	set m12(v) { this._m[6] = v; }
	set m13(v) { this._m[7] = v; }
	set m20(v) { this._m[8] = v; }
	set m21(v) { this._m[9] = v; }
	set m22(v) { this._m[10] = v; }
	set m23(v) { this._m[11] = v; }
	set m30(v) { this._m[12] = v; }
	set m31(v) { this._m[13] = v; }
	set m32(v) { this._m[14] = v; }
	set m33(v) { this._m[15] = v; }


	zero() {
		this._m[ 0] = 0; this._m[ 1] = 0; this._m[ 2] = 0; this._m[ 3] = 0;
		this._m[ 4] = 0; this._m[ 5] = 0; this._m[ 6] = 0; this._m[ 7] = 0;
		this._m[ 8] = 0; this._m[ 9] = 0; this._m[10] = 0; this._m[11] = 0;
		this._m[12] = 0; this._m[13] = 0; this._m[14] = 0; this._m[15] = 0;
		return this;
	}

	identity() {
		this._m[ 0] = 1; this._m[ 1] = 0; this._m[ 2] = 0; this._m[ 3] = 0;
		this._m[ 4] = 0; this._m[ 5] = 1; this._m[ 6] = 0; this._m[ 7] = 0;
		this._m[ 8] = 0; this._m[ 9] = 0; this._m[10] = 1; this._m[11] = 0;
		this._m[12] = 0; this._m[13] = 0; this._m[14] = 0; this._m[15] = 1;
		return this;
	}
	
	isZero() {
		return	this._m[ 0]==0 && this._m[ 1]==0 && this._m[ 2]==0 && this._m[ 3]==0 &&
				this._m[ 4]==0 && this._m[ 5]==0 && this._m[ 6]==0 && this._m[ 7]==0 &&
				this._m[ 8]==0 && this._m[ 9]==0 && this._m[10]==0 && this._m[11]==0 &&
				this._m[12]==0 && this._m[13]==0 && this._m[14]==0 && this._m[15]==0;
	}
	
	isIdentity() {
		return	this._m[ 0]==1 && this._m[ 1]==0 && this._m[ 2]==0 && this._m[ 3]==0 &&
				this._m[ 4]==0 && this._m[ 5]==1 && this._m[ 6]==0 && this._m[ 7]==0 &&
				this._m[ 8]==0 && this._m[ 9]==0 && this._m[10]==1 && this._m[11]==0 &&
				this._m[12]==0 && this._m[13]==0 && this._m[14]==0 && this._m[15]==1;
	}

	row(i) { return new bg.Vector4(this._m[i*4],this._m[i*4 + 1],this._m[i*4 + 2],this._m[i*4 + 3]); }
	setRow(i, row) { this._m[i*4]=row._v[0]; this._m[i*4 + 1]=row._v[1]; this._m[i*4 + 2]=row._v[2]; this._m[i*4 + 3]=row._v[3]; return this; }
	setScale(x,y,z) {
		let rx = new bg.Vector3(this._m[0], this._m[4], this._m[8]).normalize().scale(x);
		let ry = new bg.Vector3(this._m[1], this._m[5], this._m[9]).normalize().scale(y);
		let rz = new bg.Vector3(this._m[2], this._m[6], this._m[10]).normalize().scale(z);
		this._m[0] = rx.x; this._m[4] = rx.y; this._m[8] = rx.z;
		this._m[1] = ry.x; this._m[5] = ry.y; this._m[9] = ry.z;
		this._m[2] = rz.x; this._m[6] = rz.y; this._m[10] = rz.z;
		return this;
	}
	getScale() {
		return new bg.Vector3(
			new bg.Vector3(this._m[0], this._m[4], this._m[8]).module,
			new bg.Vector3(this._m[1], this._m[5], this._m[9]).module,
			new bg.Vector3(this._m[2], this._m[6], this._m[10]).module
		);
	}

	setPosition(pos,y,z) {
		if (typeof(pos)=="number") {
			this._m[12] = pos;
			this._m[13] = y;
			this._m[14] = z;
		}
		else {
			this._m[12] = pos.x;
			this._m[13] = pos.y;
			this._m[14] = pos.z;
		}
		return this;
	}

	get rotation() {
		let scale = this.getScale();
		return new bg.Matrix4(
				this._m[0]/scale.x, this._m[1]/scale.y, this._m[ 2]/scale.z, 0,
				this._m[4]/scale.x, this._m[5]/scale.y, this._m[ 6]/scale.z, 0,
				this._m[8]/scale.x, this._m[9]/scale.y, this._m[10]/scale.z, 0,
				0,	   0,	  0, 	1
			);
	}

	get position() {
		return new bg.Vector3(this._m[12], this._m[13], this._m[14]);
	}
	
	get length() { return this._m.length; }
	
	getMatrix3() {
		return new bg.Matrix3(this._m[0], this._m[1], this._m[ 2],
								this._m[4], this._m[5], this._m[ 6],
								this._m[8], this._m[9], this._m[10]);
	}
	
	perspective(fovy, aspect, nearPlane, farPlane) {
		this.assign(bg.Matrix4.Perspective(fovy, aspect, nearPlane, farPlane));
		return this;
	}
	
	frustum(left, right, bottom, top, nearPlane, farPlane) {
		this.assign(bg.Matrix4.Frustum(left, right, bottom, top, nearPlane, farPlane));
		return this;
	}
	
	ortho(left, right, bottom, top, nearPlane, farPlane) {
		this.assign(bg.Matrix4.Ortho(left, right, bottom, top, nearPlane, farPlane));
		return this;
	}

	lookAt(origin, target, up) {
		this.assign(bg.Matrix4.LookAt(origin,target,up));
		return this;
	}

	translate(x, y, z) {
		this.mult(bg.Matrix4.Translation(x, y, z));
		return this;
	}

	rotate(alpha, x, y, z) {
		this.mult(bg.Matrix4.Rotation(alpha, x, y, z));
		return this;
	}
	
	scale(x, y, z) {
		this.mult(bg.Matrix4.Scale(x, y, z));
		return this;
	}

	elemAtIndex(i) { return this._m[i]; }

	assign(a) {
		if (a.length==9) {
			this._m[0]  = a._m[0]; this._m[1]  = a._m[1]; this._m[2]  = a._m[2]; this._m[3]  = 0;
			this._m[4]  = a._m[3]; this._m[5]  = a._m[4]; this._m[6]  = a._m[5]; this._m[7]  = 0;
			this._m[8]  = a._m[6]; this._m[9]  = a._m[7]; this._m[10] = a._m[8]; this._m[11] = 0;
			this._m[12] = 0;	   this._m[13] = 0;		  this._m[14] = 0;		 this._m[15] = 1;
		}
		else if (a.length==16) {
			this._m[0]  = a._m[0];  this._m[1]  = a._m[1];  this._m[2]  = a._m[2];  this._m[3]  = a._m[3];
			this._m[4]  = a._m[4];  this._m[5]  = a._m[5];  this._m[6]  = a._m[6];  this._m[7]  = a._m[7];
			this._m[8]  = a._m[8];  this._m[9]  = a._m[9];  this._m[10] = a._m[10]; this._m[11] = a._m[11];
			this._m[12] = a._m[12]; this._m[13] = a._m[13];	this._m[14] = a._m[14];	this._m[15] = a._m[15];
		}
		return this;
	}
	
	equals(m) {
		return	this._m[ 0]==m._m[ 0] && this._m[ 1]==m._m[ 1] && this._m[ 2]==m._m[ 2] && this._m[ 3]==m._m[ 3] &&
				this._m[ 4]==m._m[ 4] && this._m[ 5]==m._m[ 5] && this._m[ 6]==m._m[ 6] && this._m[ 7]==m._m[ 7] &&
				this._m[ 8]==m._m[ 8] && this._m[ 9]==m._m[ 9] && this._m[10]==m._m[10] && this._m[11]==m._m[11] &&
				this._m[12]==m._m[12] && this._m[13]==m._m[13] && this._m[14]==m._m[14] && this._m[15]==m._m[15];
	}
	
	notEquals(m) {
		return	this._m[ 0]!=m._m[ 0] || this._m[ 1]!=m._m[ 1] || this._m[ 2]!=m._m[ 2] || this._m[ 3]!=m._m[ 3] ||
				this._m[ 4]!=m._m[ 4] || this._m[ 5]!=m._m[ 5] || this._m[ 6]!=m._m[ 6] || this._m[ 7]!=m._m[ 7] ||
				this._m[ 8]!=m._m[ 8] || this._m[ 9]!=m._m[ 9] || this._m[10]!=m._m[10] || this._m[11]!=m._m[11] ||
				this._m[12]!=m._m[12] || this._m[13]!=m._m[13] || this._m[14]!=m._m[14] || this._m[15]!=m._m[15];
	}
	
	mult(a) {
		if (typeof(a)=='number') {
			this._m[ 0] *= a; this._m[ 1] *= a; this._m[ 2] *= a; this._m[ 3] *= a;
			this._m[ 4] *= a; this._m[ 5] *= a; this._m[ 6] *= a; this._m[ 7] *= a;
			this._m[ 8] *= a; this._m[ 9] *= a; this._m[10] *= a; this._m[11] *= a;
			this._m[12] *= a; this._m[13] *= a; this._m[14] *= a; this._m[15] *= a;
			return this;
		}

		var rm = this._m;
		var lm = a._m;
		var res = new bg.Math.Array(16);
	
		res[0] = lm[ 0] * rm[ 0] + lm[ 1] * rm[ 4] + lm[ 2] * rm[ 8] + lm[ 3] * rm[12];
		res[1] = lm[ 0] * rm[ 1] + lm[ 1] * rm[ 5] + lm[ 2] * rm[ 9] + lm[ 3] * rm[13];
		res[2] = lm[ 0] * rm[ 2] + lm[ 1] * rm[ 6] + lm[ 2] * rm[10] + lm[ 3] * rm[14];
		res[3] = lm[ 0] * rm[ 3] + lm[ 1] * rm[ 7] + lm[ 2] * rm[11] + lm[ 3] * rm[15];
		
		res[4] = lm[ 4] * rm[ 0] + lm[ 5] * rm[ 4] + lm[ 6] * rm[ 8] + lm[ 7] * rm[12];
		res[5] = lm[ 4] * rm[ 1] + lm[ 5] * rm[ 5] + lm[ 6] * rm[ 9] + lm[ 7] * rm[13];
		res[6] = lm[ 4] * rm[ 2] + lm[ 5] * rm[ 6] + lm[ 6] * rm[10] + lm[ 7] * rm[14];
		res[7] = lm[ 4] * rm[ 3] + lm[ 5] * rm[ 7] + lm[ 6] * rm[11] + lm[ 7] * rm[15];
		
		res[8]  = lm[ 8] * rm[ 0] + lm[ 9] * rm[ 4] + lm[10] * rm[ 8] + lm[11] * rm[12];
		res[9]  = lm[ 8] * rm[ 1] + lm[ 9] * rm[ 5] + lm[10] * rm[ 9] + lm[11] * rm[13];
		res[10] = lm[ 8] * rm[ 2] + lm[ 9] * rm[ 6] + lm[10] * rm[10] + lm[11] * rm[14];
		res[11] = lm[ 8] * rm[ 3] + lm[ 9] * rm[ 7] + lm[10] * rm[11] + lm[11] * rm[15];
		
		res[12] = lm[12] * rm[ 0] + lm[13] * rm[ 4] + lm[14] * rm[ 8] + lm[15] * rm[12];
		res[13] = lm[12] * rm[ 1] + lm[13] * rm[ 5] + lm[14] * rm[ 9] + lm[15] * rm[13];
		res[14] = lm[12] * rm[ 2] + lm[13] * rm[ 6] + lm[14] * rm[10] + lm[15] * rm[14];
		res[15] = lm[12] * rm[ 3] + lm[13] * rm[ 7] + lm[14] * rm[11] + lm[15] * rm[15];
	
		this._m = res;
		return this;
	}

	multVector(vec) {
		if (typeof(vec)=='object' && vec._v && vec._v.length>=3) {
			vec = vec._v;
		}
		let x=vec[0];
		let y=vec[1];
		let z=vec[2];
		let w=1.0;
	
		return new bg.Vector4(this._m[0]*x + this._m[4]*y + this._m[ 8]*z + this._m[12]*w,
								this._m[1]*x + this._m[5]*y + this._m[ 9]*z + this._m[13]*w,
								this._m[2]*x + this._m[6]*y + this._m[10]*z + this._m[14]*w,
								this._m[3]*x + this._m[7]*y + this._m[11]*z + this._m[15]*w);
	}
	
	invert() {
		let a00 = this._m[0],  a01 = this._m[1],  a02 = this._m[2],  a03 = this._m[3],
	        a10 = this._m[4],  a11 = this._m[5],  a12 = this._m[6],  a13 = this._m[7],
	        a20 = this._m[8],  a21 = this._m[9],  a22 = this._m[10], a23 = this._m[11],
	        a30 = this._m[12], a31 = this._m[13], a32 = this._m[14], a33 = this._m[15];

	    let b00 = a00 * a11 - a01 * a10,
	        b01 = a00 * a12 - a02 * a10,
	        b02 = a00 * a13 - a03 * a10,
	        b03 = a01 * a12 - a02 * a11,
	        b04 = a01 * a13 - a03 * a11,
	        b05 = a02 * a13 - a03 * a12,
	        b06 = a20 * a31 - a21 * a30,
	        b07 = a20 * a32 - a22 * a30,
	        b08 = a20 * a33 - a23 * a30,
	        b09 = a21 * a32 - a22 * a31,
	        b10 = a21 * a33 - a23 * a31,
	        b11 = a22 * a33 - a23 * a32;

	    let det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;

	    if (!det) {
			this.zero();
	        return this;
	    }
		else {
			det = 1.0 / det;

			this._m[0] = (a11 * b11 - a12 * b10 + a13 * b09) * det;
			this._m[1] = (a02 * b10 - a01 * b11 - a03 * b09) * det;
			this._m[2] = (a31 * b05 - a32 * b04 + a33 * b03) * det;
			this._m[3] = (a22 * b04 - a21 * b05 - a23 * b03) * det;
			this._m[4] = (a12 * b08 - a10 * b11 - a13 * b07) * det;
			this._m[5] = (a00 * b11 - a02 * b08 + a03 * b07) * det;
			this._m[6] = (a32 * b02 - a30 * b05 - a33 * b01) * det;
			this._m[7] = (a20 * b05 - a22 * b02 + a23 * b01) * det;
			this._m[8] = (a10 * b10 - a11 * b08 + a13 * b06) * det;
			this._m[9] = (a01 * b08 - a00 * b10 - a03 * b06) * det;
			this._m[10] = (a30 * b04 - a31 * b02 + a33 * b00) * det;
			this._m[11] = (a21 * b02 - a20 * b04 - a23 * b00) * det;
			this._m[12] = (a11 * b07 - a10 * b09 - a12 * b06) * det;
			this._m[13] = (a00 * b09 - a01 * b07 + a02 * b06) * det;
			this._m[14] = (a31 * b01 - a30 * b03 - a32 * b00) * det;
			this._m[15] = (a20 * b03 - a21 * b01 + a22 * b00) * det;
			return this;
		}
	}
	
	traspose() {
		let r0 = new bg.Vector4(this._m[0], this._m[4], this._m[ 8], this._m[12]);
		let r1 = new bg.Vector4(this._m[1], this._m[5], this._m[ 9], this._m[13]);
		let r2 = new bg.Vector4(this._m[2], this._m[6], this._m[10], this._m[14]);
		let r3 = new bg.Vector4(this._m[3], this._m[7], this._m[11], this._m[15]);
	
		this.setRow(0, r0);
		this.setRow(1, r1);
		this.setRow(2, r2);
		this.setRow(3, r3);
		return this;
	}
	
	transformDirection(/* Vector3 */ dir) {
		let direction = new bg.Vector3(dir);
		let trx = new bg.Matrix4(this);
		trx.setRow(3, new bg.Vector4(0.0, 0.0, 0.0, 1.0));
		direction.assign(trx.multVector(direction).xyz);
		direction.normalize();
		return direction;
	}
	
	get forwardVector() {
		return this.transformDirection(new bg.Vector3(0.0, 0.0, 1.0));
	}
	
	get rightVector() {
		return this.transformDirection(new bg.Vector3(1.0, 0.0, 0.0));
	}
	
	get upVector() {
		return this.transformDirection(new bg.Vector3(0.0, 1.0, 0.0));
	}
	
	get backwardVector() {
		return this.transformDirection(new bg.Vector3(0.0, 0.0, -1.0));
	}
	
	get leftVector() {
		return this.transformDirection(new bg.Vector3(-1.0, 0.0, 0.0));
	}
	
	get downVector() {
		return this.transformDirection(new bg.Vector3(0.0, -1.0, 0.0));
	}
	
	isNan() {
		return	Number.isNaN(this._m[ 0]) || Number.isNaN(this._m[ 1]) || Number.isNaN(this._m[ 2]) || Number.isNaN(this._m[ 3]) ||
				Number.isNaN(this._m[ 4]) || Number.isNaN(this._m[ 5]) || Number.isNaN(this._m[ 6]) || Number.isNaN(this._m[ 7]) ||
				Number.isNaN(this._m[ 8]) || Number.isNaN(this._m[ 9]) || Number.isNaN(this._m[10]) || Number.isNaN(this._m[11]) ||
				Number.isNaN(this._m[12]) || Number.isNaN(this._m[13]) || Number.isNaN(this._m[14]) || Number.isNaN(this._m[15]);
	}
	
	getOrthoValues() {
		return [ (1+get23())/get22(),
				-(1-get23())/get22(),
				 (1-get13())/get11(),
				-(1+get13())/get11(),
				-(1+get03())/get00(),
				 (1-get03())/get00() ];
	}

	getPerspectiveValues() {
		return [ get23()/(get22()-1),
				 get23()/(get22()+1),
				 near * (get12()-1)/get11(),
				 near * (get12()+1)/get11(),
				 near * (get02()-1)/get00(),
				 near * (get02()+1)/get00() ];
	}

	toString() {
		return "[" + this._m[ 0] + ", " + this._m[ 1] + ", " + this._m[ 2] + ", " + this._m[ 3] + "]\n" +
			  " [" + this._m[ 4] + ", " + this._m[ 5] + ", " + this._m[ 6] + ", " + this._m[ 7] + "]\n" +
			  " [" + this._m[ 8] + ", " + this._m[ 9] + ", " + this._m[10] + ", " + this._m[11] + "]\n" +
			  " [" + this._m[12] + ", " + this._m[13] + ", " + this._m[14] + ", " + this._m[15] + "]";
	}
}

bg.Matrix4 = Matrix4;

})();

(function() {
	class Vector {
		static MinComponents(v1,v2) {
			let length = Math.min(v1.length, v2.length);
			let result = null;
			switch (length) {
			case 2:
				result = new bg.Vector2();
				break;
			case 3:
				result = new bg.Vector3();
				break;
			case 4:
				result = new bg.Vector4();
				break;
			}

			for (let i=0; i<length; ++i) {
				result._v[i] = v1._v[i]<v2._v[i] ? v1._v[i] : v2._v[i];
			}
			return result;
		}

		static MaxComponents(v1,v2) {
			let length = Math.min(v1.length, v2.length);
			let result = null;
			switch (length) {
			case 2:
				result = new bg.Vector2();
				break;
			case 3:
				result = new bg.Vector3();
				break;
			case 4:
				result = new bg.Vector4();
				break;
			}

			for (let i=0; i<length; ++i) {
				result._v[i] = v1._v[i]>v2._v[i] ? v1._v[i] : v2._v[i];
			}
			return result;
		}

		constructor(v) {
			this._v = v;
		}
		
		get v() { return this._v; }
		
		get length() { return this._v.length; }
		
		get x() { return this._v[0]; }
		set x(v) { this._v[0] = v; }
		get y() { return this._v[1]; }
		set y(v) { this._v[1] = v; }

		get module() { return this.magnitude(); }

		toArray() {
			let result = [];
			for (let i=0; i<this.v.length; ++i) {
				result.push(this.v[i]);
			}
			return result;
		}
	}
	
	bg.VectorBase = Vector;
	bg.Vector = Vector;
	
	class Vector2 extends Vector {
		static Add(v1,v2) {
			return new Vector2(v1.x + v2.x, v1.y + v2.y);
		}

		static Sub(v1,v2) {
			return new Vector2(v1.x - v2.x, v1.y - v2.y);
		}

		static Distance(a,b) {
			return (new bg.Vector2(a._v[0] - b._v[0], a._v[1] - b._v[1])).magnitude();
		}

		static Dot(v1,v2) {
			return v1._v[0] * v2._v[0] + v1._v[1] * v2._v[1];
		}

		static Cross(v1,v2) {
			let x = v1._v[1] * v2._v[2] - v1._v[2] * v2._v[1];
			let y = v1._v[2] * v2._v[0] - v1._v[0] * v2._v[2];
			let z = v1._v[0] * v2._v[1] - v1._v[1] * v2._v[0];
			return new bg.Vector3(x, y, z);
		}

		constructor(x = 0,y) {
			super(new bg.Math.ArrayHighP(2));
			if (x instanceof Vector2) {
				this._v[0] = x._v[0];
				this._v[1] = x._v[1];
			}
			else if (Array.isArray(x) && x.length>=2) {
				this._v[0] = x[0];
				this._v[1] = x[1];
			}
			else {
				if (y===undefined) y=x;
				this._v[0] = x;
				this._v[1] = y;
			}
		}

		distance(other) {
			let v3 = new bg.Vector2(this._v[0] - other._v[0],
									  this._v[1] - other._v[1]);
			return v3.magnitude();
		}

		normalize() {
			let m = this.magnitude();
			this._v[0] = this._v[0]/m; this._v[1]=this._v[1]/m;
			return this;
		}

		add(v2) {
			this._v[0] += v2._v[0];
			this._v[1] += v2._v[1];
			return this;
		}

		sub(v2) {
			this._v[0] -= v2._v[0];
			this._v[1] -= v2._v[1];
			return this;
		}

		dot(v2) {
			return this._v[0] * v2._v[0] + this._v[1] * v2._v[1];
		}

		scale(scale) {
			this._v[0] *= scale; this._v[1] *= scale;
			return this;
		}

		magnitude() {
			return Math.sqrt(
				this._v[0] * this._v[0] +
				this._v[1] * this._v[1]
			)
		}

		elemAtIndex(i) { return this._v[i]; }
		equals(v) { return this._v[0]==v._v[0] && this._v[1]==v._v[1]; }
		notEquals(v) { return this._v[0]!=v._v[0] || this._v[1]!=v._v[1]; }
		assign(v) { this._v[0]=v._v[0]; this._v[1]=v._v[1]; }

		set(x, y) {
			if (y===undefined) y = x;
			this._v[0] = x; this._v[1] = y;
		}

		get width() { return this._v[0]; }
		get height() { return this._v[1]; }
		set width(v) { this._v[0] = v; }
		set height(v) { this._v[1] = v; }

		get aspectRatio() { return this._v[0]/this._v[1]; }

		isNan() { return isNaN(this._v[0]) || isNaN(this._v[1]); }

		toString() {
			return "[" + this._v + "]";
		}
	}
	
	bg.Vector2 = Vector2;
	
	class Vector3 extends Vector {
		static Add(v1,v2) {
			return new Vector3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
		}

		static Sub(v1,v2) {
			return new Vector3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
		}

		static Distance(a,b) {
			return (new bg.Vector3(a._v[0] - b._v[0], a._v[1] - b._v[1], a._v[2] - b._v[2])).magnitude();
		}

		static Dot(v1,v2) {
			return v1._v[0] * v2._v[0] + v1._v[1] * v2._v[1] + v1._v[2] * v2._v[2];
		}

		static Cross(v1,v2) {
			let x = v1._v[1] * v2._v[2] - v1._v[2] * v2._v[1];
			let y = v1._v[2] * v2._v[0] - v1._v[0] * v2._v[2];
			let z = v1._v[0] * v2._v[1] - v1._v[1] * v2._v[0];
			return new bg.Vector3(x, y, z);
		}


		constructor(x = 0, y = 0, z = 0) {
			super(new bg.Math.ArrayHighP(3));
			if (x instanceof Vector2) {
				this._v[0] = x._v[0];
				this._v[1] = x._v[1];
				this._v[2] = y;
			}
			else if (x instanceof Vector3) {
				this._v[0] = x._v[0];
				this._v[1] = x._v[1];
				this._v[2] = x._v[2];
			}
			else if (Array.isArray(x) && x.length>=3) {
				this._v[0] = x[0];
				this._v[1] = x[1];
				this._v[2] = x[2];
			}
			else {
				if (y===undefined) y=x;
				if (z===undefined) z=y;
				this._v[0] = x;
				this._v[1] = y;
				this._v[2] = z;
			}
		}
		
		get z() { return this._v[2]; }
		set z(v) { this._v[2] = v; }

		magnitude() {
			return Math.sqrt(
				this._v[0] * this._v[0] +
				this._v[1] * this._v[1] +
				this._v[2] * this._v[2]
			);
		}
		
		normalize() {
			let m = this.magnitude();
			this._v[0] = this._v[0]/m; this._v[1]=this._v[1]/m; this._v[2]=this._v[2]/m;
			return this;
		}

		distance(other) {
			let v3 = new bg.Vector3(this._v[0] - other._v[0],
									  this._v[1] - other._v[1],
									  this._v[2] - other._v[2]);
			return v3.magnitude();
		}

		add(v2) {
			this._v[0] += v2._v[0];
			this._v[1] += v2._v[1];
			this._v[2] += v2._v[2];
			return this;
		}

		sub(v2) {
			this._v[0] -= v2._v[0];
			this._v[1] -= v2._v[1];
			this._v[2] -= v2._v[2];
			return this;
		}

		dot(v2) {
			return this._v[0] * v2._v[0] + this._v[1] * v2._v[1] + this._v[2] * v2._v[2];
		}

		scale(scale) {
			this._v[0] *= scale; this._v[1] *= scale; this._v[2] *= scale;
			return this;
		}

		cross(/* Vector3 */ v2) {
			let x = this._v[1] * v2._v[2] - this._v[2] * v2._v[1];
			let y = this._v[2] * v2._v[0] - this._v[0] * v2._v[2];
			let z = this._v[0] * v2._v[1] - this._v[1] * v2._v[0];
			this._v[0]=x; this._v[1]=y; this._v[2]=z;
			return this;
		}

		elemAtIndex(i) { return this._v[i]; }
		equals(v) { return this._v[0]==v._v[0] && this._v[1]==v._v[1] && this._v[2]==v._v[2]; }
		notEquals(v) { return this._v[0]!=v._v[0] || this._v[1]!=v._v[1] || this._v[2]!=v._v[2]; }
		assign(v) { this._v[0]=v._v[0]; this._v[1]=v._v[1]; if (v._v.length>=3) this._v[2]=v._v[2]; }

		set(x, y, z) {
			this._v[0] = x;
			this._v[1] = (y===undefined) ? x:y;
			this._v[2] = (y===undefined) ? x:(z===undefined ? y:z);
		}

		get width() { return this._v[0]; }
		get height() { return this._v[1]; }
		get depth() { return this._v[2]; }
		
		set width(v) { this._v[0] = v; }
		set height(v) { this._v[1] = v; }
		set depth(v) { this._v[2] = v; }

		get xy() { return new bg.Vector2(this._v[0],this._v[1]); }
		get yz() { return new bg.Vector2(this._v[1],this._v[2]); }
		get xz() { return new bg.Vector2(this._v[0],this._v[2]); }

		isNan() { return isNaN(this._v[0]) || isNaN(this._v[1]) || isNaN(this._v[2]); }

		toString() {
			return "[" + this._v + "]";
		}
	}
	
	bg.Vector3 = Vector3;
	
	class Vector4 extends Vector {
		static Add(v1,v2) {
			return new Vector4(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z, v1.w + v2.w);
		}

		static Sub(v1,v2) {
			return new Vector4(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z, v1.w - v2.w);
		}

		static Yellow() { return new bg.Vector4(1.0,1.0,0.0,1.0); }
		static Orange() { return new bg.Vector4(1.0,0.5,0.0,1.0); }
		static Red() { return new bg.Vector4(1.0,0.0,0.0,1.0); }
		static Violet() { return new bg.Vector4(0.5,0.0,1.0,1.0); }
		static Blue() { return new bg.Vector4(0.0,0.0,1.0,1.0); }
		static Green() { return new bg.Vector4(0.0,1.0,0.0,1.0); }
		static White() { return new bg.Vector4(1.0,1.0,1.0,1.0); }
		static LightGray() { return new bg.Vector4(0.8,0.8,0.8,1.0); }
		static Gray() { return new bg.Vector4(0.5,0.5,0.5,1.0); }
		static DarkGray() { return new bg.Vector4(0.2,0.2,0.2,1.0); }
		static Black() { return new bg.Vector4(0.0,0.0,0.0,1.0); }
		static Brown() { return new bg.Vector4(0.4,0.2,0.0,1.0); }
		static Transparent() { return new bg.Vector4(0,0,0,0); }
		
		constructor(x = 0, y = 0, z = 0, w = 0) {
			super(new bg.Math.ArrayHighP(4));
			if (x instanceof Vector2) {
				this._v[0] = x._v[0];
				this._v[1] = x._v[1];
				this._v[2] = y
				this._v[3] = z;
			}
			else if (x instanceof Vector3) {
				this._v[0] = x._v[0];
				this._v[1] = x._v[1];
				this._v[2] = x._v[2];
				this._v[3] = y;
			}
			else if (x instanceof Vector4) {
				this._v[0] = x._v[0];
				this._v[1] = x._v[1];
				this._v[2] = x._v[2];
				this._v[3] = x._v[3];
			}
			else if (Array.isArray(x) && x.length>=4) {
				this._v[0] = x[0];
				this._v[1] = x[1];
				this._v[2] = x[2];
				this._v[3] = x[3];
			}
			else {
				if (y===undefined) y=x;
				if (z===undefined) z=y;
				if (w===undefined) w=z;
				this._v[0] = x;
				this._v[1] = y;
				this._v[2] = z;
				this._v[3] = w;
			}
		}
		
		get z() { return this._v[2]; }
		set z(v) { this._v[2] = v; }
		get w() { return this._v[3]; }
		set w(v) { this._v[3] = v; }
		
		magnitude() {
			return Math.sqrt(
				this._v[0] * this._v[0] +
				this._v[1] * this._v[1] +
				this._v[2] * this._v[2] +
				this._v[3] * this._v[3]
			);
		}

		normalize() {
			let m = this.magnitude();
			this._v[0] = this._v[0]/m;
			this._v[1]=this._v[1]/m;
			this._v[2]=this._v[2]/m;
			this._v[3]=this._v[3]/m;
			return this;
		}

		distance(other) {
			let v3 = new bg.Vector4(this._v[0] - other._v[0],
									  this._v[1] - other._v[1],
									  this._v[2] - other._v[2],
									  this._v[3] - other._v[3]);
			return v3.magnitude();
		}

		add(v2) {
			this._v[0] += v2._v[0];
			this._v[1] += v2._v[1];
			this._v[2] += v2._v[2];
			this._v[3] += v2._v[3];
			return this;
		}

		sub(v2) {
			this._v[0] -= v2._v[0];
			this._v[1] -= v2._v[1];
			this._v[2] -= v2._v[2];
			this._v[3] -= v2._v[3];
			return this;
		}

		dot(v2) {
			return this._v[0] * v2._v[0] + this._v[1] * v2._v[1] + this._v[2] * v2._v[2] + this._v[3] * v2._v[3];
		}

		scale(scale) {
			this._v[0] *= scale; this._v[1] *= scale; this._v[2] *= scale; this._v[3] *= scale;
			return this;
		}

		elemAtIndex(i) { return this._v[i]; }
		equals(v) { return this._v[0]==v._v[0] && this._v[1]==v._v[1] && this._v[2]==v._v[2] && this._v[3]==v._v[3]; }
		notEquals(v) { return this._v[0]!=v._v[0] || this._v[1]!=v._v[1] || this._v[2]!=v._v[2] || this._v[3]!=v._v[3]; }
		assign(v) { this._v[0]=v._v[0]; this._v[1]=v._v[1]; if (v._v.length>=3) this._v[2]=v._v[2]; if (v._v.length==4) this._v[3]=v._v[3]; }

		set(x, y, z, w) {
			this._v[0] = x;
			this._v[1] = (y===undefined) ? x:y;
			this._v[2] = (y===undefined) ? x:(z===undefined ? y:z);
			this._v[3] = (y===undefined) ? x:(z===undefined ? y:(w===undefined ? z:w));
		}

		get r() { return this._v[0]; }
		get g() { return this._v[1]; }
		get b() { return this._v[2]; }
		get a() { return this._v[3]; }
		
		set r(v) { this._v[0] = v; }
		set g(v) { this._v[1] = v; }
		set b(v) { this._v[2] = v; }
		set a(v) { this._v[3] = v; }

		get xy() { return new bg.Vector2(this._v[0],this._v[1]); }
		get yz() { return new bg.Vector2(this._v[1],this._v[2]); }
		get xz() { return new bg.Vector2(this._v[0],this._v[2]); }
		get xyz() { return new bg.Vector3(this._v[0],this._v[1],this._v[2]); }

		get width() { return this._v[2]; }
		get height() { return this._v[3]; }
		
		set width(v) { this._v[2] = v; }
		set height(v) { this._v[3] = v; }
		
		get aspectRatio() { return this._v[3]!=0 ? this._v[2]/this._v[3]:1.0; }

		isNan() { return isNaN(this._v[0]) || isNaN(this._v[1]) || isNaN(this._v[2]) || isNaN(this._v[3]); }

		toString() {
			return "[" + this._v + "]";
		}
	}
	
	bg.Vector4 = Vector4;
	
	bg.Size2D = Vector2;
	bg.Size3D = Vector3;
	bg.Position2D = Vector2;
	bg.Viewport = Vector4;
	bg.Color = Vector4;
	
	class Bounds extends Vector {
		constructor(a=0,b=0,c=0,d=0,e=0,f=0) {
			super(new bg.Math.Array(6));
			this._v[0] = a;
			this._v[1] = b;
			this._v[2] = c;
			this._v[3] = d;
			this._v[4] = e;
			this._v[5] = f;
		}
		
		elemAtIndex(i) { return this._v[i]; }
		equals(v) { return this._v[0]==v._v[0] && this._v[1]==v._v[1] && this._v[2]==v._v[2] && this._v[3]==v._v[3] && this._v[4]==v._v[4] && this._v[5]==v._v[5]; }
		notEquals(v) { return this._v[0]!=v._v[0] || this._v[1]!=v._v[1] || this._v[2]!=v._v[2] || this._v[3]!=v._v[3] || this._v[4]!=v._v[4] || this._v[5]!=v._v[5]; }
		assign(v) { this._v[0]=v._v[0]; this._v[1]=v._v[1]; this._v[2]=v._v[2]; this._v[3]=v._v[3]; this._v[4]=v._v[4]; this._v[5]=v._v[5]; }

		set(left, right, bottom, top, back, front) {
			this._v[0] = left;
			this._v[1] = (right===undefined) ? left:right;
			this._v[2] = (right===undefined) ? left:bottom;
			this._v[3] = (right===undefined) ? left:top;
			this._v[4] = (right===undefined) ? left:back;
			this._v[5] = (right===undefined) ? left:front;
		}

		get left() { return this._v[0]; }
		get right() { return this._v[1]; }
		get bottom() { return this._v[2]; }
		get top() { return this._v[3]; }
		get back() { return this._v[4]; }
		get front() { return this._v[5]; }
		
		set left(v) { this._v[0] = v; }
		set right(v) { this._v[1] = v; }
		set bottom(v) { this._v[2] = v; }
		set top(v) { this._v[3] = v; }
		set back(v) { this._v[4] = v; }
		set front(v) { this._v[5] = v; }

		get width() { return Math.abs(this._v[1] - this._v[0]); }
		get height() { return Math.abs(this._v[3] - this._v[2]); }
		get depth() { return Math.abs(this._v[5] - this._v[4]); }

		isNan() { return isNaN(this._v[0]) || isNaN(this._v[1]) || isNaN(this._v[2]) || isNaN(this._v[3]) || isNaN(this._v[4]) || isNaN(this._v[5]); }

		toString() {
			return "[" + this._v + "]";
		}

		isInBounds(/* vwgl.Vector3*/ v) {
			return v.x>=this._v[0] && v.x<=this._v[1] &&
					v.y>=this._v[2] && v.y<=this._v[3] &&
					v.z>=this._v[4] && v.z<=this._v[5];
		}
	}
	
	bg.Bounds = Bounds;

	class Quaternion extends Vector4 {
		static MakeWithMatrix(m) {
			return new Quaternion(m);
		}
		
		constructor(a,b,c,d) {
			super();
			if (a===undefined) this.zero();
			else if (b===undefined) {
				if (a._v && a._v.lenght>=4) this.clone(a);
				else if(a._m && a._m.length==9) this.initWithMatrix3(a);
				else if(a._m && a._m.length==16) this.initWithMatrix4(a);
				else this.zero();
			}
			else if (a!==undefined && b!==undefined && c!==undefined && d!==undefined) {
				this.initWithValues(a,b,c,d);
			}
			else {
				this.zero();
			}
		}
		
		initWithValues(alpha,x,y,z) {
			this._v[0] = x * bg.Math.sin(alpha/2);
			this._v[1] = y * bg.Math.sin(alpha/2);
			this._v[2] = z * bg.Math.sin(alpha/2);
			this._v[3] = bg.Math.cos(alpha/2);
			return this;
		}
		
		clone(q) {
			this._v[0] = q._v[0];
			this._v[1] = q._v[1];
			this._v[2] = q._v[2];
			this._v[3] = q._v[3];
		}
		
		initWithMatrix3(m) {
			let w = bg.Math.sqrt(1.0 + m._m[0] + m._m[4] + m._m[8]) / 2.0;
			let w4 = 4.0 * w;
			
			this._v[0] = (m._m[7] - m._m[5]) / w;
			this._v[1] = (m._m[2] - m._m[6]) / w4;
			this._v[2] = (m._m[3] - m._m[1]) / w4;
			this._v[3] = w;
		}
		
		initWithMatrix4(m) {
			let w = bg.Math.sqrt(1.0 + m._m[0] + m._m[5] + m._m[10]) / 2.0;
			let w4 = 4.0 * w;
			
			this._v[0] = (m._m[9] - m._m[6]) / w;
			this._v[1] = (m._m[2] - m._m[8]) / w4;
			this._v[2] = (m._m[4] - m._m[1]) / w4;
			this._v[3] = w;	
		}
		
		getMatrix4() {
			let m = bg.Matrix4.Identity();
			let _v = this._v;
			m.setRow(0, new bg.Vector4(1.0 - 2.0*_v[1]*_v[1] - 2.0*_v[2]*_v[2], 2.0*_v[0]*_v[1] - 2.0*_v[2]*_v[3], 2.0*_v[0]*_v[2] + 2.0*_v[1]*_v[3], 0.0));
			m.setRow(1, new bg.Vector4(2.0*_v[0]*_v[1] + 2.0*_v[2]*_v[3], 1.0 - 2.0*_v[0]*_v[0] - 2.0*_v[2]*_v[2], 2.0*_v[1]*_v[2] - 2.0*_v[0]*_v[3], 0.0));
			m.setRow(2, new bg.Vector4(2.0*_v[0]*_v[2] - 2.0*_v[1]*_v[3], 2.0*_v[1]*_v[2] + 2.0*_v[0]*_v[3], 1.0 - 2.0*_v[0]*_v[0] - 2.0*_v[1]*_v[1], 0.0));
			return m;
		}
		
		getMatrix3() {
			let m = bg.Matrix3.Identity();
			let _v = this._v;
			
			m.setRow(0, new bg.Vector3(1.0 - 2.0*_v[1]*_v[1] - 2.0*_v[2]*_v[2], 2.0*_v[0]*_v[1] - 2.0*_v[2]*_v[3], 2.0*_v[0]*_v[2] + 2.0*_v[1]*_v[3]));
			m.setRow(1, new bg.Vector3(2.0*_v[0]*_v[1] + 2.0*_v[2]*_v[3], 1.0 - 2.0*_v[0]*_v[0] - 2.0*_v[2]*_v[2], 2.0*_v[1]*_v[2] - 2.0*_v[0]*_v[3]));
			m.setRow(2, new bg.Vector3(2.0*_v[0]*_v[2] - 2.0*_v[1]*_v[3], 2.0*_v[1]*_v[2] + 2.0*_v[0]*_v[3], 1.0 - 2.0*_v[0]*_v[0] - 2.0*_v[1]*_v[1]));
			return m;
		}
	}
	
	bg.Quaternion = Quaternion;

})();

bg.physics = {};
(function() {
	
	bg.physics.IntersectionType = {
		NONE:0,
		POINT:1,
		LLINE:2
	};
	
	class Intersection {
		
		static RayToPlane(ray,plane) {
			return new bg.physics.RayToPlaneIntersection(ray,plane);
		}
		
		constructor() {
			this._type = null;
			this._p0 = null;
			this._p1 = null;
		}

		get type() { return this._type; }
		get point() { return this._p0; }
		get endPoint() { return this._p1; }
		
		intersects() { return false; }
	}
	
	bg.physics.Intersection = Intersection;

	class RayToPlaneIntersection extends bg.physics.Intersection {
		constructor(ray,plane) {
			super();
			this._ray = null;
			this._p0 = null;
			
			this._type = bg.physics.IntersectionType.POINT;
			let p0 = new bg.Vector3(plane.origin);
			let n = new bg.Vector3(plane.normal);
			let l0 = new bg.Vector3(ray.start);
			let l = new bg.Vector3(ray.vector);
			let num = p0.sub(l0).dot(n);
			let den = l.dot(n);
			
			if (den==0) return;
			let d = num/den;
			if (d>ray.magnitude) return;
			
			this._ray = bg.physics.Ray.RayWithVector(ray.vector,ray.start,d);
			this._p0 = this._ray.end;
		}
		
		get ray() {
			return this._ray;
		}
		
		intersects() {
			return (this._ray!=null && this._p0!=null);
		}
	}
	
	bg.physics.RayToPlaneIntersection = RayToPlaneIntersection;


})();
(function() {
	
	class Joint {
		static Factory(linkData) {
			switch (linkData.type) {
			case 'LinkJoint':
				return LinkJoint.Factory(linkData);
				break;
			}
			return null;
		}

		constructor() {
			this._transform = bg.Matrix4.Identity();
		}
		
		get transform() { return this._transform; }
		set transform(t) { this._transform = t; }
		
		applyTransform(matrix) {
			
		}
		
		calculateTransform() {
			
		}
	}
	
	bg.physics.Joint = Joint;
	
	bg.physics.LinkTransformOrder = {
		TRANSLATE_ROTATE:1,
		ROTATE_TRANSLATE:0
	}
	
	class LinkJoint extends Joint {
		static Factory(data) {
			let result = new LinkJoint();
			result.offset = new bg.Vector3(
				data.offset[0] || 0,
				data.offset[1] || 0,
				data.offset[2] || 0
			);
			result.yaw = data.yaw || 0;
			result.pitch = data.pitch || 0;
			result.roll = data.roll || 0;
			result.order = data.order || 1;
			return result;
		}

		constructor() {
			super();
			this._offset = new bg.Vector3();
			this._eulerRotation = new bg.Vector3();
			
			this._transformOrder = bg.physics.LinkTransformOrder.TRANSLATE_ROTATE;
		}
		
		applyTransform(matrix) {
			matrix.mult(this.transform);
		}
		
		assign(j) {
			this.yaw = j.yaw;
			this.pitch = j.pitch;
			this.roll = j.roll;
			this.offset.assign(j.offset);
			this.transformOrder = j.transformOrder;
		}
		
		get offset() { return this._offset; }
		set offset(o) { this._offset = o; this.calculateTransform(); }
		
		get eulerRotation() { return this._eulerRotation; }
		set eulerRotation(r) { this._eulerRotation = r; this.calculateTransform(); }
		
		get yaw() { return this._eulerRotation.x; }
		get pitch() { return this._eulerRotation.y; }
		get roll() { return this._eulerRotation.z; }
		
		set yaw(y) { this._eulerRotation.x = y; this.calculateTransform(); }
		set pitch(p) { this._eulerRotation.y = p; this.calculateTransform(); }
		set roll(r) { this._eulerRotation.z = r; this.calculateTransform(); }
		
		get transformOrder() { return this._transformOrder; }
		set transformOrder(o) { this._transformOrder = o; this.calculateTransform(); }
		
		multTransform(dst) {
			let offset = this.offset;
			switch (this.transformOrder) {
				case bg.physics.LinkTransformOrder.TRANSLATE_ROTATE:
					dst.translate(offset.x,offset.y,offset.z);
					this.multRotation(dst);
					break;
				case bg.physics.LinkTransformOrder.ROTATE_TRANSLATE:
					this.multRotation(dst);
					dst.translate(offset.x,offset.y,offset.z);
					break;
			}
		}
		
		multRotation(dst) {
			dst.rotate(this.eulerRotation.z, 0,0,1)
			   .rotate(this.eulerRotation.y, 0,1,0)
			   .rotate(this.eulerRotation.x, 1,0,0);
		}
		
		calculateTransform() {
			this.transform.identity();
			this.multTransform(this.transform);
		}

		serialize(data) {
			data.type = "LinkJoint";
			data.offset = [
				this.offset.x,
				this.offset.y,
				this.offset.z
			];
			data.yaw = this.yaw;
			data.pitch = this.pitch;
			data.roll = this.roll;
			data.order = this.order;
		}
	}
	
	bg.physics.LinkJoint = LinkJoint;
})();
(function() {
	
	class Plane {
		// a = normal: create a plane with origin=(0,0,0) and normal=a
		// a = normal, b = origin: create a plane with origin=b and normal=a
		// a = p1, b = p2, c = p3: create a plane that contains the points p1, p2 and p3
		constructor(a, b, c) {
			a = a instanceof bg.Vector3 && a;
			b = b instanceof bg.Vector3 && b;
			c = c instanceof bg.Vector3 && c;
			if (a && !b) {
				this._normal = new bg.Vector3(a);
				this._origin = new bg.Vector3(0);
			}
			else if (a && b && !c) {
				this._normal = new bg.Vector3(a);
				this._origin = new bg.Vector3(b);
			}
			else if (a && b && c) {
				var vec1 = new bg.Vector3(a); vec1.sub(b);
				var vec2 = new bg.Vector3(c); vec2.sub(a);
				this._origin = new bg.Vector3(p1);
				this._normal = new bg.Vector3(vec1);
				this._normal.cross(vec2).normalize();
			}
			else {
				this._origin = new bg.Vector3(0);
				this._normal = new bg.Vector3(0,1,0);
			}
		}

		get normal() { return this._normal; }
		set normal(n) { this._normal.assign(n); }

		get origin() { return this._origin; }
		set origin(o) { this._origin.assign(o); }

		toString() {
			return `P0: ${this._origin.toString()}, normal:${this._normal.toString()}`;
		}

		valid() { return !this._origin.isNan() && !this._normal.isNan(); }

		assign(p) {
			this._origin.assign(p._origin);
			this._normal.assign(p._normal);
			return this;
		}
		
		equals(p) {
			return this._origin.equals(p._origin) && this._normal.equals(p._normal);
		}
	}
	
	bg.physics.Plane = Plane;
	
})();
(function() {

	function calculateVector(ray) {
		ray._vector = new bg.Vector3(ray._end);
		ray._vector.sub(ray._start);
		ray._magnitude = ray._vector.magnitude();
		ray._vector.normalize();
	}
	
	class Ray {
		static RayWithPoints(start,end) {
			return new Ray(start,end);
		}
		
		static RayWithVector(vec,origin,maxDepth) {
			let r = new Ray();
			r.setWithVector(vec,origin,maxDepth);
			return r;
		}
		
		static RayWithScreenPoint(screenPoint,projMatrix,viewMatrix,viewport) {
			let r = new Ray();
			r.setWithScreenPoint(screenPoint,projMatrix,viewMatrix,viewport);
			return r;
		}
		
		
		constructor(start,end) {
			this._start = start || new bg.Vector3();
			this._end = end || new bg.Vector3(1);
			calculateVector(this);
		}
		
		setWithPoints(start,end) {
			this._start.assign(start);
			this._end.assign(end);
			calculateVector();
			return this;
		}
		
		setWithVector(vec,origin,maxDepth) {
			this._start.assign(origin);
			this._end.assign(origin);
			let vector = new bg.Vector3(vec);
			vector.normalize().scale(maxDepth);
			this._end.add(vector);
			calculateVector(this);
			return this;
		}
		
		setWithScreenPoint(screenPoint,projMatrix,viewMatrix,viewport) {
			let start = bg.Matrix4.Unproject(screenPoint.x, screenPoint.y, 0, viewMatrix, projMatrix, viewport);
			let end = bg.Matrix4.Unproject(screenPoint.x, screenPoint.y, 1, viewMatrix, projMatrix, viewport);
			this._start = start.xyz;
			this._end = end.xyz;
			calculateVector(this);
			return this;
		}
		
		assign(r) {
			this._start.assign(r.start);
			this._end.assign(r.end);
			this._vector.assign(r.vector);
			this._magnitude.assign(r.magnitude);
		}
		
		get start() { return this._start; }
		set start(s) { this._start.assign(s); calculateVector(this); }
		
		get end() { return this._end; }
		set end(e) { this._end.assign(e); }
		
		get vector() { return this._vector; }
		
		get magnitude() { return this._magnitude; }
		
		toString() {
			return `start: ${this.start.toString()}, end: ${this.end.toString()}`;
		}
	}
	
	bg.physics.Ray = Ray;
	
})()

bg.scene = {};

(function() {
	
	let s_componentRegister = {};
	
	class Component extends bg.LifeCycle {
		static Factory(context,componentData,node,url) {
			let Constructor = s_componentRegister[componentData.type];
			if (Constructor) {
				let instance = new Constructor();
				node.addComponent(instance);
				return instance.deserialize(context,componentData,url);
			}
			else {
				return Promise.resolve();
			}
			
		}
		
		constructor() {
			super();
			
			this._node = null;

			this._drawGizmo = true;
		}
		
		clone() {
			bg.log(`WARNING: Component with typeid ${this.typeId} does not implmement the clone() method.`);
			return null;
		}

		destroy() {
			
		}
		
		get node() { return this._node; }
		
		get typeId() { return this._typeId; }

		get draw3DGizmo() { return this._drawGizmo; }
		set draw3DGizmo(d) { this._drawGizmo = d; }
		
		removedFromNode(node) {}
		addedToNode(node) {}
		
		// Override this to prevent serialize this component
		get shouldSerialize() { return true; }
		
		deserialize(context,sceneData,url) {
			return Promise.resolve(this);
		}

		// componentData: the current json object corresponding to the parent node
		// promises: array of promises. If the component needs to execute asynchronous
		//			 actions, it can push one or more promises into this array
		// url: the destination scene url, composed by:
		//	{
		//		path: "scene path, using / as separator even on Windows",
		//		fileName: "scene file name" 
		//	}
		serialize(componentData,promises,url) {
			componentData.type = this.typeId.split(".").pop();
		}
		
		// The following functions are implemented in the SceneComponent class, in the C++ API
		component(typeId) { return this._node && this._node.component(typeId); }
		
		get camera() { return this.component("bg.scene.Camera"); }
		get chain() { return this.component("bg.scene.Chain"); }
		get drawable() { return this.component("bg.scene.Drawable"); }
		get inputChainJoint() { return this.component("bg.scene.InputChainJoint"); }
		get outputChainJoint() { return this.component("bg.scene.OutputChainJoint"); }
		get light() { return this.component("bg.scene.Light"); }
		get transform() { return this.component("bg.scene.Transform"); }
	}
	
	bg.scene.Component = Component;
	
	bg.scene.registerComponent = function(namespace,componentClass,identifier) {
		let result = /function (.+)\(/.exec(componentClass.toString());
		if (!result) {
			result = /class ([a-zA-Z0-9_]+) /.exec(componentClass.toString());
		}
		let funcName = (result && result.length>1) ? result[1] : "";
		
		namespace[funcName] = componentClass;
		componentClass.prototype._typeId = identifier || funcName;
		
		
		s_componentRegister[funcName] = componentClass;
	}
	
})();
(function() {
	class SceneObjectLifeCycle extends bg.LifeCycle {
		// This class reimplements the bg.app.ContextObject due to the lack
		// in JavaScript of multiple ihneritance
		
		constructor(context) {
			super(context);
			
			this._context = context;
		}
		
		get context() { return this._context; }
		set context(c) { this._context = c; }
	}
	
	function updateComponentsArray() {
		this._componentsArray = [];
		for (let key in this._components) {
			this._components[key] && this._componentsArray.push(this._components[key]);
		}
	}

	class SceneObject extends SceneObjectLifeCycle {
		
		constructor(context,name="") {
			super(context);
			
			this._name = name;
			this._enabled = true;
			this._steady = false;
			
			this._components = {};
			this._componentsArray = [];
		}

		toString() {
			return " scene object: " + this._name;
		}
		
		// Create a new instance of this node, with a copy of all it's components
		cloneComponents() {
			let newNode = new bg.scene.Node(this.context,this.name ? `copy of ${this.name}`:"");
			newNode.enabled = this.enabled;
			this.forEachComponent((comp) => {
				newNode.addComponent(comp.clone());
			});
			return newNode;
		}
		
		get name() { return this._name; }
		set name(n) { this._name = n; }
		
		get enabled() { return this._enabled; }
		set enabled(e) { this._enabled = e; }

		get steady() { return this._steady; }
		set steady(s) { this._steady = s; }
		
		addComponent(c) {
			if (c._node) {
				c._node.removeComponent(c);
			}
			c._node = this;
			this._components[c.typeId] = c;
			c.addedToNode(this);
			updateComponentsArray.apply(this);
		}
		
		// It's possible to remove a component by typeId or by the specific object.
		//	- typeId: if the scene object contains a component of this type, will be removed
		// 	- specific object: if the scene object contains the specified object, will be removed
		removeComponent(findComponent) {
			let typeId = "";
			let comp = null;
			if (typeof(findComponent)=="string") {
				typeId = findComponent
				comp = this.component(findComponent);
			}
			else if (findComponent instanceof bg.scene.Component) {
				comp = findComponent;
				typeId = findComponent.typeId;
			}
			
			let status = false;
			if (this._components[typeId]==comp && comp!=null) {
				delete this._components[typeId];
				comp.removedFromNode(this);
				status = true;
			}

			updateComponentsArray.apply(this);
			return status;
		}
		
		component(typeId) {
			return this._components[typeId];
		}
		
		// Most common components
		get camera() { return this.component("bg.scene.Camera"); }
		get chain() { return this.component("bg.scene.Chain"); }
		get drawable() { return this.component("bg.scene.Drawable"); }
		get inputJoint() { return this.component("bg.scene.InputJoint"); }
		get outputJoint() { return this.component("bg.scene.OutputJoint"); }
		get light() { return this.component("bg.scene.Light"); }
		get transform() { return this.component("bg.scene.Transform"); }
		
		forEachComponent(callback) {
			this._componentsArray.forEach(callback);
		}
		
		someComponent(callback) {
			return this._componentsArray.some(callback);
		}
		
		everyComponent(callback) {
			return this._componentsArray.every(callback);
		}
		
		destroy() {
			this.forEachComponent((comp) => {
				comp.removedFromNode(this);
			});
			
			this._components = {};
			this._componentsArray = [];
		}
		
		init() {
			this._componentsArray.forEach((comp) => {
				comp.init();
			});
		}
		
		frame(delta) {
			this._componentsArray.forEach((comp) => {
				if (!comp._initialized_) {
					comp.init();
					comp._initialized_ = true;
				}
				comp.frame(delta);
			});
		}
		
		displayGizmo(pipeline,matrixState) {
			this._componentsArray.forEach((comp) => {
				if (comp.draw3DGizmo) comp.displayGizmo(pipeline,matrixState);
			});
		}

		/////// Direct rendering methods: will be deprecated soon
		willDisplay(pipeline,matrixState) {
			this._componentsArray.forEach((comp) => {
				comp.willDisplay(pipeline,matrixState);
			});
		}
		
		display(pipeline,matrixState,forceDraw=false) {
			this._componentsArray.forEach((comp) => {
				comp.display(pipeline,matrixState,forceDraw);
			});
		}

		
		didDisplay(pipeline,matrixState) {
			this._componentsArray.forEach((comp) => {
				comp.didDisplay(pipeline,matrixState);
			});
		}
		//////// End direct rendering methods


		////// Render queue methods
		willUpdate(modelMatrixStack,viewMatrixStack,projectionMatrixStack) {
			this._componentsArray.forEach((comp) => {
				comp.willUpdate(modelMatrixStack,viewMatrixStack,projectionMatrixStack);
			});
		}

		draw(renderQueue,modelMatrixStack,viewMatrixStack,projectionMatrixStack) {
			this._componentsArray.forEach((comp) => {
				comp.draw(renderQueue,modelMatrixStack,viewMatrixStack,projectionMatrixStack);
			});
		}

		didUpdate(modelMatrixStack,viewMatrixStack,projectionMatrixStack) {
			this._componentsArray.forEach((comp) => {
				comp.didUpdate(modelMatrixStack,viewMatrixStack,projectionMatrixStack);
			});
		}
		////// End render queue methods

		
		reshape(pipeline,matrixState,width,height) {
			this._componentsArray.forEach((comp) => {
				comp.reshape(width,height);
			});
		}
		
		keyDown(evt) {
			this._componentsArray.forEach((comp) => {
				comp.keyDown(evt);
			});
		}
		
		keyUp(evt) {
			this._componentsArray.forEach((comp) => {
				comp.keyUp(evt);
			});
		}
		
		mouseUp(evt) {
			this._componentsArray.forEach((comp) => {
				comp.mouseUp(evt);
			});
		}
		
		mouseDown(evt) {
			this._componentsArray.forEach((comp) => {
				comp.mouseDown(evt);
			});
		}
		
		mouseMove(evt) {
			this._componentsArray.forEach((comp) => {
				comp.mouseMove(evt);
			});
		}
		
		mouseOut(evt) {
			this._componentsArray.forEach((comp) => {
				comp.mouseOut(evt);
			});
		}
		
		mouseDrag(evt) {
			this._componentsArray.forEach((comp) => {
				comp.mouseDrag(evt);
			});
		}
		
		mouseWheel(evt) {
			this._componentsArray.forEach((comp) => {
				comp.mouseWheel(evt);
			});
		}
		
		touchStart(evt) {
			this._componentsArray.forEach((comp) => {
				comp.touchStart(evt);
			});
		}
		
		touchMove(evt) {
			this._componentsArray.forEach((comp) => {
				comp.touchMove(evt);
			});
		}
		
		touchEnd(evt) {
			this._componentsArray.forEach((comp) => {
				comp.touchEnd(evt);
			});
		}
	}
	
	bg.scene.SceneObject = SceneObject;
	
})();
(function() {
	
	function isNodeAncient(node, ancient) {
		if (!node || !ancient) {
			return false;
		}
		else if (node._parent==ancient) {
			return true;
		}
		else {
			return isNodeAncient(node._parent, ancient);
		}
	}

	function cleanupNode(sceneNode) {
        let components = [];
        let children = [];
        sceneNode.forEachComponent((c) => components.push(c));
        sceneNode.children.forEach((child) => children.push(child));
        components.forEach((c) => sceneNode.removeComponent(c));
        children.forEach((child) => {
            sceneNode.removeChild(child);
            cleanupNode(child);
        });
    }
	
	class Node extends bg.scene.SceneObject {
		// IMPORTANT: call this function to clean all the resources of
		// a node if you don't want to use it anymore.
		static CleanupNode(node) {
			cleanupNode(node);
		}

		constructor(context,name="") {
			super(context,name);
			
			this._children = [];
			this._parent = null;
		}

		toString() {
			return super.toString() + " (" + this._children.length + " children and " + Object.keys(this._components).length + " components)";
		}
		
		addChild(child) {
			if (child && !this.isAncientOf(child) && child!=this) {
				if (child.parent) {
					child.parent.removeChild(child);
				}
				this._children.push(child);
				child._parent = this;
			}
		}
		
		removeChild(node) {
			let index = this._children.indexOf(node);
			if (index>=0) {
				this._children.splice(index,1);
			}
		}
		
		get children() { return this._children; }
		
		get parent() { return this._parent; }
		
		get sceneRoot() {
			if (this._parent) {
				return this._parent.sceneRoot();
			}
			return this;
		}
		
		haveChild(node) {
			return this._children.indexOf(node)!=-1;
		}
		
		isAncientOf(node) {
			isNodeAncient(this,node);
		}
		
		accept(nodeVisitor) {
			if (!nodeVisitor.ignoreDisabled || this.enabled) {
				nodeVisitor.visit(this);
				this._children.forEach((child) => {
					child.accept(nodeVisitor);
				});
				nodeVisitor.didVisit(this);
			}
		}
		
		acceptReverse(nodeVisitor) {
			if (!nodeVisitor.ignoreDisabled || this.enabled) {
				if (this._parent) {
					this._parent.acceptReverse(nodeVisitor);
				}
				nodeVisitor.visit(this);
			}
		}
		
		destroy() {
			super.destroy();
			this._children.forEach((child) => {
				child.destroy();
			});
			this._children = [];
		}
		
	}
	
	bg.scene.Node = Node;
	
	class NodeVisitor {
		constructor() {
			this._ignoreDisabled = true;
		}

		get ignoreDisabled() { return this._ignoreDisabled; }

		set ignoreDisabled(v) { this._ignoreDisabled = v; }

		visit(node) {}
		didVisit(node) {}
	}
	
	bg.scene.NodeVisitor = NodeVisitor;
})();
(function() {
	
	class ProjectionStrategy extends bg.MatrixStrategy {
		static Factory(jsonData) {
			let result = null;
			if (jsonData) {
				if (jsonData.type=="PerspectiveProjectionMethod") {
					result = new PerspectiveProjectionStrategy();
				}
				else if (jsonData.type=="OpticalProjectionMethod") {
					result = new OpticalProjectionStrategy();
				}
				else if (jsonData.type=="OrthographicProjectionStrategy") {
					result = new OrthographicProjectionStrategy();
				}

				if (result) {
					result.deserialize(jsonData);
				}
			}
			return result;
		}

		constructor(target) {
			super(target);

			this._near = 0.1;
			this._far = 100.0;
			this._viewport = new bg.Viewport(0,0,512,512);
		}

		clone() { console.log("WARNING: ProjectionStrategy::clone() method not implemented by child class."); }

		get near() { return this._near; }
		set near(n) { this._near = n; }
		get far() { return this._far; }
		set far(f) { this._far = f; }
		get viewport() { return this._viewport; }
		set viewport(vp) { this._viewport = vp; }

		get fov() { return 0; }

		serialize(jsonData) {
			jsonData.near = this.near;
			jsonData.far = this.far;
		}
	}

	bg.scene.ProjectionStrategy = ProjectionStrategy;

	class PerspectiveProjectionStrategy extends ProjectionStrategy {
		constructor(target) {
			super(target);
			this._fov = 60;
		}

		clone() {
			let result = new PerspectiveProjectionStrategy();
			result.near = this.near;
			result.far = this.far;
			result.viewport = this.viewport;
			result.fov = this.fov;
			return result;
		}

		get fov() { return this._fov; }
		set fov(f) { this._fov = f; }

		apply() {
			if (this.target) {
				this.target.perspective(this.fov, this.viewport.aspectRatio, this.near, this.far);
			}
		}

		deserialize(jsonData) {
			this.near = jsonData.near;
			this.far = jsonData.far;
			this.fov = jsonData.fov;
		}

		serialize(jsonData) {
			jsonData.type = "PerspectiveProjectionMethod";
			jsonData.fov = this.fov;
			super.serialize(jsonData);
		}
	}

	bg.scene.PerspectiveProjectionStrategy = PerspectiveProjectionStrategy;

	class OpticalProjectionStrategy extends ProjectionStrategy {
		constructor(target) {
			super(target);
			this._focalLength = 50;
			this._frameSize = 35;
		}

		clone() {
			let result = new OpticalProjectionStrategy();
			result.near = this.near;
			result.far = this.far;
			result.viewport = this.viewport;
			result.focalLength = this.focalLength;
			result.frameSize = this.frameSize;
			return result;
		}

		get focalLength() { return this._focalLength; }
		set focalLength(fl) { this._focalLength = fl; }
		get frameSize() { return this._frameSize; }
		set frameSize(s) { this._frameSize = s; }

		get fov() {
			return 2 * bg.Math.atan(this.frameSize / (this.focalLength / 2));
		}

		apply() {
			if (this.target) {
				let fov = this.fov;
				fov = bg.Math.radiansToDegrees(fov);
				this.target.perspective(fov, this.viewport.aspectRatio, this.near, this.far);
			}
		}

		deserialize(jsonData) {
			this.frameSize = jsonData.frameSize;
			this.focalLength = jsonData.focalLength;
			this.near = jsonData.near;
			this.far = jsonData.far;
		}

		serialize(jsonData) {
			jsonData.type = "OpticalProjectionMethod";
			jsonData.frameSize = this.frameSize;
			jsonData.focalLength = this.focalLength;
			super.serialize(jsonData);
		}
	}

	bg.scene.OpticalProjectionStrategy = OpticalProjectionStrategy;

	class OrthographicProjectionStrategy extends ProjectionStrategy {
		constructor(target) {
			super(target);
			this._viewWidth = 100;
		}

		clone() {
			let result = new OrthographicProjectionStrategy();
			result.near = this.near;
			result.far = this.far;
			result.viewWidth = this.viewWidth;
			return result;
		}

		get viewWidth() { return this._viewWidth; }
		set viewWidth(w) { this._viewWidth = w; }

		apply() {
			if (this.target) {
				let ratio = this.viewport.aspectRatio;
				let height = this.viewWidth / ratio;
				let x = this.viewWidth / 2;
				let y = height / 2;
				this.target.ortho(-x, x, -y, y, -this._far, this._far);
			}
		}

		deserialize(jsonData) {
			this.viewWidth = jsonData.viewWidth;
			this.near = jsonData.near;
			this.far = jsonData.far;
		}

		serialize(jsonData) {
			jsonData.type = "OrthographicProjectionStrategy";
			jsonData.viewWidth = this.viewWidth;
			jsonData.near = this.near;
			jsonData.far = this.far;
			super.serialize(jsonData);
		}
	}

	bg.scene.OrthographicProjectionStrategy = OrthographicProjectionStrategy;

	function buildPlist(context,vertex,color) {
		let plist = new bg.base.PolyList(context);
		let normal = [];
		let texCoord0 = [];
		let index = [];
		let currentIndex = 0;
		for (let i=0; i<vertex.length; i+=3) {
			normal.push(0); normal.push(0); normal.push(1);
			texCoord0.push(0); texCoord0.push(0);
			index.push(currentIndex++);
		}
		plist.vertex = vertex;
		plist.normal = normal;
		plist.texCoord0 = texCoord0;
		plist.color = color;
		plist.index = index;
		plist.drawMode = bg.base.DrawMode.LINES;
		plist.build();
		return plist;
	}

	function getGizmo() {
		if (!this._gizmo) {
			let alpha = this.projectionStrategy ? this.projectionStrategy.fov : bg.Math.PI_4;
			alpha *= 0.5;
			let d = this.focus;
			let aspectRatio = bg.app.MainLoop.singleton.canvas.width / bg.app.MainLoop.singleton.canvas.height;
			let sx = bg.Math.sin(alpha) * d;
			let sy = (bg.Math.sin(alpha) * d) / aspectRatio;
			let vertex = [
				0, 0, 0, sx, sy, -d, 0, 0, 0, -sx, sy, -d, 0, 0, 0, sx,-sy, -d, 0, 0, 0, -sx,-sy, -d,

				sx,sy,-d, -sx,sy,-d, -sx,sy,-d, -sx,-sy,-d, -sx,-sy,-d, sx,-sy,-d, sx,-sy,-d, sx,sy,-d
			];
			let color = [
				1,0,0,1, 1,0,0,1, 1,0,0,1, 1,0,0,1, 1,0,0,1, 1,0,0,1, 1,0,0,1, 1,0,0,1,
				1,0,0,1, 1,0,0,1, 1,0,0,1, 1,0,0,1, 1,0,0,1, 1,0,0,1, 1,0,0,1, 1,0,0,1
			];
			this._gizmo = buildPlist(this.node.context,vertex,color);
		}
		return this._gizmo;
	}

	function clearMain(node) {
		if (node.camera) {
			node.camera.isMain = false;
		}
		node.children.forEach((child) => clearMain(child));
	}

	class Camera extends bg.scene.Component {

		static SetAsMainCamera(mainCamera,sceneRoot) {
			clearMain(sceneRoot);
			if (mainCamera instanceof Camera) {
				mainCamera.isMain = true;
			}
			else if (mainCamera instanceof bg.scene.Node && mainCamera.camera) {
				mainCamera.camera.isMain = true;
			}
			else {
				throw new Error("Error setting main camera: invalid camera node.");
			}
		}

		constructor() {
			super();
			
			this._projection = bg.Matrix4.Perspective(60,1,0.1,100.0);
			this._viewport = new bg.Viewport(0,0,512,512);
			
			this._visitor = new bg.scene.TransformVisitor();
			this._rebuildTransform = true;

			this._position = new bg.Vector3(0);
			this._rebuildPosition = true;
			
			this._clearBuffers = bg.base.ClearBuffers.COLOR_DEPTH;
			
			this._focus = 5;	// default 5 meters

			this._projectionStrategy = null;

			this._isMain = false;
		}
		
		clone() {
			let newCamera = new bg.scene.Camera();
			newCamera._projection = new bg.Matrix4(this._projection);
			newCamera._viewport = new bg.Matrix4(this._viewport);
			newCamera._projectionStrategy = this._projectionStrategy ? this._projectionStrategy.clone() : null;
			return newCamera;
		}
		
		get projection() { return this._projection; }
		set projection(p) {
			if (!this._projectionStrategy) {
				this._projection = p;
			}
		}
		
		get viewport() { return this._viewport; }
		set viewport(v) {
			this._viewport = v;
			if (this._projectionStrategy) {
				this._projectionStrategy.viewport = v;
				this._projectionStrategy.apply();
			}
		}
		
		get focus() { return this._focus; }
		set focus(f) { this._focus = f; this.recalculateGizmo() }

		get isMain() { return this._isMain; }
		set isMain(m) {
			this._isMain = m;
		}

		get projectionStrategy() { return this._projectionStrategy; }
		set projectionStrategy(ps) {
			this._projectionStrategy = ps;
			if (this._projectionStrategy) {
				this._projectionStrategy.target = this._projection;
			}
			this.recalculateGizmo()
		}
		
		get clearBuffers() { return this._clearBuffers; }
		set clearBuffers(c) { this._clearBuffers = c; }
		
		get modelMatrix() {
			if (this._rebuildTransform && this.node) {
				this._visitor.matrix.identity();
				this.node.acceptReverse(this._visitor);
				this._rebuildTransform = false;
			}
			return this._visitor.matrix;
		}
		
		get viewMatrix() {
			if (!this._viewMatrix || this._rebuildTransform) {
				this._viewMatrix = new bg.Matrix4(this.modelMatrix);
				this._viewMatrix.invert();
			}
			return this._viewMatrix;
		}

		get worldPosition() {
			if (this._rebuildPosition) {
				this._position = this.modelMatrix.multVector(new bg.Vector3(0)).xyz
				this._rebuildPosition = false;
				this._rebuildTransform = true;
			}
			return this._position;
		}

		recalculateGizmo() {
			if (this._gizmo) {
				this._gizmo.destroy();
				this._gizmo = null;
			}
		}
		
		frame(delta) {
			this._rebuildPosition = true;
			this._rebuildTransform = true;
		}

		displayGizmo(pipeline,matrixState) {
			if (this.isMain) return; // Do not render the main camera plist
			let plist = getGizmo.apply(this);
			if (plist) {
				pipeline.draw(plist);
			}
		}

		serialize(componentData,promises,url) {
			super.serialize(componentData,promises,url);
			componentData.isMain = this.isMain;
			if (this.projectionStrategy) {
				let projMethod = {};
				componentData.projectionMethod = projMethod;
				this.projectionStrategy.serialize(projMethod);
			}
		}

		deserialize(context,sceneData,url) {
			sceneData.isMain = sceneData.isMain || false;
			this.projectionStrategy = ProjectionStrategy.Factory(sceneData.projectionMethod || {});
		}
	}
	
	bg.scene.registerComponent(bg.scene,Camera,"bg.scene.Camera");
})();
(function() {

	let GizmoType = {
		IN_JOINT: 0,
		OUT_JOINT: 1
	}


	function buildPlist(context,vertex,color) {
		let plist = new bg.base.PolyList(context);
		let normal = [];
		let texCoord0 = [];
		let index = [];
		let currentIndex = 0;
		for (let i=0; i<vertex.length; i+=3) {
			normal.push(0); normal.push(0); normal.push(1);
			texCoord0.push(0); texCoord0.push(0);
			index.push(currentIndex++);
		}
		plist.vertex = vertex;
		plist.normal = normal;
		plist.texCoord0 = texCoord0;
		plist.color = color;
		plist.index = index;
		plist.drawMode = bg.base.DrawMode.LINES;
		plist.build();
		return plist;
	}

	
	function getGizmo(type) {
		if (!this._gizmo) {
			let s = 0.5;
			let vertex = [
				s, 0, 0,   -s, 0, 0,
				0, s, 0,   0, -s, 0,
				0, 0, s,   0, 0, -s
			];
			let color = [
				1,0,0,1, 1,0,0,1, 0,1,0,1, 0,1,0,1, 0,0,1,1, 0,0,1,1
			];
			this._gizmo = buildPlist(this.node.context,vertex,color);
		}
		return this._gizmo;
	}
	
	function updateJointTransforms() {
		if (this.node) {
			let matrix = bg.Matrix4.Identity();
			this.node.children.forEach((child, index) => {
				let trx = child.component("bg.scene.Transform");
				let inJoint = child.component("bg.scene.InputChainJoint");
				let outJoint = child.component("bg.scene.OutputChainJoint");
				
				if (index>0 && inJoint) {
					inJoint.joint.applyTransform(matrix);
				}
				else {
					matrix.identity();
				}
				
				if (trx) {
					trx.matrix.assign(matrix);
				}
				
				if (outJoint) {
					outJoint.joint.applyTransform(matrix);
				}
			});
		}
	}

	class Chain extends bg.scene.Component {
		constructor() {
			super();
		}
		
		clone() {
			return new bg.scene.Chain();
		}


		////// Direct rendering functions: will be deprecated soon
		willDisplay(pipeline,matrixState,projectionMatrixStack) {
			updateJointTransforms.apply(this);
		}

		////// Render queue functions
		willUpdate(modelMatrixStack,viewMatrixStack,projectionMatrixStack) {
			updateJointTransforms.apply(this);
		}
	}
	
	bg.scene.registerComponent(bg.scene,Chain,"bg.scene.Chain");
	
	class ChainJoint extends bg.scene.Component {
		constructor() {
			super();
			
			this._joint = new bg.physics.LinkJoint();
		}
		
		get joint() { return this._joint; }
		set joint(j) { this._joint = j; }

		deserialize(context,sceneData,url) {
			if (sceneData.joint) {
				this.joint = bg.physics.Joint.Factory(sceneData.joint);
			}
		}
	}
	
	bg.scene.ChainJoint = ChainJoint;
	
	class InputChainJoint extends ChainJoint {
		constructor(joint) {
			super();
			if (joint) {
				this.joint = joint;
			}
			else {
				this.joint.transformOrder = bg.physics.LinkTransformOrder.ROTATE_TRANSLATE;
			}
		}
		
		clone() {
			let newJoint = new bg.scene.InputChainJoint();
			newJoint.joint.assign(this.joint);
			return newJoint;
		}

		displayGizmo(pipeline,matrixState) {
			let plist = getGizmo.apply(this,[0]);
			if (plist) {
				matrixState.modelMatrixStack.push();
				let mat = new bg.Matrix4(this.joint.transform);
				mat.invert();
				matrixState.modelMatrixStack.mult(mat);
				pipeline.draw(plist);
				matrixState.modelMatrixStack.pop();
			}
		}

		serialize(componentData,promises,url) {
			super.serialize(componentData,promises,url);
			componentData.joint = {};
			this.joint.serialize(componentData.joint);
		}
	}
	
	bg.scene.registerComponent(bg.scene,InputChainJoint,"bg.scene.InputChainJoint");
	
	
	class OutputChainJoint extends ChainJoint {
		constructor(joint) {
			super();
			if (joint) {
				this.joint = joint;
			}
			else {
				this.joint.transformOrder = bg.physics.LinkTransformOrder.TRANSLATE_ROTATE;
			}
		}
		
		clone() {
			let newJoint = new bg.scene.OutputChainJoint();
			newJoint.joint.assign(this.joint);
			return newJoint;
		}

		displayGizmo(pipeline,matrixState) {
			let plist = getGizmo.apply(this,[1]);
			if (plist) {
				matrixState.modelMatrixStack.push();
				let mat = new bg.Matrix4(this.joint.transform);
				matrixState.modelMatrixStack.mult(mat);
				pipeline.draw(plist);
				matrixState.modelMatrixStack.pop();
			}
		}

		serialize(componentData,promises,url) {
			super.serialize(componentData,promises,url);
			componentData.joint = {};
			this.joint.serialize(componentData.joint);
		}
	}
	
	bg.scene.registerComponent(bg.scene,OutputChainJoint,"bg.scene.OutputChainJoint");
	
})();
(function() {
    bg.scene.CubemapImage = {
        POSITIVE_X: 0,
        NEGATIVE_X: 1,
        POSITIVE_Y: 2,
        NEGATIVE_Y: 3,
        POSITIVE_Z: 4,
        NEGATIVE_Z: 5,
    };

    let g_currentCubemap = null;
    let g_irradianceCubemap = null;
    let g_specularCubemap = [];
    let g_irradianceIntensity = 1.0;

    function copyCubemapImage(componentData,cubemapImage,dstPath) {
        let path = require("path");
        let src = bg.base.Writer.StandarizePath(this.getImageUrl(cubemapImage));
        let file = src.split('/').pop();
        let dst = bg.base.Writer.StandarizePath(path.join(dstPath,file));
        switch (cubemapImage) {
        case bg.scene.CubemapImage.POSITIVE_X:
            componentData.positiveX = file;
            break;
        case bg.scene.CubemapImage.NEGATIVE_X:
            componentData.negativeX = file;
            break;
        case bg.scene.CubemapImage.POSITIVE_Y:
            componentData.positiveY = file;
            break;
        case bg.scene.CubemapImage.NEGATIVE_Y:
            componentData.negativeY = file;
            break;
        case bg.scene.CubemapImage.POSITIVE_Z:
            componentData.positiveZ = file;
            break;
        case bg.scene.CubemapImage.NEGATIVE_Z:
            componentData.negativeZ = file;
            break;
        }
        return bg.base.Writer.CopyFile(src,dst);
    }

    class Cubemap extends bg.scene.Component {
        static Current(context) {
            if (!g_currentCubemap) {
                g_currentCubemap = bg.base.TextureCache.WhiteCubemap(context);
            }
            return g_currentCubemap;
        }

        static IrradianceMapIntensity() {
            return g_irradianceIntensity;
        }
        static IrradianceMap(context) {
            if (!g_irradianceCubemap) {
                g_irradianceCubemap = bg.base.TextureCache.BlackCubemap(context);
            }
            return g_irradianceCubemap;
        }

        static SpecularMap(context,level = 0){
            if (!g_specularCubemap[level]) {
                g_specularCubemap[level] = bg.base.TextureCache.BlackCubemap(context);
            }
            return g_specularCubemap[level];
        } 

        static SetCurrent(cubemapTexture) {
            g_currentCubemap = cubemapTexture;
        }

        static SetIrradianceMapIntensity(i) {
            g_irradianceIntensity = i;
        }

        static SetIrradiance(irradianceTexture) {
            g_irradianceCubemap = irradianceTexture;
        }

        static SetSpecular(specTexture,level = 0) {
            g_specularCubemap[level] = specTexture;
        }

        constructor() {
            super();
            this._images = [null, null, null, null, null, null];
            this._texture = null;
        }

        setImageUrl(imgCode,texture) {
            this._images[imgCode] = texture;
        }

        getImageUrl(imgCode) {
            return this._images[imgCode];
        }

        get texture() {
            return this._texture;
        }

        // Use this setter to set a custom cubemap, for example, one captured with an FBO
        set texture(t) {
            this._texture = t;
        }

        loadCubemap(context) {
            context = context || this.node && this.node.context;
            return new Promise((resolve,reject) => {
                bg.utils.Resource.LoadMultiple(this._images)
                    .then((result) => {
                        this._texture = new bg.base.Texture(context);
                        this._texture.target = bg.base.TextureTarget.CUBE_MAP;
                        this._texture.create();
                        this._texture.bind();

                        this._texture.setCubemap(
                            result[this.getImageUrl(bg.scene.CubemapImage.POSITIVE_X)],
                            result[this.getImageUrl(bg.scene.CubemapImage.NEGATIVE_X)],
                            result[this.getImageUrl(bg.scene.CubemapImage.POSITIVE_Y)],
                            result[this.getImageUrl(bg.scene.CubemapImage.NEGATIVE_Y)],
                            result[this.getImageUrl(bg.scene.CubemapImage.POSITIVE_Z)],
                            result[this.getImageUrl(bg.scene.CubemapImage.NEGATIVE_Z)]
                        );

                        g_currentCubemap = this._texture;
                        bg.emitImageLoadEvent(result[this.getImageUrl(bg.scene.CubemapImage.POSITIVE_X)]);
                        resolve(this);
                    })

                    .catch((err) => {
                        reject(err);
                    });
            });
        }

        clone() {
            let cubemap = new Cubemap();
            for (let code in this._images) {
                cubemap._images[code] = this._images[code];
            };
            cubemap._texture = this._texture;
            return cubemap;
        }

        deserialize(context,sceneData,url) {
            this.setImageUrl(
                bg.scene.CubemapImage.POSITIVE_X,
                bg.utils.Resource.JoinUrl(url,sceneData["positiveX"])
            );
            this.setImageUrl(
                bg.scene.CubemapImage.NEGATIVE_X,
                bg.utils.Resource.JoinUrl(url,sceneData["negativeX"])
            );
            this.setImageUrl(
                bg.scene.CubemapImage.POSITIVE_Y,
                bg.utils.Resource.JoinUrl(url,sceneData["positiveY"])
            );
            this.setImageUrl(
                bg.scene.CubemapImage.NEGATIVE_Y,
                bg.utils.Resource.JoinUrl(url,sceneData["negativeY"])
            );
            this.setImageUrl(
                bg.scene.CubemapImage.POSITIVE_Z,
                bg.utils.Resource.JoinUrl(url,sceneData["positiveZ"])
            );
            this.setImageUrl(
                bg.scene.CubemapImage.NEGATIVE_Z,
                bg.utils.Resource.JoinUrl(url,sceneData["negativeZ"])
            );
            return this.loadCubemap(context);
        }
        
        serialize(componentData,promises,url) {
            super.serialize(componentData,promises,url);
            if (!bg.isElectronApp) return;
            promises.push(copyCubemapImage.apply(this,[componentData,bg.scene.CubemapImage.POSITIVE_X,url.path]));
            promises.push(copyCubemapImage.apply(this,[componentData,bg.scene.CubemapImage.NEGATIVE_X,url.path]));
            promises.push(copyCubemapImage.apply(this,[componentData,bg.scene.CubemapImage.POSITIVE_Y,url.path]));
            promises.push(copyCubemapImage.apply(this,[componentData,bg.scene.CubemapImage.NEGATIVE_Y,url.path]));
            promises.push(copyCubemapImage.apply(this,[componentData,bg.scene.CubemapImage.POSITIVE_Z,url.path]));
            promises.push(copyCubemapImage.apply(this,[componentData,bg.scene.CubemapImage.NEGATIVE_Z,url.path]));
		}
    }

    bg.scene.registerComponent(bg.scene,Cubemap,"bg.scene.Cubemap");
})();
(function() {

	function escapePathCharacters(name) {
		if (!name) {
			return bg.utils.generateUUID();
		}
		else {
			var illegalRe = /[\/\?<>\\:\*\|":\[\]\(\)\{\}]/g;
			var controlRe = /[\x00-\x1f\x80-\x9f]/g;
			var reservedRe = /^\.+$/;
			var windowsReservedRe = /^(con|prn|aux|nul|com[0-9]|lpt[0-9])(\..*)?$/i;
			var windowsTrailingRe = /[\. ]+$/;

			function sanitize(input, replacement) {
				var sanitized = input
					.replace(illegalRe, replacement)
					.replace(controlRe, replacement)
					.replace(reservedRe, replacement)
					.replace(windowsReservedRe, replacement)
					.replace(windowsTrailingRe, replacement);
				return sanitized;
			}

			return sanitize(name,'-');
		}
	}
	class Drawable extends bg.scene.Component {
				
		// It creates a copy of the node with all its components, except the drawable
		// component, that will be an instance (share the same polyList instances)
		static InstanceNode(node) {
			let newNode = new bg.scene.Node(node.context,node.name ? `copy of ${node.name}`:"");
			newNode.enabled = node.enabled;
			node.forEachComponent((comp) => {
				let newComp = null;
				if (comp instanceof Drawable) {
					newComp = comp.instance();
				}
				else {
					newComp = comp.clone();
				}
				newNode.addComponent(newComp);
			});
			return newNode;
		}
		
		constructor(name="") {
			super();

			this._name = name;			
			this._items = []; // { polyList:p, material:m, transform:t }
		}
		
		get name() { return this._name; }
		set name(n) { this._name = n; }

		get length() { return this._items.length; }
		
		clone(newName) {
			let newInstance = new bg.scene.Drawable();
			newInstance.name = newName || `copy of ${this.name}`;
			this.forEach((plist,material,trx) => {
				newInstance.addPolyList(plist.clone(), material.clone(), trx ? new bg.Matrix4(trx):null);
			});
			return newInstance;
		}

		destroy() {
			this.forEach((plist,mat) => {
				plist.destroy();
				if (mat && mat.destroy) {
					mat.destroy();
				}
			});
			this._name = "";
			this._items = [];
		}
		
		// It works as clone(), but it doesn't duplicate the polyList
		instance(newName) {
			let newInstance = new bg.scene.Drawable();
			newInstance.name = newName || `copy of ${this.name}`;
			this.forEach((plist,material,trx) => {
				newInstance.addPolyList(plist, material.clone(), trx ? new bg.Matrix4(trx):null);
			});
			return newInstance;
		}
		
		addPolyList(plist,mat,trx=null) {
			if (plist && this.indexOf(plist)==-1) {
				mat = mat || new bg.base.Material();
				
				this._items.push({
					polyList:plist,
					material:mat,
					transform:trx
				});
				return true;
			}
			return false;
		}

		getExternalResources(resources = []) {
			this.forEach((plist,material) => {
				material.getExternalResources(resources)
			});
			return resources;
		}

		// Apply a material definition object to the polyLists
		applyMaterialDefinition(materialDefinitions,resourcesUrl) {
			let promises = [];
			this.forEach((plist,mat) => {
				let definition = materialDefinitions[plist.name];
				if (definition) {
					promises.push(new Promise((resolve,reject) => {
						let modifier = new bg.base.MaterialModifier(definition);
						mat.applyModifier(plist.context,modifier,resourcesUrl);
						resolve();
					}));
				}
			});
			return Promise.all(promises);
		}
		
		removePolyList(plist) {
			let index = -1;
			this._items.some((item, i) => {
				if (plist==item.polyList) {
					index = i;
				}
			})
			if (index>=0) {
				this._items.splice(index,1);
			}
		}
		
		indexOf(plist) {
			let index = -1;
			this._items.some((item,i) => {
				if (item.polyList==plist) {
					index = i;
					return true;
				}
			});
			return index;
		}
		
		replacePolyList(index,plist) {
			if (index>=0 && index<this._items.length) {
				this._items[index].polyList = plist;
				return true;
			}
			return false;
		}
		
		replaceMaterial(index,mat) {
			if (index>=0 && index<this._items.length) {
				// Release PBR material resources
				if (this._items[index].material.destroy) {
					this._items[index].material.destroy();
				}
				this._items[index].material = mat;
				return true;
			}
			return false;
		}
		
		replaceTransform(index,trx) {
			if (index>=0 && index<this._items.length) {
				this._items[index].transform = trx;
				return true;
			}
			return false;
		}
		
		getPolyList(index) {
			if (index>=0 && index<this._items.length) {
				return this._items[index].polyList;
			}
			return false;
		}
		
		getMaterial(index) {
			if (index>=0 && index<this._items.length) {
				return this._items[index].material;
			}
			return false;
		}
		
		getTransform(index) {
			if (index>=0 && index<this._items.length) {
				return this._items[index].transform;
			}
			return false;
		}
		
		
		forEach(callback) {
			for (let elem of this._items) {
				callback(elem.polyList,elem.material,elem.transform);
			}
		}
		
		some(callback) {
			for (let elem of this._items) {
				if (callback(elem.polyList,elem.material,elem.transform)) {
					return true;
				}
			}
			return false;
		}
		
		every(callback) {
			for (let elem of this._items) {
				if (!callback(elem.polyList,elem.material,elem.transform)) {
					return false;
				}
			}
			return true;
		}
		
		////// Direct rendering method: will be deprecated soon
		display(pipeline,matrixState,forceDraw=false) {
			if (!pipeline.effect) {
				throw new Error("Could not draw component: invalid effect found.");
			}

			let isShadowMap = pipeline.effect instanceof bg.base.ShadowMapEffect;

			if (!this.node.enabled) {
				return;
			}
			else {
				this.forEach((plist,mat,trx) => {
					if ((!isShadowMap && plist.visible) || (isShadowMap && plist.visibleToShadows) || forceDraw) {
						let currMaterial = pipeline.effect.material;
						if (trx) {
							matrixState.modelMatrixStack.push();
							matrixState.modelMatrixStack.mult(trx);
						}
						
						if (pipeline.shouldDraw(mat)) {
							pipeline.effect.material = mat;
							pipeline.draw(plist);
						}
						
						if (trx) {
							matrixState.modelMatrixStack.pop();
						}
						pipeline.effect.material = currMaterial;
					}
				});
			}
		}

		//// Render queue method
		draw(renderQueue,modelMatrixStack,viewMatrixStack,projectionMatrixStack) {
			if (!this.node.enabled) {
				return;
			}

			this.forEach((plist,mat,trx) => {
				if (!plist.visible) {
					return;
				}
				if (trx) {
					modelMatrixStack.push();
					modelMatrixStack.mult(trx);
				}

				if (mat.isTransparent) {
					renderQueue.renderTransparent(plist,mat,modelMatrixStack.matrix,viewMatrixStack.matrix);
				}
				else {
					renderQueue.renderOpaque(plist,mat,modelMatrixStack.matrix,viewMatrixStack.matrix);
				}

				if (trx) {
					modelMatrixStack.pop(trx);
				}
			});
		}
		
		setGroupVisible(groupName,visibility=true) {
			this.forEach((plist) => {
				if (plist.groupName==groupName) {
					plist.visible = visibility;
				}
			});
		}
		
		hideGroup(groupName) { this.setGroupVisible(groupName,false); }
		
		showGroup(groupName) { this.setGroupVisible(groupName,true); }
		
		setVisibleByName(name,visibility=true) {
			this.some((plist) => {
				if (plist.name==name) {
					plist.visible = visibility;
					return true;
				}
			});
		}
		
		showByName(name) {
			this.setVisibleByName(name,true);
		}
		
		hideByName(name) {
			this.setVisibleByName(name,false);
		}
		
		deserialize(context,sceneData,url) {
			return new Promise((resolve,reject) => {
				let modelUrl = bg.utils.Resource.JoinUrl(url,sceneData.name + '.vwglb');
				bg.base.Loader.Load(context,modelUrl)
					.then((node) => {
						let drw = node.component("bg.scene.Drawable");
						this._name = sceneData.name;
						this._items = drw._items;
						resolve(this);
					});
			});
		}

		serialize(componentData,promises,url) {
			if (!bg.isElectronApp) {
				return;
			}
			super.serialize(componentData,promises,url);
			this.name = escapePathCharacters(this.name);
		
			componentData.name = this.name;
			const path = require('path');
			let dst = path.join(url.path,componentData.name + ".vwglb");
			promises.push(new Promise((resolve,reject) => {
				bg.base.Writer.Write(dst,this.node)
					.then(() => resolve())
					.catch((err) => reject(err));
			}));
		}
	}
	
	bg.scene.registerComponent(bg.scene,Drawable,"bg.scene.Drawable");
	
})();
(function() {

    let g_environment = null;

    class Environment extends bg.scene.Component {
        static Get() {
            return g_environment;
        }
        
        constructor(env = null) {
            super();
            this._environment = env;
        }

        /*
         *  Cubemap settings:
         *      cubemapSize - default: 512
         *      irradianceMapSize - default: 32
         *      specularMapSize - default: 32
         *      specularMapL2Size - default: specularMapSize
         */
        initCubemap(texture,cubemapSettings) {
            if (this._environment) {
                this._environment.destroy();
            }
            this._environment = new bg.base.Environment(texture.context);
            this._environment.create(cubemapSettings);
            this._environment.equirectangularTexture = texture;
        }

        init() {
            if (g_environment==null) {
                g_environment = this;
            }
        }

        removedFromNode() {
            if (g_environment==this) {
                g_environment = null;
            }
        }

        get environment() { return this._environment; }
        set environment(e) {
            if (this._environment) {
                this._environment.destroy();
            }
            this._environment = e;
        }

        get equirectangularTexture() { return this._environment && this._environment.equirectangularTexture; }
        set equirectangularTexture(t) {
            if (this._environment) {
                this._environment.equirectangularTexture = t;
            }
            else {
                this.initCubemap(t);
            }
        }

        clone() {
            let other = new Environment();
            other._environment = this._environment && this._environment.clone();
            return other;
        }

        deserialize(context,sceneData,url) {
            if (!this._environment) {
                this._environment.destory();
            }
            this._environment = new bg.base.Environment(context);
            this._environment.deserialize(sceneData,url);
        }

        serialize(componentData,promises,url) {
            super.serialize(componentData,promises,url);
            if (!bg.isElectronApp) return;
            if (this._environment) {
                this._environment.serialize(componentData, promises, url);
            }
        }
    }

    bg.scene.registerComponent(bg.scene, Environment, "bg.scene.Environment");

})();

(function() {
	
	let s_lightRegister = [];
	
	function registerLight(l) {
		s_lightRegister.push(l);
	}
	
	function unregisterLight(l) {
		let i = s_lightRegister.indexOf(l);
		if (i!=-1) {
			s_lightRegister.splice(i,1);
		}
	}

	function buildPlist(context,vertex,color) {
		let plist = new bg.base.PolyList(context);
		let normal = [];
		let texCoord0 = [];
		let index = [];
		let currentIndex = 0;
		for (let i=0; i<vertex.length; i+=3) {
			normal.push(0); normal.push(0); normal.push(1);
			texCoord0.push(0); texCoord0.push(0);
			index.push(currentIndex++);
		}
		plist.vertex = vertex;
		plist.normal = normal;
		plist.texCoord0 = texCoord0;
		plist.color = color;
		plist.index = index;
		plist.drawMode = bg.base.DrawMode.LINES;
		plist.build();
		return plist;
	}

	function getDirectionalGizmo(conext) {
		if (!this._directionalGizmo) {
			let context = this.node.context;
			let vertex = [
				0,0,0, 0,0,-1,
				0,0,-1, 0,0.1,-0.9,
				0,0,-1, 0,-0.1,-0.9,
			];
			let color = [
				1,1,1,1, 1,1,0,1,
				1,1,0,1, 1,1,0,1,
				1,1,0,1, 1,1,0,1
			];
			this._directionalGizmo = buildPlist(context,vertex,color);
		}
		return this._directionalGizmo;
	}

	function getSpotGizmo() {
		let context = this.node.context;
		let distance = 5;
		let alpha = bg.Math.degreesToRadians(this.light.spotCutoff / 2);
		let salpha = bg.Math.sin(alpha) * distance;
		let calpha = bg.Math.cos(alpha) * distance;

		let rx2 = bg.Math.cos(bg.Math.PI_8) * salpha;
		let rx1 = bg.Math.cos(bg.Math.PI_4) * salpha;
		let rx0 = bg.Math.cos(bg.Math.PI_4 + bg.Math.PI_8) * salpha;

		let ry2 = bg.Math.sin(bg.Math.PI_8) * salpha;
		let ry1 = bg.Math.sin(bg.Math.PI_4) * salpha;
		let ry0 = bg.Math.sin(bg.Math.PI_4 + bg.Math.PI_8) * salpha;

		let vertex = [
			0,0,0, 0,salpha,-calpha,
			0,0,0, 0,-salpha,-calpha,
			0,0,0, salpha,0,-calpha,
			0,0,0, -salpha,0,-calpha,

			0,salpha,-calpha, rx0,ry0,-calpha, rx0,ry0,-calpha, rx1,ry1,-calpha, rx1,ry1,-calpha, rx2,ry2,-calpha, rx2,ry2,-calpha, salpha,0,-calpha,
		
			salpha,0,-calpha, rx2,-ry2,-calpha, rx2,-ry2,-calpha, rx1,-ry1,-calpha, rx1,-ry1,-calpha, rx0,-ry0,-calpha, rx0,-ry0,-calpha, 0,-salpha,-calpha,
			0,-salpha,-calpha, -rx0,-ry0,-calpha, -rx0,-ry0,-calpha, -rx1,-ry1,-calpha, -rx1,-ry1,-calpha, -rx2,-ry2,-calpha, -rx2,-ry2,-calpha, -salpha,0,-calpha,

			-salpha,0,-calpha, -rx2,ry2,-calpha, -rx2,ry2,-calpha, -rx1,ry1,-calpha, -rx1, ry1,-calpha, -rx0,ry0,-calpha, -rx0,ry0,-calpha,  0,salpha,-calpha
		];
		let color = [
			1,1,1,1, 1,1,0,1,
			1,1,1,1, 1,1,0,1,
			1,1,1,1, 1,1,0,1,
			1,1,1,1, 1,1,0,1,

			1,1,0,1, 1,1,0,1, 1,1,0,1, 1,1,0,1, 1,1,0,1, 1,1,0,1, 1,1,0,1, 1,1,0,1,
			1,1,0,1, 1,1,0,1, 1,1,0,1, 1,1,0,1, 1,1,0,1, 1,1,0,1, 1,1,0,1, 1,1,0,1,
			
			1,1,0,1, 1,1,0,1, 1,1,0,1, 1,1,0,1, 1,1,0,1, 1,1,0,1, 1,1,0,1, 1,1,0,1,
			1,1,0,1, 1,1,0,1, 1,1,0,1, 1,1,0,1, 1,1,0,1, 1,1,0,1, 1,1,0,1, 1,1,0,1
		];

		if (!this._spotGizmo) {
			this._spotGizmo = buildPlist(context,vertex,color);
		}
		else {
			this._spotGizmo.updateBuffer(bg.base.BufferType.VERTEX,vertex);
			this._spotGizmo.updateBuffer(bg.base.BufferType.COLOR,color);
		}
		return this._spotGizmo;
	}

	function getPointGizmo() {
		if (!this._pointGizmo) {
			let context = this.node.context;
			let r = 0.5;
			let s = bg.Math.sin(bg.Math.PI_4) * r;
			let vertex = [
				// x-y plane
				0,0,0, 0,r,0, 0,0,0, 0,-r,0, 0,0,0, -r,0,0, 0,0,0, r,0,0,
				0,0,0, s,s,0, 0,0,0, s,-s,0, 0,0,0, -s,s,0, 0,0,0, -s,-s,0,

				// z, -z
				0,0,0, 0,0,r, 0,0,0, 0,0,-r,
				0,0,0, 0,s,s, 0,0,0, 0,-s,s, 0,0,0, 0,-s,-s, 0,0,0, 0,s,-s,
				0,0,0, s,0,s, 0,0,0, -s,0,s, 0,0,0, -s,0,-s, 0,0,0, s,0,-s
			];
			let color = [
				1,1,1,1, 1,1,0,1, 1,1,1,1, 1,1,0,1, 1,1,1,1, 1,1,0,1, 1,1,1,1, 1,1,0,1,
				1,1,1,1, 1,1,0,1, 1,1,1,1, 1,1,0,1, 1,1,1,1, 1,1,0,1, 1,1,1,1, 1,1,0,1,
				1,1,1,1, 1,1,0,1, 1,1,1,1, 1,1,0,1,
				1,1,1,1, 1,1,0,1, 1,1,1,1, 1,1,0,1, 1,1,1,1, 1,1,0,1, 1,1,1,1, 1,1,0,1,
				1,1,1,1, 1,1,0,1, 1,1,1,1, 1,1,0,1, 1,1,1,1, 1,1,0,1, 1,1,1,1, 1,1,0,1
			];
			this._pointGizmo = buildPlist(context,vertex,color);
		}
		return this._pointGizmo;
	}

	function getGizmo() {
		switch (this._light && this._light.type) {
		case bg.base.LightType.DIRECTIONAL:
			return getDirectionalGizmo.apply(this);
		case bg.base.LightType.SPOT:
			return getSpotGizmo.apply(this);
		case bg.base.LightType.POINT:
			return getPointGizmo.apply(this);
		}
		return null;
	}
	
	class Light extends bg.scene.Component {
		// The active lights are the lights that are attached to a node
		// in the scene.
		static GetActiveLights() {
			return s_lightRegister;
		}
		
		constructor(light = null) {
			super();
			this._light = light;
			this._visitor = new bg.scene.TransformVisitor();
			this._rebuildTransform = true;
		}
		
		clone() {
			let newLight = new bg.scene.Light();
			newLight.light = this.light.clone();
			return newLight;
		}
		
		get light() { return this._light; }
		set light(l) { this._light = l; }
		
		get transform() {
			if (this._rebuildTransform && this.node) {
				this._visitor.matrix.identity();
				this.node.acceptReverse(this._visitor);
				this._rebuildTransform = false;
			}
			return this._visitor.matrix;
		}
		
		frame(delta) {
			this._rebuildTransform = true;
			this.transform;
		}

		displayGizmo(pipeline,matrixState) {
			let plist = getGizmo.apply(this);
			if (plist) {
				pipeline.draw(plist);
			}
		}
		
		removedFromNode(node) {
			unregisterLight(this);
		}
		
		addedToNode(node) {
			registerLight(this);
		}

		deserialize(context,sceneData,url) {
			return new Promise((resolve,reject) => {
				this._light = new bg.base.Light(context);
				this._light.deserialize(sceneData);
				resolve(this);
			});
		}

		serialize(componentData,promises,url) {
			super.serialize(componentData,promises,url);
			this.light.serialize(componentData);
		}
	}

	bg.scene.registerComponent(bg.scene,Light,"bg.scene.Light");
})();
(function() {
    function parseMTL_n(line) {
        let res = /newmtl\s+(.*)/.exec(line);
        if (res) {
            this._jsonData[res[1]] = JSON.parse(JSON.stringify(s_matInit));
            this._currentMat = this._jsonData[res[1]];
        }
    }

    function parseMTL_N(line) {
        let res = /Ns\s+([\d\.]+)/.exec(line);
        if (res) {  // Specular
            this._currentMat.shininess = Number(res[1]);
        }
        //else if ( (res=/Ni\s+([\d\.]+)/.exec(line)) ) {
        //}
    }

    function vectorFromRE(re) {
        return [
            Number(re[1]),
            Number(re[2]),
            Number(re[3]),
            re[4] ? Number(re[4]) : 1.0
        ]
    }

    function parseMTL_K(line) {
        let res = /Kd\s+([\d\.]+)\s+([\d\.]+)\s+([\d\.]+)\s*([\d\.]*)/.exec(line);
        if (res) {
            // Diffuse
            let d = vectorFromRE(res);
            this._currentMat.diffuseR = d[0];
            this._currentMat.diffuseG = d[1];
            this._currentMat.diffuseB = d[2];
            this._currentMat.diffuseA = d[3];
        }
        else if ( (res = /Ks\s+([\d\.]+)\s+([\d\.]+)\s+([\d\.]+)\s*([\d\.]*)/.exec(line)) ) {
            // Specular
            let s = vectorFromRE(res);
            this._currentMat.specularR = s[0];
            this._currentMat.specularG = s[1];
            this._currentMat.specularB = s[2];
            this._currentMat.specularA = s[3];
        }
    }

    function parseMTL_m(line) {
        let res = /map_Kd\s+(.*)/.exec(line);
        if (res) {
            let path = res[1];
            path = path.replace(/\\/g,'/');
            let slashIndex = path.lastIndexOf('/'); 
            if (slashIndex>=0) {
                path = path.substring(slashIndex + 1);
            }
            this._currentMat.texture = path;
        }
    }

    let s_matInit = {
        diffuseR: 1.0,
        diffuseG:1.0,
        diffuseB:1.0,
        diffuseA:1.0,
        
        specularR:1.0,
        specularG:1.0,
        specularB:1.0,
        specularA:1.0,
        
        shininess: 0,
        lightEmission: 0,
        
        refractionAmount: 0,
        reflectionAmount: 0,

        textureOffsetX: 0,
        textureOffsetY: 0,
        textureScaleX: 1,
        textureScaleY: 1,
        
        lightmapOffsetX: 0,
        lightmapOffsetY: 0,
        lightmapScaleX: 1,
        lightmapScaleY: 1,
        
        normalMapOffsetX: 0,
        normalMapOffsetY: 0,
        normalMapScaleX: 1,
        normalMapScaleY: 1,
        
        alphaCutoff: 0.5,
        castShadows: true,
        receiveShadows: true,
        
        shininessMaskChannel: 0,
        shininessMaskInvert: false,
        lightEmissionMaskChannel: 0,
        lightEmissionMaskInvert: false,

        reflectionMaskChannel: 0,
        reflectionMaskInvert: false,
    
        cullFace: true,

        texture: "",
        lightmap: "",
        normalMap: "",
        shininessMask: "",
        lightEmissionMask: "",
        reflectionMask: ""
    };

    class MTLParser {
        constructor(mtlData) {
            this._jsonData = {}
            this._currentMat = JSON.parse(JSON.stringify(s_matInit));
            let lines = mtlData.split('\n');

            lines.forEach((line) => {
                // First optimization: parse the first character and string lenght
                line = line.trim();
                if (line.length>1 && line[0]!='#') {
                    // Second optimization: parse by the first character
                    switch (line[0]) {
                    case 'n':
                        parseMTL_n.apply(this,[line]);
                        break;
                    case 'N':
                        parseMTL_N.apply(this,[line]);
                        break;
                    case 'm':
                        parseMTL_m.apply(this,[line]);
                        break;
                    case 'd':
                        break;
                    case 'T':
                        break;
                    case 'K':
                        parseMTL_K.apply(this,[line]);
                        break;
                    case 'i':
                        break;
                    case 'o':
                        break;
                    }
                }
            });
        }

        get jsonData() { return this._jsonData; }
    }

    function parseM(line) {
        // mtllib
        let res = /mtllib\s+(.*)/.exec(line);
        if (res) {
            this._mtlLib = res[1];
        }
    }

    function parseG(line) {
        // g
        let res = /g\s+(.*)/.exec(line);
        if (res) {
            this._currentPlist.name = res[1];
        }
    }

    function parseU(line) {
        // usemtl
        let res = /usemtl\s+(.*)/.exec(line);
        if (res) {
            this._currentPlist._matName = res[1];
            if (this._currentPlist.name=="") {
                this._currentPlist.name = res[1];
            }
        }
    }

    function parseS(line) {
        // s
        let res = /s\s+(.*)/.exec(line);
        if (res) {
            // TODO: Do something with smoothing groups
        }
    }

    function addPoint(pointData) {
        this._currentPlist.vertex.push(pointData.vertex[0],pointData.vertex[1],pointData.vertex[2]);
        if (pointData.normal) {
            this._currentPlist.normal.push(pointData.normal[0],pointData.normal[1],pointData.normal[2]);
        }
        if (pointData.tex) {
            this._currentPlist.texCoord0.push(pointData.tex[0],pointData.tex[1]);
        }
        this._currentPlist.index.push(this._currentPlist.index.length);
    }

    function isValid(point) {
        return point && point.vertex && point.tex && point.normal;
    }

    function addPolygon(polygonData) {
        let currentVertex = 0;
        let sides = polygonData.length;
        if (sides<3) return;
        while (currentVertex<sides) {
            let i0 = currentVertex;
            let i1 = currentVertex + 1;
            let i2 = currentVertex + 2;
            if (i2==sides) {
                i2 = 0;
            }
            else if (i1==sides) {
                i1 = 0;
                i2 = 2;
            }

            let p0 = polygonData[i0];
            let p1 = polygonData[i1];
            let p2 = polygonData[i2];

            if (isValid(p0) && isValid(p1) && isValid(p2)) {
                addPoint.apply(this,[p0]);
                addPoint.apply(this,[p1]);
                addPoint.apply(this,[p2]);
            }
            else {
                console.warn("Invalid point data found loading OBJ file");
            }
            currentVertex+=3;
        }
    }

    function parseF(line) {
        // f
        this._addPlist = true;
        let res = /f\s+(.*)/.exec(line);
        if (res) {
            let params = res[1];
            let vtnRE = /([\d\-]+)\/([\d\-]*)\/([\d\-]*)/g;
            if (params.indexOf('/')==-1) {
                let vRE = /([\d\-]+)/g;
            }
            let polygon = [];
            while ( (res=vtnRE.exec(params)) ) {
                let iV = Number(res[1]);
                let iN = res[3] ? Number(res[3]):null;
                let iT = res[2] ? Number(res[2]):null;
                iV = iV<0 ? this._vertexArray.length + iV : iV - 1;
                iN = iN<0 ? this._normalArray.length + iN : (iN===null ? null : iN - 1);
                iT = iT<0 ? this._texCoordArray.length + iT : (iT===null ? null : iT - 1)

                let v = this._vertexArray[iV];
                let n = iN!==null ? this._normalArray[iN] : null;
                let t = iT!==null ? this._texCoordArray[iT] : null;
                polygon.push({
                    vertex:v,
                    normal:n,
                    tex:t
                });
            }
            addPolygon.apply(this,[polygon]);
        }
    }

    function parseO(line) {
        // o
        let res = /s\s+(.*)/.exec(line);
        if (res && this._currentPlist.name=="") {
            this._currentPlist.name = res[1];
        }
    }

    function checkAddPlist() {
        if (this._addPlist) {
            if (this._currentPlist) {
                this._currentPlist.build();
                this._plistArray.push(this._currentPlist);
            }
            this._currentPlist = new bg.base.PolyList(this.context);
            this._addPlist = false;
        }
    }

    function parseMTL(mtlData) {
        let parser = new MTLParser(mtlData);
        return parser.jsonData;
    }

    class OBJParser {
        constructor(context,url) {
            this.context = context;
            this.url = url;

            this._plistArray = [];

            this._vertexArray = [];
            this._normalArray = [];
            this._texCoordArray = [];

            this._mtlLib = "";

            this._addPlist = true;
        }

        loadDrawable(data) {
            return new Promise((resolve,reject) => {
                let name = this.url.replace(/[\\\/]/ig,'-');
                let drawable = new bg.scene.Drawable(name);
                let lines = data.split('\n');

                let multiLine = "";
                lines.forEach((line) => {
                    line = line.trim();

                    // This section controls the break line character \
                    // to concatenate this line with the next one
                    if (multiLine) {
                        line = multiLine + line;
                    }
                    if (line[line.length-1]=='\\') {
                        line = line.substring(0,line.length-1);
                        multiLine += line;
                        return;
                    }
                    else {
                        multiLine = "";
                    }

                    // First optimization: parse the first character and string lenght
                    if (line.length>1 && line[0]!='#') {
                        // Second optimization: parse by the first character
                        switch (line[0]) {
                        case 'v':
                            let res = /v\s+([\d\.\-e]+)\s+([\d\.\-e]+)\s+([\d\.\-e]+)/.exec(line);
                            if (res) {
                                this._vertexArray.push(
                                    [ Number(res[1]), Number(res[2]), Number(res[3]) ]
                                );
                            }
                            else if ( (res = /vn\s+([\d\.\-e]+)\s+([\d\.\-e]+)\s+([\d\.\-e]+)/.exec(line)) ) {
                                this._normalArray.push(
                                    [ Number(res[1]), Number(res[2]), Number(res[3]) ]
                                );
                            }
                            else if ( (res = /vt\s+([\d\.\-e]+)\s+([\d\.\-e]+)/.exec(line)) ) {
                                this._texCoordArray.push(
                                    [ Number(res[1]), Number(res[2]) ]
                                );
                            }
                            else {
                                console.warn("Error parsing line " + line);
                            }
                            break;
                        case 'm':
                            checkAddPlist.apply(this);
                            parseM.apply(this,[line]);
                            break;
                        case 'g':
                            checkAddPlist.apply(this);
                            parseG.apply(this,[line]);
                            break;
                        case 'u':
                            checkAddPlist.apply(this);
                            parseU.apply(this,[line]);
                            break;
                        case 's':
                            //checkAddPlist.apply(this);
                            parseS.apply(this,[line]);
                            break;
                        case 'f':
                            parseF.apply(this,[line]);
                            break;
                        case 'o':
                            checkAddPlist.apply(this);
                            parseO.apply(this,[line]);
                            break;
                        }
                    }
                });

                if (this._currentPlist && this._addPlist) {
                    this._currentPlist.build();
                    this._plistArray.push(this._currentPlist);
                }

                function buildDrawable(plistArray,materials) {
                    plistArray.forEach((plist) => {
                        let mat = new bg.base.Material();
                        let matData = materials[plist._matName];
                        if (matData) {
                            let url = this.url.substring(0,this.url.lastIndexOf('/') + 1);
                            bg.base.Material.GetMaterialWithJson(this.context,matData,url)
                                .then((material) => {
                                    drawable.addPolyList(plist,material);
                                })
                        }
                        else {
                            drawable.addPolyList(plist,mat);
                        }
                    });
                }

                if (this._mtlLib) {
                    let locationUrl = this.url.substring(0,this.url.lastIndexOf("/"));
                    if (locationUrl.length>0 && locationUrl!='/') locationUrl += "/";
                    bg.utils.Resource.Load(locationUrl + this._mtlLib)
                        .then((data) => {
                            buildDrawable.apply(this,[this._plistArray,parseMTL(data)]);
                            resolve(drawable);
                        })
                        .catch(() => {
                            bg.log("Warning: no such material library file for obj model " + this.url);
                            buildDrawable.apply(this,[this._plistArray,{}]);
                            resolve(drawable);
                        });
                }
                else {
                    buildDrawable.apply(this,[this._plistArray,{}]);
                    resolve(drawable);
                }
            });
        }
    }

    class OBJLoaderPlugin extends bg.base.LoaderPlugin {
        acceptType(url,data) {
            return bg.utils.Resource.GetExtension(url)=="obj";
        }

        load(context,url,data) {
            return new Promise((resolve,reject) => {
                if (data) {
                    try {
                        let parser = new OBJParser(context,url);
                        let resultNode = null;
                        let basePath = url.split("/");
                        basePath.pop();
                        basePath = basePath.join("/") + '/';
                        let matUrl = url.split(".");
                        matUrl.pop();
                        matUrl.push("bg2mat");
                        matUrl = matUrl.join(".");
                        parser.loadDrawable(data)
                            .then((drawable) => {
                                let node = new bg.scene.Node(context,drawable.name);
                                node.addComponent(drawable);
                                resultNode = node;
                                return bg.utils.Resource.LoadJson(matUrl);
                            })

                            .then((matData) => {
                                let promises = [];
								try {
									let drw = resultNode.component("bg.scene.Drawable");
									drw.forEach((plist,mat)=> {
										let matDef = null;
										matData.some((defItem) => {
											if (defItem.name==plist.name) {
												matDef = defItem;
												return true;
											}
										});

										if (matDef) {
											let p = bg.base.Material.FromMaterialDefinition(context,matDef,basePath);
											promises.push(p)
											p.then((newMat) => {
                                                mat.assign(newMat);
                                            });
										}
									});
								}
								catch(err) {
									
								}
								return Promise.all(promises);
                            })

                            .then(() => {
                                resolve(resultNode);
                            })

                            .catch(() => {
                                // bg2mat file not found
                                resolve(resultNode)
                            })
                    }
                    catch(e) {
                        reject(e);
                    }
                }
                else {
                    reject(new Error("Error loading drawable. Data is null."));
                }
            });
        }
    }

    bg.base.OBJLoaderPlugin = OBJLoaderPlugin;
})();
(function() {
	let s_pbrMaterials = false;

	function createCube(context,w,h,d) {
		let plist = new bg.base.PolyList(context);
        
		let x = w/2;
		let y = h/2;
		let z = d/2;
		
		plist.vertex = [
			 x,-y,-z, -x,-y,-z, -x, y,-z,  x, y,-z,		// back face
			 x,-y, z,  x,-y,-z,  x, y,-z,  x, y, z,		// right face 
			-x,-y, z,  x,-y, z,  x, y, z, -x, y, z, 	// front face
			-x,-y,-z, -x,-y, z, -x, y, z, -x, y,-z,		// left face
			-x, y, z,  x, y, z,  x, y,-z, -x, y,-z,		// top face
			 x,-y, z, -x,-y, z, -x,-y,-z,  x,-y,-z		// bottom face
		];
		
		plist.normal = [
			 0, 0,-1,  0, 0,-1,  0, 0,-1,  0, 0,-1,		// back face
			 1, 0, 0,  1, 0, 0,  1, 0, 0,  1, 0, 0,		// right face 
			 0, 0, 1,  0, 0, 1,  0, 0, 1,  0, 0, 1, 	// front face
			-1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0,		// left face
			 0, 1, 0,  0, 1, 0,  0, 1, 0,  0, 1, 0,		// top face
			 0,-1, 0,  0,-1, 0,  0,-1, 0,  0,-1, 0		// bottom face
		];
		
		plist.texCoord0 = [
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1
		];

		plist.index = [
			 0, 1, 2,	 2, 3, 0,
			 4, 5, 6,	 6, 7, 4,
			 8, 9,10,	10,11, 8,
			12,13,14,	14,15,12,
			16,17,18,	18,19,16,
			20,21,22,	22,23,20
		];

		plist.texCoord1 = bg.tools.UVMap.atlas(plist.vertex,plist.index,0.03);
		
        plist.build();
		return plist;
	}
	
	function createPlane(context,w,d,plane='y') {
		let x = w / 2.0;
		let y = d / 2.0;
		
		let plist = new bg.base.PolyList(context);
		
		switch (plane.toLowerCase()) {
		case 'x':
			plist.vertex =[	0.000000,-x,-y,
							0.000000, x,-y,
							0.000000, x, y,
							0.000000, x, y,
							0.000000,-x, y,
							0.000000,-x,-y];
			
			plist.normal = [1.000000,0.000000,0.000000,
							1.000000,0.000000,0.000000,
							1.000000,0.000000,0.000000,
							1.000000,0.000000,0.000000,
							1.000000,0.000000,0.000000,
							1.000000,0.000000,0.000000];

			plist.texCoord0 = [	0.000000,0.000000,
				1.000000,0.000000,
				1.000000,1.000000,
				1.000000,1.000000,
				0.000000,1.000000,
				0.000000,0.000000];
	

			plist.index = [2,1,0,5,4,3];
			break;
		case 'y':
			plist.vertex =[	-x,0.000000,-y,
							 x,0.000000,-y,
							 x,0.000000, y,
							 x,0.000000, y,
							-x,0.000000, y,
							-x,0.000000,-y];

			plist.normal = [0.000000,1.000000,0.000000,
							0.000000,1.000000,0.000000,
							0.000000,1.000000,0.000000,
							0.000000,1.000000,0.000000,
							0.000000,1.000000,0.000000,
							0.000000,1.000000,0.000000];

			plist.texCoord0 = [	0.000000,0.000000,
				1.000000,0.000000,
				1.000000,1.000000,
				1.000000,1.000000,
				0.000000,1.000000,
				0.000000,0.000000];
	

			plist.index = [2,1,0,5,4,3];
			break;
		case 'z':
			plist.vertex =[-x, y,0.000000,
						-x,-y,0.000000,
						   x,-y,0.000000,
						   x,-y,0.000000,
						   x, y,0.000000,
						-x, y,0.000000];

			plist.normal = [0.000000,0.000000,1.000000,
							0.000000,0.000000,1.000000,
							0.000000,0.000000,1.000000,
							0.000000,0.000000,1.000000,
							0.000000,0.000000,1.000000,
							0.000000,0.000000,1.000000];
	
			plist.texCoord0 = [
				0.000000,1.000000,
				0.000000,0.000000,
				1.000000,0.000000,
				1.000000,0.000000,
				1.000000,1.000000,
				0.000000,1.000000];
	
			plist.index = [0,1,2,3,4,5];
			break;
		}

	
		plist.texCoord1 = [
			0.00,0.95,
			0.00,0.00,
			0.95,0.00,
			0.95,0.00,
			0.95,0.95,
			0.00,0.95
		];

		
		
		plist.build();
		return plist;
	}

	function createSphere(context,radius,slices,stacks) {
		let plist = new bg.base.PolyList(context);
	
		++slices;
		const R = 1/(stacks-1);
		const S = 1/(slices-1);
		let r, s;
				
		let vertex = [];
		let normal = [];
		let texCoord = [];
		let index = [];
	
		for(r = 0; r < stacks; r++) for(s = 0; s < slices; s++) {
			const y = bg.Math.sin( -bg.Math.PI_2 + bg.Math.PI * r * R );
			const x = bg.Math.cos(2*bg.Math.PI * s * S) * bg.Math.sin(bg.Math.PI * r * R);
			const z = bg.Math.sin(2*bg.Math.PI * s * S) * bg.Math.sin(bg.Math.PI * r * R);
			texCoord.push(s * S); texCoord.push(r * R);
			normal.push(x,y,z);
			vertex.push(x * radius, y * radius, z * radius);
		}

		for(r = 0; r < stacks - 1; r++) for(s = 0; s < slices - 1; s++) {
			let i1 = r * slices + s;
			let i2 = r * slices + (s + 1);
			let i3 = (r + 1) * slices + (s + 1);
			let i4 = (r + 1) * slices + s;
			index.push(i1); index.push(i4); index.push(i3);
			index.push(i3); index.push(i2); index.push(i1);
		}
		
		plist.vertex = vertex;
		plist.normal = normal;
		plist.texCoord0 = texCoord;

		plist.texCoord1 = bg.tools.UVMap.atlas(vertex,index,0.03);
		plist.index = index;

		plist.build();
		
		return plist;
	}
	
	function createDrawable(plist,name) {
		let drawable = new bg.scene.Drawable(name);
		let mat = s_pbrMaterials ? new bg.base.PBRMaterial() : new bg.base.Material();
		drawable.addPolyList(plist,mat);
		return drawable;
	}
	
	class PrimitiveFactory {
		static SetPBRMaterials(pbrMat) {
			s_pbrMaterials = pbrMat;
		}

		static CubePolyList(context,w=1,h,d) {
			h = h || w;
			d = d || w;
			return createCube(context,w,h,d);
		}

		static PlanePolyList(context,w=1,d,plane='y') {
			d = d || w;
			return createPlane(context,w,d,plane);
		}

		static SpherePolyList(context,r=1,slices=20,stacks) {
			stacks = stacks || slices;
			return createSphere(context,r,slices,stacks);
		}

		static Cube(context,w=1,h,d) {
			h = h || w;
			d = d || w;
			return createDrawable(createCube(context,w,h,d),"Cube");
		}
		
		static Plane(context,w=1,d,plane='y') {
			d = d || w;
			return createDrawable(createPlane(context,w,d,plane),"Plane");
		}
		
		static Sphere(context,r=1,slices=20,stacks) {
			stacks = stacks || slices;
			return createDrawable(createSphere(context,r,slices,stacks),"Sphere");
		}
	}
	
	bg.scene.PrimitiveFactory = PrimitiveFactory;
	
})();
(function() {
    function fooScene(context) {
        let root = new bg.scene.Node(context, "Scene Root");

        bg.base.Loader.Load(context,"../data/test-shape.vwglb")
			.then((node) => {
				root.addChild(node);
				node.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(-1.4,0.25,0).scale(0.5,0.5,0.5)));
			})
			
			.catch(function(err) {
				alert(err.message);
			});
	
		let sphereNode = new bg.scene.Node(context,"Sphere");
		sphereNode.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(-1.3,0.1,1.3)));
		sphereNode.addComponent(bg.scene.PrimitiveFactory.Sphere(context,0.1));
		sphereNode.component("bg.scene.Drawable").getMaterial(0).diffuse.a = 0.8;
		sphereNode.component("bg.scene.Drawable").getMaterial(0).reflectionAmount = 0.4;
		root.addChild(sphereNode);
	
		let floorNode = new bg.scene.Node(context,"Floor");
		floorNode.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(0,0,0)));
		floorNode.addComponent(bg.scene.PrimitiveFactory.Plane(context,10,10));
		floorNode.component("bg.scene.Drawable").getMaterial(0).shininess = 50;
		floorNode.component("bg.scene.Drawable").getMaterial(0).reflectionAmount = 0.3;
		floorNode.component("bg.scene.Drawable").getMaterial(0).normalMapScale = new bg.Vector2(10,10);
		floorNode.component("bg.scene.Drawable").getMaterial(0).textureScale = new bg.Vector2(10,10);
		floorNode.component("bg.scene.Drawable").getMaterial(0).reflectionMaskInvert = true;
		floorNode.component("bg.scene.Drawable").getMaterial(0).shininessMaskInvert = true;
		root.addChild(floorNode);

		bg.base.Loader.Load(context,"../data/bricks_nm.png")
			.then((tex) => {
				floorNode.component("bg.scene.Drawable").getMaterial(0).normalMap = tex;
			});

		bg.base.Loader.Load(context,"../data/bricks.jpg")
			.then((tex) => {
				floorNode.component("bg.scene.Drawable").getMaterial(0).texture = tex;
			});

		bg.base.Loader.Load(context,"../data/bricks_shin.jpg")
			.then((tex) => {
				floorNode.component("bg.scene.Drawable").getMaterial(0).reflectionMask = tex;
				floorNode.component("bg.scene.Drawable").getMaterial(0).shininessMask = tex;
			});
		
		let lightNode = new bg.scene.Node(context,"Light");
		lightNode.addComponent(new bg.scene.Light(new bg.base.Light(context)));	
		lightNode.addComponent(new bg.scene.Transform(bg.Matrix4.Identity()
												.rotate(bg.Math.degreesToRadians(30),0,1,0)
												.rotate(bg.Math.degreesToRadians(35),-1,0,0)));
		root.addChild(lightNode);
		
		let camera = new bg.scene.Camera();
		camera.isMain = true;
		let cameraNode = new bg.scene.Node("Camera");
		cameraNode.addComponent(camera);			
		cameraNode.addComponent(new bg.scene.Transform());
		cameraNode.addComponent(new bg.manipulation.OrbitCameraController());
		let camCtrl = cameraNode.component("bg.manipulation.OrbitCameraController");
		camCtrl.minPitch = -45;
		root.addChild(cameraNode);

        return root;
	}

    class SceneFileParser {
        constructor(url,jsonData) {
            this.url = url.substring(0,url.lastIndexOf('/'));
            this.jsonData = jsonData;
        }

        loadNode(context,jsonData,parent,promises) {
            // jsonData: object, input. Json data for the node
            // parent: scene node, input. The parent node to which we must to add the new scene node.
            // promises: array, output. Add promises from component.deserialize()
            let node = new bg.scene.Node(context,jsonData.name);
			node.enabled = jsonData.enabled;
			node.steady = jsonData.steady || false;
            parent.addChild(node);
            jsonData.components.forEach((compData) => {
                promises.push(bg.scene.Component.Factory(context,compData,node,this.url));
            });
            jsonData.children.forEach((child) => {
                this.loadNode(context,child,node,promises);
            });
        }

        loadScene(context) {
            let promises = [];
            let sceneRoot = new bg.scene.Node(context,"scene-root");

            this.jsonData.scene.forEach((nodeData) => {
                this.loadNode(context,nodeData,sceneRoot,promises);
            });

            return new Promise((resolve,reject) => {
                Promise.all(promises)
                    .then(() => {
                        let findVisitor = new bg.scene.FindComponentVisitor("bg.scene.Camera");
                        sceneRoot.accept(findVisitor);
                        
						let cameraNode = null;
						let firstCamera = null;
                        findVisitor.result.some((cn) => {
							if (!firstCamera) {
								firstCamera = cn;
							}
							if (cn.camera.isMain) {
								cameraNode = cn;
								return true;
							}
						});
						cameraNode = cameraNode || firstCamera;
                        if (!cameraNode) {
							cameraNode = new bg.scene.Node(context,"Camera");
							cameraNode.addComponent(new bg.scene.Camera());
                            let trx = bg.Matrix4.Rotation(0.52,-1,0,0);
                            trx.translate(0,0,5);
                            cameraNode.addComponent(new bg.scene.Transform(trx));
                            sceneRoot.addChild(cameraNode);
						}
						
						// Ensure that cameraNode is the only camera marked as main
						bg.scene.Camera.SetAsMainCamera(cameraNode,sceneRoot);
                        resolve({ sceneRoot:sceneRoot, cameraNode:cameraNode });
                    });
            });
        }

    }

    class SceneLoaderPlugin extends bg.base.LoaderPlugin {
		acceptType(url,data) {
            let ext = bg.utils.Resource.GetExtension(url);
			return ext=="vitscnj";
		}
		
		load(context,url,data) {
			return new Promise((resolve,reject) => {
				if (data) {
					try {
                        if (typeof(data)=="string") {
                            // Prevent a bug in the C++ API version 2.0, that inserts a comma after the last
                            // element of some arrays and objects
                            data = data.replace(/,[\s\r\n]*\]/g,']');
                            data = data.replace(/,[\s\r\n]*\}/g,'}');
                            data = JSON.parse(data);
                        }
                        let parser = new SceneFileParser(url,data);
                        parser.loadScene(context)
                            .then((result) => {
                                resolve(result);
                            });
					}
					catch(e) {
						reject(e);
					}
				}
				else {
					reject(new Error("Error loading scene. Data is null"));
				}
			});
		}
	}
	
	bg.base.SceneLoaderPlugin = SceneLoaderPlugin;

})();
(function() {

    function copyCubemapImage(componentData,cubemapImage,dstPath) {
        let path = require("path");
        let src = bg.base.Writer.StandarizePath(this.getImageUrl(cubemapImage));
        let file = src.split('/').pop();
        let dst = bg.base.Writer.StandarizePath(path.join(dstPath,file));
        switch (cubemapImage) {
        case bg.scene.CubemapImage.POSITIVE_X:
            componentData.positiveX = file;
            break;
        case bg.scene.CubemapImage.NEGATIVE_X:
            componentData.negativeX = file;
            break;
        case bg.scene.CubemapImage.POSITIVE_Y:
            componentData.positiveY = file;
            break;
        case bg.scene.CubemapImage.NEGATIVE_Y:
            componentData.negativeY = file;
            break;
        case bg.scene.CubemapImage.POSITIVE_Z:
            componentData.positiveZ = file;
            break;
        case bg.scene.CubemapImage.NEGATIVE_Z:
            componentData.negativeZ = file;
            break;
        }
        return bg.base.Writer.CopyFile(src,dst);
    }

    let g_backFace       = [  0.5,-0.5,-0.5, -0.5,-0.5,-0.5, -0.5, 0.5,-0.5,  0.5, 0.5,-0.5 ];
    let g_rightFace      = [  0.5,-0.5, 0.5,  0.5,-0.5,-0.5,  0.5, 0.5,-0.5,  0.5, 0.5, 0.5 ];
    let g_frontFace      = [ -0.5,-0.5, 0.5,  0.5,-0.5, 0.5,  0.5, 0.5, 0.5, -0.5, 0.5, 0.5 ];
    let g_leftFace       = [ -0.5,-0.5,-0.5, -0.5,-0.5, 0.5, -0.5, 0.5, 0.5, -0.5, 0.5,-0.5 ];
    let g_topFace        = [ -0.5, 0.5, 0.5,  0.5, 0.5, 0.5,  0.5, 0.5,-0.5, -0.5, 0.5,-0.5 ];
    let g_bottomFace     = [  0.5,-0.5, 0.5, -0.5,-0.5, 0.5, -0.5,-0.5,-0.5,  0.5,-0.5,-0.5 ];

    let g_backFaceNorm   = [ 0, 0, 1,  0, 0, 1,  0, 0, 1,  0, 0, 1 ];
    let g_rightFaceNorm  = [-1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0 ];
    let g_frontFaceNorm  = [ 0, 0,-1,  0, 0,-1,  0, 0,-1,  0, 0,-1 ];
    let g_leftFaceNorm   = [ 1, 0, 0,  1, 0, 0,  1, 0, 0,  1, 0, 0 ];
    let g_topFaceNorm    = [ 0,-1, 0,  0,-1, 0,  0,-1, 0,  0,-1, 0 ];
    let g_bottomFaceNorm = [ 0, 1, 0,  0, 1, 0,  0, 1, 0,  0, 1, 0 ];

    let uv0 = 0;
    let uv1 = 1;
    let g_backFaceUV     = [ uv1,uv0, uv0,uv0, uv0,uv1, uv1,uv1 ];
    let g_rightFaceUV    = [ uv1,uv0, uv0,uv0, uv0,uv1, uv1,uv1 ];
    let g_frontFaceUV    = [ uv1,uv0, uv0,uv0, uv0,uv1, uv1,uv1 ];
    let g_leftFaceUV     = [ uv1,uv0, uv0,uv0, uv0,uv1, uv1,uv1 ];
    let g_topFaceUV      = [ uv1,uv0, uv0,uv0, uv0,uv1, uv1,uv1 ];
    let g_bottomFaceUV   = [ uv1,uv0, uv0,uv0, uv0,uv1, uv1,uv1 ];

    let g_index = [ 2,1,0, 0,3,2 ];

    class Skybox extends bg.scene.Component {
        constructor() {
            super();
            this._images = [null, null, null, null, null, null];
            this._textures = [];
            this._plist = [];
            this._material = null;
        }

        clone(context) {
            let result = new Skybox();
            result._images = [
                this._images[0],
                this._images[1],
                this._images[2],
                this._images[3],
                this._images[4],
                this._images[5]
            ];
            context = context || this.node && this.node.context;
            if (context) {
                result.loadSkybox(context);
            }
            return result;
        }

        setImageUrl(imgCode,texture) {
            this._images[imgCode] = texture;
        }

        getImageUrl(imgCode) {
            return this._images[imgCode];
        }

        getTexture(imgCode) {
            return this._textures[imgCode];
        }

        loadSkybox(context = null,onProgress = null) {
            context = context || this.node && this.node.context;

            let backPlist   = new bg.base.PolyList(context);
            let rightPlist  = new bg.base.PolyList(context);
            let frontPlist  = new bg.base.PolyList(context);
            let leftPlist   = new bg.base.PolyList(context);
            let topPlist    = new bg.base.PolyList(context);
            let bottomPlist = new bg.base.PolyList(context);

            backPlist.vertex = g_backFace; backPlist.normal = g_backFaceNorm; backPlist.texCoord0 = g_backFaceUV; backPlist.texCoord1 = g_backFaceUV; backPlist.index = g_index;
            backPlist.build();

            rightPlist.vertex = g_rightFace; rightPlist.normal = g_rightFaceNorm; rightPlist.texCoord0 = g_rightFaceUV; rightPlist.texCoord1 = g_rightFaceUV; rightPlist.index = g_index;
            rightPlist.build();

            frontPlist.vertex = g_frontFace; frontPlist.normal = g_frontFaceNorm; frontPlist.texCoord0 = g_frontFaceUV; frontPlist.texCoord1 = g_frontFaceUV; frontPlist.index = g_index;
            frontPlist.build();

            leftPlist.vertex = g_leftFace; leftPlist.normal = g_leftFaceNorm; leftPlist.texCoord0 = g_leftFaceUV; leftPlist.texCoord1 = g_leftFaceUV; leftPlist.index = g_index;
            leftPlist.build();

            topPlist.vertex = g_topFace; topPlist.normal = g_topFaceNorm; topPlist.texCoord0 = g_topFaceUV; topPlist.texCoord1 = g_topFaceUV; topPlist.index = g_index;
            topPlist.build();

            bottomPlist.vertex = g_bottomFace; bottomPlist.normal = g_bottomFaceNorm; bottomPlist.texCoord0 = g_bottomFaceUV; bottomPlist.texCoord1 = g_bottomFaceUV; bottomPlist.index = g_index;
            bottomPlist.build();

            this._plist = [leftPlist,rightPlist,topPlist,bottomPlist,frontPlist,backPlist];
            this._material = new bg.base.Material();
            this._material.receiveShadows = false;
            this._material.castShadows = false;
            this._material.unlit = true;


            return new Promise((resolve,reject) => {
                bg.base.Loader.Load(context,this._images,onProgress, {
                    wrapX:bg.base.TextureWrap.MIRRORED_REPEAT,
                    wrapY:bg.base.TextureWrap.MIRRORED_REPEAT
                })
                    .then((result) => {
                        this._textures = [
                            result[this.getImageUrl(bg.scene.CubemapImage.POSITIVE_X)],
                            result[this.getImageUrl(bg.scene.CubemapImage.NEGATIVE_X)],
                            result[this.getImageUrl(bg.scene.CubemapImage.POSITIVE_Y)],
                            result[this.getImageUrl(bg.scene.CubemapImage.NEGATIVE_Y)],
                            result[this.getImageUrl(bg.scene.CubemapImage.POSITIVE_Z)],
                            result[this.getImageUrl(bg.scene.CubemapImage.NEGATIVE_Z)]
                        ];
                        this._textures.forEach((tex) => {
                            tex.wrapX = bg.base.TextureWrap.CLAMP;
                            tex.wrapY = bg.base.TextureWrap.CLAMP;
                        });
                        bg.emitImageLoadEvent(result[this.getImageUrl(bg.scene.CubemapImage.POSITIVE_X)]);
                        resolve();
                    })
                    .catch((err) => {
                        reject(err);
                    });
            })
        }

        display(pipeline,matrixState) {
            // TODO: extract far clip plane from projection matrix and use it to scale the cube before draw it
            if (!pipeline.effect) {
                throw new Error("Could not draw skybox: invalid effect");
            }
            if (!this.node.enabled) {
                return;
            }
            else if (this._textures.length==6) {
                let curMaterial = pipeline.effect.material;
                pipeline.effect.material = this._material;
                matrixState.viewMatrixStack.push();
                matrixState.modelMatrixStack.push();
                matrixState.viewMatrixStack.matrix.setPosition(0,0,0);

                let projectionMatrix = matrixState.projectionMatrixStack.matrix;
                let m22 = -projectionMatrix.m22;
                let m32 = -projectionMatrix.m32;
                let far = (2.0*m32)/(2.0*m22-2.0);
                
                let offset = 1;
                let scale = bg.Math.sin(bg.Math.PI_4) * far - offset;
                matrixState.modelMatrixStack.scale(scale,scale,scale);
                
                if (pipeline.shouldDraw(this._material)) {
                    this._plist.forEach((pl,index) => {
                        this._material.texture = this._textures[index];
                        pipeline.draw(pl);
                    });
                }

                matrixState.modelMatrixStack.pop();
                matrixState.viewMatrixStack.pop();
                pipeline.effect.material = curMaterial;
            }
        }

        draw(renderQueue,modelMatrixStack,viewMatrixStack,projectionMatrixStack) {
            if (this._textures.length==6) {
                viewMatrixStack.push();
                modelMatrixStack.push();

                viewMatrixStack.matrix.setPosition(0,0,0);

                let projectionMatrix = projectionMatrixStack.matrix;
                let m22 = -projectionMatrix.m22;
                let m32 = -projectionMatrix.m32;
                let far = (2.0*m32)/(2.0*m22-2.0);
                
                let offset = 1;
                let scale = bg.Math.sin(bg.Math.PI_4) * far - offset;
                modelMatrixStack.scale(scale,scale,scale);

                this._plist.forEach((pl,index) => {
                    this._material.texture = this._textures[index];
                    renderQueue.renderOpaque(pl,this._material.clone(),modelMatrixStack.matrix,viewMatrixStack.matrix);
                })

                viewMatrixStack.pop();
                modelMatrixStack.pop();
            }
        }

        removedFromNode() {
            this._plist.forEach((pl) => {
                pl.destroy();
            });
        }

        deserialize(context,sceneData,url) {
            this.setImageUrl(
                bg.scene.CubemapImage.POSITIVE_X,
                bg.utils.Resource.JoinUrl(url,sceneData["positiveX"])
            );
            this.setImageUrl(
                bg.scene.CubemapImage.NEGATIVE_X,
                bg.utils.Resource.JoinUrl(url,sceneData["negativeX"])
            );
            this.setImageUrl(
                bg.scene.CubemapImage.POSITIVE_Y,
                bg.utils.Resource.JoinUrl(url,sceneData["positiveY"])
            );
            this.setImageUrl(
                bg.scene.CubemapImage.NEGATIVE_Y,
                bg.utils.Resource.JoinUrl(url,sceneData["negativeY"])
            );
            this.setImageUrl(
                bg.scene.CubemapImage.POSITIVE_Z,
                bg.utils.Resource.JoinUrl(url,sceneData["positiveZ"])
            );
            this.setImageUrl(
                bg.scene.CubemapImage.NEGATIVE_Z,
                bg.utils.Resource.JoinUrl(url,sceneData["negativeZ"])
            );
            return this.loadSkybox(context);
        }

        serialize(componentData,promises,url) {
            super.serialize(componentData,promises,url);
            if (!bg.isElectronApp) return;
            promises.push(copyCubemapImage.apply(this,[componentData,bg.scene.CubemapImage.POSITIVE_X,url.path]));
            promises.push(copyCubemapImage.apply(this,[componentData,bg.scene.CubemapImage.NEGATIVE_X,url.path]));
            promises.push(copyCubemapImage.apply(this,[componentData,bg.scene.CubemapImage.POSITIVE_Y,url.path]));
            promises.push(copyCubemapImage.apply(this,[componentData,bg.scene.CubemapImage.NEGATIVE_Y,url.path]));
            promises.push(copyCubemapImage.apply(this,[componentData,bg.scene.CubemapImage.POSITIVE_Z,url.path]));
            promises.push(copyCubemapImage.apply(this,[componentData,bg.scene.CubemapImage.NEGATIVE_Z,url.path]));
        }
    }

    bg.scene.registerComponent(bg.scene,Skybox,"bg.scene.Skybox");
})();
(function() {
    class TextRect extends bg.scene.Component {
        constructor(rectSize = new bg.Vector2(1,1),textureSize = new bg.Vector2(1000,1000)) {
            super();

            this._rectSize = rectSize;
            this._textureSize = textureSize;

            this._textProperties = new bg.base.TextProperties();
            this._doubleSided = true;
            this._unlit = false;
            this._text = "Hello, World!";

            this._sprite = null;
            this._material = null;

            this._sizeMatrix = bg.Matrix4.Scale(this._rectSize.x,this._rectSize.y,1);

            this._canvasTexture = null;
            this._dirty = true;
        }

        clone() {
            let newInstance = new bg.scene.TextRect();
            newInstance._text = this._text;
            newInstance._sprite = this._sprite && this._sprite.clone();
            newInstance._material = this._material && this._material.clone();

            // TODO: Clone other properties
            return newInstance;
        }

        get textProperties() { return this._textProperties; }
        get text() { return this._text; }
        set text(t) { this._dirty = true; this._text = t; }
        get doubleSided() { return this._doubleSided; }
        set doubleSided(ds) { this._dirty = true; this._doubleSided = ds; }
        get unlit() { return this._unlit; }
        set unlit(ul) { this._dirty = true; this._unlit = ul; }
        get rectSize() { return this._rectSize; }
        set rectSize(s) {
            this._sizeMatrix.identity().scale(s.x,s.y,1);
            this._rectSize = s;
        }

        // TODO: update texture size
        get textureSize() { return this._textureSize; }
        set textureSize(t) {
            this._dirty = true;
            this._canvasTexture.resize(t.x,t.y);
            this._textureSize = t;
        }

        get material() { return this._material; }

        init() {
            if (!this._sprite && this.node && this.node.context) {
                this._sprite = bg.scene.PrimitiveFactory.PlanePolyList(this.node.context,1,1,'z');
                this._material = new bg.base.Material();
                this._material.alphaCutoff = 0.9;
                this._dirty = true;
            }
            if (!this._canvasTexture && this.node && this.node.context) {
                this._canvasTexture = new bg.tools.CanvasTexture(this.node.context,this._textureSize.x,this._textureSize.y,
                    (ctx,w,h) => {
                        ctx.clearRect(0,0,w,h);
                        if (this._textProperties.background!="transparent") {
                            ctx.fillStyle = this._textProperties.background;
                            ctx.fillRect(0,0,w,h);
                        }
                        ctx.fillStyle = this._textProperties.color;
                        let textSize = this._textProperties.size;
                        let font = this._textProperties.font;
                        let padding = 0;
                        let italic = this._textProperties.italic ? "italic" : "";
                        let bold = this._textProperties.bold ? "bold" : "";
                        ctx.textAlign = this._textProperties.align;
                        ctx.font = `${ italic } ${ bold } ${ textSize }px ${ font }`;    // TODO: Font and size
                        let textWidth = ctx.measureText(this._text);
                        let x = 0;
                        let y = 0;
                        switch (ctx.textAlign) {
                        case "center":
                            x = w / 2;
                            y = textSize + padding;
                            break;
                        case "right":
                            x = w;
                            y = textSize + padding;
                            break;
                        default:
                            x = padding;
                            y = textSize + padding;
                        }
                        let textLines = this._text.split("\n");
                        textLines.forEach((line) => {
                            ctx.fillText(line,x, y);
                            y += textSize;
                        });
                    }
                );
                this._dirty = true;
            }
        }

        frame(delta) {
            if ((this._dirty || this._textProperties.dirty)  && this._material && this._canvasTexture) {
                this._canvasTexture.update();
                this._material.texture = this._canvasTexture.texture;
                this._material.unlit = this._unlit;
                this._material.cullFace = !this._doubleSided;
                this._dirty = false;
                this.textProperties.dirty = false;
            }
        }

        ////// Direct rendering functions: will be deprecated soon
        display(pipeline,matrixState) {
            if (!pipeline.effect) {
                throw new Error("Could not draw TextRect: invalid effect");
            }
            if (!this.node.enabled) {
                return;
            }
            else if (this._sprite && this._material) {
                if (this._sprite.visible) {
                    let curMaterial = pipeline.effect.material;
                    matrixState.modelMatrixStack.push();
                    matrixState.modelMatrixStack.mult(this._sizeMatrix);

                    if (pipeline.shouldDraw(this._material)) {
                        pipeline.effect.material = this._material;
                        pipeline.draw(this._sprite);
                    }

                    matrixState.modelMatrixStack.pop();
                    pipeline.effect.material = curMaterial;
                }
            }
        }

        ///// Render queue functions
        draw(renderQueue,modelMatrixStack,viewMatrixStack,projectionMatrixStack) {
            if (this._sprite && this._material) {
                modelMatrixStack.push();
                modelMatrixStack.mult(this._sizeMatrix);

                if (this._material.isTransparent) {
					renderQueue.renderTransparent(this._sprite,this._material,modelMatrixStack.matrix,viewMatrixStack.matrix);
				}
				else {
					renderQueue.renderOpaque(this._sprite,this._material,modelMatrixStack.matrix,viewMatrixStack.matrix);
				}

                modelMatrixStack.pop();
            }
        }

        serialize(componentData,promises,url) {
            componentData.textProperties = {};
            this.textProperties.serialize(componentData.textProperties);
            componentData.text = this.text;
            componentData.doubleSided = this.doubleSided;
            componentData.unlit = this.unlit;
            componentData.textureSize = this.textureSize.toArray();
            componentData.rectSize = this.rectSize.toArray();
        }

        deserialize(context,sceneData,url) {
            this.textProperties.deserialize(sceneData.textProperties);
            this.text = sceneData.text;
            this.doubleSided = sceneData.doubleSided;
            this.unlit = sceneData.unlit;
            this.textureSize = new bg.Vector2(sceneData.textureSize);
            this.rectSize = new bg.Vector2(sceneData.rectSize);
        }
    }

    bg.scene.registerComponent(bg.scene,TextRect,"bg.scene.TextRect");
})();
(function() {
	
	class Transform extends bg.scene.Component {
		constructor(matrix) {
			super();
			
			this._matrix = matrix || bg.Matrix4.Identity();
			this._globalMatrixValid = false;
			this._transformVisitor = new bg.scene.TransformVisitor();
		}
		
		clone() {
			let newTrx = new bg.scene.Transform();
			newTrx.matrix = new bg.Matrix4(this.matrix);
			return newTrx;
		}
		
		get matrix() { return this._matrix; }
		set matrix(m) { this._matrix = m; }

		get globalMatrix() {
			if (!this._globalMatrixValid) {
				this._transformVisitor.clear();
				this.node.acceptReverse(this._transformVisitor);
				this._globalMatrix = this._transformVisitor.matrix;
			}
			return this._globalMatrix;
		}
		
		deserialize(context,sceneData,url) {
			return new Promise((resolve,reject) => {
				if (sceneData.transformStrategy) {
					let str = sceneData.transformStrategy;
					if (str.type=="TRSTransformStrategy") {
						this._matrix
							.identity()
							.translate(str.translate[0],str.translate[1],str.translate[2]);
						switch (str.rotationOrder) {
						case "kOrderXYZ":
							this._matrix
								.rotate(str.rotateX,1,0,0)
								.rotate(str.rotateY,0,1,0)
								.rotate(str.rotateZ,0,0,1);
							break;
						case "kOrderXZY":
							this._matrix
								.rotate(str.rotateX,1,0,0)
								.rotate(str.rotateZ,0,0,1)
								.rotate(str.rotateY,0,1,0);
							break;
						case "kOrderYXZ":
							this._matrix
								.rotate(str.rotateY,0,1,0)
								.rotate(str.rotateX,1,0,0)
								.rotate(str.rotateZ,0,0,1);
							break;
						case "kOrderYZX":
							this._matrix
								.rotate(str.rotateY,0,1,0)
								.rotate(str.rotateZ,0,0,1)
								.rotate(str.rotateX,1,0,0);
							break;
						case "kOrderZYX":
							this._matrix
								.rotate(str.rotateZ,0,0,1)
								.rotate(str.rotateY,0,1,0)
								.rotate(str.rotateX,1,0,0);
							break;
						case "kOrderZXY":
							this._matrix
								.rotate(str.rotateZ,0,0,1)
								.rotate(str.rotateX,1,0,0)
								.rotate(str.rotateY,0,1,0);
							break;
						}
						this._matrix.scale(str.scale[0],str.scale[1],str.scale[2])
					}
				}
				else if (sceneData.transformMatrix) {
					this._matrix = new bg.Matrix4(sceneData.transformMatrix);
				}
				resolve(this);
			});
		}

		serialize(componentData,promises,url) {
			super.serialize(componentData,promises,url);
			componentData.transformMatrix = this._matrix.toArray();
		}
		
		// The direct render methods will be deprecated soon
		////// Direct render methods
		willDisplay(pipeline,matrixState) {
			if (this.node && this.node.enabled) {
				matrixState.modelMatrixStack.push();
				matrixState.modelMatrixStack.mult(this.matrix);
			}
		}
		
		didDisplay(pipeline,matrixState) {
			if (this.node && this.node.enabled) {
				matrixState.modelMatrixStack.pop();
			}
			this._globalMatrixValid = false;
		}
		////// End direct render methods


		////// Render queue methods
		willUpdate(modelMatrixStack,viewMatrixStack,projectionMatrixStack) {
			if (this.node && this.node.enabled) {
				modelMatrixStack.push();
				modelMatrixStack.mult(this.matrix);
			}
		}

		didUpdate(modelMatrixStack,viewMatrixStack,projectionMatrixStack) {
			if (this.node && this.node.enabled) {
				modelMatrixStack.pop();
			}
			this._globalMatrixValid = false;
		}
		////// End render queue methods
		
	}
	
	bg.scene.registerComponent(bg.scene,Transform,"bg.scene.Transform");
	
})();
(function() {
	
	class DrawVisitor extends bg.scene.NodeVisitor {
		constructor(pipeline,matrixState) {
			super();
			this._pipeline = pipeline || bg.base.Pipeline.Current();
			this._matrixState = matrixState || bg.base.MatrixState.Current();
			this._forceDraw = false;
		}

		get forceDraw() { return this._forceDraw; }
		set forceDraw(f) { this._forceDraw = f; }
		
		get pipeline() { return this._pipeline; }
		get matrixState() { return this._matrixState; }
		
		visit(node) {
			node.willDisplay(this.pipeline,this.matrixState);
			node.display(this.pipeline,this.matrixState,this.forceDraw);
		}
		
		didVisit(node) {
			node.didDisplay(this.pipeline,this.matrixState);
		}
	}
	
	bg.scene.DrawVisitor = DrawVisitor;

	class RenderQueueVisitor extends bg.scene.NodeVisitor {
		constructor(modelMatrixStack,viewMatrixStack,projectionMatrixStack) {
			super();
			this._modelMatrixStack = modelMatrixStack || new bg.base.MatrixStack();
			this._viewMatrixStack = viewMatrixStack || new bg.base.MatrixStack();
			this._projectionMatrixStack = projectionMatrixStack || new bg.base.MatrixStack();
			this._renderQueue = new bg.base.RenderQueue();
		}

		get modelMatrixStack() { return this._modelMatrixStack; }
		set modelMatrixStack(m) { this._modelMatrixStack = m; }

		get viewMatrixStack() { return this._viewMatrixStack; }
		set viewMatrixStack(m) { this._viewMatrixStack = m; }

		get projectionMatrixStack() { return this._projectionMatrixStack }
		set projectionMatrixStack(m) { this._projectionMatrixStack = m; }

		get renderQueue() { return this._renderQueue; }

		visit(node) {
			node.willUpdate(this._modelMatrixStack);
			node.draw(this._renderQueue,this._modelMatrixStack, this._viewMatrixStack, this._projectionMatrixStack);
		}

		didVisit(node) {
			node.didUpdate(this._modelMatrixStack, this._viewMatrixStack, this._projectionMatrixStack);
		}
	}

	bg.scene.RenderQueueVisitor = RenderQueueVisitor;
	
	class FrameVisitor extends bg.scene.NodeVisitor {
		constructor() {
			super();
			this._delta = 0;
		}
		
		get delta() { return this._delta; }
		set delta(d) { this._delta = d; }

		visit(node) {
			node.frame(this.delta);
		}
	}
	
	bg.scene.FrameVisitor = FrameVisitor;
	
	class TransformVisitor extends bg.scene.NodeVisitor {
		constructor() {
			super();
			this._matrix = bg.Matrix4.Identity();
		}
		
		get matrix() { return this._matrix; }
		
		clear() {
			this._matrix = bg.Matrix4.Identity();
		}

		visit(node) {
			let trx = node.component("bg.scene.Transform");
			if (trx) {
				this._matrix.mult(trx.matrix);
			}
		}
	}
	
	bg.scene.TransformVisitor = TransformVisitor;
	
	class InputVisitor extends bg.scene.NodeVisitor {
		
		visit(node) {
			if (this._operation) {
				node[this._operation](this._event);
			}
		}
		
		keyDown(scene,evt) {
			this._operation = "keyDown";
			this._event = evt;
			scene.accept(this);
		}
		
		keyUp(scene,evt) {
			this._operation = "keyUp";
			this._event = evt;
			scene.accept(this);
		}
		
		mouseUp(scene,evt) {
			this._operation = "mouseUp";
			this._event = evt;
			scene.accept(this);
		}
		
		mouseDown(scene,evt) {
			this._operation = "mouseDown";
			this._event = evt;
			scene.accept(this);
		}
		
		mouseMove(scene,evt) {
			this._operation = "mouseMove";
			this._event = evt;
			scene.accept(this);
		}
		
		mouseOut(scene,evt) {
			this._operation = "mouseOut";
			this._event = evt;
			scene.accept(this);
		}
		
		mouseDrag(scene,evt) {
			this._operation = "mouseDrag";
			this._event = evt;
			scene.accept(this);
		}
		
		mouseWheel(scene,evt) {
			this._operation = "mouseWheel";
			this._event = evt;
			scene.accept(this);
		}
		
		touchStart(scene,evt) {
			this._operation = "touchStart";
			this._event = evt;
			scene.accept(this);
		}
		
		touchMove(scene,evt) {
			this._operation = "touchMove";
			this._event = evt;
			scene.accept(this);
		}
		
		touchEnd(scene,evt) {
			this._operation = "touchEnd";
			this._event = evt;
			scene.accept(this);
		}
	}
	
	bg.scene.InputVisitor = InputVisitor;

	class BoundingBoxVisitor extends bg.scene.NodeVisitor {
		constructor() {
			super();
			this.clear();
		}

		get min() {
			return this._min;
		}

		get max() {
			return this._max;
		}

		get size() {
			return this._size;
		}

		clear() {
			// left, right, bottom, top, back, front
			this._min = new bg.Vector3(bg.Math.FLOAT_MAX,bg.Math.FLOAT_MAX,bg.Math.FLOAT_MAX);
			this._max = new bg.Vector3(-bg.Math.FLOAT_MAX,-bg.Math.FLOAT_MAX,-bg.Math.FLOAT_MAX);
			this._size = new bg.Vector3(0,0,0);
		}

		visit(node) {
			let trx = bg.Matrix4.Identity();
			if (node.component("bg.scene.Transform")) {
				trx = node.component("bg.scene.Transform").globalMatrix;
			}
			if (node.component("bg.scene.Drawable")) {
				let bb = new bg.tools.BoundingBox(node.component("bg.scene.Drawable"),new bg.Matrix4(trx));
				this._min = bg.Vector.MinComponents(this._min,bb.min);
				this._max = bg.Vector.MaxComponents(this._max,bb.max);
				this._size = bg.Vector3.Sub(this._max, this._min);
			}
		}
	}

	bg.scene.BoundingBoxVisitor = BoundingBoxVisitor;


	class FindComponentVisitor extends bg.scene.NodeVisitor {
		constructor(componentId) {
			super();
			this.componentId = componentId;
			this.clear();
		}

		get result() {
			return this._result;
		}

		clear() {
			this._result = [];
		}

		visit(node) {
			if (node.component(this.componentId)) {
				this._result.push(node);
			}
		}
	}

	bg.scene.FindComponentVisitor = FindComponentVisitor;
})();
(function() {
		
	function readBlock(arrayBuffer,offset) {
		var block = new Uint8Array(arrayBuffer,offset,4);
		block = String.fromCharCode(block[0]) + String.fromCharCode(block[1]) + String.fromCharCode(block[2]) + String.fromCharCode(block[3]);
		return block;
	}

	function readInt(arrayBuffer,offset) {
		var dataView = new DataView(arrayBuffer,offset,4);
		return dataView.getInt32(0);
	}
	
	function readFloat(arrayBuffer,offset) {
		var dataView = new DataView(arrayBuffer,offset,4);
		return dataView.getFloat32(0);
	}
	
	function readMatrix4(arrayBuffer,offset) {
		var response = {offset:0,data:[]}
		var size = 16;
		var dataView = new DataView(arrayBuffer,offset, size*4);
		var littleEndian = false;
		for (var i=0;i<size;++i) {
			response.data[i] = dataView.getFloat32(i*4,littleEndian);
		}
		response.offset += size * 4;
		return response;
	}

	function readString(arrayBuffer,offset) {
		var response = {offset:0,data:""}
		var size = readInt(arrayBuffer,offset);
		response.offset += 4;
		var strBuffer = new Uint8Array(arrayBuffer, offset + 4, size);
		for (var i=0;i<size;++i) {
			response.data += String.fromCharCode(strBuffer[i]);
		}
		response.offset += size;
		return response;
	}
	
	function readFloatArray(arrayBuffer,offset) {
		var response = {offset:0,data:[]}
		var size = readInt(arrayBuffer,offset);
		response.offset += 4;
		var dataView = new DataView(arrayBuffer,offset + 4, size*4);
		var littleEndian = false;
		for (var i=0;i<size;++i) {
			response.data[i] = dataView.getFloat32(i*4,littleEndian);
		}
		response.offset += size * 4;
		return response;
	}
	
	function readIndexArray(arrayBuffer,offset) {
		var response = {offset:0,data:[]}
		var size = readInt(arrayBuffer,offset);
		response.offset += 4;
		var dataView = new DataView(arrayBuffer,offset + 4, size*4);
		var littleEndian = false;
		for (var i=0;i<size;++i) {
			response.data[i] = dataView.getInt32(i*4,littleEndian);
		}
		response.offset += size * 4;
		return response;
	}
	
	function addJoint(node,type,jointData) {
		let joint =  new bg.physics[jointData.type]();
		joint.offset = new bg.Vector3(...jointData.offset);
		joint.roll = jointData.roll;
		joint.pitch = jointData.pitch;
		joint.yaw = jointData.yaw;
		let component = new bg.scene[type](joint);
		node.addComponent(component);
	}
	
	class VWGLBParser {
		constructor(context,data) {
			this._context = context;
		}

		loadDrawable(data,path) {
			this._jointData = null;
			var parsedData = this.parseData(data);
			return this.createDrawable(parsedData,path);
		}
		
		parseData(data) {
			let polyLists = [];
			let materials = null;
			let components = null;
			
			let offset = 0;
			let header = new Uint8Array(data,0,8);
			offset = 8;
			let hdr = String.fromCharCode(header[4]) + String.fromCharCode(header[5]) + String.fromCharCode(header[6]) + String.fromCharCode(header[7]);
			
			if (header[0]==1) throw "Could not open the model file. This file has been saved as computer (little endian) format, try again saving it in network (big endian) format";
			if (hdr!='hedr') throw "File format error. Expecting header";
			
			let version = {maj:header[1],min:header[2],rev:header[3]};
			bg.log("vwglb file version: " + version.maj + "." + version.min + "." + version.rev + ", big endian");
			
			let numberOfPolyList = readInt(data,offset);
			offset += 4;
			
			let mtrl = readBlock(data,offset);
			offset += 4;
			if (mtrl!='mtrl') throw "File format error. Expecting materials definition";
			
			let matResult = readString(data,offset);
			offset += matResult.offset;
			materials = JSON.parse(matResult.data);
			
			let proj = readBlock(data,offset);
			if (proj=='proj') {
				// Projectors are deprecated. This section only skips the projector section
				offset += 4;				
				let shadowTexFile = readString(data,offset);
				offset += shadowTexFile.offset;
				
				let attenuation = readFloat(data,offset);
				offset +=4;
				
				let projectionMatData = readMatrix4(data,offset);
				offset += projectionMatData.offset;
				let projMatrix = projectionMatData.data;
				
				let transformMatData = readMatrix4(data,offset);
				offset += transformMatData.offset;
				let transMatrix = transformMatData.data;
				
				// model projectors are deprecated
				//projector = new vwgl.Projector();
				//projector.setProjection(new vwgl.Matrix4(projMatrix));
				//projector.setTransform(new vwgl.Matrix4(transMatrix));
				//projector.setTexture(loader.loadTexture(shadowTexFile.data))
			}
			
			let join = readBlock(data,offset);
			if (join=='join') {
				offset += 4;
				
				let jointData = readString(data,offset);
				offset += jointData.offset;
				
				let jointText = jointData.data;
				try {
					this._jointData = JSON.parse(jointText);
				}
				catch (e) {
					throw new Error("VWGLB file format reader: Error parsing joint data");
				}
			}
			
			let block = readBlock(data,offset);
			if (block!='plst') throw "File format error. Expecting poly list";
			let done = false;
			offset += 4;
			let plistName;
			let matName;
			let vArray;
			let nArray;
			let t0Array;
			let t1Array;
			let t2Array;
			let iArray;
			while (!done) {
				block = readBlock(data,offset);
				offset += 4;
				let strData = null;
				let tarr = null;
				switch (block) {
				case 'pnam':
					strData = readString(data,offset);
					offset += strData.offset;
					plistName = strData.data;
					break;
				case 'mnam':
					strData = readString(data,offset);
					offset += strData.offset;
					matName = strData.data;
					break;
				case 'varr':
					let varr = readFloatArray(data,offset);
					offset += varr.offset;
					vArray = varr.data;
					break;
				case 'narr':
					let narr = readFloatArray(data,offset);
					offset += narr.offset;
					nArray = narr.data;
					break;
				case 't0ar':
					tarr = readFloatArray(data,offset);
					offset += tarr.offset;
					t0Array = tarr.data;
					break;
				case 't1ar':
					tarr = readFloatArray(data,offset);
					offset += tarr.offset;
					t1Array = tarr.data;
					break;
				case 't2ar':
					tarr = readFloatArray(data,offset);
					offset += tarr.offset;
					t2Array = tarr.data;
					break;
				case 'indx':
					let iarr = readIndexArray(data,offset);
					offset += iarr.offset;
					iArray = iarr.data;
					break;
				case 'plst':
				case 'endf':
					if (block=='endf' && (offset + 4)<data.byteLength) {
						try {
							block = readBlock(data,offset);
							offset += 4;
							if (block=='cmps') {
								let componentData = readString(data,offset);
								offset += componentData.offset;
	
								// Prevent a bug in the C++ API version 2.0, that inserts a comma after the last
								// element of some arrays and objects
								componentData.data = componentData.data.replace(/,[\s\r\n]*\]/g,']');
								componentData.data = componentData.data.replace(/,[\s\r\n]*\}/g,'}');
								components = JSON.parse(componentData.data);
							}
						}
						catch (err) {
							console.error(err.message);
						}
						done = true;
					}
					else if ((offset + 4)>=data.byteLength) {
						done = true;
					}

					let plistData = {
						name:plistName,
						matName:matName,
						vertices:vArray,
						normal:nArray,
						texcoord0:t0Array,
						texcoord1:t1Array,
						texcoord2:t2Array,
						indices:iArray
					}
					polyLists.push(plistData)
					plistName = "";
					matName = "";
					vArray = null;
					nArray = null;
					t0Array = null;
					t1Array = null;
					t2Array = null;
					iArray = null;
					break;
				default:
					throw "File format exception. Unexpected poly list member found";
				}
			}

			var parsedData =  {
				version:version,
				polyList:polyLists,
				materials: {}
			}
			this._componentData = components;
			materials.forEach((matData) => {
				parsedData.materials[matData.name] = matData;
			});
			return parsedData;
		}
		
		createDrawable(data,path) {
			let drawable = new bg.scene.Drawable(this.context);
			drawable._version = data.version;
			let promises = [];
			
			data.polyList.forEach((plistData) => {
				let materialData = data.materials[plistData.matName];
				
				let polyList = new bg.base.PolyList(this._context);
				polyList.name = plistData.name;
				polyList.vertex = plistData.vertices || polyList.vertex;
				polyList.normal = plistData.normal || polyList.normal;
				polyList.texCoord0 = plistData.texcoord0 || polyList.texCoord0;
				polyList.texCoord1 = plistData.texcoord1 || polyList.texCoord1;
				polyList.texCoord2 = plistData.texcoord2 || polyList.texCoord2;
				polyList.index = plistData.indices || polyList.index;
				
				polyList.groupName = materialData.groupName;
				polyList.visible = materialData.visible;
				polyList.visibleToShadows = materialData.visibleToShadows!==undefined ? materialData.visibleToShadows : true;
				
				polyList.build();

				promises.push(bg.base.Material.GetMaterialWithJson(this._context,materialData,path)
					.then(function(material) {
						drawable.addPolyList(polyList,material);
					}));
			});
			
			return Promise.all(promises)
				.then(() => {
					return drawable;
				});
		}
		
		addComponents(node,url) {
			if (this._jointData) {
				let i = null;
				let o = null;
				if (this._jointData.input) {
					i = this._jointData.input;
				}
				if (this._jointData.output && this._jointData.output.length) {
					o = this._jointData.output[0];
				}
				
				if (i) addJoint(node,"InputChainJoint",i);
				if (o) addJoint(node,"OutputChainJoint",o);
			}

			if (this._componentData) {
				console.log("Component data found");
				let baseUrl = url;
				if (bg.isElectronApp) {
					baseUrl = bg.base.Writer.StandarizePath(url);
				}
				baseUrl = baseUrl.split("/");
				baseUrl.pop();
				baseUrl = baseUrl.join("/");
				this._componentData.forEach((cmpData) => {
					bg.scene.Component.Factory(this.context,cmpData,node,baseUrl)
				})
			}
		}
	}
	
	class VWGLBLoaderPlugin extends bg.base.LoaderPlugin {
		acceptType(url,data) {
			let ext = bg.utils.Resource.GetExtension(url);
			return ext=="vwglb" || ext=="bg2";
		}
		
		load(context,url,data) {
			return new Promise((accept,reject) => {
				if (data) {
					try {
						let parser = new VWGLBParser(context,data);
						let path = url.substr(0,url.lastIndexOf("/"));
						parser.loadDrawable(data,path)
							.then((drawable) => {
								let node = new bg.scene.Node(context,drawable.name);
								node.addComponent(drawable);
								parser.addComponents(node,url);
								accept(node);
							});
					}
					catch(e) {
						reject(e);
					}
				}
				else {
					reject(new Error("Error loading drawable. Data is null"));
				}
			});
		}
	}

	// This plugin load vwglb and bg2 files, but will also try to load the associated bg2mat file
	class Bg2LoaderPlugin extends VWGLBLoaderPlugin {
		load(context,url,data) {
			let promise = super.load(context,url,data);
			return new Promise((resolve,reject) => {
				promise
					.then((node) => {
						let basePath = url.split("/");
						basePath.pop();
						basePath = basePath.join("/") + '/';
						let matUrl = url.split(".");
						matUrl.pop();
						matUrl.push("bg2mat");
						matUrl = matUrl.join(".");
						bg.utils.Resource.LoadJson(matUrl)
							.then((matData) => {
								let promises = [];
								try {
									let drw = node.component("bg.scene.Drawable");
									drw.forEach((plist,mat)=> {
										let matDef = null;
										matData.some((defItem) => {
											if (defItem.name==plist.name) {
												matDef = defItem;
												return true;
											}
										});

										if (matDef) {
											let p = bg.base.Material.FromMaterialDefinition(context,matDef,basePath);
											promises.push(p)
											p.then((newMat) => {
													mat.assign(newMat);
												});
										}
									});
								}
								catch(err) {
									
								}
								return Promise.all(promises);
							})
							.then(() => {
								resolve(node);
							})
							.catch(() => {	// bg2mat file not found
								resolve(node);
							});
					})
					.catch((err) => {
						reject(err);
					});
			});
		}
	}

	bg.base.VWGLBLoaderPlugin = VWGLBLoaderPlugin;
	bg.base.Bg2LoaderPlugin = Bg2LoaderPlugin;
	
})();
bg.manipulation = {};
(function() {

    class DrawGizmoVisitor extends bg.scene.DrawVisitor {
        constructor(pipeline,matrixState) {
            super(pipeline,matrixState);
            this._sprite = bg.scene.PrimitiveFactory.PlanePolyList(pipeline.context,1,1,"z");

            this._gizmoScale = 1;

            this._gizmoIcons = [];

            this._show3dGizmos = true;
        }

        get gizmoScale() { return this._gizmoScale; }
        set gizmoScale(s) { this._gizmoScale = s; }

        get show3dGizmos() { return this._show3dGizmos; }
        set show3dGizmos(g) { this._show3dGizmos = g; }

        clearGizmoIcons() { this._gizmoIcons = []; }
        addGizmoIcon(type,icon,visible=true) { this._gizmoIcons.push({ type:type, icon:icon, visible:visible }); }
        setGizmoIconVisibility(type,visible) {
            this._gizmoIcons.some((iconData) => {
                if (iconData.type==type) {
                    iconData.visible = visible;
                }
            })
        }

        get gizmoIcons() { return this._gizmoIcons; }

        getGizmoIcon(node) {
            let icon = null;
            this._gizmoIcons.some((iconData) => {
                if (node.component(iconData.type) && iconData.visible) {
                    icon = iconData.icon;
                    return true;
                }
            });
            return icon;
        }

        visit(node) {
            super.visit(node);

            let icon = this.getGizmoIcon(node);
            let gizmoOpacity = this.pipeline.effect.gizmoOpacity;
            let gizmoColor = this.pipeline.effect.color;
            this.pipeline.effect.color = bg.Color.White();
            let dt = this.pipeline.depthTest;
            this.pipeline.depthTest = false;
            if (icon) {
                this.pipeline.effect.texture = icon;
                this.pipeline.effect.gizmoOpacity = 1;
                this.matrixState.viewMatrixStack.push();
                this.matrixState.modelMatrixStack.push();
                this.matrixState.viewMatrixStack.mult(this.matrixState.modelMatrixStack.matrix);
                this.matrixState.modelMatrixStack.identity();
                this.matrixState.viewMatrixStack.matrix.setRow(0,new bg.Vector4(1,0,0,0));
                this.matrixState.viewMatrixStack.matrix.setRow(1,new bg.Vector4(0,1,0,0));
                this.matrixState.viewMatrixStack.matrix.setRow(2,new bg.Vector4(0,0,1,0));
                let s = this.matrixState.cameraDistanceScale * 0.05 * this._gizmoScale;
                this.matrixState.viewMatrixStack.scale(s,s,s);
                this.pipeline.draw(this._sprite);
    
                this.matrixState.viewMatrixStack.pop();
                this.matrixState.modelMatrixStack.pop();
                this.pipeline.effect.gizmoOpacity = gizmoOpacity;
                this.pipeline.effect.texture = null;
            }
            if (this._show3dGizmos) {
                node.displayGizmo(this.pipeline,this.matrixState);
            }
            this.pipeline.effect.color = gizmoColor;
            this.pipeline.depthTest = dt;
        }

    }

    bg.manipulation = bg.manipulation || {};
    bg.manipulation.DrawGizmoVisitor = DrawGizmoVisitor;
})();
(function() {
	
	class GizmoManager extends bg.app.ContextObject {
		
		constructor(context) {
			super(context);
			this._gizmoOpacity = 0.9;
		}
		
		get pipeline() {
			if (!this._pipeline) {
				this._pipeline = new bg.base.Pipeline(this.context);
				this._pipeline.blendMode = bg.base.BlendMode.NORMAL;
				this._pipeline.effect = new bg.manipulation.GizmoEffect(this.context);
			}
			return this._pipeline;
		}
		
		get matrixState() {
			if (!this._matrixState) {
				this._matrixState = new bg.base.MatrixState();
			}
			return this._matrixState;
		}
		
		get drawVisitor() {
			if (!this._drawVisitor) {
				this._drawVisitor = new bg.manipulation.DrawGizmoVisitor(this.pipeline,this.matrixState);
			}
			return this._drawVisitor;
		}
		
		get gizmoOpacity() { return this._gizmoOpacity; }
		set gizmoOpacity(o) { this._gizmoOpacity = o; }

		get show3dGizmos() { return this.drawVisitor.show3dGizmos; }
		set show3dGizmos(g) { this.drawVisitor.show3dGizmos = g; }
		
		get working() { return this._working; }

		// Load icon textures manually
		// addGizmoIcon("bg.scene.Camera",cameraTexture)
		addGizmoIcon(type,iconTexture) {
			this.drawVisitor.addGizmoIcon(type,iconTexture);
		}

		get gizmoIconScale() { return this.drawVisitor.gizmoScale; }
		set gizmoIconScale(s) { this.drawVisitor.gizmoScale = s; }

		setGizmoIconVisibility(type,visible) { this.drawVisitor.setGizmoIconVisibility(type,visible); }
		hideGizmoIcon(type) { this.drawVisitor.setGizmoIconVisibility(type,false); }
		showGizmoIcon(type) { this.drawVisitor.setGizmoIconVisibility(type,true); }
		
		get gizmoIcons() { return this.drawVisitor.gizmoIcons; }

		/*
		 * Receives an array with the icon data, ordered by priority (only one component
		 * icon will be shown).
		 * iconData: [
		 * 		{ type:"bg.scene.Camera", icon:"../data/camera_gizmo.png" },
		 * 		{ type:"bg.scene.Light", icon:"../data/light_gizmo.png" },
		 * 		{ type:"bg.scene.Transform", icon:"../data/transform_gizmo.png" },
		 * 		{ type:"bg.scene.Drawable", icon:"../data/drawable_gizmo.png" },
		 * ],
		 * basePath: if specified, this path will be prepended to the icon paths
		 */
		loadGizmoIcons(iconData, basePath="",onProgress) {
			return new Promise((resolve,reject) => {
				let urls = [];
				let iconDataResult = [];
				iconData.forEach((data) => {
					let itemData = { type:data.type, iconTexture:null };
					itemData.path = bg.utils.path.join(basePath,data.icon);
					urls.push(itemData.path);
					iconDataResult.push(itemData);
				});
				bg.base.Loader.Load(this.context,urls,onProgress)
					.then((result) => {
						iconDataResult.forEach((dataItem) => {
							dataItem.iconTexture = result[dataItem.path];
							this.addGizmoIcon(dataItem.type,dataItem.iconTexture);
						})
						resolve(iconDataResult);
					})
					.catch((err) => {
						reject(err);
					});
			});
		}

		clearGizmoIcons() {
			this.drawVisitor.clearGizmoIcons();
		}
		
		startAction(gizmoPickData,pos) {
			this._working = true;
			this._startPoint = pos;
			this._currentGizmoData = gizmoPickData;
			if (this._currentGizmoData && this._currentGizmoData.node) {
				let gizmo = this._currentGizmoData.node.component("bg.manipulation.Gizmo");
				if (gizmo) {
					gizmo.beginDrag(this._currentGizmoData.action,pos);
				}
			}
		}
		
		move(pos,camera) {
			if (this._currentGizmoData && this._currentGizmoData.node) {
				let gizmo = this._currentGizmoData.node.component("bg.manipulation.Gizmo");
				if (gizmo) {
					pos.y = camera.viewport.height - pos.y;	// Convert to viewport coords
					gizmo.drag(this._currentGizmoData.action,this._startPoint,pos,camera);
				}
				this._startPoint = pos;
			}
		}
		
		endAction() {
			if (this._currentGizmoData && this._currentGizmoData.node) {
				let gizmo = this._currentGizmoData.node.component("bg.manipulation.Gizmo");
				if (gizmo) {
					gizmo.endDrag(this._currentGizmoData.action);
				}
			}
			this._working = false;
			this._startPoint = null;
			this._currentGizmoData = null;
		}
		
		drawGizmos(sceneRoot,camera,clearDepth=true) {
			let restorePipeline = bg.base.Pipeline.Current();
			let restoreMatrixState = bg.base.MatrixState.Current();
			bg.base.Pipeline.SetCurrent(this.pipeline);
			bg.base.MatrixState.SetCurrent(this.matrixState);
			this.pipeline.viewport = camera.viewport;
			this.pipeline.effect.matrixState = this.matrixState;
			
			if (clearDepth) {
				this.pipeline.clearBuffers(bg.base.ClearBuffers.DEPTH);
			}
			
			this.matrixState.projectionMatrixStack.set(camera.projection);
			this.matrixState.viewMatrixStack.set(camera.viewMatrix);										
			
			let opacityLayer = this.pipeline.opacityLayer;
			this.pipeline.opacityLayer = bg.base.OpacityLayer.NONE;
			
			this.pipeline.blend = true;
			this.pipeline.effect.gizmoOpacity = this.gizmoOpacity;
			sceneRoot.accept(this.drawVisitor);
			this.pipeline.blend = false;
			
			this.pipeline.opacityLayer = opacityLayer;
			
			if (restorePipeline) {
				bg.base.Pipeline.SetCurrent(restorePipeline);
			}
			if (restoreMatrixState) {
				bg.base.MatrixState.SetCurrent(restoreMatrixState);
			}
		}
	}
	
	bg.manipulation.GizmoManager = GizmoManager;
	
})();

(function() {
	
	let shaders = {};

	function initShaders() {
		shaders[bg.webgl1.EngineId] = {
			vertex: `
			attribute vec3 inVertex;
			attribute vec2 inTexCoord;
			attribute vec4 inVertexColor;
			
			uniform mat4 inModelMatrix;
			uniform mat4 inViewMatrix;
			uniform mat4 inProjectionMatrix;
			
			varying vec2 fsTexCoord;
			varying vec4 fsColor;
			
			void main() {
				fsTexCoord = inTexCoord;
				fsColor = inVertexColor;
				gl_Position = inProjectionMatrix * inViewMatrix * inModelMatrix * vec4(inVertex,1.0);
			}
			`,
			
			fragment:`
			precision highp float;
			
			uniform vec4 inColor;
			uniform sampler2D inTexture;
			uniform float inOpacity;
			
			varying vec2 fsTexCoord;
			varying vec4 fsColor;
			
			void main() {
				vec4 tex = texture2D(inTexture,fsTexCoord);
				gl_FragColor = vec4(fsColor.rgb * tex.rgb * inColor.rgb,inOpacity * tex.a);
			}
			`
		};
	}
	
	class GizmoEffect extends bg.base.Effect {
		constructor(context) { 
			super(context);
			initShaders();
			this._gizmoOpacity = 1;
			this._color = bg.Color.White();
		}
		
		get inputVars() {
			return {
				vertex:'inVertex',
				color:'inVertexColor',
				tex0:'inTexCoord'
			}
		}
		
		set matrixState(m) { this._matrixState = m; }
		get matrixState() {
			return this._matrixState;
		}
		
		set texture(t) { this._texture = t; }
		get texture() { return this._texture; }
		
		set color(c) { this._color = c; }
		get color() { return this._color; }

		set gizmoOpacity(o) { this._gizmoOpacity = o; }
		get gizmoOpacity() { return this._gizmoOpacity; }
				
		get shader() {
			if (!this._shader) {
				this._shader = new bg.base.Shader(this.context);
				this._shader.addShaderSource(bg.base.ShaderType.VERTEX, shaders[bg.webgl1.EngineId].vertex);
				this._shader.addShaderSource(bg.base.ShaderType.FRAGMENT, shaders[bg.webgl1.EngineId].fragment);
				this._shader.link();
				if (!this._shader.status) {
					console.log(this._shader.compileError);
					console.log(this._shader.linkError);
				}
				else {
					this._shader.initVars([
						'inVertex',
						'inVertexColor',
						'inTexCoord'
					],[
						'inModelMatrix',
						'inViewMatrix',
						'inProjectionMatrix',
						'inColor',
						'inTexture',
						'inOpacity'
					]);
				}
			}
			return this._shader
		}
		
		setupVars() {
			let whiteTexture = bg.base.TextureCache.WhiteTexture(this.context);
			this.shader.setMatrix4('inModelMatrix',this.matrixState.modelMatrixStack.matrixConst);
			this.shader.setMatrix4('inViewMatrix',new bg.Matrix4(this.matrixState.viewMatrixStack.matrixConst));
			this.shader.setMatrix4('inProjectionMatrix',this.matrixState.projectionMatrixStack.matrixConst);
			this.shader.setVector4('inColor',this.color);
			this.shader.setTexture('inTexture', this.texture ? this.texture : whiteTexture,bg.base.TextureUnit.TEXTURE_0);
			this.shader.setValueFloat('inOpacity',this.gizmoOpacity);
		}
		
	}
	
	bg.manipulation.GizmoEffect = GizmoEffect;

	bg.manipulation.GizmoAction = {
		TRANSLATE: 1,
		ROTATE: 2,
		ROTATE_FINE: 3,
		SCALE: 4,
		TRANSLATE_X: 5,
		TRANSLATE_Y: 6,
		TRANSLATE_Z: 7,
		ROTATE_X: 8,
		ROTATE_Y: 9,
		ROTATE_Z: 10,
		SCALE_X: 11,
		SCALE_Y: 12,
		SCALE_Z: 13,

		NONE: 99
	};

	function getAction(plist) {
		if (/rotate.*fine/i.test(plist.name)) {
			return bg.manipulation.GizmoAction.ROTATE_FINE;
		}
		if (/rotate.*x/i.test(plist.name)) {
			return bg.manipulation.GizmoAction.ROTATE_X;
		}
		if (/rotate.*y/i.test(plist.name)) {
			return bg.manipulation.GizmoAction.ROTATE_Y;
		}
		if (/rotate.*z/i.test(plist.name)) {
			return bg.manipulation.GizmoAction.ROTATE_Z;
		}
		else if (/rotate/i.test(plist.name)) {
			return bg.manipulation.GizmoAction.ROTATE;
		}
		else if (/translate.*x/i.test(plist.name)) {
			return bg.manipulation.GizmoAction.TRANSLATE_X;
		}
		else if (/translate.*y/i.test(plist.name)) {
			return bg.manipulation.GizmoAction.TRANSLATE_Y;
		}
		else if (/translate.*z/i.test(plist.name)) {
			return bg.manipulation.GizmoAction.TRANSLATE_Z;		
		}
		else if (/translate/i.test(plist.name)) {
			return bg.manipulation.GizmoAction.TRANSLATE;
		}
		else if (/scale.*x/i.test(plist.name)) {
			return bg.manipulation.GizmoAction.SCALE_X;
		}
		else if (/scale.*y/i.test(plist.name)) {
			return bg.manipulation.GizmoAction.SCALE_Y;
		}
		else if (/scale.*z/i.test(plist.name)) {
			return bg.manipulation.GizmoAction.SCALE_Z;
		}
		else if (/scale/i.test(plist.name)) {
			return bg.manipulation.GizmoAction.SCALE;
		}
	}
	
	let s_gizmoCache = {}

	class GizmoCache {
		static Get(context) {
			if (!s_gizmoCache[context.uuid]) {
				s_gizmoCache[context.uuid] = new GizmoCache(context);
			}
			return s_gizmoCache[context.uuid];
 		}
		
		constructor(context) {
			this._context = context;
			this._gizmos = {};
		}
		
		find(url) {
			return this._gizmos[url];
		}
		
		register(url,gizmoItems) {
			this._gizmos[url] = gizmoItems;
		}
		
		unregister(url) {
			if (this._gizmos[url]) {
				delete this._gizmos[url];
			}
		}
		
		clear() {
			this._gizmos = {}
		}
	}
	
	bg.manipulation.GizmoCache = GizmoCache;

	function loadGizmo(context,gizmoUrl,gizmoNode) {
		return new Promise(function(accept,reject) {
			if (!gizmoUrl) {
				accept([]);
				return;
			}
			bg.base.Loader.Load(context,gizmoUrl)
				.then(function (node) {
					let drw = node.component("bg.scene.Drawable");
					let gizmoItems = [];
					if (drw) {
						drw.forEach(function (plist,material) {
							gizmoItems.push({
								id:bg.manipulation.Selectable.GetIdentifier(),
								type:bg.manipulation.SelectableType.GIZMO,
								plist:plist,
								material:material,
								action:getAction(plist),
								node:gizmoNode
							})
						});
					}

					accept(gizmoItems);
				})
				
				.catch(function(err) {
					reject(err);
				});
		});
	}
	
	function rotationBetweenPoints(axis,p1,p2,origin,inc) {
		if (!inc) inc = 0;
		let v1 = new bg.Vector3(p2);
		v1.sub(origin).normalize();
		let v2 = new bg.Vector3(p1);
		v2.sub(origin).normalize();
		let dot = v1.dot(v2);
	
		let alpha = Math.acos(dot);
		if (alpha>=inc || inc==0) {
			if (inc!=0) {
				alpha = (alpha>=2*inc) ? 2*inc:inc;
			}
			let sign = axis.dot(v1.cross(v2));
			if (sign<0) alpha *= -1.0;
			let q = new bg.Quaternion(alpha,axis.x,axis.y,axis.z);
			q.normalize();
	
			if (!isNaN(q.x)) {
				return q;
			}
		}
		return new bg.Quaternion(0,0,1,0);
	}
	
	class Gizmo extends bg.scene.Component {
		constructor(gizmoPath,visible=true) {
			super();
			this._gizmoPath = gizmoPath;
			this._offset = new bg.Vector3(0);
			this._visible = visible;
			this._gizmoTransform = bg.Matrix4.Identity();
			this._gizmoP = bg.Matrix4.Identity();
			this._scale = 5;
			this._minSize = 0.5;
		}
		
		clone() {
			let newGizmo = new Gizmo(this._gizmoPath);
			newGizmo.offset.assign(this._offset);
			newGizmo.visible = this._visible;
			return newGizmo;
		}
		
		get offset() { return this._offset; }
		set offset(v) { this._offset = v; }
		
		get visible() { return this._visible; }
		set visible(v) { this._visible = v; }

		get gizmoTransform() {
			return this._gizmoTransform;
		}
		
		beginDrag(action,pos) {}
		
		drag(action,startPos,endPos,camera) {}
		
		endDrag(action) {}
		
		findId(id) {
			let result = null;
			if (this._gizmoItems) {
				this._gizmoItems.some((item) => {
					if (item.id.r==id.r && item.id.g==id.g && item.id.b==id.b && item.id.a==id.a) {
						result = item;
						return true;
					}
				});
			}
			return result;
		}

		init() {
			if (!this._error) {
				this._gizmoItems = [];
				loadGizmo(this.node.context,this._gizmoPath,this.node)
					.then((gizmoItems) => {
						this._gizmoItems = gizmoItems;
					})
					
					.catch((err) => {
						this._error = true;
						throw err;
					})
			}
		}
		
		frame(delta) {
			
		}
		
		display(pipeline,matrixState) {
			if (!this._gizmoItems || !this.visible) return;
			matrixState.modelMatrixStack.push();
			let modelview = new bg.Matrix4(matrixState.viewMatrixStack.matrix);
			modelview.mult(matrixState.modelMatrixStack.matrix);
			let s = modelview.position.magnitude() / this._scale;
			matrixState.modelMatrixStack.matrix.setScale(s,s,s);
			if (pipeline.effect instanceof bg.manipulation.ColorPickEffect &&
				(pipeline.opacityLayer & bg.base.OpacityLayer.GIZMOS ||
				pipeline.opacityLayer & bg.base.OpacityLayer.GIZMOS_SELECTION))
			{
				let dt = pipeline.depthTest;
				if (pipeline.opacityLayer & bg.base.OpacityLayer.GIZMOS_SELECTION) {	// drawing gizmos in selection mode
					pipeline.depthTest = true;
				}
				else {
					pipeline.depthTest = false;
				}
				this._gizmoItems.forEach((item) => {
					// The RGBA values are inverted because the alpha channel must be major than zero to
					// produce any output in the framebuffer
					if (item.plist.visible) {
						pipeline.effect.pickId = new bg.Color(item.id.a/255,item.id.b/255,item.id.g/255,item.id.r/255);
						pipeline.draw(item.plist);
					}
				});
				pipeline.depthTest = dt;
			}
			else if (pipeline.effect instanceof bg.manipulation.GizmoEffect) {
				// Draw gizmo
				this._gizmoItems.forEach((item) => {
					if (item.plist.visible) {
						pipeline.effect.texture = item.material.texture;
						pipeline.effect.color = item.material.diffuse;
						pipeline.draw(item.plist);
					}
				})
			}
			matrixState.modelMatrixStack.pop();
		}
	}

	function translateMatrix(gizmo,intersection) {
		let matrix = new bg.Matrix4(gizmo.transform.matrix);
		let rotation = matrix.rotation;
		let origin = matrix.position;
		
		if (!gizmo._lastPickPoint) {
			gizmo._lastPickPoint = intersection.ray.end;
			gizmo._translateOffset = new bg.Vector3(origin);
			gizmo._translateOffset.sub(intersection.ray.end);
		}

		switch (Math.abs(gizmo.plane)) {
		case bg.Axis.X:
			matrix = bg.Matrix4.Translation(origin.x,
											intersection.point.y + gizmo._translateOffset.y,
											intersection.point.z + gizmo._translateOffset.z);
			break;
		case bg.Axis.Y:
			matrix = bg.Matrix4.Translation(intersection.point.x + gizmo._translateOffset.x,
											origin.y,
											intersection.point.z + gizmo._translateOffset.z);
			break;
		case bg.Axis.Z:
			matrix = bg.Matrix4.Translation(intersection.point.x + gizmo._translateOffset.x,
											intersection.point.y + gizmo._translateOffset.y,
											origin.z);
			break;
		}

		matrix.mult(rotation);
		gizmo._lastPickPoint = intersection.point;

		return matrix;
	}

	function rotateMatrix(gizmo,intersection,fine) {
		let matrix = new bg.Matrix4(gizmo.transform.matrix);
		let rotation = matrix.rotation;
		let origin = matrix.position;
		
		if (!gizmo._lastPickPoint) {
			gizmo._lastPickPoint = intersection.ray.end;
			gizmo._translateOffset = new bg.Vector3(origin);
			gizmo._translateOffset.sub(intersection.ray.end);
		}

		if (!fine) {
			let prevRotation = new bg.Matrix4(rotation);
			rotation = rotationBetweenPoints(gizmo.planeAxis,gizmo._lastPickPoint,intersection.point,origin,bg.Math.degreesToRadians(22.5));
			if (rotation.x!=0 || rotation.y!=0 || rotation.z!=0 || rotation.w!=1) {
				matrix = bg.Matrix4.Translation(origin)
					.mult(rotation.getMatrix4())
					.mult(prevRotation);
				gizmo._lastPickPoint = intersection.point;
			}
		}
		else {
			let prevRotation = new bg.Matrix4(rotation);
			rotation = rotationBetweenPoints(gizmo.planeAxis,gizmo._lastPickPoint,intersection.point,origin);
			if (rotation.x!=0 || rotation.y!=0 || rotation.z!=0 || rotation.w!=1) {
				matrix = bg.Matrix4.Translation(origin)
					.mult(rotation.getMatrix4())
					.mult(prevRotation);
				gizmo._lastPickPoint = intersection.point;
			}
		}

		return matrix;
	}

	function calculateClosestPlane(gizmo,matrixState) {
		let cameraForward = matrixState.viewMatrixStack.matrix.forwardVector;
		let upVector = matrixState.viewMatrixStack.matrix.upVector;
		let xVector = new bg.Vector3(1,0,0);
		let yVector = new bg.Vector3(0,1,0);
		let zVector = new bg.Vector3(0,0,1);
		let xVectorInv = new bg.Vector3(-1,0,0);
		let yVectorInv = new bg.Vector3(0,-1,0);
		let zVectorInv = new bg.Vector3(0,0,-1);
		
		let upAlpha = Math.acos(upVector.dot(yVector));
		if (upAlpha>0.9) {
			gizmo.plane = bg.Axis.Y;
		}
		else {
			let angles = [
				Math.acos(cameraForward.dot(xVector)),		// x
				Math.acos(cameraForward.dot(yVector)),		// y
				Math.acos(cameraForward.dot(zVector)),		// z
				Math.acos(cameraForward.dot(xVectorInv)),	// -x
				Math.acos(cameraForward.dot(yVectorInv)),	// -y
				Math.acos(cameraForward.dot(zVectorInv))	// -z
			];
			let min = angles[0];
			let planeIndex = 0;
			angles.reduce(function(prev,v,index) {
				if (v<min) {
					planeIndex = index;
					min = v;
				}
			});
			switch (planeIndex) {
			case 0:
				gizmo.plane = -bg.Axis.X;
				break;
			case 1:
				gizmo.plane = bg.Axis.Y;
				break;
			case 2:
				gizmo.plane = bg.Axis.Z;
				break;
			case 3:
				gizmo.plane = bg.Axis.X;
				break;
			case 4:
				gizmo.plane = -bg.Axis.Y;
				break;
			case 5:
				gizmo.plane = -bg.Axis.Z;
				break;
			}
		}
	}
	
	class PlaneGizmo extends Gizmo {
		constructor(path,visible=true) {
			super(path,visible);
			this._plane = bg.Axis.Y;
			this._autoPlaneMode = true;
		}
		
		get plane() {
			return this._plane;
		}

		set plane(a) {
			this._plane = a;
		}

		get autoPlaneMode() {
			return this._autoPlaneMode;
		}

		set autoPlaneMode(m) {
			this._autoPlaneMode = m;
		}

		get planeAxis() {
			switch (Math.abs(this.plane)) {
			case bg.Axis.X:
				return new bg.Vector3(1,0,0);
			case bg.Axis.Y:
				return new bg.Vector3(0,1,0);
			case bg.Axis.Z:
				return new bg.Vector3(0,0,1);
			}
		}

		get gizmoTransform() {
			let result = bg.Matrix4.Identity();
			switch (this.plane) {
			case bg.Axis.X:
				return bg.Matrix4.Rotation(bg.Math.degreesToRadians(90),0,0,-1)
			case bg.Axis.Y:
				break;
			case bg.Axis.Z:
				return bg.Matrix4.Rotation(bg.Math.degreesToRadians(90),1,0,0);
			case -bg.Axis.X:
				return bg.Matrix4.Rotation(bg.Math.degreesToRadians(90),0,0,1)
			case -bg.Axis.Y:
				return bg.Matrix4.Rotation(bg.Math.degreesToRadians(180),0,-1,0);
			case -bg.Axis.Z:
				return bg.Matrix4.Rotation(bg.Math.degreesToRadians(90),-1,0,0);
			}
			return result;
		}

		clone() {
			let newGizmo = new PlaneGizmo(this._gizmoPath);
			newGizmo.offset.assign(this._offset);
			newGizmo.visible = this._visible;
			return newGizmo;
		}

		init() {
			super.init();
			this._gizmoP = bg.Matrix4.Translation(this.transform.matrix.position);
		}
		
		display(pipeline,matrixState) {
			if (!this._gizmoItems || !this.visible) return;
			if (this.autoPlaneMode) {
				calculateClosestPlane(this,matrixState);
			}
			if (!this._gizmoItems || !this.visible) return;
			let modelview = new bg.Matrix4(matrixState.viewMatrixStack.matrix);
			modelview.mult(matrixState.modelMatrixStack.matrix);
			let s = modelview.position.magnitude() / this._scale;
			s = s<this._minSize ? this._minSize : s;
			let gizmoTransform = this.gizmoTransform;
			gizmoTransform.setScale(s,s,s);
			matrixState.modelMatrixStack.push();
			matrixState.modelMatrixStack
				.mult(gizmoTransform);
			if (pipeline.effect instanceof bg.manipulation.ColorPickEffect &&
				(pipeline.opacityLayer & bg.base.OpacityLayer.GIZMOS ||
				pipeline.opacityLayer & bg.base.OpacityLayer.GIZMOS_SELECTION))
			{
				let dt = pipeline.depthTest;
				if (pipeline.opacityLayer & bg.base.OpacityLayer.GIZMOS_SELECTION) {	// drawing gizmos in selection mode
					pipeline.depthTest = true;
				}
				else {
					pipeline.depthTest = false;
				}
				this._gizmoItems.forEach((item) => {
					// The RGBA values are inverted because the alpha channel must be major than zero to
					// produce any output in the framebuffer
					if (item.plist.visible) {
						pipeline.effect.pickId = new bg.Color(item.id.a/255,item.id.b/255,item.id.g/255,item.id.r/255);
						pipeline.draw(item.plist);
					}
				});
				pipeline.depthTest = dt;
			}
			else if (pipeline.effect instanceof bg.manipulation.GizmoEffect) {
				// Draw gizmo
				this._gizmoItems.forEach((item) => {
					if (item.plist.visible) {
						pipeline.effect.texture = item.material.texture;
						pipeline.effect.color = item.material.diffuse;
						pipeline.draw(item.plist);
					}
				})
			}
			matrixState.modelMatrixStack.pop();
		}

		beginDrag(action,pos) {
			this._lastPickPoint = null;
		}
		
		drag(action,startPos,endPos,camera) {
			if (this.transform) {
				let plane = new bg.physics.Plane(this.planeAxis);
				let ray = bg.physics.Ray.RayWithScreenPoint(endPos,camera.projection,camera.viewMatrix,camera.viewport);
				let intersection = bg.physics.Intersection.RayToPlane(ray,plane);
				
				if (intersection.intersects()) {
					let matrix = new bg.Matrix4(this.transform.matrix);
					this._gizmoP = bg.Matrix4.Translation(this.transform.matrix.position);
					
					switch (action) {
					case bg.manipulation.GizmoAction.TRANSLATE:
						matrix = translateMatrix(this,intersection);
						break;
					case bg.manipulation.GizmoAction.ROTATE:
						matrix = rotateMatrix(this,intersection,false);
						break;
					case bg.manipulation.GizmoAction.ROTATE_FINE:
						matrix = rotateMatrix(this,intersection,true);
						break;
					}
					
					this.transform.matrix = matrix;
				}
			}
		}
		
		endDrag(action) {
			this._lastPickPoint = null;
		}
	}

	class UnifiedGizmo extends Gizmo {
		constructor(path,visible=true) {
			super(path,visible);
			this._translateSpeed = 0.005;
			this._rotateSpeed = 0.005;
			this._scaleSpeed = 0.001;
			this._gizmoTransform = bg.Matrix4.Identity();
		}

		get gizmoTransform() {
			return this._gizmoTransform;
		}

		get translateSpeed() { return this._translateSpeed; }
		set translateSpeed(s) { this._translateSpeed = s; }

		get rotateSpeed() { return this._rotateSpeed; }
		set rotateSpeed(s) { this._rotateSpeed = s; }

		get scaleSpeed() { return this._scaleSpeed; }
		set scaleSpeed(s) { this._scaleSpeed = s; }

		clone() {
			let newGizmo = new PlaneGizmo(this._gizmoPath);
			newGizmo.offset.assign(this._offset);
			newGizmo.visible = this._visible;
			return newGizmo;
		}

		init() {
			super.init();
			this._gizmoP = bg.Matrix4.Translation(this.transform.matrix.position);
			this._gizmoTransform = this.transform.matrix.rotation;
		}
		
		display(pipeline,matrixState) {
			if (!this._gizmoItems || !this.visible) return;
			super.display(pipeline,matrixState);
		}

		beginDrag(action,pos) {
			this._lastPickPoint = null;
		}
		
		drag(action,startPos,endPos,camera) {
			if (this.transform) {
				if (!this._lastPickPoint) {
					this._lastPickPoint = endPos;
				}

				let matrix = new bg.Matrix4(this.transform.matrix);
				this._gizmoP = bg.Matrix4.Translation(this.transform.matrix.position);
				let diff = new bg.Vector2(this._lastPickPoint);
				diff.sub(endPos);
				
				let matrixState = bg.base.MatrixState.Current();
				let modelview = new bg.Matrix4(matrixState.viewMatrixStack.matrix);
				modelview.mult(matrixState.modelMatrixStack.matrix);
				let s = modelview.position.magnitude() / this._scale;
				s = s<this._minSize ? this._minSize : s;
				let scale = matrix.getScale();

				let scaleFactor = 1 - ((diff.x + diff.y) * this.scaleSpeed);
				switch (action) {
				case bg.manipulation.GizmoAction.SCALE:
					matrix.scale(scaleFactor,scaleFactor,scaleFactor);
					break;
				case bg.manipulation.GizmoAction.TRANSLATE_X:
					matrix.translate(-(diff.x + diff.y) * this.translateSpeed * s / scale.x, 0, 0);
					break;
				case bg.manipulation.GizmoAction.TRANSLATE_Y:
					matrix.translate(0,-(diff.x + diff.y) * this.translateSpeed * s / scale.y, 0);
					break;
				case bg.manipulation.GizmoAction.TRANSLATE_Z:
					matrix.translate(0, 0,-(diff.x + diff.y) * this.translateSpeed * s  / scale.z);
					break;
				case bg.manipulation.GizmoAction.ROTATE_X:
					matrix.rotate((diff.x + diff.y) * this.rotateSpeed, 1,0,0);
					this._gizmoP.rotate((diff.x + diff.y) * this.rotateSpeed, 1,0,0);
					break;
				case bg.manipulation.GizmoAction.ROTATE_Y:
					matrix.rotate((diff.x + diff.y) * this.rotateSpeed, 0,1,0);
					this._gizmoP.rotate((diff.x + diff.y) * this.rotateSpeed, 0,1,0);
					break;
				case bg.manipulation.GizmoAction.ROTATE_Z:
					matrix.rotate((diff.x + diff.y) * this.rotateSpeed, 0,0,1);
					this._gizmoP.rotate((diff.x + diff.y) * this.rotateSpeed, 0,0,1)
					break;
				case bg.manipulation.GizmoAction.SCALE_X:
					matrix.scale(scaleFactor,1,1);
					break;
				case bg.manipulation.GizmoAction.SCALE_Y:
					matrix.scale(1,scaleFactor,1);
					break;
				case bg.manipulation.GizmoAction.SCALE_Z:
					matrix.scale(1,1,scaleFactor);
					break;
				}

				this.transform.matrix = matrix;
				this._lastPickPoint = endPos;
			}
		}
		
		endDrag(action) {
			this._lastPickPoint = null;
		}
	}

	bg.manipulation.GizmoMode = {
        SELECT: 0,
        TRANSLATE: 1,
        ROTATE: 2,
        SCALE: 3,
        TRANSFORM: 4
	};
	
	class MultiModeGizmo extends UnifiedGizmo {
		constructor(unified,translate,rotate,scale) {
			super(unified);
			this.mode = bg.manipulation.GizmoMode.TRANSFORM;
			this._transformPath = unified;
			this._translatePath = translate;
			this._rotatePath = rotate;
			this._scalePath = scale;
			this._gizmoPath = unified;
		}

		get visible() {
			return this._mode!=bg.manipulation.GizmoMode.SELECT && this._visible;
		}
		set visible(v) { this._visible = v; } 

		get mode() { return this._mode; }
        set mode(m) {
            this._mode = m;
			this._gizmoItems = [];
			switch (m) {
			case bg.manipulation.GizmoMode.SELECT:
				this._gizmoPath = "";
				break;
			case bg.manipulation.GizmoMode.TRANSLATE:
				this._gizmoPath = this._translatePath;
				break;
			case bg.manipulation.GizmoMode.ROTATE:
				this._gizmoPath = this._rotatePath;
				break;
			case bg.manipulation.GizmoMode.SCALE:
				this._gizmoPath = this._scalePath;
				break;
			case bg.manipulation.GizmoMode.TRANSFORM:
				this._gizmoPath = this._transformPath;
				break;
			}
			if (this._gizmoPath) {
				loadGizmo(this.node.context,this._gizmoPath,this.node)
					.then((gizmoItems) => {
						this._gizmoItems = gizmoItems;
						bg.emitImageLoadEvent();
					})
					
					.catch((err) => {
						this._error = true;
						throw err;
					})
			}
        }
	}
	
	// All gizmo types share the same typeId, because a node can only contain one gizmo
	bg.scene.registerComponent(bg.manipulation,Gizmo,"bg.manipulation.Gizmo");
	bg.scene.registerComponent(bg.manipulation,PlaneGizmo,"bg.manipulation.Gizmo");
	bg.scene.registerComponent(bg.manipulation,UnifiedGizmo,"bg.manipulation.Gizmo");
	bg.scene.registerComponent(bg.manipulation,MultiModeGizmo,"bg.manipulation.Gizmo");

})();
(function() {

	let shader = {};
	
	function initShaders() {
		shader[bg.webgl1.EngineId] = {
			vertex: `
			attribute vec3 inVertex;
			
			uniform mat4 inModelMatrix;
			uniform mat4 inViewMatrix;
			uniform mat4 inProjectionMatrix;
			
			void main() {
				gl_Position = inProjectionMatrix * inViewMatrix * inModelMatrix * vec4(inVertex,1.0);
			}
			`,
			
			fragment:`
			precision highp float;
			
			uniform vec4 inColorId;
			
			void main() {
				gl_FragColor = inColorId;
			}
			`
		};
	}
	
	class ColorPickEffect extends bg.base.Effect {
		constructor(context) { 
			super(context);
			initShaders();
		}
		
		get inputVars() {
			return {
				vertex:'inVertex'
			}
		}
		
		set matrixState(m) { this._matrixState = m; }
		get matrixState() {
			return this._matrixState;
		}
		
		set pickId(p) { this._pickId = p; }
		get pickId() { return this._pickId || bg.Color.Transparent(); }
		
		get shader() {
			if (!this._shader) {
				this._shader = new bg.base.Shader(this.context);
				this._shader.addShaderSource(bg.base.ShaderType.VERTEX, shader[bg.webgl1.EngineId].vertex);
				this._shader.addShaderSource(bg.base.ShaderType.FRAGMENT, shader[bg.webgl1.EngineId].fragment);
				this._shader.link();
				if (!this._shader.status) {
					console.log(this._shader.compileError);
					console.log(this._shader.linkError);
				}
				else {
					this._shader.initVars([
						'inVertex'
					],[
						'inModelMatrix',
						'inViewMatrix',
						'inProjectionMatrix',
						'inColorId'
					]);
				}
			}
			return this._shader
		}
		
		setupVars() {
			this.shader.setMatrix4('inModelMatrix',this.matrixState.modelMatrixStack.matrixConst);
			this.shader.setMatrix4('inViewMatrix',new bg.Matrix4(this.matrixState.viewMatrixStack.matrixConst));
			this.shader.setMatrix4('inProjectionMatrix',this.matrixState.projectionMatrixStack.matrixConst);
			this.shader.setVector4('inColorId', this.pickId);
		}
		
	}
	
	bg.manipulation.ColorPickEffect = ColorPickEffect;

	class FindPickIdVisitor extends bg.scene.NodeVisitor {
		constructor(target) {
			super()
			this._target = target;
		}
		
		get target() { return this._target; }
		set target(t) { this._target = t; this._result = null; }
		
		get result() { return this._result; }
		
		visit(node) {
			let selectable = node.component("bg.manipulation.Selectable");
			let gizmo = node.component("bg.manipulation.Gizmo");
			if (gizmo && !gizmo.visible) {
				gizmo = null;
			}
			this._result = 	this._result ||
							(selectable && selectable.findId(this.target)) ||
							(gizmo && gizmo.findId(this.target));
		}
	}
	
	bg.manipulation.FindPickIdVisitor = FindPickIdVisitor;
	
	class MousePicker extends bg.app.ContextObject {
		
		constructor(context) {
			super(context);
		}
		
		get pipeline() {
			if (!this._pipeline) {
				this._pipeline = new bg.base.Pipeline(this.context);
				
				this._pipeline.effect = new ColorPickEffect(this.context);
				this._renderSurface = new bg.base.TextureSurface(this.context);
				this._renderSurface.create();
				this._pipeline.renderSurface = this._renderSurface;
				this._pipeline.clearColor = new bg.Color(0,0,0,0);
			}
			return this._pipeline;
		}
		
		get matrixState() {
			if (!this._matrixState) {
				this._matrixState = new bg.base.MatrixState();
			}
			return this._matrixState;
		}
		
		get drawVisitor() {
			if (!this._drawVisitor) {
				this._drawVisitor = new bg.scene.DrawVisitor(this.pipeline,this.matrixState);
			}
			return this._drawVisitor;
		}
		
		pick(sceneRoot,camera,mousePosition) {
			let restorePipeline = bg.base.Pipeline.Current();
			let restoreMatrixState = bg.base.MatrixState.Current();
			bg.base.Pipeline.SetCurrent(this.pipeline);
			bg.base.MatrixState.SetCurrent(this.matrixState);
			this.pipeline.viewport = camera.viewport;
			this.pipeline.effect.matrixState = this.matrixState;
			
			
			this.pipeline.clearBuffers(bg.base.ClearBuffers.COLOR | bg.base.ClearBuffers.DEPTH);
			
			this.matrixState.projectionMatrixStack.set(camera.projection);
			this.matrixState.viewMatrixStack.set(camera.viewMatrix);										
			
			let opacityLayer = this.pipeline.opacityLayer;
			this.pipeline.opacityLayer = bg.base.OpacityLayer.SELECTION;
			sceneRoot.accept(this.drawVisitor);
			this.pipeline.opacityLayer = bg.base.OpacityLayer.GIZMOS_SELECTION;
			this.pipeline.clearBuffers(bg.base.ClearBuffers.DEPTH);
			sceneRoot.accept(this.drawVisitor);
			this.pipeline.opacityLayer = opacityLayer;
			
			let buffer = this.pipeline.renderSurface.readBuffer(new bg.Viewport(mousePosition.x, mousePosition.y,1,1));
			let pickId = {
				r: buffer[3],
				g: buffer[2],
				b: buffer[1],
				a: buffer[0]
			};
			
			let findIdVisitor = new FindPickIdVisitor(pickId);
			sceneRoot.accept(findIdVisitor);
			
			if (restorePipeline) {
				bg.base.Pipeline.SetCurrent(restorePipeline);
			}
			if (restoreMatrixState) {
				bg.base.MatrixState.SetCurrent(restoreMatrixState);
			}
	
			return findIdVisitor.result;
		}
	}
	
	bg.manipulation.MousePicker = MousePicker;
	
})();

(function() {
	
	let s_r = 0;
	let s_g = 0;
	let s_b = 0;
	let s_a = 0;
	
	function incrementIdentifier() {
		if (s_r==255) {
			s_r = 0;
			incG();
		}
		else {
			++s_r;
		}
	}
	
	function incG() {
		if (s_g==255) {
			s_g = 0;
			incB();
		}
		else {
			++s_g;
		}
	}
	
	function incB() {
		if (s_b==255) {
			s_b = 0;
			incA();
		}
		else {
			++s_b;
		}
	}
	
	function incA() {
		if (s_a==255) {
			s_a = 0;
			bg.log("WARNING: Maximum number of picker identifier reached.");
		}
		else {
			++s_a;
		}
	}
	
	function getIdentifier() {
		incrementIdentifier();
		return { r:s_r, g:s_g, b:s_g, a:s_a };
	}
	
	let s_selectMode = false;
	
	bg.manipulation.SelectableType = {
		PLIST:1,
		GIZMO:2,
		GIZMO_ICON:3
	};

	let s_selectionIconPlist = null;
	function selectionIconPlist() {
		if (!s_selectionIconPlist) {
			s_selectionIconPlist = bg.scene.PrimitiveFactory.SpherePolyList(this.node.context,0.5);
		}
		return s_selectionIconPlist;
	}

	let g_selectableIcons = [
		"bg.scene.Camera",
		"bg.scene.Light",
		"bg.scene.Transform",
		"bg.scene.TextRect"
	];

	
	class Selectable extends bg.scene.Component {
		static SetSelectableIcons(sel) {
			g_selectableIcons = sel;
		}

		static AddSelectableIcon(sel) {
			if (g_selectableIcons.indexOf(sel)==-1) {
				g_selectableIcons.push(sel);
			}
		}

		static RemoveSelectableIcon(sel) {
			let index = g_selectableIcons.indexOf(sel);
			if (index>=0) {
				g_selectableIcons.splice(index,1);
			}
		}

		static SetSelectMode(m) { s_selectMode = m; }
		
		static GetIdentifier() { return getIdentifier(); }
		
		constructor() {
			super();
			this._initialized = false;
			this._selectablePlist = [];
		}
		
		clone() {
			return new Selectable();
		}
		
		buildIdentifier() {
			this._initialized = false;
			this._selectablePlist = [];
		}
		
		findId(id) {
			let result = null;
			this._selectablePlist.some((item) => {
				if (item.id.r==id.r && item.id.g==id.g && item.id.b==id.b && item.id.a==id.a) {
					result = item;
					return true;
				}
			});
			return result;
		}
		
		frame(delta) {
			if (!this._initialized && this.drawable) {
				this.drawable.forEach((plist,material) => {
					let id = getIdentifier();
					this._selectablePlist.push({
						id:id,
						type:bg.manipulation.SelectableType.PLIST,
						plist:plist,
						material:material,
						drawable:this.drawable,
						node:this.node
					});
				});
				this._initialized = true;
			}
			else if (!this._initialized) {
				// Use icon to pick item
				let id = getIdentifier();
				this._selectablePlist.push({
					id:id,
					type:bg.manipulation.SelectableType.GIZMO_ICON,
					plist:null,
					material:null,
					drawable:null,
					node:this.node
				});
				this._initialized = true;
			}
		}
		
		display(pipeline,matrixState) {
			if (pipeline.effect instanceof bg.manipulation.ColorPickEffect &&
				pipeline.opacityLayer & bg.base.OpacityLayer.SELECTION)
			{
				let selectableByIcon = g_selectableIcons.some((componentType) => {
					return this.node.component(componentType)!=null;
				});
				this._selectablePlist.forEach((item) => {
					let pickId = new bg.Color(item.id.a/255,item.id.b/255,item.id.g/255,item.id.r/255);
					if (item.plist && item.plist.visible) {
						// The RGBA values are inverted because the alpha channel must be major than zero to
						// produce any output in the framebuffer
						pipeline.effect.pickId = pickId;
						pipeline.draw(item.plist);
					}
					else if (!item.plist && selectableByIcon) {
						let s = matrixState.cameraDistanceScale * 0.1;
						pipeline.effect.pickId = pickId;
						matrixState.modelMatrixStack.push();
						matrixState.modelMatrixStack.scale(s,s,s);
						pipeline.draw(selectionIconPlist.apply(this));
						matrixState.modelMatrixStack.pop();
					}
				});
			}
		}
	}
	
	bg.scene.registerComponent(bg.manipulation,Selectable,"bg.manipulation.Selectable");
})();
(function() {

    function lib() {
        return bg.base.ShaderLibrary.Get();
    }

    class BorderDetectionEffect extends bg.base.TextureEffect {
        constructor(context) {
            super(context);

            let vertex = new bg.base.ShaderSource(bg.base.ShaderType.VERTEX);
            let fragment = new bg.base.ShaderSource(bg.base.ShaderType.FRAGMENT);
            vertex.addParameter([
                lib().inputs.buffers.vertex,
                lib().inputs.buffers.tex0,
                { name:"fsTexCoord", dataType:"vec2", role:"out" }
            ]);

            fragment.addParameter([
                lib().inputs.material.texture,
                { name:"inTexSize", dataType:"vec2", role:"value" },
                { name:"inConvMatrix", dataType:"float", role:"value", vec:9 },
                { name:"inBorderColor", dataType:"vec4", role:"value" },
                { name:"inBorderWidth", dataType:"float", role:"value" },
                { name:"fsTexCoord", dataType:"vec2", role:"in" }
            ]);

            if (bg.Engine.Get().id=="webgl1") {
                vertex.setMainBody(`
                gl_Position = vec4(inVertex,1.0);
                fsTexCoord = inTex0;
                `);
                fragment.addFunction(lib().functions.utils.applyConvolution);
                fragment.setMainBody(`
                vec4 selectionColor = applyConvolution(inTexture,fsTexCoord,inTexSize,inConvMatrix,inBorderWidth);
                if (selectionColor.r!=0.0 && selectionColor.g!=0.0 && selectionColor.b!=0.0) {
                    gl_FragColor = inBorderColor;
                }
                else {
                    discard;
                }
                `);
            }

            this.setupShaderSource([
                vertex,
                fragment
            ], false);

            this._highlightColor = bg.Color.White();
            this._borderWidth = 2;
        }

        get highlightColor() { return this._highlightColor; }
        set highlightColor(c) { this._highlightColor = c; }

        get borderWidth() { return this._borderWidth; }
        set borderWidth(w) { this._borderWidth = w; } 

        setupVars() {
            let texture = null;
            if (this._surface instanceof bg.base.Texture) {
                texture = this._surface;
            }
            else if (this._surface instanceof bg.base.RenderSurface) {
                texture = this._surface.getTexture(0);
            }

            if (texture) {
                this.shader.setTexture("inTexture",texture,bg.base.TextureUnit.TEXTURE_0);
                this.shader.setVector2("inTexSize",texture.size);
            }

            let convMatrix = [
                0, 1, 0,
                1,-4, 1,
                0, 1, 0
            ];
            this.shader.setValueFloatPtr("inConvMatrix",convMatrix);
            this.shader.setVector4("inBorderColor", this._highlightColor);
            this.shader.setValueFloat("inBorderWidth", this._borderWidth);
        }
    }

    bg.manipulation.BorderDetectionEffect = BorderDetectionEffect;

    let s_plainColorVertex = null;
    let s_plainColorFragment = null;

    function plainColorVertex() {
        if (!s_plainColorVertex) {
            s_plainColorVertex = new bg.base.ShaderSource(bg.base.ShaderType.VERTEX);

            s_plainColorVertex.addParameter(lib().inputs.buffers.vertex);
            s_plainColorVertex.addParameter(lib().inputs.matrix.all);

            if (bg.Engine.Get().id=="webgl1") {
                s_plainColorVertex.setMainBody(`
                    gl_Position = inProjectionMatrix * inViewMatrix * inModelMatrix * vec4(inVertex,1.0);
                `);
            }
        }
        return s_plainColorVertex;
    }

    function plainColorFragment() {
        if (!s_plainColorFragment) {
            s_plainColorFragment = new bg.base.ShaderSource(bg.base.ShaderType.FRAGMENT);
            s_plainColorFragment.addParameter([
                { name:"inColor", dataType:"vec4", role:"value" },
                { name:"inSelectMode", dataType:"int", role:"value" }
            ]);

            if (bg.Engine.Get().id=="webgl1") {
                s_plainColorFragment.setMainBody(`
                    if (inSelectMode==0) {
                        discard;
                    }
                    else {
                        gl_FragColor = inColor;
                    }
                `);
            }
        }
        return s_plainColorFragment;
    }

    class PlainColorEffect extends bg.base.Effect {
        constructor(context) {
            super(context);

            let sources = [
                plainColorVertex(),
                plainColorFragment()
            ];
            this.setupShaderSource(sources);
        }

        beginDraw() {
            bg.Math.seed = 1;
        }

        setupVars() {
            this._baseColor = new bg.Color(bg.Math.seededRandom(),bg.Math.seededRandom(),bg.Math.seededRandom(),1);
            let matrixState = bg.base.MatrixState.Current();
            let viewMatrix = new bg.Matrix4(matrixState.viewMatrixStack.matrixConst);
            this.shader.setMatrix4('inModelMatrix',matrixState.modelMatrixStack.matrixConst);
            this.shader.setMatrix4('inViewMatrix',viewMatrix);
            this.shader.setMatrix4('inProjectionMatrix',matrixState.projectionMatrixStack.matrixConst);
            this.shader.setVector4('inColor', this._baseColor);
            this.shader.setValueInt("inSelectMode", this.material.selectMode);
        }
    }

    bg.manipulation.PlainColorEffect = PlainColorEffect;

    function buildOffscreenPipeline() {
        let offscreenPipeline = new bg.base.Pipeline(this.context);
    
        let renderSurface = new bg.base.TextureSurface(this.context);
        offscreenPipeline.effect = new bg.manipulation.PlainColorEffect(this.context);
        let colorAttachments = [
            { type:bg.base.RenderSurfaceType.RGBA, format:bg.base.RenderSurfaceFormat.UNSIGNED_BYTE },
            { type:bg.base.RenderSurfaceType.DEPTH, format:bg.base.RenderSurfaceFormat.RENDERBUFFER }
        ];
        renderSurface.create(colorAttachments);
        offscreenPipeline.renderSurface = renderSurface;

        return offscreenPipeline;
    }

    class SelectionHighlight extends bg.app.ContextObject {

        constructor(context) {
            super(context);

            this._offscreenPipeline = buildOffscreenPipeline.apply(this);

            this._pipeline = new bg.base.Pipeline(this.context);
            this._pipeline.textureEffect = new bg.manipulation.BorderDetectionEffect(this.context);
            
            this._matrixState = new bg.base.MatrixState();

            this._drawVisitor = new bg.scene.DrawVisitor(this._offscreenPipeline,this._matrixState);
            this._drawVisitor.forceDraw = false;
        }

        get highlightColor() { return this._pipeline.textureEffect.highlightColor; }
        set highlightColor(c) { this._pipeline.textureEffect.highlightColor = c; }

        get borderWidth() { return this._pipeline.textureEffect.borderWidth; }
        set borderWidth(w) { this._pipeline.textureEffect.borderWidth = w; }

        get drawInvisiblePolyList() { return this._drawVisitor.forceDraw; }
        set drawInvisiblePolyList(d) { this._drawVisitor.forceDraw = d; }

        drawSelection(sceneRoot,camera) {
            let restorePipeline = bg.base.Pipeline.Current();
            let restoreMatrixState = bg.base.MatrixState.Current();
            this._offscreenPipeline.viewport = camera.viewport;
            this._pipeline.viewport = camera.viewport;

            bg.base.Pipeline.SetCurrent(this._offscreenPipeline);
            bg.base.MatrixState.SetCurrent(this._matrixState);
            this._offscreenPipeline.clearBuffers(bg.base.ClearBuffers.COLOR | bg.base.ClearBuffers.DEPTH);
            this._matrixState.projectionMatrixStack.set(camera.projection);
            this._matrixState.viewMatrixStack.set(camera.viewMatrix);
            this._matrixState.modelMatrixStack.identity();
            sceneRoot.accept(this._drawVisitor);
            
            let texture = this._offscreenPipeline.renderSurface.getTexture(0);
            bg.base.Pipeline.SetCurrent(this._pipeline);
            this._pipeline.blend = true;
            this._pipeline.blendMode = bg.base.BlendMode.ADD;
            this._pipeline.drawTexture(texture);

            if (restorePipeline) {
                bg.base.Pipeline.SetCurrent(restorePipeline);
            }
            if (restoreMatrixState) {
                bg.base.MatrixState.SetCurrent(restoreMatrixState);
            }
        }
    }

    bg.manipulation.SelectionHighlight = SelectionHighlight;

})();
(function() {
	
	let Action = {
		ROTATE:0,
		PAN:1,
		ZOOM:2
	};
	
	function getOrbitAction(cameraCtrl) {
		let left = bg.app.Mouse.LeftButton(),
			middle = bg.app.Mouse.MiddleButton(),
			right = bg.app.Mouse.RightButton();
				
		switch (true) {
			case left==cameraCtrl._rotateButtons.left &&
				 middle==cameraCtrl._rotateButtons.middle &&
				 right==cameraCtrl._rotateButtons.right:
				 return Action.ROTATE;
			case left==cameraCtrl._panButtons.left &&
				 middle==cameraCtrl._panButtons.middle &&
				 right==cameraCtrl._panButtons.right:
				 return Action.PAN;
			case left==cameraCtrl._zoomButtons.left &&
				 middle==cameraCtrl._zoomButtons.middle &&
				 right==cameraCtrl._zoomButtons.right:
				 return Action.ZOOM;
		}
	}

	function buildPlist(context,vertex,color) {
		let plist = new bg.base.PolyList(context);
		let normal = [];
		let texCoord0 = [];
		let index = [];
		let currentIndex = 0;
		for (let i=0; i<vertex.length; i+=3) {
			normal.push(0); normal.push(0); normal.push(1);
			texCoord0.push(0); texCoord0.push(0);
			index.push(currentIndex++);
		}
		plist.vertex = vertex;
		plist.normal = normal;
		plist.texCoord0 = texCoord0;
		plist.color = color;
		plist.index = index;
		plist.drawMode = bg.base.DrawMode.LINES;
		plist.build();
		return plist;
	}

	function getGizmo() {
		let x = this.minX;
		let X = this.maxX;
		let y = this.minY;
		let Y = this.maxY;
		let z = this.minZ;
		let Z = this.maxZ;
		let vertex = [
			x,y,z, X,y,z, X,y,z, X,Y,z, X,Y,z, x,Y,z, x,Y,z, x,y,z,	// back
			x,y,Z, X,y,Z, X,y,Z, X,Y,Z, X,Y,Z, x,Y,Z, x,Y,Z, x,y,Z,	// front
			x,y,z, x,y,Z,	// edge 1
			X,y,z, X,y,Z,	// edge 2
			X,Y,z, X,Y,Z,	// edge 3
			x,Y,z, x,Y,Z	// edge 4
		];
		let color = [];
		for (let i = 0; i<vertex.length; i+=3) {
			color.push(this._limitGizmoColor.r);
			color.push(this._limitGizmoColor.g);
			color.push(this._limitGizmoColor.b);
			color.push(this._limitGizmoColor.a);
		}
		if (!this._plist) {
			this._plist = buildPlist(this.node.context,vertex,color);
		}
		else {
			this._plist.updateBuffer(bg.base.BufferType.VERTEX,vertex);
			this._plist.updateBuffer(bg.base.BufferType.COLOR,color);
		}
		return this._plist;
	}
	
	class OrbitCameraController extends bg.scene.Component {
		static DisableAll(sceneRoot) {
			let ctrl = sceneRoot.component("bg.manipulation.OrbitCameraController");
			if (ctrl) {
				ctrl.enabled = false;
			}
			sceneRoot.children.forEach((ch) => OrbitCameraController.DisableAll(ch));
		}

		static SetUniqueEnabled(orbitCameraController,sceneRoot) {
			OrbitCameraController.DisableAll(sceneRoot);
			orbitCameraController.enabled = true;
		}

		constructor() {
			super();
			
			this._rotateButtons = { left:true, middle:false, right:false };
			this._panButtons = { left:false, middle:false, right:true };
			this._zoomButtons = { left:false, middle:true, right:false };
			
			this._rotation = new bg.Vector2();
			this._distance = 5;
			this._center = new bg.Vector3();
			this._rotationSpeed = 0.2;
			this._forward = 0;
			this._left = 0;
			this._wheelSpeed = 1;
			this._minFocus = 2;

			this._minPitch = 0.1;
			this._maxPitch = 85.0;
			this._minDistance = 0.4;
			this._maxDistance = 24.0;
			
			this._maxX = 15;
			this._minX = -15;
			this._minY = 0.1;
			this._maxY = 2.0;
			this._maxZ = 15;
			this._minZ = -15;

			this._displacementSpeed = 0.1;

			this._enabled = true;

			// Do not serialize/deserialize this:
			this._keys = {};
			this._showLimitGizmo = true;
			this._limitGizmoColor = bg.Color.Green();

			// This is used for orthographic projections
			this._viewWidth = 50;

			this._lastTouch = [];
		}

		clone() {
			let result = new OrbitCameraController();
			let compData = {};
			this.serialize(compData,[],"");
			result.deserialize(null,compData,"");
			return result;
		}
		
		setRotateButtons(left,middle,right) {
			this._rotateButtons = { left:left, middle:middle, right:right };
		}
		
		setPanButtons(left,middle,right) {
			this._panButtons = { left:left, middle:middle, right:right };
		}
		
		setZoomButtons(left,middle,right) {
			this._zoomButtons = { left:left, middle:middle, right:right };
		}
		
		get rotation() { return this._rotation; }
		set rotation(r) { this._rotation = r; }
		get distance() { return this._distance; }
		set distance(d) { this._distance = d; }
		get center() { return this._center; }
		set center(c) { this._center = c; }
		get whellSpeed() { this._wheelSpeed; }
		set wheelSpeed(w) { this._wheelSpeed = w; }

		get viewWidth() { return this._viewWidth; }
		
		get minCameraFocus() { return this._minFocus; }
		set minCameraFocus(f) { this._minFocus = f; }
		get minPitch() { return this._minPitch; }
		set minPitch(p) { this._minPitch = p; }
		get maxPitch() { return this._maxPitch; }
		set maxPitch(p) { this._maxPitch = p; }
		get minDistance() { return this._minDistance; }
		set minDistance(d) { this._minDistance = d; }
		get maxDistance() { return this._maxDistance; }
		set maxDistance(d) { this._maxDistance = d; }

		get minX() { return this._minX; }
		get maxX() { return this._maxX; }
		get minY() { return this._minY; }
		get maxY() { return this._maxY; }
		get minZ() { return this._minZ; }
		get maxZ() { return this._maxZ; }

		set minX(val) { this._minX = val; }
		set maxX(val) { this._maxX = val; }
		set minY(val) { this._minY = val; }
		set maxY(val) { this._maxY = val; }
		set minZ(val) { this._minZ = val; }
		set maxZ(val) { this._maxZ = val; }

		get displacementSpeed() { return this._displacementSpeed; }
		set displacementSpeed(s) { this._displacementSpeed = s; }

		get enabled() { return this._enabled; }
		set enabled(e) { this._enabled = e; }

		get showLimitGizmo() { return this._showLimitGizmo; }
		set showLimitGizmo(l) { this._showLimitGizmo = l; }
		get limitGizmoColor() { return this._limitGizmoColor; }
		set limitGizmoColor(c) { this._limitGizmoColor = c; }

		displayGizmo(pipeline,matrixState) {
			if (!this._showLimitGizmo) return;
			let plist = getGizmo.apply(this);
			matrixState.modelMatrixStack.push();
			matrixState.modelMatrixStack.identity();
			if (plist) {
				pipeline.draw(plist);
			}
			matrixState.modelMatrixStack.pop();
		}

		serialize(componentData,promises,url) {
			super.serialize(componentData,promises,url);
			componentData.rotateButtons = this._rotateButtons;
			componentData.panButtons = this._panButtons;
			componentData.zoomButtons = this._zoomButtons;
			componentData.rotation = this._rotation.toArray();
			componentData.distance = this._distance;
			componentData.center = this._center.toArray();
			componentData.rotationSpeed = this._rotationSpeed;
			componentData.forward = this._forward;
			componentData.left = this._left;
			componentData.wheelSpeed = this._wheelSpeed;
			componentData.minFocus = this._minFocus;
			componentData.minPitch = this._minPitch;
			componentData.maxPitch = this._maxPitch;
			componentData.minDistance = this._minDistance;
			componentData.maxDistance = this._maxDistance;
			componentData.maxX = this._maxX;
			componentData.minX = this._minX;
			componentData.minY = this._minY;
			componentData.maxY = this._maxY;
			componentData.maxZ = this._maxZ;
			componentData.minZ = this._minZ;
			componentData.displacementSpeed = this._displacementSpeed;
			componentData.enabled = this._enabled;
		}

		deserialize(context,componentData,url) {
			this._rotateButtons = componentData.rotateButtons || this._rotateButtons;
			this._panButtons = componentData.panButtons || this._panButtons;
			this._zoomButtons = componentData.zoomButtons || this._zoomButtons;
			this._rotation = new bg.Vector2(componentData.rotation) || this._rotation;
			this._distance = componentData.distance!==undefined ? componentData.distance : this._distance;
			this._center = new bg.Vector3(componentData.center) || this._center;
			this._rotationSpeed = componentData.rotationSpeed!==undefined ? componentData.rotationSpeed : this._rotationSpeed;
			this._forward = componentData.forward!==undefined ? componentData.forward : this._forward;
			this._left = componentData.left!==undefined ? componentData.left : this._left;
			this._wheelSpeed = componentData.wheelSpeed!==undefined ? componentData.wheelSpeed : this._wheelSpeed;
			this._minFocus = componentData.minFocus!==undefined ? componentData.minFocus : this._minFocus;
			this._minPitch = componentData.minPitch!==undefined ? componentData.minPitch : this._minPitch;
			this._maxPitch = componentData.maxPitch!==undefined ? componentData.maxPitch : this._maxPitch;
			this._minDistance = componentData.minDistance!==undefined ? componentData.minDistance : this._minDistance;
			this._maxDistance = componentData.maxDistance!==undefined ? componentData.maxDistance : this._maxDistance;
			this._maxX = componentData.maxX!==undefined ? componentData.maxX : this._maxX;
			this._minX = componentData.minX!==undefined ? componentData.minX : this._minX;
			this._minY = componentData.minY!==undefined ? componentData.minY : this._minY;
			this._maxY = componentData.maxY!==undefined ? componentData.maxY : this._maxY;
			this._maxZ = componentData.maxZ!==undefined ? componentData.maxZ : this._maxZ;
			this._minZ = componentData.minZ!==undefined ? componentData.minZ : this._minZ;
			this._displacementSpeed = componentData.displacementSpeed!==undefined ? componentData.displacementSpeed : this._displacementSpeed;
			this._enabled = componentData.enabled!==undefined ? componentData.enabled : this._enabled;
		}

		frame(delta) {
			let orthoStrategy = this.camera && typeof(this.camera.projectionStrategy)=="object" &&
								this.camera.projectionStrategy instanceof bg.scene.OrthographicProjectionStrategy ?
									this.camera.projectionStrategy : null;
			

			if (this.transform && this.enabled) {
				let forward = this.transform.matrix.forwardVector;
				let left = this.transform.matrix.leftVector;
				forward.scale(this._forward);
				left.scale(this._left);
				this._center.add(forward).add(left);
				
				let pitch = this._rotation.x>this._minPitch ? this._rotation.x:this._minPitch;
				pitch = pitch<this._maxPitch ? pitch : this._maxPitch;
				this._rotation.x = pitch;

				this._distance = this._distance>this._minDistance ? this._distance:this._minDistance;
				this._distance = this._distance<this._maxDistance ? this._distance:this._maxDistance;

				if (this._mouseButtonPressed) {
					let displacement = new bg.Vector3();
					if (this._keys[bg.app.SpecialKey.UP_ARROW]) {
						displacement.add(this.transform.matrix.backwardVector);
						bg.app.MainLoop.singleton.windowController.postRedisplay();
					}
					if (this._keys[bg.app.SpecialKey.DOWN_ARROW]) {
						displacement.add(this.transform.matrix.forwardVector);
						bg.app.MainLoop.singleton.windowController.postRedisplay();
					}
					if (this._keys[bg.app.SpecialKey.LEFT_ARROW]) {
						displacement.add(this.transform.matrix.leftVector);
						bg.app.MainLoop.singleton.windowController.postRedisplay();
					}
					if (this._keys[bg.app.SpecialKey.RIGHT_ARROW]) {
						displacement.add(this.transform.matrix.rightVector);
						bg.app.MainLoop.singleton.windowController.postRedisplay();
					}
					displacement.scale(this._displacementSpeed);
					this._center.add(displacement);
				}

				if (this._center.x<this._minX) this._center.x = this._minX;
				else if (this._center.x>this._maxX) this._center.x = this._maxX;

				if (this._center.y<this._minY) this._center.y = this._minY;
				else if (this._center.y>this._maxY) this._center.y = this._maxY;

				if (this._center.z<this._minZ) this._center.z = this._minZ;
				else if (this._center.z>this._maxZ) this._center.z = this._maxZ;

				this.transform.matrix
						.identity()
						.translate(this._center)
						.rotate(bg.Math.degreesToRadians(this._rotation.y), 0,1,0)
						.rotate(bg.Math.degreesToRadians(pitch), -1,0,0);

				if (orthoStrategy) {
					orthoStrategy.viewWidth = this._viewWidth;
				}
				else {
					this.transform.matrix.translate(0,0,this._distance);
				}
			}
			
			if (this.camera) {
				this.camera.focus = this._distance>this._minFocus ? this._distance:this._minFocus;
				
			}
		}

		mouseDown(evt) {
			if (!this.enabled) return;
			this._mouseButtonPressed = true;
			this._lastPos = new bg.Vector2(evt.x,evt.y);
		}

		mouseUp(evt) {
			this._mouseButtonPressed = false;
		}
		
		mouseDrag(evt) {
			if (this.transform && this._lastPos && this.enabled) {
				let delta = new bg.Vector2(this._lastPos.y - evt.y,
										 this._lastPos.x - evt.x);
				this._lastPos.set(evt.x,evt.y);
				let orthoStrategy = this.camera && typeof(this.camera.projectionStrategy)=="object" &&
								this.camera.projectionStrategy instanceof bg.scene.OrthographicProjectionStrategy ?
								true : false;

				switch (getOrbitAction(this)) {
					case Action.ROTATE:
						delta.x = delta.x * -1;
						this._rotation.add(delta.scale(0.5));
						break;
					case Action.PAN:
						let up = this.transform.matrix.upVector;
						let left = this.transform.matrix.leftVector;
						
						if (orthoStrategy) {
							up.scale(delta.x * -0.0005 * this._viewWidth);
							left.scale(delta.y * -0.0005 * this._viewWidth);
						}
						else {
							up.scale(delta.x * -0.001 * this._distance);
							left.scale(delta.y * -0.001 * this._distance);
						}
						this._center.add(up).add(left);
						break;
					case Action.ZOOM:
						this._distance += delta.x * 0.01 * this._distance;
						this._viewWidth += delta.x * 0.01 * this._viewWidth;
						if (this._viewWidth<0.5) this._viewWidth = 0.5;
						break;
				}				
			}
		}

		mouseWheel(evt) {
			if (!this.enabled) return;
			let mult = this._distance>0.01 ? this._distance:0.01;
			let wMult = this._viewWidth>1 ? this._viewWidth:1;
			this._distance += evt.delta * 0.001 * mult * this._wheelSpeed;
			this._viewWidth += evt.delta * 0.0001 * wMult * this._wheelSpeed;
			if (this._viewWidth<0.5) this._viewWidth = 0.5;
		}
		
		touchStart(evt) {
			if (!this.enabled) return;
			this._lastTouch = evt.touches;
		}
		
		touchMove(evt) {
			if (this._lastTouch.length==evt.touches.length && this.transform && this.enabled) {
				if (this._lastTouch.length==1) {
					// Rotate
					let last = this._lastTouch[0];
					let t = evt.touches[0];
					let delta = new bg.Vector2((last.y - t.y)  * -1.0, last.x - t.x);
					
					this._rotation.add(delta.scale(0.5));
				}
				else if (this._lastTouch.length==2) {
					// Pan/zoom
					let l0 = this._lastTouch[0];
					let l1 = this._lastTouch[1];
					let t0 = null;
					let t1 = null;
					evt.touches.forEach((touch) => {
						if (touch.identifier==l0.identifier) {
							t0 = touch;
						}
						else if (touch.identifier==l1.identifier) {
							t1 = touch;
						}
					});
					let dist0 = Math.round((new bg.Vector2(l0.x,l0.y)).sub(new bg.Vector3(l1.x,l1.y)).magnitude());
					let dist1 = Math.round((new bg.Vector2(t0.x,t0.y)).sub(new bg.Vector3(t1.x,t1.y)).magnitude());
					let delta = new bg.Vector2(l0.y - t0.y, l1.x - t1.x);
					let up = this.transform.matrix.upVector;
					let left = this.transform.matrix.leftVector;
					
					up.scale(delta.x * -0.001 * this._distance);
					left.scale(delta.y * -0.001 * this._distance);
					this._center.add(up).add(left);
						
					this._distance += (dist0 - dist1) * 0.005 * this._distance;
				}
			}
			this._lastTouch = evt.touches;
		}

		keyDown(evt) {
			if (!this.enabled) return;
			this._keys[evt.key] = true;
			bg.app.MainLoop.singleton.windowController.postRedisplay();
		}

		keyUp(evt) {
			if (!this.enabled) return;
			this._keys[evt.key] = false;
		}
	}
	
	class FPSCameraController extends bg.scene.Component {
		
	}
	
	class TargetCameraController extends bg.scene.Component {
		
	}
	
	bg.scene.registerComponent(bg.manipulation,OrbitCameraController,"bg.manipulation.OrbitCameraController");
	bg.scene.registerComponent(bg.manipulation,FPSCameraController,"bg.manipulation.FPSCameraController");
	bg.scene.registerComponent(bg.manipulation,TargetCameraController,"bg.manipulation.TargetCameraController");
	
})();

bg.tools = {
	
};

(function() {
	class BoundingBox {
		constructor(drawableOrPlist, transformMatrix) {
			this._min = new bg.Vector3(Number.MAX_VALUE,Number.MAX_VALUE,Number.MAX_VALUE);
			this._max = new bg.Vector3(-Number.MAX_VALUE,-Number.MAX_VALUE,-Number.MAX_VALUE);
			this._center = null;
			this._size = null;
			this._halfSize = null;

			this._vertexArray = [];
			transformMatrix = transformMatrix || bg.Matrix4.Identity();
			if (drawableOrPlist instanceof bg.scene.Drawable) {
				this.addDrawable(drawableOrPlist,transformMatrix);
			}
			else if (drawableOrPlist instanceof bg.scene.PolyList) {
				this.addPolyList(drawableOrPlist,transformMatrix);
			}
		}

		clear() {
			this._min = bg.Vector3(Number.MAX_VALUE,Number.MAX_VALUE,Number.MAX_VALUE);
			this._max = bg.Vector3(-Number.MAX_VALUE,-Number.MAX_VALUE,-Number.MAX_VALUE);
			this._center = this._size = this._halfSize = null;
		}

		get min() { return this._min; }
		get max() { return this._max; }
		get center() {
			if (!this._center) {
				let s = this.halfSize;
				this._center = bg.Vector3.Add(this.halfSize, this._min);
			}
			return this._center;
		}

		get size() {
			if (!this._size) {
				this._size = bg.Vector3.Sub(this.max, this.min);
			}
			return this._size;
		} 

		get halfSize() {
			if (!this._halfSize) {
				this._halfSize = new bg.Vector3(this.size);
				this._halfSize.scale(0.5);
	 		}
			return this._halfSize;
		}

		addPolyList(polyList, trx) {
			this._center = this._size = this._halfSize = null;
			for (let i = 0; i<polyList.vertex.length; i+=3) {
				let vec = trx.multVector(new bg.Vector3(polyList.vertex[i],
														polyList.vertex[i + 1],
														polyList.vertex[i + 2]));
				if (vec.x<this._min.x) this._min.x = vec.x;
				if (vec.y<this._min.y) this._min.y = vec.y;
				if (vec.z<this._min.z) this._min.z = vec.z;
				if (vec.x>this._max.x) this._max.x = vec.x;
				if (vec.z>this._max.z) this._max.z = vec.z;
				if (vec.y>this._max.y) this._max.y = vec.y;
			}
		}

		addDrawable(drawable, trxBase) {
			drawable.forEach((plist,mat,elemTrx) => {
                if (plist.visible) {
                    let trx = new bg.Matrix4(trxBase);
                    if (elemTrx) trx.mult(elemTrx);
                    this.addPolyList(plist,trx);
                }
			});
		}
	}

	bg.tools.BoundingBox = BoundingBox;
})();
(function() {
    function createCanvas(width,height) {
        let result = document.createElement("canvas");
        result.width  = width;
        result.height = height;
        result.style.width  = width + "px";
        result.style.height = height + "px";
        return result;
    }

    function resizeCanvas(canvas,w,h) {
        canvas.width  = w;
        canvas.height = h;
        canvas.style.width  = w + 'px';
        canvas.style.height = h + 'px'; 
    }

    let g_texturePreventRemove = [];

    class CanvasTexture extends bg.app.ContextObject {

        constructor(context,width,height,drawCallback) {
            super(context);

            this._canvas = createCanvas(width,height);
                
            this._drawCallback = drawCallback;

            this._drawCallback(this._canvas.getContext("2d",{preserverDrawingBuffer:true}),this._canvas.width,this._canvas.height);
            this._texture = bg.base.Texture.FromCanvas(context,this._canvas);
        }

        get width() { return this._canvas.width; }
        get height() { return this._canvas.height; }
        get canvas() { return this._canvas; }
        get texture() { return this._texture; }

        resize(w,h) {
            resizeCanvas(this._canvas,w,h);
            this.update();
        }

        update() {
            this._drawCallback(this._canvas.getContext("2d",{preserverDrawingBuffer:true}),this.width,this.height);

			bg.base.Texture.UpdateCanvasImage(this._texture,this._canvas);
        }
    }

    bg.tools.CanvasTexture = CanvasTexture;
})();
(function() {

    bg.tools = bg.tools || {};

    function packUVs(rect, t1, t2, uvs, pad) {
		let hpad = pad / 2;
		uvs[t1.uv0.x] = rect.left + pad; uvs[t1.uv0.y] = rect.top + hpad;
		uvs[t1.uv1.x] = rect.right - hpad; uvs[t1.uv1.y] = rect.top + hpad;
		uvs[t1.uv2.x] = rect.right - hpad; uvs[t1.uv2.y] = rect.bottom - pad;

		if (t2) {
			uvs[t2.uv0.x] = rect.right - pad; uvs[t2.uv0.y] = rect.bottom - hpad;
			uvs[t2.uv1.x] = rect.left + hpad; uvs[t2.uv1.y] = rect.bottom - hpad;
			uvs[t2.uv2.x] = rect.left + hpad; uvs[t2.uv2.y] = rect.top + pad;
		}
    }


    function atlasPolyList(vertex,index,padding=0) {
        let triangles = [];
        let uv = [];

        for (let i=0; i<index.length; i+=3) {
            let i0 = index[i];
            let i1 = index[i+1];
            let i2 = index[i+2];
            triangles.push({
                indices: [i0, i1, i2],
                uv0: { x:i0 * 2, y: i0 * 2 + 1 },
                uv1: { x:i1 * 2, y: i1 * 2 + 1 },
                uv2: { x:i2 * 2, y: i2 * 2 + 1 }
            });
        }

        let numQuads = triangles.length / 2 + triangles.length % 2;
        let rows = numQuads, cols = Math.round(Math.sqrt(numQuads));
        while(rows%cols) {
            cols--;
        }

        rows = cols;
        cols = numQuads / cols;

        let currentTriangle = 0;

        let w = 1 / cols;
        let h = 1 / rows;

        for (let i=0; i<rows; ++i) {
            for (let j=0; j<cols; ++j) {
                let rect = { left: w * j, top: h * i, right:w * j + w, bottom:h * i + h};
                let t1 = triangles[currentTriangle];
                let t2 = currentTriangle+1<triangles.length ? triangles[currentTriangle + 1] : null;
                packUVs(rect,t1,t2,uv,padding);
                currentTriangle+=2;
            }
        }

        return uv;
    }

    function generateLightmapQuads(drawable) {
        let triangleCount = 0;
        drawable.forEach((polyList) => {
            let numTriangles = (polyList.index.length / 3);
            // Avoid two poly list to share one quad
            if (numTriangles%2!=0) numTriangles++;
            triangleCount += numTriangles;
        });
        let numQuads = triangleCount / 2;
        let rows = numQuads, cols = Math.round(Math.sqrt(numQuads));
        while (rows%cols) {
            cols--;
        }

        rows = cols;
        cols = numQuads / cols;
        return {
            rows: rows,
            cols: cols,
            triangleCount: triangleCount,
            quadSize: {
                width: 1 / cols,
                height: 1 / rows
            },
            currentRow:0,
            currentCol:0,
            nextRect:function() {
                let rect = {
                    left: this.quadSize.width * this.currentCol,
                    top: this.quadSize.height * this.currentRow,
                    right: this.quadSize.width * this.currentCol + this.quadSize.width,
                    bottom: this.quadSize.height * this.currentRow + this.quadSize.height
                };
                if (this.currentCol<this.cols) {
                    this.currentCol++;
                }
                else {
                    this.currentCol = 0;
                    this.currentRow++;
                }
                if (this.currentRow>=this.rows) {
                    this.currentRow = 0;
                }
                return rect;
            }
        };
    }

    function atlasDrawable(drawable,padding) {
        let quadData = generateLightmapQuads(drawable);
        quadData.currentRow = 0;
        quadData.currentCol = 0;

        drawable.forEach((polyList) => {
            if (polyList.texCoord1.length>0) return;
            let triangles = [];
            let uv = [];
            for (let i=0; i<polyList.index.length; i+=3) {
                let i0 = polyList.index[i];
                let i1 = polyList.index[i + 1];
                let i2 = polyList.index[i + 2];
                triangles.push({
                    indices: [i0, i1, i2],
                    uv0: { x:i0 * 2, y:i0 * 2 + 1 },
                    uv1: { x:i1 * 2, y:i1 * 2 + 1 },
                    uv2: { x:i2 * 2, y:i2 * 2 + 1 }
                });
            }

            for (let i=0; i<triangles.length; i+=2) {
                let t1 = triangles[i];
                let t2 = i+1<triangles.length ? triangles[i+1] : null;
                let rect = quadData.nextRect();
                packUVs(rect,t1,t2,uv,padding);
            }

            polyList.texCoord1 = uv;
            polyList.build();
        });
    }
    
    bg.tools.UVMap = {
        atlas: function(vertexOrDrawable,indexOrPadding = 0,padding = 0) {
            if (vertexOrDrawable instanceof bg.scene.Drawable) {
                return atlasDrawable(vertexOrDrawable,indexOrPadding);
            }
            else {
                return atlasPolyList(vertexOrDrawable,indexOrPadding,padding);
            }
        }


    }
})();

(function() {
    bg.tools = bg.tools || {};

    class TextureMergerImpl {
        mergeMaps(context,r,g,b,a) {
            throw new Error("TextureMergerImpl.mergeMaps(): not implemented");
        }

        destroy(ctx) {

            throw new Error("TextureMergerImpl.destroy(): not implemented");
        }
    }
    bg.tools.TextureMergerImpl = TextureMergerImpl;

    class TextureMerger extends bg.app.ContextObject {
        constructor(context) {
            super(context);
            this._mergerImpl = null;
        }

        mergeMaps(r,g,b,a) {
            this._mergerImpl = this._mergerImpl || bg.Engine.Get().createTextureMergerInstance();
            r = (r && r.map) ? r : { map:bg.base.TextureCache.BlackTexture(this.context), channel:0 };
            g = (g && g.map) ? g : { map:bg.base.TextureCache.BlackTexture(this.context), channel:0 };
            b = (b && b.map) ? b : { map:bg.base.TextureCache.BlackTexture(this.context), channel:0 };
            a = (a && a.map) ? a : { map:bg.base.TextureCache.BlackTexture(this.context), channel:0 };

            return this._mergerImpl.mergeMaps(this.context,r,g,b,a);
        }

        destroy() {
            this._mergerImpl.destroy(this.context);
        }
    }

    bg.tools.TextureMerger = TextureMerger;
})();

bg.render = {
	
};
(function() {
	
	class RenderLayer extends bg.app.ContextObject {
		constructor(context,opacityLayer = bg.base.OpacityLayer.ALL) {
			super(context);
			
			this._pipeline = new bg.base.Pipeline(context);
			this._pipeline.opacityLayer = opacityLayer;
			this._matrixState = bg.base.MatrixState.Current();
			this._drawVisitor = new bg.scene.DrawVisitor(this._pipeline,this._matrixState);
			this._settings = {};
		}

		get pipeline() { return this._pipeline; }

		get opacityLayer() { return this._pipeline.opacityLayer; }

		get drawVisitor() { return this._drawVisitor; }
		
		get matrixState() { return this._matrixState; }
		
		set settings(s) { this._settings = s; }
		get settings() { return this._settings; }

		draw(scene,camera) {}
	}
	
	bg.render.RenderLayer = RenderLayer;
	
})();
(function() {
	
	bg.render.RenderPath = {
		FORWARD:1,
		DEFERRED:2,

		PBR:3,
		PBR_DEFERRED:4
	};
	
	function getRenderPass(context,renderPath) {
		let Factory = null;
			switch (renderPath) {
				case bg.render.RenderPath.FORWARD:
					Factory = bg.render.ForwardRenderPass;
					break;
				case bg.render.RenderPath.DEFERRED:
					if (bg.render.DeferredRenderPass.IsSupported()) {
						Factory = bg.render.DeferredRenderPass;
					}
					else {
						bg.log("WARNING: Deferred renderer is not supported on this browser");
						Factory = bg.render.ForwardRenderPass;
					}
					break;
			}
			
			return Factory && new Factory(context);
	}

	// This is a foward declaration of raytracer quality settings
	bg.render.RaytracerQuality = {
		low : { maxSamples: 20, rayIncrement: 0.05 },
		mid: { maxSamples: 50, rayIncrement: 0.025 },
		high: { maxSamples: 100, rayIncrement: 0.0125 },
		extreme: { maxSamples: 200, rayIncrement: 0.0062 }
	};

	bg.render.ShadowType = {
		HARD: bg.base.ShadowType.HARD,
		SOFT: bg.base.ShadowType.SOFT
	};

	bg.render.ShadowMapQuality = {
		low: 512,
		mid: 1024,
		high: 2048,
		extreme: 4096
	};

	let renderSettings = {
		debug: {
			enabled: false
		},
		ambientOcclusion: {
			enabled: true,
			sampleRadius: 0.4,
			kernelSize: 16,
			blur: 2,
			maxDistance: 300,
			scale: 1.0
		},
		raytracer: {
			enabled: true,
			quality: bg.render.RaytracerQuality.mid,
			scale: 0.5
		},
		antialiasing: {
			enabled: false
		},
		shadows: {
			quality: bg.render.ShadowMapQuality.mid,
			type: bg.render.ShadowType.SOFT
		}
		// TODO: Color correction
	}
	
	class Renderer extends bg.app.ContextObject {
		static Create(context,renderPath) {
			let result = null;
			if (renderPath==bg.render.RenderPath.DEFERRED) {
				result = new bg.render.DeferredRenderer(context);
			}

			if (renderPath==bg.render.RenderPath.FORWARD ||
				(result && !result.isSupported))
			{
				result = new bg.render.ForwardRenderer(context);
			}

			if (renderPath==bg.render.RenderPath.PBR) {
				result = new bg.render.PBRForwardRenderer(context);
			}

			if (renderPath==bg.render.RenderPath.PBR_DEFERRED) {
				bg.log("WARNING: PBR deferred renderer is not implemented. Using PBR forward renderer");
				result = new bg.render.PBRForwardRenderer(context);
			}

			if (result.isSupported) {
				result.create();
			}
			else {
				throw new Error("No suitable renderer found.");
			}
			return result;
		}

		constructor(context) {
			super(context);
			
			this._frameVisitor = new bg.scene.FrameVisitor();

			this._settings = renderSettings;

			this._clearColor = bg.Color.Black();
		}

		get isSupported() { return false; }
		create() { console.log("ERROR: Error creating renderer. The renderer class must implemente the create() method."); }

		get clearColor() { return this._clearColor; }
		set clearColor(c) { this._clearColor = c; }

		frame(sceneRoot,delta) {
			this._frameVisitor.delta = delta;
			sceneRoot.accept(this._frameVisitor);
		}
		
		display(sceneRoot,camera) {
			this.draw(sceneRoot,camera);
		}

		get settings() {
			return this._settings;
		}
	}
	
	bg.render.Renderer = Renderer;
	
})();
(function() {

    function buildDefaultShader() {
        let shader = null;

        let vert = `
        attribute vec3 inPosition;

        varying vec3 fsPosition;

        uniform mat4 inProjection;
        uniform mat4 inView;

        void main() {
            fsPosition = inPosition;
            gl_Position = inProjection * inView * vec4(inPosition,1.0);
        }
        `;

        let frag = `
        precision highp float;
        varying vec3 fsPosition;

        void main() {
            gl_FragColor = vec4(1.0,0.0,0.0,1.0);
        }
        `;

        shader = new bg.base.Shader(this.context);
        shader.addShaderSource(bg.base.ShaderType.VERTEX, vert);
        shader.addShaderSource(bg.base.ShaderType.FRAGMENT, frag);

        status = shader.link();
        if (!shader.status) {
            throw new Error("Error generating default equirectangular cube shader");
        }

        shader.initVars(["inPosition"],["inProjection","inView"]);

        this.setShader(shader,(shader,projection,view) => { 
            shader.setMatrix4("inProjection",projection);
            shader.setMatrix4("inView",view);
        })
    }

    function buildCube(size) {
        let plist = new bg.base.PolyList(this.context);

        let hsize = size / 2;
        plist.vertex = [
            -hsize,-hsize,-hsize,   // 0: left    bottom   back
            -hsize,-hsize, hsize,   // 1: left    bottom   front
            -hsize, hsize,-hsize,   // 2: left    top      back
            -hsize, hsize, hsize,   // 3: left    top      front
             hsize,-hsize,-hsize,   // 4: right   bottom   back
             hsize,-hsize, hsize,   // 5: right   bottom   front
             hsize, hsize,-hsize,   // 6: right   top      back
             hsize, hsize, hsize,   // 7: right   top      front
        ];
        plist.index = [
            1, 5, 7,   7, 3, 1, // front
            0, 2, 6,   6, 4, 0, // back
            0, 1, 3,   3, 2, 0, // left
            5, 4, 6,   6, 7, 5, // right
            3, 7, 6,   6, 2, 3, // top
            5, 1, 0,   0, 4, 5  // bototm
        ];
        plist.build();
        return plist;
    }

    let s_cube = null;
    class CubeRenderer extends bg.app.ContextObject {

        constructor(context,size = 1) {
            super(context);

            this._size = size;
            this._shader = null;
            this._cube = null;

            this._pipeline = new bg.base.Pipeline(this.context);
        }

        setShader(shader,setInputVarsCallback) {
            this._shader = shader;
            this._setInputVarsCallback = setInputVarsCallback;
        }

        get shader() {
            if (!this._shader) {
                buildDefaultShader.apply(this);
            }
            return this._shader;
        }

        get cube() {
            if (!this._cube) {
                this._cube = buildCube.apply(this,[this._size]);
            }
            return this._cube;
        }

        get projectionMatrix() {
            if (!this._projection) {
                this._projection = bg.Matrix4.Perspective(60.0,1.0,0.1,100.0);
            }
            return this._projection;
        }

        set projectionMatrix(p) {
            this._projection = p;
        }

        get viewMatrix() {
            if (!this._viewMatrix) {
                this._viewMatrix = bg.Matrix4.Identity();
            }
            return this._viewMatrix;
        }

        set viewMatrix(m) {
            this._viewMatrix = m;
        }

        get pipeline() {
            return this._pipeline;
        }

        create() {

        }

        destroy() {
            this.shader.destroy();
            this.cube.destroy();
            this._shader = null;
            this._cube = null;
        }

        render(customPipeline = false) {
            let prevPipeline = bg.base.Pipeline.Current();
            if (!customPipeline) {
                bg.base.Pipeline.SetCurrent(this._pipeline);
                this._pipeline.clearBuffers();
            }

            this.shader.setActive();
            this.shader.setInputBuffer("position",this.cube.vertexBuffer,3);
            this._setInputVarsCallback(this.shader,this.projectionMatrix,this.viewMatrix);

            this.cube.draw();

            this.shader.disableInputBuffer("position");
            this.shader.clearActive();

            if (!customPipeline) {
                bg.base.Pipeline.SetCurrent(prevPipeline);
            }
        }
    }

    bg.render.CubeRenderer = CubeRenderer;

    class EquirectangularCubeRenderer extends CubeRenderer {
        get texture() { return this._texture; }
        set texture(t) { this._texture = t; }

        constructor(context,size=1) {
            super(context,size);
            this._texture = null;
        }

        create() {
            this.pipeline.buffersToClear = 0;
            this.pipeline.cullFace = false;

            let context = this.context;
            let shader = new bg.base.Shader(context);
            let vert = `
            attribute vec3 inPosition;

            varying vec3 fsPosition;

            uniform mat4 inProjection;
            uniform mat4 inView;

            void main() {
                fsPosition = inPosition;
                gl_Position = inProjection * inView * vec4(inPosition,1.0);
            }
            `;
            let frag = `
            precision highp float;
            varying vec3 fsPosition;
            
            uniform sampler2D inEquirectangularMap;

            const vec2 invAtan = vec2(0.1591,0.3183);
            vec2 sampleSphericalMap(vec3 v) {
                vec2 uv = vec2(atan(v.z,v.x),asin(v.y));
                uv *= invAtan;
                uv += 0.5;
                return uv;
            }

            void main() {
                vec2 uv = sampleSphericalMap(normalize(fsPosition));
                vec3 color = texture2D(inEquirectangularMap,uv).rgb;
                gl_FragColor = vec4(color,1.0);
            }
            `;
            shader.addShaderSource(bg.base.ShaderType.VERTEX, vert);
            shader.addShaderSource(bg.base.ShaderType.FRAGMENT, frag);
            if (!shader.link()) {
                throw new Error("Error generating equirectangular cube renderer: shader compile error");
            }
            shader.initVars(['inPosition'],['inProjection','inView','inEquirectangularMap']);
            this.setShader(shader,(sh,proj,view) => {
                sh.setMatrix4("inProjection",proj);
                sh.setMatrix4("inView",view);
                if (this.texture) {
                    sh.setTexture("inEquirectangularMap",this.texture,bg.base.TextureUnit.TEXTURE_0);
                }
            });
        }
    }
    
    bg.render.EquirectangularCubeRenderer = EquirectangularCubeRenderer;

    function createIrradianceMapShader() {
        let context = this.context;
        let shader = new bg.base.Shader(context);
        let vert = `
        attribute vec3 inPosition;

        varying vec3 fsPosition;

        uniform mat4 inProjection;
        uniform mat4 inView;

        void main() {
            fsPosition = inPosition;
            gl_Position = inProjection * inView * vec4(inPosition,1.0);
        }
        `;
        let sampleDelta = 0.07;
        let frag = `
        precision highp float;
        varying vec3 fsPosition;
        
        uniform samplerCube inCubeMap;

        void main() {
            vec3 normal = normalize(fsPosition);
            vec3 irradiance = vec3(0.0);

            vec3 up    = vec3(0.0, 1.0, 0.0);
            vec3 right = cross(up, normal);
            up         = cross(normal, right);
            
            float nrSamples = 0.0; 
            for(float phi = 0.0; phi < ${ 2.0 * Math.PI}; phi += ${ sampleDelta })
            {
                for(float theta = 0.0; theta < ${ 0.5 * Math.PI }; theta += ${ sampleDelta })
                {
                    // spherical to cartesian (in tangent space)
                    vec3 tangentSample = vec3(sin(theta) * cos(phi),  sin(theta) * sin(phi), cos(theta));
                    // tangent space to world
                    vec3 sampleVec = tangentSample.x * right + tangentSample.y * up + tangentSample.z * normal; 
            
                    irradiance += textureCube(inCubeMap, sampleVec).rgb * cos(theta) * sin(theta);
                    nrSamples++;
                }
            }
            irradiance = ${ Math.PI } * irradiance * (1.0 / float(nrSamples));

            gl_FragColor = vec4(irradiance,1.0);
        }
        `;
        shader.addShaderSource(bg.base.ShaderType.VERTEX, vert);
        shader.addShaderSource(bg.base.ShaderType.FRAGMENT, frag);
        if (!shader.link()) {
            throw new Error("Error generating irradiance map cube renderer: shader compile error");
        }
        shader.initVars(['inPosition'],['inProjection','inView','inCubeMap']);
        this.setShader(shader,(sh,proj,view) => {
            sh.setMatrix4("inProjection",proj);
            sh.setMatrix4("inView",view);
            if (this.texture) {
                sh.setTexture("inCubeMap",this.texture,bg.base.TextureUnit.TEXTURE_0);
            }
        });
    }

    function createSpecularMapShader() {
        let context = this.context;
        let shader = new bg.base.Shader(context);
        let vert = `
        attribute vec3 inPosition;

        varying vec3 fsPosition;

        uniform mat4 inProjection;
        uniform mat4 inView;

        void main() {
            fsPosition = inPosition;
            gl_Position = inProjection * inView * vec4(inPosition,1.0);
        }
        `;
        let sampleDelta = 0.09;
        let sampleCount = 128;
        let frag = `
        precision highp float;
        varying vec3 fsPosition;
        
        uniform samplerCube inCubeMap;
        uniform float inRoughness;

        float vanDerCorpus(int n, int base) {
            float invBase = 1.0 / float(base);
            float denom   = 1.0;
            float result  = 0.0;

            for(int i = 0; i < 16; ++i)
            {
                if(n > 0)
                {
                    denom   = mod(float(n), 2.0);
                    result += denom * invBase;
                    invBase = invBase / 2.0;
                    n       = int(float(n) / 2.0);
                }
            }

            return result;
        }

        vec2 hammersleyNoBitOps(int i, int N) {
            return vec2(float(i)/float(N), vanDerCorpus(i, 2));
        }

        vec3 importanceSampleGGX(vec2 Xi, vec3 N, float roughness) {
            float a = roughness*roughness;
            
            float phi = 2.0 * ${ Math.PI } * Xi.x;
            float cosTheta = sqrt((1.0 - Xi.y) / (1.0 + (a*a - 1.0) * Xi.y));
            float sinTheta = sqrt(1.0 - cosTheta*cosTheta);
            
            // from spherical coordinates to cartesian coordinates
            vec3 H;
            H.x = sin(phi) * sinTheta;
            H.y = cos(phi) * sinTheta;
            H.z = cosTheta;
            
            // from tangent-space vector to world-space sample vector
            vec3 up        = abs(N.z) < 0.999 ? vec3(0.0, 0.0, 1.0) : vec3(1.0, 0.0, 0.0);
            vec3 tangent   = normalize(cross(up, N));
            vec3 bitangent = cross(N, tangent);
            
            vec3 sampleVec = tangent * H.x + bitangent * H.y + N * H.z;
            return normalize(sampleVec);
        }  

        void main() {
            vec3 N = normalize(fsPosition);    
            vec3 R = N;
            vec3 V = R;

            float totalWeight = 0.0;   
            vec3 prefilteredColor = vec3(0.0);     
            for(int i = 0; i < ${ sampleCount }; ++i)
            {
                vec2 Xi = hammersleyNoBitOps(i, ${ sampleCount });
                vec3 H  = importanceSampleGGX(Xi, N, inRoughness);
                vec3 L  = normalize(2.0 * dot(V, H) * H - V);

                float NdotL = max(dot(N, L), 0.0);
                if(NdotL > 0.0)
                {
                    prefilteredColor += textureCube(inCubeMap, L).rgb * NdotL;
                    totalWeight      += NdotL;
                }
            }
            prefilteredColor = prefilteredColor / totalWeight;

            gl_FragColor = vec4(prefilteredColor, 1.0);
        }
        `;
        shader.addShaderSource(bg.base.ShaderType.VERTEX, vert);
        shader.addShaderSource(bg.base.ShaderType.FRAGMENT, frag);
        if (!shader.link()) {
            throw new Error("Error generating specular map cube renderer: shader compile error");
        }
        shader.initVars(['inPosition'],['inProjection','inView','inCubeMap','inRoughness']);
        this.setShader(shader,(sh,proj,view) => {
            sh.setMatrix4("inProjection",proj);
            sh.setMatrix4("inView",view);
            sh.setValueFloat("inRoughness",this.roughness);
            if (this.texture) {
                sh.setTexture("inCubeMap",this.texture,bg.base.TextureUnit.TEXTURE_0);
            }
        });
    }

    bg.render.CubeMapShader = {
        IRRADIANCE_MAP: 0,
        SPECULAR_MAP: 1
    };

    class CubeMapRenderer extends CubeRenderer {
        get texture() { return this._texture; }
        set texture(t) { this._texture = t; }

        set roughness(r) { this._roughness = r; }
        get roughness() { return this._roughness || 0; } 

        constructor(context,size=1) {
            super(context,size);
            this._texture = null;
        }

        create(shaderType = bg.render.CubeMapShader.IRRADIANCE_MAP) {
            this.pipeline.buffersToClear = 0;
            this.pipeline.cullFace = false;

            switch (shaderType) {
            case bg.render.CubeMapShader.IRRADIANCE_MAP:
                createIrradianceMapShader.apply(this);
                break;
            case bg.render.CubeMapShader.SPECULAR_MAP:
                createSpecularMapShader.apply(this);
                break;
            }
        }
    }

    bg.render.CubeMapRenderer = CubeMapRenderer;
})();
(function() {
	function lib() {
		return bg.base.ShaderLibrary.Get();
	}

	let MAX_KERN_OFFSETS = 64;

	class DeferredMixEffect extends bg.base.TextureEffect {
		constructor(context) {
			super(context);

			this._ssrtScale = 0.5;
			this._frameScale = 1.0;
		}
		
		get fragmentShaderSource() {
			if (!this._fragmentShaderSource) {
				this._fragmentShaderSource = new bg.base.ShaderSource(bg.base.ShaderType.FRAGMENT);
				
				this._fragmentShaderSource.addParameter([
					{ name:"inLighting", dataType:"sampler2D", role:"value"},
					{ name:"inDiffuse", dataType:"sampler2D", role:"value"},
					{ name:"inPositionMap", dataType:"sampler2D", role:"value"},
					{ name:"inSSAO", dataType:"sampler2D", role:"value"},
					{ name:"inReflection", dataType:"sampler2D", role:"value"},
					{ name:"inSpecularMap", dataType:"sampler2D", role:"value" },
					{ name:"inMaterial", dataType:"sampler2D", role:"value"},
					{ name:"inOpaqueDepthMap", dataType:"sampler2D", role:"value"},
					{ name:"inShininessColor", dataType:"sampler2D", role:"value" }, 
					{ name:"inViewSize", dataType:"vec2", role:"value"},
					{ name:"inSSAOBlur", dataType:"int", role:"value"},
					{ name:"inSSRTScale", dataType:"float", role:"value" },

					{ name:"fsTexCoord", dataType:"vec2", role:"in" }	// vTexturePosition
				]);
				
				if (bg.Engine.Get().id=="webgl1") {
					this._fragmentShaderSource.addFunction(lib().functions.blur.textureDownsample);
					this._fragmentShaderSource.addFunction(lib().functions.blur.blur);
					this._fragmentShaderSource.addFunction(lib().functions.blur.glowBlur);

					//this._fragmentShaderSource.addFunction(lib().functions.colorCorrection.all);

					this._fragmentShaderSource.setMainBody(`
					vec4 lighting = clamp(texture2D(inLighting,fsTexCoord),vec4(0.0),vec4(1.0));
					vec4 diffuse = texture2D(inDiffuse,fsTexCoord);
					vec4 pos = texture2D(inPositionMap,fsTexCoord);
					vec4 shin = texture2D(inShininessColor,fsTexCoord);
					vec4 ssao = blur(inSSAO,fsTexCoord,inSSAOBlur * 20,inViewSize);
					vec4 material = texture2D(inMaterial,fsTexCoord);

					vec4 specular = texture2D(inSpecularMap,fsTexCoord);	// The roughness parameter is stored on A component, inside specular map
					
					vec4 opaqueDepth = texture2D(inOpaqueDepthMap,fsTexCoord);
					if (pos.z<opaqueDepth.z && opaqueDepth.w<1.0) {
						discard;
					}
					else {
						float roughness = specular.a;
						float ssrtScale = inSSRTScale;
						roughness *= 250.0 * ssrtScale;
						vec4 reflect = blur(inReflection,fsTexCoord,int(roughness),inViewSize * ssrtScale);

						float reflectionAmount = material.b;
						vec3 finalColor = lighting.rgb * (1.0 - reflectionAmount);
						finalColor += reflect.rgb * reflectionAmount * diffuse.rgb + shin.rgb;
						finalColor *= ssao.rgb;
						gl_FragColor = vec4(finalColor,diffuse.a);
					}`);
				}
			}
			return this._fragmentShaderSource;
		}
		
		setupVars() {
			this.shader.setVector2("inViewSize",new bg.Vector2(this.viewport.width, this.viewport.height));

			this.shader.setTexture("inLighting",this._surface.lightingMap,bg.base.TextureUnit.TEXTURE_0);
			this.shader.setTexture("inDiffuse",this._surface.diffuseMap,bg.base.TextureUnit.TEXTURE_1);
			this.shader.setTexture("inPositionMap",this._surface.positionMap,bg.base.TextureUnit.TEXTURE_2);
			this.shader.setTexture("inSSAO",this._surface.ssaoMap,bg.base.TextureUnit.TEXTURE_3);
			this.shader.setTexture("inReflection",this._surface.reflectionMap,bg.base.TextureUnit.TEXTURE_4);
			this.shader.setTexture("inMaterial",this._surface.materialMap,bg.base.TextureUnit.TEXTURE_5);
			this.shader.setTexture("inSpecularMap",this._surface.specularMap,bg.base.TextureUnit.TEXTURE_6);
			this.shader.setTexture("inOpaqueDepthMap",this._surface.opaqueDepthMap,bg.base.TextureUnit.TEXTURE_7);
			this.shader.setTexture("inShininessColor",this._surface.shininess,bg.base.TextureUnit.TEXTURE_8);
	
			this.shader.setValueInt("inSSAOBlur",this.ssaoBlur);
			this.shader.setValueFloat("inSSRTScale",this.ssrtScale * this.frameScale);
		}

		set viewport(vp) { this._viewport = vp; }
		get viewport() { return this._viewport; }

		set clearColor(c) { this._clearColor = c; }
		get clearColor() { return this._clearColor; }
		
		set ssaoBlur(b) { this._ssaoBlur = b; }
		get ssaoBlur() { return this._ssaoBlur; }

		set ssrtScale(s) { this._ssrtScale = s; }
		get ssrtScale() { return this._ssrtScale; }

		set frameScale(s) { this._frameScale = s; }
		get frameScale() { return this._frameScale; }

		get colorCorrection() {
			if (!this._colorCorrection) {
				this._colorCorrection = {
					hue: 1.0,
					saturation: 1.0,
					lightness: 1.0,
					brightness: 0.5,
					contrast: 0.5
				};
			}
			return this._colorCorrection;
		}
	}
	
	bg.render.DeferredMixEffect = DeferredMixEffect;
})();
(function() {

	bg.render.SurfaceType = {
		UNDEFINED: 0,
		OPAQUE: 1,
		TRANSPARENT: 2
	};

	let g_ssrtScale = 0.5;
	let g_ssaoScale = 1.0;

	class DeferredRenderSurfaces extends bg.app.ContextObject {
		constructor(context) {
			super(context);
			this._type = bg.render.SurfaceType.UNDEFINED;

			this._gbufferUByteSurface = null;
			this._gbufferFloatSurface = null;
			this._lightingSurface = null;
			this._shadowSurface = null;
			this._ssaoSurface = null;
			this._mixSurface = null;
			this._ssrtSurface = null;
	
			this._opaqueSurfaces = null;
		}
	
		createOpaqueSurfaces() {
			this._type = bg.render.SurfaceType.OPAQUE;
			this.createCommon();
		}

		createTransparentSurfaces(opaqueSurfaces) {
			this._type = bg.render.SurfaceType.TRANSPARENT;
			this._opaqueSurfaces = opaqueSurfaces;
			this.createCommon();
		}
		
		resize(newSize) {
			let s = new bg.Vector2(newSize.width,newSize.height);
			this._gbufferUByteSurface.size = s;
			this._gbufferFloatSurface.size = s;
			this._lightingSurface.size = s;
			this._shadowSurface.size = s;
			this._ssaoSurface.size = new bg.Vector2(s.x * g_ssaoScale,s.y * g_ssaoScale);
			this._ssrtSurface.size = new bg.Vector2(s.x * g_ssrtScale,s.y * g_ssrtScale);
			this._mixSurface.size = s;
		}

		get type() { return this._type; }
		
		get gbufferUByteSurface() { return this._gbufferUByteSurface; }
		get gbufferFloatSurface() { return this._gbufferFloatSurface; }
		get lightingSurface() { return this._lightingSurface; }
		get shadowSurface() { return this._shadowSurface; }
		get ssaoSurface() { return this._ssaoSurface; }
		get ssrtSurface() { return this._ssrtSurface; }
		get mixSurface() { return this._mixSurface; }
		
		get diffuse() { return this._gbufferUByteSurface.getTexture(0); }
		get specular() { return this._gbufferUByteSurface.getTexture(1); }
		get normal() { return this._gbufferUByteSurface.getTexture(2); }
		get material() { return this._gbufferUByteSurface.getTexture(3); }
		get position() { return this._gbufferFloatSurface.getTexture(0); }
		get lighting() { return this._lightingSurface.getTexture(); }
		get shadow() { return this._shadowSurface.getTexture(); }
		get ambientOcclusion() { return this._ssaoSurface.getTexture(); }
		get reflection() { return this._ssrtSurface.getTexture(); }
		get mix() { return this._mixSurface.getTexture(); }
		get reflectionColor() { return this._type==bg.render.SurfaceType.OPAQUE ? this.lighting : this._opaqueSurfaces.lighting; }
		get reflectionDepth() { return this._type==bg.render.SurfaceType.OPAQUE ? this.position : this._opaqueSurfaces.position; }
		get mixDepthMap() { return this._type==bg.render.SurfaceType.OPAQUE ? this.position : this._opaqueSurfaces.position; }
		
		// Generated in lighting shader
		get shininess() { return this._lightingSurface.getTexture(1); }
		get bloom() { return this._lightingSurface.getTexture(2); }
		
		createCommon() {
			var ctx = this.context;
			this._gbufferUByteSurface = new bg.base.TextureSurface(ctx);
			this._gbufferUByteSurface.create([
				{ type:bg.base.RenderSurfaceType.RGBA, format:bg.base.RenderSurfaceFormat.UNSIGNED_BYTE },
				{ type:bg.base.RenderSurfaceType.RGBA, format:bg.base.RenderSurfaceFormat.UNSIGNED_BYTE },
				{ type:bg.base.RenderSurfaceType.RGBA, format:bg.base.RenderSurfaceFormat.UNSIGNED_BYTE },
				{ type:bg.base.RenderSurfaceType.RGBA, format:bg.base.RenderSurfaceFormat.UNSIGNED_BYTE },
				{ type:bg.base.RenderSurfaceType.DEPTH, format:bg.base.RenderSurfaceFormat.RENDERBUFFER }
			]);
			this._gbufferUByteSurface.resizeOnViewportChanged = false;

			this._gbufferFloatSurface = new bg.base.TextureSurface(ctx);
			this._gbufferFloatSurface.create([
				{ type:bg.base.RenderSurfaceType.RGBA, format:bg.base.RenderSurfaceFormat.FLOAT },
				{ type:bg.base.RenderSurfaceType.DEPTH, format:bg.base.RenderSurfaceFormat.RENDERBUFFER }
			]);
			this._gbufferFloatSurface.resizeOnViewportChanged = false;

			this._lightingSurface = new bg.base.TextureSurface(ctx);
			this._lightingSurface.create([
				{ type:bg.base.RenderSurfaceType.RGBA, format:bg.base.RenderSurfaceFormat.FLOAT },
				{ type:bg.base.RenderSurfaceType.RGBA, format:bg.base.RenderSurfaceFormat.FLOAT },
				{ type:bg.base.RenderSurfaceType.RGBA, format:bg.base.RenderSurfaceFormat.FLOAT },
				{ type:bg.base.RenderSurfaceType.DEPTH, format:bg.base.RenderSurfaceFormat.RENDERBUFFER }
			]);
			this._lightingSurface.resizeOnViewportChanged = false;

			this._shadowSurface = new bg.base.TextureSurface(ctx);
			this._shadowSurface.create([
				{ type:bg.base.RenderSurfaceType.RGBA, format:bg.base.RenderSurfaceFormat.UNSIGNED_BYTE },
				{ type:bg.base.RenderSurfaceType.DEPTH, format:bg.base.RenderSurfaceFormat.RENDERBUFFER }
			]);
			this._shadowSurface.resizeOnViewportChanged = false;

			this._ssaoSurface = new bg.base.TextureSurface(ctx);
			this._ssaoSurface.create();
			this._ssaoSurface.resizeOnViewportChanged = false;

			this._ssrtSurface = new bg.base.TextureSurface(ctx);
			this._ssrtSurface.create();
			this._ssrtSurface.resizeOnViewportChanged = false;

			this._mixSurface = new bg.base.TextureSurface(ctx);
			this._mixSurface.create();
			this._mixSurface.resizeOnViewportChanged = false;
		}
	}

	bg.render.DeferredRenderSurfaces = DeferredRenderSurfaces;

	function newPL(ctx,fx,surface,opacityLayer) {
		let pl = new bg.base.Pipeline(ctx);
		pl.renderSurface = surface;
		if (opacityLayer!==undefined) {
			pl.opacityLayer = opacityLayer;
			pl.effect = fx;
		}
		else {
			pl.textureEffect = fx;
		}
		return pl;
	}

	function createCommon(deferredRenderLayer) {
		let ctx = deferredRenderLayer.context;
		let s = deferredRenderLayer._surfaces;
		let opacityLayer = deferredRenderLayer._opacityLayer;

		deferredRenderLayer._gbufferUbyte = newPL(ctx,
			new bg.render.GBufferEffect(ctx),
			s.gbufferUByteSurface,opacityLayer);
		deferredRenderLayer._gbufferFloat = newPL(ctx,
			new bg.render.PositionGBufferEffect(ctx),
			s.gbufferFloatSurface, opacityLayer);
		deferredRenderLayer._shadow = newPL(ctx,
			new bg.render.ShadowEffect(ctx),
			s.shadowSurface,
			bg.base.OpacityLayer.ALL);

		deferredRenderLayer._lighting = newPL(ctx,new bg.render.LightingEffect(ctx), s.lightingSurface);
		deferredRenderLayer._ssao = newPL(ctx,new bg.render.SSAOEffect(ctx), s.ssaoSurface);
		deferredRenderLayer._ssao.clearColor = bg.Color.White();
		deferredRenderLayer._ssrt = newPL(ctx,new bg.render.SSRTEffect(ctx), s.ssrtSurface);

		// Visitors for gbuffers and shadow maps
		let matrixState = deferredRenderLayer.matrixState;
		deferredRenderLayer.ubyteVisitor = new bg.scene.DrawVisitor(deferredRenderLayer._gbufferUbyte,matrixState);
		deferredRenderLayer.floatVisitor = new bg.scene.DrawVisitor(deferredRenderLayer._gbufferFloat,matrixState);
		deferredRenderLayer.shadowVisitor = new bg.scene.DrawVisitor(deferredRenderLayer._shadow,matrixState);


		// TODO: Debug code. Uncomment newPL()
		deferredRenderLayer._mix = newPL(ctx,new bg.render.DeferredMixEffect(ctx), s.mixSurface);
		//deferredRenderLayer._mix.renderSurface = new bg.base.ColorSurface(ctx);
		//deferredRenderLayer._mix = new bg.base.Pipeline(ctx);
		//deferredRenderLayer._mix.renderSurface = s.mixSurface;
	}

	class DeferredRenderLayer extends bg.render.RenderLayer {
		constructor(context) {
			super(context);
		}

		createOpaque() {
			this._opacityLayer = bg.base.OpacityLayer.OPAQUE;
			this._surfaces = new bg.render.DeferredRenderSurfaces(this.context);
			this._surfaces.createOpaqueSurfaces();
			createCommon(this);
		}

		createTransparent(opaqueMaps) {
			this._opacityLayer = bg.base.OpacityLayer.TRANSPARENT;
			this._surfaces = new bg.render.DeferredRenderSurfaces(this.context);
			this._surfaces.createTransparentSurfaces(opaqueMaps);
			createCommon(this);

			this._gbufferUbyte.blend = true;
			this._gbufferUbyte.setBlendMode = bg.base.BlendMode.NORMAL;
			this._gbufferUbyte.clearColor = bg.Color.Transparent();
			this._lighting.clearColor = bg.Color.Black();
			this.pipeline.clearColor = bg.Color.Transparent();
		}

		set shadowMap(sm) { this._shadowMap = sm; }
		get shadowMap() { return this._shadowMap; }

		get pipeline() { return this._mix; }
		get texture() { return this.maps.mix; }

		// TODO: Scene is used by the shadow map generator, but the shadow map can also be
		// modified to use the render queue
		draw(renderQueue,scene,camera) {
			g_ssaoScale = this.settings.ambientOcclusion.scale || 1;
			g_ssrtScale = this.settings.raytracer.scale || 0.5;

			this.matrixState.projectionMatrixStack.set(camera.projection);
			this.matrixState.viewMatrixStack.set(camera.viewMatrix);
			this.matrixState.modelMatrixStack.identity();
			
			this.performDraw(renderQueue,scene,camera);
		}
		
		get maps() { return this._surfaces; }
		
		resize(camera) {
			g_ssaoScale = this.settings.ambientOcclusion.scale || 1;
			g_ssrtScale = this.settings.raytracer.scale || 0.5;

			let vp = camera.viewport;
			this.maps.resize(new bg.Size2D(vp.width,vp.height));
		}
		
		performDraw(renderQueue,scene,camera) {
			let activeQueue = this._opacityLayer==bg.base.OpacityLayer.OPAQUE ? renderQueue.opaqueQueue : renderQueue.transparentQueue;

			let performRenderQueue = (queue,pipeline) => {
				this.matrixState.modelMatrixStack.push();
				this.matrixState.viewMatrixStack.push();
				queue.forEach((objectData) => {
					this.matrixState.modelMatrixStack.set(objectData.modelMatrix);
					this.matrixState.viewMatrixStack.set(objectData.viewMatrix);
					pipeline.effect.material = objectData.material;
					pipeline.draw(objectData.plist);
				});
				this.matrixState.modelMatrixStack.pop();
				this.matrixState.viewMatrixStack.pop();
			}

			bg.base.Pipeline.SetCurrent(this._gbufferUbyte);
			this._gbufferUbyte.viewport = camera.viewport;
			this._gbufferUbyte.clearBuffers();
			performRenderQueue(activeQueue,this._gbufferUbyte);
			
			bg.base.Pipeline.SetCurrent(this._gbufferFloat);
			this._gbufferFloat.viewport = camera.viewport;
			this._gbufferFloat.clearBuffers();
			performRenderQueue(activeQueue,this._gbufferFloat);

			// Render lights
			this._lighting.viewport = camera.viewport;
			this._lighting.clearcolor = bg.Color.White();
			bg.base.Pipeline.SetCurrent(this._lighting);
			this._lighting.clearBuffers(bg.base.ClearBuffers.COLOR_DEPTH);
			this._lighting.blendMode = bg.base.BlendMode.ADD;
			this._lighting.blend = true;
			this._shadow.viewport = camera.viewport;

			let lightIndex = 0;
			bg.scene.Light.GetActiveLights().forEach((lightComponent) => {
				if (lightComponent.light && lightComponent.light.enabled &&
					lightComponent.node && lightComponent.node.enabled)
				{
					if (lightComponent.light.type==bg.base.LightType.DIRECTIONAL)
					{
						this._shadowMap.update(scene,camera,lightComponent.light,lightComponent.transform,bg.base.ShadowCascade.NEAR);
					
						bg.base.Pipeline.SetCurrent(this._shadow);
						this._shadow.viewport = camera.viewport;
						this._shadow.clearBuffers();
						this._shadow.effect.light = lightComponent.light;
						this._shadow.effect.shadowMap = this._shadowMap;
						scene.accept(this.shadowVisitor);	
					}
					else if (lightComponent.light.type==bg.base.LightType.SPOT) {
						this._shadowMap.shadowType = this.settings.shadows.type;
						this._shadowMap.update(scene,camera,lightComponent.light,lightComponent.transform);
						bg.base.Pipeline.SetCurrent(this._shadow);
						this._shadow.viewport = camera.viewport;
						this._shadow.clearBuffers();
						this._shadow.effect.light = lightComponent.light;
						this._shadow.effect.shadowMap = this._shadowMap;
						scene.accept(this.shadowVisitor);
					}

					bg.base.Pipeline.SetCurrent(this._lighting);
					// Only render light emission in the first light source
					this._lighting.textureEffect.lightEmissionFactor = lightIndex==0 ? 10:0;

					this._lighting.textureEffect.light = lightComponent.light;
					this._lighting.textureEffect.lightTransform = lightComponent.transform;
					this._lighting.textureEffect.shadowMap = this.maps.shadow;
					this._lighting.drawTexture(this.maps);
					++lightIndex;
				}				
			});

			let renderSSAO = this.settings.ambientOcclusion.enabled;
			let renderSSRT = this.settings.raytracer.enabled;
			let vp = new bg.Viewport(camera.viewport);

			this._ssao.textureEffect.enabled = renderSSAO;
			this._ssao.textureEffect.settings.kernelSize = this.settings.ambientOcclusion.kernelSize;
			this._ssao.textureEffect.settings.sampleRadius = this.settings.ambientOcclusion.sampleRadius;
			this._ssao.textureEffect.settings.maxDistance = this.settings.ambientOcclusion.maxDistance;
			if (renderSSAO) {
				bg.base.Pipeline.SetCurrent(this._ssao);
				this._ssao.viewport = new bg.Viewport(vp.x,vp.y,vp.width * g_ssaoScale, vp.height * g_ssaoScale);
				this._ssao.clearBuffers();
				this._ssao.textureEffect.viewport = camera.viewport;
				this._ssao.textureEffect.projectionMatrix = camera.projection;
				this._ssao.drawTexture(this.maps);
			}


			// SSRT
			bg.base.Pipeline.SetCurrent(this._ssrt);
			if (renderSSRT) {
				this._ssrt.viewport = new bg.Viewport(vp.x,vp.y,vp.width * g_ssrtScale, vp.height * g_ssrtScale);
				this._ssrt.clearBuffers(bg.base.ClearBuffers.DEPTH);
				this._ssrt.textureEffect.quality = this.settings.raytracer.quality;
				var cameraTransform = camera.node.component("bg.scene.Transform");
				if (cameraTransform) {
					let viewProjection = new bg.Matrix4(camera.projection);
					viewProjection.mult(camera.viewMatrix);
					this._ssrt.textureEffect.cameraPosition= viewProjection.multVector(cameraTransform.matrix.position).xyz;
				}
				this._ssrt.textureEffect.projectionMatrix = camera.projection;
				this._ssrt.textureEffect.rayFailColor = this.settings.raytracer.clearColor || bg.Color.Black();
				this._ssrt.textureEffect.basic = this.settings.raytracer.basicReflections || false;
				this._ssrt.textureEffect.viewportSize = new bg.Vector2(this._ssrt.viewport.width,this._ssrt.viewport.height);
				this._ssrt.drawTexture(this.maps);
			}

			bg.base.Pipeline.SetCurrent(this.pipeline);
			this.pipeline.viewport = camera.viewport;
			this.pipeline.clearBuffers();
			this.pipeline.textureEffect.viewport = camera.viewport;
			this.pipeline.textureEffect.ssaoBlur = renderSSAO ? this.settings.ambientOcclusion.blur : 1;
	
			this.pipeline.textureEffect.ssrtScale = g_ssrtScale * this.settings.renderScale;
			this.pipeline.drawTexture({
				lightingMap:this.maps.lighting,
				diffuseMap:this.maps.diffuse,
				positionMap:this.maps.position,
				ssaoMap:renderSSAO ? this.maps.ambientOcclusion:bg.base.TextureCache.WhiteTexture(this.context),
				reflectionMap:renderSSRT ? this.maps.reflection:this.maps.lighting,
				specularMap:this.maps.specular,
				materialMap:this.maps.material,
				opaqueDepthMap:this.maps.mixDepthMap,
				shininess:this.maps.shininess
				// TODO: bloom
			});	// null: all textures are specified as parameters to the effect

			camera.viewport = vp;
		}
	}
	
	bg.render.DeferredRenderLayer = DeferredRenderLayer;
	
})();

(function() {



	class DeferredRenderer extends bg.render.Renderer {
		constructor(context) {
			super(context);
			this._size = new bg.Size2D(0);
		}

		get isSupported() {
			return  bg.base.RenderSurface.MaxColorAttachments()>5 &&
					bg.base.RenderSurface.SupportFormat(bg.base.RenderSurfaceFormat.FLOAT) &&
					bg.base.RenderSurface.SupportType(bg.base.RenderSurfaceType.DEPTH);
		}

		get pipeline() { return this._pipeline; }

		create() {
			let ctx = this.context;

			this._renderQueueVisitor = new bg.scene.RenderQueueVisitor();

			this._opaqueLayer = new bg.render.DeferredRenderLayer(ctx);
			this._opaqueLayer.settings = this.settings;
			this._opaqueLayer.createOpaque();

			this._transparentLayer = new bg.render.DeferredRenderLayer(ctx);
			this._transparentLayer.settings = this.settings;
			this._transparentLayer.createTransparent(this._opaqueLayer.maps);

			this._shadowMap = new bg.base.ShadowMap(ctx);
			this._shadowMap.size = new bg.Vector2(2048);

			this._opaqueLayer.shadowMap = this._shadowMap;
			this._transparentLayer.shadowMap = this._shadowMap;

			let mixSurface = new bg.base.TextureSurface(ctx);
			mixSurface.create();
			this._mixPipeline = new bg.base.Pipeline(ctx);
			this._mixPipeline.textureEffect = new bg.render.RendererMixEffect(ctx);
			this._mixPipeline.renderSurface = mixSurface;

			this._pipeline = new bg.base.Pipeline(ctx);
			this._pipeline.textureEffect = new bg.render.PostprocessEffect(ctx);
			
			this.settings.renderScale = this.settings.renderScale || 1.0;
		}

		draw(scene,camera) {
			if (this._shadowMap.size.x!=this.settings.shadows.quality) {
				this._shadowMap.size = new bg.Vector2(this.settings.shadows.quality);
			}
			
			let vp = camera.viewport;
			let aa = this.settings.antialiasing || {};
			let maxSize = aa.maxTextureSize || 4096;
			let ratio = vp.aspectRatio;
			let scaledWidth = vp.width;
			let scaledHeight = vp.height;
			if (aa.enabled) {
				scaledWidth = vp.width * 2;
				scaledHeight = vp.height * 2;
				if (ratio>1 && scaledWidth>maxSize) {	// Landscape
					scaledWidth = maxSize;
					scaledHeight = maxSize / ratio;
				}
				else if (scaledHeight>maxSize) {	// Portrait
					scaledHeight = maxSize;
					scaledWidth = maxSize * ratio;
				}
			}
			else if (true) {
				scaledWidth = vp.width * this.settings.renderScale;
				scaledHeight = vp.height * this.settings.renderScale;
				if (ratio>1 && scaledWidth>maxSize) {	// Landscape
					scaledWidth = maxSize;
					scaledHeight = maxSize / ratio;
				}
				else if (scaledHeight>maxSize) {	// Portrait
					scaledHeight = maxSize;
					scaledWidth = maxSize * ratio;
				}
			}

			let scaledViewport = new bg.Viewport(0,0,scaledWidth,scaledHeight);
			camera.viewport = scaledViewport;
			let mainLight = null;

			this._opaqueLayer.clearColor = this.clearColor;
			if (this._size.width!=camera.viewport.width ||
				this._size.height!=camera.viewport.height)
			{
				this._opaqueLayer.resize(camera);
				this._transparentLayer.resize(camera);
			}
			

			// Update render queue
			this._renderQueueVisitor.modelMatrixStack.identity();
			this._renderQueueVisitor.projectionMatrixStack.push();
			this._renderQueueVisitor.projectionMatrixStack.set(camera.projection);
			this._renderQueueVisitor.viewMatrixStack.set(camera.viewMatrix);
			this._renderQueueVisitor.renderQueue.beginFrame(camera.worldPosition);
			scene.accept(this._renderQueueVisitor);
			this._renderQueueVisitor.renderQueue.sortTransparentObjects();
			this._opaqueLayer.draw(this._renderQueueVisitor.renderQueue,scene,camera);
			this._transparentLayer.draw(this._renderQueueVisitor.renderQueue,scene,camera);
			this._renderQueueVisitor.projectionMatrixStack.pop();

			bg.base.Pipeline.SetCurrent(this._mixPipeline);
			this._mixPipeline.viewport = camera.viewport;
			this._mixPipeline.clearColor = bg.Color.Black();
			this._mixPipeline.clearBuffers();
			this._mixPipeline.drawTexture({
				opaque:this._opaqueLayer.texture,
				transparent:this._transparentLayer.texture,
				transparentNormal:this._transparentLayer.maps.normal,
				opaqueDepth:this._opaqueLayer.maps.position,
				transparentDepth:this._transparentLayer.maps.position
			});

			bg.base.Pipeline.SetCurrent(this.pipeline);
			this.pipeline.viewport = vp;
			this.pipeline.clearColor = this.clearColor;
			this.pipeline.clearBuffers();
			this.pipeline.drawTexture({
				texture: this._mixPipeline.renderSurface.getTexture(0)
			});
			camera.viewport = vp;
		}

		getImage(scene,camera,width,height) {
			let prevViewport = camera.viewport;
			camera.viewport = new bg.Viewport(0,0,width,height);
			this.draw(scene,camera);

			let canvas = document.createElement('canvas');
			canvas.width = width;
			canvas.height = height;
			let ctx = canvas.getContext('2d');

			let buffer = this.pipeline.renderSurface.readBuffer(new bg.Viewport(0,0,width,height));
			let imgData = ctx.createImageData(width,height);
			let len = width * 4;
			// Flip image data
			for (let i = 0; i<height; ++i) {
				for (let j = 0; j<len; j+=4) {
					let srcRow = i * width * 4;
					let dstRow = (height - i) * width * 4;
					imgData.data[dstRow + j + 0] = buffer[srcRow + j + 0];
					imgData.data[dstRow + j + 1] = buffer[srcRow + j + 1];
					imgData.data[dstRow + j + 2] = buffer[srcRow + j + 2];
					imgData.data[dstRow + j + 3] = buffer[srcRow + j + 3];
				}
			}
			ctx.putImageData(imgData,0,0);

			let img = canvas.toDataURL('image/jpeg');

			camera.viewport = prevViewport;
			this.viewport = prevViewport;
			this.draw(scene,camera);
			return img;
		}
	}

	bg.render.DeferredRenderer = DeferredRenderer;
})();
(function() {

	class ForwardRenderLayer extends bg.render.RenderLayer {		
		constructor(context,opacityLayer) {
			super(context,opacityLayer);
			
			this._pipeline.effect = new bg.base.ForwardEffect(context);
			this._pipeline.opacityLayer = opacityLayer;

			// one light source
			this._lightComponent = null;

			// Multiple light sources
			this._lightComponents = [];
	
			if (opacityLayer == bg.base.OpacityLayer.TRANSPARENT) {
				this._pipeline.buffersToClear = bg.base.ClearBuffers.NONE;
				this._pipeline.blend = true;
				this._pipeline.blendMode = bg.base.BlendMode.NORMAL;
			}
		}

		// Single light
		set lightComponent(light) { this._lightComponent = light; }
		get lightComponent() { return this._lightComponent; }

		// Multiple lights
		setLightSources(lightComponents) {
			this._lightComponents = lightComponents;
		}

		set shadowMap(sm) { this._shadowMap = sm; }
		get shadowMap() { return this._shadowMap; }
		
		draw(renderQueue,scene,camera) {
			// TODO: Why is this required to update the light transforms?
			let activeQueue = this._pipeline.opacityLayer==bg.base.OpacityLayer.OPAQUE ? renderQueue.opaqueQueue : renderQueue.transparentQueue;
			this.matrixState.modelMatrixStack.push();
			activeQueue.forEach((objectData) => {
				this.matrixState.modelMatrixStack.set(objectData.modelMatrix);
				this.matrixState.viewMatrixStack.set(objectData.viewMatrix);
			});
			this.matrixState.modelMatrixStack.pop();

			bg.base.Pipeline.SetCurrent(this._pipeline);
			this._pipeline.viewport = camera.viewport;
			
			if (camera.clearBuffers!=0) {
				this._pipeline.clearBuffers();
			}
		
			this.matrixState.projectionMatrixStack.set(camera.projection);

			this.willDraw(scene,camera);
			this.performDraw(renderQueue,scene,camera);
		}

		willDraw(scene,camera) {
			if (this._lightComponent) {
				this._pipeline.effect.light = this._lightComponent.light;
				this._pipeline.effect.lightTransform = this._lightComponent.transform;
				this._pipeline.effect.shadowMap = this._shadowMap;
			}
			else if (this._lightComponents) {
				this._pipeline.effect.lightArray.reset();
				this._lightComponents.forEach((comp) => {
					this._pipeline.effect.lightArray.push(comp.light,comp.transform);
				});
				this._pipeline.effect.shadowMap = this._shadowMap;
			}
		}

		performDraw(renderQueue,scene,camera) {
			this._pipeline.viewport = camera.viewport;

			let activeQueue = this._pipeline.opacityLayer==bg.base.OpacityLayer.OPAQUE ? renderQueue.opaqueQueue : renderQueue.transparentQueue;
			this.matrixState.modelMatrixStack.push();
			activeQueue.forEach((objectData) => {
				this.matrixState.modelMatrixStack.set(objectData.modelMatrix);
				this.matrixState.viewMatrixStack.set(objectData.viewMatrix);
				this._pipeline.effect.material = objectData.material;
				this._pipeline.draw(objectData.plist);
			});
			this.matrixState.modelMatrixStack.pop();
		}
	}
	
	bg.render.ForwardRenderLayer = ForwardRenderLayer;
	
})();

(function() {

	class ForwardRenderer extends bg.render.Renderer {
		constructor(context) {
			super(context);
		}

		get isSupported() { return true; }
		create() {
			let ctx = this.context;
			this._transparentLayer = new bg.render.ForwardRenderLayer(ctx,bg.base.OpacityLayer.TRANSPARENT);
			this._opaqueLayer = new bg.render.ForwardRenderLayer(ctx,bg.base.OpacityLayer.OPAQUE);
			this._shadowMap = new bg.base.ShadowMap(ctx);
			this._shadowMap.size = new bg.Vector2(2048);

			this.settings.shadows.cascade = bg.base.ShadowCascade.NEAR;

			this._renderQueueVisitor = new bg.scene.RenderQueueVisitor
		}

		draw(scene,camera) {
			let shadowLight = null;
			let lightSources = [];
			let enabledLights = 0;
			bg.scene.Light.GetActiveLights().some((lightComponent,index) => {
				if (lightComponent.light && lightComponent.light.enabled)
				{
					enabledLights++;
					lightSources.push(lightComponent);
					if (!shadowLight && lightComponent.light.type!=bg.base.LightType.POINT && lightComponent.light.castShadows) {
						shadowLight = lightComponent;
					}
				}
				return enabledLights>=bg.base.MAX_FORWARD_LIGHTS;
			});
			
			if (shadowLight) {
				if (this._shadowMap.size.x!=this.settings.shadows.quality) {
					this._shadowMap.size = new bg.Vector2(this.settings.shadows.quality);
				}
				this._shadowMap.update(scene,camera,shadowLight.light,shadowLight.transform,this.settings.shadows.cascade);
			}
			if (lightSources.length) {
				this._opaqueLayer.setLightSources(lightSources);
				this._opaqueLayer.shadowMap = this._shadowMap;

				this._transparentLayer.setLightSources(lightSources);
				this._transparentLayer.shadowMap = this._shadowMap;
			}

			// Update render queue
			this._renderQueueVisitor.projectionMatrixStack.set(camera.projection);
			this._renderQueueVisitor.modelMatrixStack.identity();
			this._renderQueueVisitor.viewMatrixStack.set(camera.viewMatrix);
			this._renderQueueVisitor.renderQueue.beginFrame(camera.worldPosition);
			scene.accept(this._renderQueueVisitor);
			this._renderQueueVisitor.renderQueue.sortTransparentObjects();

			this._opaqueLayer.pipeline.clearColor = this.clearColor;
			this._opaqueLayer.draw(this._renderQueueVisitor.renderQueue,scene,camera);
			this._transparentLayer.draw(this._renderQueueVisitor.renderQueue,scene,camera);
		}

		getImage(scene,camera,width,height) {
			let prevViewport = camera.viewport;
			camera.viewport = new bg.Viewport(0,0,width,height);
			this.draw(scene,camera);
			this.draw(scene,camera);

			let canvas = document.createElement('canvas');
			canvas.width = width;
			canvas.height = height;
			let ctx = canvas.getContext('2d');

			let buffer = this._opaqueLayer.pipeline.renderSurface.readBuffer(new bg.Viewport(0,0,width,height));
			let imgData = ctx.createImageData(width,height);
			let len = width * 4;
			// Flip image data
			for (let i = 0; i<height; ++i) {
				for (let j = 0; j<len; j+=4) {
					let srcRow = i * width * 4;
					let dstRow = (height - i) * width * 4;
					imgData.data[dstRow + j + 0] = buffer[srcRow + j + 0];
					imgData.data[dstRow + j + 1] = buffer[srcRow + j + 1];
					imgData.data[dstRow + j + 2] = buffer[srcRow + j + 2];
					imgData.data[dstRow + j + 3] = buffer[srcRow + j + 3];
				}
			}
			ctx.putImageData(imgData,0,0);

			let img = canvas.toDataURL('image/jpeg');

			camera.viewport = prevViewport;
			this._opaqueLayer.viewport = prevViewport;
			this._transparentLayer.viewport = prevViewport;
			return img;
		}
	}

	bg.render.ForwardRenderer = ForwardRenderer;

})();
(function() {

	let s_ubyteGbufferVertex = null;
	let s_ubyteGbufferFragment = null;
	let s_floatGbufferVertex = null;
	let s_floatGbufferFragment = null;
	
	function lib() {
		return bg.base.ShaderLibrary.Get();
	}

	let deferredShaders = {
		gbuffer_ubyte_vertex() {
			if (!s_ubyteGbufferVertex) {
				s_ubyteGbufferVertex = new bg.base.ShaderSource(bg.base.ShaderType.VERTEX);
				
				s_ubyteGbufferVertex.addParameter([
					lib().inputs.buffers.vertex,
					lib().inputs.buffers.normal,
					lib().inputs.buffers.tangent,
					lib().inputs.buffers.tex0,
					lib().inputs.buffers.tex1
				]);
				
				s_ubyteGbufferVertex.addParameter(lib().inputs.matrix.all);
				
				s_ubyteGbufferVertex.addParameter([
					{ name:"fsPosition", dataType:"vec3", role:"out" },
					{ name:"fsTex0Coord", dataType:"vec2", role:"out" },
					{ name:"fsTex1Coord", dataType:"vec2", role:"out" },
					{ name:"fsNormal", dataType:"vec3", role:"out" },
					{ name:"fsTangent", dataType:"vec3", role:"out" },
					{ name:"fsBitangent", dataType:"vec3", role:"out" }
				]);
				
				if (bg.Engine.Get().id=="webgl1") {
					s_ubyteGbufferVertex.setMainBody(`
					vec4 viewPos = inViewMatrix * inModelMatrix * vec4(inVertex,1.0);
					gl_Position = inProjectionMatrix * viewPos;
					
					fsNormal = normalize((inNormalMatrix  * vec4(inNormal,1.0)).xyz);
					fsTangent = normalize((inNormalMatrix * vec4(inTangent,1.0)).xyz);
					fsBitangent = cross(fsNormal,fsTangent);
					
					fsTex0Coord = inTex0;
					fsTex1Coord = inTex1;
					fsPosition = viewPos.xyz;`);
				}
			}
			return s_ubyteGbufferVertex;
		},
		
		gbuffer_ubyte_fragment() {
			if (!s_ubyteGbufferFragment) {
				s_ubyteGbufferFragment = new bg.base.ShaderSource(bg.base.ShaderType.FRAGMENT);
				
				s_ubyteGbufferFragment.appendHeader("#extension GL_EXT_draw_buffers : require");
				
				s_ubyteGbufferFragment.addParameter([
					{ name:"fsPosition", dataType:"vec3", role:"in" },
					{ name:"fsTex0Coord", dataType:"vec2", role:"in" },
					{ name:"fsTex1Coord", dataType:"vec2", role:"in" },
					{ name:"fsNormal", dataType:"vec3", role:"in" },
					{ name:"fsTangent", dataType:"vec3", role:"in" },
					{ name:"fsBitangent", dataType:"vec3", role:"int" }
				]);
				
				s_ubyteGbufferFragment.addParameter(lib().inputs.material.all);
				
				s_ubyteGbufferFragment.addFunction(lib().functions.materials.all);
				
				if (bg.Engine.Get().id=="webgl1") {
					s_ubyteGbufferFragment.setMainBody(`
						vec4 lightMap = samplerColor(inLightMap,fsTex1Coord,inLightMapOffset,inLightMapScale);
						vec4 diffuse = samplerColor(inTexture,fsTex0Coord,inTextureOffset,inTextureScale) * inDiffuseColor * lightMap;
						if (diffuse.a>=inAlphaCutoff) {
							vec3 normal = samplerNormal(inNormalMap,fsTex0Coord,inNormalMapOffset,inNormalMapScale);
							normal = combineNormalWithMap(fsNormal,fsTangent,fsBitangent,normal);
							vec4 specular = specularColor(inSpecularColor,inShininessMask,fsTex0Coord,inTextureOffset,inTextureScale,
															inShininessMaskChannel,inShininessMaskInvert);
							float lightEmission = applyTextureMask(inLightEmission,
															inLightEmissionMask,fsTex0Coord,inTextureOffset,inTextureScale,
															inLightEmissionMaskChannel,inLightEmissionMaskInvert);
							
							float reflectionMask = applyTextureMask(inReflection,
															inReflectionMask,fsTex0Coord,inTextureOffset,inTextureScale,
															inReflectionMaskChannel,inReflectionMaskInvert);
							
							float roughnessMask = applyTextureMask(inRoughness,
															inRoughnessMask,fsTex0Coord,inTextureOffset,inTextureScale,
															inRoughnessMaskChannel,inRoughnessMaskInvert);

							gl_FragData[0] = diffuse;
							gl_FragData[1] = vec4(specular.rgb,roughnessMask); // Store roughness on A component of specular
							if (!gl_FrontFacing) {	// Flip the normal if back face
								normal *= -1.0;
							}
							gl_FragData[2] = vec4(normal * 0.5 + 0.5, inUnlit ? 0.0 : 1.0);	// Store !unlit parameter on A component of normal
							gl_FragData[3] = vec4(lightEmission,inShininess/255.0,reflectionMask,float(inCastShadows));
						}
						else {
							gl_FragData[0] = vec4(0.0);
							gl_FragData[1] = vec4(0.0);
							gl_FragData[2] = vec4(0.0);
							gl_FragData[3] = vec4(0.0);
							discard;
						}`);
				}
			}
			return s_ubyteGbufferFragment;
		},
		
		gbuffer_float_vertex() {
			if (!s_floatGbufferVertex) {
				s_floatGbufferVertex = new bg.base.ShaderSource(bg.base.ShaderType.VERTEX);
				
				s_floatGbufferVertex.addParameter([
					lib().inputs.buffers.vertex,
					lib().inputs.buffers.tex0,
					null,
					lib().inputs.matrix.model,
					lib().inputs.matrix.view,
					lib().inputs.matrix.projection,
					null,
					{ name:"fsPosition", dataType:"vec4", role:"out" },
					{ name:"fsTex0Coord", dataType:"vec2", role:"out" }
				]);
				
				if (bg.Engine.Get().id=="webgl1") {
					s_floatGbufferVertex.setMainBody(`
					fsPosition = inViewMatrix * inModelMatrix * vec4(inVertex,1.0);
					fsTex0Coord = inTex0;
					
					gl_Position = inProjectionMatrix * fsPosition;`);
				}
			}
			return s_floatGbufferVertex;
		},
		
		gbuffer_float_fragment() {
			if (!s_floatGbufferFragment) {
				s_floatGbufferFragment = new bg.base.ShaderSource(bg.base.ShaderType.FRAGMENT);
				
				s_floatGbufferFragment.addParameter([
					lib().inputs.material.texture,
					lib().inputs.material.textureScale,
					lib().inputs.material.textureOffset,
					lib().inputs.material.alphaCutoff,
					null,
					{ name:"fsPosition", dataType:"vec4", role:"in" },
					{ name:"fsTex0Coord", dataType:"vec2", role:"in" }
				]);
				
				s_floatGbufferFragment.addFunction(lib().functions.materials.samplerColor);
				
				if (bg.Engine.Get().id=="webgl1") {
					s_floatGbufferFragment.setMainBody(`
					float alpha = samplerColor(inTexture,fsTex0Coord,inTextureOffset,inTextureScale).a;
					if (alpha<inAlphaCutoff) {
						discard;
					}
					else {
						gl_FragColor = vec4(fsPosition.xyz,gl_FragCoord.z);
					}
					`)
				}
			}
			return s_floatGbufferFragment;
		}
	}
	
	
	class GBufferEffect extends bg.base.Effect {
		constructor(context) { 
			super(context);
			
			let ubyte_shaders = [
				deferredShaders.gbuffer_ubyte_vertex(),
				deferredShaders.gbuffer_ubyte_fragment()
			];
			
			this._material = null;
			this.setupShaderSource(ubyte_shaders);
		}
		
		get material() { return this._material; }
		set material(m) { this._material = m; }
		
		setupVars() {
			if (this.material) {
				// Matrix state
				let matrixState = bg.base.MatrixState.Current();
				let viewMatrix = new bg.Matrix4(matrixState.viewMatrixStack.matrixConst);
				this.shader.setMatrix4('inModelMatrix',matrixState.modelMatrixStack.matrixConst);
				this.shader.setMatrix4('inViewMatrix',viewMatrix);
				this.shader.setMatrix4('inProjectionMatrix',matrixState.projectionMatrixStack.matrixConst);
				this.shader.setMatrix4('inNormalMatrix',matrixState.normalMatrix);
				this.shader.setMatrix4('inViewMatrixInv',matrixState.viewMatrixInvert);
				
				// Material
				let texture = this.material.texture || bg.base.TextureCache.WhiteTexture(this.context);//s_whiteTexture;
				let lightMap = this.material.lightmap || bg.base.TextureCache.WhiteTexture(this.context);//s_whiteTexture;
				let normalMap = this.material.normalMap || bg.base.TextureCache.NormalTexture(this.context);//s_normalTexture;
				this.shader.setVector4('inDiffuseColor',this.material.diffuse);
				this.shader.setVector4('inSpecularColor',this.material.specular);
				this.shader.setValueFloat('inShininess',this.material.shininess);
				this.shader.setValueFloat('inLightEmission',this.material.lightEmission);
				this.shader.setValueFloat('inAlphaCutoff',this.material.alphaCutoff);
				
				this.shader.setTexture('inTexture',texture,bg.base.TextureUnit.TEXTURE_0);
				this.shader.setVector2('inTextureOffset',this.material.textureOffset);
				this.shader.setVector2('inTextureScale',this.material.textureScale);
				
				this.shader.setTexture('inLightMap',lightMap,bg.base.TextureUnit.TEXTURE_1);
				this.shader.setVector2('inLightMapOffset',this.material.lightmapOffset);
				this.shader.setVector2('inLightMapScale',this.material.lightmapScale);
				
				this.shader.setTexture('inNormalMap',normalMap,bg.base.TextureUnit.TEXTURE_2);
				this.shader.setVector2('inNormalMapScale',this.material.normalMapScale);
				this.shader.setVector2('inNormalMapOffset',this.material.normalMapOffset);
				
				let shininessMask = this.material.shininessMask || bg.base.TextureCache.WhiteTexture(this.context);
				let lightEmissionMask = this.material.lightEmissionMask || bg.base.TextureCache.WhiteTexture(this.context);
				let reflectionMask = this.material.reflectionMask || bg.base.TextureCache.WhiteTexture(this.context);
				let roughnessMask = this.material.roughnessMask || bg.base.TextureCache.WhiteTexture(this.context);
				this.shader.setTexture('inShininessMask',shininessMask,bg.base.TextureUnit.TEXTURE_3);
				this.shader.setVector4('inShininessMaskChannel',this.material.shininessMaskChannelVector);
				this.shader.setValueInt('inShininessMaskInvert',this.material.shininessMaskInvert);

				this.shader.setTexture('inLightEmissionMask',lightEmissionMask,bg.base.TextureUnit.TEXTURE_4);
				this.shader.setVector4('inLightEmissionMaskChannel',this.material.lightEmissionMaskChannelVector);
				this.shader.setValueInt('inLightEmissionMaskInvert',this.material.lightEmissionMaskInvert);
				
				this.shader.setValueFloat('inReflection',this.material.reflectionAmount);
				this.shader.setTexture('inReflectionMask',reflectionMask,bg.base.TextureUnit.TEXTURE_5);
				this.shader.setVector4('inReflectionMaskChannel',this.material.reflectionMaskChannelVector);
				this.shader.setValueInt('inReflectionMaskInvert',this.material.reflectionMaskInvert);

				this.shader.setValueFloat('inRoughness',this.material.roughness);
				this.shader.setTexture('inRoughnessMask',roughnessMask,bg.base.TextureUnit.TEXTURE_6);
				this.shader.setVector4('inRoughnessMaskChannel',this.material.roughnessMaskChannelVector);
				this.shader.setValueInt('inRoughnessMaskInvert',this.material.roughnessMaskInvert);
				
				this.shader.setValueInt('inCastShadows',this.material.castShadows);

				this.shader.setValueInt('inUnlit',this.material.unlit);
				// Other settings
				//this.shader.setValueInt('inSelectMode',false);
			}
		}
	}
	
	class PositionGBufferEffect extends bg.base.Effect {
		constructor(context) { 
			super(context);
			let ubyte_shaders = [
				deferredShaders.gbuffer_float_vertex(),
				deferredShaders.gbuffer_float_fragment()
			];
			
			this._material = null;
			this.setupShaderSource(ubyte_shaders);
		}
		
		get material() { return this._material; }
		set material(m) { this._material = m; }

		setupVars() {
			if (this.material) {
				// Matrix state
				let matrixState = bg.base.MatrixState.Current();
				let viewMatrix = new bg.Matrix4(matrixState.viewMatrixStack.matrixConst);
				this.shader.setMatrix4('inModelMatrix',matrixState.modelMatrixStack.matrixConst);
				this.shader.setMatrix4('inViewMatrix',viewMatrix);
				this.shader.setMatrix4('inProjectionMatrix',matrixState.projectionMatrixStack.matrixConst);
				
				// Material
				let texture = this.material.texture || bg.base.TextureCache.WhiteTexture(this.context);//s_whiteTexture;
				this.shader.setTexture('inTexture',texture,bg.base.TextureUnit.TEXTURE_0);
				this.shader.setVector2('inTextureOffset',this.material.textureOffset);
				this.shader.setVector2('inTextureScale',this.material.textureScale);
				this.shader.setValueFloat('inAlphaCutoff',this.material.alphaCutoff);
			}
		}
	}
	
	bg.render.PositionGBufferEffect = PositionGBufferEffect;
	bg.render.GBufferEffect = GBufferEffect;
})();

(function() {

	function updatePipeline(pipeline,drawVisitor,scene,camera) {
		bg.base.Pipeline.SetCurrent(pipeline);
		pipeline.viewport = camera.viewport;
		pipeline.clearBuffers(bg.base.ClearBuffers.COLOR | bg.base.ClearBuffers.DEPTH);
		scene.accept(drawVisitor);
	}
	
	class GBufferSet extends bg.app.ContextObject {
		constructor(context) {
			super(context);
			
			this._pipelines = {
				ubyte: new bg.base.Pipeline(context),
				float: new bg.base.Pipeline(context)
			};
			
			// Create two g-buffer render pipelines:
			//	- ubyte pipeline: to generate unsigned byte g-buffers (diffuse, material properties, etc)
			//	- float pipeline: to generate float g-buffers (currently, only needed to generate the position g-buffer)
 			let ubyteRS = new bg.base.TextureSurface(context);
			ubyteRS.create([
				{ type:bg.base.RenderSurfaceType.RGBA, format:bg.base.RenderSurfaceFormat.UNSIGNED_BYTE },
				{ type:bg.base.RenderSurfaceType.RGBA, format:bg.base.RenderSurfaceFormat.UNSIGNED_BYTE },
				{ type:bg.base.RenderSurfaceType.RGBA, format:bg.base.RenderSurfaceFormat.UNSIGNED_BYTE },
				{ type:bg.base.RenderSurfaceType.RGBA, format:bg.base.RenderSurfaceFormat.UNSIGNED_BYTE },
				{ type:bg.base.RenderSurfaceType.RGBA, format:bg.base.RenderSurfaceFormat.UNSIGNED_BYTE },
				{ type:bg.base.RenderSurfaceType.DEPTH, format:bg.base.RenderSurfaceFormat.RENDERBUFFER }
			]);
			this._pipelines.ubyte.effect = new bg.render.GBufferEffect(context);
			this._pipelines.ubyte.renderSurface = ubyteRS;

			let floatRS = new bg.base.TextureSurface(context);
			floatRS.create([
				{ type:bg.base.RenderSurfaceType.RGBA, format:bg.base.RenderSurfaceFormat.FLOAT },
				{ type:bg.base.RenderSurfaceType.DEPTH, format:bg.base.RenderSurfaceFormat.RENDERBUFFER }
			]);
			this._pipelines.float.effect = new bg.render.PositionGBufferEffect(context);
			this._pipelines.float.renderSurface = floatRS;
					
			this._ubyteDrawVisitor = new bg.scene.DrawVisitor(this._pipelines.ubyte,this._matrixState);
			this._floatDrawVisitor = new bg.scene.DrawVisitor(this._pipelines.float,this._matrixState);
		}
		
		get diffuse() { return this._pipelines.ubyte.renderSurface.getTexture(0); }
		get specular() { return this._pipelines.ubyte.renderSurface.getTexture(1); }
		get normal() { return this._pipelines.ubyte.renderSurface.getTexture(2); }
		get material() { return this._pipelines.ubyte.renderSurface.getTexture(3); }
		get position() { return this._pipelines.float.renderSurface.getTexture(0); }
		get shadow() { return this._pipelines.ubyte.renderSurface.getTexture(4); }
		
		update(sceneRoot,camera) {
			updatePipeline(this._pipelines.ubyte,this._ubyteDrawVisitor,sceneRoot,camera);
			updatePipeline(this._pipelines.float,this._floatDrawVisitor,sceneRoot,camera);
		}
	}
	
	bg.render.GBufferSet = GBufferSet;
	
})();
(function() {
	function lib() {
		return bg.base.ShaderLibrary.Get();
	}

	class LightingEffect extends bg.base.TextureEffect {
		constructor(context) {
			super(context);
		}
		
		get fragmentShaderSource() {
			if (!this._fragmentShaderSource) {
				this._fragmentShaderSource = new bg.base.ShaderSource(bg.base.ShaderType.FRAGMENT);
				this._fragmentShaderSource.appendHeader("#extension GL_EXT_draw_buffers : require");

				this._fragmentShaderSource.addParameter([
					{ name:"inDiffuse", dataType:"sampler2D", role:"value"},
					{ name:"inSpecular", dataType:"sampler2D", role:"value"},
					{ name:"inNormal", dataType:"sampler2D", role:"value"},
					{ name:"inMaterial", dataType:"sampler2D", role:"value"},
					{ name:"inPosition", dataType:"sampler2D", role:"value"},
					{ name:"inShadowMap", dataType:"sampler2D", role:"value" },
					{ name:"inLightEmissionFactor", dataType:"float", role:"value" },
					{ name:"fsTexCoord", dataType:"vec2", role:"in" }
				]);
				this._fragmentShaderSource.addParameter(lib().inputs.lighting.all);
				this._fragmentShaderSource.addFunction(lib().functions.utils.all);
				this._fragmentShaderSource.addFunction(lib().functions.lighting.all);
				
				if (bg.Engine.Get().id=="webgl1") {
					this._fragmentShaderSource.setMainBody(`
					vec4 diffuse = texture2D(inDiffuse,fsTexCoord);
					vec4 specular = vec4(texture2D(inSpecular,fsTexCoord).rgb,1.0);
					vec4 normalTex = texture2D(inNormal,fsTexCoord);
					vec3 normal = normalTex.xyz * 2.0 - 1.0;
					vec4 material = texture2D(inMaterial,fsTexCoord);
					vec4 position = texture2D(inPosition,fsTexCoord);
					
					vec4 shadowColor = texture2D(inShadowMap,fsTexCoord);
					float shininess = material.g * 255.0;
					float lightEmission = material.r;
					bool unlit = normalTex.a == 0.0;
					vec4 specularColor = vec4(0.0,0.0,0.0,1.0);
					if (unlit) {
						gl_FragData[0] = vec4(diffuse.rgb * min(inLightEmissionFactor,1.0),1.0);
					}
					else {
						vec4 light = getLight(
							inLightType,
							inLightAmbient, inLightDiffuse, inLightSpecular,shininess,
							inLightPosition,inLightDirection,
							inLightAttenuation.x,inLightAttenuation.y,inLightAttenuation.z,
							inSpotCutoff,inSpotExponent,inLightCutoffDistance,
							position.rgb,normal,
							diffuse,specular,shadowColor,
							specularColor
						);
						light.rgb = light.rgb + (lightEmission * diffuse.rgb * inLightEmissionFactor);
						gl_FragData[0] = light;
						gl_FragData[1] = specularColor;
						gl_FragData[2] = vec4((light.rgb - vec3(1.0,1.0,1.0)) * 2.0,1.0);
					}
					`);
				}
			}
			return this._fragmentShaderSource;
		}
		
		setupVars() {
			this.shader.setTexture("inDiffuse",this._surface.diffuse,bg.base.TextureUnit.TEXTURE_0);
			this.shader.setTexture("inSpecular",this._surface.specular,bg.base.TextureUnit.TEXTURE_1);
			this.shader.setTexture("inNormal",this._surface.normal,bg.base.TextureUnit.TEXTURE_2);
			this.shader.setTexture("inMaterial",this._surface.material,bg.base.TextureUnit.TEXTURE_3);
			this.shader.setTexture("inPosition",this._surface.position,bg.base.TextureUnit.TEXTURE_4);
			this.shader.setTexture("inShadowMap",this._shadowMap,bg.base.TextureUnit.TEXTURE_5);
		
			if (this.light) {
				let matrixState = bg.base.MatrixState.Current();
				let viewMatrix = new bg.Matrix4(matrixState.viewMatrixStack.matrixConst);
				
				this.shader.setVector4('inLightAmbient',this._light.ambient);
				this.shader.setVector4('inLightDiffuse',this._light.diffuse);
				this.shader.setVector4('inLightSpecular',this._light.specular);
				this.shader.setValueInt('inLightType',this._light.type);
				this.shader.setVector3('inLightAttenuation',this._light.attenuationVector);
				this.shader.setValueFloat('inLightEmissionFactor',this.lightEmissionFactor);
				this.shader.setValueFloat('inLightCutoffDistance',this._light.cutoffDistance);

				
				let dir = viewMatrix
								.mult(this._lightTransform)
								.rotation
								.multVector(this._light.direction)
								.xyz;
				let pos = viewMatrix
								//.mult(this._lightTransform)
								.position;
				this.shader.setVector3('inLightDirection',dir);
				this.shader.setVector3('inLightPosition',pos);
				this.shader.setValueFloat('inSpotCutoff',this._light.spotCutoff);
				this.shader.setValueFloat('inSpotExponent',this._light.spotExponent);
				// TODO: Type and other light properties.
			}
		}
		
		get lightEmissionFactor() { return this._lightEmissionFactor; }
		set lightEmissionFactor(f) { this._lightEmissionFactor = f; }
		
		get light() { return this._light; }
		set light(l) { this._light = l; }
		
		get lightTransform() { return this._lightTransform; }
		set lightTransform(t) { this._lightTransform = t; }
		
		get shadowMap() { return this._shadowMap; }
		set shadowMap(sm) { this._shadowMap = sm; }
	}
	
	bg.render.LightingEffect = LightingEffect;
})();
(function() {
    
    let shaders = {};

    function lib() {
        return bg.base.ShaderLibrary.Get();
    }

    let s_vertexSource = null;
    // One shader source for each number of lights
    let s_fragmentSources = [];
    let s_supportedTextureUnits = 0;

    function vertexShaderSource() {
        if (!s_vertexSource) {
            s_vertexSource = new bg.base.ShaderSource(bg.base.ShaderType.VERTEX);

            s_vertexSource.addParameter([
                lib().inputs.buffers.vertex,
                lib().inputs.buffers.normal,
				lib().inputs.buffers.tangent,
				lib().inputs.buffers.tex0,
				lib().inputs.buffers.tex1
            ]);

            s_vertexSource.addParameter(lib().inputs.matrix.all);

            s_vertexSource.addParameter([
                { name:"inLightProjectionMatrix", dataType:"mat4", role:"value" },
                { name:"inLightViewMatrix", dataType:"mat4", role:"value" }
            ])

            s_vertexSource.addParameter([
                { name:"fsTex0Coord", dataType:"vec2", role:"out" },
                { name:"fsTex1Coord", dataType:"vec2", role:"out" },
                { name:"fsNormal", dataType:"vec3", role:"out" },
                { name:"fsTangent", dataType:"vec3", role:"out" },
                { name:"fsBitangent", dataType:"vec3", role:"out" },
                { name:"fsPosition", dataType:"vec3", role:"out" },
                { name:"fsVertexPosFromLight", dataType:"vec4", role:"out" },
                { name:"fsTangentViewPos", dataType:"vec3", role:"out" },
                { name:"fsTangentFragPos", dataType:"vec3", role:"out" }
            ]);

            if (bg.Engine.Get().id=="webgl1") {
                s_vertexSource.setMainBody(`
                    mat4 ScaleMatrix = mat4(0.5, 0.0, 0.0, 0.0,
                        0.0, 0.5, 0.0, 0.0,
                        0.0, 0.0, 0.5, 0.0,
                        0.5, 0.5, 0.5, 1.0);

                    vec4 viewPos = inViewMatrix * inModelMatrix * vec4(inVertex,1.0);
                    gl_Position = inProjectionMatrix * viewPos;

                    fsNormal = normalize((inNormalMatrix * vec4(inNormal,1.0)).xyz);
                    fsTangent = normalize((inNormalMatrix * vec4(inTangent,1.0)).xyz);
                    fsBitangent = cross(fsNormal,fsTangent);

                    fsVertexPosFromLight = ScaleMatrix * inLightProjectionMatrix * inLightViewMatrix * inModelMatrix * vec4(inVertex,1.0);

                    fsTex0Coord = inTex0;
                    fsTex1Coord = inTex1;
                    fsPosition = viewPos.rgb;

                    mat3 TBN = mat3(
                        fsTangent.x, fsBitangent.x, fsNormal.x,
                        fsTangent.y, fsBitangent.y, fsNormal.y,
                        fsTangent.z, fsBitangent.z, fsNormal.z
                    );
                    fsTangentViewPos = TBN * vec3(0.0); // Using view space, the view position is at 0,0,0
                    fsTangentFragPos = TBN * fsPosition;
                `)
            }
        }
        return s_vertexSource;
    }

    function fragmentShaderSource(numLights) {
        if (!s_fragmentSources[numLights - 1] && numLights>0) {
            s_fragmentSources[numLights - 1] = new bg.base.ShaderSource(bg.base.ShaderType.FRAGMENT);
            let fragSrc = s_fragmentSources[numLights - 1];

            fragSrc.addParameter(lib().inputs.pbr.material.all);
            fragSrc.addParameter(lib().inputs.pbr.lightingForward.all);
            fragSrc.addParameter(lib().inputs.pbr.shadows.all);
            
            fragSrc.addParameter([
                { name:"fsTex0Coord", dataType:"vec2", role:"in" },
                { name:"fsTex1Coord", dataType:"vec2", role:"in" },
                { name:"fsNormal", dataType:"vec3", role:"in" },
                { name:"fsTangent", dataType:"vec3", role:"in" },
                { name:"fsBitangent", dataType:"vec3", role:"in" },
                { name:"fsPosition", dataType:"vec3", role:"in" },
                { name:"fsVertexPosFromLight", dataType:"vec4", role:"in" },

                { name:"fsTangentViewPos", dataType:"vec3", role:"in" },
                { name:"fsTangentFragPos", dataType:"vec3", role:"in" },

                { name:"inGammaCorrection", dataType:"float", role:"value" },
                { name:"inIrradianceMapIntensity", dataType:"float", role:"value" },
                { name:"inIrradianceMap", dataType:"samplerCube", role:"value" },
                { name:"inSpecularMap0", dataType:"samplerCube", role:"value" },
                { name:"inSpecularMap1", dataType:"samplerCube", role:"value" },
                { name:"inSpecularMap2", dataType:"samplerCube", role:"value" },
                { name:"inBRDF", dataType:"sampler2D", role:"value" },
                { name:"inViewMatrix", dataType:"mat4", role:"value" },

                { name:"inShadowLightDirection", dataType:"vec3", role:"value" },
                { name:"inShadowLightIndex", dataType:"int", role:"value" }
            ]);

            fragSrc.addFunction(lib().functions.pbr.material.all);
            fragSrc.addFunction(lib().functions.pbr.utils.unpack);
            fragSrc.addFunction(lib().functions.pbr.utils.pack);
            fragSrc.addFunction(lib().functions.pbr.utils.random);
            fragSrc.addFunction(lib().functions.pbr.utils.gammaCorrection);
            fragSrc.addFunction(lib().functions.pbr.utils.inverseGammaCorrection);
            fragSrc.addFunction(lib().functions.pbr.lighting.all);

            if (bg.Engine.Get().id=="webgl1") {


                // PBR test: utility functions
                fragSrc.addFunction({
                    returnType: "vec3", name:"fresnelSchlick", params: {
                        cosTheta:"float", F0:"vec3"
                    }, body: `
                    return max(F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0), 0.0);
                    `
                });

                fragSrc.addFunction({
                    returnType: "vec3", name:"fresnelSchlickRoughness", params: {
                        cosTheta:"float", F0:"vec3", roughness:"float"
                    }, body: `
                    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
                    `
                });

                fragSrc.addFunction({
                    returnType:"float", name:"distributionGGX", params:{
                        N:"vec3",
                        H:"vec3",
                        roughness:"float"
                    }, body:`
                    float a = roughness * roughness;
                    float a2 = a * a;
                    float NdotH = max(dot(N,H), 0.0);
                    float NdotH2 = NdotH * NdotH;

                    float num = a2;
                    float denom = (NdotH2 * (a2 - 1.0) +  1.0);
                    denom = ${ Math.PI } * denom * denom;

                    return num / denom;
                    `
                });

                fragSrc.addFunction({
                    returnType:"float", name:"geometrySchlickGGX", params: {
                        NdotV:"float",
                        roughness:"float"
                    }, body: `
                    float r = (roughness + 1.0);
                    float k = (r*r) / 8.0;
                    return NdotV / NdotV * (1.0 - k) + k;
                    `
                });

                fragSrc.addFunction({
                    returnType:"float", name:"geometrySmith", params: {
                        N:"vec3",
                        V:"vec3",
                        L:"vec3",
                        roughness:"float"
                    }, body:`
                    float NdotV = dot(N, V);
                    float NdotL = dot(N, L);
                    float ggx2 = geometrySchlickGGX(NdotV, roughness);
                    float ggx1 = geometrySchlickGGX(NdotL, roughness);

                    return ggx1 * ggx2;
                    `
                });


                let maxReflectionLod = 4;
                let irradianceBody = `
                vec3 N = normalize(normal);
                vec3 V = normalize(camPos - worldPos);
                roughness = max(roughness,0.01);    // Prevent some artifacts

                vec3 F0 = vec3(0.04); 
                F0 = mix(F0, albedo, metallic);

                // reflectance equation
                vec3 Lo = vec3(0.0);
                for(int i = 0; i < ${ numLights}; ++i) 
                {
                    // calculate per-light radiance
                    vec3 L = vec3(0.0);
                    vec3 H = vec3(0.0);

                    float intensity = 1.0;
                    if (inLightType[i]==${ bg.base.LightType.POINT}) {
                        L = normalize(inLightPosition[i] - worldPos);
                        H = normalize(V + L);
                    }
                    else if (inLightType[i]==${ bg.base.LightType.SPOT}) {
                        float theta = dot(normalize(inLightPosition[i] - worldPos),normalize(-inLightDirection[i]));
                        if (theta > inLightSpotCutoff[i]) {
                            L = normalize(-inLightDirection[i]);
                            H = normalize(V + L);
                            float epsilon = inLightSpotCutoff[i] - inLightOuterSpotCutoff[i];
                            intensity = 1.0 - clamp((theta - inLightOuterSpotCutoff[i]) / epsilon, 0.0, 1.0);
                        }
                        else {
                            //Lo += inLightAmbient[i].rgb * albedo;
                            if (i==inShadowLightIndex) {
                                shadowColor = vec3(1.0);
                            }
                            else {
                                shadowColor *= vec3(1.0/${ numLights }.0 );
                            } 
                            
                            continue;
                        }
                    }
                    else if (inLightType[i]==${ bg.base.LightType.DIRECTIONAL}) {
                        L = normalize(-inLightDirection[i]);
                        H = normalize(V + L);
                    }
                    
                    float distance    = length(inLightPosition[i] - worldPos);
                    float attenuation = 1.0 / distance * distance;
                    vec3 radiance     = inLightDiffuse[i].rgb * attenuation * inLightIntensity[i];
                    
                    // cook-torrance brdf
                    float NDF = distributionGGX(N, H, roughness);        
                    float G   = geometrySmith(N, V, L, roughness);

                    vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);
                    
                    vec3 kS = F;
                    vec3 kD = vec3(1.0) - kS;
                    kD *= 1.0 - metallic;	  
                    
                    vec3 numerator    = NDF * G * F;
                    float denominator = 4.0 * max(dot(N, V), 0.4) * max(dot(N, L), 0.4);
                    vec3 specular     = numerator / max(denominator, 0.0001) * pow(shadowColor,vec3(30.0)) * inLightIntensity[i];

                    // add to outgoing radiance Lo
                    float NdotL = max(dot(N, L), 0.0);
                    vec3 color = (kD * albedo / ${ Math.PI } + specular) * radiance * NdotL * intensity;
                    // vec3 ambient = inLightAmbient[i].rgb * albedo;
                    // vec3 shadowAmbient = clamp(shadowColor,ambient,vec3(1.0));
                    // color = min(color,shadowAmbient);
                    Lo += clamp(color,0.0,1.0);
                } 
                vec3 kS = fresnelSchlickRoughness(max(dot(N,V), 0.001), F0, roughness);

                vec3 kD = 1.0 - kS;
                kD *= 1.0 - metallic;

                vec3 R = reflect(worldPos - camPos, N);

                vec3 specMap0 = textureCube(inSpecularMap0,R).rgb;
                vec3 specMap1 = textureCube(inSpecularMap1,R).rgb;
                vec3 specMap2 = textureCube(${ s_supportedTextureUnits>8 ? "inSpecularMap2" : "inSpecularMap1" },R).rgb;

                vec3 prefilteredColor = vec3(0.0);
                if (roughness<${ s_supportedTextureUnits>8 ? 0.333 : 0.5 }) {
                    prefilteredColor = mix(specMap0,specMap1,roughness/${ s_supportedTextureUnits>8 ? 0.333 : 0.5 });
                }
                else {
                    prefilteredColor = mix(specMap1,specMap2,(roughness - ${ s_supportedTextureUnits>8 ? 0.333 : 0.5 }) / ${ s_supportedTextureUnits>8 ? 0.666 : 0.5 });
                }
            
                vec2 envBRDF = texture2D(inBRDF, vec2(min(max(dot(N,V), 0.0), 0.99), min(roughness,0.99))).rg;
                vec3 specular = prefilteredColor * (F0 * envBRDF.x + envBRDF.y) * shadowColor;

                vec3 irradiance = textureCube(inIrradianceMap,N).rgb * inIrradianceMapIntensity;
	            vec3 diffuse = (irradiance * 1.0/shadowColor) * (albedo * shadowColor);// * shadowColor;
	            vec3 irradianceAmbient = (kD * diffuse + specular); // TODO: * ao;
            
                return vec4(irradianceAmbient + Lo, 1.0);
                `;
                fragSrc.addFunction({
                    returnType:"vec4", name:"irradiance", params: {
                        normal:"vec3",
                        camPos:"vec3",
                        albedo:"vec3",
                        worldPos:"vec3",
                        metallic:"float",
                        roughness:"float",
                        shadowColor:"vec3",
                        texCoord:"vec2"
                    }, body: irradianceBody
                })

                fragSrc.setMainBody(`
                    // Shader for ${ numLights } lights 
                    vec3 viewDir = normalize(fsTangentFragPos - fsTangentViewPos);
                    float height = samplerColor(inHeighMetallicRoughnessAO,fsTex0Coord,inDiffuseOffset,inDiffuseScale).x;
                    vec2 texCoords = parallaxMapping(-height,fsTex0Coord, -viewDir, inHeightScale, inDiffuseScale);

                    vec4 hmrao = samplerColor(inHeighMetallicRoughnessAO,texCoords,inDiffuseOffset,inDiffuseScale);
                    float metallic = hmrao.y;
                    float roughness = hmrao.z;

                    vec4 diffuse = inverseGammaCorrection(samplerColor(inDiffuse,texCoords,inDiffuseOffset,inDiffuseScale),inGammaCorrection);
                    if (diffuse.a>inAlphaCutoff) {
                        vec3 normalMap = samplerNormal(inNormalMap,texCoords,inNormalOffset,inNormalScale);
                        vec3 frontFacingNormal = fsNormal;
                        // There is a bug on Mac Intel GPUs that produces an invalid value of gl_FrontFacing
                        //if (!gl_FrontFacing) {
                            //frontFacingNormal *= -1.0;
                        //}
    
                        vec3 combinedNormal = combineNormalWithMap(frontFacingNormal,fsTangent,fsBitangent,normalMap);   
    
                        vec4 shadowColor = vec4(1.0);
                        if (inReceiveShadows) {
                            shadowColor = getShadowColor(fsVertexPosFromLight,inShadowMap,inShadowMapSize,inShadowType,inShadowStrength,inShadowBias,inShadowColor);
                        }
    
                        vec4 lighting = irradiance(combinedNormal,vec3(0.0),diffuse.rgb,fsPosition,metallic,roughness,shadowColor.rgb,texCoords);
    
                        vec4 result = lighting;// * diffuse;
    
                        gl_FragColor = gammaCorrection(result,inGammaCorrection);
                    }
                    else {
                        discard;
                    }
                `);
            }
        }
        return s_fragmentSources[numLights - 1];
    }

    let s_brdfPrecomputedTextureLoad = false;
    let s_brdfPrecomputedTexture = null;


    class PBRForwardEffect extends bg.base.Effect {
        constructor(context) {
            super(context);
            this._material = null;
            s_supportedTextureUnits = context.getParameter(context.MAX_TEXTURE_IMAGE_UNITS);
            if (s_supportedTextureUnits<8) {
                throw new Error("Could not use PBR materials: not enought texture units available");
            }

            this._light = null;
            this._lightTransform = bg.Matrix4.Identity();

            this._lightArray = new bg.base.LightArray();

            this._shadowMap = null;

            // Generate one shader for each number of lights. Each shader have 
            // a fixed number of lights sources, to avoid use loops and conditional
            // instructions inside the shader
            for (let i = 1; i<=bg.base.MAX_FORWARD_LIGHTS; ++i) {
                this.setupShaderSource([
                    vertexShaderSource(),
                    fragmentShaderSource(i)
                ]);
            }

            if (!s_brdfPrecomputedTextureLoad) {
                s_brdfPrecomputedTextureLoad = true;
                //s_brdfPrecomputedTexture = bg.base.TextureCache.WhiteTexture(context);
                s_brdfPrecomputedTexture = bg.base.TextureCache.PrecomputedBRDFLookupTexture(context);
                // bg.base.Loader.Load(this.context,"../data/ibl_brdf_lut.png")
                //     .then((texture) => {
                //         s_brdfPrecomputedTexture = texture;
                //     })

                //     .catch((err) => {
                //         console.error(err.message);
                //     })
            }
        
        }

        get material() { return this._material; }
        set material(m) { this._material = m; }

        // Individual light mode
		get light() { return this._light; }
		set light(l) { this._light = l; this._lightArray.reset(); }
		get lightTransform() { return this._lightTransform; }
		set lightTransform(trx) { this._lightTransform = trx; this._lightArray.reset();}

		// Multiple light mode: use light arrays
		get lightArray() { return this._lightArray; }

        set shadowMap(sm) { this._shadowMap = sm; }
        get shadowMap() { return this._shadowMap; }

        setActive() {
            let numLights = 0
            if (this._light) {
                this.lightArray.reset();
                this.lightArray.push(this.light,this.lightTransform);
                
            }
            numLights = Math.min(this.lightArray.numLights,bg.base.MAX_FORWARD_LIGHTS);
            
            // Set the appropiate shader for the current number of enabled lights
            if (numLights>0) {
                this.setCurrentShader(numLights - 1);
            }
            super.setActive();
        }

        beginDraw() {

            if (this.lightArray.numLights && this.lightArray.numLights<bg.base.MAX_FORWARD_LIGHTS) {
                
                let matrixState = bg.base.MatrixState.Current();
                let viewMatrix = new bg.Matrix4(matrixState.viewMatrixStack.matrixConst);

                this.lightArray.updatePositionAndDirection(viewMatrix);

                // Forward render only supports one shadow map
				let lightTransform = this.shadowMap ? this.shadowMap.viewMatrix : this.lightArray.shadowLightTransform;
				this.shader.setMatrix4("inLightProjectionMatrix", this.shadowMap ? this.shadowMap.projection : this.lightArray.shadowLight.projection);
                let shadowColor = this.shadowMap ? this.shadowMap.shadowColor : bg.Color.Transparent();
                let blackTex = bg.base.TextureCache.BlackTexture(this.context);
                this.shader.setMatrix4("inLightViewMatrix",lightTransform);
                this.shader.setValueInt("inShadowType",this._shadowMap ? this._shadowMap.shadowType : 0);
                this.shader.setTexture("inShadowMap",this._shadowMap ? this._shadowMap.texture : blackTex,bg.base.TextureUnit.TEXTURE_0);
				this.shader.setVector2("inShadowMapSize",this._shadowMap ? this._shadowMap.size : new bg.Vector2(32,32));
				this.shader.setValueFloat("inShadowStrength",this.lightArray.shadowLight.shadowStrength);
				this.shader.setVector4("inShadowColor",shadowColor);
				this.shader.setValueFloat("inShadowBias",this.lightArray.shadowLight.shadowBias);
                this.shader.setValueInt("inCastShadows",this.lightArray.shadowLight.castShadows);
                this.shader.setVector3("inShadowLightDirection",this.lightArray.shadowLightDirection);
                this.shader.setValueInt("inShadowLightIndex",this.lightArray.shadowLightIndex);


                this.shader.setValueIntPtr('inLightType',this.lightArray.type);
                this.shader.setVector4Ptr('inLightDiffuse',this.lightArray.diffuse);
                this.shader.setVector4Ptr('inLightSpecular',this.lightArray.specular);
                this.shader.setVector3Ptr('inLightPosition',this.lightArray.position);
                this.shader.setVector3Ptr('inLightDirection',this.lightArray.direction);
                this.shader.setValueFloatPtr('inLightIntensity',this.lightArray.intensity);
                this.shader.setValueFloatPtr('inLightSpotCutoff',this.lightArray.cosSpotCutoff);
                this.shader.setValueFloatPtr('inLightOuterSpotCutoff',this.lightArray.cosSpotExponent);

                this.shader.setValueFloat('inGammaCorrection',2.0);
            }
        }

        setupVars() {
            let material = this.material;
            if (!(material instanceof bg.base.PBRMaterial) && !material.pbr) {
                material.pbr = material.pbr || bg.base.PBRMaterial.ImportFromLegacyMaterial(this.context,material);
                material = material.pbr;
            }
            else  if (material.pbr) {
                material.pbr.updateFromLegacyMaterial(this.context,material);
                material = material.pbr;
            }

            if (material instanceof bg.base.PBRMaterial) {
                let matrixState = bg.base.MatrixState.Current();
                let viewMatrix = new bg.Matrix4(matrixState.viewMatrixStack.matrixConst);
                this.shader.setMatrix4('inModelMatrix',matrixState.modelMatrixStack.matrixConst);
                this.shader.setMatrix4('inViewMatrix',viewMatrix);
                this.shader.setMatrix4('inProjectionMatrix',matrixState.projectionMatrixStack.matrixConst);
                this.shader.setMatrix4('inNormalMatrix',matrixState.normalMatrix);
                this.shader.setMatrix4('inViewMatrixInv',matrixState.viewMatrixInvert);

                let shaderParams = material.getShaderParameters(this.context);
            
                // Vector and scalar parameters
                this.shader.setVector2("inDiffuseOffset",shaderParams.diffuseOffset);
                this.shader.setVector2("inDiffuseScale",shaderParams.diffuseScale);
                this.shader.setVector2("inNormalOffset",shaderParams.normalOffset);
                this.shader.setVector2("inNormalScale",shaderParams.normalScale);
                this.shader.setValueInt("inReceiveShadows",true);
                this.shader.setValueFloat("inHeightScale",material.heightScale);
                this.shader.setValueFloat("inAlphaCutoff",shaderParams.alphaCutoff);

                // Texture maps
                let textureUnit = bg.base.TextureUnit.TEXTURE_1;    // TEXTURE_0 is used in beginDraw()
                //let textureUnit = bg.base.TextureUnit.TEXTURE_0;    // TEXTURE_0 is used in beginDraw()
                this.shader.setTexture("inDiffuse",shaderParams.diffuse.map,textureUnit++);
    
                this.shader.setTexture("inHeighMetallicRoughnessAO",shaderParams.heightMetallicRoughnessAO.map,textureUnit++);

                this.shader.setTexture("inNormalMap",shaderParams.normal.map,textureUnit++);

                let defaultMap = bg.base.TextureCache.BlackCubemap(this.context);
                let irradianceMap = defaultMap;
                let specMap0 = defaultMap;
                let specMap1 = defaultMap;
                let specMap2 = defaultMap;
                let env = bg.scene.Environment.Get();
                let irradianceIntensity = 1;
                if (env && env.environment) {
                    irradianceMap = env.environment.irradianceMapTexture || irradianceMap;
                    specMap0 = env.environment.specularMapTextureL0 || specMap0;
                    specMap1 = env.environment.specularMapTextureL1 || specMap1;
                    specMap2 = env.environment.specularMapTextureL2 || specMap2;
                    irradianceIntensity = env.environment.irradianceIntensity;
                }

                this.shader.setValueFloat("inIrradianceMapIntensity",irradianceIntensity);
                this.shader.setTexture("inIrradianceMap",irradianceMap,textureUnit++);
                this.shader.setTexture("inSpecularMap0",specMap0,textureUnit++);
                if (s_supportedTextureUnits>8) {
                    this.shader.setTexture("inSpecularMap1",specMap1,textureUnit++);
                    this.shader.setTexture("inSpecularMap2",specMap2,textureUnit++);
                }
                else {
                    this.shader.setTexture("inSpecularMap1",specMap1,textureUnit++);
                }

                this.shader.setTexture("inBRDF",s_brdfPrecomputedTexture,textureUnit++);
            }
            else {
                console.warn("Using invalid material or no PBRMaterial on PBR renderer.");
            }
        }
    }

    bg.base.PBRForwardEffect = PBRForwardEffect;
})();
(function() {

    class PBRForwardRenderLayer extends bg.render.ForwardRenderLayer {
        constructor(context,opacityLayer) {
            super(context,opacityLayer);

            this._pipeline.effect = new bg.base.PBRForwardEffect(context);
        }

        draw(renderQueue,scene,camera) {
            super.draw(renderQueue,scene,camera);
        }

        willDraw(scene,camera) {
            super.willDraw(scene,camera);
        }

        performDraw(renderQueue,scene,camera) {
            super.performDraw(renderQueue,scene,camera);
        }
    }

    bg.render.PBRForwardRenderLayer = PBRForwardRenderLayer;
})();
(function() {

    class PBRForwardRenderer extends bg.render.ForwardRenderer {
        constructor(context) {
            super(context);
        }

        create() {
            let ctx = this.context;

            this._transparentLayer = new bg.render.PBRForwardRenderLayer(this.context,bg.base.OpacityLayer.TRANSPARENT);
            this._opaqueLayer = new bg.render.PBRForwardRenderLayer(this.context,bg.base.OpacityLayer.OPAQUE);

            this._shadowMap = new bg.base.ShadowMap(ctx);
			this._shadowMap.size = new bg.Vector2(2048);

			this.settings.shadows.cascade = bg.base.ShadowCascade.NEAR;

			this._renderQueueVisitor = new bg.scene.RenderQueueVisitor
        }

        display(sceneRoot,camera) {
            let sceneEnvironment = bg.scene.Environment.Get();
            if (sceneEnvironment) {
                sceneEnvironment.environment.update(camera);
            }

            super.draw(sceneRoot,camera);
            
            if (sceneEnvironment) {
                sceneEnvironment.environment.renderSkybox(camera);
            }
		}
    }

    bg.render.PBRForwardRenderer = PBRForwardRenderer;
})();

(function() {
	function lib() {
		return bg.base.ShaderLibrary.Get();
	}

	class PostprocessEffect extends bg.base.TextureEffect {
		constructor(context) {
			super(context);
		}

		get fragmentShaderSource() {
			if (!this._fragmentShaderSource) {
				this._fragmentShaderSource = new bg.base.ShaderSource(bg.base.ShaderType.FRAGMENT);
				
				this._fragmentShaderSource.addParameter([
					{ name:"inTexture", dataType:"sampler2D", role:"value" },
					{ name:"inFrameSize", dataType:"vec2", role:"value"},
					{ name:"inBorderAntiAlias", dataType:"int", role:"value"},

					{ name:"fsTexCoord", dataType:"vec2", role:"in" }	// vTexturePosition
				]);
				
				if (bg.Engine.Get().id=="webgl1") {

					this._fragmentShaderSource.addFunction(lib().functions.utils.texOffset);
					this._fragmentShaderSource.addFunction(lib().functions.utils.luminance);
					this._fragmentShaderSource.addFunction(lib().functions.utils.borderDetection);
					this._fragmentShaderSource.addFunction(lib().functions.blur.textureDownsample);
					this._fragmentShaderSource.addFunction(lib().functions.blur.gaussianBlur);
					this._fragmentShaderSource.addFunction(lib().functions.blur.antiAlias);

					
					this._fragmentShaderSource.setMainBody(`
						vec4 result = vec4(0.0,0.0,0.0,1.0);
						if (inBorderAntiAlias==1) {
							result = antiAlias(inTexture,fsTexCoord,inFrameSize,0.1,3);
						}
						else {
							result = texture2D(inTexture,fsTexCoord);
						}
						gl_FragColor = result;
						`);
				}
			}
			return this._fragmentShaderSource;
		}

		setupVars() {
			this.shader.setTexture("inTexture",this._surface.texture,bg.base.TextureUnit.TEXTURE_0);
			this.shader.setVector2("inFrameSize",this._surface.texture.size);
			this.shader.setValueInt("inBorderAntiAlias",0);
		}

		get settings() {
			if (!this._settings) {
				this._currentKernelSize = 0;
				this._settings = {
					refractionAmount: 0.01
				}
			}
			return this._settings;
		}

	}

	bg.render.PostprocessEffect = PostprocessEffect;	
})();

(function() {
	function lib() {
		return bg.base.ShaderLibrary.Get();
	}

	class RendererMixEffect extends bg.base.TextureEffect {
		constructor(context) {
			super(context);
		}

		get fragmentShaderSource() {
			if (!this._fragmentShaderSource) {
				this._fragmentShaderSource = new bg.base.ShaderSource(bg.base.ShaderType.FRAGMENT);
				
				this._fragmentShaderSource.addParameter([
					{ name:"inOpaque", dataType:"sampler2D", role:"value" },
					{ name:"inTransparent", dataType:"sampler2D", role:"value"},
					{ name:"inTransparentNormal", dataType:"sampler2D", role:"value"},
					{ name:"inOpaqueDepth", dataType:"sampler2D", role:"value" },
					{ name:"inTransparentDepth", dataType:"sampler2D", role:"value" },
					{ name:"inRefractionAmount", dataType:"float", role:"value"},

					{ name:"fsTexCoord", dataType:"vec2", role:"in" }	// vTexturePosition
				]);
				
				if (bg.Engine.Get().id=="webgl1") {

					this._fragmentShaderSource.setMainBody(`
						vec4 opaque = texture2D(inOpaque,fsTexCoord);
						vec4 transparent = texture2D(inTransparent,fsTexCoord);
						vec3 normal = texture2D(inTransparentNormal,fsTexCoord).rgb * 2.0 - 1.0;
						if (transparent.a>0.0) {
							float refractionFactor = inRefractionAmount / texture2D(inTransparentDepth,fsTexCoord).z;
							vec2 offset = fsTexCoord - normal.xy * refractionFactor;
							vec4 opaqueDepth = texture2D(inOpaqueDepth,offset);
							vec4 transparentDepth = texture2D(inTransparentDepth,offset);
							//if (opaqueDepth.w>transparentDepth.w) {
								opaque = texture2D(inOpaque,offset);
							//}
						}
						vec3 color = opaque.rgb * (1.0 - transparent.a) + transparent.rgb * transparent.a;
						gl_FragColor = vec4(color, 1.0);
						`);
				}
			}
			return this._fragmentShaderSource;
		}

		setupVars() {
			this.shader.setTexture("inOpaque",this._surface.opaque,bg.base.TextureUnit.TEXTURE_0);
			this.shader.setTexture("inTransparent",this._surface.transparent,bg.base.TextureUnit.TEXTURE_1);
			this.shader.setTexture("inTransparentNormal",this._surface.transparentNormal,bg.base.TextureUnit.TEXTURE_2);
			this.shader.setTexture("inOpaqueDepth",this._surface.opaqueDepth,bg.base.TextureUnit.TEXTURE_3);
			this.shader.setTexture("inTransparentDepth",this._surface.transparentDepth,bg.base.TextureUnit.TEXTURE_4);
			this.shader.setValueFloat("inRefractionAmount",this.settings.refractionAmount);
		}

		get settings() {
			if (!this._settings) {
				this._currentKernelSize = 0;
				this._settings = {
					refractionAmount: -0.05
				}
			}
			return this._settings;
		}

	}

	bg.render.RendererMixEffect = RendererMixEffect;	
})();
(function() {

	
	function lib() {
		return bg.base.ShaderLibrary.Get();
	}

	class ShadowEffect extends bg.base.Effect {
		constructor(context) {
			super(context);
			
			let vertex = new bg.base.ShaderSource(bg.base.ShaderType.VERTEX);
			let fragment = new bg.base.ShaderSource(bg.base.ShaderType.FRAGMENT);
			
			vertex.addParameter([
				lib().inputs.buffers.vertex,
				lib().inputs.buffers.tex0,
				null,
				lib().inputs.matrix.model,
				lib().inputs.matrix.view,
				lib().inputs.matrix.projection,
				{ name:"inLightProjectionMatrix", dataType:"mat4", role:"value" },
				{ name:"inLightViewMatrix", dataType:"mat4", role:"value" },
				null,
				{ name:"fsTexCoord", dataType:"vec2", role:"out" },
				{ name:"fsVertexPosFromLight", dataType:"vec4", role:"out" }
			]);
			
			fragment.addParameter(lib().inputs.shadows.all);
			fragment.addFunction(lib().functions.utils.unpack);
			fragment.addFunction(lib().functions.lighting.getShadowColor);
			
			fragment.addParameter([
				lib().inputs.material.receiveShadows,
				lib().inputs.material.texture,
				lib().inputs.material.textureOffset,
				lib().inputs.material.textureScale,
				lib().inputs.material.alphaCutoff,
				null,
				{ name:"fsTexCoord", dataType:"vec2", role:"in" },
				{ name:"fsVertexPosFromLight", dataType:"vec4", role:"in" }
			]);
			
			fragment.addFunction(lib().functions.materials.samplerColor);
			
			if (bg.Engine.Get().id=="webgl1") {
				vertex.setMainBody(`
					mat4 ScaleMatrix = mat4(0.5, 0.0, 0.0, 0.0,
											0.0, 0.5, 0.0, 0.0,
											0.0, 0.0, 0.5, 0.0,
											0.5, 0.5, 0.5, 1.0);
					
					fsVertexPosFromLight = ScaleMatrix * inLightProjectionMatrix * inLightViewMatrix * inModelMatrix * vec4(inVertex,1.0);
					fsTexCoord = inTex0;
					
					gl_Position = inProjectionMatrix * inViewMatrix * inModelMatrix * vec4(inVertex,1.0);`);
				
				fragment.setMainBody(`
					float alpha = samplerColor(inTexture,fsTexCoord,inTextureOffset,inTextureScale).a;
					if (alpha>inAlphaCutoff) {
						vec4 shadowColor = vec4(1.0, 1.0, 1.0, 1.0);
						if (inReceiveShadows) {
							shadowColor = getShadowColor(fsVertexPosFromLight,inShadowMap,inShadowMapSize,
														 inShadowType,inShadowStrength,inShadowBias,inShadowColor);
						}
						gl_FragColor = shadowColor;
					}
					else {
						discard;
					}`);
			}
			
			this.setupShaderSource([ vertex, fragment ]);
		}
		
		beginDraw() {
			if (this.light && this.shadowMap) {
				let matrixState = bg.base.MatrixState.Current();
				let viewMatrix = new bg.Matrix4(matrixState.viewMatrixStack.matrixConst);
				let lightTransform = this.shadowMap.viewMatrix;
				
				this.shader.setMatrix4("inLightProjectionMatrix", this.shadowMap.projection);
				
				this.shader.setMatrix4("inLightViewMatrix",lightTransform);
				this.shader.setValueInt("inShadowType",this.shadowMap.shadowType);
				this.shader.setTexture("inShadowMap",this.shadowMap.texture,bg.base.TextureUnit.TEXTURE_1);
				this.shader.setVector2("inShadowMapSize",this.shadowMap.size);
				this.shader.setValueFloat("inShadowStrength",this.light.shadowStrength);
				this.shader.setVector4("inShadowColor",this.shadowMap.shadowColor);
				this.shader.setValueFloat("inShadowBias",this.light.shadowBias);
			}
		}
		
		setupVars() {
			if (this.material && this.light) {
				let matrixState = bg.base.MatrixState.Current();
				let viewMatrix = new bg.Matrix4(matrixState.viewMatrixStack.matrixConst);
				
				this.shader.setMatrix4("inModelMatrix",matrixState.modelMatrixStack.matrixConst);
				this.shader.setMatrix4("inViewMatrix",viewMatrix);
				this.shader.setMatrix4("inProjectionMatrix",matrixState.projectionMatrixStack.matrixConst);
				
				let texture = this.material.texture || bg.base.TextureCache.WhiteTexture(this.context);
				this.shader.setTexture("inTexture",texture,bg.base.TextureUnit.TEXTURE_0);
				this.shader.setVector2("inTextureOffset",this.material.textureOffset);
				this.shader.setVector2("inTextureScale",this.material.textureScale);
				this.shader.setValueFloat("inAlphaCutoff",this.material.alphaCutoff);
				
				this.shader.setValueInt("inReceiveShadows",this.material.receiveShadows);
			}
		}
		
		get material() { return this._material; }
		set material(m) { this._material = m; }
		
		get light() { return this._light; }
		set light(l) { this._light = l; }
		
		get shadowMap() { return this._shadowMap; }
		set shadowMap(sm) { this._shadowMap = sm; }
	}
	
	bg.render.ShadowEffect = ShadowEffect;
	
})();
(function() {
	function lib() {
		return bg.base.ShaderLibrary.Get();
	}

	let MAX_KERN_OFFSETS = 64;

	class SSAOEffect extends bg.base.TextureEffect {
		constructor(context) {
			super(context);
		}
		
		get fragmentShaderSource() {
			if (!this._fragmentShaderSource) {
				this._fragmentShaderSource = new bg.base.ShaderSource(bg.base.ShaderType.FRAGMENT);
				
				this._fragmentShaderSource.addParameter([
					{ name:"inViewportSize", dataType:"vec2", role:"value" },
					{ name:"inPositionMap", dataType:"sampler2D", role:"value"},
					{ name:"inNormalMap", dataType:"sampler2D", role:"value"},
					{ name:"inProjectionMatrix", dataType:"mat4", role:"value"},
					{ name:"inRandomMap", dataType:"sampler2D", role:"value"},
					{ name:"inRandomMapSize", dataType:"vec2", role:"value"},
					{ name:"inSampleRadius", dataType:"float", role:"value" },
					{ name:"inKernelOffsets", dataType:"vec3", role:"value", vec:MAX_KERN_OFFSETS },
					{ name:"inKernelSize", dataType:"int", role:"value" },
					{ name:"inSSAOColor", dataType:"vec4", role:"value" },
					{ name:"inEnabled", dataType:"bool", role:"value"},
					{ name:"inMaxDistance", dataType:"float", role:"value" },

					{ name:"fsTexCoord", dataType:"vec2", role:"in" }	// vTexturePosition
				]);
				
				if (bg.Engine.Get().id=="webgl1") {
					this._fragmentShaderSource.setMainBody(`
					if (!inEnabled) discard;
					else {
						vec4 normalTex = texture2D(inNormalMap,fsTexCoord);
						vec3 normal = normalTex.xyz * 2.0 - 1.0;
						vec4 vertexPos = texture2D(inPositionMap,fsTexCoord);
						if (distance(vertexPos.xyz,vec3(0))>inMaxDistance || vertexPos.w==1.0 || normalTex.a==0.0) {
							discard;
						}
						else {
							vec2 noiseScale = vec2(inViewportSize.x/inRandomMapSize.x,inViewportSize.y/inRandomMapSize.y);
							vec3 randomVector = texture2D(inRandomMap, fsTexCoord * noiseScale).xyz * 2.0 - 1.0;
							vec3 tangent = normalize(randomVector - normal * dot(randomVector, normal));
							vec3 bitangent = cross(normal,tangent);
							mat3 tbn = mat3(tangent, bitangent, normal);

							float occlusion = 0.0;
							for (int i=0; i<${ MAX_KERN_OFFSETS }; ++i) {
								if (inKernelSize==i) break;
								vec3 samplePos = tbn * inKernelOffsets[i];
								samplePos = samplePos * inSampleRadius + vertexPos.xyz;

								vec4 offset = inProjectionMatrix * vec4(samplePos, 1.0);	// -w, w
								offset.xyz /= offset.w;	// -1, 1
								offset.xyz = offset.xyz * 0.5 + 0.5;	// 0, 1

								vec4 sampleRealPos = texture2D(inPositionMap, offset.xy);
								if (samplePos.z<sampleRealPos.z) {
									float dist = distance(vertexPos.xyz, sampleRealPos.xyz);
									occlusion += dist<inSampleRadius ? 1.0:0.0;
								}
								
							}
							occlusion = 1.0 - (occlusion / float(inKernelSize));
							gl_FragColor = clamp(vec4(occlusion, occlusion, occlusion, 1.0) + inSSAOColor, 0.0, 1.0);
						}
					}`);
				}
			}
			return this._fragmentShaderSource;
		}
		
		setupVars() {
			if (this.settings.kernelSize>MAX_KERN_OFFSETS) {
				this.settings.kernelSize = MAX_KERN_OFFSETS;
			}
			this.shader.setVector2("inViewportSize",new bg.Vector2(this.viewport.width, this.viewport.height));

			// Surface map parameters
			this.shader.setTexture("inPositionMap",this._surface.position,bg.base.TextureUnit.TEXTURE_0);
			this.shader.setTexture("inNormalMap",this._surface.normal,bg.base.TextureUnit.TEXTURE_1);

			this.shader.setMatrix4("inProjectionMatrix",this.projectionMatrix);
			this.shader.setTexture("inRandomMap",this.randomMap,bg.base.TextureUnit.TEXTURE_2);
			this.shader.setVector2("inRandomMapSize",this.randomMap.size);
			this.shader.setValueFloat("inSampleRadius",this.settings.sampleRadius);
			this.shader.setVector3Ptr("inKernelOffsets",this.kernelOffsets);
			this.shader.setValueInt("inKernelSize",this.settings.kernelSize);
			this.shader.setVector4("inSSAOColor",this.settings.color);
			this.shader.setValueInt("inEnabled",this.settings.enabled);
			this.shader.setValueFloat("inMaxDistance",this.settings.maxDistance);
		}
		
		// The following parameters in this effect are required, and the program will crash if
		// some of them are not set
		get viewport() { return this._viewport; }
		set viewport(vp) { this._viewport = vp; }

		get projectionMatrix() { return this._projectionMatrix; }
		set projectionMatrix(p) { this._projectionMatrix = p; }

		// randomMap is optional. By default, it takes the default random texture from TextureCache
		get randomMap() {
			if (!this._randomMap) {
				this._randomMap = bg.base.TextureCache.RandomTexture(this.context);
			}
			return this._randomMap;
		}

		set randomMap(rm) { this._randomMap = rm; }

		// Settings: the following parameters are group in a settings object that can be exposed outside
		// the renderer object.
		get settings() {
			if (!this._settings) {
				this._currentKernelSize = 0;
				this._settings = {
					kernelSize: 32,
					sampleRadius: 0.3,
					color: bg.Color.Black(),
					blur: 4,
					maxDistance: 100.0,
					enabled: true
				}
			}
			return this._settings;
		}

		// This parameter is generated automatically using the kernelSize setting
		get kernelOffsets() {
			if (this._currentKernelSize!=this.settings.kernelSize) {
				this._kernelOffsets = [];
				for (let i=0; i<this.settings.kernelSize*3; i+=3) {
					let kernel = new bg.Vector3(bg.Math.random() * 2.0 - 1.0,
												bg.Math.random() * 2.0 - 1.0,
												bg.Math.random());
					kernel.normalize();

					let scale = (i/3) / this.settings.kernelSize;
					scale = bg.Math.lerp(0.1,1.0, scale * scale);
					kernel.scale(scale);
					this._kernelOffsets.push(kernel.x);
					this._kernelOffsets.push(kernel.y);
					this._kernelOffsets.push(kernel.z);
				}
				this._currentKernelSize = this.settings.kernelSize;
			}
			return this._kernelOffsets;
		}
	}
	
	bg.render.SSAOEffect = SSAOEffect;
})();
(function() {
	function lib() {
		return bg.base.ShaderLibrary.Get();
	}

	bg.render.RaytracerQuality = {
		low : { maxSamples: 50, rayIncrement: 0.025 },
		mid: { maxSamples: 100, rayIncrement: 0.0125 },
		high: { maxSamples: 200, rayIncrement: 0.0062 },
		extreme: { maxSamples: 300, rayIncrement: 0.0031 }
	}; 

	class SSRTEffect extends bg.base.TextureEffect {
		constructor(context) {
			super(context);
			this._basic = false;
			this._viewportSize = new bg.Vector2(1920,1080);
			this._frameIndex = 0;
		}
		
		get fragmentShaderSource() {
			if (!this._fragmentShaderSource) {
				this._fragmentShaderSource = new bg.base.ShaderSource(bg.base.ShaderType.FRAGMENT);
				let q = this.quality;

				this._fragmentShaderSource.addParameter([
					{ name:"inPositionMap", dataType:"sampler2D", role:"value"},
					{ name:"inSpecularMap", dataType:"sampler2D", role:"value" },
					{ name:"inNormalMap", dataType:"sampler2D", role:"value"},
					{ name:"inLightingMap", dataType:"sampler2D", role:"value"},
					{ name:"inMaterialMap", dataType:"sampler2D", role:"value"},
					{ name:"inSamplePosMap", dataType:"sampler2D", role:"value"},
					{ name:"inProjectionMatrix", dataType:"mat4", role:"value"},
					{ name:"inCameraPos", dataType:"vec3", role:"value" },
					{ name:"inRayFailColor", dataType:"vec4", role:"value" },
					{ name:"inBasicMode", dataType:"bool", role:"value" },
					{ name:"inFrameIndex", dataType:"float", role:"value" },
					{ name:"inCubeMap", dataType:"samplerCube", role:"value" },

					{ name:"inRandomTexture", dataType:"sampler2D", role:"value" },

					{ name:"fsTexCoord", dataType:"vec2", role:"in" }	// vTexturePosition
				]);

				this._fragmentShaderSource.addFunction(lib().functions.utils.random);
				
				if (bg.Engine.Get().id=="webgl1") {
					this._fragmentShaderSource.setMainBody(`
						vec2 p = vec2(floor(gl_FragCoord.x), floor(gl_FragCoord.y));
						bool renderFrame = false;
						if (inFrameIndex==0.0 && mod(p.x,2.0)==0.0 && mod(p.y,2.0)==0.0) {
							renderFrame = true;
						}
						else if (inFrameIndex==1.0 && mod(p.x,2.0)==0.0 && mod(p.y,2.0)!=0.0) {
							renderFrame = true;
						}
						else if (inFrameIndex==2.0 && mod(p.x,2.0)!=0.0 && mod(p.y,2.0)==0.0) {
							renderFrame = true;
						}
						else if (inFrameIndex==3.0 && mod(p.x,2.0)!=0.0 && mod(p.y,2.0)!=0.0) {
							renderFrame = true;
						}

						vec4 material = texture2D(inMaterialMap,fsTexCoord);
						if (renderFrame && material.b>0.0) {	// material[2] is reflectionAmount
							vec3 normal = texture2D(inNormalMap,fsTexCoord).xyz * 2.0 - 1.0;
							
							vec4 specular = texture2D(inSpecularMap,fsTexCoord);
							float roughness = specular.a * 0.3;
							vec3 r = texture2D(inRandomTexture,fsTexCoord*200.0).xyz * 2.0 - 1.0;
							vec3 roughnessFactor = normalize(r) * roughness;
							normal = normal + roughnessFactor;
							vec4 vertexPos = texture2D(inPositionMap,fsTexCoord);
							vec3 cameraVector = vertexPos.xyz - inCameraPos;
							vec3 rayDirection = reflect(cameraVector,normal);
							vec4 lighting = texture2D(inLightingMap,fsTexCoord);
							
							vec4 rayFailColor = inRayFailColor;
	
							vec3 lookup = reflect(cameraVector,normal);
							rayFailColor = textureCube(inCubeMap, lookup);
							
							float increment = ${q.rayIncrement};
							vec4 result = rayFailColor;
							if (!inBasicMode) {
								result = rayFailColor;
								for (float i=0.0; i<${q.maxSamples}.0; ++i) {
									if (i==${q.maxSamples}.0) {
										break;
									}
	
									float radius = i * increment;
									increment *= 1.01;
									vec3 ray = vertexPos.xyz + rayDirection * radius;
	
									vec4 offset = inProjectionMatrix * vec4(ray, 1.0);	// -w, w
									offset.xyz /= offset.w;	// -1, 1
									offset.xyz = offset.xyz * 0.5 + 0.5;	// 0, 1
	
									vec4 rayActualPos = texture2D(inSamplePosMap, offset.xy);
									float hitDistance = rayActualPos.z - ray.z;
									if (offset.x>1.0 || offset.y>1.0 || offset.x<0.0 || offset.y<0.0) {
										result = rayFailColor;
										break;
									}
									else if (hitDistance>0.02 && hitDistance<0.15) {
										result = texture2D(inLightingMap,offset.xy);
										break;
									}
								}
							}
							if (result.a==0.0) {
								gl_FragColor = rayFailColor;
							}
							else {
								gl_FragColor = result;
							}
						}
						else {
							discard;
						}`);
				}
			}
			return this._fragmentShaderSource;
		}
		
		setupVars() {
			this._frameIndex = (this._frameIndex + 1) % 4;
			this.shader.setTexture("inPositionMap",this._surface.position,bg.base.TextureUnit.TEXTURE_0);
			this.shader.setTexture("inNormalMap",this._surface.normal,bg.base.TextureUnit.TEXTURE_1);
			this.shader.setTexture("inLightingMap",this._surface.reflectionColor,bg.base.TextureUnit.TEXTURE_2);
			this.shader.setTexture("inMaterialMap",this._surface.material,bg.base.TextureUnit.TEXTURE_3);
			this.shader.setTexture("inSamplePosMap",this._surface.reflectionDepth, bg.base.TextureUnit.TEXTURE_4);
			this.shader.setMatrix4("inProjectionMatrix",this._projectionMatrix);
			this.shader.setVector3("inCameraPos",this._cameraPos);
			this.shader.setVector4("inRayFailColor",this.rayFailColor);
			this.shader.setValueInt("inBasicMode",this.basic);
			this.shader.setValueFloat("inFrameIndex",this._frameIndex);
			
			this.shader.setTexture("inCubeMap",bg.scene.Cubemap.Current(this.context), bg.base.TextureUnit.TEXTURE_5);
			
			
			if (!this._randomTexture) {
				this._randomTexture = bg.base.TextureCache.RandomTexture(this.context,new bg.Vector2(1024));
			}
			this.shader.setTexture("inRandomTexture",this._randomTexture, bg.base.TextureUnit.TEXTURE_6);

			this.shader.setTexture("inSpecularMap",this._surface.specular,bg.base.TextureUnit.TEXTURE_7);
		}

		get projectionMatrix() { return this._projectionMatrix; }
		set projectionMatrix(p) { this._projectionMatrix = p; }

		get cameraPosition() { return this._cameraPos; }
		set cameraPosition(c) { this._cameraPos = c; }

		get rayFailColor() { return this._rayFailColor || bg.Color.Black(); }
		set rayFailColor(c) { this._rayFailColor = c; }

		get viewportSize() { return this._viewportSize; }
		set viewportSize(s) { this._viewportSize = s; }

		get quality() { return this._quality || bg.render.RaytracerQuality.low; }
		set quality(q) {
			if (!this._quality || this._quality.maxSamples!=q.maxSamples ||
				this._quality.rayIncrement!=q.rayIncrement)
			{
				this._quality = q;
				this._fragmentShaderSource = null;
				this.rebuildShaders();
			}
		}

		get basic() { return this._basic; }
		set basic(b) { this._basic = b; }

		// TODO: SSRT settings
		get settings() {
			if (!this._settings) {
			}
			return this._settings;
		}
	}
	
	bg.render.SSRTEffect = SSRTEffect;
})();
bg.webgl1 = {};

(function() {
	let WEBGL_1_STRING = "webgl1";
	
	bg.webgl1.EngineId = WEBGL_1_STRING;
	
	class WebGL1Engine extends bg.Engine {
		constructor(context) {
			super(context);
			
			// Initialize webgl extensions
			bg.webgl1.Extensions.Get(context);
			
			this._engineId = WEBGL_1_STRING;
			this._texture = new bg.webgl1.TextureImpl(context);
			this._pipeline = new bg.webgl1.PipelineImpl(context);
			this._polyList = new bg.webgl1.PolyListImpl(context);
			this._shader = new bg.webgl1.ShaderImpl(context);
			this._colorBuffer = new bg.webgl1.ColorRenderSurfaceImpl(context);
			this._textureBuffer = new bg.webgl1.TextureRenderSurfaceImpl(context);
			this._shaderSource = new bg.webgl1.ShaderSourceImpl();
			this._cubemapCapture = new bg.webgl1.CubemapCaptureImpl();
		}

		createTextureMergerInstance() {
			return new bg.webgl1.TextureMergerImpl();
		}
	}
	
	bg.webgl1.Engine = WebGL1Engine;
	
})();

(function() {
	
	bg.webgl1.shaderLibrary = {
		inputs:{},
		functions:{}
	}

	class ShaderSourceImpl extends bg.base.ShaderSourceImpl {
		header(shaderType) {
			return "precision highp float;\nprecision highp int;";
		}
		
		parameter(shaderType,paramData) {
			if (!paramData) return "\n";
			let role = "";
			switch (paramData.role) {
				case "buffer":
					role = "attribute";
					break;
				case "value":
					role = "uniform";
					break;
				case "in":
				case "out":
					role = "varying";
					break;
			}
			let vec = "";
			if (paramData.vec) {
				vec = `[${paramData.vec}]`;
			}
			return `${role} ${paramData.dataType} ${paramData.name}${vec};`;
		}
		
		func(shaderType,funcData) {
			if (!funcData) return "\n";
			let params = "";
			for (let name in funcData.params) {
				params += `${funcData.params[name]} ${name},`;
			}
			let src = `${funcData.returnType} ${funcData.name}(${params}) {`.replace(',)',')');
			let body = ("\n" + bg.base.ShaderSource.FormatSource(funcData.body)).replace(/\n/g,"\n\t");
			return src + body + "\n}";
		}
	}
	
	bg.webgl1.ShaderSourceImpl = ShaderSourceImpl;
	
	
})();
(function() {

    let s_captureViews = [
        bg.Matrix4.LookAt(new bg.Vector3(0,0,0), new bg.Vector3( 1, 0, 0), new bg.Vector3(0,-1, 0)),
        bg.Matrix4.LookAt(new bg.Vector3(0,0,0), new bg.Vector3(-1, 0, 0), new bg.Vector3(0,-1, 0)),
        bg.Matrix4.LookAt(new bg.Vector3(0,0,0), new bg.Vector3( 0, 1, 0), new bg.Vector3(0, 0, 1)),
        bg.Matrix4.LookAt(new bg.Vector3(0,0,0), new bg.Vector3( 0,-1, 0), new bg.Vector3(0, 0,-1)),
        bg.Matrix4.LookAt(new bg.Vector3(0,0,0), new bg.Vector3( 0, 0, 1), new bg.Vector3(0,-1, 0)),
        bg.Matrix4.LookAt(new bg.Vector3(0,0,0), new bg.Vector3( 0, 0,-1), new bg.Vector3(0,-1, 0))
    ];

    let s_captureProjection = bg.Matrix4.Perspective(90,1,0.1,1000.0);

    class CubemapCaptureImpl extends bg.base.CubemapCaptureImpl {
        createCaptureBuffers(gl,size) {
            let captureBuffers = {
                fbo: gl.createFramebuffer(),
                rbo: gl.createRenderbuffer(),
                texture: null,
                size:size
            };

            gl.bindFramebuffer(gl.FRAMEBUFFER, captureBuffers.fbo);
            gl.bindRenderbuffer(gl.RENDERBUFFER, captureBuffers.rbo);
            gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, size, size);
            gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, captureBuffers.rbo);

            captureBuffers.texture = new bg.base.Texture(gl);
            captureBuffers.texture.target = bg.base.TextureTarget.CUBE_MAP;
            captureBuffers.texture.create();
            captureBuffers.texture.bind();
            for (let i=0; i<6; ++i) {
                gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, gl.RGB, size, size, 0, gl.RGB, gl.UNSIGNED_BYTE, null);
            }
            //gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, false);
            gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
            gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        
            captureBuffers.texture.unbind();
            captureBuffers.texture._size = new bg.Vector2(size);

            gl.bindFramebuffer(gl.FRAMEBUFFER, null);
            gl.bindRenderbuffer(gl.RENDERBUFFER, null);
            
            return captureBuffers;
		}

		beginRender(gl,captureBuffers) {
            captureBuffers._currentViewport = bg.base.Pipeline.Current() ? bg.base.Pipeline.Current().viewport : new bg.Viewport(0,0,512,512);
            //gl.viewport(0,0,captureBuffers.size,captureBuffers.size);
            gl.bindFramebuffer(gl.FRAMEBUFFER,captureBuffers.fbo);
        }
        
        beginRenderFace(gl,face,captureBuffers,viewMatrix) {
            let textureImpl = captureBuffers.texture.texture;
            gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_CUBE_MAP_POSITIVE_X + face, textureImpl, 0);

            // TODO: Debug framebuffer
            gl.clearColor(0,0,0,1);
            gl.viewport(0,0,captureBuffers.size,captureBuffers.size);
            gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
            gl.disable(gl.CULL_FACE);

            let captureView = new bg.Matrix4(s_captureViews[face]);
            captureView.mult(viewMatrix);

            return {
                view:captureView,
                projection: s_captureProjection
            };
        }

        endRenderFace(context,face,captureBuffers) {
        }

		endRender(gl,captureBuffers) {
            gl.viewport(
                captureBuffers._currentViewport.x,
                captureBuffers._currentViewport.y,
                captureBuffers._currentViewport.width,
                captureBuffers._currentViewport.height
            );
            gl.bindFramebuffer(gl.FRAMEBUFFER, null);
            gl.enable(gl.CULL_FACE);
		}

        getTexture(context,captureBuffers) {
            return captureBuffers.texture;
        }
        
        destroy(context,captureBuffers) {
            // TODO: implement this
        }
    }


    bg.webgl1.CubemapCaptureImpl = CubemapCaptureImpl;

})();
(function() {
	let s_singleton = null;

	class Extensions extends bg.app.ContextObject {
		static Get(gl) {
			if (!s_singleton) {
				s_singleton = new Extensions(gl);
			}
			return s_singleton;
		}
		
		constructor(gl) {
			super(gl);
		}
		
		getExtension(ext) {
			return this.context.getExtension(ext);
		}
		
		get textureFloat() {
			if (this._textureFloat===undefined) {
				this._textureFloat = this.getExtension("OES_texture_float");
			}
			return this._textureFloat;
		}
		
		get depthTexture() {
			if (this._depthTexture===undefined) {
				this._depthTexture = this.getExtension("WEBGL_depth_texture");
			}
			return this._depthTexture;
		}
		
		get drawBuffers() {
			if (this._drawBuffers===undefined) {
				this._drawBuffers = this.getExtension("WEBGL_draw_buffers");
			}
			return this._drawBuffers;
		}
	}
	
	bg.webgl1.Extensions = Extensions;
})();
(function() {
	
	class PipelineImpl extends bg.base.PipelineImpl {
		initFlags(context) {
			bg.base.ClearBuffers.COLOR = context.COLOR_BUFFER_BIT;
			bg.base.ClearBuffers.DEPTH = context.DEPTH_BUFFER_BIT;
		}
		
		setViewport(context,vp) {
			context.viewport(vp.x,vp.y,vp.width,vp.height);
		}
		
		clearBuffers(context,color,buffers) {
			context.clearColor(color.r,color.g,color.b,color.a);
			if (buffers) context.clear(buffers);
		}
		
		setDepthTestEnabled(context,e) {
			e ? context.enable(context.DEPTH_TEST):context.disable(context.DEPTH_TEST);
		}
		
		setCullFace(context,e) {
			e ? context.enable(context.CULL_FACE):context.disable(context.CULL_FACE);
		}
		
		setBlendEnabled(context,e) {
			e ? context.enable(context.BLEND):context.disable(context.BLEND);
		}
		
		setBlendMode(gl,m) {
			switch (m) {
				case bg.base.BlendMode.NORMAL:
					gl.blendFunc(gl.SRC_ALPHA,gl.ONE_MINUS_SRC_ALPHA);
					gl.blendEquation(gl.FUNC_ADD);
					break;
				case bg.base.BlendMode.MULTIPLY:
					gl.blendFunc(gl.ZERO,gl.SRC_COLOR);
					gl.blendEquation(gl.FUNC_ADD);
					break;
				case bg.base.BlendMode.ADD:
					gl.blendFunc(gl.ONE,gl.ONE);
					gl.blendEquation(gl.FUNC_ADD);
					break;
				case bg.base.BlendMode.SUBTRACT:
					gl.blendFunc(gl.ONE,gl.ONE);
					gl.blendEquation(gl.FUNC_SUBTRACT);
					break;
				case bg.base.BlendMode.ALPHA_ADD:
					gl.blendFunc(gl.SRC_ALPHA,gl.SRC_ALPHA);
					gl.blendEquation(gl.FUNC_ADD);
					break;
				case bg.base.BlendMode.ALPHA_SUBTRACT:
					gl.blendFunc(gl.SRC_ALPHA,gl.SRC_ALPHA);
					gl.blendEquation(gl.FUNC_SUBTRACT);
					break;
			}
		}
	}
	
	bg.webgl1.PipelineImpl = PipelineImpl;
})();
(function() {
	function createBuffer(context,array,itemSize,drawMode) {
		let result = null;
		if (array.length) {
			result = context.createBuffer();
			context.bindBuffer(context.ARRAY_BUFFER,result);
			context.bufferData(context.ARRAY_BUFFER, new Float32Array(array), drawMode);
					result.itemSize = itemSize;
					result.numItems = array.length/itemSize;
		}
		return result;
	}
	
	function deleteBuffer(context,buffer) {
		if (buffer) {
			context.deleteBuffer(buffer);
		}
		return null;
	}

	let s_uintElements = false;
	
	class PolyListImpl extends bg.base.PolyListImpl {
		initFlags(context) {
			bg.base.DrawMode.TRIANGLES = context.TRIANGLES;
			bg.base.DrawMode.TRIANGLE_FAN = context.TRIANGLE_FAN;
			bg.base.DrawMode.TRIANGLE_STRIP = context.TRIANGLE_STRIP;
			bg.base.DrawMode.LINES = context.LINES;
			bg.base.DrawMode.LINE_STRIP = context.LINE_STRIP;

			s_uintElements = context.getExtension("OES_element_index_uint");
		}
		
		create(context) {
			return {
				vertexBuffer:null,
				normalBuffer:null,
				tex0Buffer:null,
				tex1Buffer:null,
				tex2Buffer:null,
				colorBuffer:null,
				tangentBuffer:null,
				indexBuffer:null
			}
		}
		
		build(context,plist,vert,norm,t0,t1,t2,col,tan,index) {
			plist.vertexBuffer = createBuffer(context,vert,3,context.STATIC_DRAW);
			plist.normalBuffer = createBuffer(context,norm,3,context.STATIC_DRAW);
			plist.tex0Buffer = createBuffer(context,t0,2,context.STATIC_DRAW);
			plist.tex1Buffer = createBuffer(context,t1,2,context.STATIC_DRAW);
			plist.tex2Buffer = createBuffer(context,t2,2,context.STATIC_DRAW);
			plist.colorBuffer = createBuffer(context,col,4,context.STATIC_DRAW);
			plist.tangentBuffer = createBuffer(context,tan,3,context.STATIC_DRAW);
		
			if (index.length>0 && s_uintElements) {
				plist.indexBuffer = context.createBuffer();
				context.bindBuffer(context.ELEMENT_ARRAY_BUFFER, plist.indexBuffer);
				context.bufferData(context.ELEMENT_ARRAY_BUFFER, new Uint32Array(index),context.STATIC_DRAW);
				plist.indexBuffer.itemSize = 3;
				plist.indexBuffer.numItems = index.length;
			}
			else {
				plist.indexBuffer = context.createBuffer();
				context.bindBuffer(context.ELEMENT_ARRAY_BUFFER, plist.indexBuffer);
				context.bufferData(context.ELEMENT_ARRAY_BUFFER, new Uint16Array(index),context.STATIC_DRAW);
				plist.indexBuffer.itemSize = 3;
				plist.indexBuffer.numItems = index.length;
			}
			
			return plist.vertexBuffer && plist.indexBuffer;
		}
		
		draw(context,plist,drawMode,numberOfIndex) {
			context.bindBuffer(context.ELEMENT_ARRAY_BUFFER, plist.indexBuffer);
			if (s_uintElements) {
				context.drawElements(drawMode, numberOfIndex, context.UNSIGNED_INT, 0);
			}
			else {
				context.drawElements(drawMode, numberOfIndex, context.UNSIGNED_SHORT, 0);
			}
		}
		
		destroy(context,plist) {
			context.bindBuffer(context.ARRAY_BUFFER, null);
			context.bindBuffer(context.ELEMENT_ARRAY_BUFFER, null);

			plist.vertexBuffer = deleteBuffer(context,plist.vertexBuffer);
			plist.normalBuffer = deleteBuffer(context,plist.normalBuffer);
			plist.tex0Buffer = deleteBuffer(context,plist.tex0Buffer);
			plist.tex1Buffer = deleteBuffer(context,plist.tex1Buffer);
			plist.tex2Buffer = deleteBuffer(context,plist.tex2Buffer);
			plist.colorBuffer = deleteBuffer(context,plist.colorBuffer);
			plist.tangentBuffer = deleteBuffer(context,plist.tangentBuffer);
			plist.indexBuffer = deleteBuffer(context,plist.indexBuffer);
		}

		update(context,plist,bufferType,newData) {
			if (bufferType==bg.base.BufferType.INDEX) {
				if (s_uintElements) {
					context.bindBuffer(context.ELEMENT_ARRAY_BUFFER, plist.indexBuffer);
					context.bufferData(context.ELEMENT_ARRAY_BUFFER, new Uint32Array(index),context.STATIC_DRAW);
				}
				else {
					context.bindBuffer(context.ELEMENT_ARRAY_BUFFER, plist.indexBuffer);
					context.bufferData(context.ELEMENT_ARRAY_BUFFER, new Uint16Array(index),context.STATIC_DRAW);
				}
				context.bindBuffer(context.ELEMENT_ARRAY_BUFFER,null);
			}
			else {
				switch (bufferType) {
				case bg.base.BufferType.VERTEX:
					context.bindBuffer(context.ARRAY_BUFFER, plist.vertexBuffer);
					break;
				case bg.base.BufferType.NORMAL:
					context.bindBuffer(context.ARRAY_BUFFER, plist.normalBuffer);
					break;
				case bg.base.BufferType.TEX_COORD_0:
					context.bindBuffer(context.ARRAY_BUFFER, plist.tex0Buffer);
					break;
				case bg.base.BufferType.TEX_COORD_1:
					context.bindBuffer(context.ARRAY_BUFFER, plist.tex1Buffer);
					break;
				case bg.base.BufferType.TEX_COORD_2:
					context.bindBuffer(context.ARRAY_BUFFER, plist.tex2Buffer);
					break;
				case bg.base.BufferType.COLOR:
					context.bindBuffer(context.ARRAY_BUFFER, plist.colorBuffer);
					break;
				case bg.base.BufferType.TANGENT:
					context.bindBuffer(context.ARRAY_BUFFER, plist.tangentBuffer);
					break;
				}
				context.bufferData(context.ARRAY_BUFFER, new Float32Array(newData), context.STATIC_DRAW);
				context.bindBuffer(context.ARRAY_BUFFER,null);
			}
		}
	}
	
	bg.webgl1.PolyListImpl = PolyListImpl;
})();

(function() {
	
	let ext = null;
	
	function getMaxColorAttachments() {
		if (ext.drawBuffers) {
			return	ext.drawBuffers.MAX_COLOR_ATTACHMENTS ||
					ext.drawBuffers.MAX_COLOR_ATTACHMENTS_WEBGL;
		}
		return 1;
	}
	
	// type: RGBA, format: UNSIGNED_BYTE => regular RGBA texture
	// type: RGBA, format: FLOAT => float texture attachment
	// type: DEPTH, format: RENDERBUFFER => regular depth renderbuffer
	// type: DEPTH, format: UNSIGNED_SHORT => depth texture
	// every one else: error, unsupported combination
	function checkValid(attachment) {
		switch (true) {
			case attachment.type==bg.base.RenderSurfaceType.RGBA && attachment.format==bg.base.RenderSurfaceFormat.UNSIGNED_BYTE:
				return true;
			case attachment.type==bg.base.RenderSurfaceType.RGBA && attachment.format==bg.base.RenderSurfaceFormat.FLOAT:
				return true;
			case attachment.type==bg.base.RenderSurfaceType.DEPTH && attachment.format==bg.base.RenderSurfaceFormat.RENDERBUFFER:
				return true;
			case attachment.type==bg.base.RenderSurfaceType.DEPTH && attachment.format==bg.base.RenderSurfaceFormat.UNSIGNED_SHORT:
				return true;
			// TODO: Cubemaps
			default:
				return false;
		}
	}
	
	function getTypeString(type) {
		switch (type) {
			case bg.base.RenderSurfaceType.RGBA:
				return "RGBA";
			case bg.base.RenderSurfaceType.DEPTH:
				return "DEPTH";
			default:
				return "unknown";
		}
	}
	
	function getFormatString(format) {
		switch (format) {
			case bg.base.RenderSurfaceFormat.UNSIGNED_BYTE:
				return "UNSIGNED_BYTE";
			case bg.base.RenderSurfaceFormat.FLOAT:
				return "FLOAT";
			case bg.base.RenderSurfaceFormat.RENDERBUFFER:
				return "RENDERBUFFER";
			case bg.base.RenderSurfaceFormat.UNSIGNED_SHORT:
				return "UNSIGNED_SHORT";
			default:
				return "unknown";
		}
	}
	
	function checkCompatibility(attachments) {
		let colorAttachments = 0;
		let maxColorAttachments = getMaxColorAttachments();
		let error = null;
		
		attachments.every(function(att,index) {
			if (!checkValid(att)) {
				error = `Error in attachment ${index}: Invalid combination of type and format (${getTypeString(att.type)} is incompatible with ${getFormatString(att.format)}).`;
				return false;
			}
			
			if (att.type==bg.base.RenderSurfaceType.DEPTH &&
				index!=attachments.length-1
			) {
				error = `Error in attachment ${index}: Depth attachment must be specified as the last attachment. Specified at index ${index} of ${attachments.length - 1}`;
				return false;
			}
			
			if (att.type==bg.base.RenderSurfaceType.RGBA) {
				++colorAttachments;
			}
			
			if (att.format==bg.base.RenderSurfaceFormat.FLOAT && !ext.textureFloat) {
				error = `Error in attachment ${index}: Floating point render surface requested, but the required extension is not present: OES_texture_float.`;
				return false;
			}
			if (att.type==bg.base.RenderSurfaceType.DEPTH &&
				att.format!=bg.base.RenderSurfaceFormat.RENDERBUFFER &&
				!ext.depthTexture
			) {
				error = `Error in attachment ${index}: Depth texture attachment requested, but the requiered extension is not present: WEBGL_depth_texture.`;
				return false;
			}
			if (colorAttachments>maxColorAttachments) {
				error = `Error in attachment ${index}: Maximum number of ${maxColorAttachments} color attachment exceeded.`;
				return false;
			}
			
			return true;
		});
		
		return error;
	}
	
	function addAttachment(gl,size,attachment,index) {
		if (attachment.format==bg.base.RenderSurfaceFormat.RENDERBUFFER) {
			let renderbuffer = gl.createRenderbuffer();
			
			gl.bindRenderbuffer(gl.RENDERBUFFER, renderbuffer);
			gl.renderbufferStorage(gl.RENDERBUFFER,gl.DEPTH_COMPONENT16,size.width,size.height);
			gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, renderbuffer);
			gl.bindRenderbuffer(gl.RENDERBUFFER,null);
			
			return { _renderbuffer: renderbuffer };
		}
		else {
			let texture = new bg.base.Texture(gl);
			let format = attachment.format;
			let type = attachment.type;
			
			texture.create();
			texture.bind();
			texture.minFilter = bg.base.TextureFilter.LINEAR;
			texture.magFilter = bg.base.TextureFilter.LINEAR;
			texture.wrapX = bg.base.TextureWrap.CLAMP;
			texture.wrapY = bg.base.TextureWrap.CLAMP;
			texture.setImageRaw(size.width,size.height,null,type,format);
			gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0 + index, gl.TEXTURE_2D, texture._texture, 0);
			texture.unbind();

			return texture;		
		}
	}
	
	function resizeAttachment(gl,size,att,index) {
		if (att.texture) {
			att.texture.bind();
			att.texture.setImageRaw(size.width,size.height,null,att.type,att.format);
			att.texture.unbind();
		}
		if (att.renderbuffer) {
			let rb = att.renderbuffer._renderbuffer;
			gl.bindRenderbuffer(gl.RENDERBUFFER,rb);
			gl.renderbufferStorage(gl.RENDERBUFFER,gl.DEPTH_COMPONENT16, size.width, size.height);
			gl.bindRenderbuffer(gl.RENDERBUFFER,null);
		}
	}
	
	
	class WebGLRenderSurfaceImpl extends bg.base.RenderSurfaceBufferImpl {
		initFlags(gl) {
			bg.base.RenderSurfaceType.RGBA = gl.RGBA;
			bg.base.RenderSurfaceType.DEPTH = gl.DEPTH_COMPONENT;
			
			bg.base.RenderSurfaceFormat.UNSIGNED_BYTE = gl.UNSIGNED_BYTE;
			bg.base.RenderSurfaceFormat.UNSIGNED_SHORT = gl.UNSIGNED_SHORT;
			bg.base.RenderSurfaceFormat.FLOAT = gl.FLOAT;
			// This is not a format. This will create a renderbuffer instead a texture
			bg.base.RenderSurfaceFormat.RENDERBUFFER = gl.RENDERBUFFER;
			
			ext = bg.webgl1.Extensions.Get();
		}
		
		supportType(type) {
			switch (type) {
				case bg.base.RenderSurfaceType.RGBA:
					return true;
				case bg.base.RenderSurfaceType.DEPTH:
					return ext.depthTexture!=null;
				default:
					return false;
			}
		}
		
		supportFormat(format) {
			switch (format) {
				case bg.base.RenderSurfaceFormat.UNSIGNED_BYTE:
				case bg.base.RenderSurfaceFormat.UNSIGNED_SHORT:
					return true;
				case bg.base.RenderSurfaceFormat.FLOAT:
					return ext.textureFloat!=null;
				default:
					return false;
			}
		}
		
		get maxColorAttachments() {
			return getMaxColorAttachments();
		}
	}
	
	class ColorRenderSurfaceImpl extends WebGLRenderSurfaceImpl {
		create(gl) {
			return {};
		}
		setActive(gl,renderSurface,attachments) {
			gl.bindFramebuffer(gl.FRAMEBUFFER,null);
		}
		resize(gl,renderSurface,size) {}
		destroy(gl,renderSurface) {}

		readBuffer(gl,renderSurface,rectangle,viewportSize) {
			let pixels = new Uint8Array(rectangle.width * rectangle.height * 4);
			// Note that the webgl texture is flipped vertically, so we need to convert the Y coord
			gl.readPixels(rectangle.x, rectangle.y, rectangle.width, rectangle.height, gl.RGBA, gl.UNSIGNED_BYTE, pixels);
			return pixels;
		}
	}
	
	bg.webgl1.ColorRenderSurfaceImpl = ColorRenderSurfaceImpl;
	
	class TextureRenderSurfaceImpl extends WebGLRenderSurfaceImpl {
		initFlags(gl) {}	// Flags initialized in ColorRenderSurfaceImpl
				
		create(gl,attachments) {
			// If this function returns no error, the browser is compatible with
			// the specified attachments.
			let error = checkCompatibility(attachments);
			if (error) {
				throw new Error(error);
			}
			// Initial size of 256. The actual size will be defined in resize() function
			let size = new bg.Vector2(256);
			let surfaceData = {
				fbo: gl.createFramebuffer(),
				size: size,
				attachments: []
			};
			
			gl.bindFramebuffer(gl.FRAMEBUFFER, surfaceData.fbo);
			
			let colorAttachments = [];
			attachments.forEach((att,i) => {
				// This will return a bg.base.Texture or a renderbuffer
				let result = addAttachment(gl,size,att,i);
				if (result instanceof bg.base.Texture) {
					colorAttachments.push(ext.drawBuffers ? ext.drawBuffers.COLOR_ATTACHMENT0_WEBGL + i : gl.COLOR_ATTACHMENT0);
				}
				surfaceData.attachments.push({
					texture: result instanceof bg.base.Texture ? result:null,
					renderbuffer: result instanceof bg.base.Texture ? null:result,
					format:att.format,
					type:att.type
				});
			});
			if (colorAttachments.length>1) {
				ext.drawBuffers.drawBuffersWEBGL(colorAttachments);
			}
			
			gl.bindFramebuffer(gl.FRAMEBUFFER,null);
			return surfaceData;
		}
		
		setActive(gl,renderSurface) {
			gl.bindFramebuffer(gl.FRAMEBUFFER,renderSurface.fbo);
		}
		
		readBuffer(gl,renderSurface,rectangle,viewportSize) {
			let pixels = new Uint8Array(rectangle.width * rectangle.height * 4);
			// Note that the webgl texture is flipped vertically, so we need to convert the Y coord
			gl.readPixels(rectangle.x, viewportSize.height - rectangle.y, rectangle.width, rectangle.height, gl.RGBA, gl.UNSIGNED_BYTE, pixels);
			return pixels;
		}
		
		resize(gl,renderSurface,size) {
			renderSurface.size.width = size.width;
			renderSurface.size.height = size.height;
			renderSurface.attachments.forEach((att,index) => {
				resizeAttachment(gl,size,att,index);
			});
		}
		
		destroy(gl,renderSurface) {
			gl.bindFramebuffer(gl.FRAMEBUFFER, null);
			let attachments = renderSurface && renderSurface.attachments;
			if (renderSurface.fbo) {
				gl.deleteFramebuffer(renderSurface.fbo);
			}
			if (attachments) {
				attachments.forEach((attachment) => {
					if (attachment.texture) {
						attachment.texture.destroy();
					}
					else if (attachment.renderbuffer) {
						gl.deleteRenderbuffer(attachment.renderbuffer._renderbuffer);
					}
				});
			}
			renderSurface.fbo = null;
			renderSurface.size = null;
			renderSurface.attachments = null;
		}
	}
	
	bg.webgl1.TextureRenderSurfaceImpl = TextureRenderSurfaceImpl;
})();
(function() {
	let MAX_BLUR_ITERATIONS = 40;
	
	let BLUR_DOWNSAMPLE = 15 	;

	let textureCubeDownsampleParams = {
		textureInput:'samplerCube', texCoord:'vec3', size:'vec2', reduction:'vec2'
	};
	let textureCubeDownsampleBody = `
		float dx = reduction.x / size.x;
		float dy = reduction.y / size.y;
		vec2 coord = vec2(dx * texCoord.x / dx, dy * texCoord.y / dy);
		return textureCube(textureInput,coord);
	`;

	let textureDownsampleParams = {
		textureInput:'sampler2D', texCoord:'vec2', size:'vec2', reduction:'vec2'
	};
	let textureDownsampleBody = `
		float dx = reduction.x / size.x;
		float dy = reduction.y / size.y;
		vec2 coord = vec2(dx * texCoord.x / dx, dy * texCoord.y / dy);
		return texture2D(textureInput,coord);
	`;

	let blurParams = {
			textureInput:'sampler2D', texCoord:'vec2', size:'int', samplerSize:'vec2'
		};
	let blurBody = `
		int downsample = ${ BLUR_DOWNSAMPLE };
		vec2 texelSize = 1.0 / samplerSize;
		vec3 result = vec3(0.0);
		size = int(max(float(size / downsample),1.0));
		vec2 hlim = vec2(float(-size) * 0.5 + 0.5);
		vec2 sign = vec2(1.0);
		float blurFactor = 10.0 - 0.2 * float(size) * log(float(size));
		for (int x=0; x<${ MAX_BLUR_ITERATIONS }; ++x) {
			if (x==size) break;
			for (int y=0; y<${ MAX_BLUR_ITERATIONS }; ++y) {
				if (y==size) break;
				vec2 offset = (hlim + vec2(float(x), float(y))) * texelSize * float(downsample) / blurFactor;
				result += textureDownsample(textureInput, texCoord + offset,samplerSize,vec2(downsample)).rgb;
			}
		}
		return vec4(result / float(size * size), 1.0);
		`;
	
	let glowParams = {
			textureInput:'sampler2D', texCoord:'vec2', size:'int', samplerSize:'vec2'			
		};
	let glowBody = `
		int downsample = ${ BLUR_DOWNSAMPLE };
		vec2 texelSize = 1.0 / samplerSize;
		vec3 result = vec3(0.0);
		size = int(max(float(size / downsample),1.0));
		vec2 hlim = vec2(float(-size) * 0.5 + 0.5);
		vec2 sign = vec2(1.0);
		for (int x=0; x<${ MAX_BLUR_ITERATIONS }; ++x) {
			if (x==size) break;
			for (int y=0; y<${ MAX_BLUR_ITERATIONS }; ++y) {
				if (y==size) break;
				vec2 offset = (hlim + vec2(float(x), float(y))) * texelSize;
				result += textureDownsample(textureInput, texCoord + offset,samplerSize,vec2(downsample)).rgb;
			}
		}
		return vec4(result / float(size * size), 1.0);
	`;

	let blurCubeParams = {
		textureInput:'samplerCube', texCoord:'vec3', size:'int', samplerSize:'vec2', dist:'float'
	};
	let blurCubeBody = `
		int downsample = int(max(1.0,dist));
		vec2 texelSize = 1.0 / samplerSize;
		vec3 result = vec3(0.0);
		size = int(max(float(size / downsample),1.0));
		vec2 hlim = vec2(float(-size) * 0.5 + 0.5);
		vec2 sign = vec2(1.0);
		for (int x=0; x<40; ++x) {
			if (x==size) break;
			for (int y=0; y<40; ++y) {
				if (y==size) break;
				vec3 offset = vec3((hlim + vec2(float(x*downsample), float(y*downsample))) * texelSize,0.0);
				result += textureCube(textureInput, texCoord + offset,2.0).rgb;
			}
		}
		return vec4(result / float(size * size), 1.0);
		`;
	
	bg.webgl1.shaderLibrary
		.functions
		.blur = {
			textureDownsample:{
				returnType:"vec4", name:'textureDownsample', params:textureDownsampleParams, body:textureDownsampleBody
			},
			gaussianBlur:{
				returnType:"vec4", name:"gaussianBlur", params:blurParams, body:blurBody
			},
			blur:{
				returnType:"vec4", name:"blur", params:blurParams, body:blurBody
			},
			glowBlur:{
				returnType:"vec4", name:"glowBlur", params:glowParams, body:glowBody
			},
			blurCube:{
				returnType:"vec4", name:"blurCube", params:blurCubeParams, body:blurCubeBody
			},

			// Require: utils.borderDetection
			antiAlias:{
				returnType:'vec4', name:'antiAlias', params: {
					sampler:'sampler2D', texCoord:'vec2', frameSize:'vec2', tresshold:'float', iterations:'int'
				}, body: `
				return (borderDetection(sampler,texCoord,frameSize)>tresshold) ?
					gaussianBlur(sampler,texCoord,iterations,frameSize) :
					texture2D(sampler,texCoord);
				`
			}
		}
})();
(function() {
	
	bg.webgl1.shaderLibrary
		.functions
		.colorCorrection = {
			rgb2hsv: {
				returnType:"vec3", name:"rgb2hsv", params:{ c:"vec3" }, body:`
				vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
				vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
				vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

				float d = q.x - min(q.w, q.y);
				float e = 1.0e-10;
				return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);`
			},
			hsv2rgb: {
				returnType:"vec3", name:"hsv2rgb", params: { c:"vec3" }, body:`
				vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
				vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
				return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);`
			},
			applyBrightness: {
				returnType:"vec4", name:"applyBrightness", params: { color:"vec4", brightness:"float" }, body:`
					return clamp(vec4(color.rgb + brightness - 0.5,1.0),0.0,1.0);
				`
			},
			applyContrast: {
				returnType:"vec4", name:"applyContrast", params:{ color:"vec4", contrast:"float" }, body:`
				return clamp(vec4((color.rgb * max(contrast + 0.5,0.0)),1.0),0.0,1.0);`
			},
			applySaturation: {
				returnType:"vec4", name:"applySaturation", params:{ color:"vec4", hue:"float", saturation:"float", lightness:"float" }, body:`
				vec3 fragRGB = clamp(color.rgb + vec3(0.001),0.0,1.0);
				vec3 fragHSV = rgb2hsv(fragRGB);
				lightness -= 0.01;
				float h = hue;
				fragHSV.x *= h;
				fragHSV.yz *= vec2(saturation,lightness);
				fragHSV.x = mod(fragHSV.x, 1.0);
				fragHSV.y = mod(fragHSV.y, 1.0);
				fragHSV.z = mod(fragHSV.z, 1.0);
				fragRGB = hsv2rgb(fragHSV);
				return clamp(vec4(hsv2rgb(fragHSV), color.w),0.0,1.0);`
			},
			colorCorrection: {
				returnType:"vec4", name:"colorCorrection", params:{
					fragColor:"vec4", hue:"float", saturation:"float",
					lightness:"float", brightness:"float", contrast:"float" },
				body:`
				return applyContrast(applyBrightness(applySaturation(fragColor,hue,saturation,lightness),brightness),contrast);`
			}
		}
	
})();
(function() {
	
	bg.webgl1.shaderLibrary
		.inputs = {
			// Input buffers
			buffers: {
				vertex: { name:"inVertex", dataType:"vec3", role:"buffer", target:"vertex" },	// role:buffer => attribute
				normal: { name:"inNormal", dataType:"vec3", role:"buffer", target:"normal" },
				tangent: { name:"inTangent", dataType:"vec3", role:"buffer", target:"tangent" },
				tex0: { name:"inTex0", dataType:"vec2", role:"buffer", target:"tex0" },
				tex1: { name:"inTex1", dataType:"vec2", role:"buffer", target:"tex1" },
				tex2: { name:"inTex2", dataType:"vec2", role:"buffer", target:"tex2" },
				color: { name:"inColor", dataType:"vec4", role:"buffer", target:"color" }
			},
			
			// Matrixes
			matrix: {
				model: { name:"inModelMatrix", dataType:"mat4", role:"value" },	// role:value => uniform
				view: { name:"inViewMatrix", dataType:"mat4", role:"value" },
				projection: { name:"inProjectionMatrix", dataType:"mat4", role:"value" },
				normal: { name:"inNormalMatrix", dataType:"mat4", role:"value" },
				viewInv: { name:"inViewMatrixInv", dataType:"mat4", role:"value" }
			},
			
			///// Material properties
			material: {
				// 		Color
				diffuse: { name:"inDiffuseColor", dataType:"vec4", role:"value" },
				specular: { name:"inSpecularColor", dataType:"vec4", role:"value" },
				
				// 		Shininess
				shininess: { name:"inShininess", dataType:"float", role:"value" },
				shininessMask: { name:"inShininessMask", dataType:"sampler2D", role:"value" },
				shininessMaskChannel: { name:"inShininessMaskChannel", dataType:"vec4", role:"value" },
				shininessMaskInvert: { name:"inShininessMaskInvert", dataType:"bool", role:"value" },
				
				// 		Light emission
				lightEmission: { name:"inLightEmission", dataType:"float", role:"value" },
				lightEmissionMask: { name:"inLightEmissionMask", dataType:"sampler2D", role:"value" },
				lightEmissionMaskChannel: { name:"inLightEmissionMaskChannel", dataType:"vec4", role:"value" },
				lightEmissionMaskInvert: { name:"inLightEmissionMaskInvert", dataType:"bool", role:"value" },
				
				
				// 		Textures
				texture: { name:"inTexture", dataType:"sampler2D", role:"value" },
				textureOffset: { name:"inTextureOffset", dataType:"vec2", role:"value" },
				textureScale: { name:"inTextureScale", dataType:"vec2", role:"value" },
				alphaCutoff: { name:"inAlphaCutoff", dataType:"float", role:"value" },
				
				lightMap: { name:"inLightMap", dataType:"sampler2D", role:"value" },
				lightMapOffset: { name:"inLightMapOffset", dataType:"vec2", role:"value" },
				lightMapScale: { name:"inLightMapScale", dataType:"vec2", role:"value" },
				
				normalMap: { name:"inNormalMap", dataType:"sampler2D", role:"value" },
				normalMapOffset: { name:"inNormalMapOffset", dataType:"vec2", role:"value" },
				normalMapScale: { name:"inNormalMapScale", dataType:"vec2", role:"value" },
				
				//		Reflection
				reflection: { name:"inReflection", dataType:"float", role:"value" },
				reflectionMask: { name:"inReflectionMask", dataType:"sampler2D", role:"value" },
				reflectionMaskChannel: { name:"inReflectionMaskChannel", dataType:"vec4", role:"value" },
				reflectionMaskInvert: { name:"inReflectionMaskInvert", dataType:"bool", role:"value" },
				
				//		Shadows
				castShadows: { name:"inCastShadows", dataType:"bool", role:"value" },
				receiveShadows: { name:"inReceiveShadows", dataType:"bool", role:"value" },

				//		Roughness
				roughness: { name:"inRoughness", dataType:"float", role:"value" },
				roughnessMask: { name:"inRoughnessMask", dataType:"sampler2D", role:"value" },
				roughnessMaskChannel: { name:"inRoughnessMaskChannel", dataType:"vec4", role:"value" },
				roughnessMaskInvert: { name:"inRoughnessMaskInvert", dataType:"bool", role:"value" },

				unlit: { name:"inUnlit", dataType:"bool", role:"value" }
			},
			
			// Lighting
			lighting: {
				type: { name:"inLightType", dataType:"int", role:"value" },
				position: { name:"inLightPosition", dataType:"vec3", role:"value" },
				direction: { name:"inLightDirection", dataType:"vec3", role:"value" },
				ambient: { name:"inLightAmbient", dataType:"vec4", role:"value" },
				diffuse: { name:"inLightDiffuse", dataType:"vec4", role:"value" },
				specular: { name:"inLightSpecular", dataType:"vec4", role:"value" },
				attenuation: { name:"inLightAttenuation", dataType:"vec3", role:"value" },	// const, linear, exp
				spotExponent: { name:"inSpotExponent", dataType:"float", role:"value" },
				spotCutoff: { name:"inSpotCutoff", dataType:"float", role:"value" },
				cutoffDistance: { name:"inLightCutoffDistance", dataType:"float", role:"value" },
				exposure: { name:"inLightExposure", dataType:"float", role:"value" },
				castShadows: { name:"inLightCastShadows", dataType:"bool", role:"value" }
			},

			lightingForward: {
				type: { name:"inLightType", dataType:"int", role:"value", vec:bg.base.MAX_FORWARD_LIGHTS },
				position: { name:"inLightPosition", dataType:"vec3", role:"value", vec:bg.base.MAX_FORWARD_LIGHTS },
				direction: { name:"inLightDirection", dataType:"vec3", role:"value", vec:bg.base.MAX_FORWARD_LIGHTS },
				ambient: { name:"inLightAmbient", dataType:"vec4", role:"value", vec:bg.base.MAX_FORWARD_LIGHTS },
				diffuse: { name:"inLightDiffuse", dataType:"vec4", role:"value", vec:bg.base.MAX_FORWARD_LIGHTS },
				specular: { name:"inLightSpecular", dataType:"vec4", role:"value", vec:bg.base.MAX_FORWARD_LIGHTS },
				attenuation: { name:"inLightAttenuation", dataType:"vec3", role:"value", vec:bg.base.MAX_FORWARD_LIGHTS },	// const, linear, exp
				spotExponent: { name:"inSpotExponent", dataType:"float", role:"value", vec:bg.base.MAX_FORWARD_LIGHTS },
				spotCutoff: { name:"inSpotCutoff", dataType:"float", role:"value", vec:bg.base.MAX_FORWARD_LIGHTS },
				cutoffDistance: { name:"inLightCutoffDistance", dataType:"float", role:"value", vec:bg.base.MAX_FORWARD_LIGHTS },
				exposure: { name:"inLightExposure", dataType:"float", role:"value", vec:bg.base.MAX_FORWARD_LIGHTS },
				castShadows: { name:"inLightCastShadows", dataType:"bool", role:"value", vec:bg.base.MAX_FORWARD_LIGHTS },
				numLights: { name:"inNumLights", dataType:"int", role:"value" }
			},
			
			// Shadows
			shadows: {
				shadowMap: { name:"inShadowMap", dataType:"sampler2D", role:"value" },
				shadowMapSize: { name:"inShadowMapSize", dataType:"vec2", role:"value" },
				shadowStrength: { name:"inShadowStrength", dataType:"float", role:"value" },
				shadowColor: { name:"inShadowColor", dataType:"vec4", role:"value" },
				shadowBias: { name:"inShadowBias", dataType:"float", role:"value" },
				shadowType: { name:"inShadowType", dataType:"int", role:"value" }
			},
			
			// Color correction
			colorCorrection: {
				hue: { name:"inHue", dataType:"float", role:"value" },
				saturation: { name:"inSaturation", dataType:"float", role:"value" },
				lightness: { name:"inLightness", dataType:"float", role:"value" },
				brightness: { name:"inBrightness", dataType:"float", role:"value" },
				contrast: { name:"inContrast", dataType:"float", role:"value" }
			}

		}
})();
(function() {
	
	bg.webgl1.shaderLibrary
		.functions
		.lighting = {
			beckmannDistribution: {
				returnType:"float", name:"beckmannDistribution", params: {
					x:"float", roughness:"float"
				}, body: `
					float NdotH = max(x,0.0001);
					float cos2Alpha = NdotH * NdotH;
					float tan2Alpha = (cos2Alpha - 1.0) / cos2Alpha;
					float roughness2 = roughness * roughness;
					float denom = 3.141592653589793 * roughness2 * cos2Alpha * cos2Alpha;
					return exp(tan2Alpha / roughness2) / denom;
				`
			},

			beckmannSpecular: {
				returnType:"float", name:"beckmannSpecular", params:{
					lightDirection:"vec3", viewDirection:"vec3", surfaceNormal:"vec3", roughness:"float"
				}, body: `
					return beckmannDistribution(dot(surfaceNormal, normalize(lightDirection + viewDirection)), roughness);
				`
			},

			getDirectionalLight: {
				returnType:"vec4", name:"getDirectionalLight", params:{
					ambient:"vec4", diffuse:"vec4", specular:"vec4", shininess:"float",
					direction:"vec3", vertexPos:"vec3", normal:"vec3", matDiffuse:"vec4", matSpecular:"vec4",
					shadowColor:"vec4",
					specularOut:"out vec4"
				}, body: `
				vec3 color = ambient.rgb * matDiffuse.rgb;
				vec3 diffuseWeight = max(0.0, dot(normal,direction)) * diffuse.rgb;
				color += min(diffuseWeight,shadowColor.rgb) * matDiffuse.rgb;
				specularOut = vec4(0.0,0.0,0.0,1.0);
				if (shininess>0.0) {
					vec3 eyeDirection = normalize(-vertexPos);
					vec3 reflectionDirection = normalize(reflect(-direction,normal));
					float specularWeight = clamp(pow(max(dot(reflectionDirection, eyeDirection), 0.0), shininess), 0.0, 1.0);
					//sspecularWeight = beckmannSpecular(direction,eyeDirection,normal,0.01);
					vec3 specularColor = specularWeight * pow(shadowColor.rgb,vec3(10.0));
					//color += specularColor * specular.rgb * matSpecular.rgb;
					specularOut = vec4(specularColor * specular.rgb * matSpecular.rgb,1.0);
				}
				return vec4(color,1.0);`
			},

			getPointLight: {
				returnType:"vec4", name:"getPointLight", params: {
					ambient: "vec4", diffuse: "vec4", specular: "vec4", shininess: "float",
					position: "vec3", constAtt: "float", linearAtt: "float", expAtt: "float",
					vertexPos: "vec3", normal: "vec3", matDiffuse: "vec4", matSpecular: "vec4",
					specularOut:"out vec4"
				}, body: `
				vec3 pointToLight = position - vertexPos;
				float distance = length(pointToLight);
				vec3 lightDir = normalize(pointToLight);
				float attenuation = 1.0 / (constAtt + linearAtt * distance + expAtt * distance * distance);
				vec3 color = ambient.rgb * matDiffuse.rgb;
				vec3 diffuseWeight = max(0.0,dot(normal,lightDir)) * diffuse.rgb * attenuation;
				color += diffuseWeight * matDiffuse.rgb;
				specularOut = vec4(0.0,0.0,0.0,1.0);
				if (shininess>0.0) {
					vec3 eyeDirection = normalize(-vertexPos);
					vec3 reflectionDirection = normalize(reflect(-lightDir, normal));
					float specularWeight = clamp(pow(max(dot(reflectionDirection,eyeDirection),0.0), shininess), 0.0, 1.0);
					//color += specularWeight * specular.rgb * matSpecular.rgb * attenuation;
					specularOut = vec4(specularWeight * specular.rgb * matSpecular.rgb * attenuation,1.0);
				}
				return vec4(color,1.0);`
			},

			getSpotLight: {
				returnType:"vec4", name:"getSpotLight", params: {
					ambient:"vec4", diffuse:"vec4", specular:"vec4", shininess:"float",
					position:"vec3", direction:"vec3",
					constAtt:"float", linearAtt:"float", expAtt:"float",
					spotCutoff:"float", spotExponent:"float",
					vertexPos:"vec3", normal:"vec3",
					matDiffuse:"vec4", matSpecular:"vec4", shadowColor:"vec4",
					specularOut:"out vec4"
				}, body: `
				vec4 matAmbient = vec4(1.0);
				vec3 s = normalize(position - vertexPos);
				float angle = acos(dot(-s, direction));
				float cutoff = radians(clamp(spotCutoff / 2.0,0.0,90.0));
				float distance = length(position - vertexPos);
				float attenuation = 1.0 / (constAtt );//+ linearAtt * distance + expAtt * distance * distance);
				if (angle<cutoff) {
					float spotFactor = pow(dot(-s, direction), spotExponent);
					vec3 v = normalize(vec3(-vertexPos));
					vec3 h = normalize(v + s);
					vec3 diffuseAmount = matDiffuse.rgb * diffuse.rgb * max(dot(s, normal), 0.0);
					specularOut = vec4(0.0,0.0,0.0,1.0);
					if (shininess>0.0) {
						specularOut.rgb = matSpecular.rgb * specular.rgb * pow(max(dot(h,normal), 0.0),shininess);
						specularOut.rgb *= pow(shadowColor.rgb,vec3(10.0));
						//diffuseAmount += matSpecular.rgb * specular.rgb * pow(max(dot(h,normal), 0.0),shininess);
						//diffuseAmount *= pow(shadowColor.rgb,vec3(10.0));
					}
					diffuseAmount.r = min(diffuseAmount.r, shadowColor.r);
					diffuseAmount.g = min(diffuseAmount.g, shadowColor.g);
					diffuseAmount.b = min(diffuseAmount.b, shadowColor.b);
					return vec4(ambient.rgb * matDiffuse.rgb + attenuation * spotFactor * diffuseAmount,1.0);
				}
				else {
					return vec4(ambient.rgb * matDiffuse.rgb,1.0);
				}`
			},

			getLight: {
				returnType:"vec4", name:"getLight", params: {
					lightType:"int",
					ambient:"vec4", diffuse:"vec4", specular:"vec4", shininess:"float",
					lightPosition:"vec3", lightDirection:"vec3",
					constAtt:"float", linearAtt:"float", expAtt:"float",
					spotCutoff:"float", spotExponent:"float",cutoffDistance:"float",
					vertexPosition:"vec3", vertexNormal:"vec3",
					matDiffuse:"vec4", matSpecular:"vec4", shadowColor:"vec4",
					specularOut:"out vec4"
				}, body: `
					vec4 light = vec4(0.0);
					if (lightType==${ bg.base.LightType.DIRECTIONAL }) {
						light = getDirectionalLight(ambient,diffuse,specular,shininess,
										-lightDirection,vertexPosition,vertexNormal,matDiffuse,matSpecular,shadowColor,specularOut);
					}
					else if (lightType==${ bg.base.LightType.SPOT }) {
						float d = distance(vertexPosition,lightPosition);
						if (d<=cutoffDistance || cutoffDistance==-1.0) {
							light = getSpotLight(ambient,diffuse,specular,shininess,
											lightPosition,lightDirection,
											constAtt,linearAtt,expAtt,
											spotCutoff,spotExponent,
											vertexPosition,vertexNormal,matDiffuse,matSpecular,shadowColor,specularOut);
						}
					}
					else if (lightType==${ bg.base.LightType.POINT }) {
						float d = distance(vertexPosition,lightPosition);
						if (d<=cutoffDistance || cutoffDistance==-1.0) {
							light = getPointLight(ambient,diffuse,specular,shininess,
											lightPosition,
											constAtt,linearAtt,expAtt,
											vertexPosition,vertexNormal,matDiffuse,matSpecular,specularOut);
						}
					}
					return light;
				`
			},
			
			getShadowColor:{
				returnType:"vec4", name:"getShadowColor", params:{
					vertexPosFromLight:'vec4', shadowMap:'sampler2D', shadowMapSize:'vec2',
					shadowType:'int', shadowStrength:'float', shadowBias:'float', shadowColor:'vec4'
				}, body:`
				float visibility = 1.0;
				vec3 depth = vertexPosFromLight.xyz / vertexPosFromLight.w;
				const float kShadowBorderOffset = 3.0;
				float shadowBorderOffset = kShadowBorderOffset / shadowMapSize.x;
				float bias = shadowBias;
				vec4 shadow = vec4(1.0);
				if (shadowType==${ bg.base.ShadowType.HARD }) {	// hard
					float shadowDepth = unpack(texture2D(shadowMap,depth.xy));
					if (shadowDepth<depth.z - bias &&
						(depth.x>0.0 && depth.x<1.0 && depth.y>0.0 && depth.y<1.0))
					{
						visibility = 1.0 - shadowStrength;
					}
					shadow = clamp(shadowColor + visibility,0.0,1.0);
				}
				else if (shadowType>=${ bg.base.ShadowType.SOFT }) {	// soft / soft stratified (not supported on webgl, fallback to soft)
					vec2 poissonDisk[4];
					poissonDisk[0] = vec2( -0.94201624, -0.39906216 );
					poissonDisk[1] = vec2( 0.94558609, -0.76890725 );
					poissonDisk[2] = vec2( -0.094184101, -0.92938870 );
					poissonDisk[3] = vec2( 0.34495938, 0.29387760 );
					
					for (int i=0; i<4; ++i) {
						float shadowDepth = unpack(texture2D(shadowMap, depth.xy + poissonDisk[i]/1000.0));
						
						if (shadowDepth<depth.z - bias
							&& (depth.x>0.0 && depth.x<1.0 && depth.y>0.0 && depth.y<1.0)) {
							visibility -= (shadowStrength) * 0.25;
						}
					}
					shadow = clamp(shadowColor + visibility,0.0,1.0);
				}
				return shadow;`
			}
		}	
})();
(function() {
	
	bg.webgl1.shaderLibrary
		.functions
		.materials = {
			samplerColor: {
				returnType:"vec4", name:"samplerColor", params: {
					sampler:"sampler2D",
					uv:"vec2", offset:"vec2", scale:"vec2"
				}, body:`
				return texture2D(sampler,uv * scale + offset);`
			},
			
			samplerNormal: {
				returnType:"vec3", name:"samplerNormal", params:{
					sampler:"sampler2D",
					uv:"vec2", offset:"vec2", scale:"vec2"
				}, body:`
				return normalize(samplerColor(sampler,uv,offset,scale).xyz * 2.0 - 1.0);
				`
			},
			
			combineNormalWithMap:{
				returnType:"vec3", name:"combineNormalWithMap", params:{
					normalCoord:"vec3",
					tangent:"vec3",
					bitangent:"vec3",
					normalMapValue:"vec3"
				}, body:`
				mat3 tbnMat = mat3( tangent.x, bitangent.x, normalCoord.x,
							tangent.y, bitangent.y, normalCoord.y,
							tangent.z, bitangent.z, normalCoord.z
						);
				return normalize(normalMapValue * tbnMat);`
			},
			
			applyTextureMask:{
				returnType:"float", name:"applyTextureMask", params: {
					value:"float",
					textureMask:"sampler2D",
					uv:"vec2", offset:"vec2", scale:"vec2",
					channelMask:"vec4",
					invert:"bool"
				}, body:`
				float mask;
				vec4 color = samplerColor(textureMask,uv,offset,scale);
				mask = color.r * channelMask.r +
						 color.g * channelMask.g +
						 color.b * channelMask.b +
						 color.a * channelMask.a;
				if (invert) {
					mask = 1.0 - mask;
				}
				return value * mask;`
			},
			
			specularColor:{
				returnType:"vec4", name:"specularColor", params:{
					specular:"vec4",
					shininessMask:"sampler2D",
					uv:"vec2", offset:"vec2", scale:"vec2",
					channelMask:"vec4",
					invert:"bool"
				}, body:`
				float maskValue = applyTextureMask(1.0, shininessMask,uv,offset,scale,channelMask,invert);
				return vec4(specular.rgb * maskValue, 1.0);`
			}
		};
	
})();

(function() {
    bg.webgl1.shaderLibrary
        .functions
        .pbr = {};
        
    bg.webgl1.shaderLibrary
        .inputs
        .pbr = {};

    // This file includes all the new pbr shader functions
    bg.webgl1.shaderLibrary
        .functions
        .pbr = {
        lighting: {
            processLight: {
                returnType: "vec4", name:"processLight", params: {
                    inAmbient:"vec3", inDiffuse:"vec3", inSpecular:"vec3", type:"int", specularType:"int",
                    direction:"vec3", position:"vec3",
                    inAttenuation:"vec3", spotCutoff:"float", spotOuterCutoff:"float",
                    surfaceNormal:"vec3", surfacePosition:"vec3", surfaceDiffuse:"vec3",
                    shadowColor:"vec3"
                },
                body:`
                    vec3 diffuse = vec3(0.0);
                    vec3 specular = vec3(0.0);
                    float attenuation = 1.0;
                    float shininess = 64.0;
                    if (type==${ bg.base.LightType.DIRECTIONAL }) {
                        vec3 lightDir = normalize(-direction);
                        diffuse = max(dot(normalize(surfaceNormal), lightDir), 0.0) * surfaceDiffuse;
                        vec3 viewDir = normalize(vec3(0.0) - surfacePosition);
                        vec3 reflectDir = reflect(-lightDir, surfaceNormal);
                        float spec = 0.0;
                        if (specularType==${ bg.base.SpecularType.PHONG}) {
                            spec = pow(max(dot(viewDir, reflectDir), 0.0), shininess);
                        }
                        else {
                            vec3 halfwayDir = normalize(lightDir + viewDir);
                            spec = pow(max(dot(surfaceNormal, halfwayDir), 0.0), shininess);
                        }
                        specular = spec * inSpecular;
                        specular *= pow(shadowColor,vec3(10.0));
                    }
                    else if (type==${ bg.base.LightType.POINT }) {
                        vec3 lightDir = normalize(position - surfacePosition);
                        float distance = length(position - surfacePosition);
                        attenuation = 1.0 / (
                            inAttenuation.x +
                            inAttenuation.y * distance +
                            inAttenuation.z * distance * distance
                        );
                        diffuse = max(dot(normalize(surfaceNormal), lightDir), 0.0) * inDiffuse;
                        vec3 viewDir = normalize(vec3(0.0) - surfacePosition);
                        vec3 reflectDir = reflect(-lightDir, surfaceNormal);

                        float spec = 0.0;
                        if (specularType==${ bg.base.SpecularType.PHONG}) {
                            spec = pow(max(dot(viewDir, reflectDir), 0.0), shininess);
                        }
                        else {
                            vec3 halfwayDir = normalize(lightDir + viewDir);
                            spec = pow(max(dot(surfaceNormal, halfwayDir), 0.0), shininess);
                        }

                        specular = spec * inSpecular;
                    }
                    else if (type==${ bg.base.LightType.SPOT }) {
                        vec3 lightDirToSurface = normalize(position - surfacePosition);
                        float theta = dot(lightDirToSurface,normalize(-direction));
                        if (theta > spotCutoff) {
                            float epsilon = spotCutoff - spotOuterCutoff;
                            float intensity = 1.0 - clamp((theta - spotOuterCutoff) / epsilon, 0.0, 1.0);
                            float distance = length(position - surfacePosition);
                            diffuse = max(dot(normalize(surfaceNormal), lightDirToSurface), 0.0) * inDiffuse * intensity;
                            vec3 viewDir = normalize(vec3(0.0) - surfacePosition);
                            vec3 reflectDir = reflect(-lightDirToSurface, surfaceNormal);
                            float spec = 0.0;
                            if (specularType==${ bg.base.SpecularType.PHONG}) {
                                spec = pow(max(dot(viewDir, reflectDir), 0.0), shininess);
                            }
                            else {
                                vec3 halfwayDir = normalize(lightDirToSurface + viewDir);
                                spec = pow(max(dot(surfaceNormal, halfwayDir), 0.0), shininess);
                            }
                            specular = spec * inSpecular * intensity;
                            specular *= pow(shadowColor,vec3(10.0));
                        }
                    }
                    vec3 shadowAmbient = clamp(shadowColor,inAmbient,vec3(1.0));
                    diffuse = min(diffuse,shadowAmbient);
                    return vec4((inAmbient + diffuse + specular) * attenuation,1.0);
                `
            },
            
            getShadowColor:{
				returnType:"vec4", name:"getShadowColor", params:{
					vertexPosFromLight:'vec4', shadowMap:'sampler2D', shadowMapSize:'vec2',
					shadowType:'int', shadowStrength:'float', shadowBias:'float', shadowColor:'vec4'
				}, body:`
				float visibility = 1.0;
				vec3 depth = vertexPosFromLight.xyz / vertexPosFromLight.w;
				const float kShadowBorderOffset = 3.0;
				float shadowBorderOffset = kShadowBorderOffset / shadowMapSize.x;
				float bias = shadowBias;
                vec4 shadow = vec4(1.0);
                
                float shadowDepth = unpack(texture2D(shadowMap,depth.xy));

				if (shadowType==${ bg.base.ShadowType.HARD }) {	// hard
                    float shadowDepth = unpack(texture2D(shadowMap,depth.xy));
					if (shadowDepth<depth.z - bias &&
						(depth.x>0.0 && depth.x<1.0 && depth.y>0.0 && depth.y<1.0))
					{
						visibility = 1.0 - shadowStrength;
					}
					shadow = clamp(shadowColor + visibility,0.0,1.0);
				}
				else if (shadowType>=${ bg.base.ShadowType.SOFT }) {	// soft / soft stratified (not supported on webgl, fallback to soft)
					vec2 poissonDisk[7];
                    poissonDisk[0] = vec2( -0.54201624, -0.19906216 );
                    poissonDisk[1] = vec2( 0.54558609, -0.46890725 );
                    poissonDisk[2] = vec2( -0.054184101, -0.52938870 );
                    poissonDisk[3] = vec2( 0.02495938, -0.14387760 );
                    poissonDisk[4] = vec2( -0.24495938, 0.14387760 );
                    poissonDisk[5] = vec2( 0.3495938, -0.74387760 );
                    poissonDisk[6] = vec2( -0.07495938, 0.54387760 );
                    
                    for (int i=0; i<7; ++i) {
                        float shadowDepth = unpack(texture2D(shadowMap, depth.xy + poissonDisk[i]/3000.0));
                        
                        if (shadowDepth<depth.z - bias
                            && (depth.x>0.0 && depth.x<1.0 && depth.y>0.0 && depth.y<1.0)) {
                            visibility -= (shadowStrength) * 0.14;
                        }
                    }
                    shadow = clamp(shadowColor + visibility,0.0,1.0);
				}
				return shadow;`
			}
        },

        material: {
            samplerColor: {
                returnType: "vec4", name:"samplerColor", params: {
                    sampler:"sampler2D",
                    uv:"vec2", offset:"vec2", scale:"vec2"
                }, body: `
                return texture2D(sampler,uv*scale+offset);`
            },

            samplerNormal: {
				returnType:"vec3", name:"samplerNormal", params:{
					sampler:"sampler2D",
					uv:"vec2", offset:"vec2", scale:"vec2"
				}, body:`
				return normalize(samplerColor(sampler,uv,offset,scale).xyz * 2.0 - 1.0);`
            },

            combineNormalWithMap:{
				returnType:"vec3", name:"combineNormalWithMap", params:{
					normalCoord:"vec3",
					tangent:"vec3",
					bitangent:"vec3",
					normalMapValue:"vec3"
				}, body:`
				mat3 tbnMat = mat3( tangent.x, bitangent.x, normalCoord.x,
							tangent.y, bitangent.y, normalCoord.y,
							tangent.z, bitangent.z, normalCoord.z
						);
				return normalize(normalMapValue * tbnMat);`
            },
            
            parallaxMapping: {
                returnType: "vec2", name:"parallaxMapping", params: {
                    height:"float", texCoords:"vec2", viewDir:"vec3", scale:"float", texScale:"vec2"
                }, body: `
                float height_scale = (0.01 * scale) / ((texScale.x + texScale.y) / 2.0);
                vec2 p = viewDir.xy / viewDir.z;
                p.x *= -1.0;
                p *= (height * height_scale);
                return texCoords - p;
                `
            }
        },

        utils: {
            pack: {
				returnType:"vec4", name:"pack", params:{ depth:"float" }, body: `
				const vec4 bitSh = vec4(256 * 256 * 256,
										256 * 256,
										256,
										1.0);
				const vec4 bitMsk = vec4(0,
										1.0 / 256.0,
										1.0 / 256.0,
										1.0 / 256.0);
				vec4 comp = fract(depth * bitSh);
				comp -= comp.xxyz * bitMsk;
				return comp;`
			},
			
			unpack: {
				returnType:"float", name:"unpack", params:{ color:"vec4" }, body:`
				const vec4 bitShifts = vec4(1.0 / (256.0 * 256.0 * 256.0),
											1.0 / (256.0 * 256.0),
											1.0 / 256.0,
											1.0);
				return dot(color, bitShifts);`
			},
			
			random: {
				returnType:"float", name:"random", params:{
					seed:"vec3", i:"int"
				}, body:`
				vec4 seed4 = vec4(seed,i);
				float dot_product = dot(seed4, vec4(12.9898,78.233,45.164,94.673));
				return fract(sin(dot_product) * 43758.5453);`
            },
            
            gammaCorrection: {
                returnType:"vec4", name:"gammaCorrection", params:{
                    color:"vec4", gamma:"float"
                }, body:`
                return pow(color,vec4(1.0/gamma));
                `
            },

            inverseGammaCorrection: {
                returnType:"vec4", name:"inverseGammaCorrection", params: {
                    color:"vec4", gamma:"float"
                }, body:`
                return pow(color,vec4(gamma));
                `
            }
        }
    }
        

    bg.webgl1.shaderLibrary
        .inputs
        .pbr.material = {
            diffuse: { name:"inDiffuse", dataType:"sampler2D", role:"value" },
            diffuseOffset: { name:"inDiffuseOffset", dataType:"vec2", role:"value" },
            diffuseScale: { name:"inDiffuseScale", dataType:"vec2", role:"value" },
            alphaCutoff: { name:"inAlphaCutoff", dataType:"float", fole:"value" },

            height: { name:"inHeight", dataType:"sampler2D", role:"value" },
            metallic: { name:"inMetallic", dataType:"sampler2D", role:"value" },
            roughness: { name:"inRoughness", dataType:"sampler2D", role:"value" },

            heighMetallicRoughnessAO: { name:"inHeighMetallicRoughnessAO", dataType:"sampler2D", role:"value" },

            normal: { name:"inNormalMap", dataType:"sampler2D", role:"value" },
            normalOffset: { name:"inNormalOffset", dataType:"vec2", role:"value" },
            normalScale: { name:"inNormalScale", dataType:"vec2", role:"value" },

            receiveShadows: { name:"inReceiveShadows", dataType:"bool", role:"value" },
            heightScale: { name:"inHeightScale", dataType:"float", role:"value" }
        };

    bg.webgl1.shaderLibrary
        .inputs
        .pbr.lighting = {
            ambient: { name:"inLightAmbient", dataType:"vec4", role:"value" }
        };

    
    bg.webgl1.shaderLibrary
        .inputs
        .pbr.lightingForward = {
            lightType: { name:"inLightType", dataType:"int", role:"value", vec:bg.base.MAX_FORWARD_LIGHTS },
            diffuse: { name:"inLightDiffuse", dataType:"vec4", role:"value", vec:bg.base.MAX_FORWARD_LIGHTS },
            specular: { name:"inLightSpecular", dataType:"vec4", role:"value", vec:bg.base.MAX_FORWARD_LIGHTS },
            position: { name:"inLightPosition", dataType:"vec3", role:"value", vec:bg.base.MAX_FORWARD_LIGHTS },
            direction: { name:"inLightDirection", dataType:"vec3", role:"value", vec:bg.base.MAX_FORWARD_LIGHTS },
            intensity: { name:"inLightIntensity", dataType:"float", role:"value", vec:bg.base.MAX_FORWARD_LIGHTS },
            spotCutoff: { name:"inLightSpotCutoff", dataType:"float", role:"value", vec:bg.base.MAX_FORWARD_LIGHTS },
            outerSpotCutoff: { name:"inLightOuterSpotCutoff", dataType:"float", role:"value", vec:bg.base.MAX_FORWARD_LIGHTS }
        };

    bg.webgl1.shaderLibrary
        .inputs
        .pbr.shadows = {
            shadowMap: { name:"inShadowMap", dataType:"sampler2D", role:"value" },
            shadowMapSize: { name:"inShadowMapSize", dataType:"vec2", role:"value" },
            shadowStrength: { name:"inShadowStrength", dataType:"float", role:"value" },
            shadowColor: { name:"inShadowColor", dataType:"vec4", role:"value" },
            shadowBias: { name:"inShadowBias", dataType:"float", role:"value" },
            shadowType: { name:"inShadowType", dataType:"int", role:"value" }
        };
})();

(function() {
	
	bg.webgl1.shaderLibrary
		.functions
		.utils = {
			pack: {
				returnType:"vec4", name:"pack", params:{ depth:"float" }, body: `
				const vec4 bitSh = vec4(256 * 256 * 256,
										256 * 256,
										256,
										1.0);
				const vec4 bitMsk = vec4(0,
										1.0 / 256.0,
										1.0 / 256.0,
										1.0 / 256.0);
				vec4 comp = fract(depth * bitSh);
				comp -= comp.xxyz * bitMsk;
				return comp;`
			},
			
			unpack: {
				returnType:"float", name:"unpack", params:{ color:"vec4" }, body:`
				const vec4 bitShifts = vec4(1.0 / (256.0 * 256.0 * 256.0),
											1.0 / (256.0 * 256.0),
											1.0 / 256.0,
											1.0);
				return dot(color, bitShifts);`
			},
			
			random: {
				returnType:"float", name:"random", params:{
					seed:"vec3", i:"int"
				}, body:`
				vec4 seed4 = vec4(seed,i);
				float dot_product = dot(seed4, vec4(12.9898,78.233,45.164,94.673));
				return fract(sin(dot_product) * 43758.5453);`
			},

			texOffset: {
				returnType: 'vec4', name:'texOffset', params: {
					sampler:'sampler2D',
					texCoord:'vec2',
					offset:'vec2',
					frameSize:'vec2'
				}, body: `
				return texture2D(sampler,texCoord + vec2(offset.x * 1.0/frameSize.x,offset.y * 1.0 / frameSize.y));
				`
			},

			luminance: {
				returnType: 'float', name:'luminance', params: { color:'vec3' }, body: `
				return dot(vec3(0.2126,0.7152,0.0722), color);
				`
			},

			// Require: colorCorrection.luminance and utils.texOffset
			borderDetection:{
				returnType: 'float', name:'borderDetection', params: {
					sampler:'sampler2D', texCoord:'vec2', frameSize:'vec2'
				}, body: `
				float s00 = luminance(texOffset(sampler,texCoord,vec2(-1.0, 1.0),frameSize).rgb);
				float s10 = luminance(texOffset(sampler,texCoord,vec2(-1.0, 0.0),frameSize).rgb);
				float s20 = luminance(texOffset(sampler,texCoord,vec2(-1.0,-1.0),frameSize).rgb);
				float s01 = luminance(texOffset(sampler,texCoord,vec2(-1.0, 1.0),frameSize).rgb);
				float s21 = luminance(texOffset(sampler,texCoord,vec2( 0.0,-1.0),frameSize).rgb);
				float s02 = luminance(texOffset(sampler,texCoord,vec2( 1.0, 1.0),frameSize).rgb);
				float s12 = luminance(texOffset(sampler,texCoord,vec2( 1.0, 0.0),frameSize).rgb);
				float s22 = luminance(texOffset(sampler,texCoord,vec2( 1.0,-1.0),frameSize).rgb);

				float sx = s00 + 2.0 * s10 + s20 - (s02 + 2.0 * s12 + s22);
				float sy = s00 + 2.0 * s01 + s02 - (s20 + 2.0 * s21 + s22);

				return sx * sx + sy * sy;
				`
			},

			applyConvolution:{
				returnType:'vec4', name:'applyConvolution', params: {
					texture:'sampler2D', texCoord:'vec2', texSize:'vec2', convMatrix:'float[9]', radius:'float'
				}, body: `
				vec2 onePixel = vec2(1.0,1.0) / texSize * radius;
				vec4 colorSum = 
					texture2D(texture, texCoord + onePixel * vec2(-1, -1)) * convMatrix[0] +
					texture2D(texture, texCoord + onePixel * vec2( 0, -1)) * convMatrix[1] +
					texture2D(texture, texCoord + onePixel * vec2( 1, -1)) * convMatrix[2] +
					texture2D(texture, texCoord + onePixel * vec2(-1,  0)) * convMatrix[3] +
					texture2D(texture, texCoord + onePixel * vec2( 0,  0)) * convMatrix[4] +
					texture2D(texture, texCoord + onePixel * vec2( 1,  0)) * convMatrix[5] +
					texture2D(texture, texCoord + onePixel * vec2(-1,  1)) * convMatrix[6] +
					texture2D(texture, texCoord + onePixel * vec2( 0,  1)) * convMatrix[7] +
					texture2D(texture, texCoord + onePixel * vec2( 1,  1)) * convMatrix[8];
				float kernelWeight =
					convMatrix[0] +
					convMatrix[1] +
					convMatrix[2] +
					convMatrix[3] +
					convMatrix[4] +
					convMatrix[5] +
					convMatrix[6] +
					convMatrix[7] +
					convMatrix[8];
				if (kernelWeight <= 0.0) {
					kernelWeight = 1.0;
				}
				return vec4((colorSum / kernelWeight).rgb, 1.0);
				`
			}
		}	
})();
(function() {
	
	class ShaderImpl extends bg.base.ShaderImpl {
		initFlags(context) {
			bg.base.ShaderType.VERTEX = context.VERTEX_SHADER;
			bg.base.ShaderType.FRAGMENT = context.FRAGMENT_SHADER;
		}
		
		setActive(context,shaderProgram) {
			context.useProgram(shaderProgram && shaderProgram.program);
		}
		
		create(context) {
			return {
				program:context.createProgram(),
				attribLocations:{},
				uniformLocations:{}
			};
		}
		
		addShaderSource(context,shaderProgram,shaderType,source) {
			let error = null;
			
			if (!shaderProgram || !shaderProgram.program ) {
				error = "Could not attach shader. Invalid shader program";
			}
			else {
				let shader = context.createShader(shaderType);
				context.shaderSource(shader,source);
				context.compileShader(shader);
				
				if (!context.getShaderParameter(shader, context.COMPILE_STATUS)) {
					error = context.getShaderInfoLog(shader);
				}
				else {
					context.attachShader(shaderProgram.program,shader);
				}
				
				context.deleteShader(shader);
			}
			
			return error;
		}
		
		link(context,shaderProgram) {
			let error = null;
			if (!shaderProgram || !shaderProgram.program ) {
				error = "Could not link shader. Invalid shader program";
			}
			else {
				context.linkProgram(shaderProgram.program);
				if (!context.getProgramParameter(shaderProgram.program,context.LINK_STATUS)) {
					error = context.getProgramInfoLog(shaderProgram.program);
				}
			}
			return error;
		}
		
		initVars(context,shader,inputBufferVars,valueVars) {
			inputBufferVars.forEach(function(name) {
				shader.attribLocations[name] = context.getAttribLocation(shader.program,name);
			});
			
			valueVars.forEach(function(name) {
				shader.uniformLocations[name] = context.getUniformLocation(shader.program,name);
			});
		}

		setInputBuffer(context,shader,varName,vertexBuffer,itemSize) {
			if (vertexBuffer && shader && shader.program) {
				let loc = shader.attribLocations[varName];
				if (loc!=-1) {
					context.bindBuffer(context.ARRAY_BUFFER,vertexBuffer);
					context.enableVertexAttribArray(loc);
					context.vertexAttribPointer(loc,itemSize,context.FLOAT,false,0,0);
				}
			}
		}
		
		disableInputBuffer(context,shader,name) {
			context.disableVertexAttribArray(shader.attribLocations[name]);
		}
		
		setValueInt(context,shader,name,v) {
			context.uniform1i(shader.uniformLocations[name],v);
		}

		setValueIntPtr(context,shader,name,v) {
			context.uniform1iv(shader.uniformLocations[name],v);
		}
		
		setValueFloat(context,shader,name,v) {
			context.uniform1f(shader.uniformLocations[name],v);
		}

		setValueFloatPtr(context,shader,name,v) {
			context.uniform1fv(shader.uniformLocations[name],v);
		}
		
		setValueVector2(context,shader,name,v) {
			context.uniform2fv(shader.uniformLocations[name],v.v);
		}
		
		setValueVector3(context,shader,name,v) {
			context.uniform3fv(shader.uniformLocations[name],v.v);
		}
		
		setValueVector4(context,shader,name,v) {
			context.uniform4fv(shader.uniformLocations[name],v.v);
		}

		setValueVector2v(context,shader,name,v) {
			context.uniform2fv(shader.uniformLocations[name],v);
		}

		setValueVector3v(context,shader,name,v) {
			context.uniform3fv(shader.uniformLocations[name],v);
		}

		setValueVector4v(context,shader,name,v) {
			context.uniform4fv(shader.uniformLocations[name],v);
		}
		
		setValueMatrix3(context,shader,name,traspose,v) {
			context.uniformMatrix3fv(shader.uniformLocations[name],traspose,v.m);
		}
		
		setValueMatrix4(context,shader,name,traspose,v) {
			context.uniformMatrix4fv(shader.uniformLocations[name],traspose,v.m);
		}
		
		setTexture(context,shader,name,texture,textureUnit) {
			texture.setActive(textureUnit);
			texture.bind();
			context.uniform1i(shader.uniformLocations[name],textureUnit);
		}
	}
	
	bg.webgl1.ShaderImpl = ShaderImpl;
	
})();

(function() {

    let g_textureTools = {};
    class TextureTools extends bg.app.ContextObject {
        constructor(context) {
            super(context);
        }

        static Get(context) {
            if (!g_textureTools[context]) {
                g_textureTools = new TextureTools(context);
            }
            return g_textureTools;
        }

        getFBO(width,height) {
            let gl = this.context;
            let fboData = {
                gl: gl,
                fbo: gl.createFramebuffer(),
                rbo: gl.createRenderbuffer(),
                texture: new bg.base.Texture(gl),
                width: width,
                height: height,

                bind:function() {
                    this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.fbo);       
                },

                unbind:function() {
                    this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null);
                },

                resize:function(w,h) {
                    if (w!=this.width || h!=this.height) {
                        this.bind();
                        this.width = w;
                        this.height = height;
                        let gl = this.gl;
                        gl.bindRenderbuffer(gl.RENDERBUFFER,this.rbo);
                        gl.renderbufferStorage(gl.RENDERBUFFER,gl.DEPTH_COMPONENT16,this.width,this.height);
                        gl.bindRenderbuffer(gl.RENDERBUFFER,null);
                        this.texture.bind();
                        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, this.width, this.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
                        this.texture.unbind();
                        this.unbind();
                    }
                },

                beginRender:function() {
                    this._currentViewport = this.gl.getParameter(this.gl.VIEWPORT);
                    this.bind();
                    this.gl.framebufferTexture2D(
                        this.gl.FRAMEBUFFER,
                        this.gl.COLOR_ATTACHMENT0,
                        this.gl.TEXTURE_2D,
                        this.texture.texture, 0);
                    this.gl.viewport(0,0,this.width,this.height);
                },

                endRender:function() {
                    this.gl.viewport(
                        this._currentViewport[0],
                        this._currentViewport[1],
                        this._currentViewport[2],
                        this._currentViewport[3]
                    );
                    this.unbind();
                },

                destroy:function(destroyTexture = false) {
                    this.unbind();
                    if (destroyTexture) {
                        this.texture.destroy();
                    }

                    this.gl.deleteRenderbuffer(this.rbo);
                    this.gl.deleteFramebuffer(this.fbo);
                }
            };

            // Create the framebuffer
            gl.bindFramebuffer(gl.FRAMEBUFFER, fboData.fbo);
            gl.bindRenderbuffer(gl.RENDERBUFFER, fboData.rbo);  
            gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, width, height);
            gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, fboData.rbo);

            fboData.texture.target = bg.base.TextureTarget.TEXTURE_2D;
            fboData.texture.create();
            fboData.texture.bind();
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, width, height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);

            fboData.texture.unbind();
            fboData.texture._size = new bg.Vector2(width,height);

            gl.bindFramebuffer(gl.FRAMEBUFFER, null);
            gl.bindRenderbuffer(gl.RENDERBUFFER, null);

            return fboData;
        }
    }
    bg.webgl1.TextureTools = TextureTools;

    let s_mergeMapsPlane = null;
    function getMergeMapPlane(gl) {
        if (!s_mergeMapsPlane) {
            s_mergeMapsPlane = new bg.base.PolyList(gl);

            s_mergeMapsPlane.vertex = [ 1, 1, 0, -1, 1, 0, -1,-1, 0,1,-1, 0 ];
            s_mergeMapsPlane.texCoord0 = [ 1, 1, 0, 1, 0, 0, 1, 0 ];
            s_mergeMapsPlane.index = [ 0, 1, 2,  2, 3, 0 ];

            s_mergeMapsPlane.build();
        }
        return s_mergeMapsPlane;
    }
    
    let s_mergeMapsShader = null;
    function getMergeMapShader(gl) {
        if (!s_mergeMapsShader) {
            let vert = `
            attribute vec3 inPosition;
            attribute vec2 inTexCoord;

            varying vec2 fsTexCoord;

            void main() {
                fsTexCoord = inTexCoord;
                gl_Position = vec4(inPosition,1.0);
            }
            `;

            let frag = `
            precision mediump float;
            varying vec2 fsTexCoord;

            uniform sampler2D inT0;
            uniform int inT0Channel;
            uniform sampler2D inT1;
            uniform int inT1Channel;
            uniform sampler2D inT2;
            uniform int inT2Channel;
            uniform sampler2D inT3;
            uniform int inT3Channel;

            float mapChannel(sampler2D tex, int channel) {
                vec4 color = texture2D(tex,fsTexCoord);
                if (channel==0) {
                    return color.r;
                }
                else if (channel==1) {
                    return color.g;
                }
                else if (channel==2) {
                    return color.b;
                }
                else if (channel==3) {
                    return color.a;
                }
                else {
                    return 0.0;
                }
            }

            void main() {
                vec4 result;
                result.r = mapChannel(inT0,inT0Channel);
                result.g = mapChannel(inT1,inT1Channel);
                result.b = mapChannel(inT2,inT2Channel);
                result.a = mapChannel(inT3,inT3Channel);
                gl_FragColor = result;
            }
            `;

            s_mergeMapsShader = new bg.base.Shader(gl);
            s_mergeMapsShader.addShaderSource(bg.base.ShaderType.VERTEX, vert);
            s_mergeMapsShader.addShaderSource(bg.base.ShaderType.FRAGMENT, frag);
            let status = s_mergeMapsShader.link();
            if (!status) {
                throw new Error("Error generating texture merger shader.");
            }

            s_mergeMapsShader.initVars([
                "inPosition",
                "inTexCoord"
            ],[
                "inT0",
                "inT1",
                "inT2",
                "inT3",
                "inT0Channel",
                "inT1Channel",
                "inT2Channel",
                "inT3Channel"
            ]);
            

        }
        return s_mergeMapsShader;
    }

    class TextureMergerImpl extends bg.tools.TextureMergerImpl {
        constructor() {
            super();
            this._fbo = null;
        }
        // Get the greatest sizes in the textures r, g, b or a maps
        // (the greatest width and the greatest height)
        getMapSize(r,g,b,a) {
            return {
                width: Math.max(r.map.size.width, g.map.size.width, b.map.size.width, a.map.size.width),
                height: Math.max(r.map.size.height, g.map.size.height, b.map.size.height, a.map.size.height)
            };
        }


        mergeMaps(gl,r,g,b,a) {
            let activeShader = bg.base.Shader.GetActiveShader();
            let size = this.getMapSize(r,g,b,a);

            // TODO: there is some bug in the fbo.resize function. This can be optimized
            // resizing the fbo, instead of create it again
            // let fbo = this._fbo;
            // if (!fbo) {
            //     let tools = TextureTools.Get(gl);
            //     this._fbo = tools.getFBO(size.width,size.height);
            //     fbo = this._fbo;
            // }
            // else {
            //     fbo.resize(size.width,size.height);
            // }
            let tools = TextureTools.Get(gl);
            this._fbo = tools.getFBO(size.width,size.height);
            let fbo = this._fbo;

            let shader = getMergeMapShader.apply(this,[gl]);
            let plane = getMergeMapPlane.apply(this,[gl]);
            
            fbo.beginRender();
            gl.clearColor(0,0,0,1);
            gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
            gl.disable(gl.CULL_FACE);

            shader.setActive();
            shader.setInputBuffer("inPosition",plane.vertexBuffer,3);
            shader.setInputBuffer("inTexCoord",plane.texCoord0Buffer,2);

            shader.setTexture("inT0",r.map,bg.base.TextureUnit.TEXTURE_0);
            shader.setTexture("inT1",g.map,bg.base.TextureUnit.TEXTURE_1);
            shader.setTexture("inT2",b.map,bg.base.TextureUnit.TEXTURE_2);
            shader.setTexture("inT3",a.map,bg.base.TextureUnit.TEXTURE_3);
            shader.setValueInt("inT0Channel",r.channel);
            shader.setValueInt("inT1Channel",g.channel);
            shader.setValueInt("inT2Channel",b.channel);
            shader.setValueInt("inT3Channel",a.channel);
            plane.draw();

            shader.disableInputBuffer("inPosition");
            shader.disableInputBuffer("inTexCoord");
            shader.clearActive();
 
            fbo.endRender();

            if (activeShader) {
                activeShader.setActive();
            }

            return fbo.texture;
        }

        destroy(context) {
            this._fbo.destroy();
        }
    }


    bg.webgl1.TextureMergerImpl = TextureMergerImpl;

})();
(function() {
	
	class TextureImpl extends bg.base.TextureImpl {
		initFlags(context) {
			bg.base.TextureWrap.REPEAT = context.REPEAT;
			bg.base.TextureWrap.CLAMP = context.CLAMP_TO_EDGE;
			bg.base.TextureWrap.MIRRORED_REPEAT = context.MIRRORED_REPEAT;
			
			bg.base.TextureFilter.NEAREST_MIPMAP_NEAREST = context.NEAREST_MIPMAP_NEAREST;
			bg.base.TextureFilter.LINEAR_MIPMAP_NEAREST = context.LINEAR_MIPMAP_NEAREST;
			bg.base.TextureFilter.NEAREST_MIPMAP_LINEAR = context.NEAREST_MIPMAP_LINEAR;
			bg.base.TextureFilter.LINEAR_MIPMAP_LINEAR = context.LINEAR_MIPMAP_LINEAR;
			bg.base.TextureFilter.NEAREST = context.NEAREST;
			bg.base.TextureFilter.LINEAR = context.LINEAR;
			
			bg.base.TextureTarget.TEXTURE_2D 		= context.TEXTURE_2D;
			bg.base.TextureTarget.CUBE_MAP 		= context.TEXTURE_CUBE_MAP;
			bg.base.TextureTarget.POSITIVE_X_FACE = context.TEXTURE_CUBE_MAP_POSITIVE_X;
			bg.base.TextureTarget.NEGATIVE_X_FACE = context.TEXTURE_CUBE_MAP_NEGATIVE_X;
			bg.base.TextureTarget.POSITIVE_Y_FACE = context.TEXTURE_CUBE_MAP_POSITIVE_Y;
			bg.base.TextureTarget.NEGATIVE_Y_FACE = context.TEXTURE_CUBE_MAP_NEGATIVE_Y;
			bg.base.TextureTarget.POSITIVE_Z_FACE = context.TEXTURE_CUBE_MAP_POSITIVE_Z;
			bg.base.TextureTarget.NEGATIVE_Z_FACE = context.TEXTURE_CUBE_MAP_NEGATIVE_Z;
		}
		
		requireMipmaps(minFilter,magFilter) {
			return	minFilter==bg.base.TextureFilter.NEAREST_MIPMAP_NEAREST	||
					minFilter==bg.base.TextureFilter.LINEAR_MIPMAP_NEAREST 	||
					minFilter==bg.base.TextureFilter.NEAREST_MIPMAP_LINEAR 	||
					minFilter==bg.base.TextureFilter.LINEAR_MIPMAP_LINEAR		||
					magFilter==bg.base.TextureFilter.NEAREST_MIPMAP_NEAREST	||
					magFilter==bg.base.TextureFilter.LINEAR_MIPMAP_NEAREST 	||
					magFilter==bg.base.TextureFilter.NEAREST_MIPMAP_LINEAR	||
					magFilter==bg.base.TextureFilter.LINEAR_MIPMAP_LINEAR;
		}
		
		create(context) {
			return context.createTexture();
		}
		
		setActive(context,texUnit) {
			context.activeTexture(context.TEXTURE0 + texUnit);
		}
		
		bind(context,target,texture) {
			context.bindTexture(target, texture);
		}
		
		unbind(context,target) {
			this.bind(context,target,null);
		}
		
		setTextureWrapX(context,target,texture,wrap) {
			context.texParameteri(target, context.TEXTURE_WRAP_S, wrap);
		}
		
		setTextureWrapY(context,target,texture,wrap) {
			context.texParameteri(target, context.TEXTURE_WRAP_T, wrap);
		}
		
		setImage(context,target,minFilter,magFilter,texture,img,flipY) {
			if (flipY) context.pixelStorei(context.UNPACK_FLIP_Y_WEBGL, true);
			context.texParameteri(target, context.TEXTURE_MIN_FILTER, minFilter);
			context.texParameteri(target, context.TEXTURE_MAG_FILTER, magFilter);
			context.texImage2D(target,0,context.RGBA,context.RGBA,context.UNSIGNED_BYTE, img);
			if (this.requireMipmaps(minFilter,magFilter)) {
				context.generateMipmap(target);
			}
		}
		
		setImageRaw(context,target,minFilter,magFilter,texture,width,height,data,type,format) {
			if (!type) {
				type = context.RGBA;
			}
			if (!format) {
				format = context.UNSIGNED_BYTE;
			}
			if (format==bg.base.RenderSurfaceFormat.FLOAT) {
				minFilter = bg.base.TextureFilter.NEAREST;
				magFilter = bg.base.TextureFilter.NEAREST;
			}
			context.texParameteri(target, context.TEXTURE_MIN_FILTER, minFilter);
			context.texParameteri(target, context.TEXTURE_MAG_FILTER, magFilter);
			context.texImage2D(target,0, type,width,height,0,type,format, data);
			if (this.requireMipmaps(minFilter,magFilter)) {
				context.generateMipmap(target);
			}
		}

		setTextureFilter(context,target,minFilter,magFilter) {
			context.texParameteri(target,context.TEXTURE_MIN_FILTER,minFilter);
			context.texParameteri(target,context.TEXTURE_MAG_FILTER,magFilter);
		}

		setCubemapImage(context,face,image) {
			context.pixelStorei(context.UNPACK_FLIP_Y_WEBGL, false);
			context.texParameteri(context.TEXTURE_CUBE_MAP, context.TEXTURE_MIN_FILTER, bg.base.TextureFilter.LINEAR);
			context.texParameteri(context.TEXTURE_CUBE_MAP, context.TEXTURE_MAG_FILTER, bg.base.TextureFilter.LINEAR);
			context.texImage2D(face, 0, context.RGBA, context.RGBA, context.UNSIGNED_BYTE, image);
		}

		setCubemapRaw(context,face,rawImage,w,h) {
			let type = context.RGBA;
			let format = context.UNSIGNED_BYTE;
			context.texParameteri(context.TEXTURE_CUBE_MAP, context.TEXTURE_MIN_FILTER, bg.base.TextureFilter.LINEAR);
			context.texParameteri(context.TEXTURE_CUBE_MAP, context.TEXTURE_MAG_FILTER, bg.base.TextureFilter.LINEAR);
			context.pixelStorei(context.UNPACK_FLIP_Y_WEBGL, false);
			context.texImage2D(face, 0, type, w, h, 0, type, format, rawImage);
		}

		setVideo(context,target,texture,video,flipY) {
			if (flipY) context.pixelStorei(context.UNPACK_FLIP_Y_WEBGL, false);
			context.texParameteri(target, context.TEXTURE_MAG_FILTER, context.LINEAR);
			context.texParameteri(target, context.TEXTURE_MIN_FILTER, context.LINEAR);
			context.texParameteri(target, context.TEXTURE_WRAP_S, context.CLAMP_TO_EDGE);
			context.texParameteri(target, context.TEXTURE_WRAP_T, context.CLAMP_TO_EDGE);
			context.texImage2D(target,0,context.RGBA,context.RGBA,context.UNSIGNED_BYTE,video);
		}

		updateVideoData(context,target,texture,video) {
			context.bindTexture(target, texture);
			context.texImage2D(target,0,context.RGBA,context.RGBA,context.UNSIGNED_BYTE,video);
			context.bindTexture(target,null);
		}

		destroy(context,texture) {
			context.deleteTexture(this._texture);
		}
	}
	
	bg.webgl1.TextureImpl = TextureImpl;
	
})();