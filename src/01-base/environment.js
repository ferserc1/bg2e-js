(function() {

    function destoryResource(paramName) {
        if (this[paramName]) {
            this[paramName].destroy();
            this[paramName] = null;
        }
    }

    class Environment extends bg.app.ContextObject {
        constructor(context) {
            super(context);

            this._irradianceIntensity = 1;

            // Cubemap generators:
            this._cubemapRenderer = null;       // Convert the equirectangular 360º image into a cubemap texture
            this._irradianceRenderer = null;    // Generate the irradiance map
            this._specularRenderer = null;      // Generate the specular reflections
    

            // Renderer: generate the cubemap textures
            this._cubemapCapture = null;        // Main cubemap and specular reflection L0 (from 0% to 33% roughness)
            this._irradianceCapture = null;     // Irradiance map
            this._specularCaptureL1 = null;     // Specular reflections L1 (from 33% to 66% roughness)
            this._specularCaptureL2 = null;     // Specular reflections L2 (from 66% yo 100% roughness)

            this._blackTexture = bg.base.TextureCache.BlackTexture(context);
            this._texture = this._blackTexture;
        }

        set equirectangularTexture(t) {
            this._texture = t;
            if (this._cubemapRenderer) {
                this._cubemapRenderer.texture = t;
            }
        }

        set irradianceIntensity(i) { this._irradianceIntensity = i; }

        get equirectangularTexture() { return this._texture; }

        get cubemapTexture() { return this._cubemapCapture ? this._cubemapCapture.texture : this._blackTexture; }
        get irradianceMapTexture() { return this._irradianceCapture ? this._irradianceCapture.texture : this._blackTexture; }
        get specularMapTextureL0() { return this._cubemapCapture ? this._cubemapCapture.texture : this._blackTexture; }
        get specularMapTextureL1() { return this._specularCaptureL1 ? this._specularCaptureL1.texture : this._blackTexture; }
        get specularMapTextureL2() { return this._specularCaptureL2 ? this._specularCaptureL2.texture : this._blackTexture; }
        get irradianceIntensity() { return this._irradianceIntensity; }

        create(params) {
            if (!params) {
                params = {};
            }
            params.cubemapSize = params.cubemapSize || 512;
            params.irradianceMapSize = params.irradianceMapSize || 32;
            params.specularMapSize = params.specularMapSize || 32;
            params.specularMapL2Size = params.specularMapL2Size || params.specularMapSize || 32;
            this.destroy();
            this._cubemapRenderer = new bg.render.EquirectangularCubeRenderer(this.context,90);
            this._cubemapRenderer.create();
            this._cubemapRenderer.texture = this.texture;

            this._irradianceRenderer = new bg.render.CubeMapRenderer(this.context,90);
            this._irradianceRenderer.create(bg.render.CubeMapShader.IRRADIANCE_MAP);

            this._specularRenderer = new bg.render.CubeMapRenderer(this.context,90);
            this._specularRenderer.create(bg.render.CubeMapShader.SPECULAR_MAP);
            
            this._cubemapCapture = new bg.base.CubemapCapture(this.context);
            this._cubemapCapture.create(params.cubemapSize);

            this._irradianceCapture = new bg.base.CubemapCapture(this.context);
            this._irradianceCapture.create(params.irradianceMapSize);

            this._specularCaptureL1 = new bg.base.CubemapCapture(this.context);
            this._specularCaptureL1.create(params.specularMapSize);

            this._specularCaptureL2 = new bg.base.CubemapCapture(this.context);
            this._specularCaptureL2.create(params.specularMapL2Size);

            this._frame = 0;

            this._maxTextureUnits = this.context.getParameter(this.context.MAX_TEXTURE_IMAGE_UNITS);
        }

        update(camera) {
            let view = new bg.Matrix4(camera.viewMatrix);
            view.setPosition(0,0,0);
            //this._cubemapRendere.pipeline.viewport = this._camera.viewport;

            // Update main cubemap
            this._cubemapCapture.updateTexture((projectionMatrix,viewMatrix,usePipeline) => {
                this._cubemapRenderer.viewMatrix = viewMatrix;
                this._cubemapRenderer.projectionMatrix = projectionMatrix;
                this._cubemapRenderer.render(!usePipeline);
            }, view);


            // Update cubemap renderers textures
            this._irradianceRenderer.texture = this._cubemapCapture.texture;
            this._specularRenderer.texture = this._cubemapCapture.texture;

            // Update irradiance and specular maps
            if (this._frame == 0) {
                this._irradianceCapture.updateTexture((projectionMatrix,viewMatrix) => {
                    this._irradianceRenderer.viewMatrix = viewMatrix;
                    this._irradianceRenderer.projectionMatrix = projectionMatrix;
                    this._irradianceRenderer.render(true);
                }, bg.Matrix4.Identity(), view);
                this._frame = 1;
            }
            else if (this._frame == 1) {
                this._specularCaptureL1.updateTexture((projectionMatrix,viewMatrix) => {
                    this._specularRenderer.viewMatrix = viewMatrix;
                    this._specularRenderer.projectionMatrix = projectionMatrix;
                    this._specularRenderer.roughness = this._maxTextureUnits>8 ? 0.2 : 0.3;
                    this._specularRenderer.render(true);
                }, bg.Matrix4.Identity(), view);
                this._frame = this._maxTextureUnits>8 ? 2 : 0;
            }
            else if (this._frame == 2) {
                this._specularCaptureL2.updateTexture((projectionMatrix,viewMatrix) => {
                    this._specularRenderer.viewMatrix = viewMatrix;
                    this._specularRenderer.projectionMatrix = projectionMatrix;
                    this._specularRenderer.roughness = 0.8;
                    this._specularRenderer.render(true);
                }, bg.Matrix4.Identity(), view);
                this._frame = 0;
            }
        }

        renderSkybox(camera) {
            let view = new bg.Matrix4(camera.viewMatrix);
            view.setPosition(0,0,0);
            this._cubemapRenderer.pipeline.viewport = camera.viewport;
            this._cubemapRenderer.viewMatrix = view;
            this._cubemapRenderer.projectionMatrix = camera.projection;
            this._cubemapRenderer.render(false);
        }
    
        destroy() {
            destoryResource.apply(this,["_cubemapRenderer"]);
            destoryResource.apply(this,["_irradianceRenderer"]);
            destoryResource.apply(this,["_specularRenderer"]);
            destoryResource.apply(this,["_cubemapCapture"]);
            destoryResource.apply(this,["_irradianceCapture"]);
            destoryResource.apply(this,["_specularCaptureL1"]);
            destoryResource.apply(this,["_specularCaptureL2"]);
        }

        clone() {
            console.warn("bg.base.Environment.clone(): not implemented");
        }

        serialize(data,promises,url) {
            console.warn("bg.base.Environment.serialize(): not implemented");
        }

        deserialize(data,url) {
            console.warn("bg.base.Environment.deserialize(): not implemented");
        }
    }

    bg.base.Environment = Environment;
})();