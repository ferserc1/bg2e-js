(function() {
    bg.base.PBRMaterialFlag = {
        DIFFUSE                     : 1 << 0,   // diffuse, isTransparent, alphaCutoff
        DIFFUSE_SCALE               : 1 << 1,   // diffuseScale
        DIFFUSE_OFFSET              : 1 << 2,   // diffuseOffset
        METALLIC                    : 1 << 3,   // metallic, metallicChannel
        ROUGHNESS                   : 1 << 4,   // roughness, roughnessChannel
        LIGHT_EMISSION              : 1 << 5,   // lightEmission, lightEmissionChannel
        NORMAL                      : 1 << 6,   // normal
        NORMAL_SCALE                : 1 << 7,   // normalScale
        NORMAL_OFFSET               : 1 << 8,   // normalOffset
        LIGHT_MAP                   : 1 << 9,   // not used
        HEIGHT                      : 1 << 10,  // height, heightChannel
        HEIGHT_SCALE                : 1 << 11,  // heightScale
        SHADOWS                     : 1 << 12,  // castShadows/receiveShadows
        CULL_FACE                   : 1 << 13,  // cullFace
        UNLIT                       : 1 << 14   // unlit
    };

    function getColorOrTexture(data,defaultValue = bg.Color.Black()) {
        if (Array.isArray(diffuse) && diffuse.length==3) {
            return new bg.Color(data[0],data[1],data[2],1);
        }
        else if (Array.isArray(diffuse) && diffuse.length>=4) {
            return new bg.Color(data[0],data[1],data[2],data[3]);
        }
        else if (typeof(diffuse) == "string" && diffuse != "") {
            return diffuse;
        }
        else {
            return defaultValue;
        }
    }

    function getVector(data,defaultValue = bg.Vector2()) {
        if (Array.isArray(data) && data.length==2) {
            return new bg.Vector2(data);
        }
        else if (Array.isArray(data) && data.length==3) {
            return new bg.Vector3(data);
        }
        else if (Array.isArray(data) && data.length==4) {
            return new bg.Vector4(data);
        }
        else {
            return defaultValue;
        }
    }

    function getScalarOrTexture(data,defaultValue = 0) {
        if (data!==undefined && !isNaN(Number(data)) && data!=="") {
            return Number(data);
        }
        else if (data!==undefined && typeof(data)=="string" && data!="") {
            return data;
        }
        else {
            return defaultValue;
        }
    }

    function getScalar(data,defaultValue = 0) {
        if (data!==undefined && !isNaN(Number(data)) && data!=="") {
            return Number(data);
        }
        else {
            return defaultValue;
        }
    }

    function getBoolean(value, defaultValue=true) {
        if (typeof(value)=="string" && value!=="") {
            return /true/i.test(value) || /yes/i.test(value) || /1/.test(value);
        }
        else if (value!==undefined) {
            return value;
        }
        else {
            return defaultValue;
        }
    }

    function getVector(value) {
        if (value instanceof bg.Vector2 || value instanceof bg.Vector3 || value instanceof bg.Vector4) {
            return value.toArray();            
        }
        else {
            return [0,0];
        }
    }

    function getColorOrTexture(value) {
        if (value instanceof bg.Color) {
            return value.toArray();
        }
        else if (typeof(value) == "string" && value != "") {
            return value;
        }
        else {
            return [0,0,0,1];
        }
    }

    function getScalarOrTexture(value) {
        if (value!==undefined && !isNaN(Number(value))) {
            return Number(value);
        }
        else if (typeof(value) == "string" && value!="") {
            return "";
        }
        else {
            return 0;
        }
    }

    class PBRMaterialModifier {
        constructor(jsonData) {
            this._modifierFlags = 0;

            this._diffuse = bg.Color.White();
            this._isTransparent = false;
            this._alphaCutoff = 0.5;
            this._diffuseScale = new bg.Vector2(1);
            this._diffuseOffset = new bg.Vector2();
            this._metallic = 0;
            this._metallicChannel = 0;
            this._roughness = 1;
            this._roughnessChannel = 0;
            this._lightEmission = 0;
            this._lightEmissionChannel = 0;
            this._height = 0;
            this._heightChannel = 0;
            this._heightScale = 1;
            this._normal = new bg.Color(0.5,0.5,1,1);
            this._normalScale = new bg.Vector2(1);
            this._normalOffset = new bg.Vector2();
            this._castShadows = true;
            this._cullFace = true;
            this._unlit = false;

            if (jsonData && jsonData.type != "pbr") {
                console.warn("non-pbr data used in pbr material modifier.");
                // TODO: Import from legacy material modifier
                if (jsonData.texture) {
                    this._diffuse = getColorOrTexture(jsonData.texture, this._diffuse);
                }
                else {
                    this._diffuse = new bg.Color(
                        jsonData.diffuseR!==undefined ? jsonData.diffuseR : 1,
                        jsonData.diffuseG!==undefined ? jsonData.diffuseG : 1,
                        jsonData.diffuseB!==undefined ? jsonData.diffuseB : 1,
                        jsonData.diffuseA!==undefined ? jsonData.diffuseA : 1
                    );
                }

                this._diffuseScale = new bg.Vector2(
                    jsonData.diffuseScaleX!==undefined ? jsonData.diffuseScaleX : this._diffuseScale.x,
                    jsonData.diffuseScaleY!==undefined ? jsonData.diffuseScaleY : this._diffuseScale.y
                );
                this._diffuseOffset = new bg.Vector2(
                    jsonData.diffuseOffsetX!==undefined ? jsonData.diffuseOffsetX : this._diffuseOffset.x,
                    jsonData.diffuseOffsetY!==undefined ? jsonData.diffuseOffsetY : this._diffuseOffset.y
                );

                if (jsonData.normalMap) {
                    this._normal = getColorOrTexture(jsonData.normalMap, this._normal);
                }
                this._normalScale = new bg.Vector2(
                    jsonData.normalMapScaleX!==undefined ? jsonData.normalMapScaleX : this._normalScale.x,
                    jsonData.normalMapScaleY!==undefined ? jsonData.normalMapScaleY : this._normalScale.y
                );
                this._normalOffset = new bg.Vector2(
                    jsonData.normalMapOffsetX!==undefined ? jsonData.normalMapOffsetX : this._normalOffset.x,
                    jsonData.normalMapOffsetY!==undefined ? jsonData.normalMapOffsetY : this._normalOffset.y
                );

                if (jsonData.diffuseR || jsonData.diffuseG || jsonData.diffuseB || jsonData.diffuseA || jsonData.texture) {
                    this.setEnabled(bg.base.PBRMaterialFlag.DIFFUSE);
                }
                if (jsonData.diffuseScaleX || jsonData.diffuseScaleY) {
                    this.setEnabled(bg.base.PBRMaterialFlag.DIFFUSE_SCALE);
                }
                if (jsonData.diffuseOffsetX || jsonData.diffuseOffsetY) {
                    this.setEnabled(bg.base.PBRMaterialFlag.DIFFUSE_OFFSET);
                }
                if (jsonData.normalMap) {
                    this.setEnabled(bg.base.PBRMaterialFlag.NORMAL);
                }
                if  (jsonData.normalMapScaleX || jsonData.normalMapScaleY) {
                    this.setEnabled(bg.base.PBRMaterialFlag.NORMAL_SCALE);
                }
                if  (jsonData.normalMapOffsetX || jsonData.normalMapOffsetY) {
                    this.setEnabled(bg.base.PBRMaterialFlag.NORMAL_OFFSET);
                }
            }
            else if (jsonData) {
                this._diffuse = getColorOrTexture(jsonData.diffuse, this._diffuse);
                this._isTransparent = getBoolean(jsonData.isTransparent, this._isTransparent);
                this._alphaCutoff = getScalar(jsonData.alphaCutoff, this._alphaCutoff);
                this._diffuseScale = getVector(jsonData.diffuseScale, this._diffuseScale);
                this._diffuseOffset = getVector(jsonData.diffuseOffset, this._diffuseOffset);
                this._metallic = getScalarOrTexture(jsonData.metallic, this._metallic);
                this._metallicChannel = getScalar(jsonData.metallicChannel, this._metallicChannel);
                this._roughness = getScalarOrTexture(jsonData.roughness, this._roughness);
                this._roughnessChannel = getScalar(jsonData.roughnessChannel, this._roughnessChannel);
                this._lightEmission = getScalarOrTexture(jsonData.lightEmission, this._lightEmission);
                this._lightEmissionChannel = getScalar(jsonData.lightEmissionChannel, this._lightEmissionChannel);
                this._height = getColorOrTexture(jsonData.height, this._height);
                this._heightChannel = getScalar(jsonData.heightChannel, this._heightChannel);
                this._heightScale = getScalar(jsonData.heightScale, this._heightScale);
                this._normal = getColorOrTexture(jsonData.normal, this._normal);
                this._normalScale = getVector(jsonData.normalScale, this._normalScale);
                this._normalOffset = getVector(jsonData.normalOffset, this._normalOffset);
                this._castShadows = getBoolean(jsonData.castShadows, this._castShadows);
                this._cullFace = getBoolean(jsonData.cullFace, this._cullFace);
                this._unlit = getBoolean(jsonData.unlit, this._unlit);

                if (jsonData.diffuse || jsonData.isTransparent || jsonData.alphaCutoff) {
                    this.setEnabled(bg.base.PBRMaterialFlag.DIFFUSE);
                }
                if (jsonData.diffuseScale) {
                    this.setEnabled(bg.base.PBRMaterialFlag.DIFFUSE_SCALE);
                }
                if (jsonData.diffuseOffset) {
                    this.setEnabled(bg.base.PBRMaterialFlag.DIFFUSE_OFFSET);
                }
                if (jsonData.metallic!==undefined) {
                    this.setEnabled(bg.base.PBRMaterialFlag.METALLIC);
                }
                if (jsonData.roughness!==undefined) {
                    this.setEnabled(bg.base.PBRMaterialFlag.ROUGHNESS);
                }
                if (jsonData.lightEmission!==undefined) {
                    this.setEnabled(bg.base.PBRMaterialFlag.LIGHT_EMISSION);
                }
                if (jsonData.heigh!==undefined) {
                    this.setEnabled(bg.base.PBRMaterialFlag.HEIGHT);
                }
                if (jsonData.heightScale!==undefined) {
                    this.setEnabled(bg.base.PBRMaterialFlag.HEIGHT_SCALE);
                }
                if (jsonData.normal) {
                    this.setEnabled(bg.base.PBRMaterialFlag.NORMAL);
                }
                if (jsonData.normalScale) {
                    this.setEnabled(bg.base.PBRMaterialFlag.NORMAL_SCALE);
                }
                if (jsonData.normalOffset) {
                    this.setEnabled(bg.base.PBRMaterialFlag.NORMAL_OFFSET);
                }
                if (jsonData.castShadows!==undefined) {
                    this.setEnabled(bg.base.PBRMaterialFlag.SHADOWS);
                }
                if (jsonData.cullFace!==undefined) {
                    this.setEnabled(bg.base.PBRMaterialFlag.CULL_FACE);
                }
                if (jsonData.unlit!==undefined) {
                    this.setEnabled(bg.base.PBRMaterialFlag.UNLIT);
                }

            }
        }

        get modifierFlags() { return this._modifierFlags; }
		set modifierFlags(f) { this._modifierFlags = f; }
		setEnabled(flag) { this._modifierFlags = this._modifierFlags | flag; }
        isEnabled(flag) { return (this._modifierFlags & flag)!=0; }
        
        get diffuse() { return this._diffuse; }
        get isTransparent() { return this._isTransparent; }
        get alphaCutoff() { return this._alphaCutoff; }
        set diffuse(v) { this._diffuse = v; this.setEnabled(bg.base.PBRMaterialFlag.DIFFUSE); }
        set isTransparent(v) { this._isTransparent = v; this.setEnabled(bg.base.PBRMaterialFlag.DIFFUSE); }
        set alphaCutoff(v) { this._alphaCutoff = v; this.setEnabled(bg.base.PBRMaterialFlag.DIFFUSE); }
        get diffuseScale() { return this._diffuseScale; }
        set diffuseScale(v) { this._diffuseScale = v; this.setEnabled(bg.base.PBRMaterialFlag.DIFFUSE_SCALE); }
        get diffuseOffset() { return this._diffuseOffset; }
        set diffuseOffset(v) { this._diffuseOffset = v; this.setEnabled(bg.base.PBRMaterialFlag.DIFFUSE_OFFSET); }
        get metallic() { return this._metallic; }
        set metallic(v) { this._metallic = v; this.setEnabled(bg.base.PBRMaterialFlag.METALLIC); }
        get metallicChannel() { return this._metallicChannel; }
        set metallicChannel(v) { this._metallicChannel = v; this.setEnabled(bg.base.PBRMaterialFlag.METALLIC); }
        get roughness() { return this._roughness; }
        set roughness(v) { this._roughness = v; this.setEnabled(bg.base.PBRMaterialFlag.ROUGHNESS); }
        get roughnessChannel() { return this._roughnessChannel; }
        set roughnessChannel(v) { this._roughnessChannel = v; this.setEnabled(bg.base.PBRMaterialFlag.ROUGHNESS); }
        get lightEmission() { return this._lightEmission; }
        set lightEmission(v) { this._lightEmission = v; this.setEnabled(bg.base.PBRMaterialFlag.LIGHT_EMISSION); }
        get lightEmissionChannel() { return this._lightEmissionChannel; }
        set lightEmissionChannel(v) { this._lightEmissionChannel = v; this.setEnabled(bg.base.PBRMaterialFlag.LIGHT_EMISSION); }
        get height() { return this._height; }
        set height(v) { this._height =v; this.setEnabled(bg.base.PBRMaterialFlag.HEIGHT); }
        get heightChannel() { return this._heightChannel; }
        set heightChannel(v) { this._heightChannel = v; this.setEnabled(bg.base.PBRMaterialFlag.HEIGHT); }
        get heighScale() { return this._heighScale; }
        set heighScale(v) { this._heighScale = v; this.setEnabled(bg.base.PBRMaterialFlag.HEIGHT_SCALE); }
        get normal() { return this._normal; }
        set normal(v) { this._normal =v; this.setEnabled(bg.base.PBRMaterialFlag.NORMAL); }
        get normalScale() { return this._normalScale; }
        set normalScale(v) { this._normalScale = v; this.setEnabled(bg.base.PBRMaterialFlag.NORMAL_SCALE); }
        get normalOffset() { return this._normalOffset; }
        set normalOffset(v) { this._normalOffset = v; this.setEnabled(bg.base.PBRMaterialFlag.NORMAL_OFFSET); }
        get castShadows() { return this._castShadows; }
        set castShadows(v) { this._castShadows = v; this.setEnabled(bg.base.PBRMaterialFlag.SHADOWS); }
        get cullFace() { return this._cullFace; }
        set cullFace(v) { this._cullFace = v; this.setEnabled(bg.base.PBRMaterialFlag.CULL_FACE); }
        get unlit() { return this._unlit; }
        set unlit(v) { this._unlit = v; this.setEnabled(bg.base.PBRMaterialFlag.UNLIT); }

        clone() {
            let copy = new PBRMaterialModifier();
            copy.assign(this);
            return copy;
        }

        assign(mod) {
            this._modifierFlags = mod._modifierFlags;
            
            this._diffuse = mod._diffuse;
            this._isTransparent = mod._isTransparent;
            this._alphaCutoff = mod._alphaCutoff;
            this._diffuseScale = mod._diffuseScale;
            this._diffuseOffset = mod._diffuseOffset;
            this._metallic = mod._metallic;
            this._metallicChannel = mod._metallicChannel;
            this._roughness = mod._roughness;
            this._roughnessChannel = mod._roughnessChannel;
            this._lightEmission = mod._lightEmission;
            this._lightEmissionChannel = mod._lightEmissionChannel;
            this._height = mod._height;
            this._heightChannel = mod._heightChannel;
            this._heightScale = mod._heightScale;
            this._normal = mod._normal;
            this._normalScale = mod._normalScale;
            this._normalOffset = mod._normalOffset;
            this._castShadows = mod._castShadows;
            this._cullFace = mod._cullFace;
            this._unlit = mod._unlit;        
        }

        serialize() {
            let result = {};
            let mask = this._modifierMask;

            if (mask & bg.base.PBRMaterialFlag.DIFFUSE) {
                result.diffuse = getColorOrTexture(this.diffuse);
                result.isTransparent = getBoolean(this.isTransparent);
                result.alphaCutoff = getBoolean(this.alphaCutoff);
            }
            if (mask & bg.base.PBRMaterialFlag.DIFFUSE_SCALE) {
                result.diffuseScale = getVector(this.diffuseScale);
            }
            if (mask & bg.base.PBRMaterialFlag.DIFFUSE_OFFSET) {
                result.diffuseOffset = getVector(this.diffuseOffset);
            }
            if (mask & bg.base.PBRMaterialFlag.METALLIC) {
                result.metallic = getScalarOrTexture(this.metallic);
                result.metallicChannel = getScalar(this.metallicChannel);
            }
            if (mask & bg.base.PBRMaterialFlag.ROUGHNESS) {
                result.roughness = getScalarOrTexture(this.roughness);
                result.roughnessChannel = getScalar(this.roughnessChannel);
            }
            if (mask & bg.base.PBRMaterialFlag.LIGHT_EMISSION) {
                result.lightEmission = getScalarOrTexture(this.lightEmission);
                result.lightEmissionChannel = getScalar(this.lightEmissionChannel);
            }
            if (mask & bg.base.PBRMaterialFlag.NORMAL) {
                result.normal = getColorOrTexture(this.normal);
            }
            if (mask & bg.base.PBRMaterialFlag.NORMAL_SCALE) {
                result.normalScale = getVector(this.normalScale);
            }
            if (mask & bg.base.PBRMaterialFlag.NORMAL_OFFSET) {
                result.normalOffset = getVector(this.normalOffset);
            }
            if (mask & bg.base.PBRMaterialFlag.HEIGHT) {
                result.height = getScalarOrTexture(this.height);
                result.heightChannel = getScalar(this.heightChannel);
            }
            if (mask & bg.base.PBRMaterialFlag.HEIGHT_SCALE) {
                result.heightScale = getScalar(this.heightScale);
            }
            if (mask & bg.base.PBRMaterialFlag.SHADOWS) {
                result.castShadows = getBoolean(this.castShadows);
            }
            if (mask & bg.base.PBRMaterialFlag.CULL_FACE) {
                result.cullFace = getBoolean(this.cullFace);
            }
            if (mask & bg.base.PBRMaterialFlag.UNLIT) {
                result.unlit = getBoolean(this.unlit);
            }

            return result;
        }
    }

    bg.base.PBRMaterialModifier = PBRMaterialModifier;

    // Image load functions defined in bg.base.imageTools:
    //      isAbsolutePath(path)
    //      getTexture(context,texturePath,resourcePath)
    //      getPath(texture)        texture ? texture.fileName : ""
    //      readTexture(context,basePath,texData,mat,property)  

    // Returns a texture from a scalar or a vector parameter
    function getMap(context,matParam) {

        let vecValue = null;
        let num = Number(matParam);
        if (isNaN(num)) {
            if (matParam instanceof bg.Vector3) {
                vecValue = new bg.Vector4(matParam.x,matParam.y,matParam.z,0);
            }
            else if (matParam instanceof bg.Vector2) {
                vecValue = new bg.Vector4(matParam.x,matParam.y,0,0);
            }
            else if (matParam instanceof bg.Vector4) {
                vecValue = matParam;
            }
            else if (matParam===undefined) {
                vecValue = new bg.Vector4(0,0,0,0);
            }
        }
        else {
            vecValue = new bg.Vector4(num,num,num,num);
        }

        if (vecValue) {
            return bg.base.Texture.ColorTexture(context,vecValue,{ width: 1, height: 1 });
        }
        else {
            throw new Error("PBRMaterial invalid material parameter specified.");
        }
    }

    // Release the specified map, if is marked to release
    function release(mapName) {
        let map = this._shaderParameters[mapName];
        if (map && map.map && map.release) {
            map.map.destroy();
        }
    }

    // Combine height, metallic, roughness and ambient occlus into one map
    // TODO: Combine the light emission channel in the normal map alpha channel
    function combineMaps(gl) {
        // Combine height, metallic, roughness and ambient occlussion
        // TODO: ambient occlussion not implemented yet
        

        let height = {
            map: this._shaderParameters.height.map,
            channel: this._heightChannel
        };
        let metallic = {
            map: this._shaderParameters.metallic.map,
            channel: this._metallicChannel
        };
        let roughness = {
            map: this._shaderParameters.roughness.map,
            channel: this._roughnessChannel
        };
        // TODO: Implement ambien occlussion
        let ao = {
            map: bg.base.TextureCache.WhiteTexture(gl),
            channel: 3
        };


        if (!this._merger) {
            this._merger = new bg.tools.TextureMerger(gl);
        }
        let hmrao = this._merger.mergeMaps(height, metallic, roughness, ao);

        //if (!this._shaderParameters.heightMetallicRoughnessAO.map) {
            this._shaderParameters.heightMetallicRoughnessAO.map = hmrao;
        //}

        this._updateMergedTexture = false;
    }

    // colorOrPath: MUST be a valid file name or a bg.Color instance
    function getMaterialMap(context,paramName,colorOrPath,basePath) {
        return new Promise((resolve,reject) => {
            if (typeof(colorOrPath) == "string") {
                if (!bg.base.imageTools.isAbsolutePath(colorOrPath)) {
                    colorOrPath = bg.base.imageTools.mergePath(basePath,colorOrPath);
                }
                bg.base.Loader.Load(context,colorOrPath)
                    .then((texture) => {
                        this[paramName] = texture;
                        resolve(texture);
                    })
                    .catch((err) => {
                        reject(err);
                    });
            }
            else if (colorOrPath instanceof bg.Color) {
                this[paramName] = colorOrPath;
                resolve(colorOrPath);
            }
            else {
                reject(new Error("Invalid PBR color parameter"));
            }
        })
    }

    class PBRMaterial {
        static ImportFromLegacyMaterial(context,mat) {
            let result = new bg.base.PBRMaterial();
            result.diffuse  = mat.texture || mat.diffuse;
            result.diffuseScale = mat.textureScale;
            result.diffuseOffset = mat.textureOffset;
            result.normalScale = mat.normalMapScale;
            result.normalOffset = mat.normalMapOffset;
            if (mat.normalMap) {
                result.normal = mat.normalMap;
            }
            if (mat.roughnessMask) {
                result.roughness = mat.roughnessMask;
            }
            if (!result.roughness && mat.shininessMask) {
                result.roughness = mat.shininessMask;
            }
            if (mat.reflectionMask) {
                result.metallic = mat.reflectionMask;
            }
            result.castShadows = mat.castShadows;
            return result;
        }

        static FromMaterialDefinition(context,def,basePath="") {
            let mat = new PBRMaterial();
            
            mat.diffuseScale = getVector(def.diffuseScale, new bg.Vector2(1));
            mat.diffuseOffset = getVector(def.diffuseOffset, new bg.Vector2());
            
            mat.metallicChannel = getScalar(def.metallicChannel, 0);
            mat.roughnessChannel = getScalar(def.roughnessChannel, 0);
            mat.lightEmissionChannel = getScalar(def.lightEmissionChannel, 0);
            mat.heightChannel = getScalar(def.heightChannel, 0);
            mat.heightScale = getScalar(def.heightScale, 1);
            
            mat.normalScale = getVector(def.normalScale, new bg.Vector2(1));
            mat.normalOffset = getVector(def.normalOffset, new bg.Vector2());
            mat.alphaCutoff = getScalar(def.alphaCutoff, 0.5);
            mat.castShadows = getBoolean(def.castShadows, true);
            mat.cullFace = getBoolean(def.cullFace, true);
            
            let promises = [
                getMaterialMap.apply(this, [context, 'diffuse', getColorOrTexture(def.diffuse, bg.Color.White()), basePath]),
                getMaterialMap.apply(this, [context, 'metallic', getScalarOrTexture(def.metallic, 0), basePath]),
                getMaterialMap.apply(this, [context, 'roughness', getScalarOrTexture(def.roughness, 1), basePath]),
                getMaterialMap.apply(this, [context, 'lightEmission', getScalarOrTexture(def.lightEmission, 0), basePath]),
                getMaterialMap.apply(this, [context, 'height', getScalarOrTexture(def.height, 0), basePath]),
                getMaterialMap.apply(this, [context, 'normal', getColorOrTexture(def.normal, new bg.Color(0.5,0.5,1,1)), basePath])
            ];

            return new Promise((resolve,reject) => {
                Promise.all(promises)
                    .then((result) => {
                        mat.getShaderParameters(context);
                        resolve(mat);
                    })
                    .catch((err) => {
                        console.warn(err.message);
                        mat.getShaderParameters(context);
                        resolve(mat);
                    });
            });
        }

        updateFromLegacyMaterial(context,mat) {
            if (mat.texture && this.diffuse!=mat.texture) {
                this.diffuse = mat.texture;
            }
            this.diffuseScale = mat.textureScale;
            this.diffuseOffset = mat.textureOffset;
            this.normalScale = mat.normalMapScale;
            this.normalOffset = mat.normalMapOffset;
            if (mat.normalMap != this.normal) {
                this.normal = mat.normalMap;
            }
            if (mat.roughnessMask && mat.roughnessMask != this.roughness) {
                this.roughness = mat.roughnessMask;
            }
            if (!mat.roughnessMask && this.roughness != mat.shininessMask) {
                this.roughness = mat.shininessMask;
            }
            if (mat.reflectionMask != this.metallic) {
                this.metallic = mat.reflectionMask;
            }
        }

        constructor() {
            this._diffuse = bg.Color.White();
            this._alphaCutoff = 0.5;
            this._isTransparent = false;
            this._metallic = 0;
            this._metallicChannel = 0;
            this._roughness = 1;
            this._roughnessChannel = 0;
            this._lightEmission = 0;
            this._lightEmissionChannel = 0;
            this._normal = new bg.Color(0.5,0.5,1,1);
            this._normalChannel = 0;
            this._height = bg.Color.Black();
            this._heightChannel = 0;
            this._heightScale = 1.0;
            this._castShadows = true;
            this._cullFace = true;
            this._unlit = false;

            // This variable is a flat that indicate that the
            // combined heighMetallicRoughnessAO texture must be
            // updated, because at least one of the base textures
            // have been chanted
            this._updateMergedTexture = true;

            this._shaderParameters = {
                diffuse: { map: null, release: false },
                metallic: { map: null, release: false },
                roughness: { map: null, release: false },
                lightEmission: { map: null, release: false },
                normal: { map: null, release: false },
                height: { map: null, release: false },

                heightMetallicRoughnessAO: { map: null, release: false },

                // Scalar and vector parameters
                diffuseOffset: new bg.Vector2(0,0),
                diffuseScale: new bg.Vector2(1,1),
                normalOffset: new bg.Vector2(0,0),
                normalScale: new bg.Vector2(1,1),
                alphaCutoff: 0.5,
                castShadows: true
            };
        }

        static Defaults(context) {
            let TexCache = bg.base.TextureCache;
            let whiteTexture = TexCache.WhiteTexture(context);
            let blackTexture = TexCache.BlackTexture(context);
            let normalTexture = TexCache.NormalTexture(context);

            return {
                diffuse: {
                    map: whiteTexture
                    // channel not used
                },
                metallic: {
                    map: blackTexture,
                    channel: 0
                },
                roughness: {
                    map: whiteTexture,
                    channel: 0
                },
                lightEmission: {
                    map: blackTexture,
                    channel: 0
                },
                normal: {
                    map: normalTexture
                    // channel not used
                },
                height: {
                    map: blackTexture,
                    channel: 0
                }
            };
        }

        clone() {
            let copy = new PBRMaterial();
            copy.assign(this);
            return copy;
        }

        assign(other) {
            this.diffuse = other.diffuse;
            this.isTransparent = other.isTransparent;
            this.alphaCutoff = other.alphaCutoff;
            this.diffuseScale = other.diffuseScale;
            this.diffuseOffset = other.diffuseOffset;
            this.metallic = other.metallic;
            this.metallicChannel = other.metallicChannel;
            this.roughness = other.roughness;
            this.roughnessChannel = other.roughnessChannel;
            this.lightEmission = other.lightEmission;
            this.lightEmissionChannel = other.lightEmissionChannel;
            this.height = other.height;
            this.heightChannel = other.heightChannel;
            this.heightScale = other.heightScale;
            this.normal = other.normal;
            this.normalScale = other.normalScale;
            this.normalOffset = other.normalOffset;
            this.castShadows = other.castShadows;
            this.cullFace = other.cullFace;
            this.unlit = other.unlit;
        }

        /* Release the PBR material resources:
         *  The PBR material parameters are always textures. You can set scalar or vector parameters,
         *  but in that case will be converted into a minimum size texture, that is stored in the material
         *  object, and for that reason it's important to delete the material objects to release that 
         *  resources.
         * 
         *  If the specified parameters are textures, then the PBRMaterial will not release that objects
         *  when the destroy() function is called
         */
        destroy() {
            release.apply(this,["diffuse"]);
            release.apply(this,["metallic"]);
            release.apply(this,["roughness"]);
            release.apply(this,["lightEmission"]);
            release.apply(this,["normal"]);
            release.apply(this,["height"]);
        }

        /* Returns the PBR shader parameters
         *  This function returns the material _shaderParameters object, and if it's necesary, will
         *  create the texture resources (for example, if a scalar or vector parameter is set).
         * 
         */
        getShaderParameters(context) {
            let prepareResource = (paramName) => {
                if (!this._shaderParameters[paramName].map) {
                    this._shaderParameters[paramName].release = true;
                    this._shaderParameters[paramName].map = getMap.apply(this,[context,this[paramName]]);
                }
            }

            prepareResource("diffuse");
            prepareResource("metallic");
            prepareResource("roughness");
            prepareResource("lightEmission");
            prepareResource("normal");
            prepareResource("height");

            if (this._updateMergedTexture) {
                combineMaps.apply(this,[context]);
            }

            return this._shaderParameters;
        }

        /* PBR material parameters
         *  The following functions returns the shader parameters. Each parameter may be an scalar,
         *  a vector or a texture value. If the parameter is intrinsically a one-dimesional value, for
         *  example, the roughness map, it will be accompanied by a channel index
         */
        get diffuse() { return this._diffuse; }
        get metallic() { return this._metallic; }
        get metallicChannel() { return this._metallicChannel; }
        get roughness() { return this._roughness; }
        get roughnessChannel() { return this._roughnessChannel; }
        get lightEmission() { return this._lightEmission; }
        get lightEmissionChannel() { return this._lightEmissionChannel; }
        get normal() { return this._normal; }
        get height() { return this._height; }

        set diffuse(v) {
            release.apply(this,["diffuse"]);
            this._shaderParameters.diffuse.map = v instanceof bg.base.Texture ? v : null;
            this._shaderParameters.diffuse.release = this._shaderParameters.diffuse.map == null;
            this._diffuse = v;
        }

        set metallic(v) {
            release.apply(this,["metallic"]);
            this._shaderParameters.metallic.map = v instanceof bg.base.Texture ? v : null;
            this._shaderParameters.metallic.release = this._shaderParameters.metallic.map == null;
            this._metallic = v;
            this._updateMergedTexture = true;
        }

        set metallicChannel(c) { this._metallicChannel = c; }

        set roughness(v) {
            release.apply(this,["roughness"]);
            this._shaderParameters.roughness.map = v instanceof bg.base.Texture ? v : null;
            this._shaderParameters.roughness.release = this._shaderParameters.roughness.map == null;
            this._roughness = v;
            this._updateMergedTexture = true;
        }

        set roughnessChannel(c) { this._roughnessChannel = c; }

        set lightEmission(v) {
            release.apply(this,["lightEmission"]);
            this._shaderParameters.lightEmission.map = v instanceof bg.base.Texture ? v : null;
            this._shaderParameters.lightEmission.release = this._shaderParameters.lightEmission.map == null;
            this._lightEmission = v;
            this._updateMergedTexture = true;
        }

        set lightEmissionChannel(c) { this._lightEmissionChannel = c; }

        set normal(v) {
            release.apply(this,["normal"]);
            this._shaderParameters.normal.map = v instanceof bg.base.Texture ? v : null;
            this._shaderParameters.normal.release = this._shaderParameters.normal.map == null;
            this._normal = v;
            this._updateMergedTexture = true;
        }

        set height(v) {
            release.apply(this,["height"]);
            this._shaderParameters.height.map = v instanceof bg.base.Texture ? v : null;
            this._shaderParameters.height.release = this._shaderParameters.height.map == null;
            this._height = v;
            this._updateMergedTexture = true;
        }

        set heightChannel(c) { this._heightChannel = c; }

        // Vector and scalar parameters
        
        get alphaCutoff() { return this._alphaCutoff; }
        set alphaCutoff(v) { this._alphaCutoff = v; }
        get isTransparent() { return this._isTransparent; }
        set isTransparent(v) { this._isTransparent = v; }
        get diffuseOffset() { return this._shaderParameters.diffuseOffset; }
        set diffuseOffset(v) { this._shaderParameters.diffuseOffset = v; }
        get diffuseScale() { return this._shaderParameters.diffuseScale; }
        set diffuseScale(v) { this._shaderParameters.diffuseScale = v; }
        get normalOffset() { return this._shaderParameters.normalOffset; }
        set normalOffset(v) { this._shaderParameters.normalOffset = v; }
        get normalScale() { return this._shaderParameters.normalScale; }
        set normalScale(v) { this._shaderParameters.normalScale = v; }
        get castShadows() { return this._castShadows; }
        set castShadows(c) { this._castShadows = c; }
        get heightScale() { return this._heightScale; }
        set heightScale(h) { this._heightScale = h; }
        get cullFace() { return this._cullFace; }
        set cullFace(c) { this._cullFace = c; }
        get unlit() { return this._unlit; }
        set unlit(v) { this._unlit = v; }

        getExternalResources(resources=[]) {
            function tryadd(texture) {
                if (texture instanceof bg.base.Texture &&
                    texture.fileName && 
                    texture.fileName!="" &&
                    resources.indexOf(texture.fileName)==-1)
                {
                    resources.push(texture.fileName);
                }
            }
            tryadd(this.diffuse);
            tryadd(this.metallic);
            tryadd(this.roughness);
            tryadd(this.lightEmission);
            tryadd(this.height);
            tryadd(this.normal);
            return resources;
        }

        copyMaterialSettings(mat,mask) {
            if (mask & bg.base.PBRMaterialFlag.DIFFUSE) {                  // diffuse, isTransparent, alphaCutoff
                mat.diffuse = this.diffuse;
                mat.alphaCutoff = this.alphaCutoff;
                mat.isTransparent = this.isTransparent;
            }   
            if (mask & bg.base.PBRMaterialFlag.DIFFUSE_SCALE) {            // diffuseScale
                mat.diffuseScale = this.diffuseScale;
            }   
            if (mask & bg.base.PBRMaterialFlag.DIFFUSE_OFFSET) {           // diffuseOffset
                mat.diffuseOffset = this.diffuseOffset;
            }   
            if (mask & bg.base.PBRMaterialFlag.METALLIC) {                 // metallic, metallicChannel
                mat.metallic = this.metallic;
                mat.metallicChannel = this.metallicChannel;
            }   
            if (mask & bg.base.PBRMaterialFlag.ROUGHNESS) {                // roughness, roughnessChannel
                mat.roughness = this.roughness;
                mat.roughnessChannel = this.roughnessChannel;
            }   
            if (mask & bg.base.PBRMaterialFlag.LIGHT_EMISSION) {           // lightEmission, lightEmissionChannel
                mat.lightEmission = this.lightEmission;
                mat.lightEmissionChannel = this.lightEmissionChannel;
            }   
            if (mask & bg.base.PBRMaterialFlag.HEIGHT) {                   // height, heightChannel
                mat.height = this.height;
                mat.heightChannel = this.heightChannel;
            }   
            if (mask & bg.base.PBRMaterialFlag.HEIGHT_SCALE) {             // heightScale
                mat.heightScale = this.heightScale;
            }   
            if (mask & bg.base.PBRMaterialFlag.NORMAL) {                   // normal
                mat.normal = this.normal;
            }   
            if (mask & bg.base.PBRMaterialFlag.NORMAL_SCALE) {             // normalScale
                mat.normalScale = this.normalScale;
            }   
            if (mask & bg.base.PBRMaterialFlag.NORMAL_OFFSET) {            // normalOffset
                mat.normalOffset = this.normalOffset;
            }   
            if (mask & bg.base.PBRMaterialFlag.LIGHT_MAP) {                // not used

            }   
            if (mask & bg.base.PBRMaterialFlag.SHADOWS) {                  // castShadows/receiveShadows
                mat.castShadows = this.castShadows;
            }   
            if (mask & bg.base.PBRMaterialFlag.CULL_FACE) {                // cullFace
                mat.cullFace = this.cullFace;
            }   
            if (mask & bg.base.PBRMaterialFlag.UNLIT) {                    // unlit
                mat.unlit = this.unlit;
            }   
        }

        applyModifier(context,mod,resourcePath) {
            if (mod.isEnabled(bg.base.PBRMaterialFlag.DIFFUSE_SCALE)) {    // diffuseScale
                this.diffuseScale = mod.diffuseScale;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.DIFFUSE_OFFSET)) {   // diffuseOffset
                this.diffuseOffset = mod.diffuseOffset;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.HEIGHT_SCALE)) {     // heightScale
                this.heightScale = mod.heightScale;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.NORMAL_SCALE)) {     // normalScale
                this.normalScale = mod.normalScale;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.NORMAL_OFFSET)) {    // normalOffset
                this.normalOffset = mod.normalOffset;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.SHADOWS)) {          // castShadows/receiveShadows
                this.castShadows = mod.castShadows;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.CULL_FACE)) {        // cullFace
                this.cullFace = mod.cullFace;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.UNLIT)) {            // unlit
                this.unlit = mod.unlit;
            }
            
            // Texture or scalar/vector
            function getTexture(texturePath) {
                if (!bg.base.imageTools.isAbsolutePath(texturePath)) {
                    texturePath = bg.base.imageTools.mergePath(resourcePath, texturePath);
                }

                let result = bg.base.TextureCache.Get(context).find(texturePath);
                if (!result) {
                    result = new bg.base.Texture(context);
                    result.create();
                    result.fileName = texturePath;
                    bg.base.TextureCache.Get(context).register(texturePath,result);

                    (function(p,t) {
                        bg.utils.Resource.Load(p)
                            .then((imgData) => {
                                t.bind();
                                t.minFilter = bg.base.TextureLoaderPlugin.GetMinFilter();
                                t.magFilter = bg.base.TextureLoaderPlugin.GetMagFilter();
                                t.fileName = p;
                                t.setImage(imgData);
                            });
                    })(texturePath,result);
                }
            }

            function textureOrScalar(value) {
                if (typeof(value)=="string" && value!=="") {
                    // texture
                    return getTexture(value);
                }
                else if (!isNaN(value)) {
                    // scalar
                    return value;
                }
                else {
                    throw new Error("Invalid parameter: expecting texture path or scalar");
                }
            }
            function textureOrColor(value) {
                if (typeof(value)=="string" && value!=="") {
                    // texture
                    return getTexture(value);
                }
                else if (value instanceof bg.Color) {
                    // color
                    return value;
                }
                else {
                    throw new Error("Invalid parameter: expecting texture path or color");
                }
            }

            if (mod.isEnabled(bg.base.PBRMaterialFlag.DIFFUSE)) {          // diffuse, isTransparent, alphaCutoff
                this.diffuse = textureOrColor(mod.diffuse);
                this.isTransparent = mod.isTransparent;
                this.alphaCutoff = mod.alphaCutoff;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.METALLIC)) {         // metallic, metallicChannel
                this.metallic = textureOrScalar(mod.metallic);
                this.metallicChannel = mod.metallicChannel;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.ROUGHNESS)) {        // roughness, roughnessChannel
                this.roughness = textureOrScalar(mod.roughness);
                this.roughnessChannel = mod.roughnessChannel;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.LIGHT_EMISSION)) {   // lightEmission, lightEmissionChannel
                this.lightEmission = textureOrScalar(mod.lightEmission);
                this.lightEmissionChannel = mod.lightEmissionChannel;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.HEIGHT)) {           // height, heightChannel
                this.height = textureOrScalar(mod.height);
                this.heightChannel = mod.heightChannel;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.NORMAL)) {           // normal
                this.normal = textureOrColor(mod.normal);
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.LIGHT_MAP)) {        // not used
            }
        }

        getModifierWithMask(modifierMask) {
            let mod = new PBRMaterialModifier();
            mod.modifierMask = modifierMask;

            function colorOrTexturePath(paramName) {
                let data = this[paramName];
                if (data instanceof bg.base.Texture) {
                    return data.fileName;
                }
                else {
                    return data;
                }
            }

            function scalarOrTexturePath(paramName) {
                let data = this[paramName];
                if (data instanceof bg.base.Texture) {
                    return data.fileName;
                }
                else {
                    return data;
                }
            }

            if (mod.isEnabled(bg.base.PBRMaterialFlag.DIFFUSE)) {
                mod.diffuse = colorOrTexturePath.apply(this,["diffuse"]);
                mod.isTransparent = this.isTransparent;
                mod.alphaCutoff = this.alphaCutoff;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.DIFFUSE_SCALE)) {
                mod.diffuseScale = this.diffuseScale;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.DIFFUSE_OFFSET)) {
                mod.diffuseOffset = this.diffuseOffset;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.METALLIC)) {
                mod.metallic = scalarOrTexturePath.apply(this,["metallic"]);
                mod.metallicChannel = this.metallicChannel;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.ROUGHNESS)) {
                mod.roghness = scalarOrTexturePath.apply(this,["roughness"]);
                mod.roughnessChannel = this.roughnessChannel;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.LIGHT_EMISSION)) {
                mod.lightEmission = scalarOrTexturePath.apply(this,["lightEmission"]);
                mod.lightEmissionChannel = this.lightEmissionChannel;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.HEIGHT)) {
                mod.height = scalarOrTexturePath.apply(this,["height"]);
                mod.heightChannel = this.heightChannel;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.HEIGHT_SCALE)) {
                mod.heightScale = this.heightScale;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.NORMAL)) {
                mod.normal = colorOrTexturePath.apply(this,["normal"]);
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.NORMAL_SCALE)) {
                mod.normalScale = this.normalScale;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.NORMAL_OFFSET)) {
                mod.normalOffset = this.normalOffset;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.LIGHT_MAP)) {
                // not used
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.SHADOWS)) {
                mod.castShadows = this.castShadows;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.CULL_FACE)) {
                mod.cullFace = this.cullFace;
            }
            if (mod.isEnabled(bg.base.PBRMaterialFlag.UNLIT)) {
                mod.unlit = this.unlit;
            }

            return mod;
        }

        static GetMaterialWithJson(context,data,path) {
            return PBRMaterial.FromMaterialDefinition(context,data,path);            
        }
    }

    bg.base.PBRMaterial = PBRMaterial;

})();