(function() {

    class PBRForwardRenderer extends bg.render.ForwardRenderer {
        constructor(context) {
            super(context);
        }

        create() {
            let ctx = this.context;

            this._transparentLayer = new bg.render.PBRForwardRenderLayer(this.context,bg.base.OpacityLayer.TRANSPARENT);
            this._opaqueLayer = new bg.render.PBRForwardRenderLayer(this.context,bg.base.OpacityLayer.OPAQUE);

            this._shadowMap = new bg.base.ShadowMap(ctx);
			this._shadowMap.size = new bg.Vector2(2048);

			this.settings.shadows.cascade = bg.base.ShadowCascade.NEAR;

			this._renderQueueVisitor = new bg.scene.RenderQueueVisitor
        }

        display(sceneRoot,camera) {
            let sceneEnvironment = bg.scene.Environment.Get();
            if (sceneEnvironment) {
                sceneEnvironment.environment.update(camera);
            }

            super.draw(sceneRoot,camera);
            
            if (sceneEnvironment) {
                sceneEnvironment.environment.renderSkybox(camera);
            }
		}
    }

    bg.render.PBRForwardRenderer = PBRForwardRenderer;
})();