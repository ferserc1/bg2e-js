
// This sample shows how to create scene components to perform operations in the scene, and
// how to setup a simple scene renderer to handle the pipeline setup.
// The scene renderer class will create a pipeline, but we can modify it if we want to use it to
// render offscreen.


///// Components
// We can create custom components to add functionality
// to the scene nodes. This component adds a rotation to the transform
// component of the node, if the node has one.
class AnimationComponent extends bg.scene.Component {
	constructor(increment = 0.001,rotX=0) {
		super();
		
		this._rotX = rotX;
		this._rotY = 0;
		this._increment = increment;
	}
	
	get increment() { return this._increment; }
	set increment(i) { this._increment = i; }
	
	// We can use a visitor to traverse the scene tree and call
	// all the component life cycle callbacks.
	frame(delta) {
		this._rotY += delta * this.increment;
		if (this.transform) {
			this.transform.matrix
				.identity()
				.rotate(this._rotY,0,1,0)
				.rotate(bg.Math.degreesToRadians(this._rotX),-1,0,0);
		}
	}
}

// Register the component before use it. The las parameter is the component Id, and it will
// be used to create new instances and to identify the component.
bg.scene.registerComponent(window,AnimationComponent,"AnimationComponent");

class SimpleSceneSampleWindowController extends bg.app.WindowController {
	buildLight() {
		let light = new bg.base.Light(this.gl);
		light.ambient = bg.Color.Black();
		light.diffuse = new bg.Color(0.65,0.75,0.95,1);
		light.specular = new bg.Color(0.5, 0.6,1,1);
		return light;
	}
	
	// Create scene: this function creates a simple scene tree containing:
	//	- One node with a drawable, a transform and an AnimationComponent component. The
	//	  drawable component contains a polyList with a cube.
	//	- One node with a light, a transform and an AnimationComponent component.
	//	- Two nodes with one camera. We will present the two cameras at the same time
	buildScene() {
		this._root = new bg.scene.Node(this.gl, "Root node");
		
		let cubeNode = new bg.scene.Node(this.gl, "Cube");
		this._root.addChild(cubeNode);
		
		cubeNode.addComponent(new bg.scene.Transform());
		cubeNode.addComponent(new bg.scene.Drawable());
		cubeNode.addComponent(new AnimationComponent(-0.0001));
		
		let drw = cubeNode.component("bg.scene.Drawable");
		drw.addPolyList(SampleUtils.Cube(this.gl),SampleUtils.Material(this.gl));
		
		// The light node is used to place the light in the scene to generate
		// its position and orientation automatically
		let lightNode = new bg.scene.Node(this.gl,"Light");
		this._root.addChild(lightNode);
		
		// The light component is used to bind the light to the scene node: the scene
		// node contains a light component, and the light component contains a light.
		this._lightComponent = new bg.scene.Light(this.buildLight());
		
		// Adding the light component to a node will activate the light. It's important to
		// add the light to the light component before add the light component to the node.
		lightNode.addComponent(this._lightComponent);	
		
		// We also add a transform node and an AnimationComponent to the light
		lightNode.addComponent(new bg.scene.Transform());
		lightNode.addComponent(new AnimationComponent(0.001,35));
		
		
		// We'll keep track of the camera status because we need to specify
		// which camera we want to render each frame. And sometimes we'll want to render
		// more than one camera, as we see here.
		// We also need to setup the projection matrix in the reshape() callback
		this._leftCamera = new bg.scene.Camera();
		let cameraNode = new bg.scene.Node("Left camera");
		cameraNode.addComponent(this._leftCamera);			
		cameraNode.addComponent(new bg.scene.Transform());
		cameraNode.component("bg.scene.Transform").matrix
			.translate(0.2,0,0)
			.rotate(bg.Math.degreesToRadians(22.5),-1,0,0)
			.translate(0,0,6);
		this._root.addChild(cameraNode);
		

		// The second camera. Note that the second camera is configured to not delte
		// the color buffer, because this action is performed by the first camera.
		this._rightCamera = new bg.scene.Camera();
		this._rightCamera.clearBuffers = 0;	// Don't clear the second camera's buffer
		let cameraNode2 = new bg.scene.Node("Right camera");
		cameraNode2.addComponent(this._rightCamera);
		cameraNode2.addComponent(new bg.scene.Transform());
		cameraNode2.component("bg.scene.Transform").matrix
			.translate(-0.2,0,0)
			.rotate(bg.Math.degreesToRadians(22.5),-1,0,0)
			.translate(0,0,6);
		this._root.addChild(cameraNode2);
	}
    
	init() {
		// Use WebGL V1 engine
		bg.Engine.Set(new bg.webgl1.Engine(this.gl));
		
		// Build the scene
        this.buildScene();
		
		// Create the renderer.
		// You may use the Renderer factory to ensure that your browser supports the specified renderer 
		this._renderer = new bg.render.Renderer.Create(this.gl,bg.render.RenderPath.FORWARD);
		
		// We can access to the renderer pipeline with:
		//		this._renderer.pipeline
	}
    
    frame(delta) {
		// Delegate the frame() function to the renderer. The renderer receives the scene and the delta as parameters
		this._renderer.frame(this._root, delta);
    }
	
	display() {
		// To render the scene, we specify the scene and the camera.
		this._renderer.display(this._root, this._leftCamera);
		this._renderer.display(this._root, this._rightCamera);
	}
	
	reshape(width,height) {
		// Set the left and right camera viewports and projections
		this._leftCamera.viewport = new bg.Viewport(0,0,width/2,height);
		this._leftCamera.projection.perspective(60,this._leftCamera.viewport.aspectRatio,0.1,100);

		this._rightCamera.viewport = new bg.Viewport(width/2,0,width/2,height);
		this._rightCamera.projection.perspective(60,this._rightCamera.viewport.aspectRatio,0.1,100);
	}
}

function load() {
	let controller = new SimpleSceneSampleWindowController();
	let mainLoop = bg.app.MainLoop.singleton;
	
	mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
	mainLoop.canvas = document.getElementsByTagName('canvas')[0];
	mainLoop.run(controller);
}
