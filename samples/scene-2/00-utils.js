class SampleUtils {
	static Cube(context) {
		let plist = new bg.base.PolyList(context);
        
		plist.vertex = [
			 1,-1,-1, -1,-1,-1, -1, 1,-1,  1, 1,-1,		// back face
			 1,-1, 1,  1,-1,-1,  1, 1,-1,  1, 1, 1,		// right face 
			-1,-1, 1,  1,-1, 1,  1, 1, 1, -1, 1, 1, 	// front face
			-1,-1,-1, -1,-1, 1, -1, 1, 1, -1, 1,-1,		// left face
			-1, 1, 1,  1, 1, 1,  1, 1,-1, -1, 1,-1,		// top face
			 1,-1, 1, -1,-1, 1, -1,-1,-1,  1,-1,-1		// bottom face
		];
		
		plist.normal = [
			 0, 0,-1,  0, 0,-1,  0, 0,-1,  0, 0,-1,		// back face
			 1, 0, 0,  1, 0, 0,  1, 0, 0,  1, 0, 0,		// right face 
			 0, 0, 1,  0, 0, 1,  0, 0, 1,  0, 0, 1, 	// front face
			-1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0,		// left face
			 0, 1, 0,  0, 1, 0,  0, 1, 0,  0, 1, 0,		// top face
			 0,-1, 0,  0,-1, 0,  0,-1, 0,  0,-1, 0		// bottom face
		];
		
		plist.texCoord0 = [
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1
		];
		
		plist.texCoord1 = [
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1
		];
		
		plist.index = [
			 0, 1, 2,	 2, 3, 0,
			 4, 5, 6,	 6, 7, 4,
			 8, 9,10,	10,11, 8,
			12,13,14,	14,15,12,
			16,17,18,	18,19,16,
			20,21,22,	22,23,20
		];
		
        plist.build();
		
		return plist;
	}
	
	static Material(context) {
		let material = new bg.base.Material();
		material.shininess = 20;
		
		bg.utils.Resource.Load("../data/bricks.jpg")
			.then((img) => {
				let texture = new bg.base.Texture(context);
				texture.create();
				texture.bind();
				texture.minFilter = bg.base.TextureFilter.LINEAR;
				texture.magFilter = bg.base.TextureFilter.LINEAR;
				texture.setImage(img);
				material.texture = texture;
			});
			
		bg.utils.Resource.Load("../data/bricks_nm.png")
			.then((img) => {
				let texture = new bg.base.Texture(context);
				texture.create();
				texture.bind();
				texture.minFilter = bg.base.TextureFilter.LINEAR;
				texture.magFilter = bg.base.TextureFilter.LINEAR;
				texture.setImage(img);
				material.normalMap = texture;
			});
			
		return material;
	}
}