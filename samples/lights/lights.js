
class DeferredBasicWindowController extends bg.app.WindowController {
	addLight(parent,alpha,distance,y) {
		let x = Math.sin(bg.Math.degreesToRadians(alpha)) * distance;
		let z = Math.cos(bg.Math.degreesToRadians(alpha)) * distance;
		let lightNode = new bg.scene.Node(this.gl,`light_${ alpha }`);

		let light = new bg.base.Light(this.gl);
		light.type = bg.base.LightType.POINT;
		light.ambient = bg.Color.Black();
		window.lights = window.lights || [];
		window.lights.push(light);
		lightNode.addComponent(new bg.scene.Light(light));
		lightNode.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(x,y,z)));
		parent.addChild(lightNode);
	}

	buildScene() {
		this._root = new bg.scene.Node(this.gl, "Root node");
		
		bg.base.Loader.RegisterPlugin(new bg.base.TextureLoaderPlugin());
		bg.base.Loader.RegisterPlugin(new bg.base.VWGLBLoaderPlugin());
		
		bg.base.Loader.Load(this.gl,"../data/test-shape.vwglb")
			.then((node) => {
				this._root.addChild(node);
				node.addComponent(new bg.scene.Transform(bg.Matrix4.Scale(0.5,0.5,0.5)));
				
				this.postRedisplay();	// Update the view manually, because in this sample the auto update is disabled
			})
			
			.catch(function(err) {
				alert(err.message);
			});
	
		let floorNode = new bg.scene.Node(this.gl,"Floor");
		floorNode.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(0,-0.26,0)));
		floorNode.addComponent(bg.scene.PrimitiveFactory.Plane(this.gl,30,30));
		floorNode.component("bg.scene.Drawable").getMaterial(0).shininess = 50;
		floorNode.component("bg.scene.Drawable").getMaterial(0).reflectionAmount = 0.32;
		floorNode.component("bg.scene.Drawable").getMaterial(0).normalMapScale = new bg.Vector2(10,10);
		floorNode.component("bg.scene.Drawable").getMaterial(0).textureScale = new bg.Vector2(10,10);
		floorNode.component("bg.scene.Drawable").getMaterial(0).reflectionMaskInvert = false;
		floorNode.component("bg.scene.Drawable").getMaterial(0).shininessMaskInvert = true;
		this._root.addChild(floorNode);

		bg.base.Loader.Load(this.gl,"../data/bricks_nm.png")
			.then((tex) => {
				floorNode.component("bg.scene.Drawable").getMaterial(0).normalMap = tex;
			});

		bg.base.Loader.Load(this.gl,"../data/bricks.jpg")
			.then((tex) => {
				floorNode.component("bg.scene.Drawable").getMaterial(0).texture = tex;
			});

		bg.base.Loader.Load(this.gl,"../data/bricks_shin.jpg")
			.then((tex) => {
				floorNode.component("bg.scene.Drawable").getMaterial(0).reflectionMask = tex;
				floorNode.component("bg.scene.Drawable").getMaterial(0).shininessMask = tex;
			});


		// This cubemap is used to show something when the ray fall out the viewport
		let cm = new bg.scene.Cubemap();
		this._root.addComponent(cm);
		cm.setImageUrl(bg.scene.CubemapImage.POSITIVE_X,"../data/sample-scene/posx.jpg");
		cm.setImageUrl(bg.scene.CubemapImage.NEGATIVE_X,"../data/sample-scene/negx.jpg");
		cm.setImageUrl(bg.scene.CubemapImage.POSITIVE_Y,"../data/sample-scene/posy.jpg");
		cm.setImageUrl(bg.scene.CubemapImage.NEGATIVE_Y,"../data/sample-scene/negy.jpg");
		cm.setImageUrl(bg.scene.CubemapImage.POSITIVE_Z,"../data/sample-scene/posz.jpg");
		cm.setImageUrl(bg.scene.CubemapImage.NEGATIVE_Z,"../data/sample-scene/negz.jpg");
		cm.loadCubemap(this.gl)
			.then(() => {
				this.postRedisplay();
			});

		for (let i = 0; i<360; i+=36) {
			this.addLight(this._root,i,6,0.4);
		}
		
		this._camera = new bg.scene.Camera();
		let cameraNode = new bg.scene.Node("Camera");
		cameraNode.addComponent(this._camera);			
		cameraNode.addComponent(new bg.scene.Transform());
		cameraNode.addComponent(new bg.manipulation.OrbitCameraController());
		let camCtrl = cameraNode.component("bg.manipulation.OrbitCameraController");
		camCtrl.minPitch = -45;
		this._root.addChild(cameraNode);
	}
    
	init() {
		bg.Engine.Set(new bg.webgl1.Engine(this.gl));
		
        this.buildScene();
		
		this._renderer = new bg.render.Renderer.Create(this.gl,bg.render.RenderPath.DEFERRED);
		this._inputVisitor = new bg.scene.InputVisitor();

		
		this._renderer.settings.ambientOcclusion.kernelSize = 16;
		this._renderer.settings.ambientOcclusion.blur = 2;
		this._renderer.settings.ambientOcclusion.sampleRadius = 0.15;
		this._renderer.settings.raytracer.quality = bg.render.RaytracerQuality.low;
		this._renderer.settings.raytracer.basicReflections = false;
	}
    
    frame(delta) { this._renderer.frame(this._root, delta); }
	
	display() { this._renderer.display(this._root, this._camera); }
	
	reshape(width,height) {
		this._camera.viewport = new bg.Viewport(0,0,width,height);
		this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
	}
	
	// Pass the input events to the scene
	mouseDown(evt) {
		this._renderer.settings.ambientOcclusion.enabled = false;
		this._inputVisitor.mouseDown(this._root,evt);
	}
	
	mouseDrag(evt) {
		this._inputVisitor.mouseDrag(this._root,evt);
		this.postRedisplay();
	}
	
	mouseWheel(evt) {
		this._inputVisitor.mouseWheel(this._root,evt);
		this.postRedisplay();
	}
	
	touchStart(evt) {
		this._inputVisitor.touchStart(this._root,evt);
	}
	
	touchMove(evt) {
		this._inputVisitor.touchMove(this._root,evt);
		this.postRedisplay();
	}
	
	// You may pass also the following events, but they aren't used by the camera controller
	mouseUp(evt) {
		this._inputVisitor.mouseUp(this._root,evt);
		this._renderer.settings.ambientOcclusion.enabled = true;
		this.postRedisplay();
	}
	mouseMove(evt) { this._inputVisitor.mouseMove(this._root,evt); }
	mouseOut(evt) { this._inputVisitor.mouseOut(this._root,evt); }
	touchEnd(evt) { this._inputVisitor.touchEnd(this._root,evt); }
}

function load() {
	let controller = new DeferredBasicWindowController();
	let mainLoop = bg.app.MainLoop.singleton;
	window.windowController = controller;
	
	mainLoop.updateMode = bg.app.FrameUpdate.MANUAL;
	mainLoop.canvas = document.getElementsByTagName('canvas')[0];
	mainLoop.run(controller);
}
