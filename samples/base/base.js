

class BaseSampleWindowController extends bg.app.WindowController {
	constructor() {
		super();
		this.increment = new bg.Color(0.001,0.0009,0.013,0.0);
	}
	
	init() {
		// Use WebGL V1 engine
		bg.Engine.Set(new bg.webgl1.Engine(this.gl));
		
		this.pipeline = new bg.base.Pipeline(this.gl);
		bg.base.Pipeline.SetCurrent(this.pipeline);
	}

	frame(delta) {
		this.pipeline.clearColor.add(this.increment);
		if (this.pipeline.clearColor.r>1 || this.pipeline.clearColor.r<0) {
			this.increment.r *= -1;
		}
		if (this.pipeline.clearColor.g>1 || this.pipeline.clearColor.g<0) {
			this.increment.g *= -1;
		}
		if (this.pipeline.clearColor.b>1 || this.pipeline.clearColor.b<0) {
			this.increment.b *= -1;
		}
	}
	
	display() {
		this.pipeline.clearBuffers(bg.base.ClearBuffers.COLOR | bg.base.ClearBuffers.DEPTH);
	}
	
	reshape(width,height) {
		this.pipeline.viewport = new bg.Viewport(0,0,width,height);
	}
}

function load() {
	let controller = new BaseSampleWindowController();
	let mainLoop = bg.app.MainLoop.singleton;
	
	mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
	mainLoop.canvas = document.getElementsByTagName('canvas')[0];
	mainLoop.run(controller);
}