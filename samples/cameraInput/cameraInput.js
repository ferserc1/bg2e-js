
class CameraInputWindowController extends bg.app.WindowController {
	buildScene() {
		this._root = new bg.scene.Node(this.gl, "Root node");
		
		bg.base.Loader.RegisterPlugin(new bg.base.TextureLoaderPlugin());
		bg.base.Loader.RegisterPlugin(new bg.base.VWGLBLoaderPlugin());
		
		bg.base.Loader.Load(this.gl,"../data/test-shape.vwglb")
			.then((node) => {
				this._root.addChild(node);
				
				this.postRedisplay();	// Update the view manually, because in this sample the auto update is disabled
			})
			
			.catch(function(err) {
				alert(err.message);
			});
		
			
		let lightNode = new bg.scene.Node(this.gl,"Light");
		let l = new bg.base.Light(this.gl);
		l.type = bg.base.LightType.SPOT;
		lightNode.addComponent(new bg.scene.Light(l));	
		lightNode.addComponent(new bg.scene.Transform(bg.Matrix4.Identity()
			.rotate(bg.Math.degreesToRadians(30),0,1,0)
			.rotate(bg.Math.degreesToRadians(65),-1,0,0)
			.translate(0,0,10)));
			
		this._root.addChild(lightNode);

		let skyboxNode = new bg.scene.Node(this.gl,"Skybox");
		let skybox = new bg.scene.Skybox();
		skyboxNode.addComponent(skybox);
		skybox.setImageUrl(bg.scene.CubemapImage.POSITIVE_X,"../data/posx.jpg");
		skybox.setImageUrl(bg.scene.CubemapImage.NEGATIVE_X,"../data/negx.jpg");
		skybox.setImageUrl(bg.scene.CubemapImage.POSITIVE_Y,"../data/posy.jpg");
		skybox.setImageUrl(bg.scene.CubemapImage.NEGATIVE_Y,"../data/negy.jpg");
		skybox.setImageUrl(bg.scene.CubemapImage.POSITIVE_Z,"../data/posz.jpg");
		skybox.setImageUrl(bg.scene.CubemapImage.NEGATIVE_Z,"../data/negz.jpg");
		skybox.loadSkybox();
		this._root.addChild(skyboxNode);

		let floor = new bg.scene.Node(this.gl,"Floor");
		floor.addComponent(new bg.scene.Transform(0,-0.5,0));
		floor.addComponent(bg.scene.PrimitiveFactory.Plane(this.gl,10,10));
		this._root.addChild(floor);

		
		this._camera = new bg.scene.Camera();
		let cameraNode = new bg.scene.Node("Camera");
		cameraNode.addComponent(this._camera);			
		cameraNode.addComponent(new bg.scene.Transform());
		let controller = new bg.manipulation.OrbitCameraController();
		controller.minPitch = -90;
		cameraNode.addComponent(controller);
		this._root.addChild(cameraNode);
	}
    
	init() {
		bg.Engine.Set(new bg.webgl1.Engine(this.gl));
		
        this.buildScene();
		
		this._renderer = bg.render.Renderer.Create(this.gl,bg.render.RenderPath.FORWARD);
		
		this._inputVisitor = new bg.scene.InputVisitor();
	}

	capture() {
		// Capture a frame from a renderer
		if (!this._img) {
			this._img = new Image();
			this._img.style = 'position: fixed; top: 10px; left: 10px; z-index: 1;'
			document.body.appendChild(this._img);
		}
		this._img.src = this._renderer.getImage(this._root,this._camera,200,100);
	}
    
    frame(delta) { this._renderer.frame(this._root, delta); }
	
	display() {
		this._renderer.display(this._root, this._camera);
	}
	
	reshape(width,height) {
		this._camera.viewport = new bg.Viewport(0,0,width,height);
		this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
	}
	
	// Pass the input events to the scene
	mouseDown(evt) {
		this._inputVisitor.mouseDown(this._root,evt);
	}
	
	mouseDrag(evt) {
		this._inputVisitor.mouseDrag(this._root,evt);
		this.postRedisplay();
	}
	
	mouseWheel(evt) {
		this._inputVisitor.mouseWheel(this._root,evt);
		this.postRedisplay();
	}
	
	touchStart(evt) {
		this._inputVisitor.touchStart(this._root,evt);
	}
	
	touchMove(evt) {
		this._inputVisitor.touchMove(this._root,evt);
		this.postRedisplay();
	}
	
	// You may pass also the following events, but they aren't used by the camera controller
	mouseUp(evt) { this._inputVisitor.mouseUp(this._root,evt); }
	mouseMove(evt) { this._inputVisitor.mouseMove(this._root,evt); }
	mouseOut(evt) { this._inputVisitor.mouseOut(this._root,evt); }
	touchEnd(evt) { this._inputVisitor.touchEnd(this._root,evt); }

	keyUp(evt) {
		if (evt.key=="Space") {
			this.capture();
			this.postRedisplay();
		}
	}
}

function load() {
	let controller = new CameraInputWindowController();
	let mainLoop = bg.app.MainLoop.singleton;
	window.windowController = controller;

	mainLoop.updateMode = bg.app.FrameUpdate.MANUAL;
	mainLoop.canvas = document.getElementsByTagName('canvas')[0];
	mainLoop.run(controller);
}
