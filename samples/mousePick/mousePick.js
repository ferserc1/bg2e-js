
class MousePickWindowController extends bg.app.WindowController {
	buildScene() {
		this._root = new bg.scene.Node(this.gl, "Root node");
		
		bg.base.Loader.RegisterPlugin(new bg.base.TextureLoaderPlugin());
		bg.base.Loader.RegisterPlugin(new bg.base.VWGLBLoaderPlugin());
		
		bg.base.Loader.Load(this.gl,"../data/test-shape.vwglb")
			.then((node) => {
				this._root.addChild(node);
				node.name = "Cube 1";
				node.addComponent(new bg.manipulation.Selectable());
				node.addComponent(new bg.scene.Transform());
				
				let cube2 = bg.scene.Drawable.InstanceNode(node);
				cube2.name = "Cube 2";
				cube2.component("bg.scene.Transform").matrix.translate(2,0,0);
				this._root.addChild(cube2);
				
				let cube3 = bg.scene.Drawable.InstanceNode(node);
				cube3.name = "Cube 3";
				cube3.component("bg.scene.Transform").matrix.translate(-2,0,0);
				this._root.addChild(cube3);
				
				this.postRedisplay();	// Update the view manually, because in this sample the auto update is disabled
			})
			
			.catch(function(err) {
				alert(err.message);
			});
		
		let lightNode = new bg.scene.Node(this.gl,"Light");
		lightNode.addComponent(new bg.scene.Light(new bg.base.Light(this.gl)));	
		lightNode.addComponent(new bg.scene.Transform(bg.Matrix4.Identity()
												.rotate(bg.Math.degreesToRadians(30),0,1,0)
												.rotate(bg.Math.degreesToRadians(65),-1,0,0)));
		this._root.addChild(lightNode);
		
		this._camera = new bg.scene.Camera();
		let cameraNode = new bg.scene.Node("Camera");
		cameraNode.addComponent(this._camera);			
		cameraNode.addComponent(new bg.scene.Transform());
		cameraNode.addComponent(new bg.manipulation.OrbitCameraController());
		this._root.addChild(cameraNode);
	}
    
	init() {
		bg.Engine.Set(new bg.webgl1.Engine(this.gl));
		
        this.buildScene();
		
		this._renderer = bg.render.Renderer.Create(this.gl,bg.render.RenderPath.FORWARD);
		
		this._inputVisitor = new bg.scene.InputVisitor();
		this._mousePicker = new bg.manipulation.MousePicker(this.gl);
		this._selectionHighlight = new bg.manipulation.SelectionHighlight(this.gl);
		this._selectionHighlight.highlightColor = new bg.Color(0,1,0,1);
		this._selectionHighlight.borderWidth = 1.0;
	}
    
    frame(delta) { this._renderer.frame(this._root, delta); }
	
	display() {
		this._renderer.display(this._root, this._camera);
		this._selectionHighlight.drawSelection(this._root, this._camera);
	}
	
	reshape(width,height) {
		this._camera.viewport = new bg.Viewport(0,0,width,height);
		this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
	}
	
	// Pass the input events to the scene
	mouseDown(evt) {
		this.initSelect(evt.x,evt.y);
		this._inputVisitor.mouseDown(this._root,evt);
	}
	
	mouseDrag(evt) {
		this._inputVisitor.mouseDrag(this._root,evt);
		this.postRedisplay();
	}
	
	mouseUp(evt) {
		this.endSelect(evt.x,evt.y);
	}
	
	initSelect(x,y) {
		this._downPosition = new bg.Vector2(x,y);
	}
	
	endSelect(x,y) {
		let upPosition = new bg.Vector2(x,y);
		// Ignore mouseUp if the user is dragging the mouse (with a little offset to avoid ignore accidental drag)
		if (Math.abs(this._downPosition.distance(upPosition))<3) {
			if (this._currentMat) {
				this._currentMat.selectMode = false;
			}

			let result = this._mousePicker.pick(this._root,this._camera,new bg.Vector2(x,y));
			let out = document.getElementById("out");
			if (result && result.type==bg.manipulation.SelectableType.PLIST) {
				out.innerHTML = `Node: ${result.node.name}`;
				this._currentNode = result.node;
				this._currentMat = result.material;
				this._currentMat.selectMode = true;
			}
			else {
				out.innerHTML = "";
				this._currentMat = null;
			}
		}
		this.postRedisplay();
	}
	
	mouseWheel(evt) {
		this._inputVisitor.mouseWheel(this._root,evt);
		this.postRedisplay();
	}
	
	touchStart(evt) {
		this._lastTouch = [evt.touches[0].x, evt.touches[0].y];
		this.initSelect(evt.touches[0].x,evt.touches[0].y);
		this._inputVisitor.touchStart(this._root,evt);
	}
	
	touchMove(evt) {
		this._lastTouch = [evt.touches[0].x, evt.touches[0].y];
		this._inputVisitor.touchMove(this._root,evt);
		
		this.postRedisplay();
	}
	
	mouseMove(evt) { this._inputVisitor.mouseMove(this._root,evt); }
	mouseOut(evt) { this._inputVisitor.mouseOut(this._root,evt); }
	touchEnd(evt) {
		this.endSelect(this._lastTouch[0],this._lastTouch[1]);
		this._inputVisitor.touchEnd(this._root,evt);
	}
}

function load() {
	let controller = new MousePickWindowController();
	let mainLoop = bg.app.MainLoop.singleton;
	
	mainLoop.updateMode = bg.app.FrameUpdate.MANUAL;
	mainLoop.canvas = document.getElementsByTagName('canvas')[0];
	mainLoop.run(controller);
}
