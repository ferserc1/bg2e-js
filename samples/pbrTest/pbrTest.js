
class PBRTestWindowController extends bg.app.WindowController {
	switchLights() {
		switch (true) {
		case this._directional.enabled:
			this._directional.enabled = false;
			this._point1.enabled = true;
			this._point2.enabled = true;
			this._spot.enabled = false;
			this._spot2.enabled = false;
			break;
		case this._point1.enabled:
			this._directional.enabled = false;
			this._point1.enabled = false;
			this._point2.enabled = false;
			this._spot.enabled = true;
			this._spot2.enabled = true;
			break;
		case this._spot.enabled:
			this._spot.enabled = false;
			this._spot2.enabled = false;
			this._directional.enabled = true;
			this._point1.enabled = false;
			this._point2.enabled = false;
			break;
		}
		this.postRedisplay();
	}
	
	switchEnvironment() {
		let env = bg.scene.Environment.Get() && bg.scene.Environment.Get().environment
		if (env) {
			switch (env.equirectangularTexture) {
			case this._env1:
				env.equirectangularTexture = this._env2;
				break;
			case this._env2:
				env.equirectangularTexture = this._env3;
				break;
			case this._env3:
				env.equirectangularTexture = this._env4;
				break;
			case this._env4:
				env.equirectangularTexture = this._env5;
				break;
			case this._env5:
				env.equirectangularTexture = this._env6;
				break;
			case this._env6:
				env.equirectangularTexture = this._env7;
				break;
			case this._env7:
				env.equirectangularTexture = this._env1;
				break;
			}
	
			this.postRedisplay();
		}
	}

	createLights() {
		let lightNode = new bg.scene.Node(this.gl,"Lights");
		this._root.addChild(lightNode);
		let addLightNode = (l) => {
			let n = new bg.scene.Node(this.gl);
			n.addComponent(new bg.scene.Light(l));
			n.addComponent(new bg.scene.Transform());
			lightNode.addChild(n);
			return n;
		}
		this._directional = new bg.base.Light(this.gl);
		this._directional.enabled = true;
		this._directional.shadowStrength = 0.9;
		this._directional.ambient = new bg.Color(0.1,0.1,0.13,1);
		addLightNode(this._directional).transform.matrix
			.rotate(bg.Math.degreesToRadians(15), 0,1,0)
			.rotate(bg.Math.degreesToRadians(55),-1,0,0);

		this._point1 = new bg.base.Light(this.gl);
		this._point1.enabled = false;
		this._point1.type = bg.base.LightType.POINT;
		//this._point1.diffuse = new bg.Color(0.8,0.32,0.11,1);
		this._point1.ambient = new bg.Color(0,0,0,1);
		this._point1.quadraticAttenuation = 0.1;
		addLightNode(this._point1).transform.matrix
			.translate(3.5,0.5,3.5);

		this._point2 = new bg.base.Light(this.gl);
		this._point2.type = bg.base.LightType.POINT;
		//this._point2.diffuse = new bg.Color(0.22,0.8,0.11,1);
		this._point2.ambient = new bg.Color(0,0,0,1);
		this._point2.quadraticAttenuation = 0.1;
		addLightNode(this._point2).transform.matrix
			.translate(-3.5,0.5,-3.5);

		this._spot2 = new bg.base.Light(this.gl);
		this._spot2.type = bg.base.LightType.SPOT;
		this._spot2.spotCutoff = 24;
		this._spot2.spotExponent = 18;
		this._spot2.diffuse = new bg.Color(0.87,0.87,0,1);
		this._spot2.ambient = new bg.Color(0,0,0,1);
		this._spot2.quadraticAttenuation = 0.03;
		addLightNode(this._spot2).transform.matrix
			.identity()
			.rotate(bg.Math.degreesToRadians(210),0,1,0)
			.rotate(bg.Math.degreesToRadians(35),-1,0,0)
			.translate(0,0,6);


		this._spot = new bg.base.Light(this.gl);
		this._spot.type = bg.base.LightType.SPOT;
		this._spot.spotCutoff = 24;
		this._spot.spotExponent = 19;
		this._spot.quadraticAttenuation = 0.03;
		this._spot.specularType = bg.base.SpecularType.BLINN;
		this._spot.diffuse = new bg.Color(0.87,0.17,0.77,1);
		this._spot.ambient = new bg.Color(0.003,0.003,0.003,1);
		addLightNode(this._spot).transform.matrix
			.identity()
			.rotate(bg.Math.degreesToRadians(30),0,1,0)
			.rotate(bg.Math.degreesToRadians(45),-1,0,0)
			.translate(0,0,6);

		this.switchLights();
	}

	buildScene() {
		this._root = new bg.scene.Node(this.gl, "Root node");
		
		bg.base.Loader.RegisterPlugin(new bg.base.TextureLoaderPlugin());
		bg.base.Loader.RegisterPlugin(new bg.base.VWGLBLoaderPlugin());		
		this.createLights();

		let cubemap = new bg.scene.Cubemap();
		this._root.addComponent(cubemap);

		let floor = new bg.scene.Node(this.gl,"Floor");
		floor.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(0,-0.5,0)));
		bg.scene.PrimitiveFactory.SetPBRMaterials(true);
		floor.addComponent(bg.scene.PrimitiveFactory.Plane(this.gl,40,40));
		this.floorMat = floor.drawable.getMaterial(0);
		//this.floorMat.specular = new bg.Color(0.3, 0.3, 0.3, 1.0);
		this.floorMat.diffuseScale = new bg.Vector2(24,24);
		this.floorMat.normalScale = new bg.Vector2(24,24);
		this.floorMat.roughness = 0.3;
		this.floorMat.metallic = 0;
		document.getElementById("metallic").value = this.floorMat.metallic;
		document.getElementById("roughness").value = this.floorMat.roughness;
		this._root.addChild(floor);

		let sphere = new bg.scene.Node(this.gl,"Sphere");
		sphere.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(0,0.5,0)));
		sphere.addComponent(bg.scene.PrimitiveFactory.Sphere(this.gl,1.0,30,30));
		this.sphereMat = sphere.drawable.getMaterial(0);
		this.sphereMat.diffuseScale = new bg.Vector2(2,2);
		this.sphereMat.normalScale = new bg.Vector2(2,2);
		// this.sphereMat.metallic = 0.0;
		// this.sphereMat.roughness = 0.94;
		this._root.addChild(sphere);

		let s2 = new bg.scene.Node(this.gl);
		s2.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(1.7,0.5,1.7)));
		s2.addComponent(bg.scene.PrimitiveFactory.Sphere(this.gl,0.5,30,30));
		this.s2Mat = s2.drawable.getMaterial(0);
		this.s2Mat.diffuse = new bg.Color(1.0,0.2,0.3,1);
		this.s2Mat.metallic = 0.03;
		this.s2Mat.roughness = 0.95;
		this.s2Mat.diffuseScale = new bg.Vector2(8,8);
		this.s2Mat.normalScale = new bg.Vector2(8,8);
		this.s2Mat.cullFace = false;
		this._root.addChild(s2);

		let s3 = new bg.scene.Node(this.gl);
		s3.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(-1.7,0.5,-1.7)));
		s3.addComponent(bg.scene.PrimitiveFactory.Sphere(this.gl,0.5,30,30));
		this.s3Mat = s3.drawable.getMaterial(0);
		this.s3Mat.diffuse = new bg.Color(1.0,0.2,0.3,1);
		this.s3Mat.metallic = 0.2;
		this.s3Mat.roughness = 0.5;
		this.s3Mat.diffuseScale = new bg.Vector2(4,4);
		this.s3Mat.normalScale = new bg.Vector2(4,4);
		this._root.addChild(s3);

		let s4 = new bg.scene.Node(this.gl);
		s4.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(1.7,0.5,-1.7)));
		s4.addComponent(bg.scene.PrimitiveFactory.Sphere(this.gl,0.5,30,30));
		this.s4Mat = s4.drawable.getMaterial(0);
		this.s4Mat.diffuse = new bg.Color(1.0,0.2,0.3,1);
		this.s4Mat.metallic = 0.7;
		this.s4Mat.roughness = 0.3;
		this.s4Mat.diffuseScale = new bg.Vector2(4,4);
		this.s4Mat.normalScale = new bg.Vector2(4,4);
		this._root.addChild(s4);

		let s5 = new bg.scene.Node(this.gl);
		s5.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(-1.7,0.5,1.7)));
		s5.addComponent(bg.scene.PrimitiveFactory.Sphere(this.gl,0.5,30,30));
		this.s5Mat = s5.drawable.getMaterial(0);
		this.s5Mat.diffuse = new bg.Color(1.0,0.2,0.3,1);
		this.s5Mat.metallic = 0.9;
		this.s5Mat.roughness = 0.1;
		this.s5Mat.diffuseScale = new bg.Vector2(4,4);
		this.s5Mat.normalScale = new bg.Vector2(4,4);
		this._root.addChild(s5);

		let s6 = new bg.scene.Node(this.gl);
		s6.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(0.0,0.5, 2.3)));
		s6.addComponent(bg.scene.PrimitiveFactory.Sphere(this.gl,0.5,30,30));
		this.s6Mat = s6.drawable.getMaterial(0);
		this.s6Mat.roughness = 0.3;
		this.s6Mat.metallic = 0;
		this._root.addChild(s6);

		let s7 = new bg.scene.Node(this.gl);
		s7.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(0.0,0.5,-2.3)));
		s7.addComponent(bg.scene.PrimitiveFactory.Sphere(this.gl,0.5,30,30));
		this.s7Mat = s7.drawable.getMaterial(0);
		this.s7Mat.roughness = 0.3;
		this.s7Mat.metallic = 0;
		this.s7Mat.diffuse = new bg.Color(0.2,0.4,1.0,1.0);
		this._root.addChild(s7);

		let s8 = new bg.scene.Node(this.gl);
		s8.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(2.3,0.5,0)));
		s8.addComponent(bg.scene.PrimitiveFactory.Sphere(this.gl,0.5,30,30));
		this.s8Mat = s8.drawable.getMaterial(0);
		this.s8Mat.roughness = 0.3;
		this.s8Mat.metallic = 0;
		this.s8Mat.diffuse = new bg.Color(0.1,0.9,0.2,1.0);
		this._root.addChild(s8);

		let s9 = new bg.scene.Node(this.gl);
		s9.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(-2.3,0.5,0)));
		s9.addComponent(bg.scene.PrimitiveFactory.Sphere(this.gl,0.5,30,30));
		this.s9Mat = s9.drawable.getMaterial(0);
		this.s9Mat.roughness = 0.3;
		this.s9Mat.metallic = 0;
		this.s9Mat.diffuse = new bg.Color(0.9,0.24,0.2,1.0);
		this._root.addChild(s9);

		bg.base.Loader.Load(this.gl,"../data/bricks2_disp.jpg")
			.then((height) => {
				this.floorMat.height = height;
				this.s2Mat.height = height;
				this.s3Mat.height = height;
				this.s4Mat.height = height;
				this.s5Mat.height = height;
				return bg.base.Loader.Load(this.gl,"../data/bricks2_diffuse.jpg");
			})

			.then((diffuse) => {
				this.floorMat.diffuse = diffuse;
				return bg.base.Loader.Load(this.gl,"../data/bricks2_normal.jpg");
			})

			.then((normal) => {
				this.floorMat.normal = normal;
				this.s2Mat.normal = normal;
				this.s3Mat.normal = normal;
				this.s4Mat.normal = normal;
				this.s5Mat.normal = normal;
				return bg.base.Loader.Load(this.gl,"../data/grid.png");
			})

			.then((grid) => {
				this.s2Mat.diffuse = grid;
				return bg.base.Loader.Load(this.gl,"../data/grid_height.jpg");
			})

			.then((gridHeight) => {
				this.s2Mat.height = gridHeight;
				return bg.base.Loader.Load(this.gl,"../data/grid_nm.jpg");
			})

			.then((gridNM) => {
				this.s2Mat.normal = gridNM;
			})

			.catch((err) => {
				alert(err.message);
			});

		bg.base.Loader.Load(this.gl,"../data/pbr/rustediron2_basecolor.jpg")
			.then((albedo) => {
				this.sphereMat.diffuse = albedo;
				return bg.base.Loader.Load(this.gl,"../data/pbr/rustediron2_metallic.jpg");
			})

			.then((metallic) => {
				this.sphereMat.metallic = metallic;
				return bg.base.Loader.Load(this.gl,"../data/pbr/rustediron2_normal.jpg");
			})

			.then((normal) => {
				this.sphereMat.normal = normal;
				return bg.base.Loader.Load(this.gl,"../data/pbr/rustediron2_roughness.jpg")
			})

			.then((roughness) => {
				this.sphereMat.roughness = roughness;
			})

			.catch((err) => {
				console.warn(err.message);
			});

		
		this._camera = new bg.scene.Camera();
		let cameraNode = new bg.scene.Node("Camera");
		cameraNode.addComponent(this._camera);			
		cameraNode.addComponent(new bg.scene.Transform());
		let controller = new bg.manipulation.OrbitCameraController();
		controller.minPitch = -90;
		cameraNode.addComponent(controller);
		this._root.addChild(cameraNode);

		// To create a custom size environment cubemap, you can
		// create it manually and pass it to the environment component
		// let env = new bg.base.Environment(this.gl);
		// env.create({
		// 	// All the parameters are optional
		// 	cubemapSize: 512,
		// 	irradianceMapSize: 32,
		// 	specularMapSize: 32
		// });
		// this._root.addComponent(new bg.scene.Environment(env));

		// Or you can create it with the default parameters
		this._root.addComponent(new bg.scene.Environment());
		
		this._env7 = bg.base.TextureCache.BlackCubemap(this.gl);
		bg.base.Loader.Load(this.gl,"../data/equirectangular-env.jpg")
			.then((texture) => {
				this._env1 = texture;
				let env = this._root.component("bg.scene.Environment");
				env.equirectangularTexture = texture;
				return bg.base.Loader.Load(this.gl,"../data/equirectangular-env2.jpg");
			})
			
			.then((texture) => {
				this._env2 = texture;
				return bg.base.Loader.Load(this.gl,"../data/equirectangular-env3.jpg");
			})
			
			.then((texture) => {
				this._env3 = texture;
				return bg.base.Loader.Load(this.gl,"../data/equirectangular-env4.jpg");
			})

			.then((texture) => {
				this._env4 = texture;
				return bg.base.Loader.Load(this.gl,"../data/equirectangular-env5.jpg");
			})

			.then((texture) => {
				this._env5 = texture;
				return bg.base.Loader.Load(this.gl,"../data/equirectangular-env6.jpg");
			})

			.then((texture) => {
				this._env6 = texture;
				return bg.base.Loader.Load(this.gl,"../data/equirectangular-env7.jpg");
			})

			.then((texture) => {
				this._env7 = texture;
			})

			.catch((err) => {
				console.error(err.message);
			});
	}
    
	init() {
		bg.Engine.Set(new bg.webgl1.Engine(this.gl));
		
        this.buildScene();
		
		this._renderer = bg.render.Renderer.Create(this.gl,bg.render.RenderPath.PBR);
		//this._renderer = bg.render.Renderer.Create(this.gl,bg.render.RenderPath.FORWARD);
		this._renderer.settings.shadows.quality = 4096;
		
		this._inputVisitor = new bg.scene.InputVisitor();
	}
    
    frame(delta) { this._renderer.frame(this._root, delta); }
	
	display() {
		this._renderer.display(this._root, this._camera);
	}
	
	reshape(width,height) {
		this._camera.viewport = new bg.Viewport(0,0,width,height);
		this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
		windowController.postRedisplay();
	}
	
	// Pass the input events to the scene
	mouseDown(evt) {
		this._inputVisitor.mouseDown(this._root,evt);
	}
	
	mouseDrag(evt) {
		this._inputVisitor.mouseDrag(this._root,evt);
		this.postRedisplay();
	}
	
	mouseWheel(evt) {
		this._inputVisitor.mouseWheel(this._root,evt);
		this.postRedisplay();
	}
	
	touchStart(evt) {
		this._inputVisitor.touchStart(this._root,evt);
	}
	
	touchMove(evt) {
		this._inputVisitor.touchMove(this._root,evt);
		this.postRedisplay();
	}
	
	// You may pass also the following events, but they aren't used by the camera controller
	mouseUp(evt) { this._inputVisitor.mouseUp(this._root,evt); }
	mouseMove(evt) { this._inputVisitor.mouseMove(this._root,evt); }
	mouseOut(evt) { this._inputVisitor.mouseOut(this._root,evt); }
	touchEnd(evt) { this._inputVisitor.touchEnd(this._root,evt); }

	keyUp(evt) {
		if (evt.key=="Space") {
			this.switchLights();	
		}
		else if (evt.key=="KeyE") {
			this.switchEnvironment();
		}
	}
}

function load() {
	let controller = new PBRTestWindowController();
	let mainLoop = bg.app.MainLoop.singleton;
	window.windowController = controller;

	mainLoop.updateMode = bg.app.FrameUpdate.MANUAL;
	mainLoop.canvas = document.getElementsByTagName('canvas')[0];
	mainLoop.run(controller);
}

function applyFloorMaterial() {
	let metallic = document.getElementById("metallic").value;
	let roughness = document.getElementById("roughness").value;
	let irradiance = document.getElementById("irradiance").value;
	let lightIntensity = document.getElementById("lightIntensity").value;

	windowController.floorMat.metallic = metallic;
	windowController.floorMat.roughness = roughness;
	windowController.s6Mat.metallic = metallic;
	windowController.s6Mat.roughness = roughness;
	windowController.s7Mat.metallic = metallic;
	windowController.s7Mat.roughness = roughness;
	windowController.s8Mat.metallic = metallic;
	windowController.s8Mat.roughness = roughness;
	windowController.s9Mat.metallic = metallic;
	windowController.s9Mat.roughness = roughness;

	windowController.s2Mat.metallic = metallic;
	windowController.s2Mat.roughness = roughness;


	let sceneEnvironment = bg.scene.Environment.Get();
	if (sceneEnvironment) {
		sceneEnvironment.environment.irradianceIntensity = irradiance;
	}

	windowController._directional.intensity = lightIntensity;
	windowController._point1.intensity = lightIntensity;
	windowController._point2.intensity = lightIntensity;
	windowController._spot.intensity = lightIntensity;
	windowController._spot2.intensity = lightIntensity;




	// Force shader parameter calculation
	windowController.floorMat.getShaderParameters(windowController.gl);
	windowController.s6Mat.getShaderParameters(windowController.gl);	
	windowController.s7Mat.getShaderParameters(windowController.gl);	
	windowController.s8Mat.getShaderParameters(windowController.gl);	
	windowController.s9Mat.getShaderParameters(windowController.gl);	
	windowController.s2Mat.getShaderParameters(windowController.gl);

	windowController.postRedisplay();
}

function switchEnvironment() {
	windowController.switchEnvironment();
}