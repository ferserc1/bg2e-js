class RotateComponent extends bg.scene.Component {
	frame(delta) {
		if (this.transform) {
			this.transform.matrix.rotate(0.04,0,1,0);
			this.postRedisplay();
		}
	}
}

bg.scene.registerComponent(bg.scene,RotateComponent,"bg.scene.RotateComponent");

class TextRectWindowController extends bg.app.WindowController {
	get text() {
		return window.textComponent.text;
	}

	set text(t) {
		window.textComponent.text = t;
	}

	buildScene() {
        bg.base.Loader.RegisterPlugin(new bg.base.TextureLoaderPlugin());

        this._root = new bg.scene.Node(this.gl, "Root node");
			
		let lightNode = new bg.scene.Node(this.gl,"Light");
		let l = new bg.base.Light(this.gl);
        l.type = bg.base.LightType.SPOT;
        l.spotExponent = 180;
        l.spotCutoff = 40;
		lightNode.addComponent(new bg.scene.Light(l));	
		lightNode.addComponent(new bg.scene.Transform(bg.Matrix4.Identity()
			.rotate(bg.Math.degreesToRadians(30),0,1,0)
			.rotate(bg.Math.degreesToRadians(40),-1,0,0)
			.translate(0,0,10)));
			
		this._root.addChild(lightNode);

        let quad = new bg.scene.Node(this.gl,"TextQuad");
        window.textComponent = new bg.scene.TextRect(new bg.Vector2(2,1),new bg.Vector2(2000,1000));
        window.textComponent.textProperties.size = 200;
        window.textComponent.textProperties.background = "#333333";
        window.textComponent.textProperties.align = "center";
        window.textComponent.textProperties.font = "Courier";
        window.textComponent.textProperties.bold = true;
        window.textComponent.textProperties.italic = true;
        window.textComponent.text = "Hello!";
		quad.addComponent(textComponent);
		quad.addComponent(new bg.scene.Transform());
		quad.addComponent(new bg.scene.RotateComponent);
        this._root.addChild(quad);


        let floor = new bg.scene.Node(this.gl,"floor");
        floor.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(0,-1.3,0)));
        floor.addComponent(bg.scene.PrimitiveFactory.Plane(this.gl,10,10));
        this._root.addChild(floor);

		
		this._camera = new bg.scene.Camera();
		let cameraNode = new bg.scene.Node("Camera");
		cameraNode.addComponent(this._camera);			
		cameraNode.addComponent(new bg.scene.Transform());
        let controller = new bg.manipulation.OrbitCameraController();
        controller.minY = -10;
        controller.maxY = 10;
		controller.minPitch = -90;
		cameraNode.addComponent(controller);
		this._root.addChild(cameraNode);
	}
    
	init() {
		bg.Engine.Set(new bg.webgl1.Engine(this.gl));
		
        this.buildScene();
		
		this._renderer = bg.render.Renderer.Create(this.gl,bg.render.RenderPath.FORWARD);
		
		this._inputVisitor = new bg.scene.InputVisitor();
	}
    
    frame(delta) {
		this.time += delta;
		this._renderer.frame(this._root, delta);
	}
	
	display() {
		this._renderer.display(this._root, this._camera);
	}
	
	reshape(width,height) {
		this._camera.viewport = new bg.Viewport(0,0,width,height);
		this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
	}
	
	// Pass the input events to the scene
	mouseDown(evt) {
		this._inputVisitor.mouseDown(this._root,evt);
	}
	
	mouseDrag(evt) {
		this._inputVisitor.mouseDrag(this._root,evt);
		this.postRedisplay();
	}
	
	mouseWheel(evt) {
		this._inputVisitor.mouseWheel(this._root,evt);
		this.postRedisplay();
	}
	
	touchStart(evt) {
		this._inputVisitor.touchStart(this._root,evt);
	}
	
	touchMove(evt) {
		this._inputVisitor.touchMove(this._root,evt);
		this.postRedisplay();
	}
	
	// You may pass also the following events, but they aren't used by the camera controller
	mouseUp(evt) { this._inputVisitor.mouseUp(this._root,evt); }
	mouseMove(evt) { this._inputVisitor.mouseMove(this._root,evt); }
	mouseOut(evt) { this._inputVisitor.mouseOut(this._root,evt); }
	touchEnd(evt) { this._inputVisitor.touchEnd(this._root,evt); }

	keyUp(evt) {
	}
}

function load() {
	let controller = new TextRectWindowController();
	let mainLoop = bg.app.MainLoop.singleton;
	window.windowController = controller;

	mainLoop.updateMode = bg.app.FrameUpdate.MANUAL;
	mainLoop.canvas = document.getElementsByTagName('canvas')[0];
	mainLoop.run(controller);
}
