
class DeferredBasicWindowController extends bg.app.WindowController {
	buildScene() {
		this._root = new bg.scene.Node(this.gl, "Root node");
		
		bg.base.Loader.RegisterPlugin(new bg.base.TextureLoaderPlugin());
		bg.base.Loader.RegisterPlugin(new bg.base.VWGLBLoaderPlugin());
		
		bg.base.Loader.Load(this.gl,"../data/test-shape.vwglb")
			.then((node) => {
				this._root.addChild(node);
				node.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(-0.5,0.25,0).scale(0.5,0.5,0.5)));
				
				this.postRedisplay();	// Update the view manually, because in this sample the auto update is disabled
			})
			
			.catch(function(err) {
				alert(err.message);
			});
	
		let sphereNode = new bg.scene.Node(this.gl,"Sphere");
		sphereNode.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(0.5,0.2,0.0)));
		sphereNode.addComponent(bg.scene.PrimitiveFactory.Sphere(this.gl,0.2));
		sphereNode.component("bg.scene.Drawable").getMaterial(0).diffuse.a = 0.8;
		sphereNode.component("bg.scene.Drawable").getMaterial(0).reflectionAmount = 0.4;
		this._root.addChild(sphereNode);
	
		let floorNode = new bg.scene.Node(this.gl,"Floor");
		floorNode.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(0,0,0)));
		floorNode.addComponent(bg.scene.PrimitiveFactory.Plane(this.gl,10,10));
		floorNode.component("bg.scene.Drawable").getMaterial(0).shininess = 50;
		floorNode.component("bg.scene.Drawable").getMaterial(0).reflectionAmount = 0.55;
		floorNode.component("bg.scene.Drawable").getMaterial(0).normalMapScale = new bg.Vector2(10,10);
		floorNode.component("bg.scene.Drawable").getMaterial(0).textureScale = new bg.Vector2(10,10);
		floorNode.component("bg.scene.Drawable").getMaterial(0).reflectionMaskInvert = true;
		floorNode.component("bg.scene.Drawable").getMaterial(0).shininessMaskInvert = true;
		this._root.addChild(floorNode);

		bg.base.Loader.Load(this.gl,"../data/bricks_nm.png")
			.then((tex) => {
				floorNode.component("bg.scene.Drawable").getMaterial(0).normalMap = tex;
			});

		bg.base.Loader.Load(this.gl,"../data/bricks.jpg")
			.then((tex) => {
				floorNode.component("bg.scene.Drawable").getMaterial(0).texture = tex;
			});

		bg.base.Loader.Load(this.gl,"../data/bricks_shin.jpg")
			.then((tex) => {
				floorNode.component("bg.scene.Drawable").getMaterial(0).reflectionMask = tex;
				floorNode.component("bg.scene.Drawable").getMaterial(0).shininessMask = tex;
			});


		// This cubemap is used to show something when the ray fall out the viewport
		let cm = new bg.scene.Cubemap();
		this._root.addComponent(cm);
		cm.setImageUrl(bg.scene.CubemapImage.POSITIVE_X,"../data/sample-scene/posx.jpg");
		cm.setImageUrl(bg.scene.CubemapImage.NEGATIVE_X,"../data/sample-scene/negx.jpg");
		cm.setImageUrl(bg.scene.CubemapImage.POSITIVE_Y,"../data/sample-scene/posy.jpg");
		cm.setImageUrl(bg.scene.CubemapImage.NEGATIVE_Y,"../data/sample-scene/negy.jpg");
		cm.setImageUrl(bg.scene.CubemapImage.POSITIVE_Z,"../data/sample-scene/posz.jpg");
		cm.setImageUrl(bg.scene.CubemapImage.NEGATIVE_Z,"../data/sample-scene/negz.jpg");
		cm.loadCubemap(this.gl)
			.then(() => {
				this.postRedisplay();
			});

		
		let lightNode = new bg.scene.Node(this.gl,"Light");
		let light = new bg.base.Light(this.gl);
		light.type = bg.base.LightType.DIRECTIONAL;
		light.spotCutoff = 30;
		lightNode.addComponent(new bg.scene.Light(light));
		lightNode.addComponent(new bg.scene.Transform(bg.Matrix4.Identity()
												.translate(0,0,5)
												.rotate(bg.Math.degreesToRadians(0),0,1,0)
												.rotate(bg.Math.degreesToRadians(45),-1,0,0)
												.translate(0,1.4,3)));
		this._root.addChild(lightNode);
		
		this._camera = new bg.scene.Camera();
		let cameraNode = new bg.scene.Node("Camera");
		cameraNode.addComponent(this._camera);			
		cameraNode.addComponent(new bg.scene.Transform());
		cameraNode.addComponent(new bg.manipulation.OrbitCameraController());
		let camCtrl = cameraNode.component("bg.manipulation.OrbitCameraController");
		camCtrl.minPitch = -45;
		this._root.addChild(cameraNode);
	}
    
	init() {
		bg.Engine.Set(new bg.webgl1.Engine(this.gl));
		
        this.buildScene();
		
		this._renderer = new bg.render.Renderer.Create(this.gl,bg.render.RenderPath.DEFERRED);
		this._inputVisitor = new bg.scene.InputVisitor();

		
		this._renderer.settings.ambientOcclusion.kernelSize = 16;
		this._renderer.settings.ambientOcclusion.blur = 2;
		this._renderer.settings.ambientOcclusion.sampleRadius = 0.15;
		this._renderer.settings.raytracer.quality = bg.render.RaytracerQuality.low;
		this._renderer.settings.raytracer.basicReflections = false;
		this._renderer.settings.antialiasing.enabled = true;
	}
    
    frame(delta) { this._renderer.frame(this._root, delta); }
	
	display() { this._renderer.display(this._root, this._camera); }
	
	reshape(width,height) {
		this._camera.viewport = new bg.Viewport(0,0,width,height);
		this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
	}
	
	// Pass the input events to the scene
	mouseDown(evt) {
		this._renderer.settings.ambientOcclusion.enabled = false;
		this._renderer.settings.antialiasing.enabled = false;
		this._inputVisitor.mouseDown(this._root,evt);
	}
	
	mouseDrag(evt) {
		this._inputVisitor.mouseDrag(this._root,evt);
		this.postRedisplay();
	}
	
	mouseWheel(evt) {
		this._inputVisitor.mouseWheel(this._root,evt);
		this.postRedisplay();
	}
	
	touchStart(evt) {
		this._inputVisitor.touchStart(this._root,evt);
	}
	
	touchMove(evt) {
		this._inputVisitor.touchMove(this._root,evt);
		this.postRedisplay();
	}
	
	// You may pass also the following events, but they aren't used by the camera controller
	mouseUp(evt) {
		this._inputVisitor.mouseUp(this._root,evt);
		this._renderer.settings.ambientOcclusion.enabled = true;
		this._renderer.settings.antialiasing.enabled = true;
		this.postRedisplay();
	}
	mouseMove(evt) { this._inputVisitor.mouseMove(this._root,evt); }
	mouseOut(evt) { this._inputVisitor.mouseOut(this._root,evt); }
	touchEnd(evt) { this._inputVisitor.touchEnd(this._root,evt); }
}

function load() {
	let controller = new DeferredBasicWindowController();
	let mainLoop = bg.app.MainLoop.singleton;
	window.windowController = controller;
	
	mainLoop.updateMode = bg.app.FrameUpdate.MANUAL;
	mainLoop.canvas = document.getElementsByTagName('canvas')[0];
	mainLoop.run(controller);
}
