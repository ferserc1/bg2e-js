
class TextureMergerWindowController extends bg.app.WindowController {
	
	buildScene() {
        this._root = new bg.scene.Node(this.gl, "Root node");
        
        bg.base.Loader.RegisterPlugin(new bg.base.TextureLoaderPlugin());
		
		let floor = new bg.scene.Node(this.gl, "Floor");
		floor.addComponent(bg.scene.PrimitiveFactory.Plane(this.gl,10,10));
		floor.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(0,-0.5,0)));
		this._root.addChild(floor);
				
		let lightNode = new bg.scene.Node(this.gl,"Light");
		lightNode.addComponent(new bg.scene.Light(new bg.base.Light(this.gl)));	
		lightNode.addComponent(new bg.scene.Transform(bg.Matrix4.Rotation(bg.Math.degreesToRadians(25),0,1,0)
																	.rotate(bg.Math.degreesToRadians(25),-1,0,0)));
		this._root.addChild(lightNode);
		
		this._camera = new bg.scene.Camera();
		let cameraNode = new bg.scene.Node("Camera");
		cameraNode.addComponent(this._camera);			
		cameraNode.addComponent(new bg.scene.Transform());
		cameraNode.component("bg.scene.Transform").matrix
			.rotate(bg.Math.degreesToRadians(90),-1,0,0)
			.translate(0,0,10);
        this._root.addChild(cameraNode);
        
        let t0 = {
            map:bg.base.Texture.ColorTexture(this.gl, new bg.Color(1,0,0,0),new bg.Vector2(32,32)),
            channel: 0
        };
        let t1 = {
            map:bg.base.Texture.ColorTexture(this.gl, new bg.Color(0,1,0,0),new bg.Vector2(32,32)),
            channel: 1 };
        let t2 = {
            map:bg.base.Texture.ColorTexture(this.gl, new bg.Color(0,0,0,0),new bg.Vector2(32,32)),
            channel: 2
        };
        let t3 = {
            map:bg.base.Texture.ColorTexture(this.gl, new bg.Color(0,0,0,1),new bg.Vector2(32,32)),
            channel: 3
		};
		
		let merger = new bg.tools.TextureMerger(this.gl);
		let mergedTexture = merger.mergeMaps(t0, t1, t2, t3);
		floor.drawable.getMaterial(0).texture = mergedTexture;
        
		

		bg.base.Loader.Load(this.gl,"../data/bricks2_disp.jpg")
			.then((height) => {
				t0.map = height;
				return bg.base.Loader.Load(this.gl,"../data/bricks2_diffuse.jpg");
			})

			.then((diffuse) => {
				t1.map = diffuse;
				return bg.base.Loader.Load(this.gl,"../data/bricks2_normal.jpg");
			})

			.then((normal) => {
				t2.map = normal;
				let mergedTexture = merger.mergeMaps(t0, t1, t2, t3);
				floor.drawable.getMaterial(0).texture = mergedTexture;
			})

			.catch((err) => {
				alert(err.message);
			});
	}
    
	init() {
		bg.Engine.Set(new bg.webgl1.Engine(this.gl));
		
        this.buildScene();
		
		this._renderer = bg.render.Renderer.Create(this.gl,bg.render.RenderPath.FORWARD);
	}
    
    frame(delta) { this._renderer.frame(this._root, delta); }
	
	display() { this._renderer.display(this._root, this._camera); }
	
	reshape(width,height) {
		this._camera.viewport = new bg.Viewport(0,0,width,height);
		this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
		this.postRedisplay();
	}
	
	mouseMove(evt) {
		this.postRedisplay();
	}
}

function load() {
	let controller = new TextureMergerWindowController();
    let mainLoop = bg.app.MainLoop.singleton;
    window.windowController = controller;
	
	mainLoop.updateMode = bg.app.FrameUpdate.MANUAL;
	mainLoop.canvas = document.getElementsByTagName('canvas')[0];
	mainLoop.run(controller);
}
