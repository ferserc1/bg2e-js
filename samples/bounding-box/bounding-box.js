
// This sample shows how to use the bounding box tools to query the scene elements size

class SimpleSceneSampleWindowController extends bg.app.WindowController {
	buildLight() {
		let light = new bg.base.Light(this.gl);
		light.ambient = bg.Color.Black();
		light.diffuse = new bg.Color(0.65,0.75,0.95,1);
		light.specular = new bg.Color(0.5, 0.6,1,1);
		return light;
	}
	
	buildScene() {
		this._root = new bg.scene.Node(this.gl, "Root node");
		
		let cubeNode = new bg.scene.Node(this.gl, "Cube");
		this._root.addChild(cubeNode);
		
		cubeNode.addComponent(new bg.scene.Transform(bg.Matrix4.Scale(2,2,2).rotate(bg.Math.degreesToRadians(45),0,1,0)));
		cubeNode.addComponent(new bg.scene.Drawable());
		
		let drw = cubeNode.component("bg.scene.Drawable");
		drw.addPolyList(SampleUtils.Cube(this.gl),SampleUtils.Material(this.gl));

		let trx = cubeNode.component("bg.scene.Transform");

		let bbox = new bg.tools.BoundingBox(drw,trx.matrix);
		console.log(`Center: ${bbox.center.toString()}, size: ${bbox.size.toString()}`);
		// Calcualte the camera distance to the object using the bounding box size
		let distance = bg.Math.max(bbox.size.x,bg.Math.max(bbox.size.y,bbox.size.z));
			
		let lightNode = new bg.scene.Node(this.gl,"Light");
		this._root.addChild(lightNode);
		
		this._lightComponent = new bg.scene.Light(this.buildLight());
		
		lightNode.addComponent(this._lightComponent);	
		
		lightNode.addComponent(new bg.scene.Transform(bg.Matrix4.Rotation(bg.Math.degreesToRadians(22),0,1,0)
														.rotate(bg.Math.degreesToRadians(55),-1,0,0)));
	
		// 	Camera projection strategy: is a strategy class that generate the camera projection matrix
		//	automatically when the viewport is set. You can use bg.scene.OpticalProjectionStrategy to
		//	generate a projection matrix using a frame/film size and a focal length
		this._camera = new bg.scene.Camera();
		let projection = new bg.scene.OpticalProjectionStrategy();
		projection.frameSize = 35;	// Equivalent to a full frame digital camera 
		projection.focalLength = 75;	// 75mm focal length
		this._camera.projectionStrategy = projection;
		let cameraNode = new bg.scene.Node("Camera");
		cameraNode.addComponent(this._camera);			
		cameraNode.addComponent(new bg.scene.Transform());
		cameraNode.component("bg.scene.Transform").matrix
			.translate(0.2,0,0)
			.rotate(bg.Math.degreesToRadians(22.5),-1,0,0)
			.translate(0,0,distance);
		this._root.addChild(cameraNode);
	}
    
	init() {
		bg.Engine.Set(new bg.webgl1.Engine(this.gl));
	    this.buildScene();
		this._renderer = new bg.render.Renderer(this.gl,bg.render.RenderPath.FORWARD);
		window.renderer = this._renderer;
	}
    
    frame(delta) { this._renderer.frame(this._root, delta); }
	display() { this._renderer.display(this._root, this._camera); }
	
	reshape(width,height) {
		// If the camera is configured with a projection strategy, we only need
		// to set the viewport, and the strategy will automatically update the projection 
		this._camera.viewport = new bg.Viewport(0,0,width,height);
	}
}

function load() {
	let controller = new SimpleSceneSampleWindowController();
	let mainLoop = bg.app.MainLoop.singleton;
	
	mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
	mainLoop.canvas = document.getElementsByTagName('canvas')[0];
	mainLoop.run(controller);
}
