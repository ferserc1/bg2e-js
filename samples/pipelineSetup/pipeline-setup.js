
class BasicEffect extends bg.base.Effect {
	constructor(context) { super(context); }
	
	get addColor() {
		if (!this._addColor) {
			this._addColor = bg.Color.Black();
		}
		return this._addColor;
	}
	set addColor(c) { this._addColor = c; }
	
	get inputVars() {
		return {
			vertex:'position',
			color:'color'
		}
	}
	
	get shader() {
		if (!this._shader) {
			this._shader = new bg.base.Shader(this.context);
			this._shader.addShaderSource(bg.base.ShaderType.VERTEX, `
                attribute vec4 position;
                attribute vec4 color;
                varying vec4 vColor;
                void main() {
                    gl_Position = position;
                    vColor = color;
                }`);
			this._shader.addShaderSource(bg.base.ShaderType.FRAGMENT,`
                precision mediump float;
                varying vec4 vColor;
                uniform vec4 uColorAdd;
                void main() {
                    gl_FragColor = clamp(vColor + uColorAdd,vec4(0.0),vec4(1.0));
                }`);
			this._shader.link();
			if (!this._shader.status) {
				console.log(this._shader.compileError);
				console.log(this._shader.linkError);
			}
			else {
				this._shader.initVars(['position','color'],['uColorAdd']);
			}
		}
		return this._shader
	}
	
	setupVars() {
		this.shader.setVector4('uColorAdd',this.addColor);
	}
}

class EffectSampleWindowController extends bg.app.WindowController {
    buildShape() {
        this.plist = new bg.base.PolyList(this.gl);
        
        this.plist.vertex = [ -0.9,-0.9,0, 0.9,-0.9,0, 0,0.9,0 ];
        this.plist.color = [ 1,0,0,1, 0,1,0,1, 0,0,1,1 ];
        this.plist.index = [ 0, 1, 2 ];
        
        this.plist.build();
	}
    
	init() {
		// Use WebGL V1 engine
		bg.Engine.Set(new bg.webgl1.Engine(this.gl));

        this.glowInc = 0.1;
		
        this.buildShape();
        this.effect = new BasicEffect(this.gl);
                
		let pipeline = new bg.base.Pipeline(this.gl);
		pipeline.effect = this.effect;
		
		bg.base.Pipeline.SetCurrent(pipeline);
	}
    
    frame(delta) {
        if (this.effect.addColor.r>1) {
            this.glowInc = -0.05;
        }
        else if (this.effect.addColor.r<-0.5) {
            this.glowInc = 0.05;
        }
        this.effect.addColor.add(new bg.Color(this.glowInc, this.glowInc, this.glowInc, 0));
    }
	
	display() {
		let pipeline = bg.base.Pipeline.Current();
		pipeline.clearBuffers(bg.base.ClearBuffers.COLOR | bg.base.ClearBuffers.DEPTH);
		pipeline.draw(this.plist);
	}
	
	reshape(width,height) {
		bg.base.Pipeline.Current()
			.viewport = new bg.Viewport(0,0,width,height);
	}
}

function load() {
	let controller = new EffectSampleWindowController();
	let mainLoop = bg.app.MainLoop.singleton;
	
	mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
	mainLoop.canvas = document.getElementsByTagName('canvas')[0];
	mainLoop.run(controller);
}
