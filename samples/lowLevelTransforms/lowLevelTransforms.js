
class LowLevelTransformsSampleWindowController extends bg.app.WindowController {
    buildCube() {
        this.plist = new bg.base.PolyList(this.gl);
        
		this.plist.vertex = [
			 1,-1,-1, -1,-1,-1, -1, 1,-1,  1, 1,-1,		// back face
			 1,-1, 1,  1,-1,-1,  1, 1,-1,  1, 1, 1,		// right face 
			-1,-1, 1,  1,-1, 1,  1, 1, 1, -1, 1, 1, 	// front face
			-1,-1,-1, -1,-1, 1, -1, 1, 1, -1, 1,-1,		// left face
			-1, 1, 1,  1, 1, 1,  1, 1,-1, -1, 1,-1,		// top face
			 1,-1, 1, -1,-1, 1, -1,-1,-1,  1,-1,-1		// bottom face
		];
		
		this.plist.normal = [
			 0, 0,-1,  0, 0,-1,  0, 0,-1,  0, 0,-1,		// back face
			 1, 0, 0,  1, 0, 0,  1, 0, 0,  1, 0, 0,		// right face 
			 0, 0, 1,  0, 0, 1,  0, 0, 1,  0, 0, 1, 	// front face
			-1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0,		// left face
			 0, 1, 0,  0, 1, 0,  0, 1, 0,  0, 1, 0,		// top face
			 0,-1, 0,  0,-1, 0,  0,-1, 0,  0,-1, 0		// bottom face
		];
		
		this.plist.texCoord0 = [
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1
		];
		
		this.plist.texCoord1 = [
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1,
			0,0, 1,0, 1,1, 0,1
		];
		
		this.plist.index = [
			 0, 1, 2,	 2, 3, 0,
			 4, 5, 6,	 6, 7, 4,
			 8, 9,10,	10,11, 8,
			12,13,14,	14,15,12,
			16,17,18,	18,19,16,
			20,21,22,	22,23,20
		];
		
        this.plist.build();
	}
	
	buildMaterial() {
		let material = new bg.base.Material();
		material.shininess = 20;
		
		bg.utils.Resource.Load("../data/bricks.jpg")
			.then((img) => {
				let texture = new bg.base.Texture(this.gl);
				texture.create();
				texture.bind();
				texture.minFilter = bg.base.TextureFilter.LINEAR;
				texture.magFilter = bg.base.TextureFilter.LINEAR;
				texture.setImage(img);
				material.texture = texture;
			});
			
		bg.utils.Resource.Load("../data/bricks_nm.png")
			.then((img) => {
				let texture = new bg.base.Texture(this.gl);
				texture.create();
				texture.bind();
				texture.minFilter = bg.base.TextureFilter.LINEAR;
				texture.magFilter = bg.base.TextureFilter.LINEAR;
				texture.setImage(img);
				material.normalMap = texture;
			});
			
		return material;
	}
	
	buildLight() {
		let light = new bg.base.Light(this.gl);
		light.ambient = bg.Color.Black();
		light.diffuse = new bg.Color(0.65,0.75,0.95,1);
		light.specular = new bg.Color(0.5, 0.6,1,1);
		return light;
	}
    
	init() {
		// Use WebGL V1 engine
		bg.Engine.Set(new bg.webgl1.Engine(this.gl));

		// Build a cube
        this.buildCube();

		// Build the render pipeline
		let pipeline = new bg.base.Pipeline(this.gl);

		// Set the effect strategy
		let effect = new bg.base.ForwardEffect(this.gl);
		effect.material = this.buildMaterial();
		effect.light = this.buildLight();
		pipeline.effect = effect;		
		
		// Set the pipeline as the current render pipeline
		bg.base.Pipeline.SetCurrent(pipeline);
		
		// We'll use this attribute to animate the light and the cube
		this._rotation = 0;
	}
    
    frame(delta) {
		// Animation
		this._rotation += delta * 0.001;
    }
	
	display() {
		// This is an auxiliar variable to simplify a little bit the code
		let pl = bg.base.Pipeline.Current();
		
		// Clear buffers
		pl.clearBuffers(bg.base.ClearBuffers.COLOR | bg.base.ClearBuffers.DEPTH);

		// The current projection, view and model matrixes, as long as they corresponding
		// stacks and other utilities, are placed in a bg.base.MatrixState instance.
		//
		// You can create several MatrixState instances. To specify the current MatrixState you can use
		// bg.base.MatrixState.SetCurrent() function
		//
		// If you don't specify any MatrixState, bg2 engine will do it for you
		let matrixState = bg.base.MatrixState.Current();
		
		// Place the camera using the view matrix.
		//	Transform camera position and orientation: place the camera in model space
		matrixState.viewMatrixStack
					.identity()
					.rotate(bg.Math.degreesToRadians(22.5),-1,0,0)
					.translate(0,0,5)
					.invert();	//	Invert the view matrix to convert from model to view space
				
		// Transform cube with the model matrix
		matrixState.modelMatrixStack
				.identity()
				.rotate(this._rotation * -0.1, 0,1,0);
			
		// Transform the light using the lightTransform effect attribute
		pl.effect.lightTransform = bg.Matrix4.Identity()
									.rotate(this._rotation,0,1,0)
									.rotate(bg.Math.degreesToRadians(35),-1,0,0);
		
		// Draw the cube polyList
		pl.draw(this.plist);
	}
	
	reshape(width,height) {
		// We need to specify the viewport to the current render pipeline and use
		// it's aspect ratio to create the projection matrix.
		let viewport = new bg.Viewport(0,0,width,height);
		
		// Set the viewport
		bg.base.Pipeline.Current().viewport = viewport;
			
		// Create the projection matrix
		bg.base.MatrixState.Current()
			.projectionMatrixStack.perspective(60,viewport.aspectRatio,0.1,100);
	}
}

function load() {
	let controller = new LowLevelTransformsSampleWindowController();
	let mainLoop = bg.app.MainLoop.singleton;
	
	mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
	mainLoop.canvas = document.getElementsByTagName('canvas')[0];
	mainLoop.run(controller);
}
