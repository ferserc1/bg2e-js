

class AnimationComponent extends bg.scene.Component {
	constructor(increment = 0.001,rotX=0) {
		super();
		
		this._rotX = rotX;
		this._rotY = 0;
		this._increment = increment;
	}
	
	get increment() { return this._increment; }
	set increment(i) { this._increment = i; }
	
	frame(delta) {
		this._rotY += delta * this.increment;
		if (this.transform) {
			this.transform.matrix
				.identity()
				.rotate(this._rotY,0,1,0)
				.rotate(bg.Math.degreesToRadians(this._rotX),-1,0,0);
		}
	}
}

bg.scene.registerComponent(window,AnimationComponent,"AnimationComponent");

class LoaderWindowController extends bg.app.WindowController {
	buildLight() {
		let light = new bg.base.Light(this.gl);
		light.ambient = bg.Color.Black();
		light.diffuse = new bg.Color(0.65,0.75,0.95,1);
		light.specular = new bg.Color(0.5, 0.6,1,1);
		return light;
	}
	
	buildScene() {
		this._root = new bg.scene.Node(this.gl, "Root node");
		
		bg.base.Loader.RegisterPlugin(new bg.base.TextureLoaderPlugin());
		bg.base.Loader.RegisterPlugin(new bg.base.VWGLBLoaderPlugin());
		bg.base.Loader.RegisterPlugin(new bg.base.OBJLoaderPlugin());
		bg.base.Loader.RegisterPlugin(new bg.base.Bg2matLoaderPlugin());
		
		//bg.base.Loader.Load(this.gl,"../data/test-shape.vwglb")
		bg.base.Loader.Load(this.gl,"../data/obj-plugin/absolute-coords.obj")
			.then((node) => {
				this._root.addChild(node);
		
				node.addComponent(new bg.scene.Transform());
				node.addComponent(new AnimationComponent(-0.0001));
			})			
			.catch(function(err) {
				alert(err.message);
			});

		bg.base.Loader.Load(this.gl,"../data/test.bg2mat")
			.then((materials) => {
				let planeNode = new bg.scene.Node(this.gl, "Floor");

				let planeDrawable = bg.scene.PrimitiveFactory.Plane(this.gl,10);
				window.planeDrawable = planeDrawable;
				planeNode.addComponent(planeDrawable);
				planeNode.addComponent(new bg.scene.Transform(new bg.Matrix4.Translation(0,-0.5,0)));
				this._root.addChild(planeNode);

				let i = 0;
				planeDrawable.forEach(function(plist,material) {
					let mat = i<materials.length ? materials[i]:materials[0];
					material.assign(mat);
					++i;
				});
			});
		
		let lightNode = new bg.scene.Node(this.gl,"Light");
		this._root.addChild(lightNode);
		let lightComponent = new bg.scene.Light(this.buildLight());
		lightNode.addComponent(lightComponent);	
		lightNode.addComponent(new bg.scene.Transform());
		lightNode.addComponent(new AnimationComponent(0.001,35));
		
		this._camera = new bg.scene.Camera();
		let cameraNode = new bg.scene.Node("Camera");
		cameraNode.addComponent(this._camera);			
		cameraNode.addComponent(new bg.scene.Transform());
		cameraNode.component("bg.scene.Transform").matrix
			.translate(0.2,0,0)
			.rotate(bg.Math.degreesToRadians(22.5),-1,0,0)
			.translate(0,0,4);
		this._root.addChild(cameraNode);
	}
    
	init() {
		bg.Engine.Set(new bg.webgl1.Engine(this.gl));
		
        this.buildScene();
		
		this._renderer = bg.render.Renderer.Create(this.gl,bg.render.RenderPath.FORWARD);
		this._renderer.clearColor = bg.Color.Gray();
	}
    
    frame(delta) { this._renderer.frame(this._root, delta); }
	
	display() { this._renderer.display(this._root, this._camera); }
	
	reshape(width,height) {
		this._camera.viewport = new bg.Viewport(0,0,width,height);
		this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
	}
}

function load() {
	let controller = new LoaderWindowController();
	let mainLoop = bg.app.MainLoop.singleton;
	
	mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
	mainLoop.canvas = document.getElementsByTagName('canvas')[0];
	mainLoop.run(controller);
}
