
class PrimitivesWindowController extends bg.app.WindowController {
	
	buildScene() {
		this._root = new bg.scene.Node(this.gl, "Root node");
		
		let sphere = new bg.scene.Node(this.gl, "Sphere");
		sphere.addComponent(bg.scene.PrimitiveFactory.Sphere(this.gl,0.5,30,30));
		sphere.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(-1,0,0)));
		this._root.addChild(sphere);
		
		let cube = new bg.scene.Node(this.gl, "Cube");
		cube.addComponent(bg.scene.PrimitiveFactory.Cube(this.gl,1,1,1));
		cube.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(1,0,0)));
		this._root.addChild(cube);
		
		let floor = new bg.scene.Node(this.gl, "Floor");
		floor.addComponent(bg.scene.PrimitiveFactory.Plane(this.gl,10,10));
		floor.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(0,-0.5,0)));
		this._root.addChild(floor);
				
		let lightNode = new bg.scene.Node(this.gl,"Light");
		lightNode.addComponent(new bg.scene.Light(new bg.base.Light(this.gl)));	
		lightNode.addComponent(new bg.scene.Transform(bg.Matrix4.Rotation(bg.Math.degreesToRadians(25),0,1,0)
																	.rotate(bg.Math.degreesToRadians(25),-1,0,0)));
		this._root.addChild(lightNode);
		
		this._camera = new bg.scene.Camera();
		let cameraNode = new bg.scene.Node("Camera");
		cameraNode.addComponent(this._camera);			
		cameraNode.addComponent(new bg.scene.Transform());
		cameraNode.component("bg.scene.Transform").matrix
			.translate(0.2,0,0)
			.rotate(bg.Math.degreesToRadians(-15),0,1,0)
			.rotate(bg.Math.degreesToRadians(22.5),-1,0,0)
			.translate(0,0,4);
		this._root.addChild(cameraNode);
	}
    
	init() {
		bg.Engine.Set(new bg.webgl1.Engine(this.gl));
		
        this.buildScene();
		
		this._renderer = bg.render.Renderer.Create(this.gl,bg.render.RenderPath.FORWARD);
	}
    
    frame(delta) { this._renderer.frame(this._root, delta); }
	
	display() { this._renderer.display(this._root, this._camera); }
	
	reshape(width,height) {
		this._camera.viewport = new bg.Viewport(0,0,width,height);
		this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
		this.postRedisplay();
	}
	
	mouseMove(evt) {
		this.postRedisplay();
	}
}

function load() {
	let controller = new PrimitivesWindowController();
	let mainLoop = bg.app.MainLoop.singleton;
	
	mainLoop.updateMode = bg.app.FrameUpdate.MANUAL;
	mainLoop.canvas = document.getElementsByTagName('canvas')[0];
	mainLoop.run(controller);
}
