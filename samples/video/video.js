
class VideoSampleWindowController extends bg.app.WindowController {
    buildShape() {
        this.plist = new bg.base.PolyList(this.gl);
        
        this.plist.vertex = [ -0.9,-0.9,0, 0.9,-0.9,0, 0.9,0.9,0, -0.9,0.9,0, ];
        this.plist.texCoord0 = [ 0,0, 1,0, 1,1, 0,1 ];
        this.plist.index = [ 0, 1, 2, 2, 3, 0 ];
        
        this.plist.build();
    }
    
    buildShader() {
		let vshader = `
                attribute vec4 position;
                attribute vec2 texCoord;
                varying vec2 vTexCoord;
                void main() {
                    gl_Position = position;
                    vTexCoord = texCoord;
                }
            `;
		let fshader = `
                precision mediump float;
                varying vec2 vTexCoord;
                uniform sampler2D inTexture;
                void main() {
                    gl_FragColor = texture2D(inTexture,vTexCoord);
                }
            `;
		
        this.shader = new bg.base.Shader(this.gl);
        this.shader.addShaderSource(bg.base.ShaderType.VERTEX, vshader);

        this.shader.addShaderSource(bg.base.ShaderType.FRAGMENT, fshader);

        status = this.shader.link();
        if (!this.shader.status) {
            console.log(this.shader.compileError);
            console.log(this.shader.linkError);
        }
        
        this.shader.initVars(["position","texCoord"],["inTexture"]);
    }
    
	init() {
		// Use WebGL V1 engine
		bg.Engine.Set(new bg.webgl1.Engine(this.gl));
        
        this.buildShape();
        this.buildShader();
                
		this.pipeline = new bg.base.Pipeline(this.gl);
		bg.base.Pipeline.SetCurrent(this.pipeline);

        bg.utils.Resource.Load("../data/bricks.jpg")
            .then((img) => {
                let texture = new bg.base.Texture(this.gl);
				texture.create();
				texture.bind();
				texture.minFilter = bg.base.TextureFilter.LINEAR;
				texture.magFilter = bg.base.TextureFilter.LINEAR;
				texture.setImage(img);
				this.tex = texture;
            });
	}
    
    frame(delta) {
    }
	
	display() {
		this.pipeline.clearBuffers(bg.base.ClearBuffers.COLOR | bg.base.ClearBuffers.DEPTH);
        
        this.shader.setActive();
        this.shader.setInputBuffer("position",this.plist.vertexBuffer,3);
        this.shader.setInputBuffer("texCoord",this.plist.texCoord0Buffer,2);
        this.shader.setTexture("inTexture",this.tex || bg.base.TextureCache.WhiteTexture(this.gl),bg.base.TextureUnit.TEXTURE_0);
        
        this.plist.draw();
        
        this.shader.disableInputBuffer("position");
        this.shader.disableInputBuffer("texCoord");
        this.shader.clearActive();
	}
	
	reshape(width,height) {
		this.pipeline.viewport = new bg.Viewport(0,0,width,height);
	}
    
    mouseMove(evt) { this.postRedisplay(); }
}

function load() {
	let controller = new VideoSampleWindowController();
	let mainLoop = bg.app.MainLoop.singleton;
	
	mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
	mainLoop.canvas = document.getElementsByTagName('canvas')[0];
	mainLoop.run(controller);
}