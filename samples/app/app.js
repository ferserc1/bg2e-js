
let gl = null;

class MyWindowController extends bg.app.WindowController {
	constructor() {
		super();
		this.clearColor = [0,0,0,1];
		this.increment = [0.001,0.0009,0.013];
	}
	
	log(msg) {
		document.getElementById('eventLog').innerHTML = msg;
	}
	
	init() {
		gl = this.gl;		
		gl.clearColor(this.clearColor.r,this.clearColor.g,this.clearColor.b,this.clearColor.a);
		gl.enable(gl.DEPTH_TEST);
	}

	frame(delta) {
		let r = this.clearColor[0] + this.increment[0];
		let g = this.clearColor[1] + this.increment[1];
		let b = this.clearColor[2] + this.increment[2];
		if (r>1.0 || r<0.0) {
			this.increment[0] *= -1;
		}
		if (g>1.0 || g<0.0) {
			this.increment[1] *= -1;
		}
		if (b>1.0 || b<0.0) {
			this.increment[2] *= -1;
		}

		this.clearColor[0] = r;
		this.clearColor[1] = g;
		this.clearColor[2] = b;
		gl.clearColor(this.clearColor.r,this.clearColor.g,this.clearcolor.b,this.clearColor.a);
	}
	
	display() {
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	}
	
	reshape(width,height) {
		gl.viewport(0,0,width,height);
	}
	
	keyDown(evt) {
		this.log(`KeyDown: ${evt.key}`);
	}
	
	keyUp(evt) {
		this.log(`KeyUp: ${evt.key}`);
	}
	
	mouseUp(evt) {
		this.log(`MouseUp button: ${evt.button}, pos:${evt.x},${evt.y}`);
	}
	
	mouseDown(evt) {
		this.log(`MouseDown button: ${evt.button}, pos:${evt.x},${evt.y}`);
	}
	
	mouseMove(evt) {
		this.log(`MouseMove pos:${evt.x},${evt.y}`);
	}
	
	mouseOut(evt) {
		this.log(`MouseOut pos:${evt.x},${evt.y}`);
	}
	
	mouseDrag(evt) {
		this.log(`MouseDrag pos:${evt.x},${evt.y}`);
	}
	
	mouseWheel(evt) {
		this.log(`MouseWheel delta:${evt.delta} pos:${evt.x},${evt.y}`);
	}
	
	touchStart(evt) {
        console.log(evt);
        let touches = "";
        evt.touches.forEach((touch)=> {
            touches += `[${touch.identifier}: ${touch.x}, ${touch.y}] `;
        })
		this.log(`TouchStart ${touches}`);
	}
	
	touchMove(evt) {
		let touches = "";
        evt.touches.forEach((touch)=> {
            touches += `[${touch.identifier}: ${touch.x}, ${touch.y}] `;
        })
		this.log(`TouchMove ${touches}`);
	}
	
	touchEnd(evt) {
		let touches = "";
        evt.touches.forEach((touch)=> {
            touches += `[${touch.identifier}: ${touch.x}, ${touch.y}]`;
        })
		this.log(`TouchEnd ${touches}`);
	}
}

function load() {
	let controller = new MyWindowController();
	let mainLoop = bg.app.MainLoop.singleton;
	
	mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
	mainLoop.canvas = document.getElementsByTagName('canvas')[0];
	mainLoop.run(controller);
}