
class OptimizationTestWindowController extends bg.app.WindowController {

    createLightAndCamera() {
        let l = new bg.scene.Node(this.gl,"Light");
        l.addComponent(new bg.scene.Light(new bg.base.Light()));
        l.addComponent(new bg.scene.Transform(bg.Matrix4.Identity()
            .rotate(bg.Math.degreesToRadians(65),0,1,0)
            .rotate(bg.Math.degreesToRadians(22),-1,0,0)
        ));
        this._root.addChild(l);

        let c = new bg.scene.Node(this.gl,"Camera");
        this._camera = new bg.scene.Camera();
        let projection = new bg.scene.OpticalProjectionStrategy();
        projection.far = 1000;
        this._camera.projectionStrategy = projection;
        c.addComponent(this._camera);
        c.addComponent(new bg.scene.Transform());
        let ctrl = new bg.manipulation.OrbitCameraController();
        ctrl.minPitch = -90;
        ctrl.minX = -20;
        ctrl.maxX = 20;
        ctrl.minY = -10;
        ctrl.maxY = 10;
        ctrl.minX = -20;
        ctrl.maxX = 20;
        ctrl.distance = 90;
        ctrl.maxDistance = 300;

        window.ctrl = ctrl;
        
        c.addComponent(ctrl);
        this._root.addChild(c);
    }

    createSkybox() {
        let skyboxNode = new bg.scene.Node(this.gl,"Skybox");
		let skybox = new bg.scene.Skybox();
		skyboxNode.addComponent(skybox);
		skybox.setImageUrl(bg.scene.CubemapImage.POSITIVE_X,"../data/posx.jpg");
		skybox.setImageUrl(bg.scene.CubemapImage.NEGATIVE_X,"../data/negx.jpg");
		skybox.setImageUrl(bg.scene.CubemapImage.POSITIVE_Y,"../data/posy.jpg");
		skybox.setImageUrl(bg.scene.CubemapImage.NEGATIVE_Y,"../data/negy.jpg");
		skybox.setImageUrl(bg.scene.CubemapImage.POSITIVE_Z,"../data/posz.jpg");
		skybox.setImageUrl(bg.scene.CubemapImage.NEGATIVE_Z,"../data/negz.jpg");
		skybox.loadSkybox();
		this._root.addChild(skyboxNode);
    }

    createComplexScene() {
        let getNode = (parent,name,trx,drw = null) => {
            let n = new bg.scene.Node(this.gl, name);
            n.addComponent(new bg.scene.Transform(trx));
            if (drw){
                n.addComponent(drw);
                drw.getMaterial(0).diffuse = new bg.Color(bg.Math.random(),bg.Math.random(),bg.Math.random(),1);
            }
            parent && parent.addChild(n);
            return n;
        };

        let NUM_ELEMS = 20;
        let sep = 2;
        let size = sep * NUM_ELEMS;
        let spheres = getNode(this._root,"Spheres", bg.Matrix4.Identity());
        let sphereDrw = bg.scene.PrimitiveFactory.Sphere(this.gl,0.55,20,20);
        for (let i = 0; i<NUM_ELEMS; ++i) {
            for (let j = 0; j<NUM_ELEMS; ++j) {
                let drw = sphereDrw.instance();
                let x = (i * sep) - (size/2);
                let y = (j * sep) - (size/2);
                let trx = bg.Matrix4.Identity().translate(x, 0, y);
                getNode(spheres,`sphere-${i}-${j}`,trx,drw);
            }
        }
        let cubes = getNode(this._root,"Cubes", bg.Matrix4.Identity().translate(0,-sep,0));
        let cubeDrw = bg.scene.PrimitiveFactory.Cube(this.gl,1);
        for (let i = 0; i<NUM_ELEMS; ++i) {
            for (let j = 0; j<NUM_ELEMS; ++j) {
                let drw = cubeDrw.instance();
                let x = (i * sep) - (size/2);
                let y = (j * sep) - (size/2);
                let trx = bg.Matrix4.Identity().translate(x, 0, y);
                getNode(cubes,`cube-${i}-${j}`,trx,drw);
            }
        }
        //let models = getNode(this._root,"Models", bg.Matrix4.Identity().translate(0,sep,0));


        this.createLightAndCamera();

        this.createSkybox();
    }

    loadTestScene() {
        bg.base.Loader.Load(this.gl,"../data/sample-scene/scene.vitscnj")
			.then((result) => {
				this._root = result.sceneRoot;
				let cameraNode = result.cameraNode;
				this._camera = cameraNode.camera;

				// Add a camera handler component
				let ctrl = new bg.manipulation.OrbitCameraController();
				//cameraNode.addComponent(ctrl);
				cameraNode.addComponent(new bg.scene.Transform());
				ctrl.minPitch = -45;

				// Post reshape (to update the camera viewport) and redisplay
				this.postReshape();
				this.updateView();
			});
    }

    buildScene() {
        this._root = new bg.scene.Node(this.gl, "Scene Root");

        bg.base.Loader.RegisterPlugin(new bg.base.TextureLoaderPlugin());
        bg.base.Loader.RegisterPlugin(new bg.base.VWGLBLoaderPlugin());
        bg.base.Loader.RegisterPlugin(new bg.base.SceneLoaderPlugin());

        this.createComplexScene();
    }

    init() {
        this._fpsCounter = document.getElementById("fpsCounter");

        bg.Engine.Set(new bg.webgl1.Engine(this.gl));

        this.buildScene();

        this._renderer = new bg.render.Renderer.Create(this.gl, bg.render.RenderPath.DEFERRED);
        //this._renderer = new bg.render.Renderer.Create(this.gl, bg.render.RenderPath.FORWARD);
        this._inputVisitor = new bg.scene.InputVisitor();

        this._renderer.settings.ambientOcclusion.kernelSize = 16;
		this._renderer.settings.ambientOcclusion.blur = 2;
		this._renderer.settings.ambientOcclusion.sampleRadius = 0.15;
		this._renderer.settings.raytracer.quality = bg.render.RaytracerQuality.extreme;
		this._renderer.settings.raytracer.basicReflections = false;
		this._renderer.settings.antialiasing.enabled = false;
    }

    frame(delta) {
        this._totalTime = this._totalTime || 0;
        this._fps = this._fps || 0;
        this._totalTime += delta;
        this._fps++;
        this._renderer.frame(this._root, delta);
        if (this._totalTime>=1000) {
            this._fpsCounter.innerText = this._fps;
            this._totalTime = 0;
            this._fps = 0;
        }
    }

    display() {
        if (this._camera) {
            this._renderer.display(this._root, this._camera);
        }
    }

    reshape(width,height) {
        if (this._camera) {
            this._camera.viewport = new bg.Viewport(0,0,width,height);
            //this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
        }
    }

    mouseDown(evt) {
        this._inputVisitor.mouseDown(this._root, evt);
    }

    mouseDrag(evt) {
        this._inputVisitor.mouseDrag(this._root, evt);
    }

    mouseWheel(evt) {
        this._inputVisitor.mouseWheel(this._root, evt);
    }

    mouseUp(evt) {
        this._inputVisitor.mouseUp(this._root,evt);
    }
}

function load() {
    let controller = new OptimizationTestWindowController();
    let mainLoop = bg.app.MainLoop.singleton;
    
    mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
    mainLoop.canvas = document.getElementsByTagName('canvas')[0];
    mainLoop.run(controller);
}