
class PrintSceneVisitor extends bg.scene.NodeVisitor {
	constructor(domRoot) {
		super();
		this._domRoot = domRoot;
		this._parent = domRoot;
		this._currentNode = domRoot;
		this._tabs = 0;
	}

	// Called when the visitor is about to visit a node, and before
	// visit its children	
	visit(node) {
		let li = document.createElement('li');
		li.innerHTML = `<p>${node.name}</p>`;
		this._currentNode.appendChild(li);
		this._currentNode = document.createElement('ul');
		li.appendChild(this._currentNode);
	}
	
	// Between visit() and didVisit(), the visitor is applied recursively to the
	// node children
	
	// Called after visit the node children
	didVisit(node) {
		this._currentNode = this._currentNode.parentNode;
		if (this._currentNode.tagName.toLowerCase()=='li') {
			this._currentNode = this._currentNode.parentNode;
		}
	}
}

class Scene1SampleWindowController extends bg.app.WindowController {
    buildScene() {
		// Second parameter (node name) is optional
		let root = new bg.scene.Node(this.gl,"Root node");
		
		let n1 = new bg.scene.Node(this.gl,"Parent 1");
		
		let n2 = new bg.scene.Node(this.gl,"Parent 2");
		
		let l1 = new bg.scene.Node(this.gl,"Leaf 1");
		let l2 = new bg.scene.Node(this.gl,"Leaf 2");
		let l3 = new bg.scene.Node(this.gl,"Leaf 3");
		let l4 = new bg.scene.Node(this.gl,"Leaf 4");
		
		// Build the hierarchy with addChild function.
		// NEVER use the node.children property to add a children,
		// under ANY circunstance.
		root.addChild(n1);
		root.addChild(n2);
		
		n1.addChild(l1);
		n1.addChild(l2);
		
		n2.addChild(l3);
		n2.addChild(l4);
		
		return root;
	}
	
	traverseScene(sceneRoot) {
		let outNode = document.getElementById("sceneStructure");
		let visitor = new PrintSceneVisitor(outNode);
		
		let title = document.createElement("h2");
		title.innerHTML = "Printing scene from the root node";
		outNode.appendChild(title);
		sceneRoot.accept(visitor);
		
		let l3 = sceneRoot
					.children[1]	// n2 node (see buildScene)
					.children[0];	// l3 node
		title = document.createElement("h2");
		title.innerHTML = "Printing scene from a leaf node";
		outNode.appendChild(title);
		l3.acceptReverse(visitor);
	}
	
	init() {
		bg.Engine.Set(new bg.webgl1.Engine(this.gl));
		bg.base.Pipeline.SetCurrent(new bg.base.Pipeline(this.gl));
		
		let root = this.buildScene();
		this.traverseScene(root);
	}
    
	
	display() {
		bg.base.Pipeline.Current().clearBuffers(bg.base.ClearBuffers.COLOR_DEPTH);
	}
	
	reshape(width,height) {
		bg.base.Pipeline.Current().viewport = new bg.Viewport(0,0,width,height);
	}
}

function load() {
	let controller = new Scene1SampleWindowController();
	let mainLoop = bg.app.MainLoop.singleton;
	
	mainLoop.updateMode = bg.app.FrameUpdate.MANUAL;
	mainLoop.canvas = document.getElementsByTagName('canvas')[0];
	mainLoop.run(controller);
}
