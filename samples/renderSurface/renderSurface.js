
class RenderSurfaceSampleWindowController extends bg.app.WindowController {
	
	// Scene utils: create cube, material, light and manage view, model and projection matrixes
	get scene() {
		if (!this._scene) {
			this._scene = new sample.Scene(this.gl);
		}
		return this._scene;
	}
	
	buildOffscreenPipeline() {
		let offscreenPipeline = new bg.base.Pipeline(this.gl);
		
		// Setup the pipeline as if it were a onscreen pipeline
		//offscreenPipeline.effect = new bg.base.ForwardEffect(this.gl);
		offscreenPipeline.effect = new bg.base.ForwardEffect(this.gl);
		offscreenPipeline.effect.material = this.scene.material;
		offscreenPipeline.effect.light = this.scene.light;		

		// Texture surface: this allows to render the pipeline into a texture
		let renderSurface = new bg.base.TextureSurface(this.gl);
		// Standard color attachments: RGBA texture and a depth buffer. If we don't
		// specify any configuration of color attachments, the TextureSurface will
		// create one. The DEPTH component must be always specified as the last attachment
		let colorAttachments = [
			{ type:bg.base.RenderSurfaceType.RGBA, format:bg.base.RenderSurfaceFormat.UNSIGNED_BYTE },
			{ type:bg.base.RenderSurfaceType.DEPTH, format:bg.base.RenderSurfaceFormat.RENDERBUFFER }
		]
		// Create the offscreen surface and set it to the pipeline
		renderSurface.create(colorAttachments);
		offscreenPipeline.renderSurface = renderSurface;
		
		return offscreenPipeline;
	}
	
	init() {
		bg.Engine.Set(new bg.webgl1.Engine(this.gl));
		
		// Create an offscreen pipeline
		this._offscreenPipeline = this.buildOffscreenPipeline();
		
		// Setup the onscreen pipeline. In this case, well use the pipeline
		// to render a texture with the function drawTexture. The drawTexture
		// function doesn't need any setup
		this._pipeline = new bg.base.Pipeline(this.gl);
		bg.base.Pipeline.SetCurrent(this._pipeline);
	}
	
	frame(delta) {
		this._scene.frame(delta);
	}
    	
	display() {
		// Set active the offscreen pipeline and render the scene
		bg.base.Pipeline.SetCurrent(this._offscreenPipeline);
		this._offscreenPipeline.clearBuffers(bg.base.ClearBuffers.COLOR | bg.base.ClearBuffers.DEPTH);
		this._scene.setupMatrixState(this._offscreenPipeline);
		this._offscreenPipeline.draw(this._scene.cube);
		
		// Get the RGBA texture and draw it with the onscreen pipeline.
		// The TextureSurface.getTexture(colorAttachmentIndex) returns the 
		// texture corresponding to the color attachment index specified
		// as parameter. Note that the RENDERBUFFER attachment is not a texture 
		let texture = this._offscreenPipeline.renderSurface.getTexture(0);
		bg.base.Pipeline.SetCurrent(this._pipeline);
		this._pipeline.drawTexture(texture);
	}
	
	reshape(width,height) {
		let vp = new bg.Viewport(0,0,width,height);
		this._pipeline.viewport = vp;
		this._offscreenPipeline.viewport = vp;
		this._scene.setupProjection(vp);
	}
}

function load() {
	let controller = new RenderSurfaceSampleWindowController();
	let mainLoop = bg.app.MainLoop.singleton;
	
	mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
	mainLoop.canvas = document.getElementsByTagName('canvas')[0];
	mainLoop.run(controller);
}
