let sample = {};

(function() {
	class Scene extends bg.app.ContextObject {
		constructor(context) {
			super(context);
			
			this._cube = null;
			this._material = null;
			this._light = null;
			this._rotation = 0;
		}
		
		get cube() {
			if (!this._cube) {
				this._cube = new bg.base.PolyList(this.context);
				
				this._cube.vertex = [
					1,-1,-1, -1,-1,-1, -1, 1,-1,  1, 1,-1,		// back face
					1,-1, 1,  1,-1,-1,  1, 1,-1,  1, 1, 1,		// right face 
					-1,-1, 1,  1,-1, 1,  1, 1, 1, -1, 1, 1, 	// front face
					-1,-1,-1, -1,-1, 1, -1, 1, 1, -1, 1,-1,		// left face
					-1, 1, 1,  1, 1, 1,  1, 1,-1, -1, 1,-1,		// top face
					1,-1, 1, -1,-1, 1, -1,-1,-1,  1,-1,-1		// bottom face
				];
				
				this._cube.normal = [
					0, 0,-1,  0, 0,-1,  0, 0,-1,  0, 0,-1,		// back face
					1, 0, 0,  1, 0, 0,  1, 0, 0,  1, 0, 0,		// right face 
					0, 0, 1,  0, 0, 1,  0, 0, 1,  0, 0, 1, 	// front face
					-1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0,		// left face
					0, 1, 0,  0, 1, 0,  0, 1, 0,  0, 1, 0,		// top face
					0,-1, 0,  0,-1, 0,  0,-1, 0,  0,-1, 0		// bottom face
				];
				
				this._cube.texCoord0 = [
					0,0, 1,0, 1,1, 0,1,
					0,0, 1,0, 1,1, 0,1,
					0,0, 1,0, 1,1, 0,1,
					0,0, 1,0, 1,1, 0,1,
					0,0, 1,0, 1,1, 0,1,
					0,0, 1,0, 1,1, 0,1
				];
				
				this._cube.texCoord1 = [
					0,0, 1,0, 1,1, 0,1,
					0,0, 1,0, 1,1, 0,1,
					0,0, 1,0, 1,1, 0,1,
					0,0, 1,0, 1,1, 0,1,
					0,0, 1,0, 1,1, 0,1,
					0,0, 1,0, 1,1, 0,1
				];
				
				this._cube.index = [
					0, 1, 2,	 2, 3, 0,
					4, 5, 6,	 6, 7, 4,
					8, 9,10,	10,11, 8,
					12,13,14,	14,15,12,
					16,17,18,	18,19,16,
					20,21,22,	22,23,20
				];
				
				this._cube.build();
			}
			return this._cube;
		}
		
		get material() {
			if (!this._material) {
				let material = new bg.base.Material();
				material.shininess = 20;
				
				bg.utils.Resource.Load("../data/bricks.jpg")
					.then((img) => {
						let texture = new bg.base.Texture(this.context);
						texture.create();
						texture.bind();
						texture.minFilter = bg.base.TextureFilter.LINEAR;
						texture.magFilter = bg.base.TextureFilter.LINEAR;
						texture.setImage(img);
						material.texture = texture;
					});
					
				bg.utils.Resource.Load("../data/bricks_nm.png")
					.then((img) => {
						let texture = new bg.base.Texture(this.context);
						texture.create();
						texture.bind();
						texture.minFilter = bg.base.TextureFilter.LINEAR;
						texture.magFilter = bg.base.TextureFilter.LINEAR;
						texture.setImage(img);
						material.normalMap = texture;
					});
				this._material = material;
			}
			return this._material;
		}
		
		get light() {
			if (!this._light) {
				this._light = new bg.base.Light(this.context);
				this._light.ambient = bg.Color.Black();
				this._light.diffuse = new bg.Color(0.65,0.75,0.95,1);
				this._light.specular = new bg.Color(0.5, 0.6,1,1);
			}
			return this._light;
		}
		
		frame(delta) {
			this._rotation += delta * 0.001;
		}
		
		setupMatrixState(pipeline) {
			let matrixState = bg.base.MatrixState.Current();
			matrixState.viewMatrixStack
						.identity()
						.rotate(bg.Math.degreesToRadians(22.5),-1,0,0)
						.translate(0,0,5)
						.invert();

			matrixState.modelMatrixStack
					.identity()
					.rotate(this._rotation * -0.1, 0,1,0);
				
			pipeline.effect.lightTransform = bg.Matrix4.Identity()
										.rotate(this._rotation,0,1,0)
										.rotate(bg.Math.degreesToRadians(35),-1,0,0);
		}
		
		setupProjection(viewport) {
			bg.base.MatrixState.Current()
					.projectionMatrixStack.perspective(60,viewport.aspectRatio,0.1,100);
		}
	}
	
	sample.Scene = Scene;
})()