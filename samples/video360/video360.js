
class Video360WindowController extends bg.app.WindowController {
	constructor(data) {
		super();
		this._dataPath = data;
	}

	buildScene() {
		this._root = new bg.scene.Node(this.gl, "Root node");
		
		bg.base.Loader.RegisterPlugin(new bg.base.TextureLoaderPlugin());
		bg.base.Loader.RegisterPlugin(new bg.base.VWGLBLoaderPlugin());
		
		var re = /([a-z0-9\-_]+=[a-z0-9\.\-_]+)/ig;
		let result = null;
		let img = "";
		while ((result = re.exec(location.search))) {
			let keyValue = /([a-z0-9_\-\.]+)=([a-z0-9_\-\.]+)/i.exec(result[1]);
			if (keyValue && keyValue[1]=="photo") {
				img = keyValue[2];
				break;
			}
		}
		if (!img) return;
		bg.base.Loader.Load(this.gl,this._dataPath + img)
			.then((img) => {
				let sphere = bg.scene.PrimitiveFactory.Sphere(this.gl,10,50);
				let sphereNode = new bg.scene.Node(this.gl);
				sphereNode.addComponent(sphere);
				sphere.getMaterial(0).texture = img;
				sphere.getMaterial(0).lightEmission = 1.0;
				sphere.getMaterial(0).lightEmissionMaskInvert = true;
				sphere.getMaterial(0).cullFace = false;
				this._root.addChild(sphereNode);
				this.postRedisplay();
			});
		
		//let lightNode = new bg.scene.Node(this.gl,"Light");
		//lightNode.addComponent(new bg.scene.Light(new bg.base.Light(this.gl)));	
		//lightNode.addComponent(new bg.scene.Transform(bg.Matrix4.Identity()
		//										.rotate(bg.Math.degreesToRadians(30),0,1,0)
		//										.rotate(bg.Math.degreesToRadians(65),-1,0,0)));
		//this._root.addChild(lightNode);
		
		this._camera = new bg.scene.Camera();
		let cameraNode = new bg.scene.Node("Camera");
		cameraNode.addComponent(this._camera);			
		cameraNode.addComponent(new bg.scene.Transform());
		let oc = new bg.manipulation.OrbitCameraController();
		cameraNode.addComponent(oc);
		oc.maxPitch = 90;
		oc.minPitch = -90;
		oc.maxDistance = 0;
		oc.minDistace = 0;
		this._root.addChild(cameraNode);
	}
    
	init() {
		bg.Engine.Set(new bg.webgl1.Engine(this.gl));
		
        this.buildScene();
		
		this._renderer = bg.render.Renderer.Create(this.gl,bg.render.RenderPath.FORWARD);
		
		this._inputVisitor = new bg.scene.InputVisitor();
	}
    
    frame(delta) { this._renderer.frame(this._root, delta); }
	
	display() { this._renderer.display(this._root, this._camera); }
	
	reshape(width,height) {
		this._camera.viewport = new bg.Viewport(0,0,width,height);
		this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
	}
	
	// Pass the input events to the scene
	mouseDown(evt) {
		this._inputVisitor.mouseDown(this._root,evt);
	}
	
	mouseDrag(evt) {
		this._inputVisitor.mouseDrag(this._root,evt);
		this.postRedisplay();
	}
	
	mouseWheel(evt) {
		this._inputVisitor.mouseWheel(this._root,evt);
		this.postRedisplay();
	}
	
	touchStart(evt) {
		this._inputVisitor.touchStart(this._root,evt);
	}
	
	touchMove(evt) {
		this._inputVisitor.touchMove(this._root,evt);
		this.postRedisplay();
	}
	
	// You may pass also the following events, but they aren't used by the camera controller
	mouseUp(evt) { this._inputVisitor.mouseUp(this._root,evt); }
	mouseMove(evt) { this._inputVisitor.mouseMove(this._root,evt); }
	mouseOut(evt) { this._inputVisitor.mouseOut(this._root,evt); }
	touchEnd(evt) { this._inputVisitor.touchEnd(this._root,evt); }
}

function load(dataPath) {
	let controller = new Video360WindowController(dataPath);
	let mainLoop = bg.app.MainLoop.singleton;
	
	mainLoop.updateMode = bg.app.FrameUpdate.MANUAL;
	mainLoop.canvas = document.getElementsByTagName('canvas')[0];
	mainLoop.run(controller);
}
