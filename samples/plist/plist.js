
class PlistSampleWindowController extends bg.app.WindowController {
    buildShape() {
        this.plist = new bg.base.PolyList(this.gl);
        
        this.plist.vertex = [ -0.9,-0.9,0, 0.9,-0.9,0, 0,0.9,0 ];
        this.plist.color = [ 1,0,0,1, 0,1,0,1, 0,0,1,1 ];
        this.plist.index = [ 0, 1, 2 ];
        
        this.plist.build();
    }
    
    buildShader() {
		let vshader = `
                attribute vec4 position;
                attribute vec4 color;
                varying vec4 vColor;
                void main() {
                    gl_Position = position;
                    vColor = color;
                }
            `;
		let fshader = `
                precision mediump float;
                varying vec4 vColor;
                uniform vec4 uColorAdd;
                void main() {
                    gl_FragColor = clamp(vColor + uColorAdd,vec4(0.0),vec4(1.0));
                }
            `;
		
        this.shader = new bg.base.Shader(this.gl);
        this.shader.addShaderSource(bg.base.ShaderType.VERTEX, vshader);

        this.shader.addShaderSource(bg.base.ShaderType.FRAGMENT, fshader);

        status = this.shader.link();
        if (!this.shader.status) {
            console.log(this.shader.compileError);
            console.log(this.shader.linkError);
        }
        
        this.shader.initVars(["position","color"],["uColorAdd"]);
    }
    
	init() {
		// Use WebGL V1 engine
		bg.Engine.Set(new bg.webgl1.Engine(this.gl));
        
        this.glow = new bg.Color(0,0,0,1);
        this.glowInc = 0.1;
		
        this.buildShape();
        this.buildShader();
                
		this.pipeline = new bg.base.Pipeline(this.gl);
		bg.base.Pipeline.SetCurrent(this.pipeline);
	}
    
    frame(delta) {
        if (this.glow.r>1) {
            this.glowInc = -0.05;
        }
        else if (this.glow.r<-0.5) {
            this.glowInc = 0.05;
        }
        this.glow.add(new bg.Color(this.glowInc, this.glowInc, this.glowInc, 0));
    }
	
	display() {
		this.pipeline.clearBuffers(bg.base.ClearBuffers.COLOR | bg.base.ClearBuffers.DEPTH);
        
        this.shader.setActive();
        this.shader.setInputBuffer("position",this.plist.vertexBuffer,3);
        this.shader.setInputBuffer("color",this.plist.colorBuffer,4);
        this.shader.setVector4("uColorAdd",this.glow);
        
        this.plist.draw();
        
        this.shader.disableInputBuffer("position");
        this.shader.disableInputBuffer("color");
        this.shader.clearActive();
	}
	
	reshape(width,height) {
		this.pipeline.viewport = new bg.Viewport(0,0,width,height);
	}
    
    mouseMove(evt) { this.postRedisplay(); }
}

function load() {
	let controller = new PlistSampleWindowController();
	let mainLoop = bg.app.MainLoop.singleton;
	
	mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
	mainLoop.canvas = document.getElementsByTagName('canvas')[0];
	mainLoop.run(controller);
}