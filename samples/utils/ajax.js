(function() {
	function loadResources() {
		return bg.utils.Resource.Load("../data/file.txt")
			.then(function(data) {
				console.log(data);
				return bg.utils.Resource.Load("../data/file.json");
			})
			.then(function(data) {
				console.log(data);
				return bg.utils.Resource.Load("../data/simple_cube.vwglb");
			})
			.then(function(data) {
				console.log(data);
				return bg.utils.Resource.Load("../data/bricks.jpg");
			})
			.then(function(img) {
				console.log(img);
				return img;
			});
	}
	
	function multipleLoad() {
		let resources = ["../data/file.txt",
						 "../data/simple_cube.vwglb",
						 "../data/bricks.jpg",
						 "../data/file.json"];

		return bg.utils.Resource.Load(resources)
			.then(function(data) {
				console.log(data);
				let image = data["../data/bricks.jpg"];
				document.body.appendChild(image);
			});
	}
	
	loadResources()
		.then(function() {
			return multipleLoad();
		})
		.then(function() {
			console.log("Test done");
		})
		.catch(function(err) {
			console.log(err);
		});
})();