
class LoaderWindowController extends bg.app.WindowController {
	buildScene() {
		this._root = new bg.scene.Node(this.gl, "Root node");
		
		bg.base.Loader.RegisterPlugin(new bg.base.TextureLoaderPlugin());
		bg.base.Loader.RegisterPlugin(new bg.base.VWGLBLoaderPlugin());
		
		let chain = new bg.scene.Node(this.gl,"Chain");
		chain.addComponent(new bg.scene.Chain());
		this._root.addChild(chain);
		
		let elements = [];
		
		elements.push(bg.base.Loader.Load(this.gl,"../data/test-shape.vwglb"));
		elements.push(bg.base.Loader.Load(this.gl,"../data/test-shape-angle.vwglb"));
		
		Promise.all(elements)
			.then(function(nodes) {
				// A node instance is created with a COPY of all of it's components, except
				// the Drawable component, that is a copy of the textures, but share the same
				// instances of the polyList. This allows to modify the material, but at the
				// same time is less expensive, because the polyList are the same instances.
				let cube = nodes[0];			
				cube.addComponent(new bg.scene.Transform());
	
				let cubeAngle = nodes[1];
				cubeAngle.addComponent(new bg.scene.Transform());
				
				chain.addChild(cube);
				chain.addChild(bg.scene.Drawable.InstanceNode(cube));
				chain.addChild(cubeAngle);
				chain.addChild(bg.scene.Drawable.InstanceNode(cube));
				chain.addChild(bg.scene.Drawable.InstanceNode(cube));
				chain.addChild(bg.scene.Drawable.InstanceNode(cube));
				chain.addChild(bg.scene.Drawable.InstanceNode(cubeAngle));
				chain.addChild(bg.scene.Drawable.InstanceNode(cube));
				chain.addChild(bg.scene.Drawable.InstanceNode(cube));
				chain.addChild(bg.scene.Drawable.InstanceNode(cube));
				chain.addChild(bg.scene.Drawable.InstanceNode(cubeAngle));
				chain.addChild(bg.scene.Drawable.InstanceNode(cube));
				chain.addChild(bg.scene.Drawable.InstanceNode(cube));
				chain.addChild(bg.scene.Drawable.InstanceNode(cube));
				chain.addChild(bg.scene.Drawable.InstanceNode(cubeAngle));
				chain.addChild(bg.scene.Drawable.InstanceNode(cube));
				
				// To show that each drawable contains a copy of the material, we going
				// to mark the original drawables
				let cubeDrw = cube.component("bg.scene.Drawable");
				cubeDrw.getMaterial(0).diffuse = bg.Color.Red();
				cubeDrw.getMaterial(0).lightEmission = 0.5;
				
				let cubeAngleDrw = cubeAngle.component("bg.scene.Drawable");
				cubeAngleDrw.getMaterial(0).diffuse = bg.Color.Red();
				cubeAngleDrw.getMaterial(0).lightEmission = 0.5;
			})
			
			.catch(function(err) {
				alert(err.message);
			});
		
		let lightNode = new bg.scene.Node(this.gl,"Light");
		this._root.addChild(lightNode);
		let lightComponent = new bg.scene.Light(new bg.base.Light(this.gl));
		lightNode.addComponent(lightComponent);	
		lightNode.addComponent(new bg.scene.Transform());
		lightNode.component("bg.scene.Transform").matrix
					.identity()
					.rotate(bg.Math.degreesToRadians(53),0,1,0)
					.rotate(bg.Math.degreesToRadians(65),-1,0,0);
		
		this._camera = new bg.scene.Camera();
		let cameraNode = new bg.scene.Node("Camera");
		cameraNode.addComponent(this._camera);			
		cameraNode.addComponent(new bg.scene.Transform());
		cameraNode.component("bg.scene.Transform").matrix
			.translate(0.2,0,0)
			.rotate(bg.Math.degreesToRadians(22.5),-1,0,0)
			.translate(0,0,6);
		this._root.addChild(cameraNode);
	}
    
	init() {
		bg.Engine.Set(new bg.webgl1.Engine(this.gl));
		
        this.buildScene();
		
		this._renderer = new bg.render.Renderer.Create(this.gl,bg.render.RenderPath.FORWARD);
	}
    
    frame(delta) { this._renderer.frame(this._root, delta); }
	
	display() { this._renderer.display(this._root, this._camera); }
	
	reshape(width,height) {
		this._camera.viewport = new bg.Viewport(0,0,width,height);
		this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
	}
}

function load() {
	let controller = new LoaderWindowController();
	let mainLoop = bg.app.MainLoop.singleton;
	
	mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
	mainLoop.canvas = document.getElementsByTagName('canvas')[0];
	mainLoop.run(controller);
}
