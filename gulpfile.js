const   gulp = require('gulp'),
        sourcemaps = require('gulp-sourcemaps'),
        traceur = require('gulp-traceur'),
        concat = require('gulp-concat'),
        connect = require('gulp-connect'),
		replace = require('gulp-replace'),
		sort = require('gulp-sort'),
		minify = require('gulp-minify'),
		fs = require('fs'),
		path = require('path'),
		exec = require('child_process').execSync;

var config = {
	outDir:'../bg2e-js-dist',
	npmDir:'../bg2e-npm',
	inDir:'dist'
}

function getVersion(simple = false) {
	let pkg = require('./package.json');
	try {
		if (simple) {
			return pkg.version;
		}
		let rev = exec('git show --oneline -s');
		let re = /([a-z0-9:]+)\s/i;
		let reResult = re.exec(rev);
		if (reResult && !/fatal/.test(reResult[1])) {
			return pkg.version + ' - build: ' + reResult[1];
		}
		else {
			return pkg.version;
		}
	}
	catch (e) {
		return pkg.version;
	}
}

function addJavaScript(source,fileName,dest,compileES) {
	if (compileES) {
		return Promise.all([
			gulp.src(source)
				.pipe(sort())
				.pipe(traceur())
				.pipe(concat(fileName))
				.pipe(replace(/@version@/,getVersion()))
				.pipe(gulp.dest(dest)),
		
			gulp.src(source)
				.pipe(sort())
				.pipe(traceur())
				.pipe(concat(fileName))
				.pipe(replace(/@version@/,getVersion()))
				.pipe(minify({
					ext: {
						src:'.js',
						min:'.min.js'
					}
				}))
				.pipe(gulp.dest(dest))
		]);
	}
	else {
		return Promise.all([
			gulp.src(source)
				.pipe(sort())
				.pipe(concat(fileName))
				.pipe(replace(/@version@/,getVersion()))
				.pipe(gulp.dest(dest))
		]);
	}
}

function addSample(sampleName) {
	gulp.src(`samples/${sampleName}/index.html`)
		.pipe(gulp.dest(`${config.outDir}/sample/${sampleName}`));
	
	if (fs.existsSync(`samples/${sampleName}/es2015.txt`)) {
		gulp.src(`samples/${sampleName}/*.js`)
			.pipe(concat(`${sampleName}.js`))
			.pipe(gulp.dest(`${config.outDir}/sample/${sampleName}`));
	}
	else {
		gulp.src(`samples/${sampleName}/*.js`)
			.pipe(traceur())
			.pipe(concat(`${sampleName}.js`))
			.pipe(gulp.dest(`${config.outDir}/sample/${sampleName}`));
	}
}

gulp.task("webserver", function() {
	connect.server({
		livereload: true,
		port: 8888,
		root: `${config.outDir}`
	});
});

gulp.task("library", function() {
	return Promise.all([
		gulp.src("node_modules/traceur/bin/traceur-runtime.js")
			.pipe(gulp.dest(config.outDir + '/js')),
	
		addJavaScript(["src/**/*.js"],"bg2e.js",config.outDir + "/js",true),

		addJavaScript(["src/**/*.js"],"bg2e-es2015.js",config.outDir + '/js',false),

		gulp.src("node_modules/traceur/bin/traceur-runtime.js")
			.pipe(gulp.dest(config.inDir)),

		addJavaScript(["src/**/*.js"],"bg2e.js",config.inDir,true),
		
		addJavaScript(["src/**/*.js"],"bg2e-es2015.js",config.inDir,false)
	]);
});

gulp.task("samples", function() {
	var sampleString = "";
	fs.readdirSync('samples')
		.filter(function(file) {
			return fs.statSync(path.join('samples',file)).isDirectory();
		})
		.forEach(function(sample) {
			sampleString += "\n" + `		<li><a href="sample/${sample}/index.html">${sample}</a></li>`;
			addSample(sample);
		});
	
	gulp.src(`data/*`)
		.pipe(gulp.dest(`${config.outDir}/sample/data`));

	gulp.src(`data/obj-plugin/*`)
		.pipe(gulp.dest(`${config.outDir}/sample/data/obj-plugin`));
	
	gulp.src(`data/sample-scene/*`)
		.pipe(gulp.dest(`${config.outDir}/sample/data/sample-scene`));
		
	gulp.src(`data/gizmos/*`)
		.pipe(gulp.dest(`${config.outDir}/sample/data/gizmos`));

	gulp.src(`data/pbr/*`)
		.pipe(gulp.dest(`${config.outDir}/sample/data/pbr`));

	gulp.src('index.html')
		.pipe(replace('{{ samples }}',sampleString))
		.pipe(gulp.dest(`${config.outDir}/`));
});

gulp.task("watch", function() {
	gulp.watch([
		"src/*.js",
		"src/**/*.js"
	],["library"]);
	
	gulp.watch([
		"index.html",
		"samples/**/*.js",
		"data/*",
		"samples/**/*.html"],["samples"]);
});

gulp.task("packageDist", function() {
	return Promise.all([
		gulp.src("dist_packages/bower.json")
			.pipe(replace(/@version@/,getVersion(true)))
			.pipe(gulp.dest(config.outDir)),
		
		gulp.src("dist_packages/package.json")
			.pipe(replace(/@version@/,getVersion(true)))
			.pipe(gulp.dest(config.outDir))
	])
});

gulp.task("setNpmOut", function() {
	return new Promise((resolve,reject) => {
		config.outDir = config.npmDir;
		resolve();
	});
});

gulp.task("build",["library","samples","packageDist"]);

gulp.task("default",["build","webserver","watch"]);

gulp.task("npm",["setNpmOut","library"]);
