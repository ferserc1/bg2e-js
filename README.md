# Business Grade Graphic Engine - JavaScript API
## About

This library is the WebGL/JavaScript counterpart of the bg2 engine C++ API. This library provides an API to
load scenes and models created with the bg2 Composer tool.

This project is currently a draft, and the authors recommends not to use it because it may be unstable.

More info at the main [bg2 engine website](http://www.bg2engine.com)

## Install

You need to install Node.js, bower and gulp.

### Node.js

Get Node.js installation package for OS X or Windows from the [Node.js website](https://nodejs.org). For Linux
PCs, follow the installation documentation for your specific distribution at Node.js website. In Linux systems
maybe you'll also need to install npm (node package manager).

### Bower and Gulp

Use the Node Package Manager (npm) to install Bower and Gulp globally:

OS X, Linux, UNIX systems:

	sudo npm install -g bower
	sudo npm install -g gulp


Windows systems:

	npm install -g bower
	npm install -g gulp

### Compile and launch the samples

In the command line, go to the bg2e-js folder. run the following commands:

	npm install
	bower install

This will install the library and build system dependencies.

To build the library and execute the samples, run:

	gulp

A folder named 'dist' will be created in the bg2e-js root, and this will also launch a test server. You
can browse the demos entering the following URL in your browser:

	http://localhost:8080/dist/index.html

If you only want to compile the library, run this command:

	gulp library

If you also want to build the samples, run:

	gulp samples
	

## License

bg2e is distributed under the MIT license: you can use it for free, for any purpose, in all the universe (and also in other parallel universes, if they exists), with only two conditions: you can't claim that this work is yours (for example, fork this repository and change my name by yours), and you use this library as is (you can't sue me if this software does something wrong). You can see the full license [here](LICENSE.md)

