# Shader Library: inputs

The shader library inputs is a set of objects that contains the definition of
all the standard shader input variables. The variable definition is a set of
three values:

- name: The name of the variable in the shader.
- dataType: The data type, as a GLSL 1 type (vec3, sampler2D, etc). This type
may be converted to other when the shader code is generated, depending on the
active engine. (webgl v1, webgl v2, or any future version).
- role: buffer for a vertex buffer object, value for a uniform, in or out for varying


## Standard input variables:

Access to the input varialbes:

	let lib = bg2e.base.ShaderLibrary.Get()
	lib.inputs
		>> [Object]

### Vertex buffer variables (equivalent to attribute in GLSL v1)

	lib.inputs.buffers => role: buffer
		
Variable            | name          | data type | description
------------------- | ------------- | --------- | -------------------------
vertex              | inVertex      | vec3      | vertex array
normal              | inNormal      | vec3      | normal vectors array
tangent             | inTangent     | vec3      | tangent vectors array
tex0                | inTex0        | vec2      | texture coord 0 array
tex1                | inTex1        | vec2      | texture coord 1 array
tex2                | inTex2        | vec2      | texture coord 2 array
color               | inColor       | vec4      | color array

### Matrix

	lib.inputs.matrix => role: value

Variable            | name                    | data type | description
------------------- | ----------------------- | --------- | -----------------------------
model               | inModelMatrix           | mat4      | Model matrix
view                | inViewMatrix            | mat4      | Camera view matrix
projection          | inProjectionMatrix      | mat4      | Camera projection matrix
normal              | inModelMatrix           | mat4      | Normal matrix
viewInv             | inViewMatrixInv         | mat4      | Camera view matrix inverted


### Material

	lib.inputs.material => role: value

Variable                     | name                          | data type | description
---------------------------- | ----------------------------- | --------- | -----------------------------
diffuse                      | inDiffuseColor                | vec4      | diffuse color
specular                     | inSpecularColor               | vec4      | specular color
shininess                    | inShininess                   | float     | shininess (0=off, 1..255) 
shininessMask                | inShininessMask               | sampler2D | shinines texture mask (RGBA)
shininessMaskChannel         | inShininessMaskChannel        | vec4      | result = mask * maskChannel
shininessMaskInvert          | inShininessMaskInvert         | bool      | invert shininess mask value
lightEmission                | inLightEmission               | float     | amount of light emission (0..1)
lightEmissionMask            | inLightEmissionMask           | sampler2D | light emission tex mask (RGBA)
lightEmissionMaskChannel     | inLightEmissionMaskChannel    | vec4      | result = mask * maskChannel
lightEmissionMaskInvert      | inLightEmissionMaskInvert     | bool      | invert light emission mask
texture                      | inTexture                     | sampler2D | diffuse texture
textureOffset                | inTextureOffset               | vec2      |   UVt = UVt * scale + offset
textureScale                 | inTextureScale                | vec2      | 
lightMap                     | inLightMap                    | sampler2D | light map texture
lightMapOffset               | inLightMapOffset              | vec2      |  UVlm = UVlm * scale + offset
lightMapScale                | inLightMapScale               | vec2      |
normalMap                    | inNormalMap                   | sampler2D | normal map texture
normalMapOffset              | inNormalMapOffset             | vec2      |  nm = nm * scale + offset
normalMapScale               | inNormalMapScale              | vec2      |
reflection                   | inReflection                  | float     | amount of reflection (0..1)
reflectionMask               | inReflectionMask              | sampler2D | reflection mask texture
reflectionMaskChannel        | inReflectionMaskChannel       | vec4      | result = mask * maskChannel
reflectionMaskInvert         | inReflectionMaskInvert        | bool      | invert reflection mask values
castShadows                  | inCastShadows                 | bool      | should cast shadows
receiveShadows               | inReceiveShadows              | bool      | should receive shadows


### Lighting

	lib.inputs.lighting => role: value

Variable                     | name                      | data type | description
---------------------------- | ------------------------- | --------- | --------------------------------------------
type                         | inLightType               | int       | directional=4, spot=1, point=5, disabled=10
position                     | inLightPosition           | vec3      | light position in model space
direction                    | inLightDirection          | vec3      | light direction in model space
ambient                      | inLightAmbient            | vec4      | ambient color
diffuse                      | inLightDiffuse            | vec4      | diffuse color
specular                     | inLightSpecular           | vec4      | specular color
attenuation                  | inLightAttenuation        | vec3      | x=const, y=linear, z=exp 
spotExponent                 | inSpotExponent            | float     | spot light exponent
spotCutoff                   | inSpotCutoff              | float     | spot light cutoff angle
cutoffDistance               | inLightCutoffDistance     | float     | point light cutoff fistance
exposure                     | inLightExposure           | float     | light exposure multiplier
shadowMap                    | inShadowMap               | sampler2D | depth map from light
shadowMapSize                | inShadowMapSize           | vec2      | depth map resolution
shadowStrength               | inShadowStrength          | float     | shadow strength 0:no shadow, 1: drarkest shadow
shadowColor                  | inShadowColor             | vec4      | color of the shadow multiplied by shadowStrength
shadowBias                   | inShadowBias              | float     | shadow depth test bias

### Shadows

	lib.inputs.shadows => role: value

Variable                     | name                      | data type | description
---------------------------- | ------------------------- | --------- | --------------------------------------------
shadowMap                    | inShadowMap               | sampler2D | depth map from light
shadowMapSize                | inShadowMapSize           | vec2      | depth map resolution
shadowStrength               | inShadowStrength          | float     | shadow strength 0:no shadow, 1: drarkest shadow
shadowColor                  | inShadowColor             | vec4      | color of the shadow multiplied by shadowStrength
shadowBias                   | inShadowBias              | float     | shadow depth test bias

### Color correction

	lib.inputs.colorCorrection => role: value

Variable                     | name                      | data type | description
---------------------------- | ------------------------- | --------- | --------------------------------------------
hue                          | inHue                     | float     | color variation
saturation                   | inSaturation              | float     | color intensity
lightness                    | inLightness               | float     | amount of overexposition
brightness                   | inBrightness              | float     | similar to lightness
contrast                     | inContrast                | float     | difference between light and shadows