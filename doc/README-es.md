
# Business Grade Graphic Engine

## Código de ejemplo

### Basic

- [Configuración del entorno](basic/setup-es.md)
- [Crear el canvas 3D](basic/create-canvas-es.md)
- [Eventos de entrada y refresco de la imagen](basic/input-events-es.md)
- [Escena y Renderer](basic/basic-renderer-es.md)
- [Manejar los eventos de entrada](basic/input-handle-es.md)
