# Manejar los eventos de entrada
### Prerrequisitos
Utiliza el directorio que has creado en el tutorial [escena y renderer](basic-renderer-es.md) como punto inicial.

## Eventos y componentes de ecena
Con bg2 engine es extremadamente fácil añadir un comportamiento a un objeto de escena en respuesta a una acción de usuario. Los componentes de la escena pueden manejar los eventos de entrada, y la única cosa que tenemos que hacer es asegurarnos de que éstos eventos son despachados convenientemente a la raíz de la esena. Una vez hecho esto, cada componente tendrá acceso a los eventos de entrada.

Para conectar los eventos del usuario con la escena, utilizaremos un SceneVisitor. El visitante es un patrón de diseño que se utiliza para aplicar cierta acción a una estructura de datos compleja, como por ejemplo, el árbol de nuestra escena. En este caso, utilizaremos un visitante de tipo 'bg.scene.InputVisitor'. Añade el siguiente código inmediatamente después de la creación del renderer:

```javascript
this._inputVisitor = new bg.scene.InputVisitor();
```
Una vez hecho esto, hay que mapear todos los eventos del window controller al input visitor. Añade el siguiente código al final de tu clase Window controller.

```javascript
keyDown(evt) { this._inputVisitor.keyDown(this._sceneRoot, evt); }	
keyUp(evt) { this._inputVisitor.keyDown(this._sceneRoot, evt); }
mouseUp(evt) { this._inputVisitor.mouseUp(this._sceneRoot, evt); }
mouseDown(evt) { this._inputVisitor.mouseDown(this._sceneRoot, evt); }
mouseMove(evt) { this._inputVisitor.mouseMove(this._sceneRoot, evt); }
mouseOut(evt) { this._inputVisitor.mouseOut(this._sceneRoot, evt); }
mouseDrag(evt) { this._inputVisitor.mouseDrag(this._sceneRoot, evt); }
mouseWheel(evt) { this._inputVisitor.mouseWheel(this._sceneRoot, evt); }
touchStart(evt) { this._inputVisitor.touchStart(this._sceneRoot, evt); }
touchMove(evt) { this._inputVisitor.touchMove(this._sceneRoot, evt); }
touchEnd(evt) { this._inputVisitor.touchEnd(this._sceneRoot, evt); }
```

Este código es bastante sencillo: cada función del input visitor recibe la raíz de la escena y el evento de entrada, y las funciones de eventos del input visitor se llaman igual que las callbacks correspondientes del window controller.

## Usando los eventos de entrada
Con estas modificaciones, la escena ya puede manejar los eventos de entrada. Lo único que falta por hacer es añadir algún comportamiento a la escena que utilice estos eventos.

Para hacerlo, vamos a añadir un componente bg.manipulation.OrbitCameraController al nodo Camera. Este componente permite manipular la cámara utilizando el ratón o la pantalla táctil.

Busca la función createCamera() y reemplaza el código correspondiente con la creación del componente Camera con este otro:

```javascript
this._camera = new bg.scene.Camera();
cameraNode.addComponent(this._camera);
cameraNode.addComponent(new bg.scene.Transform());
cameraNode.addComponent(new bg.manipulation.OrbitCameraController());
```

## Compilar y ejecutar
Compila la aplicación usando gulp y pruébala utilizando tu navegador favorito:


![basic-renderer.jpg](basic-renderer.jpg)

Ahora podemos cambiar el punto de vista de la cámara utilizando el ratón o la pantalla táctil.

## Código final
##### index.js
```javascript
class MyWindowController extends bg.app.WindowController {
    constructor() {
        super();
    }

    createObjects() {
        let cubeNode = new bg.scene.Node(this.gl);
        this._sceneRoot.addChild(cubeNode);
        cubeNode.addComponent(bg.scene.PrimitiveFactory.Cube(this.gl,1,1,1));

        let floorNode = new bg.scene.Node(this.gl);
        this._sceneRoot.addChild(floorNode);
        floorNode.addComponent(bg.scene.PrimitiveFactory.Plane(this.gl,10));
        floorNode.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(0,-1,0)));
    }

    createLight() {
        let lightNode = new bg.scene.Node(this.gl);
        this._sceneRoot.addChild(lightNode);

        lightNode.addComponent(new bg.scene.Light(new bg.base.Light(this.gl)));
        lightNode.addComponent(new bg.scene.Transform(
            new bg.Matrix4.Identity()
                .translate(0,0,5)
                .rotate(bg.Math.degreesToRadians(15),0,1,0)
                .rotate(bg.Math.degreesToRadians(55),-1,0,0)
                .translate(0,1.4,3)
        ));
    }

    createCamera() {
        let cameraNode = new bg.scene.Node(this.gl,"Main camera");
        this._sceneRoot.addChild(cameraNode);

        this._camera = new bg.scene.Camera();
        cameraNode.addComponent(this._camera);
        cameraNode.addComponent(new bg.scene.Transform());
        cameraNode.addComponent(new bg.manipulation.OrbitCameraController());
    }

    buildScene() {
        this._sceneRoot = new bg.scene.Node(this.gl,"Scene Root");

        this.createObjects();
        this.createLight();
        this.createCamera();
    }
    
    init() {
        bg.Engine.Set(new bg.webgl1.Engine(this.gl));

        this.buildScene();

        this._renderer = bg.render.Renderer.Create(
            this.gl,
            bg.render.RenderPath.FORWARD
        );
        this._renderer.clearColor = new bg.Color(0.2,0.4,0.7,1);

        this._inputVisitor = new bg.scene.InputVisitor();
    }
    
    frame(delta) {
        this._renderer.frame(this._sceneRoot, delta);
    }

    display() {
        this._renderer.display(this._sceneRoot, this._camera);
    }
    
    reshape(width,height) {
        this._camera.viewport = new bg.Viewport(0,0,width,height);
        this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
    }

    keyDown(evt) { this._inputVisitor.keyDown(this._sceneRoot, evt); }	
	keyUp(evt) { this._inputVisitor.keyDown(this._sceneRoot, evt); }
	mouseUp(evt) { this._inputVisitor.mouseUp(this._sceneRoot, evt); }
	mouseDown(evt) { this._inputVisitor.mouseDown(this._sceneRoot, evt); }
	mouseMove(evt) { this._inputVisitor.mouseMove(this._sceneRoot, evt); }
	mouseOut(evt) { this._inputVisitor.mouseOut(this._sceneRoot, evt); }
	mouseDrag(evt) { this._inputVisitor.mouseDrag(this._sceneRoot, evt); }
	mouseWheel(evt) { this._inputVisitor.mouseWheel(this._sceneRoot, evt); }
	touchStart(evt) { this._inputVisitor.touchStart(this._sceneRoot, evt); }
	touchMove(evt) { this._inputVisitor.touchMove(this._sceneRoot, evt); }
	touchEnd(evt) { this._inputVisitor.touchEnd(this._sceneRoot, evt); }
}

function load(canvas) {
    let ctrl = new MyWindowController();
    let mainLoop = bg.app.MainLoop.singleton;
    
    mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
    mainLoop.canvas = canvas;
    mainLoop.run(ctrl);
}
```