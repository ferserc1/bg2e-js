# Renderer and scenes
### Prerrequisites
Use the resulting folder of [create a 3D canvas](create-canvas.md) tutorial as the starting source code for this sample app.

## Engine initialization
bg2 engine is designed to support several rendering technologies. For example, you can use the standard WebGL API, but you also can use the newer WebGL 2.0 API, or in a future, you can create a custom interface to use another underlaying technology.

The first task you must to do before anything is to initialize the rendering engine. In this example, we'll use WebGL 1.0. Add the following code to the init() function:

```javascript
init() {
	bg.Engine.Set(new bg.webgl1.Engine(this.gl));
}
```
## The scene
Now that you have create the rendering engine, you can build the scene. In this sample, we'll create a simple scene composed by a floor, a cube, a light source and a camera.

We have divided these tasks in four functions. Start by adding the buildScene() function at the begining of your Window Controller class.

```javascript
buildScene() {
	this._sceneRoot = new bg.scene.Node(this.gl,"Scene Root");
	
	this.createObjects();
	this.createLight();
	this.createCamera();
}
```

The `bg.scene` package contains all the classes that we'll use to create the scene. The scene is a tree data structure composed by one unique type of node. The bg.scene.Node class represents a tree node.

The scene nodes does not implements anything apart from the scene structure. To provide some behavior to a node, we'll use the scene components.

The first scene node is known as the 'scene root'. All the scene nodes will belong to this node.

## The camera
Now, we'll continue by adding the createCamera() function:

```javascript
createCamera() {
    let cameraNode = new bg.scene.Node(this.gl,"Main camera");
    this._sceneRoot.addChild(cameraNode);
    
    this._camera = new bg.scene.Camera();
    cameraNode.addComponent(this._camera);
    cameraNode.addComponent(new bg.scene.Transform(
        bg.Matrix4.Translation(0.2,0,0)
            .rotate(bg.Math.degreesToRadians(-25),0,1,0)
            .rotate(bg.Math.degreesToRadians(22.5),-1,0,0)
            .translate(0,0,3)
    ));
}
```

In the first place, we create the camera node. As you can see, we do it in the same way as we have done previously, to create the scene root node. Next, we add the camera node to the scene root using the addChild function.

The rest of the function code will add the behavior to convert the generic scene node into a camera node. First, we'll add a Camera component. Then, we'll add a Transform component to place the camera in the desired position.

Note that we have stored the camera component in an attribute of our window controller (`this._camera`). We do this because will need to know which camera we want to use to render the scene.

The Transform component stores a transformation matrix and it apply it to its container node (and also to it child nodes). In this case, we have supplied the transformation matrix to the Transform component using its constructor. The bg.Matrix4.Translation() is an static function that returns a translation matrix. As you can see, you can pipe several functions to compose the full transformation matrix.

You can also create the matrix previously, call the transformation functions one by one and pass the resulting matrix to the Transform component constructor:

```javascript
...
let matrix = new bg.Matrix4();
matrix.identity();
matrix.translate(0.2,0,0);
matrix.rotate(bg.Math.degreesToRadians(-25),0,1,0);
matrix.rotate(bg.Math.degreesToRadians(22.5),-1,0,0);
matrix.translate(0,0,3);
cameraNode.addComponent(new bg.scene.Transform(matrix));
```

## The light
To render the scene, we need a light source to lit the scene objects. Add the following function to your Window Controller class:

```javascript
createLight() {
    let lightNode = new bg.scene.Node(this.gl);
    this._sceneRoot.addChild(lightNode);
    
    lightNode.addComponent(new bg.scene.Light(new bg.base.Light(this.gl)));
    lightNode.addComponent(new bg.scene.Transform(
        new bg.Matrix4.Identity()
            .translate(0,0,5)
            .rotate(bg.Math.degreesToRadians(15),0,1,0)
            .rotate(bg.Math.degreesToRadians(55),-1,0,0)
            .translate(0,1.4,3)
    ));
}
```

In this function, we do basically the same as we have done in the createCamera() function, but in this case we add a Light component, instead of a Camera component.

The Light component constructor seems a little bit confusing, because we are passing a `bg.base.Light` object to the `bg.scene.Light` component constructor. 
Why we need to create two different lights? The answer is simple: the Light behavior is implemented as a low level functionality in the bg.base package. You can use bg2 engine without using any scene at all using the low level APIs. The bg.base.Light is the basic implementation of a light source, and it stores all the light properties. The bg.scene.Light component is used to connect the base light source to the scene. You can specify more light types, and for that reason you need to provide the base light implementation to the light component.

```javascript
lightNode.addComponent(
	new bg.scene.Light(	// Light component
		new bg.base.Light(this.gl)	// Light implementation
	)
);
```

## Scene objects
The only remaining task to complete our scene is to create the scene objects. Add the following function to your Window Controller class:

```javascript
createObjects() {
    let cubeNode = new bg.scene.Node(this.gl);
    this._sceneRoot.addChild(cubeNode);
    cubeNode.addComponent(bg.scene.PrimitiveFactory.Cube(this.gl,1,1,1));

    let floorNode = new bg.scene.Node(this.gl);
    this._sceneRoot.addChild(floorNode);
    floorNode.addComponent(bg.scene.PrimitiveFactory.Plane(this.gl,10));
    floorNode.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(0,-1,0)));
}
```

In this function, we add two nodes to store the floor and the cube. To create the floor and the cube objects, we use a factory class: `bg.scene.PrimitiveFactory`. This class is composed by several static functions that returns instances of `bg.scene.Drawable` components. We'll review this component in future tutorials.

## Finish the init() implementation
Now, we'll complete the init() function implementation. First, we'll add a call to the buildScene() function that we just added. After that, we'll create the Renderer instance:

```javascript
init() {
    bg.Engine.Set(new bg.webgl1.Engine(this.gl));

    this.buildScene();

    this._renderer = bg.render.Renderer.Create(
        this.gl,
        bg.render.RenderPath.FORWARD
    );
    this._renderer.clearColor = new bg.Color(0.2,0.4,0.7,1);
}
```

The renderer is created using the `bg.render.Renderer.Create()` factory method. It receive two parameters: the engine context and the rendering path:

- bg.renderer.RenderPath.FORWARD: create a renderer using forward rendering path.
- bg.renderer.RenderPath.DEFERRED: create a renderer using deferred rendering path.

The deferred render path supports high quality rendering techniques, such as screen space ambient occlusion, screen space ray traced reflections, unlimited light sources and other postprocess effects, but it require a high end graphic card to work smoothly, and even your browser may not support this mode. If your browser does not support deferred render, the factory method will return a forward renderer. Is for that reason that you need to create the renderer using a factory method.

You can use more than one renderer in your application, and combine them to alternate between high quality (but slow) rendering, and basic quality (but fast) rendering. This feature, in conjunction with the capacity of the MainLoop to update the scene manually, allows to generate high quality static images and basic quality animations in the same application. You can, for example, use a deferred renderer to take an screenshot of the scene. Anyway, the deferred renderer should work fine in recent PC hardware, even in discrete graphic cards.

The las line in the function simply sets the background rendering color.


## frame(delta), display() and reshape(width,height)
Finally, we need to update the rendering loop functions to use the renderer to generate the frame.

```javascript
frame(delta) {
	this._renderer.frame(this._sceneRoot, delta);
}

display() {
	this._renderer.display(this._sceneRoot, this._camera);
}
```

The renderer can handle all the underlying tasks needed to update the scene and generate de frame. To do it, we'll use the frame() and display() functions of the renderer. The first one, receives the scene root and the delta time increment. The second one receives the scene root and the camera which want to use as the viewer point.

Note that with this approach you can change the scene and the camera that you want to render at execution time. In the same way, you can use one or other renderer to generate the frame (deferred and forward, for example).


```javascript
reshape(width,height) {
    this._camera.viewport = new bg.Viewport(0,0,width,height);
    this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
}
```

In the reshape function, we'll update the camera's viewport and projection matrix. Note that if you have more than one camera in the scene, you probably want to update all your camera's properties here, and not only the main camera.

# Build and run
Build the application using gulp and test it using your favorite browser:

![basic-renderer.jpg](basic-renderer.jpg)

# Final code
##### index.js
```javascript
class MyWindowController extends bg.app.WindowController {
    constructor() {
        super();
    }

    createObjects() {
        let cubeNode = new bg.scene.Node(this.gl);
        this._sceneRoot.addChild(cubeNode);
        cubeNode.addComponent(bg.scene.PrimitiveFactory.Cube(this.gl,1,1,1));

        let floorNode = new bg.scene.Node(this.gl);
        this._sceneRoot.addChild(floorNode);
        floorNode.addComponent(bg.scene.PrimitiveFactory.Plane(this.gl,10));
        floorNode.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(0,-1,0)));
    }

    createLight() {
        let lightNode = new bg.scene.Node(this.gl);
        this._sceneRoot.addChild(lightNode);

        lightNode.addComponent(new bg.scene.Light(new bg.base.Light(this.gl)));
        lightNode.addComponent(new bg.scene.Transform(
            new bg.Matrix4.Identity()
                .translate(0,0,5)
                .rotate(bg.Math.degreesToRadians(15),0,1,0)
                .rotate(bg.Math.degreesToRadians(55),-1,0,0)
                .translate(0,1.4,3)
        ));
    }

    createCamera() {
        let cameraNode = new bg.scene.Node(this.gl,"Main camera");
        this._sceneRoot.addChild(cameraNode);

        this._camera = new bg.scene.Camera();
        cameraNode.addComponent(this._camera);
        cameraNode.addComponent(new bg.scene.Transform(
            bg.Matrix4.Translation(0.2,0,0)
                .rotate(bg.Math.degreesToRadians(-25),0,1,0)
                .rotate(bg.Math.degreesToRadians(22.5),-1,0,0)
                .translate(0,0,3)
        ));
    }

    buildScene() {
        this._sceneRoot = new bg.scene.Node(this.gl,"Scene Root");

        this.createObjects();
        this.createLight();
        this.createCamera();
    }
    
    init() {
        bg.Engine.Set(new bg.webgl1.Engine(this.gl));

        this.buildScene();

        this._renderer = bg.render.Renderer.Create(
            this.gl,
            bg.render.RenderPath.FORWARD
        );
        this._renderer.clearColor = new bg.Color(0.2,0.4,0.7,1);
    }
    
    frame(delta) {
        this._renderer.frame(this._sceneRoot, delta);
    }

    display() {
        this._renderer.display(this._sceneRoot, this._camera);
    }
    
    reshape(width,height) {
        this._camera.viewport = new bg.Viewport(0,0,width,height);
        this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
    }
}

function load(canvas) {
    let ctrl = new MyWindowController();
    let mainLoop = bg.app.MainLoop.singleton;
    
    mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
    mainLoop.canvas = canvas;
    mainLoop.run(ctrl);
}
```