# Escena y Renderer
### Prerrequisitos
Utiliza el directorio resultante del tutorial [create a 3D canvas](create-canvas-es.md) como punto de inicio para esta aplicación de ejemplo.

## Inicializar el motor
bg2 engine está diseñado para soportar varias tecnologías de renderizado 3D. Por ejemplo, puedes utilizar el API estándar de WebGL, pero también puedes utilizar la nueva version de WebGL 2.0, o incluso crear una interfaz nueva para futuras tecnologías.

La primera tarea que tenemos que hacer antes de nada es inicializar el motor gráfico. En este ejemplo, utilizaremos WebGL 1.0. Añade el siguiente código a la función init():

```javascript
init() {
	bg.Engine.Set(new bg.webgl1.Engine(this.gl));
}
```
## La escena
Ahora que hemos creado el motor de render, podemos construir la escena. En este ejemplo, crearemos una escena sencilla compuesta por un suelo, un cubo, una fuente de luz y una cámara.

Hemos dividido estas secciones en cuatro funciones. Empearemos creando la función buildScene() al principio de la clase Window Controller.

```javascript
buildScene() {
	this._sceneRoot = new bg.scene.Node(this.gl,"Scene Root");
	
	this.createObjects();
	this.createLight();
	this.createCamera();
}
```

El paquete `bg.scene` contiene todas las clases que se usarán para crear la escena. La escena es una estructura de datos de tipo árbol compuesta por un único tipo de nodo. La clase bg.scene.Node representa un nodo del árbol.

Los nodos de la escena no implementan nada aparte de la estructura de la escena. Para proporcionar comportamineto a un nodo, utilizaremos los componentes de escena.

El primer nodo de la escena se conoce como 'la raíz de la escena' (scene root). Todos los nodos de la escena pertenecerán directa o indirectamente a este nodo.


## The camera
Continuaremos añadiendo la función createCamera():

```javascript
createCamera() {
    let cameraNode = new bg.scene.Node(this.gl,"Main camera");
    this._sceneRoot.addChild(cameraNode);
    
    this._camera = new bg.scene.Camera();
    cameraNode.addComponent(this._camera);
    cameraNode.addComponent(new bg.scene.Transform(
        bg.Matrix4.Translation(0.2,0,0)
            .rotate(bg.Math.degreesToRadians(-25),0,1,0)
            .rotate(bg.Math.degreesToRadians(22.5),-1,0,0)
            .translate(0,0,3)
    ));
}
```

En priner lugar, creamos el nodo de la cámara. Como puedes ver, lo hacemos de la misma forma que lo hemos hecho previamente al crear la raíz de la escena. A continuación, añadiremos el nodo de cámara a la raíz de la escena, usando la función addChild.

El resto del código de la función añadirá el comportamiento necesario para convertir un nodo genérico de escena en una cámara. Primero, añadiremos un componente Camera. Después, añadiremos un componente Transform que usaremos para colocar la cámara en la posición que queramos.

Fíjate en que hemos guardado el componente Camera en un atributo de nuestra clase Window Controller (`this._camera`). Hacemos esto porque necesitamos especificar la cámara desde donde queremos renderizar la escena.

El componente Transform guarda una matriz de transformación y la aplica al nodo que contiene ese componente (también se aplica a todos sus nodos hijo). En este caso, hemos suministrado una matriz de transformación al componente Transform utilizando su constructor. La función estática `bg.Matrix4.Translation()` devuelve una matriz de traslación. Como puedes ver, es posible encadenar varias funciones para componer una matriz de transformación completa.

También puedes crear la matriz antes, llamar a las funciones de transformación una por una, y pasar la matriz resultante al constructor del componente Transform. El siguiente código es equivalente:

```javascript
...
let matrix = new bg.Matrix4();
matrix.identity();
matrix.translate(0.2,0,0);
matrix.rotate(bg.Math.degreesToRadians(-25),0,1,0);
matrix.rotate(bg.Math.degreesToRadians(22.5),-1,0,0);
matrix.translate(0,0,3);
cameraNode.addComponent(new bg.scene.Transform(matrix));
```

## La luz
Para renderizar la escena, necesitamos una fuente de luz para iluminar los objetos. Añade la siguiente función a tu clase Window Controller:

```javascript
createLight() {
    let lightNode = new bg.scene.Node(this.gl);
    this._sceneRoot.addChild(lightNode);
    
    lightNode.addComponent(new bg.scene.Light(new bg.base.Light(this.gl)));
    lightNode.addComponent(new bg.scene.Transform(
        new bg.Matrix4.Identity()
            .translate(0,0,5)
            .rotate(bg.Math.degreesToRadians(15),0,1,0)
            .rotate(bg.Math.degreesToRadians(55),-1,0,0)
            .translate(0,1.4,3)
    ));
}
```
En esta función hacemos básicamente lo mismo que hicimos en la función createCamera(), pero en este caso añadimos un componente Light en vez de un componente Camera.

El constructor del componente Light parece un poco confuso, porque estamos pasando un objeto `bg.base.Light` al constructor del componente `bg.scene.Light`.

¿Por qué necesitamos crear dos luces diferentes? La respuesta es simple: el comportamiento de la luz está implementado como una funcionalidad de bajo nivel dentro del paquete `bg.base`. Puedes utilizar bg2 engine sin crear ninguna escena utilizando el API de bajo nivel. La clase `bg.base.Light` es la implementación básica de una fuente de luz. El componente `bg.scene.Light` se utiliza para conectar la implementación de la luz con la escena. Puedes especificar otros tipos de luz, y por esta razón has de pasar la implementación base de la luz que quieres utilizar al constructor del componente Light.

```javascript
lightNode.addComponent(
	new bg.scene.Light(	// Light component
		new bg.base.Light(this.gl)	// Light implementation
	)
);
```

## Objetos de la escena
La única tarea que falta es crear los objetos de la escena. Añade la siguiente función a tu clase Window Controller:

```javascript
createObjects() {
    let cubeNode = new bg.scene.Node(this.gl);
    this._sceneRoot.addChild(cubeNode);
    cubeNode.addComponent(bg.scene.PrimitiveFactory.Cube(this.gl,1,1,1));

    let floorNode = new bg.scene.Node(this.gl);
    this._sceneRoot.addChild(floorNode);
    floorNode.addComponent(bg.scene.PrimitiveFactory.Plane(this.gl,10));
    floorNode.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(0,-1,0)));
}
```
En esta función, añadimos dos nodos para guardar el suelo y el cubo. Para crear estos objetos, usaremos la clase factoría `bg.scene.PrimitiveFactory`. Esta clase está compuesta por varias funciones estáticas que devuelven instancias a componentes de tipo `bg.scene.Drawable`. Revisaremos este componente en otros tutoriales.

## Acabar la implementación de init()
Ahora, terminaremos la imlementación de la función init(). Primero, añadiremos una llamada a la función buildScene() que acabamos de añadir. Después de esto, crearemos la instancia del Renderer:

```javascript
init() {
    bg.Engine.Set(new bg.webgl1.Engine(this.gl));

    this.buildScene();

    this._renderer = bg.render.Renderer.Create(
        this.gl,
        bg.render.RenderPath.FORWARD
    );
    this._renderer.clearColor = new bg.Color(0.2,0.4,0.7,1);
}
```

El renderer se crea utilizando el método factoría `bg.render.Renderer.Create()`. Recibe dos parámetros: el contexto del motor y el tipo de renderizador (render path):

- bg.renderer.RenderPath.FORWARD: Crea un renderizador que utiliza la técnica de forward render (renderización directa).
- bg.renderer.RenderPath.DEFERRED: Crea un renderizador que utiliza la técnica de deferred render (renderización diferida).

La renderización diferida soporta técnicas avanzadas de alta calidad, como screen space ambient occlusion, reflejos mediante trazador de rayos, posiblidad de renderizar un número indefinido de luces simultáneas y otros efectos de post proceso, pero requiere una alta capacidad gráfica para renderizar de forma fluida, e incluso puede que algunos navegadores no soporten las extensiones de WebGL necesarias para utilizar esta técnica.
Si tu navegador no soporta deferred render, el método factoría devolverá una instancia de tipo forward render. Es por este motivo que utilizamos un método factoría para crear el renderer.

Puedes utilizar más de un renderizador en tu aplicación, y combinarlos para alternar entre gráficos de alta calidad (aunque lentos) y gráficos con calidad más básica (pero más rápidos). Esta característica, junto con la capacidad del MainLoop para actualizar la escena manualmente, permite generar imágenes estáticas de alta calidad, y combinarlas con animaciones de calidad más básicas pero fluidas. Por ejemplo, puedes utilizar un renderizador de tipo deferred para capturar fotogramas independientes. De todas formas, el modo deferred debería funcionar razonablemente bien en cualquier hardware actual, incluso en portátiles de última generación con tarjeta gráfica discreta (no dedicada).

La última línea de la función simplemente establece el color de fondo en el renderizador.

## frame(delta), display() y reshape(width,height)
Por último, necesitamos actualizar la función del bucle de simulación para utilizar el renderer que hemos creqado:

```javascript
frame(delta) {
	this._renderer.frame(this._sceneRoot, delta);
}

display() {
	this._renderer.display(this._sceneRoot, this._camera);
}
```
El renderizador maneja todas las tareas necesarias para actualizar la escena y generar el fotograma. Para hacerlo, tendremos que utilizar las funciones frame() y display() del renderizador. La primera, recibe el nodo raíz de la escena y el incremento de tiempo delta. La segunda, recibe el nodo raíz de la escena y la cámara que queremos utilizar como punto de vista.

Fíjate que con esta aproximación, es posible cambiar la escena y la cámara siempre que queramos, en tiempo de ejecución. De la misma forma, puedes utilizar uno u otro renderizador para generar el fotograma (forward o deferred).

```javascript
reshape(width,height) {
    this._camera.viewport = new bg.Viewport(0,0,width,height);
    this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
}
```

En la función reshape, actualizaremos el viewport y la proyección de la cámara. Ten en cuenta que si tienes más una cámara, seguramente querrás actualizarlas todas en esta función.

## Compilar y ejecutar
Compila la aplicación usando gulp y pruébala utilizando tu navegador favorito:

![basic-renderer.jpg](basic-renderer.jpg)

## Código final
##### index.js
```javascript
class MyWindowController extends bg.app.WindowController {
    constructor() {
        super();
    }

    createObjects() {
        let cubeNode = new bg.scene.Node(this.gl);
        this._sceneRoot.addChild(cubeNode);
        cubeNode.addComponent(bg.scene.PrimitiveFactory.Cube(this.gl,1,1,1));

        let floorNode = new bg.scene.Node(this.gl);
        this._sceneRoot.addChild(floorNode);
        floorNode.addComponent(bg.scene.PrimitiveFactory.Plane(this.gl,10));
        floorNode.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(0,-1,0)));
    }

    createLight() {
        let lightNode = new bg.scene.Node(this.gl);
        this._sceneRoot.addChild(lightNode);

        lightNode.addComponent(new bg.scene.Light(new bg.base.Light(this.gl)));
        lightNode.addComponent(new bg.scene.Transform(
            new bg.Matrix4.Identity()
                .translate(0,0,5)
                .rotate(bg.Math.degreesToRadians(15),0,1,0)
                .rotate(bg.Math.degreesToRadians(55),-1,0,0)
                .translate(0,1.4,3)
        ));
    }

    createCamera() {
        let cameraNode = new bg.scene.Node(this.gl,"Main camera");
        this._sceneRoot.addChild(cameraNode);

        this._camera = new bg.scene.Camera();
        cameraNode.addComponent(this._camera);
        cameraNode.addComponent(new bg.scene.Transform(
            bg.Matrix4.Translation(0.2,0,0)
                .rotate(bg.Math.degreesToRadians(-25),0,1,0)
                .rotate(bg.Math.degreesToRadians(22.5),-1,0,0)
                .translate(0,0,3)
        ));
    }

    buildScene() {
        this._sceneRoot = new bg.scene.Node(this.gl,"Scene Root");

        this.createObjects();
        this.createLight();
        this.createCamera();
    }
    
    init() {
        bg.Engine.Set(new bg.webgl1.Engine(this.gl));

        this.buildScene();

        this._renderer = bg.render.Renderer.Create(
            this.gl,
            bg.render.RenderPath.FORWARD
        );
        this._renderer.clearColor = new bg.Color(0.2,0.4,0.7,1);
    }
    
    frame(delta) {
        this._renderer.frame(this._sceneRoot, delta);
    }

    display() {
        this._renderer.display(this._sceneRoot, this._camera);
    }
    
    reshape(width,height) {
        this._camera.viewport = new bg.Viewport(0,0,width,height);
        this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
    }
}

function load(canvas) {
    let ctrl = new MyWindowController();
    let mainLoop = bg.app.MainLoop.singleton;
    
    mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
    mainLoop.canvas = canvas;
    mainLoop.run(ctrl);
}
```