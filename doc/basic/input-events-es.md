# Eventos de entrada y refresco de la imagen
### Prerrequsitos
Utiliza el directorio que has creado en el tutorial [crear el canvas 3d](create-canvas-es.md) como punto inicial.

## Paso 1: actualiza el constructor
En este ejemplo, variaremos el color de fondo del lienzo utilizando la función frame(). Primero, necesitamos añadir una variable auxilar que utilizaremos para guardar el incremento que añadiremos a los componentes de color en cada fotograma.

```javascript
	constructor() {
        super();
        this.clearColor = [0,0.4,0.9,1];
        
        // Background color increment for each component (r, g, b)
        this.increment = [0.0001,0.00009,0.00013];
    }
```

## Paso 2: función frame(delta)
Añade la función frame(delta). Esta función recibe el tiempo que ha transcurrido desde el fotograma previo (en milisegundos) como parámetro:

```javascript
frame(delta) {
    let r = this.clearColor[0] + this.increment[0] * delta;
	let g = this.clearColor[1] + this.increment[1] * delta;
	let b = this.clearColor[2] + this.increment[2] * delta;
	if (r>1.0 || r<0.0) {
		this.increment[0] *= -1;
	}
	if (g>1.0 || g<0.0) {
		this.increment[1] *= -1;
	}
	if (b>1.0 || b<0.0) {
		this.increment[2] *= -1;
	}

	this.clearColor[0] = r;
	this.clearColor[1] = g;
	this.clearColor[2] = b;
	this.gl.clearColor(...this.clearColor);
}
```

# Paso 3: eventos de entrada
En este ejemplo, solamente imprimiremos los eventos en la consola. Puedes ver los logs utilizando la consola de javascript de tu navegador.

Añade las siguientes funciones a tu clase Window Controller, después de `reshape(width,height)`

```javascript
keyDown(evt) {
	console.log(`KeyDown: ${evt.key}`);
}
	
keyUp(evt) {
	console.log(`KeyUp: ${evt.key}`);
}
	
mouseUp(evt) {
	console.log(`MouseUp button: ${evt.button}, pos:${evt.x},${evt.y}`);
}
	
mouseDown(evt) {
	console.log(`MouseDown button: ${evt.button}, pos:${evt.x},${evt.y}`);
}
	
mouseMove(evt) {
	console.log(`MouseMove pos:${evt.x},${evt.y}`);
}
	
mouseOut(evt) {
	console.log(`MouseOut pos:${evt.x},${evt.y}`);
}
	
mouseDrag(evt) {
	console.log(`MouseDrag pos:${evt.x},${evt.y}`);
}
	
mouseWheel(evt) {
	console.log(`MouseWheel delta:${evt.delta} pos:${evt.x},${evt.y}`);
}
	
touchStart(evt) {
    console.log(evt);
    let touches = "";
    evt.touches.forEach((touch)=> {
        touches += `[${touch.identifier}: ${touch.x}, ${touch.y}] `;
    })
	console.log(`TouchStart ${touches}`);
}
	
touchMove(evt) {
	let touches = "";
    evt.touches.forEach((touch)=> {
        touches += `[${touch.identifier}: ${touch.x}, ${touch.y}] `;
    })
	console.log(`TouchMove ${touches}`);
}
	
touchEnd(evt) {
	let touches = "";
    evt.touches.forEach((touch)=> {
        touches += `[${touch.identifier}: ${touch.x}, ${touch.y}]`;
    })
	console.log(`TouchEnd ${touches}`);
}
```

## Compilar y ejecutar
Compila la aplicación usando gulp y pruébala utilizando tu navegador favorito:

![input-events.jpg](input-events.jpg)

## Código final
##### index.js
```javascript
class MyWindowController extends bg.app.WindowController {
    constructor() {
        super();
        this.clearColor = [0,0.4,0.9,1];
        this.increment = [0.0001,0.00009,0.00013];
    }
    
    init() {
        this.gl.clearColor(...this.clearColor);
    }
    
    frame(delta) {
        let r = this.clearColor[0] + this.increment[0] * delta;
		let g = this.clearColor[1] + this.increment[1] * delta;
		let b = this.clearColor[2] + this.increment[2] * delta;
		if (r>1.0 || r<0.0) {
			this.increment[0] *= -1;
		}
		if (g>1.0 || g<0.0) {
			this.increment[1] *= -1;
		}
		if (b>1.0 || b<0.0) {
			this.increment[2] *= -1;
		}

		this.clearColor[0] = r;
		this.clearColor[1] = g;
		this.clearColor[2] = b;
		this.gl.clearColor(...this.clearColor);
    }

    display() {
        this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
    }
    
    reshape(width,height) {
        this.gl.viewport(0,0,width,height);
    }

    keyDown(evt) {
		console.log(`KeyDown: ${evt.key}`);
	}
	
	keyUp(evt) {
		console.log(`KeyUp: ${evt.key}`);
	}
	
	mouseUp(evt) {
		console.log(`MouseUp button: ${evt.button}, pos:${evt.x},${evt.y}`);
	}
	
	mouseDown(evt) {
		console.log(`MouseDown button: ${evt.button}, pos:${evt.x},${evt.y}`);
	}
	
	mouseMove(evt) {
		console.log(`MouseMove pos:${evt.x},${evt.y}`);
	}
	
	mouseOut(evt) {
		console.log(`MouseOut pos:${evt.x},${evt.y}`);
	}
	
	mouseDrag(evt) {
		console.log(`MouseDrag pos:${evt.x},${evt.y}`);
	}
	
	mouseWheel(evt) {
		console.log(`MouseWheel delta:${evt.delta} pos:${evt.x},${evt.y}`);
	}
	
	touchStart(evt) {
        console.log(evt);
        let touches = "";
        evt.touches.forEach((touch)=> {
            touches += `[${touch.identifier}: ${touch.x}, ${touch.y}] `;
        })
		console.log(`TouchStart ${touches}`);
	}
	
	touchMove(evt) {
		let touches = "";
        evt.touches.forEach((touch)=> {
            touches += `[${touch.identifier}: ${touch.x}, ${touch.y}] `;
        })
		console.log(`TouchMove ${touches}`);
	}
	
	touchEnd(evt) {
		let touches = "";
        evt.touches.forEach((touch)=> {
            touches += `[${touch.identifier}: ${touch.x}, ${touch.y}]`;
        })
		console.log(`TouchEnd ${touches}`);
	}
}

function load(canvas) {
    let ctrl = new MyWindowController();
    let mainLoop = bg.app.MainLoop.singleton;
    
    mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
    mainLoop.canvas = canvas;
    mainLoop.run(ctrl);
}
```