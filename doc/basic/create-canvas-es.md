# Crear el canvas 3d
### Prerrequsitos
Utiliza el directorio que has creado en el tutorial [configuración del entorno](setup-es.md) como punto inicial.


## Paso 1:Crear un elemento `<canvas>`
Añade un elemento `<canvas>` a tu página web. Puedes hacerlo de cualquier forma (directamente en el código HTML, utilizando el API DOm de JavaScript o usando cualquier otro framework (como Angular.js), pero hay que crearlo y añadirlo al documento HTML antes del siguiente paso.

En esta aplicación de ejemplo, añadiremos el elemento canvas directamente en el fichero `index.html`.

```html
<body>
    <canvas width="480" height="360">Your browser does not support WebGL</canvas>
</body>
```
Puedes usar cualquier valor para `width` y `height` porque estos valores se configurarán automáticamente.

Añade el siguiente estilo a la cabecera de tu fichero HTML, para configurar el canvas para que se dibuje en toda la pantalla:

```html
	<style>
		canvas {
			position: fixed;
			width: 100%;
			height: 100%;
			left: 0px;
			right: 0px;
			top: 0px;
			bottom: 0px;
		}
	</style>
```

Por defecto, bg2 engine redimensiona automáticamente los atributos width y height del canvas utilizando el estilo CSS.

## Paso 2: código base JavaScript
En el fichero `index.js`, añade el siguiente código:

```javascript
class MyWindowController extends bg.app.WindowController {
	constructor() {
		super();
		this.clearColor = [0,0.4,0.9,1];
	}
	
	init() {
		this.gl.clearColor(...this.clearColor);
	}
	
	display() {
		this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
	}
	
	reshape(width,height) {
		this.gl.viewport(0,0,width,height);
	}
}

function load(canvas) {
	let ctrl = new MyWindowController();
	let mainLoop = bg.app.MainLoop.singleton;
	
	mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
	mainLoop.canvas = canvas;
	mainLoop.run(ctrl);
}
```
En este código:

- Hemos creado una subclase de `bg.app.WindowController`, que usaremos como punto de entrada de todo el bucle de simulación.
	- Utiliza la función init() para inicializar los ajustes gráficos. El atributo this.gl de la clase WindowController contiene el contexto de WebGL asociado al canvas.
	- Utiliza la función display() para dibujar el fotograma.
	- Utiliza la función reshape(width,height) para actualizar el tamaño del viewport.
- Hemos creado la función load(canvas), que se usará desde el fichero index.html para rear el canvas e iniciar el bucle de simulación.
	- bg.app.MainLoop.singleton es la instancia del bucle principal de simulación. Utiliza el atributo .singleton para crear una app que solamente tenga un único canvas 3d.
	- el atributo mainLoop.updateMode define cómo queremos actualizar la escena:
		- bg.app.FrameUpdate.AUTO: redibuja el canvas tan rápido como se pueda.
		- bg.app.FrameUpdate.MANUAL: no se redibuja el canvas, solamente se hará cuando llamemos manualmente a la función `WindowController.postRedisplay()`.
	- Establece el canvas que quieres usar como lienzo con el atributo MainLoop.canvas.
	- Llama a la función MainLoop.run(controller) para iniciar el bucle de simulación. Esta función recibe como parámetro la instancia del window controller.

# Paso 3: llama a la función load()
Modifica la etiqueta <body> para llamar a la función load():

```html
<body onload="load(document.getElementsByTagName('canvas')[0])">
```

# Compilar y ejecutar
Compila la aplicación usando gulp y pruébala utilizando tu navegador favorito:

![create-canvas.jpg](create-canvas.jpg)

# Código final
##### index.js
```javascript
class MyWindowController extends bg.app.WindowController {
    constructor() {
        super();
        this.clearColor = [0,0.4,0.9,1];
    }
    
    init() {
        this.gl.clearColor(...this.clearColor);
    }
    
    display() {
        this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
    }
    
    reshape(width,height) {
        this.gl.viewport(0,0,width,height);
    }
}

function load(canvas) {
    let ctrl = new MyWindowController();
    let mainLoop = bg.app.MainLoop.singleton;
    
    mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
    mainLoop.canvas = canvas;
    mainLoop.run(ctrl);
}
```

##### index.html
```html
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" context="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-COMPATIBLE" content="IE=edge">
	
	<script src="js/traceur.min.js"></script>
	<script src="js/bg2e.js"></script>
	<script src="js/bg2e-sample.js"></script>

	<style>
		canvas {
			position: fixed;
			width: 100%;
			height: 100%;
			left: 0px;
			right: 0px;
			top: 0px;
			bottom: 0px;
		}
	</style>
</head>
<body onload="load(document.getElementsByTagName('canvas')[0])">
    <canvas width="480" height="360">Your browser does not support WebGL</canvas>
</body>
</html>
```
