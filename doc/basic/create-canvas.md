# Create 3D Canvas
### Prerrequisites
Use the resulting folder of [environment setup](setup.md) tutorial as the starting source code for this sample app.


## Step 1: Create a `<canvas>` element
Add a `<canvas>` element to your web page. You can do it in any way (directly in your HTML file, using the DOM JavaScript API or using any JavaScript Framework, such as Angular.js), but it need to be created and inserted in the HTML document before the next step.

In this sample application, we'll add the canvas element directly to the `index.html` file.

```html
<body>
    <canvas width="480" height="360">Your browser does not support WebGL</canvas>
</body>
```

You can use any `width` and `height` values, because this parameters will be configured automatically.

Add the following style to the HTML header, to configure the canvas to be presented in the entire window:

```html
	<style>
		canvas {
			position: fixed;
			width: 100%;
			height: 100%;
			left: 0px;
			right: 0px;
			top: 0px;
			bottom: 0px;
		}
	</style>
```

By default, bg2 engine resizes automatically the width and height canvas attributes using the canvas style.

## Step 2: JavaScript base code
In the `index.js` file, add the following code:

```javascript
class MyWindowController extends bg.app.WindowController {
	constructor() {
		super();
		this.clearColor = [0,0.4,0.9,1];
	}
	
	init() {
		this.gl.clearColor(...this.clearColor);
	}
	
	display() {
		this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
	}
	
	reshape(width,height) {
		this.gl.viewport(0,0,width,height);
	}
}

function load(canvas) {
	let ctrl = new MyWindowController();
	let mainLoop = bg.app.MainLoop.singleton;
	
	mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
	mainLoop.canvas = canvas;
	mainLoop.run(ctrl);
}
```

#### Note: Use the previous source as the initial code for every sample apps.

In this source:

- We have created a `bg.app.WindowController` subclass, that will use as the entry point of all the simulation loop callbacks.
	- Use the init() function to initialize all the graphic settings. The this.gl attribute contains the WebGL context associated with the canvas.
	- Use the display() function to draw the frame.
	- Use the reshape(width,height) function to update the viewport size.
- We have created the load(canvas) function, that will be used from the index.html file to create canvas and start the simulation loop.
	- bg.app.MainLoop.singleton is the main loop instance. Use the .singleton attribute to create an app that only have one 3D canvas.
	- the mainLoop.updateMode define how the scene is updated:
		- bg.app.FrameUpdate.AUTO: redraw the canvas as fast as posibble.
		- bg.app.FrameUpdate.MANUAL: do not redraw the canvas. To update the canvas, use the `WindowController.postRedisplay()` function.
	- Set the target `canvas` with MainLoop.canvas attribute.
	- Call MainLoop.run(controller) function to begin the simulation loop. This function receives our window controller instance as parameter.

# Step 3: call the load() function
Modify the <body> tag to call the load() function:

```html
<body onload="load(document.getElementsByTagName('canvas')[0])">
```

# Build and run
Build the application using gulp and test it using your favorite browser:

![create-canvas.jpg](create-canvas.jpg)

# Final code
##### index.js
```javascript
class MyWindowController extends bg.app.WindowController {
    constructor() {
        super();
        this.clearColor = [0,0.4,0.9,1];
    }
    
    init() {
        this.gl.clearColor(...this.clearColor);
    }
    
    display() {
        this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
    }
    
    reshape(width,height) {
        this.gl.viewport(0,0,width,height);
    }
}

function load(canvas) {
    let ctrl = new MyWindowController();
    let mainLoop = bg.app.MainLoop.singleton;
    
    mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
    mainLoop.canvas = canvas;
    mainLoop.run(ctrl);
}
```

##### index.html
```html
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" context="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-COMPATIBLE" content="IE=edge">
	
	<script src="js/traceur.min.js"></script>
	<script src="js/bg2e.js"></script>
	<script src="js/bg2e-sample.js"></script>

	<style>
		canvas {
			position: fixed;
			width: 100%;
			height: 100%;
			left: 0px;
			right: 0px;
			top: 0px;
			bottom: 0px;
		}
	</style>
</head>
<body onload="load(document.getElementsByTagName('canvas')[0])">
    <canvas width="480" height="360">Your browser does not support WebGL</canvas>
</body>
</html>
```
