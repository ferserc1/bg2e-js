# Load 3D models
### Prerrequisites
Use the resulting folder of [Renderer and scenes](basic-renderer.md) tutorial as the starting source code for this sample app.

## Loader
bg2 engine integrates an extensible file loader that provides the capacity to load different resources. To load a resource, you need to provide one appropriate loader plugin. You can implement your own loader plugins, or you can use the default ones.

In this example, we'll modify the [renderer and scenes](basic-renderer.md) sample app to replace the primitive cube by a 3D model, using the Loader.

## Step 1: copy the resources and update the gulpfile.js
Download the [resources for this tutorial](cube-obj.zip). Extract the zip file, and place its contents into a folder with the name "resources", inside the sample app directory:

![resources folder](resources-folder.jpg)

The obj format requires an independent material definition file with mtl extension. The obj plugin will try to load the associated mtl file and it's images. To do it, you must ensure that the mtl file and the texture images are placed in the same path as the obj file.

Some similar occurs with the native bg2 files. The native bg2 models includes a material definition inside the file, but you need to ensure that you place the texture images in the same path as the bg2 file.

Update the gulpfile.js to copy the resources into the build folder:

##### gulpfile.js
```javascript
const   gulp = require('gulp'),
        sourcemaps = require('gulp-sourcemaps'),
        traceur = require('gulp-traceur'),
        concat = require('gulp-concat'),
        connect = require('gulp-connect'),
		replace = require('gulp-replace'),
		sort = require('gulp-sort'),
		minify = require('gulp-minify'),
		fs = require('fs'),
		path = require('path');

var config = {
	outDir:'build',
    serverPort: 8888,
    sourceFiles:[
        "index.js"
    ],
    watchFiles:[
        "index.js",
        "index.html"
    ]
}

gulp.task("webserver", function() {
	connect.server({
		livereload: true,
		port: config.serverPort,
		root: config.outDir
	});
});

gulp.task("copy", function() {
    gulp.src("bower_components/traceur/traceur.min.js")
        .pipe(gulp.dest(config.outDir + '/js'));

    gulp.src("bower_components/bg2e-js/js/bg2e.js")
        .pipe(gulp.dest(config.outDir + '/js'));
    
    gulp.src("*.html")
        .pipe(gulp.dest(config.outDir));

    gulp.src("resources/*")
        .pipe(gulp.dest(config.outDir + '/resources'));
});

gulp.task("compile", function() {
    gulp.src(config.sourceFiles)
        .pipe(sort())
        .pipe(traceur())
        .pipe(concat('bg2e-sample.js'))
        .pipe(gulp.dest(config.outDir + '/js'));
});

gulp.task("watch", function() {
    gulp.watch(config.watchFiles,["compile","copy"]);
});

gulp.task("build",["compile","copy"]);
gulp.task("default",["build","webserver","watch"]);

```

## Step 2: add the required plugins
Find the `buildScene()` function, and add the following code at the beginning of the function:

```javascript
buildScene() {
    bg.base.Loader.RegisterPlugin(new bg.base.TextureLoaderPlugin());
    bg.base.Loader.RegisterPlugin(new bg.base.Bg2LoaderPlugin());
    bg.base.Loader.RegisterPlugin(new bg.base.OBJLoaderPlugin());
    ...
```

We have registered three plugins:

- TextureLoaderPlugin: required to load textures.
- Bg2LoaderPlugin: native bg2 model file plugin (in this tutorial we really don't use this plugin, but we'll need it in future tutorials).
- OBJLoaderPlugin: required to load the .obj model.

## Step 3: update createObjects()
Update the createObjects() function to replace the primitive cube with the new loaded cube:

```javascript
createObjects() {
    bg.base.Loader.Load(this.gl, "resources/cube.obj")
        .then((node) => {
            this._sceneRoot.addChild(node);
        })
        .catch((err) => {
            alert(err);
        });

    let floorNode = new bg.scene.Node(this.gl);
    this._sceneRoot.addChild(floorNode);
    floorNode.addComponent(bg.scene.PrimitiveFactory.Plane(this.gl,10));
    floorNode.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(0,-1,0)));
}
```
To load a file, we'll use the bg.base.Loader.Load function. This function receives the rendering context, and a path to load the file. This function returns an [ECMAScript 6 promise object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise).

If you are not familiar with the promise design pattern, you only need to know that is a mechanism used to give a functional approach to the asynchronous functions.

The Promise object includes a `then()` and a `catch()` functions that receive a callback as a parameter. The Promise will call the `then()` callback when the asynchronous operation is complete, and the `catch()` callback if there were an error.

You will see more examples with promises in other tutorials, but it would be recommendable that you check the documentation about ECMAScript 6 promises.

In this case, the `Loader.Load()` function returns a node containing a Drawable component with the loaded model. The only thing that remains to do is to add the returned node to the scene root.

# Build and run
Build the application using gulp and test it using your favorite browser:

![load-3d-model.jpg](load-3d-model.jpg)

Now, you can try to do the same that we have done in this tutorial, but using the [handle input events](input-handle-md) tutorial as starting point.

# Final code
##### index.js
```javascript
class MyWindowController extends bg.app.WindowController {
    constructor() {
        super();
    }

    createObjects() {
        bg.base.Loader.Load(this.gl, "resources/cube.obj")
            .then((node) => {
                this._sceneRoot.addChild(node);
            })
            .catch((err) => {
                alert(err);
            });

        let floorNode = new bg.scene.Node(this.gl);
        this._sceneRoot.addChild(floorNode);
        floorNode.addComponent(bg.scene.PrimitiveFactory.Plane(this.gl,10));
        floorNode.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(0,-1,0)));
    }

    createLight() {
        let lightNode = new bg.scene.Node(this.gl);
        this._sceneRoot.addChild(lightNode);

        lightNode.addComponent(new bg.scene.Light(new bg.base.Light(this.gl)));
        lightNode.addComponent(new bg.scene.Transform(
            new bg.Matrix4.Identity()
                .translate(0,0,5)
                .rotate(bg.Math.degreesToRadians(15),0,1,0)
                .rotate(bg.Math.degreesToRadians(55),-1,0,0)
                .translate(0,1.4,3)
        ));
    }

    createCamera() {
        let cameraNode = new bg.scene.Node(this.gl,"Main camera");
        this._sceneRoot.addChild(cameraNode);

        this._camera = new bg.scene.Camera();
        cameraNode.addComponent(this._camera);
        cameraNode.addComponent(new bg.scene.Transform(
            bg.Matrix4.Translation(0.2,0,0)
                .rotate(bg.Math.degreesToRadians(-25),0,1,0)
                .rotate(bg.Math.degreesToRadians(22.5),-1,0,0)
                .translate(0,0,3)
        ));
    }

    buildScene() {
        bg.base.Loader.RegisterPlugin(new bg.base.TextureLoaderPlugin());
        bg.base.Loader.RegisterPlugin(new bg.base.Bg2LoaderPlugin());
        bg.base.Loader.RegisterPlugin(new bg.base.OBJLoaderPlugin());

        this._sceneRoot = new bg.scene.Node(this.gl,"Scene Root");

        this.createObjects();
        this.createLight();
        this.createCamera();
    }
    
    init() {
        bg.Engine.Set(new bg.webgl1.Engine(this.gl));

        this.buildScene();

        this._renderer = bg.render.Renderer.Create(
            this.gl,
            bg.render.RenderPath.FORWARD
        );
        this._renderer.clearColor = new bg.Color(0.2,0.4,0.7,1);
    }
    
    frame(delta) {
        this._renderer.frame(this._sceneRoot, delta);
    }

    display() {
        this._renderer.display(this._sceneRoot, this._camera);
    }
    
    reshape(width,height) {
        this._camera.viewport = new bg.Viewport(0,0,width,height);
        this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
    }
}

function load(canvas) {
    let ctrl = new MyWindowController();
    let mainLoop = bg.app.MainLoop.singleton;
    
    mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
    mainLoop.canvas = canvas;
    mainLoop.run(ctrl);
}
```