# Environment setup
## Reqirements
bg2 engine is developed using ECMAScript 6, so you need to provide the appropriate execution environment. If you preview that the browser will not support EcmaScript 6, you can use a JavaScript compiler. In this setup, we'll use Traceur.

We'll also use Node, gulp and bower to simplify the installation setup, but you can use any other system, or even setup your environment manually. If you follow theese 

## Setup your computer

You will only need to do the folloging steps once in your computer:

- Download [Node.js](https://nodejs.org/en/) and install it, following the instructions for your platform.
- Once installed, open a terminal window.
- Type the following command to install [bower](https://bower.io) (a package manager that we'll use to download bg2 engine and other dependencies):

#### Mac/Linux:
``` bash
	$ sudo npm install -g bower
```
#### Windows:
``` bash
	> npm install -g bower
```

- Type the following command to install [gulp](http://gulpjs.com) (a JavaScript build system):

#### Mac/Linux:
``` bash
	$ sudo npm install -g gulp
```
#### Windows:
``` bash
	> npm install -g gulp
```

## Prepare your app folder
Create a folder and add to it the following files:

- package.json: this file is used to download and configure our building system (gulp).
- bower.json: this file is used to automatically download all the app dependencies.
- index.html: our main html file.
- index.js: here we'll put the app JavaScript code.
- gulpfile.js: the main file of our build system.

Use this scheme as a template to set up all the sample applications.

### package.json
This file is used to download the building system dependencies using the npm package manager (included with Node.js).

``` json
{
  "name": "sample-app",
  "version": "1.0.0",
  "description": "",
  "author": "Fernando Serrano Carpena <ferserc1@gmail.com>",
  "license": "MIT",
  "devDependencies": {
    "gulp": "^3.9.1",
    "gulp-concat": "^2.6.0",
    "gulp-connect": "^3.1.0",
    "gulp-replace": "^0.5.4",
    "gulp-sort": "^2.0.0",
    "gulp-sourcemaps": "^1.6.0",
    "gulp-traceur": "^0.17.2",
    "gulp-minify": "0.0.14"
  }
}

```

### bower.json
This file is used to configure the bower package manager with the required dependencies. In this case, we only need the traceur ECMAScript 6 compiler, and bg2 engine. You can use this package manager to add more dependencies.

```
{
  "name": "sample-app",
  "authors": [
    "Fernando Serrano Carpena <ferserc1@gmail.com>"
  ],
  "description": "",
  "main": "",
  "license": "MIT",
  "homepage": "http://www.bg2engine.com",
  "dependencies": {
    "traceur": "^0.0.102",
    "bg2e-js": "^1.0.29"
  }
}
```

### gulpfile.js
This is the build script, equivalent to the GNU Makefile or CMake building system.

```
const   gulp = require('gulp'),
        sourcemaps = require('gulp-sourcemaps'),
        traceur = require('gulp-traceur'),
        concat = require('gulp-concat'),
        connect = require('gulp-connect'),
		replace = require('gulp-replace'),
		sort = require('gulp-sort'),
		minify = require('gulp-minify'),
		fs = require('fs'),
		path = require('path');

var config = {
	outDir:'build',
    serverPort: 8888,
    sourceFiles:[
        "index.js"
    ],
    watchFiles:[
        "index.js",
        "index.html"
    ]
}

gulp.task("webserver", function() {
	connect.server({
		livereload: true,
		port: config.serverPort,
		root: config.outDir
	});
});

gulp.task("copy", function() {
    gulp.src("bower_components/traceur/traceur.min.js")
        .pipe(gulp.dest(config.outDir + '/js'));

    gulp.src("bower_components/bg2e-js/js/bg2e.js")
        .pipe(gulp.dest(config.outDir + '/js'));
    
    gulp.src("*.html")
        .pipe(gulp.dest(config.outDir));
});

gulp.task("compile", function() {
    gulp.src(config.sourceFiles)
        .pipe(sort())
        .pipe(traceur())
        .pipe(concat('bg2e-sample.js'))
        .pipe(gulp.dest(config.outDir + '/js'));
});

gulp.task("watch", function() {
    gulp.watch(config.watchFiles,["compile","copy"]);
});

gulp.task("build",["compile","copy"]);
gulp.task("default",["build","webserver","watch"]);

```

### index.html and index.js
The following files contains the minimum code to test our execution environment.

##### index.html

``` html
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" context="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-COMPATIBLE" content="IE=edge">
	
	<script src="js/traceur.min.js"></script>
	<script src="js/bg2e.js"></script>
	<script src="js/bg2e-sample.js"></script>
</head>
<body onload="new bg2eSample.TestClass('Hello, World!')">
    <h1>Hello, World!</h1>
</body>
</html>
```

##### index.html

``` javascript
(function() {

    window.bg2eSample = window.bg2eSample || {};

    class TestClass {
        constructor(greeting) {
            alert(greeting);
        }
    }
    
    bg2eSample.TestClass = TestClass;

})();
```

## Install dependencies
With npm and bower package manager, the application dependencies are installed locally, in your application folder. Npm will install the dependencies in the `node_modules` folder, and bower will do it in the `bower_components` folder.

To download the dependencies, you only need to open a terminal session in the sample app folder and execute the following commands:

```shell
npm install
bower install
```

You only need to do this step once for each sample application.

## Setup comlete! Execute the sample app
Go to the sample project folder in a terminal session and run the following command:

``` shell
gulp
```

This will execute the gulp build system. The build script is configured to setup and execute a web server in `localhost`, using the port 8888 by default (you can change this in the gulpfile.js script). To test the sample application, open your favorite browser and open [http://localhost:8888](http://localhost:8888)


## Examine the code
The build script generate a `build` folder with the compiled JavaScript sample, the dependencies and the HTML file. If you check the `build/js/bg2e-sample.js` file, you can view how the traceur compiler translate the ECMAScript 6 code into JavaScript 5:

```javascript
"use strict";
(function() {
  window.bg2eSample = window.bg2eSample || {};
  var TestClass = function() {
    function TestClass(greeting) {
      alert(greeting);
    }
    return ($traceurRuntime.createClass)(TestClass, {}, {});
  }();
  bg2eSample.TestClass = TestClass;
})();
```

Note that you can access the bg2 engine API directly from JavaScript 5, but in all the sample code we'll always use ECMAScript 6.
