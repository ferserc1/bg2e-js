# Load scene
### Prerrequisites
Use the resulting folder of [load 3D model](load-models.md) tutorial as the starting source code for this sample app.

## Loader
In the previous tutorial, we use the loader API to load a 3d model from the server and append it to a scene.

In this example, we'll modify the [load 3D model](load-models.md) sample app to load a scene stored on the server.

## Step 1: copy the resources and update the gulpfile.js
Download the [resources for this tutorial](robot.zip). Extract the zip file, and place its contents into the "resources" folder from the previous tutorial. You can also remove the files from the previous tutorial:

```
    load-scene-tutorial
        bower.json
        gulpfile.js
        index.html
        index.js
        package.json
        resources
            0757a369-79ee-4325-b211-607d89b809dd.vwglb
            084cc79a-b40f-43fa-b953-8e7c53e43eae.vwglb
            1e253949-841d-4261-8e07-d1b18f2b44d6.vwglb
            2e52cc6d-e2da-44dd-9262-6080e9ae8bff.vwglb
            61bfc6ea-a334-4ee2-be4a-69ab7a1c85a6.vwglb
            d743fc92-453e-4cc7-bc7d-d1365b1e585d.vwglb
            ee83fadb-c202-463f-9c5c-9529a83ae27a.vwglb
            scene.vitscnj
```

This is a basic scene created with bg2 engine Composer tool (you can download it from [http://www.bg2engine.com](http://www.bg2engine.com))


## Step 2: add the required plugins
Find the `buildScene()` function, and modify the code to register the loader plugins. In this case, we don't need the OBJ loader plugin, but we need to register the scene loader plugin:

```javascript
buildScene() {
    bg.base.Loader.RegisterPlugin(new bg.base.TextureLoaderPlugin());
    bg.base.Loader.RegisterPlugin(new bg.base.Bg2LoaderPlugin());
    bg.base.Loader.RegisterPlugin(new bg.base.SceneLoaderPlugin());
    ...
```

## Step 3: remove the old functions used to create the scene
Remove the functions `createObjects()`, `createLight()` and `createCamera()`, because now we will load the entire scene from a file.

## Step 4: modify the `buildScene()` function to load the scene.
Remove the code that create the scene root node and the calls to `createObjects()`, `createLight()` and `createCamera()`. Add the following code instead:

```javascript
bg.base.Loader.Load(this.gl, "resources/scene.vitscnj")
            .then((sceneData) => {
                this._camera = sceneData.cameraNode.camera;
                this._sceneRoot = sceneData.sceneRoot;
                this.postReshape();
                this.postRedisplay();
            });
```

The SceneLoaderPlugin returns an object that contains the scene root and the first camera defined in the scene (a scene may contain more tan one camera):

```json
    {
        cameraNode: { [Camera scene node] },
        sceneRoot: { [Scene root node] }
    }
```

Note that the cameraNode element of the object may be null if the scene does not contains any camera.

## Step 5: modify the `frame()`, `display()` and `reshape()` functions
As you should imagine, the Load methdo from the loader is asynchronous, and if you check our new `buildScene()` function, you may notice that the _sceneRoot variable will be null when the window controller calls the frame, display and reshape functions.

To avoid errors, you must check that the _sceneRoot and _camera variables are valid before call the corresponding renderer and camera functions.

``` javascript
 frame(delta) {
        if (this._sceneRoot) {
            this._renderer.frame(this._sceneRoot, delta);
        }
    }

    display() {
        if (this._sceneRoot && this._camera) {
            this._renderer.display(this._sceneRoot, this._camera);
        }
    }
    
    reshape(width,height) {
        if (this._camera) {
            this._camera.viewport = new bg.Viewport(0,0,width,height);
            this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
        }
    }
```

Note that in the promise that returns the Loader.Load function, we have added the following functions:

```
    this.postReshape();
    this.postRedisplay();
```

This functions will post a resize and a display command when the scene is loaded.

# Build and run
Build the application using gulp and test it using your favorite browser:

![load-scene.jpg](load-scene.jpg)

# Final code
##### index.js
```javascript
class MyWindowController extends bg.app.WindowController {
    constructor() {
        super();
    }

    buildScene() {
        bg.base.Loader.RegisterPlugin(new bg.base.TextureLoaderPlugin());
        bg.base.Loader.RegisterPlugin(new bg.base.Bg2LoaderPlugin());
        bg.base.Loader.RegisterPlugin(new bg.base.SceneLoaderPlugin());

        bg.base.Loader.Load(this.gl, "resources/scene.vitscnj")
            .then((sceneData) => {
                this._camera = sceneData.cameraNode.camera;
                this._sceneRoot = sceneData.sceneRoot;
                this.postReshape();
                this.postRedisplay();
            });
    }
    
    init() {
        bg.Engine.Set(new bg.webgl1.Engine(this.gl));

        this.buildScene();

        this._renderer = bg.render.Renderer.Create(
            this.gl,
            bg.render.RenderPath.FORWARD
        );
        this._renderer.clearColor = new bg.Color(0.2,0.4,0.7,1);
    }
    
    frame(delta) {
        if (this._sceneRoot) {
            this._renderer.frame(this._sceneRoot, delta);
        }
    }

    display() {
        if (this._sceneRoot && this._camera) {
            this._renderer.display(this._sceneRoot, this._camera);
        }
    }
    
    reshape(width,height) {
        if (this._camera) {
            this._camera.viewport = new bg.Viewport(0,0,width,height);
            this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
        }
    }
}

function load(canvas) {
    let ctrl = new MyWindowController();
    let mainLoop = bg.app.MainLoop.singleton;
    
    mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
    mainLoop.canvas = canvas;
    mainLoop.run(ctrl);
}
```
