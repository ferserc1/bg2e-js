# Configuración del entorno
## Requisitos
bg2 engine está desarrollado utilizando ECMAScript 6, así que necesitaos proporcionar el entorno de ejecución adecuado. Si prevees que el navegador no va a soportar ECMAScript 6, puedes utilizar un compilador de JavaScript. En esta instalación, utilizaremos Traceur.

También utilizaremos Node.js, gulp y bower para simlificar el proceso de instalación, pero puedes utilizar cualquier otro sistema, o incluso preparar el entorno de desarrollo manualmente.

## Configurar tu ordenador
Solamente necesitas ejecutar estos pasos una vez en tu ordenador:

- Descarga e instala [Node.js](https://nodejs.org/en/), siguiendo las instrucciones específicas para tu plataforma.
- Una vez instalado, abre una ventana de la terminal.
- Escribe el siguiente comando para instalar [bower](https://bower.io) (el gestor de paquetes que usaremos para descargar bg2 engine y otras dependencias):

#### Mac/Linux:
``` bash
	$ sudo npm install -g bower
```
#### Windows:
``` bash
	> npm install -g bower
```

- Escribe el siguiente comando para instalar [gulp](http://gulpjs.com) (un sistema de compilación basado en JavaScript):

#### Mac/Linux:
``` bash
	$ sudo npm install -g gulp
```
#### Windows:
``` bash
	> npm install -g gulp
```

## Prepara el directorio de la aplicación
Crea un directorio y añade los siguientes ficheros:

- package.json: este fichero se utiliza para descargar y configurar el sistema de building (gulp).
- bower.json: este archivo se utilizará para descargar automáticamente las dependencias de la app.
- index.html: nuestro fichero html principal.
- index.js: aquí pondremos nuestro código JavaScript.
- gulpfile.js: el script principal del sistema de building.

Utiliza este esquema como plantilla para configurar el resto de las aplicaciones del tutorial.

### package.json
Este fichero se utiliza para descargar el sistema de building utilizando el gestor de paquetes npm (que está incluido con Node.js).

``` json
{
  "name": "sample-app",
  "version": "1.0.0",
  "description": "",
  "author": "Fernando Serrano Carpena <ferserc1@gmail.com>",
  "license": "MIT",
  "devDependencies": {
    "gulp": "^3.9.1",
    "gulp-concat": "^2.6.0",
    "gulp-connect": "^3.1.0",
    "gulp-replace": "^0.5.4",
    "gulp-sort": "^2.0.0",
    "gulp-sourcemaps": "^1.6.0",
    "gulp-traceur": "^0.17.2",
    "gulp-minify": "0.0.14"
  }
}

```

### bower.json
Este archivo se utiliza para configurar el gestor de paquetes bower para descargar las dependencias necesarias. En este caso, las dependencias son el motor gráfico y el compilador de ECMAScrip 6 Traceur.

```
{
  "name": "sample-app",
  "authors": [
    "Fernando Serrano Carpena <ferserc1@gmail.com>"
  ],
  "description": "",
  "main": "",
  "license": "MIT",
  "homepage": "http://www.bg2engine.com",
  "dependencies": {
    "traceur": "^0.0.102",
    "bg2e-js": "^1.0.28"
  }
}
```

### gulpfile.js
Este es el script de compilación, equivalente a GNU Makefile o CMake.

```
const   gulp = require('gulp'),
        sourcemaps = require('gulp-sourcemaps'),
        traceur = require('gulp-traceur'),
        concat = require('gulp-concat'),
        connect = require('gulp-connect'),
		replace = require('gulp-replace'),
		sort = require('gulp-sort'),
		minify = require('gulp-minify'),
		fs = require('fs'),
		path = require('path');

var config = {
	outDir:'build',
    serverPort: 8888,
    sourceFiles:[
        "index.js"
    ],
    watchFiles:[
        "index.js",
        "index.html"
    ]
}

gulp.task("webserver", function() {
	connect.server({
		livereload: true,
		port: config.serverPort,
		root: config.outDir
	});
});

gulp.task("copy", function() {
    gulp.src("bower_components/traceur/traceur.min.js")
        .pipe(gulp.dest(config.outDir + '/js'));

    gulp.src("bower_components/bg2e-js/js/bg2e.js")
        .pipe(gulp.dest(config.outDir + '/js'));
    
    gulp.src("*.html")
        .pipe(gulp.dest(config.outDir));
});

gulp.task("compile", function() {
    gulp.src(config.sourceFiles)
        .pipe(sort())
        .pipe(traceur())
        .pipe(concat('bg2e-sample.js'))
        .pipe(gulp.dest(config.outDir + '/js'));
});

gulp.task("watch", function() {
    gulp.watch(config.watchFiles,["compile","copy"]);
});

gulp.task("build",["compile","copy"]);
gulp.task("default",["build","webserver","watch"]);

```

### index.html e index.js
Los siguientes archivos contienen el código mínimo para probar nuestro entorno de ejecución.

##### index.html

``` html
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" context="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-COMPATIBLE" content="IE=edge">
	
	<script src="js/traceur.min.js"></script>
	<script src="js/bg2e.js"></script>
	<script src="js/bg2e-sample.js"></script>
</head>
<body onload="new bg2eSample.TestClass('Hello, World!')">
    <h1>Hello, World!</h1>
</body>
</html>
```

##### index.html

``` javascript
(function() {

    window.bg2eSample = window.bg2eSample || {};

    class TestClass {
        constructor(greeting) {
            alert(greeting);
        }
    }
    
    bg2eSample.TestClass = TestClass;

})();
```

## Instalar las depepndencias
Con los gestores de paquetes npm y bower, las dependencias se instalan de forma local en el directorio de la aplicación. Npm guardará las dependencias en el directorio `node_modules`, y bower las instalará en el directorio `bower_components`.

Para descargar las dependencias, solamente tienes que abrir una terminal en el mismo directorio que has creado y ejecutar los siguientes comandos:

```shell
npm install
bower install
```

IMPORTANTE: Ten en cuenta que tendrás que ejecutar estos comandos para cada aplicación que crees, ya que las dependencias se instalan localmente en cada una de ellas.

## ¡Instalación completa! Ejecuta la aplicación
Abre una sesión de terminal en el directorio del proyecto y ejecuta el siguiente comando:

``` shell
gulp
```
Esto ejecutará el script de compilación. El script está configurado para instalar y ejecutar un servidor web en `localhost`, utilizando por defecto el puerto 8888. (puedes cambiar esto en el fichero gulpfile.js). Para probar la aplicación, abre la url [http://localhost:8888](http://localhost:8888) en tu navegador favorito.


## Examina el código
El script de compilación genera un directorio `build` con el código JavaScript compilado, las dependencias y el fichero HTML. Si abres el archivo `build/js/bg2e-sample.js`, puedes comprobar cómo se ha traducido el código ECMAScript 6 a JavaScript 5:

```javascript
"use strict";
(function() {
  window.bg2eSample = window.bg2eSample || {};
  var TestClass = function() {
    function TestClass(greeting) {
      alert(greeting);
    }
    return ($traceurRuntime.createClass)(TestClass, {}, {});
  }();
  bg2eSample.TestClass = TestClass;
})();
```

Ten en cuenta que puedes acceder al API de bg2 engine directamente desde JavaScript 5, pero en todo el código de ejemplo usaremos siempre ECMAScript 6.
