# Handle input events
### Prerrequisites
Use the resulting folder of [Renderer and scenes](basic-renderer.md) tutorial as the starting source code for this sample app.

## Events and scene components
With bg2 engine is extremely easy to provide a behavior to a scene object in response to a user action. The scene components can handle the input events, and the only thing that we have to do is to ensure that the events are dispatched to the scene. Once done this, each component will have access to the user input events.

To connect the user events to the scene, we'll use a SceneVisitor. The visitor is a design pattern used to apply some action to a complex data structure, such as a tree, a vector or a graph. In this case, we'll use a 'bg.scene.InputVisitor'. Add the following code immediately after the renderer creation:

```javascript
this._inputVisitor = new bg.scene.InputVisitor();
```

Once done this, we need to map all window controller event functions to the input visitor. Add the following code at the end of your window controller class.

```javascript
keyDown(evt) { this._inputVisitor.keyDown(this._sceneRoot, evt); }	
keyUp(evt) { this._inputVisitor.keyDown(this._sceneRoot, evt); }
mouseUp(evt) { this._inputVisitor.mouseUp(this._sceneRoot, evt); }
mouseDown(evt) { this._inputVisitor.mouseDown(this._sceneRoot, evt); }
mouseMove(evt) { this._inputVisitor.mouseMove(this._sceneRoot, evt); }
mouseOut(evt) { this._inputVisitor.mouseOut(this._sceneRoot, evt); }
mouseDrag(evt) { this._inputVisitor.mouseDrag(this._sceneRoot, evt); }
mouseWheel(evt) { this._inputVisitor.mouseWheel(this._sceneRoot, evt); }
touchStart(evt) { this._inputVisitor.touchStart(this._sceneRoot, evt); }
touchMove(evt) { this._inputVisitor.touchMove(this._sceneRoot, evt); }
touchEnd(evt) { this._inputVisitor.touchEnd(this._sceneRoot, evt); }
```

This code is very straightforward: each input visitor funcion receive the scene root and the input event, and the input visitor functions have the same name as the corresponding callbacks in the window controller.

## Using the input events
With this modifications, the scene can handle the input events. The only thing that remains to do is to add some behavior to the scene that use the input events.

To do it, we going to add a bg.manipulation.OrbitCameraController component to the camera node. This component allows to manipulate the camera using a mouse or a touch screen.

Find the createCamera() function and replace the camera component creation code with this new one:

```javascript
this._camera = new bg.scene.Camera();
cameraNode.addComponent(this._camera);
cameraNode.addComponent(new bg.scene.Transform());
cameraNode.addComponent(new bg.manipulation.OrbitCameraController());
```

## Build and run
Build the application using gulp and test it using your favorite browser:

![basic-renderer.jpg](basic-renderer.jpg)

Now you can change the camera point of view using the mouse or a touch screen.

## Final code
##### index.js
```javascript
class MyWindowController extends bg.app.WindowController {
    constructor() {
        super();
    }

    createObjects() {
        let cubeNode = new bg.scene.Node(this.gl);
        this._sceneRoot.addChild(cubeNode);
        cubeNode.addComponent(bg.scene.PrimitiveFactory.Cube(this.gl,1,1,1));

        let floorNode = new bg.scene.Node(this.gl);
        this._sceneRoot.addChild(floorNode);
        floorNode.addComponent(bg.scene.PrimitiveFactory.Plane(this.gl,10));
        floorNode.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(0,-1,0)));
    }

    createLight() {
        let lightNode = new bg.scene.Node(this.gl);
        this._sceneRoot.addChild(lightNode);

        lightNode.addComponent(new bg.scene.Light(new bg.base.Light(this.gl)));
        lightNode.addComponent(new bg.scene.Transform(
            new bg.Matrix4.Identity()
                .translate(0,0,5)
                .rotate(bg.Math.degreesToRadians(15),0,1,0)
                .rotate(bg.Math.degreesToRadians(55),-1,0,0)
                .translate(0,1.4,3)
        ));
    }

    createCamera() {
        let cameraNode = new bg.scene.Node(this.gl,"Main camera");
        this._sceneRoot.addChild(cameraNode);

        this._camera = new bg.scene.Camera();
        cameraNode.addComponent(this._camera);
        cameraNode.addComponent(new bg.scene.Transform());
        cameraNode.addComponent(new bg.manipulation.OrbitCameraController());
    }

    buildScene() {
        this._sceneRoot = new bg.scene.Node(this.gl,"Scene Root");

        this.createObjects();
        this.createLight();
        this.createCamera();
    }
    
    init() {
        bg.Engine.Set(new bg.webgl1.Engine(this.gl));

        this.buildScene();

        this._renderer = bg.render.Renderer.Create(
            this.gl,
            bg.render.RenderPath.FORWARD
        );
        this._renderer.clearColor = new bg.Color(0.2,0.4,0.7,1);

        this._inputVisitor = new bg.scene.InputVisitor();
    }
    
    frame(delta) {
        this._renderer.frame(this._sceneRoot, delta);
    }

    display() {
        this._renderer.display(this._sceneRoot, this._camera);
    }
    
    reshape(width,height) {
        this._camera.viewport = new bg.Viewport(0,0,width,height);
        this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
    }

    keyDown(evt) { this._inputVisitor.keyDown(this._sceneRoot, evt); }	
	keyUp(evt) { this._inputVisitor.keyDown(this._sceneRoot, evt); }
	mouseUp(evt) { this._inputVisitor.mouseUp(this._sceneRoot, evt); }
	mouseDown(evt) { this._inputVisitor.mouseDown(this._sceneRoot, evt); }
	mouseMove(evt) { this._inputVisitor.mouseMove(this._sceneRoot, evt); }
	mouseOut(evt) { this._inputVisitor.mouseOut(this._sceneRoot, evt); }
	mouseDrag(evt) { this._inputVisitor.mouseDrag(this._sceneRoot, evt); }
	mouseWheel(evt) { this._inputVisitor.mouseWheel(this._sceneRoot, evt); }
	touchStart(evt) { this._inputVisitor.touchStart(this._sceneRoot, evt); }
	touchMove(evt) { this._inputVisitor.touchMove(this._sceneRoot, evt); }
	touchEnd(evt) { this._inputVisitor.touchEnd(this._sceneRoot, evt); }
}

function load(canvas) {
    let ctrl = new MyWindowController();
    let mainLoop = bg.app.MainLoop.singleton;
    
    mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
    mainLoop.canvas = canvas;
    mainLoop.run(ctrl);
}
```